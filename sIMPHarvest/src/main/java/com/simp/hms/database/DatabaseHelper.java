package com.simp.hms.database;

import com.simp.hms.model.AbsentType;
import com.simp.hms.model.AncakPanenHeader;
import com.simp.hms.model.AncakPanenQuality;
import com.simp.hms.model.BLKRISET;
import com.simp.hms.model.BLKSUGC;
import com.simp.hms.model.BPNKaretReportBlock;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.ConfigApp;
import com.simp.hms.model.DeviceAlias;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.model.BJR;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.BLKPLT;
import com.simp.hms.model.BLKSBC;
import com.simp.hms.model.BLKSBCDetail;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.DayOff;
import com.simp.hms.model.Employee;
import com.simp.hms.model.ForemanActive;
import com.simp.hms.model.GroupHead;
import com.simp.hms.model.MasterDownload;
import com.simp.hms.model.Penalty;
import com.simp.hms.model.SKB;
import com.simp.hms.model.SPBSDestination;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSRunningNumber;
import com.simp.hms.model.SPBSWB;
import com.simp.hms.model.SPTA;
import com.simp.hms.model.SPTARunningNumber;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.TaksasiLine;
import com.simp.hms.model.UserApp;
import com.simp.hms.model.UserLogin;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.model.Vendor;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public final static int dbVersion = 151;
    public final static String dbName = "hms.db";

    public DatabaseHelper(Context context) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase) {
        String query;

        query = "create table " + MasterDownload.TABLE_NAME +
                "(" +
                MasterDownload.XML_NAME + " text not null, " +
                MasterDownload.XML_FILENAME + " text, " +
                MasterDownload.XML_SYNC_DATE + " integer, " +
                MasterDownload.XML_STATUS + " integer, " +
                "primary key (" +
                MasterDownload.XML_NAME +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SKB.TABLE_NAME +
                "(" +
                SKB.XML_COMPANY_CODE + " text, " +
                SKB.XML_ESTATE + " text, " +
                SKB.XML_BLOCK + " text, " +
                SKB.XML_BARIS_SKB + " integer, " +
                SKB.XML_VALID_FROM + " text, " +
                SKB.XML_VALID_TO + " text,  " +
                SKB.XML_BARIS_BLOCK + " integer, " +
                SKB.XML_JUMLAH_POKOK + " double, " +
                SKB.XML_POKOK_MATI + " double, " +
                SKB.XML_TANGGAL_TANAM + " text, " +
                SKB.XML_LINE_SKB + " integer, " +
                "primary key (" +
                SKB.XML_COMPANY_CODE + ", " +
                SKB.XML_ESTATE + ", " +
                SKB.XML_BLOCK + ", " +
                SKB.XML_BARIS_SKB + ", " +
                SKB.XML_VALID_FROM + ", " +
                SKB.XML_BARIS_BLOCK + ", " +
                SKB.XML_LINE_SKB +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BlockHdrc.TABLE_NAME +
                "(" +
                BlockHdrc.XML_COMPANY_CODE + " text, " +
                BlockHdrc.XML_ESTATE + " text, " +
                BlockHdrc.XML_BLOCK + " text, " +
                BlockHdrc.XML_VALID_FROM + " text, " +
                BlockHdrc.XML_VALID_TO + " text, " +
                BlockHdrc.XML_DIVISION + " text, " +
                BlockHdrc.XML_TYPE + " text, " +
                BlockHdrc.XML_STATUS + " text, " +
                BlockHdrc.XML_PROJECT_DEFINITION + " text, " +
                BlockHdrc.XML_OWNER + " text, " +
                BlockHdrc.XML_TGL_TANAM + " text, " +
                BlockHdrc.XML_MANDT + " text, " +
                "primary key (" +
                BlockHdrc.XML_COMPANY_CODE + ", " +
                BlockHdrc.XML_ESTATE + ", " +
                BlockHdrc.XML_BLOCK + ", " +
                BlockHdrc.XML_VALID_FROM + ", " +
                BlockHdrc.XML_DIVISION + ", " +
                BlockHdrc.XML_MANDT +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKSBC.TABLE_NAME +
                "(" +
                BLKSBC.XML_COMPANY_CODE + " text not null, " +
                BLKSBC.XML_ESTATE + " text not null, " +
                BLKSBC.XML_BLOCK + " text not null, " +
                BLKSBC.XML_VALID_FROM + " text not null, " +
                BLKSBC.XML_BASIS_NORMAL + " double, " +
                BLKSBC.XML_BASIS_FRIDAY + " double, " +
                BLKSBC.XML_BASIS_HOLIDAY + " double, " +
                BLKSBC.XML_PREMI_NORMAL + " double, " +
                BLKSBC.XML_PREMI_FRIDAY + " double, " +
                BLKSBC.XML_PREMI_HOLIDAY + " double, " +
                BLKSBC.XML_PRIORITY + " text, " +
                "primary key (" +
                BLKSBC.XML_COMPANY_CODE + ", " +
                BLKSBC.XML_ESTATE + ", " +
                BLKSBC.XML_BLOCK + ", " +
                BLKSBC.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKSBCDetail.TABLE_NAME +
                "(" +
                BLKSBCDetail.XML_COMPANY_CODE + " text not null, " +
                BLKSBCDetail.XML_ESTATE + " text not null, " +
                BLKSBCDetail.XML_BLOCK + " text not null, " +
                BLKSBCDetail.XML_VALID_FROM + " text not null, " +
                BLKSBCDetail.XML_MIN_VAL + " double, " +
                BLKSBCDetail.XML_MAX_VAL + " double, " +
                BLKSBCDetail.XML_OVER_BASIC_RATE + " double, " +
                "primary key (" +
                BLKSBCDetail.XML_COMPANY_CODE + ", " +
                BLKSBCDetail.XML_ESTATE + ", " +
                BLKSBCDetail.XML_BLOCK + ", " +
                BLKSBCDetail.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BJR.TABLE_NAME +
                "(" +
                BJR.XML_COMPANY_CODE + " text, " +
                BJR.XML_ESTATE + " text, " +
                BJR.XMl_EFF_DATE + " text, " +
                BJR.XML_BLOCK + " text, " +
                BJR.XML_BJR + " double, " +
                "primary key (" +
                BJR.XML_COMPANY_CODE + ", " +
                BJR.XML_ESTATE + ", " +
                BJR.XMl_EFF_DATE + ", " +
                BJR.XML_BLOCK +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKPLT.TABLE_NAME +
                "(" +
                BLKPLT.XML_COMPANY_CODE + " text, " +
                BLKPLT.XML_ESTATE + " text, " +
                BLKPLT.XML_BLOCK + " text, " +
                BLKPLT.XML_VALID_FROM + " text, " +
                BLKPLT.XML_VALID_TO + " text, " +
                BLKPLT.XML_CROP_TYPE + " text, " +
                BLKPLT.XML_PREVIOUS_CROP + " text, " +
                BLKPLT.XML_FINISH_DATE + " text, " +
                BLKPLT.XML_REFERENCE + " text, " +
                BLKPLT.XML_JARAK_TANAM + " text, " +
                BLKPLT.XML_HARVESTING_DATE + " text, " +
                BLKPLT.XML_HARVESTED + " double, " +
                BLKPLT.XML_PLAN_DATE + " text, " +
                BLKPLT.XML_TOPOGRAPHY + " text, " +
                BLKPLT.XML_SOIL_TYPE + " text, " +
                BLKPLT.XML_SOIL_CATEGORY + " text, " +
                BLKPLT.XML_PROD_TREES + " double, " +
                "primary key (" +
                BLKPLT.XML_COMPANY_CODE + ", " +
                BLKPLT.XML_ESTATE + ", " +
                BLKPLT.XML_BLOCK + ", " +
                BLKPLT.XML_VALID_FROM + ", " +
                BLKPLT.XML_CROP_TYPE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + Employee.TABLE_NAME +
                "(" +
                Employee.XML_COMPANY_CODE + " text, " +
                Employee.XML_ESTATE + " text, " +
                Employee.XML_FISCAL_YEAR + " integer, " +
                Employee.XML_FISCAL_PERIOD + " integer, " +
                Employee.XML_NIK + " text, " +
                Employee.XML_NAME + " text, " +
                Employee.XML_TERM_DATE + " text, " +
                Employee.XML_DIVISION + " text, " +
                Employee.XML_ROLE_ID + " text, " +
                Employee.XML_JOB_POS + " text, " +
                Employee.XML_GANG + " text, " +
                Employee.XML_COST_CENTER + " text, " +
                Employee.XML_EMP_TYPE + " text, " +
                Employee.XML_VALID_FROM + " text, " +
                Employee.XML_HARVESTER_CODE + " text, " +
                "primary key (" +
                Employee.XML_COMPANY_CODE + ", " +
                Employee.XML_ESTATE + ", " +
                Employee.XML_FISCAL_YEAR + ", " +
                Employee.XML_FISCAL_PERIOD + ", " +
                Employee.XML_NIK +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + UserApp.TABLE_NAME +
                "(" +
                UserApp.XML_NIK + " text not null, " +
                UserApp.XML_USERNAME + " text not null, " +
                UserApp.XML_PASSWORD + " text not null, " +
                UserApp.XML_VALID_TO + " integer not null, " +
                UserApp.XML_CREATED_DATE + " integer, " +
                UserApp.XML_CREATED_BY + " text, " +
                "primary key (" +
                UserApp.XML_NIK + ", " +
                UserApp.XML_USERNAME +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + AbsentType.TABLE_NAME +
                "(" +
                AbsentType.XML_COMPANY_CODE + " text not null, " +
                AbsentType.XML_ABSENT_TYPE + " text not null, " +
                AbsentType.XML_DESCRIPTION + " text, " +
                AbsentType.XML_HKRLLO + " double, " +
                AbsentType.XML_HKRLHI + " double, " +
                AbsentType.XML_HKPYLO + " double, " +
                AbsentType.XML_HKPYHI + " double, " +
                AbsentType.XML_HKVLLO + " double, " +
                AbsentType.XML_HKVLHI + " double, " +
                AbsentType.XML_HKMEIN + " double, " +
                AbsentType.XML_AGROUP + " double, " +
                "primary key (" +
                AbsentType.XML_COMPANY_CODE + ", " +
                AbsentType.XML_ABSENT_TYPE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + DayOff.TABLE_NAME +
                "(" +
                DayOff.XML_ESTATE + " text not null, " +
                DayOff.XML_DATE + " text not null, " +
                DayOff.XML_DAY_OFF_TYPE + " text not null, " +
                DayOff.XML_DESCRIPTION + " text, " +
                "primary key (" +
                DayOff.XML_ESTATE + ", " +
                DayOff.XML_DATE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + DivisionAssistant.TABLE_NAME +
                "(" +
                DivisionAssistant.XML_ESTATE + " text not null, " +
                DivisionAssistant.XML_DIVISION + " text not null, " +
                DivisionAssistant.XML_SPRAS + " text, " +
                DivisionAssistant.XML_DESCRIPTION + " text, " +
                DivisionAssistant.XML_ASSISTANT + " text, " +
                DivisionAssistant.XML_DISTANCE_TO_MILL + " text, " +
                DivisionAssistant.XML_UOM + " text, " +
                DivisionAssistant.XML_ASSISTANT_NAME + " text, " +
                DivisionAssistant.XML_LIFNR + " text, " +
                "primary key (" +
                DivisionAssistant.XML_ESTATE + ", " +
                DivisionAssistant.XML_DIVISION +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        //transaction

        query = "create table " + ForemanActive.TABLE_NAME +
                "(" +
                ForemanActive.XML_ID + " integer not null, " +
                ForemanActive.XML_COMPANY_CODE + " text not null, " +
                ForemanActive.XML_ESTATE + " text not null, " +
                ForemanActive.XML_FISCAL_YEAR + " integer, " +
                ForemanActive.XML_FISCAL_PERIOD + " integer, " +
                ForemanActive.XML_NIK + " text not null, " +
                ForemanActive.XML_NAME + " text not null, " +
                ForemanActive.XML_TERM_DATE + " text, " +
                ForemanActive.XML_DIVISION + " text not null, " +
                ForemanActive.XML_ROLE_ID + " text, " +
                ForemanActive.XML_JOB_POS + " text not null, " +
                ForemanActive.XML_GANG + " text not null, " +
                ForemanActive.XML_COST_CENTER + " text, " +
                ForemanActive.XML_EMP_TYPE + " text, " +
                ForemanActive.XML_VALID_FROM + " text, " +
                ForemanActive.XML_HARVESTER_CODE + " text, " +
                "primary key (" +
                ForemanActive.XML_ID +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + UserLogin.TABLE_NAME +
                "(" +
                UserLogin.XML_ID + " integer not null, " +
                UserLogin.XML_COMPANY_CODE + " text not null, " +
                UserLogin.XML_ESTATE + " text not null, " +
                UserLogin.XML_FISCAL_YEAR + " integer, " +
                UserLogin.XML_FISCAL_PERIOD + " integer, " +
                UserLogin.XML_NIK + " text not null, " +
                UserLogin.XML_NAME + " text not null, " +
                UserLogin.XML_TERM_DATE + " text, " +
                UserLogin.XML_DIVISION + " text not null, " +
                UserLogin.XML_ROLE_ID + " text, " +
                UserLogin.XML_JOB_POS + " text not null, " +
                UserLogin.XML_GANG + " text not null, " +
                UserLogin.XML_COST_CENTER + " text, " +
                UserLogin.XML_EMP_TYPE + " text, " +
                UserLogin.XML_VALID_FROM + " text, " +
                UserLogin.XML_HARVESTER_CODE + " text, " +
                "primary key (" +
                UserLogin.XML_ID +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BPNHeader.TABLE_NAME +
                "(" +
                BPNHeader.XML_BPN_ID + " text not null, " +
                BPNHeader.XML_IMEI + " text not null, " +
                BPNHeader.XML_COMPANY_CODE + " text not null, " +
                BPNHeader.XML_ESTATE + " text not null, " +
                BPNHeader.XML_BPN_DATE + " text not null, " +
                BPNHeader.XML_DIVISION + " text not null, " +
                BPNHeader.XML_GANG + " text not null, " +
                BPNHeader.XML_LOCATION + " text not null, " +
                BPNHeader.XML_TPH + " text not null, " +
                BPNHeader.XML_NIK_HARVESTER + " text not null, " +
                BPNHeader.XML_HARVESTER + " text not null, " +
                BPNHeader.XML_NIK_FOREMAN + " text not null, " +
                BPNHeader.XML_FOREMAN + " text not null, " +
                BPNHeader.XML_NIK_CLERK + " text not null, " +
                BPNHeader.XML_CLERK + " text not null, " +
                BPNHeader.XML_USE_GERDANG + " integer default (0), " +
                BPNHeader.XML_CROP + " text not null, " +
                BPNHeader.XML_GPS_KOORDINAT + " text not null, " +
                BPNHeader.XML_PHOTO + " text, " +
                BPNHeader.XML_STATUS + " integer not null default (0), " +
                BPNHeader.XML_SPBS_NUMBER + " text, " +
                BPNHeader.XML_CREATED_DATE + " integer, " +
                BPNHeader.XML_CREATED_BY + " text, " +
                BPNHeader.XML_MODIFIED_DATE + " integer, " +
                BPNHeader.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                BPNHeader.XML_BPN_ID +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BPNQuantity.TABLE_NAME +
                "(" +
                BPNQuantity.XML_BPN_ID + " text not null, " +
                BPNQuantity.XML_IMEI + " text not null, " +
                BPNQuantity.XML_COMPANY_CODE + " text not null, " +
                BPNQuantity.XML_ESTATE + " text not null, " +
                BPNQuantity.XML_BPN_DATE + " text not null, " +
                BPNQuantity.XML_DIVISION + " text not null, " +
                BPNQuantity.XML_GANG + " text not null, " +
                BPNQuantity.XML_LOCATION + " text not null, " +
                BPNQuantity.XML_TPH + " text not null, " +
                BPNQuantity.XML_NIK_HARVESTER + " text not null, " +
                BPNQuantity.XML_CROP + " text not null, " +
                BPNQuantity.XML_ACHIEVEMENT_CODE + " text not null, " +
                BPNQuantity.XML_QUANTITY + " double not null default (0), " +
                BPNQuantity.XML_QUANTITY_REMAINING + " double not null default (0), " +
                BPNQuantity.XML_STATUS + " integer not null default (0), " +
                BPNQuantity.XML_CREATED_DATE + " integer, " +
                BPNQuantity.XML_CREATED_BY + " text, " +
                BPNQuantity.XML_MODIFIED_DATE + " integer, " +
                BPNQuantity.XML_MODIFIED_BY + " text, " +
                BPNQuantity.XML_MODIFIED_DATE_STR + " text, " +
                "primary key (" +
                BPNQuantity.XML_BPN_ID + ", " +
                BPNQuantity.XML_IMEI + ", " +
                BPNQuantity.XML_COMPANY_CODE + ", " +
                BPNQuantity.XML_ESTATE + ", " +
                BPNQuantity.XML_BPN_DATE + ", " +
                BPNQuantity.XML_DIVISION + ", " +
                BPNQuantity.XML_GANG + ", " +
                BPNQuantity.XML_LOCATION + ", " +
                BPNQuantity.XML_TPH + ", " +
                BPNQuantity.XML_NIK_HARVESTER + ", " +
                BPNQuantity.XML_CROP + ", " +
                BPNQuantity.XML_ACHIEVEMENT_CODE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BPNQuality.TABLE_NAME +
                "(" +
                BPNQuality.XML_BPN_ID + " text not null, " +
                BPNQuality.XML_IMEI + " text not null, " +
                BPNQuality.XML_COMPANY_CODE + " text not null, " +
                BPNQuality.XML_ESTATE + " text not null, " +
                BPNQuality.XML_BPN_DATE + " text not null, " +
                BPNQuality.XML_DIVISION + " text not null, " +
                BPNQuality.XML_GANG + " text not null, " +
                BPNQuality.XML_LOCATION + " text not null, " +
                BPNQuality.XML_TPH + " text not null, " +
                BPNQuality.XML_NIK_HARVESTER + " text not null, " +
                BPNQuality.XML_CROP + " text not null, " +
                BPNQuality.XML_ACHIEVEMENT_CODE + " text not null, " +
                BPNQuality.XML_QUALITY_CODE + " text not null, " +
                BPNQuality.XML_QUANTITY + " double not null default (0), " +
                BPNQuality.XML_STATUS + " integer not null default (0), " +
                BPNQuality.XML_CREATED_DATE + " integer, " +
                BPNQuality.XML_CREATED_BY + " text, " +
                BPNQuality.XML_MODIFIED_DATE + " integer, " +
                BPNQuality.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                BPNQuality.XML_BPN_ID + ", " +
                BPNQuality.XML_IMEI + ", " +
                BPNQuality.XML_COMPANY_CODE + ", " +
                BPNQuality.XML_ESTATE + ", " +
                BPNQuality.XML_BPN_DATE + ", " +
                BPNQuality.XML_DIVISION + ", " +
                BPNQuality.XML_GANG + ", " +
                BPNQuality.XML_LOCATION + ", " +
                BPNQuality.XML_TPH + ", " +
                BPNQuality.XML_NIK_HARVESTER + ", " +
                BPNQuality.XML_CROP + ", " +
                BPNQuality.XML_ACHIEVEMENT_CODE + ", " +
                BPNQuality.XML_QUALITY_CODE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BKMHeader.TABLE_NAME +
                "(" +
                BKMHeader.XML_IMEI + " text not null, " +
                BKMHeader.XML_COMPANY_CODE + " text not null, " +
                BKMHeader.XML_ESTATE + " text not null, " +
                BKMHeader.XML_BKM_DATE + " text not null, " +
                BKMHeader.XML_DIVISION + " text not null, " +
                BKMHeader.XML_GANG + " text not null, " +
                BKMHeader.XML_NIK_FOREMAN + " text, " +
                BKMHeader.XML_FOREMAN + " text, " +
                BKMHeader.XML_NIK_CLERK + " text, " +
                BKMHeader.XML_CLERK + " text, " +
                BKMHeader.XML_GPS_KOORDINAT + " text, " +
                BKMHeader.XML_STATUS + " integer default(0), " +
                BKMHeader.XML_CREATED_DATE + " integer, " +
                BKMHeader.XML_CREATED_BY + " text, " +
                BKMHeader.XML_MODIFIED_DATE + " integer, " +
                BKMHeader.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                BKMHeader.XML_IMEI + ", " +
                BKMHeader.XML_COMPANY_CODE + ", " +
                BKMHeader.XML_ESTATE + ", " +
                BKMHeader.XML_BKM_DATE + ", " +
                BKMHeader.XML_DIVISION + ", " +
                BKMHeader.XML_GANG +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BKMLine.TABLE_NAME +
                "(" +
                BKMLine.XML_IMEI + " text not null, " +
                BKMLine.XML_COMPANY_CODE + " text not null, " +
                BKMLine.XML_ESTATE + " text not null, " +
                BKMLine.XML_BKM_DATE + " text not null, " +
                BKMLine.XML_DIVISION + " text not null, " +
                BKMLine.XML_GANG + " text not null, " +
                BKMLine.XML_NIK + " text not null, " +
                BKMLine.XML_NAME + " text, " +
                BKMLine.XML_ABSENT_TYPE + " text, " +
                BKMLine.XML_MANDAYS + " double not null default (0), " +
                BKMLine.XML_UOM + " text, " +
                BKMLine.XML_USE_GERDANG + " integer default(0), " +
                BKMLine.XML_STATUS + " integer default(0), " +
                BKMLine.XML_CREATED_DATE + " integer, " +
                BKMLine.XML_CREATED_BY + " text, " +
                BKMLine.XML_MODIFIED_DATE + " integer, " +
                BKMLine.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                BKMLine.XML_IMEI + ", " +
                BKMLine.XML_COMPANY_CODE + ", " +
                BKMLine.XML_ESTATE + ", " +
                BKMLine.XML_BKM_DATE + ", " +
                BKMLine.XML_DIVISION + ", " +
                BKMLine.XML_GANG + ", " +
                BKMLine.XML_NIK +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BKMOutput.TABLE_NAME +
                "(" +
                BKMOutput.XML_IMEI + " text not null, " +
                BKMOutput.XML_COMPANY_CODE + " text not null, " +
                BKMOutput.XML_ESTATE + " text not null, " +
                BKMOutput.XML_BKM_DATE + " text not null, " +
                BKMOutput.XML_DIVISION + " text not null, " +
                BKMOutput.XML_GANG + " text not null, " +
                BKMOutput.XML_BLOCK + " text not null, " +
                BKMOutput.XML_NIK + " text not null, " +
                BKMOutput.XML_NAME + " text, " +
                BKMOutput.XML_OUTPUT + " double not null default(0), " +
                BKMOutput.XML_UOM + " text, " +
                BKMOutput.XML_STATUS + " integer default(0), " +
                BKMOutput.XML_CREATED_DATE + " integer, " +
                BKMOutput.XML_CREATED_BY + " text, " +
                BKMOutput.XML_MODIFIED_DATE + " integer, " +
                BKMOutput.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                BKMOutput.XML_IMEI + ", " +
                BKMOutput.XML_COMPANY_CODE + ", " +
                BKMOutput.XML_ESTATE + ", " +
                BKMOutput.XML_BKM_DATE + ", " +
                BKMOutput.XML_DIVISION + ", " +
                BKMOutput.XML_GANG + ", " +
                BKMOutput.XML_BLOCK + ", " +
                BKMOutput.XML_NIK +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPBSHeader.TABLE_NAME +
                "(" +
                SPBSHeader.XML_IMEI + " text not null, " +
                SPBSHeader.XML_YEAR + " text not null, " +
                SPBSHeader.XML_COMPANY_CODE + "  text not null, " +
                SPBSHeader.XML_ESTATE + " text not null, " +
                SPBSHeader.XML_CROP + " text not null, " +
                SPBSHeader.XML_SPBS_NUMBER + " text not null, " +
                SPBSHeader.XML_SPBS_DATE + " text not null, " +
                SPBSHeader.XML_DIVISION + " text, " +
                SPBSHeader.XML_DEST_ID + " text, " +
                SPBSHeader.XML_DEST_DESC + " text, " +
                SPBSHeader.XML_DEST_TYPE + " text, " +
                SPBSHeader.XML_NIK_ASSISTANT + " text, " +
                SPBSHeader.XML_ASSISTANT + " text, " +
                SPBSHeader.XML_NIK_CLERK + " text, " +
                SPBSHeader.XML_CLERK + " text, " +
                SPBSHeader.XML_NIK_DRIVER + " text, " +
                SPBSHeader.XML_DRIVER + " text, " +
                SPBSHeader.XML_NIK_KERNET + " text, " +
                SPBSHeader.XML_KERNET + " text, " +
                SPBSHeader.XML_LICENSE_PLATE + " text, " +
                SPBSHeader.XML_RUNNING_ACCOUNT + " text, " +
                SPBSHeader.XML_GPS_KOORDINAT + " text, " +
                SPBSHeader.XML_IS_SAVE + " integer, " +
                SPBSHeader.XML_STATUS + " integer, " +
                SPBSHeader.XML_CREATED_DATE + " integer, " +
                SPBSHeader.XML_CREATED_BY + " text, " +
                SPBSHeader.XML_MODIFIED_DATE + " integer, " +
                SPBSHeader.XML_MODIFIED_BY + " text, " +
                SPBSHeader.XML_LIFNR + " text, " +
                SPBSHeader.XML_SEND_STATUS + " integer, " +
                SPBSHeader.XML_PRINT_STATUS + " integer, " +
                "primary key (" +
                SPBSHeader.XML_IMEI + ", " +
                SPBSHeader.XML_YEAR + ", " +
                SPBSHeader.XML_COMPANY_CODE + ", " +
                SPBSHeader.XML_ESTATE + ", " +
                SPBSHeader.XML_CROP + ", " +
                SPBSHeader.XML_SPBS_NUMBER +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPBSLine.TABLE_NAME +
                "(" +
                SPBSLine.XML_ID + " integer not null, " +
                SPBSLine.XML_IMEI + " text not null, " +
                SPBSLine.XML_YEAR + " text not null, " +
                SPBSLine.XML_COMPANY_CODE + " text not null, " +
                SPBSLine.XML_ESTATE + " text not null, " +
                SPBSLine.XML_CROP + " text not null, " +
                SPBSLine.XML_SPBS_NUMBER + " text not null, " +
                SPBSLine.XML_SPBS_DATE + " text not null, " +
                SPBSLine.XML_BLOCK + " text not null, " +
                SPBSLine.XML_TPH + " text not null, " +
                SPBSLine.XML_BPN_DATE + " text not null, " +
                SPBSLine.XML_ACHIEVEMENT_CODE + " text not null, " +
                SPBSLine.XML_QUANTITY + " double, " +
                SPBSLine.XML_TOTAL_HARVESTER + " integer, " +
                SPBSLine.XML_UOM + " text, " +
                SPBSLine.XML_GPS_KOORDINAT + " text, " +
                SPBSLine.XML_IS_SAVE + " integer, " +
                SPBSLine.XML_STATUS + " integer, " +
                SPBSLine.XML_BPN_ID + " text, " +
                SPBSLine.XML_SPBS_REF + " text, " +
                SPBSLine.XML_SPBS_NEXT + " text, " +
                SPBSLine.XML_CREATED_DATE + " integer, " +
                SPBSLine.XML_CREATED_BY + " text, " +
                SPBSLine.XML_MODIFIED_DATE + " integer, " +
                SPBSLine.XML_MODIFIED_BY + " text, " +
                SPBSLine.XML_QUANTITY_ANGKUT + " double, " +
                SPBSLine.XML_QUANTITY_REMAINING + " double not null default (0), " +
                "primary key (" +
                SPBSLine.XML_ID + ", " +
                SPBSLine.XML_IMEI + ", " +
                SPBSLine.XML_YEAR + ", " +
                SPBSLine.XML_COMPANY_CODE + ", " +
                SPBSLine.XML_ESTATE + ", " +
                SPBSLine.XML_CROP + ", " +
                SPBSLine.XML_SPBS_NUMBER + ", " +
                SPBSLine.XML_SPBS_DATE + ", " +
                SPBSLine.XML_BLOCK + ", " +
                SPBSLine.XML_TPH + ", " +
                SPBSLine.XML_BPN_DATE + ", " +
                SPBSLine.XML_ACHIEVEMENT_CODE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPBSRunningNumber.TABLE_NAME +
                "(" +
                SPBSRunningNumber.XML_ID + " integer not null, " +
                SPBSRunningNumber.XML_ESTATE + " text not null, " +
                SPBSRunningNumber.XML_DIVISION + " text not null, " +
                SPBSRunningNumber.XML_YEAR + " text not null, " +
                SPBSRunningNumber.XML_MONTH + " text not null, " +
                SPBSRunningNumber.XML_IMEI + " text not null, " +
                SPBSRunningNumber.XML_RUNNING_NUMBER + " integer default (1), " +
                SPBSRunningNumber.XML_DEVICE_ALIAS + " text, " +
                "primary key (" +
                SPBSRunningNumber.XML_ID + ", " +
                SPBSRunningNumber.XML_ESTATE + ", " +
                SPBSRunningNumber.XML_DIVISION + ", " +
                SPBSRunningNumber.XML_YEAR + ", " +
                SPBSRunningNumber.XML_MONTH + ", " +
                SPBSRunningNumber.XML_IMEI +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + Bluetooth.TABLE_NAME +
                "(" +
                Bluetooth.XML_NAME + " text, " +
                Bluetooth.XML_ADDRESS + " text not null, " +
                Bluetooth.XML_IS_PAIRED + " integer default (0), " +
                Bluetooth.XML_IS_SELECTED + " integer default (0)," +
                "primary key (" +
                Bluetooth.XML_ADDRESS +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + RunningAccount.TABLE_NAME +
                "(" +
                RunningAccount.XML_COMPANY_CODE + " text not null, " +
                RunningAccount.XML_ESTATE + " text not null, " +
                RunningAccount.XML_RUNNING_ACCOUNT + " text not null, " +
                RunningAccount.XML_LICENSE_PLATE + " text, " +
                RunningAccount.XML_LIFNR + " text, " +
                RunningAccount.XML_OWNERSHIPFLAG + " text, " +
                "primary key (" +
                RunningAccount.XML_COMPANY_CODE + ", " +
                RunningAccount.XML_ESTATE + ", " +
                RunningAccount.XML_RUNNING_ACCOUNT +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + TaksasiHeader.TABLE_NAME +
                "(" +
                TaksasiHeader.XML_IMEI + " text not null, " +
                TaksasiHeader.XML_COMPANY_CODE + " text not null, " +
                TaksasiHeader.XML_ESTATE + " text not null, " +
                TaksasiHeader.XML_DIVISION + " text not null, " +
                TaksasiHeader.XML_TAKSASI_DATE + " text not null, " +
                TaksasiHeader.XML_BLOCK + " text not null, " +
                TaksasiHeader.XML_CROP + " text not null, " +
                TaksasiHeader.XML_NIK_FOREMAN + " text, " +
                TaksasiHeader.XML_FOREMAN + " text, " +
                TaksasiHeader.XML_PROD_TREES + " double, " +
                TaksasiHeader.XML_BJR + " double, " +
                TaksasiHeader.XML_GPS_KOORDINAT + " text, " +
                TaksasiHeader.XML_IS_SAVE + " integer, " +
                TaksasiHeader.XML_STATUS + " integer, " +
                TaksasiHeader.XML_CREATED_DATE + " integer, " +
                TaksasiHeader.XML_CREATED_BY + " text, " +
                TaksasiHeader.XML_MODIFIED_DATE + " integer, " +
                TaksasiHeader.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                TaksasiHeader.XML_IMEI + ", " +
                TaksasiHeader.XML_COMPANY_CODE + ", " +
                TaksasiHeader.XML_ESTATE + ", " +
                TaksasiHeader.XML_DIVISION + ", " +
                TaksasiHeader.XML_TAKSASI_DATE + ", " +
                TaksasiHeader.XML_BLOCK + ", " +
                TaksasiHeader.XML_CROP +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + TaksasiLine.TABLE_NAME +
                "(" +
                TaksasiLine.XML_IMEI + " text not null, " +
                TaksasiLine.XML_COMPANY_CODE + " text not null, " +
                TaksasiLine.XML_ESTATE + " text not null, " +
                TaksasiLine.XML_DIVISION + " text not null, " +
                TaksasiLine.XML_TAKSASI_DATE + " text not null, " +
                TaksasiLine.XML_BLOCK + " text not null, " +
                TaksasiLine.XML_CROP + " text not null, " +
                TaksasiLine.XML_BARIS_SKB + " integer not null, " +
                TaksasiLine.XML_BARIS_BLOCK + " integer not null, " +
                TaksasiLine.XML_LINE_SKB + " integer not null, " +
                TaksasiLine.XML_QTY_POKOK + " double, " +
                TaksasiLine.XML_QTY_JANJANG + " integer, " +
                TaksasiLine.XML_GPS_KOORDINAT + " text, " +
                TaksasiLine.XML_IS_SAVE + " integer, " +
                TaksasiLine.XML_STATUS + " integer, " +
                TaksasiLine.XML_CREATED_DATE + " integer, " +
                TaksasiLine.XML_CREATED_BY + " text, " +
                TaksasiLine.XML_MODIFIED_DATE + " integer, " +
                TaksasiLine.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                TaksasiLine.XML_IMEI + ", " +
                TaksasiLine.XML_COMPANY_CODE + ", " +
                TaksasiLine.XML_ESTATE + ", " +
                TaksasiLine.XML_DIVISION + ", " +
                TaksasiLine.XML_TAKSASI_DATE + ", " +
                TaksasiLine.XML_BLOCK + ", " +
                TaksasiLine.XML_CROP + ", " +
                TaksasiLine.XML_BARIS_SKB + ", " +
                TaksasiLine.XML_BARIS_BLOCK + ", " +
                TaksasiLine.XML_LINE_SKB +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + Penalty.TABLE_NAME +
                "(" +
                Penalty.XML_PENALTY_CODE + " text not null, " +
                Penalty.XML_PENALTY_DESC + " text, " +
                Penalty.XML_UOM + " text, " +
                Penalty.XMl_CROP_TYPE + " text, " +
                Penalty.XML_IS_LOADING + " integer, " +
                Penalty.XML_IS_ANCAK + " integer, " +
                "primary key (" +
                Penalty.XML_PENALTY_CODE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + DeviceAlias.TABLE_NAME +
                "(" +
                DeviceAlias.XML_DEVICE_ALIAS + " text not null " +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BlockPlanning.TABLE_NAME +
                "(" +
                BlockPlanning.XML_BLOCK + " text not null, " +
                BlockPlanning.XML_CREATED_DATE + " text not null, " +
                BlockPlanning.XML_CREATED_BY + " text not null, " +
                "primary key (" +
                BlockPlanning.XML_BLOCK + ", " +
                BlockPlanning.XML_CREATED_DATE + ", " +
                BlockPlanning.XML_CREATED_BY +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + AncakPanenHeader.TABLE_NAME +
                "(" +
                AncakPanenHeader.XML_ANCAK_PANEN_ID + " text not null, " +
                AncakPanenHeader.XML_IMEI + " text not null, " +
                AncakPanenHeader.XML_COMPANY_CODE + " text not null, " +
                AncakPanenHeader.XML_ESTATE + " text not null, " +
                AncakPanenHeader.XML_ANCAK_DATE + " text not null, " +
                AncakPanenHeader.XML_DIVISION + " text not null, " +
                AncakPanenHeader.XML_GANG + " text not null, " +
                AncakPanenHeader.XML_LOCATION + " text not null, " +
                AncakPanenHeader.XML_TPH + " text not null, " +
                AncakPanenHeader.XML_NIK_HARVESTER + " text not null, " +
                AncakPanenHeader.XML_HARVESTER + " text not null, " +
                AncakPanenHeader.XML_NIK_FOREMAN + " text not null, " +
                AncakPanenHeader.XML_FOREMAN + " text not null, " +
                AncakPanenHeader.XML_NIK_CLERK + " text not null, " +
                AncakPanenHeader.XML_CLERK + " text not null, " +
                AncakPanenHeader.XML_CROP + " text not null, " +
                AncakPanenHeader.XML_GPS_KOORDINAT + " text not null, " +
                AncakPanenHeader.XML_STATUS + " integer not null default (0), " +
                AncakPanenHeader.XML_CREATED_DATE + " integer, " +
                AncakPanenHeader.XML_CREATED_BY + " text, " +
                AncakPanenHeader.XML_MODIFIED_DATE + " integer, " +
                AncakPanenHeader.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                AncakPanenHeader.XML_ANCAK_PANEN_ID +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + AncakPanenQuality.TABLE_NAME +
                "(" +
                AncakPanenQuality.XML_ANCAK_PANEN_ID + " text not null, " +
                AncakPanenQuality.XML_IMEI + " text not null, " +
                AncakPanenQuality.XML_COMPANY_CODE + " text not null, " +
                AncakPanenQuality.XML_ESTATE + " text not null, " +
                AncakPanenQuality.XML_ANCAK_DATE + " text not null, " +
                AncakPanenQuality.XML_DIVISION + " text not null, " +
                AncakPanenQuality.XML_GANG + " text not null, " +
                AncakPanenQuality.XML_LOCATION + " text not null, " +
                AncakPanenQuality.XML_TPH + " text not null, " +
                AncakPanenQuality.XML_NIK_HARVESTER + " text not null, " +
                AncakPanenQuality.XML_CROP + " text not null, " +
                AncakPanenQuality.XML_ACHIEVEMENT_CODE + " text not null, " +
                AncakPanenQuality.XML_QUALITY_CODE + " text not null, " +
                AncakPanenQuality.XML_QUANTITY + " double not null default (0), " +
                AncakPanenQuality.XML_STATUS + " integer not null default (0), " +
                AncakPanenQuality.XML_CREATED_DATE + " integer, " +
                AncakPanenQuality.XML_CREATED_BY + " text, " +
                AncakPanenQuality.XML_MODIFIED_DATE + " integer, " +
                AncakPanenQuality.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                AncakPanenQuality.XML_ANCAK_PANEN_ID + ", " +
                AncakPanenQuality.XML_IMEI + ", " +
                AncakPanenQuality.XML_COMPANY_CODE + ", " +
                AncakPanenQuality.XML_ESTATE + ", " +
                AncakPanenQuality.XML_ANCAK_DATE + ", " +
                AncakPanenQuality.XML_DIVISION + ", " +
                AncakPanenQuality.XML_GANG + ", " +
                AncakPanenQuality.XML_LOCATION + ", " +
                AncakPanenQuality.XML_TPH + ", " +
                AncakPanenQuality.XML_NIK_HARVESTER + ", " +
                AncakPanenQuality.XML_CROP + ", " +
                AncakPanenQuality.XML_ACHIEVEMENT_CODE + ", " +
                AncakPanenQuality.XML_QUALITY_CODE +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPBSDestination.TABLE_NAME +
                "(" +
                SPBSDestination.XML_MANDT + " text not null, " +
                SPBSDestination.XML_ESTATE + " text not null, " +
                SPBSDestination.XML_DEST_TYPE + " text not null, " +
                SPBSDestination.XML_DEST_ID + " text not null, " +
                SPBSDestination.XML_DEST_DESC + " text, " +
                SPBSDestination.XML_ACTIVE + " integer, " +
                "primary key (" +
                SPBSDestination.XML_MANDT + ", " +
                SPBSDestination.XML_ESTATE + ", " +
                SPBSDestination.XML_DEST_TYPE + ", " +
                SPBSDestination.XML_DEST_ID +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPTARunningNumber.TABLE_NAME +
                "(" +
                SPTARunningNumber.XML_ID + " integer not null, " +
                SPTARunningNumber.XML_ESTATE + " text not null, " +
                SPTARunningNumber.XML_DIVISION + " text not null, " +
                SPTARunningNumber.XML_YEAR + " text not null, " +
                SPTARunningNumber.XML_MONTH + " text not null, " +
                SPTARunningNumber.XML_IMEI + " text not null, " +
                SPTARunningNumber.XML_RUNNING_NUMBER + " integer default (1), " +
                SPTARunningNumber.XML_DEVICE_ALIAS + " text, " +
                "primary key (" +
                SPTARunningNumber.XML_ID + ", " +
                SPTARunningNumber.XML_ESTATE + ", " +
                SPTARunningNumber.XML_DIVISION + ", " +
                SPTARunningNumber.XML_YEAR + ", " +
                SPTARunningNumber.XML_MONTH + ", " +
                SPTARunningNumber.XML_IMEI +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + GroupHead.TABLE_NAME +
                "(" +
                GroupHead.XML_COMPANY_CODE + " text not null, " +
                GroupHead.XML_ESTATE + " text not null, " +
                GroupHead.XML_LIFNR + " text not null, " +
                GroupHead.XML_INITIAL + " text not null, " +
                GroupHead.XML_VALID_FROM + " text, " +
                GroupHead.XML_VALID_TO + " text, " +
                GroupHead.XML_NAME + " text not null, " +
                GroupHead.XML_STATUS + " integer, " +
                "primary key (" +
                GroupHead.XML_COMPANY_CODE + ", " +
                GroupHead.XML_ESTATE + ", " +
                GroupHead.XML_LIFNR + ", " +
                GroupHead.XML_INITIAL + ", " +
                GroupHead.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + BLKSUGC.TABLE_NAME +
                "(" +
                BLKSUGC.XML_COMPANY_CODE + " text not null, " +
                BLKSUGC.XML_ESTATE + " text not null, " +
                BLKSUGC.XML_BLOCK + " text not null, " +
                BLKSUGC.XML_VALID_FROM + " text not null, " +
                BLKSUGC.XML_VALID_TO + " text, " +
                BLKSUGC.XML_PHASE + " text, " +
                BLKSUGC.XML_DISTANCE + " double, " +
                BLKSUGC.XML_SUB_DIVISION + " text, " +
                "primary key (" +
                BLKSUGC.XML_COMPANY_CODE + ", " +
                BLKSUGC.XML_ESTATE + ", " +
                BLKSUGC.XML_BLOCK + ", " +
                BLKSUGC.XML_VALID_FROM +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + Vendor.TABLE_NAME +
                "(" +
                Vendor.XML_ESTATE + " text not null, " +
                Vendor.XML_LIFNR + " text not null, " +
                Vendor.XML_NAME + " text, " +
                "primary key (" +
                Vendor.XML_ESTATE + ", " +
                Vendor.XML_LIFNR +
                ")" +
                ")";
        sqliteDatabase.execSQL(query);

        query = "create table " + SPTA.TABLE_NAME +
                "(" +
                SPTA.XML_ZYEAR + " text not null, " +
                SPTA.XML_IMEI + " text not null, " +
                SPTA.XML_SPTA_NUM + " text not null, " +
                SPTA.XML_COMPANY_CODE + " text not null, " +
                SPTA.XML_ESTATE + " text not null, " +
                SPTA.XML_DIVISI + " text, " +
                SPTA.XML_SPTA_DATE + " text, " +
                SPTA.XML_SUB_DIV + " text, " +
                SPTA.XML_PETAK_ID + " text, " +
                SPTA.XML_VENDOR_ID + " text, " +
                SPTA.XML_NOPOL + " text, " +
                SPTA.XML_LOGO + " text, " +
                SPTA.XML_JARAK + " double default (0), " +
                SPTA.XML_RUN_ACC_1 + " text, " +
                SPTA.XML_EMPL_ID_1 + " text, " +
                SPTA.XML_RUN_ACC_2 + " text, " +
                SPTA.XML_EMPL_ID_2 + " text, " +
                SPTA.XML_CHOPPED_DATE + " text, " +
                SPTA.XML_CHOPPED_HOUR + " text, " +
                SPTA.XML_BURN_DATE + " text, " +
                SPTA.XML_BURN_HOUR + " text, " +
                SPTA.XML_LOAD_DATE + " text, " +
                SPTA.XML_LOAD_HOUR + " text, " +
                SPTA.XML_QUALITY + " text, " +
                SPTA.XML_CANE_TYPE + " text, " +
                SPTA.XML_INSENTIVE_GULMA + " integer default (0), " +
                SPTA.XML_INSENTIVE_LANGSIR + " integer default (0), " +
                SPTA.XML_INSENTIVE_ROBOH + " integer default (0), " +
                SPTA.XML_COST_TEBANG + " integer default (0), " +
                SPTA.XML_COST_MUAT + " integer default (0), " +
                SPTA.XML_COST_ANGKUT + " integer default (0), " +
                SPTA.XML_PENALTY_TRASH + " integer default (0), " +
                SPTA.XML_GPS_KOORDINAT + " text not null, " +
                SPTA.XML_IS_SAVE + " integer not null default (0), " +
                SPTA.XML_STATUS + " integer not null default (0), " +
                SPTA.XML_CREATED_DATE + " integer, " +
                SPTA.XML_CREATED_BY + " text, " +
                SPTA.XML_MODIFIED_DATE + " integer, " +
                SPTA.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                SPTA.XML_ZYEAR + ", " +
                SPTA.XML_SPTA_NUM + ", " +
                SPTA.XML_COMPANY_CODE + ", " +
                SPTA.XML_ESTATE +
                ")" +
                ")";

        sqliteDatabase.execSQL(query);

        query = "create table " + BLKRISET.TABLE_NAME +
                "(" +
                BLKRISET.XML_COMPANY_CODE + " text not null, " +
                BLKRISET.XML_ESTATE + " text not null, " +
                BLKRISET.XML_BLOCK + " text not null, " +
                BLKRISET.XML_STATUS + " text, " +
                BLKRISET.XML_START_DATE + " text not null, " +
                BLKRISET.XML_START_DATE_LONG + " long, " +
                BLKRISET.XML_END_DATE + " text, " +
                BLKRISET.XML_END_DATE_LONG + " long, " +
                BLKRISET.XML_DESCRIPTION + " text, " +
                BLKRISET.XML_REF_DOC + " text, " +
                BLKRISET.XML_REF_DOC_DATE + " text, " +
                BLKRISET.XML_MANDT + " text not null, " +
                BLKRISET.XML_CREATED_DATE + " text, " +
                //BLKRISET.XML_CREATED_BY + " text, " +
                BLKRISET.XML_MODIFIED_DATE + " text, " +
                BLKRISET.XML_MODIFIED_BY + " text, " +
                "primary key (" +
                BLKRISET.XML_COMPANY_CODE + ", " +
                BLKRISET.XML_ESTATE + ", " +
                BLKRISET.XML_BLOCK + ", " +
                BLKRISET.XML_START_DATE + ", " +
                BLKRISET.XML_MANDT +
                ")" +
                ")";

        sqliteDatabase.execSQL(query);

        query = "create table " + ConfigApp.TABLE_NAME +
                "(" +
                ConfigApp.XML_COMPANY_CODE + " text not null, " +
                ConfigApp.XML_COMPANY_NAME + " text, " +
                ConfigApp.XML_ESTATE + " text not null, " +
                ConfigApp.XML_ESTATE_NAME + " text, " +
                "primary key (" +
                ConfigApp.XML_COMPANY_CODE + ", " +
                ConfigApp.XML_ESTATE + ")" +
                ")";

        sqliteDatabase.execSQL(query);

        query = "create table " + SPBSWB.XML.TABLE_NAME +
                "(" +
                SPBSWB.XML.WB_NUMBER + " text not null, " +
                SPBSWB.XML.SPBS_NUMBER + " text, " +
                SPBSWB.XML.DRIVER + " text, " +
                SPBSWB.XML.LICENSE_PLATE + " text, " +
                SPBSWB.XML.NETTO + " text, " +
                SPBSWB.XML.WB_DATE + " text, " +
                SPBSWB.XML.TRANSACTION_TYPE + " text, " +
                SPBSWB.XML.TIME_IN + " text, " +
                SPBSWB.XML.TIME_OUT + " text, " +
                SPBSWB.XML.DEDUCTION + " text " +
                ")";
        sqliteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, int versionOld, int versionNew) {
        String query;

        try {
            switch (versionOld) {
                case 131:
                    sqliteDatabase.execSQL("alter table " + Penalty.TABLE_NAME + " add column " + Penalty.XMl_CROP_TYPE + " text");
                    sqliteDatabase.execSQL("alter table " + Penalty.TABLE_NAME + " add column " + Penalty.XML_IS_LOADING + " integer");
                case 132:
                    sqliteDatabase.execSQL("alter table " + SPBSLine.TABLE_NAME + " add column " + SPBSLine.XML_QUANTITY_ANGKUT + " double");
                case 133:
                    sqliteDatabase.execSQL("alter table " + SPBSRunningNumber.TABLE_NAME + " add column " + SPBSRunningNumber.XML_DEVICE_ALIAS + " text");

                    query = "create table " + DeviceAlias.TABLE_NAME +
                            "(" +
                            DeviceAlias.XML_DEVICE_ALIAS + " text not null " +
                            ")";
                    sqliteDatabase.execSQL(query);
                case 134:
                    query = "create table " + BlockPlanning.TABLE_NAME +
                            "(" +
                            BlockPlanning.XML_BLOCK + " text not null, " +
                            BlockPlanning.XML_CREATED_DATE + " text not null, " +
                            BlockPlanning.XML_CREATED_BY + " text not null, " +
                            "primary key (" +
                            BlockPlanning.XML_BLOCK + ", " +
                            BlockPlanning.XML_CREATED_DATE + ", " +
                            BlockPlanning.XML_CREATED_BY +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);
                case 135:
                    sqliteDatabase.execSQL("alter table " + Penalty.TABLE_NAME + " add column " + Penalty.XML_IS_ANCAK + " integer");
                case 136:
                    query = "create table " + AncakPanenHeader.TABLE_NAME +
                            "(" +
                            AncakPanenHeader.XML_ANCAK_PANEN_ID + " text not null, " +
                            AncakPanenHeader.XML_IMEI + " text not null, " +
                            AncakPanenHeader.XML_COMPANY_CODE + " text not null, " +
                            AncakPanenHeader.XML_ESTATE + " text not null, " +
                            AncakPanenHeader.XML_ANCAK_DATE + " text not null, " +
                            AncakPanenHeader.XML_DIVISION + " text not null, " +
                            AncakPanenHeader.XML_GANG + " text not null, " +
                            AncakPanenHeader.XML_LOCATION + " text not null, " +
                            AncakPanenHeader.XML_TPH + " text not null, " +
                            AncakPanenHeader.XML_NIK_HARVESTER + " text not null, " +
                            AncakPanenHeader.XML_HARVESTER + " text not null, " +
                            AncakPanenHeader.XML_NIK_FOREMAN + " text not null, " +
                            AncakPanenHeader.XML_FOREMAN + " text not null, " +
                            AncakPanenHeader.XML_NIK_CLERK + " text not null, " +
                            AncakPanenHeader.XML_CLERK + " text not null, " +
                            AncakPanenHeader.XML_CROP + " text not null, " +
                            AncakPanenHeader.XML_GPS_KOORDINAT + " text not null, " +
                            AncakPanenHeader.XML_STATUS + " integer not null default (0), " +
                            AncakPanenHeader.XML_CREATED_DATE + " integer, " +
                            AncakPanenHeader.XML_CREATED_BY + " text, " +
                            AncakPanenHeader.XML_MODIFIED_DATE + " integer, " +
                            AncakPanenHeader.XML_MODIFIED_BY + " text, " +
                            "primary key (" +
                            AncakPanenHeader.XML_ANCAK_PANEN_ID +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);

                    query = "create table " + AncakPanenQuality.TABLE_NAME +
                            "(" +
                            AncakPanenQuality.XML_ANCAK_PANEN_ID + " text not null, " +
                            AncakPanenQuality.XML_IMEI + " text not null, " +
                            AncakPanenQuality.XML_COMPANY_CODE + " text not null, " +
                            AncakPanenQuality.XML_ESTATE + " text not null, " +
                            AncakPanenQuality.XML_ANCAK_DATE + " text not null, " +
                            AncakPanenQuality.XML_DIVISION + " text not null, " +
                            AncakPanenQuality.XML_GANG + " text not null, " +
                            AncakPanenQuality.XML_LOCATION + " text not null, " +
                            AncakPanenQuality.XML_TPH + " text not null, " +
                            AncakPanenQuality.XML_NIK_HARVESTER + " text not null, " +
                            AncakPanenQuality.XML_CROP + " text not null, " +
                            AncakPanenQuality.XML_ACHIEVEMENT_CODE + " text not null, " +
                            AncakPanenQuality.XML_QUALITY_CODE + " text not null, " +
                            AncakPanenQuality.XML_QUANTITY + " double not null default (0), " +
                            AncakPanenQuality.XML_STATUS + " integer not null default (0), " +
                            AncakPanenQuality.XML_CREATED_DATE + " integer, " +
                            AncakPanenQuality.XML_CREATED_BY + " text, " +
                            AncakPanenQuality.XML_MODIFIED_DATE + " integer, " +
                            AncakPanenQuality.XML_MODIFIED_BY + " text, " +
                            "primary key (" +
                            AncakPanenQuality.XML_ANCAK_PANEN_ID + ", " +
                            AncakPanenQuality.XML_IMEI + ", " +
                            AncakPanenQuality.XML_COMPANY_CODE + ", " +
                            AncakPanenQuality.XML_ESTATE + ", " +
                            AncakPanenQuality.XML_ANCAK_DATE + ", " +
                            AncakPanenQuality.XML_DIVISION + ", " +
                            AncakPanenQuality.XML_GANG + ", " +
                            AncakPanenQuality.XML_LOCATION + ", " +
                            AncakPanenQuality.XML_TPH + ", " +
                            AncakPanenQuality.XML_NIK_HARVESTER + ", " +
                            AncakPanenQuality.XML_CROP + ", " +
                            AncakPanenQuality.XML_ACHIEVEMENT_CODE + ", " +
                            AncakPanenQuality.XML_QUALITY_CODE +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);
                case 137:
                    query = "create table " + SPBSDestination.TABLE_NAME +
                            "(" +
                            SPBSDestination.XML_MANDT + " text not null, " +
                            SPBSDestination.XML_ESTATE + " text not null, " +
                            SPBSDestination.XML_DEST_TYPE + " text not null, " +
                            SPBSDestination.XML_DEST_ID + " text not null, " +
                            SPBSDestination.XML_DEST_DESC + " text, " +
                            SPBSDestination.XML_ACTIVE + " integer, " +
                            "primary key (" +
                            SPBSDestination.XML_MANDT + ", " +
                            SPBSDestination.XML_ESTATE + ", " +
                            SPBSDestination.XML_DEST_TYPE + ", " +
                            SPBSDestination.XML_DEST_ID +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);

                    sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_DEST_ID + " text");
                    sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_DEST_DESC + " text");
                    sqliteDatabase.execSQL("alter table " + SPBSLine.TABLE_NAME + " add column " + SPBSLine.XML_SPBS_REF + " text");
                case 138:
                    sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_DEST_TYPE + " text");
                case 139:
                    sqliteDatabase.execSQL("alter table " + SPBSLine.TABLE_NAME + " add column " + SPBSLine.XML_SPBS_NEXT + " text");
                case 140:
                    sqliteDatabase.execSQL("alter table " + BKMLine.TABLE_NAME + " add column " + BKMLine.XML_USE_GERDANG + " integer default(0)");
                case 141:
                    query = "create table " + SPTARunningNumber.TABLE_NAME +
                            "(" +
                            SPTARunningNumber.XML_ID + " integer not null, " +
                            SPTARunningNumber.XML_ESTATE + " text not null, " +
                            SPTARunningNumber.XML_DIVISION + " text not null, " +
                            SPTARunningNumber.XML_YEAR + " text not null, " +
                            SPTARunningNumber.XML_MONTH + " text not null, " +
                            SPTARunningNumber.XML_IMEI + " text not null, " +
                            SPTARunningNumber.XML_RUNNING_NUMBER + " integer default (1), " +
                            SPTARunningNumber.XML_DEVICE_ALIAS + " text, " +
                            "primary key (" +
                            SPTARunningNumber.XML_ID + ", " +
                            SPTARunningNumber.XML_ESTATE + ", " +
                            SPTARunningNumber.XML_DIVISION + ", " +
                            SPTARunningNumber.XML_YEAR + ", " +
                            SPTARunningNumber.XML_MONTH + ", " +
                            SPTARunningNumber.XML_IMEI +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);

                    query = "create table " + GroupHead.TABLE_NAME +
                            "(" +
                            GroupHead.XML_COMPANY_CODE + " text not null, " +
                            GroupHead.XML_ESTATE + " text not null, " +
                            GroupHead.XML_LIFNR + " text not null, " +
                            GroupHead.XML_INITIAL + " text not null, " +
                            GroupHead.XML_VALID_FROM + " text, " +
                            GroupHead.XML_VALID_TO + " text, " +
                            GroupHead.XML_NAME + " text not null, " +
                            GroupHead.XML_STATUS + " integer, " +
                            "primary key (" +
                            GroupHead.XML_COMPANY_CODE + ", " +
                            GroupHead.XML_ESTATE + ", " +
                            GroupHead.XML_LIFNR + ", " +
                            GroupHead.XML_INITIAL + ", " +
                            GroupHead.XML_VALID_FROM +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);

                    query = "create table " + BLKSUGC.TABLE_NAME +
                            "(" +
                            BLKSUGC.XML_COMPANY_CODE + " text not null, " +
                            BLKSUGC.XML_ESTATE + " text not null, " +
                            BLKSUGC.XML_BLOCK + " text not null, " +
                            BLKSUGC.XML_VALID_FROM + " text not null, " +
                            BLKSUGC.XML_VALID_TO + " text, " +
                            BLKSUGC.XML_PHASE + " text, " +
                            BLKSUGC.XML_DISTANCE + " double, " +
                            BLKSUGC.XML_SUB_DIVISION + " text, " +
                            "primary key (" +
                            BLKSUGC.XML_COMPANY_CODE + ", " +
                            BLKSUGC.XML_ESTATE + ", " +
                            BLKSUGC.XML_BLOCK + ", " +
                            BLKSUGC.XML_VALID_FROM +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);

                    query = "create table " + Vendor.TABLE_NAME +
                            "(" +
                            Vendor.XML_ESTATE + " text not null, " +
                            Vendor.XML_LIFNR + " text not null, " +
                            Vendor.XML_NAME + " text, " +
                            "primary key (" +
                            Vendor.XML_ESTATE + ", " +
                            Vendor.XML_LIFNR +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);

                    query = "create table " + SPTA.TABLE_NAME +
                            "(" +
                            SPTA.XML_ZYEAR + " text not null, " +
                            SPTA.XML_IMEI + " text not null, " +
                            SPTA.XML_SPTA_NUM + " text not null, " +
                            SPTA.XML_COMPANY_CODE + " text not null, " +
                            SPTA.XML_ESTATE + " text not null, " +
                            SPTA.XML_DIVISI + " text, " +
                            SPTA.XML_SPTA_DATE + " text, " +
                            SPTA.XML_SUB_DIV + " text, " +
                            SPTA.XML_PETAK_ID + " text, " +
                            SPTA.XML_VENDOR_ID + " text, " +
                            SPTA.XML_NOPOL + " text, " +
                            SPTA.XML_LOGO + " text, " +
                            SPTA.XML_JARAK + " double default (0), " +
                            SPTA.XML_RUN_ACC_1 + " text, " +
                            SPTA.XML_EMPL_ID_1 + " text, " +
                            SPTA.XML_RUN_ACC_2 + " text, " +
                            SPTA.XML_EMPL_ID_2 + " text, " +
                            SPTA.XML_CHOPPED_DATE + " text, " +
                            SPTA.XML_CHOPPED_HOUR + " text, " +
                            SPTA.XML_BURN_DATE + " text, " +
                            SPTA.XML_BURN_HOUR + " text, " +
                            SPTA.XML_LOAD_DATE + " text, " +
                            SPTA.XML_LOAD_HOUR + " text, " +
                            SPTA.XML_QUALITY + " text, " +
                            SPTA.XML_CANE_TYPE + " text, " +
                            SPTA.XML_INSENTIVE_GULMA + " integer default (0), " +
                            SPTA.XML_INSENTIVE_LANGSIR + " integer default (0), " +
                            SPTA.XML_INSENTIVE_ROBOH + " integer default (0), " +
                            SPTA.XML_COST_TEBANG + " integer default (0), " +
                            SPTA.XML_COST_MUAT + " integer default (0), " +
                            SPTA.XML_COST_ANGKUT + " integer default (0), " +
                            SPTA.XML_PENALTY_TRASH + " integer default (0), " +
                            SPTA.XML_GPS_KOORDINAT + " text not null, " +
                            SPTA.XML_IS_SAVE + " integer not null default (0), " +
                            SPTA.XML_STATUS + " integer not null default (0), " +
                            SPTA.XML_CREATED_DATE + " integer, " +
                            SPTA.XML_CREATED_BY + " text, " +
                            SPTA.XML_MODIFIED_DATE + " integer, " +
                            SPTA.XML_MODIFIED_BY + " text, " +
                            "primary key (" +
                            SPTA.XML_ZYEAR + ", " +
                            SPTA.XML_SPTA_NUM + ", " +
                            SPTA.XML_COMPANY_CODE + ", " +
                            SPTA.XML_ESTATE +
                            ")" +
                            ")";

                    sqliteDatabase.execSQL(query);
                case 142:
                    sqliteDatabase.execSQL("alter table " + RunningAccount.TABLE_NAME + " add column " + RunningAccount.XML_LIFNR + " text ");
                    sqliteDatabase.execSQL("alter table " + RunningAccount.TABLE_NAME + " add column " + RunningAccount.XML_OWNERSHIPFLAG + " text ");
                case 143:
                    sqliteDatabase.execSQL("alter table " + DivisionAssistant.TABLE_NAME + " add column " + DivisionAssistant.XML_LIFNR + " text");
                    sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_LIFNR + " text");
                case 144:
                    sqliteDatabase.execSQL("alter table " + BPNQuantity.TABLE_NAME + " add column " + BPNQuantity.XML_QUANTITY_REMAINING + " double not null default (0)");
                    sqliteDatabase.execSQL("alter table " + SPBSLine.TABLE_NAME + " add column " + SPBSLine.XML_QUANTITY_REMAINING + " double not null default (0)");
                case 145:
                    query = "create table " + BLKRISET.TABLE_NAME +
                            "(" +
                            BLKRISET.XML_COMPANY_CODE + " text not null, " +
                            BLKRISET.XML_ESTATE + " text not null, " +
                            BLKRISET.XML_BLOCK + " text not null, " +
                            BLKRISET.XML_STATUS + " text, " +
                            BLKRISET.XML_START_DATE + " text not null, " +
                            BLKRISET.XML_START_DATE_LONG + " long, " +
                            BLKRISET.XML_END_DATE + " text, " +
                            BLKRISET.XML_END_DATE_LONG + " long, " +
                            BLKRISET.XML_DESCRIPTION + " text, " +
                            BLKRISET.XML_REF_DOC + " text, " +
                            BLKRISET.XML_REF_DOC_DATE + " text, " +
                            BLKRISET.XML_MANDT + " text not null, " +
                            BLKRISET.XML_CREATED_DATE + " text, " +
                            //BLKRISET.XML_CREATED_BY + " text, " +
                            BLKRISET.XML_MODIFIED_DATE + " text, " +
                            BLKRISET.XML_MODIFIED_BY + " text, " +
                            "primary key (" +
                            BLKRISET.XML_COMPANY_CODE + ", " +
                            BLKRISET.XML_ESTATE + ", " +
                            BLKRISET.XML_BLOCK + ", " +
                            BLKRISET.XML_START_DATE + ", " +
                            BLKRISET.XML_MANDT +
                            ")" +
                            ")";
                    sqliteDatabase.execSQL(query);
                case 146:
                    sqliteDatabase.execSQL("alter table " + BPNQuantity.TABLE_NAME + " add column " + BPNQuantity.XML_MODIFIED_DATE_STR + " text");

                case 147:
                    query = "create table " + ConfigApp.TABLE_NAME +
                            "(" +
                            ConfigApp.XML_COMPANY_CODE + " text not null, " +
                            ConfigApp.XML_COMPANY_NAME + " text, " +
                            ConfigApp.XML_ESTATE + " text not null, " +
                            ConfigApp.XML_ESTATE_NAME + " text, " +
                            "primary key (" +
                            ConfigApp.XML_COMPANY_CODE + ", " +
                            ConfigApp.XML_ESTATE + ")" +
                            ")";

                    sqliteDatabase.execSQL(query);
                case 148:
                    sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_SEND_STATUS + " integer default (0)");
                    sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_PRINT_STATUS + " integer default (0)");
                    break;
                case 149:
                    query = "create table " + SPBSWB.XML.TABLE_NAME +
                            "(" +
                            SPBSWB.XML.WB_NUMBER + " text not null, " +
                            SPBSWB.XML.SPBS_NUMBER + " text, " +
                            SPBSWB.XML.DRIVER + " text, " +
                            SPBSWB.XML.LICENSE_PLATE + " text, " +
                            SPBSWB.XML.NETTO + " text, " +
                            SPBSWB.XML.WB_DATE + " text, " +
                            SPBSWB.XML.TRANSACTION_TYPE + " text, " +
                            SPBSWB.XML.TIME_IN + " text, " +
                            SPBSWB.XML.TIME_OUT + " text, " +
                            SPBSWB.XML.DEDUCTION + " text " +
                            ")";
                    sqliteDatabase.execSQL(query);
                    break;
                case 150:
                    if (!isFieldExist(sqliteDatabase, SPBSHeader.TABLE_NAME, SPBSHeader.XML_SEND_STATUS)) {
                        sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_SEND_STATUS + " integer default (0)");
                        sqliteDatabase.execSQL("alter table " + SPBSHeader.TABLE_NAME + " add column " + SPBSHeader.XML_PRINT_STATUS + " integer default (0)");
                    }
                    break;
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public boolean isFieldExist(SQLiteDatabase db, String tableName, String fieldName) {
        boolean isExist = false;
        Cursor res = db.rawQuery("PRAGMA table_info(" + tableName + ")", null);
        res.moveToFirst();
        do {
            String currentColumn = res.getString(1);
            if (currentColumn.equals(fieldName)) {
                isExist = true;
            }
        } while (res.moveToNext());
        return isExist;
    }
}
