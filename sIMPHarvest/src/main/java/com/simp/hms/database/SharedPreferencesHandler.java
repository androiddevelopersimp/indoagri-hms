package com.simp.hms.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferencesHandler {
	private SharedPreferences shared;
	private Editor editor;
	
	public final static String SHARED_NAME = "HMS";
	public final static String SHARED_COUNTRY_CODE = "COUNTRY_CODE";
	public final static String SHARED_QR = "QR";
	
	public SharedPreferencesHandler(Context context){
		shared = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
	}
	
	public void setCountryCode(String countryCode){
		editor = shared.edit();
		editor.putString(SHARED_COUNTRY_CODE, countryCode);
		editor.commit();
	}
	
	public String getCountryCode(){
		return shared.getString(SHARED_COUNTRY_CODE, "");
	}

	public void setQR(String qrType){
		editor = shared.edit();
		editor.putString(SHARED_QR, qrType);
		editor.commit();
	}

	public String getQR(){
		return shared.getString(SHARED_QR,"");
	}
}
