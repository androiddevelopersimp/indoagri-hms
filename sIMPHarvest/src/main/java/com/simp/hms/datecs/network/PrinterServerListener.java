package com.simp.hms.datecs.network;

import java.net.Socket;

public interface PrinterServerListener {
    public void onConnect(Socket socket);
}
