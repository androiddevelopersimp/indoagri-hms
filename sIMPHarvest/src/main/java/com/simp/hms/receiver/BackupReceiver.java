package com.simp.hms.receiver;

import com.simp.hms.service.BackupService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BackupReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent service = new Intent(context, BackupService.class);
		context.startService(service);
	}
}