package com.simp.hms.listener;

import java.util.Date;

public interface DialogDateListener {
	public void onDateOK(Date date, int id);
}
