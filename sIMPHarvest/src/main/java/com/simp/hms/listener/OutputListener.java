package com.simp.hms.listener;

public interface OutputListener {
	public void onOutputChange();
}
