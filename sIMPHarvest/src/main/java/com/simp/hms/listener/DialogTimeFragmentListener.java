package com.simp.hms.listener;

import java.util.Date;

public interface DialogTimeFragmentListener {
	public void onTimeFragmentOK(int hour, int minute, int second, int id);
}
