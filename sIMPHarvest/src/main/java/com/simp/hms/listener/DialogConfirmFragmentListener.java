package com.simp.hms.listener;

public interface DialogConfirmFragmentListener {
	public void onConfirmFragmentOK(int id);
}
