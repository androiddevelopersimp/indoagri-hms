package com.simp.hms.listener;

public interface DialogConfirmListener {
	public void onConfirmOK(Object object, int id);
}
