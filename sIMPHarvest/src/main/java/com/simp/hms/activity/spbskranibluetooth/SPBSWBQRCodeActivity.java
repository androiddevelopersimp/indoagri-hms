package com.simp.hms.activity.spbskranibluetooth;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.AdapterSPBSNumber;
import com.simp.hms.adapter.AdapterSPBSWBRecyclerview;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSWB;

import java.util.ArrayList;
import java.util.List;

public class SPBSWBQRCodeActivity extends AppCompatActivity implements View.OnClickListener, DialogNotificationListener {

    private Toolbar tbrMain;
    private TextView title;
    private RecyclerView rvSPBSWB;

    Button btnSave;
    DatabaseHandler database;

    List<SPBSWB> listSPBS;
    String wbNumber, transactionType, wbDate, timeIn, timeOut, deduction, nettWeight;

    AdapterSPBSWBRecyclerview adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spbswb_qrcode);

        String result = getIntent().getStringExtra("WBRESULT");

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);

        title = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        title.setText("SPBS WB");
        btnActionBarRight.setVisibility(View.INVISIBLE);


        initView();
        if (result != null) {
            parsingHeader(result);
        }
        setView();
    }

    public void initView() {
        btnSave = (Button) findViewById(R.id.btnSaveWB);
        rvSPBSWB = (RecyclerView) findViewById(R.id.rvListwb);

        btnSave.setOnClickListener(this);

        listSPBS = new ArrayList<>();
        database = new DatabaseHandler(SPBSWBQRCodeActivity.this);

        adapter = new AdapterSPBSWBRecyclerview(SPBSWBQRCodeActivity.this, listSPBS);
        LinearLayoutManager layoutManagerListAPK = new LinearLayoutManager(SPBSWBQRCodeActivity.this, RecyclerView.VERTICAL, false);
        rvSPBSWB.setLayoutManager(layoutManagerListAPK);
        rvSPBSWB.setAdapter(adapter);
    }

    public void parsingHeader(String data) {
        String[] splitheader = data.split("\\|");
        for (String dataHeader : splitheader) {
            parsingData(dataHeader);
//            Log.e(ContentValues.TAG, "parsingHeader: "+dataHeader );
        }
    }

    public void parsingData(String data) {
        try {
            String[] splitdata = data.split(";");
            if (splitdata.length > 7) {
                wbNumber = splitdata[0];
                transactionType = splitdata[1];
                wbDate = splitdata[2];
                timeIn = splitdata[3];
                timeOut = splitdata[4];
                nettWeight = splitdata[5];
                deduction = splitdata[6];
                for (int i = 7; i < splitdata.length; i++) {
                    String spbsnumber = splitdata[i];
                    if (spbsnumber != null) {
                        database.openTransaction();
                        SPBSHeader spbsHeader = (SPBSHeader) database.getDataFirst(false,
                                SPBSHeader.TABLE_NAME, null,
                                SPBSHeader.XML_SPBS_NUMBER + "=?", new String[]{spbsnumber},
                                null, null, null, null);
                        database.closeTransaction();
                        if (spbsHeader != null)
                            listSPBS.add(new SPBSWB(wbNumber, spbsnumber, spbsHeader.getDriver(), spbsHeader.getLicensePlate(), nettWeight, wbDate, transactionType, timeIn, timeOut, deduction));
                    }
                }

            } else {
                new DialogNotification(this, getResources().getString(R.string.informasi),
                        "Format Tidak Sesuai", false).show();
            }
        } catch (Exception e) {
            Log.e(ContentValues.TAG, "setView: " + e.getMessage());
        }
    }

    public void setView() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSaveWB:
                if (listSPBS.size() > 0) {
                    saveWB();
                    new DialogNotification(this, getResources().getString(R.string.informasi),
                            "Data Tersimpan", true).show();
                } else {
                    new DialogNotification(this, getResources().getString(R.string.informasi),
                            "Data Kosong", true).show();
                }
                break;
            default:
                break;
        }
    }

    public void saveWB() {
        try {
            database.openTransaction();
            for (SPBSWB spbswb : listSPBS) {
                ContentValues values = new ContentValues();
                values.put(SPBSWB.XML.WB_NUMBER, spbswb.getWbNumber());
                values.put(SPBSWB.XML.TRANSACTION_TYPE, spbswb.getTransactionType());
                values.put(SPBSWB.XML.WB_DATE, spbswb.getWbDate());
                values.put(SPBSWB.XML.TIME_IN, spbswb.getTimeIn());
                values.put(SPBSWB.XML.TIME_OUT, spbswb.getTimeOut());
                values.put(SPBSWB.XML.DEDUCTION, spbswb.getDeduction());
                values.put(SPBSWB.XML.NETTO, spbswb.getNetto());
                values.put(SPBSWB.XML.SPBS_NUMBER, spbswb.getSpbsNumber());
                values.put(SPBSWB.XML.DRIVER, spbswb.getDriver());
                values.put(SPBSWB.XML.LICENSE_PLATE, spbswb.getLicensePlate());

                SPBSWB spbsWBExisting = (SPBSWB) database.getDataFirst(false, SPBSWB.XML.TABLE_NAME, null,
                        SPBSWB.XML.WB_NUMBER + "=? AND " + SPBSWB.XML.SPBS_NUMBER + " =? ", new String[]{spbswb.getWbNumber(), spbswb.getSpbsNumber()}, null, null, null, null);
                if (spbsWBExisting == null) {
                    database.insertDataSQL(SPBSWB.XML.TABLE_NAME, values);
                    updateConfirmStatus(spbswb.getSpbsNumber());
                } else {
                    database.updateDataSQL(SPBSWB.XML.TABLE_NAME, values, SPBSWB.XML.WB_NUMBER + "=? AND " + SPBSWB.XML.SPBS_NUMBER + " =? ", new String[]{spbswb.getWbNumber(), spbswb.getSpbsNumber()});
                }
            }
            database.commitTransaction();
            database.closeTransaction();

            new DialogNotification(this, getResources().getString(R.string.informasi),
                    "Data Tersimpan", true).show();
        } catch (Exception e) {
            Log.e(ContentValues.TAG, "saveWB: " + e.getMessage());
        }
    }


    public void updateConfirmStatus(String spbsNumber) {
        database.updateRawData(SPBSHeader.TABLE_NAME,
                "UPDATE " + SPBSHeader.TABLE_NAME
                        + " SET " + SPBSHeader.XML_SEND_STATUS + " = 3 "
                        + " WHERE " + SPBSHeader.XML_SPBS_NUMBER + " = '" + spbsNumber + "'");
    }

    @Override
    public void onOK(boolean is_finish) {
        if (is_finish) {
            finish();
        }
    }
}
