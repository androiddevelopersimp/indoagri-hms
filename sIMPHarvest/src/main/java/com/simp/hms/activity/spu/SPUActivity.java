package com.simp.hms.activity.spu;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.datecs.api.emsr.EMSR;
import com.datecs.api.printer.Printer;
import com.datecs.api.printer.ProtocolAdapter;
import com.datecs.api.universalreader.UniversalReader;
import com.sewoo.jpos.command.ESCPOS;
import com.sewoo.jpos.command.ESCPOSConst;
import com.sewoo.jpos.printer.ESCPOSPrinter;
import com.sewoo.jpos.printer.LKPrint;
import com.sewoo.port.android.BluetoothPort;
import com.sewoo.request.android.RequestHandler;
import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.bpn.BPNActivity;
import com.simp.hms.activity.master.MasterBlockHdrcActivity;
import com.simp.hms.activity.master.MasterDivisionAssistantActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.master.MasterVehicleActivity;
import com.simp.hms.activity.other.BlockPlanningActivity;
import com.simp.hms.activity.spbs.DriverExternalActivity;
import com.simp.hms.activity.spbs.LicensePlateExternalActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSBlockActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spbs.SPBSKernetActivity;
import com.simp.hms.adapter.AdapterSPBSSummary;
import com.simp.hms.adapter.AdapterSPUSummary;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.database.SharedPreferencesHandler;
import com.simp.hms.datecs.network.PrinterServer;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BLKRISET;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.DeviceAlias;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.model.Employee;
import com.simp.hms.model.NomorFormat;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSPrint;
import com.simp.hms.model.SPBSRunningNumber;
import com.simp.hms.model.SPBSSummary;
import com.simp.hms.model.SPUPrint;
import com.simp.hms.model.SPUSummary;
import com.simp.hms.model.UserLogin;
import com.simp.hms.model.Vendor;
import com.simp.hms.routines.Constanta;
import com.simp.hms.routines.Utils;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;
import com.simp.hms.zebra.DemoSleeper;
import com.simp.hms.zebra.SettingsHelper;
import com.simp.hms.zebra.UIHelper;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class SPUActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener,
        DialogNotificationListener, DialogConfirmListener, DialogDateListener {

    private Toolbar tbrMain;
    private TextView txtSpuNumber;
    private TextView txtSpuDate;
    private TextView txtSpuDivision;
    private TextView txtSpuDriver;
    private Button btnSpuDriverIn;
    private Button btnSpuDriverEx;
    private TextView txtSpuKernet;
    private TextView txtSpuLicensePlate;
    private Button btnSpuLicensePlateIn;
    private Button btnSpuLicensePlateEx;
    private Button btnSpuLoadBpn;
    private ListView lsvSpuSummary;
    private Button btnSpuAddBlock;

    private List<SPUSummary> listSpuSummary = new ArrayList<SPUSummary>();
    private AdapterSPUSummary adapter = null;

    SPUActivity.GetDataAsyncTask getDataAsync;

    DialogProgress dialogProgress;
    GPSService gps;
    GpsHandler gpsHandler;
    private UIHelper helper = new UIHelper(this);
    DatabaseHandler database = new DatabaseHandler(SPUActivity.this);

    BluetoothAdapter bluetoothAdapter = null;
    BluetoothPort bluetoothPort = null;
    BluetoothDevice bluetoothDevice = null;
    Thread hThread;

    String companyCode = "";
    String estate = "";
    String division = "";
    String divisionLogin = "";
    String gang = "";
    String nikClerk = "";
    String clerk = "";
    String alias = "";
    String imei = "";
    String imeiSerial = "000000";
    String year = "";
    String yearTwoDigits = "";
    String crop = "04";
    String spbsNumber = "";
    String spbsNumberTemp = "";
    String spbsDate = "";
    String nikDriver = "";
    String driver = "";
    String nikKernet = "";
    String kernet = "";
    String licensePlate = "";
    String runningAccount = "";
    String nikAssistant = "-";
    String assistant = "";
    String month = "";
    String monthAlphabet = "";
    int id = 1;
    int lastNumber = 0;
    String gpsKoordinat = "0.0:0.0";
    int isSave = 0;
    int status = 0;
    double latitude = 0;
    double longitude = 0;
    long todayDate = 0;
    String divisionRun = "";
    String destId = "";
    String destDesc = "";
    String destType = "";
    String lifnr = "";
    String vendorName = "";

    final int MST_ASS = 101;
    final int MST_BLOCK = 102;
    final int MST_TPH = 103;
    final int MST_DRIVER = 104;
    final int MST_RUN_ACC = 105;
    final int MST_KERNET = 106;
    final int INP_DRIVER_EX = 107;
    final int INP_LICENSE_EX = 108;
    final int MST_DEST = 109;

    final String QR_CODE_DELIMITER = ";";
    final String QR_CODE_HEADER_DELIMITER = "|";
    final String QR_CODE_LINE_DELIMITER = "*";

    boolean isNew = true;

    String qtyGoodQtyCode = BPNQuantity.GOOD_QTY_CODE;
    String qtyGoodWeightCode = BPNQuantity.GOOD_WEIGHT_CODE;

    String qtyBadQtyCode = BPNQuantity.BAD_QTY_CODE;
    String qtyBadWeightCode = BPNQuantity.BAD_WEIGHT_CODE;

    String qtyPoorQtyCode = BPNQuantity.POOR_QTY_CODE;
    String qtyPoorWeightCode = BPNQuantity.POOR_WEIGHT_CODE;

    Connection printerConnection;

    private static final int REQUEST_ENABLE_BT = 3;

    private ProtocolAdapter mProtocolAdapter;
    private ProtocolAdapter.Channel mPrinterChannel;
    private ProtocolAdapter.Channel mUniversalChannel;
    private Printer mPrinter;
    private BluetoothSocket mBtSocket;
    private PrinterServer mPrinterServer;

    private interface PrinterRunnable {
        public void run(ProgressDialog dialog, Printer printer) throws IOException, ParseException;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spu);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        txtSpuNumber = (TextView) findViewById(R.id.txtSpuNumber);
        txtSpuDate = (TextView)findViewById(R.id.txtSpuDate);
        txtSpuDivision = (TextView)findViewById(R.id.txtSpuDivision);
        txtSpuDriver = (TextView)findViewById(R.id.txtSpuDriver);
        btnSpuDriverIn = (Button) findViewById(R.id.btnSpuDriverIn);
        btnSpuDriverEx = (Button) findViewById(R.id.btnSpuDriverEx);
        txtSpuKernet = (TextView) findViewById(R.id.txtSpuKernet);
        txtSpuLicensePlate = (TextView) findViewById(R.id.txtSpuLicensePlate);
        btnSpuLicensePlateIn = (Button) findViewById(R.id.btnSpuLicensePlateIn);
        btnSpuLicensePlateEx = (Button) findViewById(R.id.btnSpuLicensePlateEx);
        btnSpuLoadBpn = (Button) findViewById(R.id.btnSpuLoadBpn);
        lsvSpuSummary = (ListView) findViewById(R.id.lsvSpuSummary);
        btnSpuAddBlock = (Button) findViewById(R.id.btnSpuAddBlock);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);
        TextView btnActionBarPrint = (TextView) tbrMain.findViewById(R.id.btnActionBarPrint);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.spu));
        btnActionBarRight.setText(getResources().getString(R.string.save));
        btnActionBarRight.setOnClickListener(this);
        btnActionBarPrint.setOnClickListener(this);
        btnActionBarPrint.setVisibility(View.VISIBLE);
        txtSpuDivision.setOnClickListener(this);
        btnSpuDriverIn.setOnClickListener(this);
        btnSpuDriverEx.setOnClickListener(this);
        txtSpuKernet.setOnClickListener(this);
        btnSpuLicensePlateIn.setOnClickListener(this);
        btnSpuLicensePlateEx.setOnClickListener(this);
        btnSpuLoadBpn.setOnClickListener(this);
        lsvSpuSummary.setOnItemClickListener(this);
        btnSpuAddBlock.setOnClickListener(this);

        deleteUnSaved();

        SPBSHeader spbsHeader = null;

        gps = new GPSService(SPUActivity.this);
        gpsHandler = new GpsHandler(SPUActivity.this);

        GPSTriggerService.spuActivity = this;

        gpsHandler.startGPS();

        if(getIntent().getExtras() != null){
            spbsHeader = (SPBSHeader) getIntent().getParcelableExtra(SPBSHeader.TABLE_NAME);
        }

        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if(userLogin != null){
            companyCode = userLogin.getCompanyCode();
            estate = userLogin.getEstate();
            divisionLogin = userLogin.getDivision();
            gang = userLogin.getGang();
            nikClerk  = userLogin.getNik();
            clerk = userLogin.getName();
        }

        if(spbsHeader != null) {
            imei = spbsHeader.getImei();
            year = spbsHeader.getYear();
            companyCode = spbsHeader.getCompanyCode();
            estate = spbsHeader.getEstate();
            division = spbsHeader.getDivision();
            spbsNumber = spbsHeader.getSpbsNumber();
            spbsDate = spbsHeader.getSpbsDate();
            crop = spbsHeader.getCrop();
            destId = spbsHeader.getDestId();
            destDesc = spbsHeader.getDestDesc();
            destType = spbsHeader.getDestType();

            database.openTransaction();
            spbsHeader = (SPBSHeader) database.getDataFirst(false, SPBSHeader.TABLE_NAME, null,
                    SPBSHeader.XML_YEAR + "=?" + " and " +
                            SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                            SPBSHeader.XML_CROP + "=?" + " and " +
                            SPBSHeader.XML_SPBS_NUMBER + "=?",
                    new String [] {year, companyCode, estate, crop, spbsNumber},
                    null, null, null, null);
            database.closeTransaction();

            nikClerk  = spbsHeader.getNikClerk();
            clerk = spbsHeader.getClerk();
            nikAssistant = spbsHeader.getNikAssistant();
            nikDriver = spbsHeader.getNikDriver();
            driver = spbsHeader.getDriver();
            nikKernet = spbsHeader.getNikKernet();
            kernet = spbsHeader.getKernet();
            licensePlate = spbsHeader.getLicensePlate();
            runningAccount = spbsHeader.getRunningAccount();
            assistant = spbsHeader.getAssistant();
            status = spbsHeader.getStatus();
            isSave = 1;
            isNew = false;
            alias = spbsHeader.getSpbsNumber().substring(spbsHeader.getSpbsNumber().length() - 6, spbsHeader.getSpbsNumber().length() - 3);
            lifnr = spbsHeader.getLifnr();

            txtSpuNumber.setText(spbsNumber);
        }else{
            imei = new DeviceHandler(SPUActivity.this).getImei();
            year = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_YEAR_ONLY);
            spbsDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
            month = String.valueOf(new Converter(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_MONTH_ONLY)).StrToInt());
            monthAlphabet = new DateLocal(new Date()).getMonthAlphabet();
            todayDate = new Date().getTime();
            spbsNumber = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID);
            isNew = true;

            SPBSRunningNumber spbsNumberMax = getAlias(estate, divisionLogin, year, month, imei);

            if(spbsNumberMax != null){
                alias = spbsNumberMax.getDeviceAlias();
                lastNumber = new Converter(spbsNumberMax.getRunningNumber()).StrToInt() + 1;
                id = spbsNumberMax.getId();
                divisionRun = spbsNumberMax.getDivision();
            }else{
                alias = "";
                lastNumber = 1;
            }

            if(gps.canGetLocation()){
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
            }

            gpsKoordinat = String.valueOf(latitude) + ":" + String.valueOf(longitude);
            isSave = 0;
        }

        adapter = new AdapterSPUSummary(SPUActivity.this, listSpuSummary, R.layout.item_spu_summary);
        lsvSpuSummary.setAdapter(adapter);

        initView();

        getDataAsync = new SPUActivity.GetDataAsyncTask();
        getDataAsync.execute();
    }

    private void initView(){
        txtSpuDate.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtSpuDivision.setText(destDesc);

        String tempDivision = division;
        if(!TextUtils.isEmpty(lifnr))
            tempDivision = division + "," + lifnr;
        txtSpuDivision.setText(tempDivision);

        txtSpuDriver.setText(driver);
        txtSpuKernet.setText(kernet);
        txtSpuLicensePlate.setText(licensePlate);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        ArrayList<String> listBlock = new ArrayList<String>();

        for(int i = 0; i < adapter.getCount(); i++){
            SPUSummary spuSummary = (SPUSummary) adapter.getItem(i);

            listBlock.add(spuSummary.getBlock());
        }

        SPUSummary spuSummary = (SPUSummary) adapter.getItem(pos);

        startActivityForResult(new Intent(SPUActivity.this, SPUBlockActivity.class)
                .putExtra(SPBSHeader.XML_BLOCKS, listBlock)
                .putExtra(SPBSLine.XML_BLOCK, spuSummary.getBlock())
                .putExtra(SPBSHeader.XML_IMEI, imei)
                .putExtra(SPBSHeader.XML_YEAR, year)
                .putExtra(SPBSHeader.XML_COMPANY_CODE, companyCode)
                .putExtra(SPBSHeader.XML_ESTATE, estate)
                .putExtra(SPBSHeader.XML_DIVISION, division)
                .putExtra(SPBSHeader.XML_CROP, crop)
                .putExtra(SPBSHeader.XML_SPBS_NUMBER, spbsNumber)
                .putExtra(SPBSHeader.XML_SPBS_DATE, spbsDate)
                .putExtra(SPBSHeader.XML_CLERK, clerk)
                .putExtra(SPBSHeader.XML_STATUS, status), MST_TPH);
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> listNikFilter = new ArrayList<String>();

        switch (view.getId()) {
            case R.id.btnActionBarLeft:
                break;

            case R.id.btnActionBarRight:
                if(status == 0){
                    if (listSpuSummary.size() > 0 && !TextUtils.isEmpty(division) &&
                            !TextUtils.isEmpty(nikDriver) && !TextUtils.isEmpty(licensePlate) && !TextUtils.isEmpty(alias)) {
                        if (!isNew) {
                            new DialogConfirm(SPUActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                        }else{
                            try{

                                String realSPUNumber = new NomorFormat(companyCode, estate, division, year, monthAlphabet, String.valueOf(alias), String.valueOf(lastNumber)).getSPBSFormat();

                                database.openTransaction();
                                long result = database.setData(new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPUNumber, spbsDate, destId, destDesc, destType, division,
                                        nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                        gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr));

                                if (result == 0) {
                                    //update spbs hdr
                                    SPBSHeader objSpbsHdr = new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPUNumber, spbsDate, destId, destDesc, destType, division,
                                            nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                            gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr);
                                    database.updateData(objSpbsHdr,
                                            SPBSHeader.XML_IMEI + "=?" + " and " +
                                                    SPBSHeader.XML_YEAR + "=?" + " and " +
                                                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                    SPBSHeader.XML_CROP + "=?" + " and " +
                                                    SPBSHeader.XML_SPBS_NUMBER + "=?",
                                            new String[]{imei, year, companyCode, estate, crop, spbsNumber}
                                    );
                                }

                                for (int i = 0; i < listSpuSummary.size(); i++) {
                                    SPUSummary spuSummary = (SPUSummary) listSpuSummary.get(i);

                                    List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?",
                                            new String[]{year, companyCode, estate, spbsNumber, spbsDate, spuSummary.getBlock(), crop},
                                            null, null, null, null);

                                    if (listObject.size() > 0) {
                                        for (int j = 0; j < listObject.size(); j++) {
                                            SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                            String bpnID = spbsLine.getBpnId();
                                            spbsLine.setSpbsNumber(realSPUNumber);
                                            spbsLine.setIsSave(1);
                                            database.updateData(spbsLine,
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                            SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ID + "=?",
                                                    new String[]{year, companyCode, estate, spbsNumber, spbsDate, spuSummary.getBlock(), crop, spbsLine.getTph(),
                                                            spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});

                                            //Update BPN Header -> Set SPBSNumber
                                            BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(true, BPNHeader.TABLE_NAME, null,
                                                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                            BPNHeader.XML_ESTATE + "=?" + " and " +
                                                            BPNHeader.XML_DIVISION + "=?" + " and " +
                                                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                            BPNHeader.XML_CROP + "=?" + " and " +
                                                            BPNHeader.XML_BPN_ID + "=?",
                                                    new String [] {companyCode, estate, divisionLogin, nikClerk, crop, bpnID},
                                                    null, null, null, null);
                                            if(bpnHeader !=null){
                                                bpnHeader.setSpbsNumber(spbsNumber);

                                                database.updateData(bpnHeader,
                                                        BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                                BPNHeader.XML_ESTATE + "=?" + " and " +
                                                                BPNHeader.XML_DIVISION + "=?" + " and " +
                                                                BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                                BPNHeader.XML_CROP + "=?" + " and " +
                                                                BPNHeader.XML_BPN_ID + "=?",
                                                        new String[]{companyCode, estate, division, nikClerk, crop, bpnID});
                                            }
                                        }
                                    }
                                }

                                database.deleteData(SPBSRunningNumber.TABLE_NAME,
                                        SPBSRunningNumber.XML_ID + "=?" + " and " +
                                                SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                                                SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                                                SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                                                SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                                SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                                        new String[]{String.valueOf(id), estate, year, month, imei, alias});

                                database.setData(new SPBSRunningNumber(0, id, estate, divisionRun, year, month, imei, String.valueOf(lastNumber), alias));

                                database.deleteData(DeviceAlias.TABLE_NAME, null, null);
                                database.setData(new DeviceAlias(alias));

                                database.commitTransaction();

                                txtSpuNumber.setText(realSPUNumber);
                                spbsNumber = realSPUNumber;
                                isSave = 1;
                                new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                        getResources().getString(R.string.save_successed), false).show();

                            }catch (SQLiteException e) {
                                e.printStackTrace();
                                database.closeTransaction();
                                new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                        e.getMessage(), false).show();
                            } finally {
                                database.closeTransaction();
                            }
                        }
                    }else {
                        new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.Invalid_data), false).show();
                    }
                }
                break;

            case R.id.btnActionBarPrint:
                if(isSave == 1){
                    database.openTransaction();
                    Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
                    database.closeTransaction();

                    if(bluetooth !=null) {

                        if (bluetooth.getName().equalsIgnoreCase("DPP-350")) {
                            connectToPrinter();
                        }
                        else if(bluetooth.getName().contains("XX")){
                            PrintOut();
                        }

                        else if(!bluetooth.getName().contains("XX") || !bluetooth.getName().contains("DPP")){
                            connectToPrinter();
                        }

                    }else{
                        new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.bluetooth_not_found), false).show();
                    }
                }else{
                    new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_printing), false).show();
                }

                break;

            case R.id.txtSpuDivision:
                if(status == 0){
                    startActivityForResult(new Intent(SPUActivity.this, MasterDivisionAssistantActivity.class)
                                    .putExtra(DivisionAssistant.XML_ESTATE, estate)
                                    .putExtra(Constanta.SEARCH, true)
                                    .putExtra(BPNHeader.XML_CROP, crop),
                            MST_ASS);
                }else{
                    new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnSpuDriverIn:
                startActivityForResult(new Intent(SPUActivity.this, MasterEmployeeActivity.class)
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, "70")
                                .putExtra(Employee.XML_GANG, "")
                                .putExtra(Employee.XML_NIK, listNikFilter)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(Constanta.TYPE, EmployeeType.DRIVER.getId())
                                .putExtra(BPNHeader.XML_CROP, crop)
                                .putExtra("Activity", "SP"),
                        MST_DRIVER);
                break;
            case R.id.btnSpuDriverEx:
                startActivityForResult(new Intent(SPUActivity.this, DriverExternalActivity.class)
                                .putExtra(Employee.XML_NAME, driver)
                                .putExtra(BPNHeader.XML_CROP, crop)
                        , INP_DRIVER_EX);
                break;
            case R.id.txtSpuKernet:
                startActivityForResult(new Intent(SPUActivity.this, SPBSKernetActivity.class)
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, "70")
                                .putExtra(Employee.XML_GANG, "")
                                .putExtra(Employee.XML_NIK, nikKernet)
                                .putExtra(Employee.XML_NAME,  kernet)
                                .putExtra(BPNHeader.XML_CROP, crop)
                                .putExtra("Activity", "SP"),
                        MST_KERNET);
                break;
            case R.id.btnSpuLicensePlateIn:
                startActivityForResult(new Intent(SPUActivity.this, MasterVehicleActivity.class)
                                .putExtra(RunningAccount.XML_COMPANY_CODE, companyCode)
                                .putExtra(RunningAccount.XML_ESTATE, estate)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(BPNHeader.XML_CROP, crop),
                        MST_RUN_ACC);
                break;
            case R.id.btnSpuLicensePlateEx:
                startActivityForResult(new Intent(SPUActivity.this, LicensePlateExternalActivity.class)
                                .putExtra(RunningAccount.XML_LICENSE_PLATE, licensePlate)
                                .putExtra(BPNHeader.XML_CROP, crop),
                        INP_LICENSE_EX);
                break;
            case R.id.btnSpuLoadBpn:
                if(status == 0){
                    if(!TextUtils.isEmpty(nikAssistant)){
                        new DialogConfirm(SPUActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.data_reseted), null, R.id.btnSpuLoadBpn).show();
                    }else{
                        new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.select_assistant), false).show();
                    }
                }else{
                    new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnSpuAddBlock:
                if(status == 0){
                    if(!TextUtils.isEmpty(division)){
                       /* startActivityForResult(new Intent(SPUActivity.this, MasterBlockHdrcActivity.class)
                                        .putExtra(BlockHdrc.XML_COMPANY_CODE, companyCode)
                                        .putExtra(BlockHdrc.XML_ESTATE, estate)
                                        .putExtra(BlockHdrc.XML_DIVISION, division)
                                        .putExtra(Constanta.SEARCH, true),
                                MST_BLOCK);*/
                        startActivityForResult(new Intent(SPUActivity.this, BlockPlanningActivity.class)
                                .putExtra("readOnly", true)
                                .putExtra("croptype", "04"), MST_BLOCK);
                    }else{
                        new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.select_assistant), false).show();
                    }
                }else{
                    new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            default:
                break;
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPUSummary>, List<SPUSummary>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(SPUActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<SPUSummary> doInBackground(Void... voids) {
            List<SPUSummary> listTemp = new ArrayList<SPUSummary>();

            database.openTransaction();
            List<Object> listObject =  database.getListData(false, SPBSLine.TABLE_NAME, new String [] {SPBSLine.XML_BLOCK},
                    SPBSLine.XML_YEAR + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?" + " and " +
                            SPBSLine.XML_CROP + "=?" + " and " +
                            SPBSLine.XML_SPBS_NUMBER + "=?",
                    new String [] {year, companyCode, estate, crop, spbsNumber},
                    SPBSLine.XML_BLOCK, null, null, null);

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    SPBSLine spbsLine = (SPBSLine) listObject.get(i);

                    List<Object> listQtyGoodWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                            new String [] {year, companyCode, estate, crop, spbsNumber, spbsLine.getBlock(), BPNQuantity.GOOD_WEIGHT_CODE},
                            null, null, null, null);

                    List<Object> listQtyBadWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                            new String [] {year, companyCode, estate, crop, spbsNumber, spbsLine.getBlock(), BPNQuantity.BAD_WEIGHT_CODE},
                            null, null, null, null);

                    List<Object> listQtyPoorWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                            new String [] {year, companyCode, estate, crop, spbsNumber, spbsLine.getBlock(), BPNQuantity.POOR_WEIGHT_CODE},
                            null, null, null, null);

                    double qtyGoodWeight = 0;
                    double qtyBadWeight = 0;
                    double qtyPoorWeight = 0;
                    double qtyTotalWeight = 0;

                    if(listQtyGoodWeight.size() > 0){
                        for(int a = 0; a < listQtyGoodWeight.size(); a++){
                            SPBSLine spbsTemp = (SPBSLine) listQtyGoodWeight.get(a);

                            qtyGoodWeight = qtyGoodWeight + spbsTemp.getQuantityAngkut();
                        }
                    }

                    if(listQtyBadWeight.size() > 0){
                        for(int a = 0; a < listQtyBadWeight.size(); a++){
                            SPBSLine spbsTemp = (SPBSLine) listQtyBadWeight.get(a);

                            qtyBadWeight = qtyBadWeight + spbsTemp.getQuantityAngkut();
                        }
                    }

                    if(listQtyPoorWeight.size() > 0){
                        for(int a = 0; a < listQtyPoorWeight.size(); a++){
                            SPBSLine spbsTemp = (SPBSLine) listQtyPoorWeight.get(a);

                            qtyPoorWeight = qtyPoorWeight + spbsTemp.getQuantityAngkut();
                        }
                    }

                    listTemp.add(new SPUSummary(spbsNumber, spbsDate, spbsLine.getBlock(), qtyGoodWeight, qtyBadWeight, qtyPoorWeight, qtyTotalWeight) );
                }
            }
            database.closeTransaction();

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<SPUSummary> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            for(SPUSummary spuSummary : listTemp){
                adapter.addOrUpdateData(spuSummary);
            }
        }
    }

    @Override
    public void onBackPressed() {
        String spunumber = txtSpuNumber.getText().toString();
        if(!TextUtils.isEmpty(spunumber)) {
            new DialogConfirm(SPUActivity.this, getResources().getString(R.string.informasi),
                    getResources().getString(R.string.exit_saved), null, 1).show();
        }else {
            new DialogConfirm(SPUActivity.this, getResources().getString(R.string.informasi),
                    getResources().getString(R.string.exit), null, 1).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(requestCode == MST_ASS){
                division = data.getExtras().getString(DivisionAssistant.XML_DIVISION);

                String[] separated = division.split(",");
                if(separated.length == 2)
                {
                    division = separated[0];
                    lifnr = separated[1];
                }
                nikAssistant = "-";
                assistant = data.getExtras().getString(DivisionAssistant.XML_ASSISTANT_NAME, "");
                txtSpuDivision.setText(data.getExtras().getString(DivisionAssistant.XML_DIVISION));
            }else if(requestCode == MST_BLOCK){
                String block = data.getExtras().getString(BlockHdrc.XML_BLOCK, "");

                if(!TextUtils.isEmpty(block)){
                    boolean found = false;
                    for(int i = 0; i < listSpuSummary.size(); i++){
                        SPUSummary spuSummary = (SPUSummary) listSpuSummary.get(i);

                        if(block.equalsIgnoreCase(spuSummary.getBlock())){
                            found = true;
                            break;
                        }
                    }

                    if(!found){
                        if(adapter == null){
                            listSpuSummary = new ArrayList<SPUSummary>();
                            listSpuSummary.add(new SPUSummary(spbsNumber, spbsDate, block, 0,  0, 0, 0));
                            adapter = new AdapterSPUSummary(SPUActivity.this, listSpuSummary, R.layout.item_spu_summary);
                            lsvSpuSummary.setAdapter(adapter);
                        }else{
                            adapter.addOrUpdateData(new SPUSummary(spbsNumber, spbsDate, block, 0,  0, 0, 0));
                        }
                    }
                }
            }else if(requestCode == MST_TPH){
                getDataAsync = new SPUActivity.GetDataAsyncTask();
                getDataAsync.execute();
            }else if(requestCode == MST_DRIVER || requestCode == INP_DRIVER_EX){
                nikDriver = data.getExtras().getString(Employee.XML_NIK);
                driver = data.getExtras().getString(Employee.XML_NAME);
                txtSpuDriver.setText(driver);
            }else if(requestCode == MST_KERNET){
                nikKernet = data.getExtras().getString(Employee.XML_NIK);
                kernet = data.getExtras().getString(Employee.XML_NAME);
                txtSpuKernet.setText(kernet);
            }else if(requestCode == MST_RUN_ACC || requestCode == INP_LICENSE_EX){
                runningAccount = data.getExtras().getString(RunningAccount.XML_RUNNING_ACCOUNT);
                runningAccount = data.getExtras().getString(RunningAccount.XML_RUNNING_ACCOUNT);
                licensePlate = data.getExtras().getString(RunningAccount.XML_LICENSE_PLATE);
                txtSpuLicensePlate.setText(licensePlate);
            }
        }
    }

    @Override
    public void onOK(boolean is_finish) {

    }

    @Override
    public void onConfirmOK(Object object, int id) {
        switch (id) {
            case R.id.btnActionBarRight:

                long todayDate = new Date().getTime();
                try{
                    database.openTransaction();

                    database.updateData(new SPBSHeader(0, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, destId, destDesc, destType, division,
                                    nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                    gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr),
                            SPBSHeader.XML_YEAR + "=?" + " and " +
                                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSHeader.XML_ESTATE + "=?" + " and " +
                                    SPBSHeader.XML_CROP + "=?" + " and " +
                                    SPBSHeader.XML_SPBS_NUMBER + "=?",
                            new String [] {year, companyCode, estate, crop, spbsNumber});

                    for(int i = 0; i < listSpuSummary.size(); i++) {
                        SPUSummary spuSummary = (SPUSummary) listSpuSummary.get(i);

                        List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                SPBSLine.XML_YEAR + "=?"+ " and " +
                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                        SPBSLine.XML_CROP + "=?",
                                new String [] {year, companyCode, estate, spbsNumber, spbsDate, spuSummary.getBlock(), crop},
                                null, null, null, null);

                        if(listObject.size() > 0) {
                            for (int j = 0; j < listObject.size(); j++) {
                                SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                String bpnID = spbsLine.getBpnId();
                                spbsLine.setIsSave(1);
                                spbsLine.setModifiedDate(todayDate);
                                spbsLine.setModifiedBy(clerk);

                                database.updateData(spbsLine,
                                        SPBSLine.XML_YEAR + "=?"+ " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                SPBSLine.XML_ID + "=?",
                                        new String [] {year, companyCode, estate, spbsNumber, spbsDate, spuSummary.getBlock(), crop, spbsLine.getTph(),
                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});

                                //Update BPN Header -> Set SPBSNumber
                                BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(true, BPNHeader.TABLE_NAME, null,
                                        BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNHeader.XML_ESTATE + "=?" + " and " +
                                                BPNHeader.XML_DIVISION + "=?" + " and " +
                                                BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                BPNHeader.XML_CROP + "=?" + " and " +
                                                BPNHeader.XML_BPN_ID + "=?",
                                        new String [] {companyCode, estate, divisionLogin, nikClerk, crop, bpnID},
                                        null, null, null, null);
                                if(bpnHeader !=null){
                                    bpnHeader.setSpbsNumber(spbsNumber);

                                    database.updateData(bpnHeader,
                                            BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNHeader.XML_ESTATE + "=?" + " and " +
                                                    BPNHeader.XML_DIVISION + "=?" + " and " +
                                                    BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                    BPNHeader.XML_CROP + "=?" + " and " +
                                                    BPNHeader.XML_BPN_ID + "=?",
                                            new String[]{companyCode, estate, division, nikClerk, crop, bpnID});
                                }
                            }
                        }
                    }

                    database.commitTransaction();

                    txtSpuNumber.setText(spbsNumber);

                    new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_successed), false).show();
                }catch(SQLiteException e){
                    e.printStackTrace();
                    database.closeTransaction();
                    new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                            e.getMessage(), false).show();
                }finally{
                    database.closeTransaction();
                }
                break;

            case R.id.btnSpuLoadBpn:
                new DialogDate(SPUActivity.this, getResources().getString(R.string.tanggal), new Date(), R.id.btnSpuLoadBpn).show();
                break;

            case R.id.btnSpuSummaryItemDelete:
                Toast.makeText(getApplicationContext(),"DELETE SPU ITEM DETAIL",Toast.LENGTH_SHORT).show();
                try{
                    SPUSummary spuSummary = (SPUSummary) object;

                    database.openTransaction();

                    List<Object> lstObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?",
                            new String [] {spbsNumber, spbsDate, spuSummary.getBlock()},
                            null, null, null, null);

                    int delCount = 0;
                    boolean isSPBSNext = false;

                    if(lstObject != null && lstObject.size() > 0) {

                        String bpnId = "";
                        for (Object obj : lstObject) {
                            SPBSLine spbsLine = (SPBSLine) obj;

                            String block = spbsLine.getBlock();
                            String tph = spbsLine.getTph();
                            String spbsNext = spbsLine.getSpbsNext();
                            String achievmentcode = spbsLine.getAchievementCode();
                            bpnId = spbsLine.getBpnId();
                            spbsNumber = spbsLine.getSpbsNumber();

                            if(TextUtils.isEmpty(spbsNext)){
                                database.deleteData(SPBSLine.TABLE_NAME,
                                        SPBSLine.XML_ID + "=?" + " and " +
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                SPBSLine.XML_BPN_ID + "=?" + " and " +
                                                SPBSLine.XML_BPN_DATE + "=?",
                                        new String [] {String.valueOf(spbsLine.getId()), year, companyCode, estate, crop, spbsNumber, spbsDate, block, tph, achievmentcode, bpnId, spbsLine.getBpnDate()});
                            }else{
                                isSPBSNext = true;
                                break;
                            }
                        }

                        if(isSPBSNext) {
                            new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.data_delete_cannot_all), false).show();
                        }else {

                            BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(true, BPNHeader.TABLE_NAME, null,
                                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                            BPNHeader.XML_ESTATE + "=?" + " and " +
                                            BPNHeader.XML_DIVISION + "=?" + " and " +
                                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                            BPNHeader.XML_SPBS_NUMBER + " is null" + " or " +
                                            BPNHeader.XML_SPBS_NUMBER + "=?" + " and " +
                                            BPNHeader.XML_CROP + "=?" + " and " +
                                            BPNHeader.XML_BPN_ID + "=?",
                                    new String[]{companyCode, estate, division, nikClerk, "", crop, bpnId},
                                    null, null, null, null);
                            if (bpnHeader != null) {
                                bpnHeader.setSpbsNumber("");

                                database.updateData(bpnHeader,
                                        BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNHeader.XML_ESTATE + "=?" + " and " +
                                                BPNHeader.XML_DIVISION + "=?" + " and " +
                                                BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                BPNHeader.XML_SPBS_NUMBER + " is null" + " or " +
                                                BPNHeader.XML_SPBS_NUMBER + "=?" + " and " +
                                                BPNHeader.XML_CROP + "=?" + " and " +
                                                BPNHeader.XML_BPN_ID + "=?",
                                        new String[]{companyCode, estate, division, nikClerk, "", crop, bpnId});

                                database.commitTransaction();

                                List<Object> lstObjectDelete = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?",
                                        new String[]{spbsNumber, spbsDate, spuSummary.getBlock()},
                                        null, null, null, null);

                                if (delCount == lstObjectDelete.size()) {
                                    adapter.deleteData(spuSummary);
                                } else {
                                    new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                            getResources().getString(R.string.data_delete_cannot), false).show();
                                }
                            }
                            else{
                                database.commitTransaction();
                                adapter.deleteData(spuSummary);
                            }
                        }
                    }

                }catch(SQLiteException e){
                    e.printStackTrace();
                }finally{
                    database.closeTransaction();
                }

                break;

            default:
                if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
                    getDataAsync.cancel(true);
                }

                if(dialogProgress != null && dialogProgress.isShowing()){
                    dialogProgress.dismiss();
                    dialogProgress = null;
                }

                if(gps != null){
                    gps.stopUsingGPS();
                }

                deleteUnSaved();
                gpsHandler.stopGPS();

                if(status == 0) {
                    if (listSpuSummary.size() > 0 && !TextUtils.isEmpty(division) &&
                            !TextUtils.isEmpty(nikDriver) && !TextUtils.isEmpty(licensePlate) && !TextUtils.isEmpty(alias)) {

                        todayDate = new Date().getTime();
                        try{
                            database.openTransaction();

                            database.updateData(new SPBSHeader(0, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, destId, destDesc, destType, division,
                                            nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                            gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr),
                                    SPBSHeader.XML_YEAR + "=?" + " and " +
                                            SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                                            SPBSHeader.XML_CROP + "=?" + " and " +
                                            SPBSHeader.XML_SPBS_NUMBER + "=?",
                                    new String [] {year, companyCode, estate, crop, spbsNumber});

                            for(int i = 0; i < listSpuSummary.size(); i++){
                                SPUSummary spuSummary = (SPUSummary) listSpuSummary.get(i);

                                List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                        SPBSLine.XML_YEAR + "=?"+ " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?",
                                        new String [] {year, companyCode, estate, spbsNumber, spbsDate, spuSummary.getBlock(), crop},
                                        null, null, null, null);

                                if(listObject.size() > 0) {
                                    for (int j = 0; j < listObject.size(); j++) {
                                        SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                        String bpnID = spbsLine.getBpnId();
                                        spbsLine.setIsSave(1);
                                        spbsLine.setModifiedDate(todayDate);
                                        spbsLine.setModifiedBy(clerk);

                                        database.updateData(spbsLine,
                                                SPBSLine.XML_YEAR + "=?"+ " and " +
                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                        SPBSLine.XML_CROP + "=?" + " and " +
                                                        SPBSLine.XML_TPH + "=?" + " and " +
                                                        SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ID + "=?",
                                                new String [] {year, companyCode, estate, spbsNumber, spbsDate, spuSummary.getBlock(), crop, spbsLine.getTph(),
                                                        spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});

                                        //Update BPN Header -> Set SPBSNumber
                                        BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(true, BPNHeader.TABLE_NAME, null,
                                                BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                        BPNHeader.XML_ESTATE + "=?" + " and " +
                                                        BPNHeader.XML_DIVISION + "=?" + " and " +
                                                        BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                        BPNHeader.XML_CROP + "=?" + " and " +
                                                        BPNHeader.XML_BPN_ID + "=?",
                                                new String [] {companyCode, estate, divisionLogin, nikClerk, crop, bpnID},
                                                null, null, null, null);
                                        if(bpnHeader !=null){
                                            bpnHeader.setSpbsNumber(spbsNumber);

                                            database.updateData(bpnHeader,
                                                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                            BPNHeader.XML_ESTATE + "=?" + " and " +
                                                            BPNHeader.XML_DIVISION + "=?" + " and " +
                                                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                            BPNHeader.XML_CROP + "=?" + " and " +
                                                            BPNHeader.XML_BPN_ID + "=?",
                                                    new String[]{companyCode, estate, division, nikClerk, crop, bpnID});
                                        }
                                    }
                                }
                            }

                            database.commitTransaction();

                            txtSpuNumber.setText(spbsNumber);

                            Toast.makeText(getApplicationContext(), "Save/Update Success", Toast.LENGTH_SHORT).show();
                            finish();
                            animOnFinish();
                        }catch(SQLiteException e){
                            e.printStackTrace();
                            database.closeTransaction();
                            new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                    e.getMessage(), false).show();
                        }finally{
                            database.closeTransaction();
                        }
                    }

                    finish();
                    animOnFinish();
                }else{
                    //new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi),
                    //		getResources().getString(R.string.data_already_export), false).show();
                    finish();
                    animOnFinish();
                }

                break;
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        String filterDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
        long todayDate = new Date().getTime();
        int spbsLineid=0;

        try{
            database.openTransaction();

            List<Object> lstSpbsLine = database.getListData(false, SPBSLine.TABLE_NAME, null,
                    SPBSLine.XML_YEAR + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?" + " and " +
                            SPBSLine.XML_CROP + "=?" + " and " +
                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                            SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                            SPBSLine.XML_BPN_DATE + "=?",
                    new String [] {year, companyCode, estate, crop, spbsNumber, spbsDate, filterDate},
                    null, null, null, null);

            if(lstSpbsLine.size() > 0){
                for(int i = 0; i < lstSpbsLine.size(); i++){
                    SPBSLine spbsLine = (SPBSLine) lstSpbsLine.get(i);

                    String bpnId = spbsLine.getBpnId();
                    String spbsRef = spbsLine.getSpbsRef();
                    String spbsNext = spbsLine.getSpbsNext();

                    boolean del = false;

                    if(spbsRef.isEmpty()){
                    }else{
                        if(TextUtils.isEmpty(spbsNext)){
                            String spbsNumber = spbsRef.split("_")[0];
                            String lineId = spbsRef.split("_")[1];

                            SPBSLine spbsLineRef = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                    SPBSLine.XML_ID + " = ? " + " and " +
                                            SPBSLine.XML_SPBS_NUMBER + " = ? ",
                                    new String [] {lineId, spbsNumber},
                                    null, null, null, null);

                            if(spbsLineRef != null){
                                spbsLineRef.setSpbsNext("");

                                database.updateData(spbsLineRef,
                                        SPBSLine.XML_ID + " = ? " + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + " = ?",
                                        new String [] {lineId, spbsNumber});

                                del = true;
                            }
                        }
                    }

                    if(del){
                        database.deleteData(SPBSLine.TABLE_NAME,
                                SPBSLine.XML_ID + "=?" + " and " +
                                        SPBSLine.XML_SPBS_NUMBER + "=?",
                                new String [] {String.valueOf(spbsLine.getId()), spbsLine.getSpbsNumber()});
                    }
                }
            }

            List<Object> lstSpbsLineExist = database.getListData(false, SPBSLine.TABLE_NAME, null,
                    SPBSLine.XML_YEAR + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?" + " and " +
                            SPBSLine.XML_CROP + "=?" + " and " +
                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                            SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                            SPBSLine.XML_BPN_DATE + "!=?",
                    new String [] {year, companyCode, estate, crop, spbsNumber, spbsDate, filterDate},
                    null, null, null, null);

            if(lstSpbsLineExist.size() > 0 )
                spbsLineid = lstSpbsLineExist.size();

            List<Object> listBlock = database.getListData(true, BPNHeader.TABLE_NAME, null,
                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BPNHeader.XML_ESTATE + "=?" + " and " +
                            //BPNHeader.XML_DIVISION + "=?" + " and " +
                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                            /*BPNHeader.XML_SPBS_NUMBER + " is null" + " or " +
                            BPNHeader.XML_SPBS_NUMBER + "=?" + " and " +*/
                            BPNHeader.XML_CROP + "=?" + " and " +
                            BPNHeader.XML_BPN_DATE + "=?",
                    new String [] {companyCode, estate, nikClerk, crop, filterDate},
                    null, null, null, null);

            List<Object> listBlockDivision = database.getListData(true, BlockHdrc.TABLE_NAME, null,
                    BlockHdrc.XML_COMPANY_CODE + "=?" + " and " +
                            BlockHdrc.XML_ESTATE + "=?" + " and " +
                            BlockHdrc.XML_DIVISION + "=?",
                    new String [] {companyCode, estate,  division},
                    null, null, null, null);

            List<Object> listBlockFilterDivistion = new ArrayList<Object>();

            for(int j = 0; j< listBlock.size(); j++){
                BPNHeader bpnHeader = (BPNHeader) listBlock.get(j);
                for(int q = 0; q < listBlockDivision.size(); q++){
                    BlockHdrc blockHdrc = (BlockHdrc) listBlockDivision.get(q);
                    if(bpnHeader.getLocation().equalsIgnoreCase(blockHdrc.getBlock())){
                        listBlockFilterDivistion.add(bpnHeader);
                    }
                }
            }


            if(listBlockFilterDivistion != null && listBlockFilterDivistion.size() > 0){
                for(int i = 0; i < listBlockFilterDivistion.size(); i++) {
                    BPNHeader bpnHeader = (BPNHeader) listBlockFilterDivistion.get(i);

                    List<Object> lstBPNExist = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_BPN_ID + "=?",
                            new String[]{year, companyCode, estate, crop, bpnHeader.getBpnId()},
                            null, null, null, null);

                    if (lstBPNExist.size() == 0) {

                        if (spbsLineid == 0)
                            spbsLineid = 1;
                        else
                            spbsLineid = spbsLineid + 1;

                        String imei = bpnHeader.getImei();
                        String companyCode = bpnHeader.getCompanyCode();
                        String estate = bpnHeader.getEstate();
                        String bpnId = bpnHeader.getBpnId();
                        String bpnDt = bpnHeader.getBpnDate();
                        String division = bpnHeader.getDivision();
                        String gang = bpnHeader.getGang();
                        String block = bpnHeader.getLocation();
                        String crop = bpnHeader.getCrop();
                        String tph = bpnHeader.getTph();
                        String gpsKoordinat = bpnHeader.getGpsKoordinat();

                        double qtyGoodQty = getQtyRemaining(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.GOOD_QTY_CODE);
                        double qtyGoodWeight = getQtyRemaining(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.GOOD_WEIGHT_CODE);

                        double qtyBadQty = getQtyRemaining(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.BAD_QTY_CODE);
                        double qtyBadWeight = getQtyRemaining(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.BAD_WEIGHT_CODE);

                        double qtyPoorQty = getQtyRemaining(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.POOR_QTY_CODE);
                        double qtyPoorWeight = getQtyRemaining(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.POOR_WEIGHT_CODE);

                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.GOOD_QTY_CODE, qtyGoodQty, qtyGoodQty, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));
                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.GOOD_WEIGHT_CODE, qtyGoodWeight, qtyGoodWeight, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));

                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.BAD_QTY_CODE, qtyBadQty, qtyBadQty, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));
                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.BAD_WEIGHT_CODE, qtyBadWeight, qtyBadWeight, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));

                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.POOR_QTY_CODE, qtyPoorQty, qtyPoorQty, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));
                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.POOR_WEIGHT_CODE, qtyPoorWeight, qtyPoorWeight, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));
                    }
                }
            }

            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
            new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                    e.getMessage(), false).show();
        }finally{
            database.closeTransaction();
        }

        getDataAsync = new SPUActivity.GetDataAsyncTask();
        getDataAsync.execute();
    }


    private double getQtyRemaining(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String tph, String achievementCode){
        double qty = 0;

        List<Object> lstQty=  database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_GANG + "=?" + " and " +
                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                        BPNQuantity.XML_TPH + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                new String [] {bpnId, bpnDate, companyCode, estate, bpnDate, division, gang, block, tph, achievementCode},
                null, null, null, null);

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }

    private void deleteUnSaved(){
        database.openTransaction();
        database.deleteData(SPBSLine.TABLE_NAME,
                SPBSLine.XML_IS_SAVE + "=?",
                new String [] {"0"});
        database.commitTransaction();
        database.closeTransaction();
    }

    public void updateGpsKoordinat(Location location){
        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }
    }

    private SPBSRunningNumber getAlias(String estate, String division, String year, String month, String imei){
        DatabaseHandler database = new DatabaseHandler(SPUActivity.this);
        SPBSRunningNumber spbsNumberMax = null;

        database.openTransaction();
        DeviceAlias deviceAlias = (DeviceAlias) database.getDataFirst(false, DeviceAlias.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if(deviceAlias == null){
            database.openTransaction();
            spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                    SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                            SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                            SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                            SPBSRunningNumber.XML_IMEI + "=?",
                    new String [] {estate, year, month, imei},
                    null, null, SPBSRunningNumber.XML_ID + " desc", null);
            database.closeTransaction();

            if(spbsNumberMax == null){
                database.openTransaction();
                SPBSRunningNumber tempNumber = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                        SPBSRunningNumber.XML_IMEI + "=?",
                        new String [] {imei},
                        null, null, SPBSRunningNumber.XML_ID + " desc", null);
                database.closeTransaction();

                if(tempNumber != null){
                    String alias = tempNumber.getDeviceAlias();
                    int id = tempNumber.getId() + 1;
                    spbsNumberMax = new SPBSRunningNumber(0, id, estate, division, year, month, imei, "0", alias);
                }
            }
        }else{
            database.openTransaction();
            spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                    SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                            SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                            SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                            SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                            SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                    new String [] {estate, year, month, imei, deviceAlias.getDeviceAlias()},
                    null, null, SPBSRunningNumber.XML_ID + " desc", null);
            database.closeTransaction();

            if(spbsNumberMax == null){
                database.openTransaction();
                SPBSRunningNumber tempNumber = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                        SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                        new String [] {imei, deviceAlias.getDeviceAlias()},
                        null, null, SPBSRunningNumber.XML_ID + " desc", null);
                database.closeTransaction();

                if(tempNumber != null){
                    String alias = tempNumber.getDeviceAlias();
                    int id = tempNumber.getId() + 1;
                    spbsNumberMax = new SPBSRunningNumber(0, id, estate, division, year, month, imei, "0", alias);
                }
            }
        }

        return spbsNumberMax;
    }

    // DATECS PRINT -- START //
    private void connectToPrinter(){
        try{
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (!bluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }


            database.openTransaction();
            Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            BluetoothDevice pairedDevice;
            Iterator<BluetoothDevice> iter = (bluetoothAdapter.getBondedDevices()).iterator();
            while(iter.hasNext()){
                pairedDevice = iter.next();
                if(bluetooth != null){
                    if(pairedDevice.getAddress().equalsIgnoreCase(bluetooth.getAddress())){
                        bluetoothDevice = pairedDevice;
                        break;
                    }
                }
            }

            if(bluetoothDevice != null){
                if(mBtSocket != null && mBtSocket.isConnected()){
                    collectSPU();
                }else{
                    establishBluetoothConnection(bluetoothDevice.getAddress());
                }
            }else{
                new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                        getResources().getString(R.string.bluetooth_not_found), false).show();
            }

        }catch(NullPointerException e){
            e.printStackTrace();
        }
    }

    // The Handler that gets information back from the BluetoothPrintService
    private final MyHandler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<SPUActivity> mActivity;

        public MyHandler(SPUActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            SPUActivity activity = mActivity.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }

    private void handleMessage(Message msg) {
        new SPUActivity.connTask().execute();
    }

    class connTask extends AsyncTask<Void, Void, Void>{
        String name;
        String address;
        boolean isPaired;
        boolean isSelected;

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids){

            return null;
        }

        @Override
        protected void onPostExecute(Void voids){
            super.onPostExecute(voids);
            collectSPU();
        }
    }

    private void collectSPU(){
        List<SPUPrint> listSpuPrint = new ArrayList<SPUPrint>();

        database.openTransaction();

        List<Object> listSpuLineBlock = database.getListData(true, SPBSLine.TABLE_NAME,
                new String [] {SPBSLine.XML_BLOCK, SPBSLine.XML_BPN_DATE},
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?",
                new String [] {year, companyCode, estate, crop, spbsNumber},
                SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null, SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null);
        database.closeTransaction();

        if(listSpuLineBlock.size() > 0) {
            for (int i = 0; i < listSpuLineBlock.size(); i++) {
                SPBSLine spuLineBlock = (SPBSLine) listSpuLineBlock.get(i);

                String block = spuLineBlock.getBlock();
                String bpnDate = spuLineBlock.getBpnDate();

                database.openTransaction();

                List<Object> listSpuLineQtyGoodWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.GOOD_WEIGHT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyGoodWeight = 0;
                if(listSpuLineQtyGoodWeight.size() > 0){
                    for(int k = 0; k < listSpuLineQtyGoodWeight.size(); k++){
                        SPBSLine spbsLine = (SPBSLine) listSpuLineQtyGoodWeight.get(k);

                        qtyGoodWeight = (double) (qtyGoodWeight + spbsLine.getQuantityAngkut());
                    }
                }

                database.openTransaction();
                List<Object> listSpuLineQtyBadWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.BAD_WEIGHT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyBadWeight = 0;
                if(listSpuLineQtyBadWeight.size() > 0){
                    for(int k = 0; k < listSpuLineQtyBadWeight.size(); k++){
                        SPBSLine spbsLine = (SPBSLine) listSpuLineQtyBadWeight.get(k);

                        qtyBadWeight = (double) (qtyBadWeight + spbsLine.getQuantityAngkut());
                    }
                }

                database.openTransaction();
                List<Object> listSpuLineQtyPoorWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.POOR_WEIGHT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyPoorWeight = 0;
                if(listSpuLineQtyPoorWeight.size() > 0){
                    for(int k = 0; k < listSpuLineQtyPoorWeight.size(); k++){
                        SPBSLine spbsLine = (SPBSLine) listSpuLineQtyPoorWeight.get(k);

                        qtyPoorWeight = (double) (qtyPoorWeight + spbsLine.getQuantityAngkut());
                    }
                }

                double qtyTotalWeight = (double) qtyGoodWeight +  qtyBadWeight +  qtyPoorWeight;

                SPUPrint spuPrint = new SPUPrint(block, bpnDate, qtyGoodWeight, qtyBadWeight, qtyPoorWeight, qtyTotalWeight);
                listSpuPrint.add(spuPrint);
            }

            final int MAX_LINE = 15;

            if(listSpuPrint.size() > 0){
                int id = 1;
                int maxPages = 1;
                int pages = 1;
                int posStart = 0;
                int posEnd = 0;

                if(listSpuPrint.size() % MAX_LINE == 0){
                    maxPages = (int) listSpuPrint.size() / MAX_LINE;
                }else{
                    maxPages = ((int) listSpuPrint.size() / MAX_LINE) + 1;
                }

                for(int i = 0; i < listSpuPrint.size(); i++){
                    posEnd = i;

                    if((id == MAX_LINE) || (i == (listSpuPrint.size()-1))){
                        try {
                            int retVal = printSPU(listSpuPrint, posStart, posEnd, pages, maxPages);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        id = 1;
                        posStart = i+1;
                        pages++;
                    }

                    id++;
                }
            }
        }
    }

    private int printSPU(final List<SPUPrint> listSpuPrint, final int posStart, final int posEnd, final int pages, final int maxPages) throws IOException {
        runTask(new SPUActivity.PrinterRunnable() {
            @Override
            public void run(ProgressDialog dialog, Printer printer) throws IOException, ParseException {
                StringBuffer textBuffer = new StringBuffer();

                String headerQrCode = getHeaderQrCode(spbsNumber, spbsDate, estate, division, licensePlate, driver, nikDriver, runningAccount, pages, maxPages);
                String lineQrCode = "";

//            	long print_date=new Date().getTime();
                String strPrintDate=new DateLocal(String.valueOf(todayDate), DateLocal.FORMAT_MASTER_XML).getDateString(DateLocal.FORMAT_MASTER_XML);
                for(int i = posStart; i <= posEnd; i++){
                    SPUPrint spuPrint = listSpuPrint.get(i);

                    if( i == posStart){
                        lineQrCode = getLineQrCode(i + 1, spuPrint.getBlock(), spuPrint.getBpnDate(), spuPrint.getQtyGoodWeight(), spuPrint.getQtyBadWeight(), spuPrint.getQtyPoorWeight(), spuPrint.getQtyTotalWeight());
                    }else{
                        lineQrCode = lineQrCode + QR_CODE_LINE_DELIMITER + getLineQrCode(i + 1, spuPrint.getBlock(), spuPrint.getBpnDate(), spuPrint.getQtyGoodWeight(), spuPrint.getQtyBadWeight(), spuPrint.getQtyPoorWeight(), spuPrint.getQtyTotalWeight());
                    }
                }

                database.openTransaction();
                Vendor vendor = (Vendor) database.getDataFirst(false, Vendor.TABLE_NAME, null,
                        Vendor.XML_ESTATE + "=?" + " and " +
                                Vendor.XML_LIFNR + "=?" ,
                        new String [] {estate, lifnr},  null, null, null, null);

                if(vendor !=null){
                    vendorName = vendor.getName();
                }

                database.closeTransaction();

                textBuffer.append("{right} {i} "+strPrintDate);
                textBuffer.append("{br}");
                textBuffer.append("{reset}{center}{b}SPU Ticket");
                textBuffer.append("{br}");

                textBuffer.append("{br}");
                textBuffer.append("{reset}");
                textBuffer.append("SPU Number     : " + spbsNumber);
                textBuffer.append("{br}");
                textBuffer.append("SPU Date       : " + new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS));
                textBuffer.append("{br}");
                textBuffer.append("Estate/Division : " + estate + " / " + division);
                textBuffer.append("{br}");
                textBuffer.append("Clerk           : " + nikClerk + " - " + addChar(clerk, " ", 15, 1));
                textBuffer.append("{br}");
                textBuffer.append("License Plate   : " + licensePlate);
                textBuffer.append("{br}");
                textBuffer.append("Running Account : " + runningAccount);
                textBuffer.append("{br}");
                textBuffer.append("Driver          : " + nikDriver + " - " + addChar(driver, " ", 15, 1));
                textBuffer.append("{br}");
                textBuffer.append("Vendor          : " + lifnr + " - " + vendorName);
                textBuffer.append("{br}");
                textBuffer.append("Pages           : " + String.valueOf(pages) + " of " + String.valueOf(maxPages));
                textBuffer.append("{br}");
                textBuffer.append("-----------------------------------------------");
                textBuffer.append("{br}");
                textBuffer.append(addChar("  Block", " ", 7,-1) + " " + addChar("HVRDate", " ", 12, -1) + " " + addChar("Good(Kg)", " ", 8, -1) + " " + addChar("Poor(Kg)", " ", 8, -1) + " " + "Bad(Kg)");
                textBuffer.append("{br}");
                textBuffer.append("-----------------------------------------------");
                textBuffer.append("{br}");

                for(int i = posStart; i <= posEnd; i++){
                    SPUPrint spuPrint = listSpuPrint.get(i);

                    String block = spuPrint.getBlock();
                    String bpnDate =  new DateLocal(spuPrint.getBpnDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW);
                    String qtyGoodWeight = String.valueOf(Utils.round(spuPrint.getQtyGoodWeight(), 0)); //addNol(spuPrint.getQtyGoodWeight(), 0);
                    String qtyPoorWeight = String.valueOf(Utils.round(spuPrint.getQtyPoorWeight(), 0)); //addNol(spuPrint.getQtyPoorWeight(), 0);
                    String qtyBadWeight = String.valueOf(Utils.round(spuPrint.getQtyBadWeight(), 0));//addNol(spuPrint.getQtyBadWeight(), 0);

                    database.openTransaction();
                    String bpnDateStr = spuPrint.getBpnDate();

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        long bpndate = format.parse(spuPrint.getBpnDate()).getTime();

                        BLKRISET blkriset = (BLKRISET) database.getDataFirst(false, BLKRISET.TABLE_NAME, null,
                                BLKRISET.XML_BLOCK + "=?" + " and " +
                                        BLKRISET.XML_START_DATE_LONG + "<=?" + " and " +
                                        BLKRISET.XML_END_DATE_LONG + ">=?",
                                new String [] {block, String.valueOf(bpndate), String.valueOf(bpndate)},  null, null, null, null);

                        if(blkriset !=null){
                            block = block + "-R";
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    database.closeTransaction();

                    textBuffer.append(addChar(block, " ", 8, -1) + " " + addChar(bpnDate, " ", 12, -1) + " " + addChar(qtyGoodWeight, " ", 8, -1) + " " + addChar(qtyPoorWeight, " ", 7, -1) + " " + addChar(qtyBadWeight, " ", 7, -1));
                    textBuffer.append("{br}");
                }

                textBuffer.append("{br}");
                String qrType = new SharedPreferencesHandler(SPUActivity.this).getQR();
                String qrCode = "";
                if(qrType.equalsIgnoreCase("full")){
                    qrCode = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
                }
                else {
                    qrCode = spbsNumber + ";" + licensePlate + ";" + driver + ";" + lifnr;
                }
//            	String qrCode = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
//            	String qrCode = spbsNumber + ";" + licensePlate + ";" + driver;

                printer.reset();
                printer.printTaggedText(textBuffer.toString());
                printer.setBarcode(Printer.ALIGN_CENTER, false, 2, Printer.HRI_NONE, 100);
                printer.printQRCode(14, 1, qrCode);
                printer.feedPaper(110);

                printer.flush();
            }
        }, R.string.wait);


        return 0;
    }

    protected void initPrinter(InputStream inputStream, OutputStream outputStream) throws IOException {
        // Here you can enable various debug information
        //ProtocolAdapter.setDebug(true);
        Printer.setDebug(true);
        EMSR.setDebug(true);

        // Check if printer is into protocol mode. Ones the object is created it can not be released
        // without closing base streams.
        mProtocolAdapter = new ProtocolAdapter(inputStream, outputStream);
        if (mProtocolAdapter.isProtocolEnabled()) {
            // Into protocol mode we can callbacks to receive printer notifications
            mProtocolAdapter.setPrinterListener(new ProtocolAdapter.PrinterListener() {
                @Override
                public void onThermalHeadStateChanged(boolean overheated) {
                }

                @Override
                public void onPaperStateChanged(boolean hasPaper) {
                }

                @Override
                public void onBatteryStateChanged(boolean lowBattery) {
                }
            });

            mProtocolAdapter.setBarcodeListener(new ProtocolAdapter.BarcodeListener() {
                @Override
                public void onReadBarcode() {
                }
            });

            mProtocolAdapter.setCardListener(new ProtocolAdapter.CardListener() {
                @Override
                public void onReadCard(boolean encrypted) {
                }
            });

            // Get printer instance
            mPrinterChannel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_PRINTER);
            mPrinter = new Printer(mPrinterChannel.getInputStream(), mPrinterChannel.getOutputStream());

            // Check if printer has encrypted magnetic head
            mUniversalChannel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_UNIVERSAL_READER);
            new UniversalReader(mUniversalChannel.getInputStream(), mUniversalChannel.getOutputStream());

        } else {
            // Protocol mode it not enables, so we should use the row streams.
            mPrinter = new Printer(mProtocolAdapter.getRawInputStream(),
                    mProtocolAdapter.getRawOutputStream());
        }

        mPrinter.setConnectionListener(new Printer.ConnectionListener() {
            @Override
            public void onDisconnect() {
                closePrinterServer();
            }
        });

    }

    private void establishBluetoothConnection(final String address) {
        final ProgressDialog dialog = new ProgressDialog(SPUActivity.this);
        dialog.setTitle(getString(R.string.informasi));
        dialog.setMessage(getString(R.string.wait));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        closePrinterServer();

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                btAdapter.cancelDiscovery();

                try {
                    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                    BluetoothDevice btDevice = btAdapter.getRemoteDevice(address);

                    InputStream in = null;
                    OutputStream out = null;

                    try {
                        BluetoothSocket btSocket = btDevice.createRfcommSocketToServiceRecord(uuid);
                        btSocket.connect();

                        mBtSocket = btSocket;
                        in = mBtSocket.getInputStream();
                        out = mBtSocket.getOutputStream();
                    } catch (IOException e) {
                        return;
                    }

                    try {
                        initPrinter(in, out);

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                collectSPU();
                            }
                        });
                    } catch (IOException e) {
                        return;
                    }
                } finally {
                    dialog.dismiss();
                }
            }
        });
        t.start();
    }

    private void runTask(final SPUActivity.PrinterRunnable r, final int msgResId) {
        final ProgressDialog dialog = new ProgressDialog(SPUActivity.this);
        dialog.setTitle(getString(R.string.informasi));
        dialog.setMessage(getString(msgResId));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    r.run(dialog, mPrinter);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                } finally {
                    dialog.dismiss();
                }
            }
        });
        t.start();
    }

    private synchronized void closePrinterServer() {
        PrinterServer ps = mPrinterServer;
        mPrinterServer = null;
        if (ps != null) {
            try {
                ps.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    // DATECS PRINT -- END//

    // ZEBRA PRINT -- START //
    private void PrintOut() {
        DialogNotification dialogNotification;
        database.openTransaction();
        Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        printerConnection = new BluetoothConnection(bluetooth.getAddress());

        try {
            helper.showLoadingDialog("Connecting...");
            printerConnection.open();

            ZebraPrinter printer = null;

            if (printerConnection.isConnected()) {
                printer = ZebraPrinterFactory.getInstance(printerConnection);

                if (printer != null) {
                    PrinterLanguage pl = printer.getPrinterControlLanguage();
                    if (pl == PrinterLanguage.CPCL) {
                        helper.showErrorDialogOnGuiThread("This demo will not work for CPCL printers!");
                    } else {
                        // [self.connectivityViewController setStatus:@"Building receipt in ZPL..." withColor:[UIColor
                        // cyanColor]];
                        dialogNotification = new DialogNotification(SPUActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.wait), false);
                        dialogNotification.show();
                        //sendTestLabel();
                        collectSPUZebra(dialogNotification);

                    }
                    printerConnection.close();
                    saveSettings();
                }
            }
        } catch (ConnectionException e) {
            helper.showErrorDialogOnGuiThread(e.getMessage());
        } catch (ZebraPrinterLanguageUnknownException e) {
            helper.showErrorDialogOnGuiThread("Could not detect printer language");
        } finally {
            helper.dismissLoadingDialog();
        }

    }

    private void collectSPUZebra(DialogNotification dialogNotification){
        List<SPUPrint> listSpuPrint = new ArrayList<SPUPrint>();

        database.openTransaction();
        List<Object> listSpuLineBlock = database.getListData(true, SPBSLine.TABLE_NAME,
                new String [] {SPBSLine.XML_BLOCK, SPBSLine.XML_BPN_DATE},
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?",
                new String [] {year, companyCode, estate, crop, spbsNumber},
                SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null, SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null);
        database.closeTransaction();

        if(listSpuLineBlock.size() > 0) {
            for (int i = 0; i < listSpuLineBlock.size(); i++) {
                SPBSLine spuLineBlock = (SPBSLine) listSpuLineBlock.get(i);

                String block = spuLineBlock.getBlock();
                String bpnDate = spuLineBlock.getBpnDate();

                database.openTransaction();
                List<Object> listSpuLineQtyGoodWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.GOOD_WEIGHT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyGoodWeight = 0;
                if(listSpuLineQtyGoodWeight.size() > 0){
                    for(int k = 0; k < listSpuLineQtyGoodWeight.size(); k++){
                        SPBSLine spbsLine = (SPBSLine) listSpuLineQtyGoodWeight.get(k);

                        qtyGoodWeight = (double) (qtyGoodWeight + spbsLine.getQuantityAngkut());
                    }
                }

                database.openTransaction();
                List<Object> listSpuLineQtyBadWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.BAD_WEIGHT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyBadWeight = 0;
                if(listSpuLineQtyBadWeight.size() > 0){
                    for(int k = 0; k < listSpuLineQtyBadWeight.size(); k++){
                        SPBSLine spbsLine = (SPBSLine) listSpuLineQtyBadWeight.get(k);

                        qtyBadWeight = (double) (qtyBadWeight + spbsLine.getQuantityAngkut());
                    }
                }

                database.openTransaction();
                List<Object> listSpuLineQtyPoorWeight = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.POOR_WEIGHT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyPoorWeight = 0;
                if(listSpuLineQtyPoorWeight.size() > 0){
                    for(int k = 0; k < listSpuLineQtyPoorWeight.size(); k++){
                        SPBSLine spbsLine = (SPBSLine) listSpuLineQtyPoorWeight.get(k);

                        qtyPoorWeight = (double) (qtyPoorWeight + spbsLine.getQuantityAngkut());
                    }
                }

                double qtyTotalWeight = (double) qtyGoodWeight +  qtyBadWeight +  qtyPoorWeight;

                SPUPrint spuPrint = new SPUPrint(block, bpnDate, qtyGoodWeight, qtyBadWeight, qtyPoorWeight, qtyTotalWeight);
                listSpuPrint.add(spuPrint);
            }

            final int MAX_LINE = 8;
            if(listSpuPrint.size() > 0){
                int id = 1;
                int maxPages = 1;
                int pages = 1;
                int posStart = 0;
                int posEnd = 0;

                if(listSpuPrint.size() % MAX_LINE == 0){
                    maxPages = (int) listSpuPrint.size() / MAX_LINE;
                }else{
                    maxPages = ((int) listSpuPrint.size() / MAX_LINE) + 1;
                }

                for(int i = 0; i < listSpuPrint.size(); i++){
                    posEnd = i;

                    if((id == MAX_LINE) || (i == (listSpuPrint.size()-1))){
                        SendPrintOut(listSpuPrint, posStart, posEnd, pages, maxPages);
                        id = 1;
                        posStart = i+1;
                        pages++;
                    }

                    id++;
                }
            }
        }
    }

    private void saveSettings() {
        database.openTransaction();
        Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        Toast.makeText(getApplicationContext(),bluetooth.getName(),Toast.LENGTH_SHORT).show();
        SettingsHelper.saveBluetoothAddress(SPUActivity.this, bluetooth.getAddress());
        SettingsHelper.saveIp(SPUActivity.this, "");
        SettingsHelper.savePort(SPUActivity.this, "");
    }

    private void SendPrintOut(List<SPUPrint> listSpbsPrint, int posStart, int posEnd, int pages, int maxPages) {
        try {
            byte[] configLabel = printSPUZEBRA(listSpbsPrint, posStart,posEnd, pages, maxPages).getBytes();
            printerConnection.write(configLabel);
            DemoSleeper.sleep(1500);
            if (printerConnection instanceof BluetoothConnection) {
                DemoSleeper.sleep(500);
            }
        } catch (ConnectionException e) {
        }
    }

    private String printSPUZEBRA(List<SPUPrint> listSpuPrint, int posStart, int posEnd, int pages, int maxPages){
        String headerQrCode = getHeaderQrCode(spbsNumber, spbsDate, estate, division, licensePlate, driver, nikDriver, runningAccount, pages, maxPages);
        String lineQrCode = "";
        for(int i = posStart; i <= posEnd; i++){
            SPUPrint spuPrint = listSpuPrint.get(i);

            if( i == posStart){
                lineQrCode = getLineQrCode(i + 1, spuPrint.getBlock(), spuPrint.getBpnDate(), spuPrint.getQtyGoodWeight(), spuPrint.getQtyBadWeight(), spuPrint.getQtyPoorWeight(), spuPrint.getQtyTotalWeight());
            }else{
                lineQrCode = lineQrCode + QR_CODE_LINE_DELIMITER + getLineQrCode(i + 1, spuPrint.getBlock(), spuPrint.getBpnDate(), spuPrint.getQtyGoodWeight(), spuPrint.getQtyBadWeight(), spuPrint.getQtyPoorWeight(), spuPrint.getQtyTotalWeight());
            }
        }

        String qrType = new SharedPreferencesHandler(SPUActivity.this).getQR();
        String qrCode = "";
        if(qrType.equalsIgnoreCase("full")){
            qrCode = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
        }
        else {
            qrCode = spbsNumber + ";" + licensePlate + ";" + driver;
        }

        //Find Vendor Name -- add by Fernando 20171005
        database.openTransaction();
        Vendor vendor = (Vendor) database.getDataFirst(false, Vendor.TABLE_NAME, null,
                Vendor.XML_ESTATE + "=?" + " and " +
                        Vendor.XML_LIFNR + "=?" ,
                new String [] {estate, lifnr},  null, null, null, null);
        database.closeTransaction();

        if(vendor !=null){
            vendorName = vendor.getName();
        }

        Date date = new Date();
        String Tanggal = new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm ");
        String dateString = DateLocal.FORMAT_REPORT_VIEW2.format(date);

        String tmpHeader = "^XA" +

                "^PON^PW790^MNN^LL%d^LH0,0" + "\r\n" +

                "^FO5,50" + "\r\n" + "^A0,N,25,25" + "\r\n" +"^FB900,3,0,C,0"+ "^FD"+dateString+"^FS" + "\r\n" +
                "^FO5,100" + "\r\n" + "^A0,N,50,50" + "\r\n" +"^FB600,3,0,C,0"+ "^FDSPU Ticket^FS" + "\r\n" +

                "^FO5,180" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDSPU Number^FS" + "\r\n" +

                "^FO230,180" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,210" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDSPU Date^FS" + "\r\n" +

                "^FO230,210" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,240" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^Estate/Division^FS" + "\r\n" +

                "^FO230,240" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,270" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDClerk^FS" + "\r\n" +

                "^FO230,270" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,300" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDLicense Plate^FS" + "\r\n" +

                "^FO230,300" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,330" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDRunning Account^FS" + "\r\n" +

                "^FO230,330" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,360" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDDriver^FS" + "\r\n" +

                "^FO230,360" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,390" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDVendor^FS" + "\r\n" +

                "^FO230,390" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,420" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDPages^FS" + "\r\n" +

                "^FO230,420" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,450" + "\r\n" + "^GB750,5,5,B,0^FS" + "\r\n" +

                "^FO5,460" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDBlock^FS" + "\r\n" +

                "^FO100,460" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDHarvest Date^FS" + "\r\n" +

                "^FO250,460" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDGood(Kg)^FS" + "\r\n" +

                "^FO350,460" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDBad(Kg)^FS" + "\r\n" +

                "^FO450,460" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDPoor(Kg)^FS" + "\r\n" +

                "^FO5,490" + "\r\n" + "^GB750,5,5,B,0^FS" + "\r\n";

        int headerHeight = 520;
        String body = String.format("^LH0,%d", headerHeight);
        int heightOfOneLine = 30;
        int i = 0;
        for(int h = posStart; h <= posEnd; h++){
            SPUPrint spuPrint = listSpuPrint.get(h);

            String block = spuPrint.getBlock();
            String bpnDate =  new DateLocal(spuPrint.getBpnDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS);
            String qtyGoodWeight = addNol(spuPrint.getQtyGoodWeight(), 2);
            String qtyBadWeight = addNol(spuPrint.getQtyBadWeight(), 2);
            String qtyPoorWeight = addNol(spuPrint.getQtyPoorWeight(), 2);
            String qtyTotalWeight = addNol(spuPrint.getQtyTotalWeight(), 2);
            /*String lineItem = "^FO5,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n" + "^FO130,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS"
                    + "\r\n" + "^FO330,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n" + "^FO450,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS";*/

            String lineItem = "^FO5,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n"
                            + "^FO100,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n"
                            + "^FO250,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n"
                            + "^FO350,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n"
                            + "^FO450,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS";
            int totalHeight = i++ * heightOfOneLine;
            //body += String.format(lineItem, totalHeight, block, totalHeight, bpnDate, totalHeight, qtyGoodWeight, qtyBadWeight, qtyPoorWeight, qtyTotalWeight);
            body += String.format(lineItem, totalHeight, block, totalHeight, bpnDate, totalHeight, qtyGoodWeight, totalHeight, qtyBadWeight, totalHeight, qtyPoorWeight);
        }

        long totalBodyHeight = (listSpuPrint.size() + 1) * heightOfOneLine;
        long footerStartPosition = headerHeight + totalBodyHeight;
        String footer = String.format("^LH0,%d" + "\r\n" +
                "^FO150,20"+"\r\n"+"^BQN,2,7"+"\r\n"+"^FDQA,"+spbsNumber+";"+licensePlate+";"+driver+";"+vendorName+lifnr+"^FS"+"r\n"+
                "^XZ", footerStartPosition);
        long footerHeight = 400;
        long labelLength = headerHeight + totalBodyHeight + footerHeight;
        String spuNumber = spbsNumber;
        String spuDate = spbsDate;
        String Estate = estate + " / " + division;
        String Clerk = nikClerk + " - " + addChar(clerk, " ", 15, 1);
        String LicensePlate = licensePlate;
        String RunningAccount = runningAccount;
        String Driver = nikDriver + " - " + addChar(driver, " ", 15, 1);
        String Vendor = lifnr + " - " + addChar(vendorName, " ", 15, 1);
        String Pages = String.valueOf(pages) + " of " + String.valueOf(maxPages);
        String header = String.format(tmpHeader, labelLength, spuNumber, Tanggal,Estate,Clerk,LicensePlate,RunningAccount,Driver,Vendor,Pages);
        String wholeZplLabel = String.format("%s%s%s", header, body, footer);

        return wholeZplLabel;
    }
    // ZEBRA PRINT -- END //



    private String getHeaderQrCode(String spbsNumber, String spbsDate, String estate, String division, String licensePlate,
                                   String driver, String nikDriver, String runningAccount, int pages, int maxPages){
        String header = "";

        spbsDate = new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_QR_CODE_SPBS);

//		header = spbsNumber + "\n" +
//				 spbsDate + "\n" +
//				 estate + QR_CODE_DELIMITER + division + "\n" +
//				 nikClerk + "\n" +
//				 String.valueOf(pages) + QR_CODE_DELIMITER + String.valueOf(maxPages) + "\n";

        header = spbsNumber + QR_CODE_DELIMITER + licensePlate + QR_CODE_DELIMITER + driver + "-" + nikDriver + QR_CODE_DELIMITER +
                spbsDate + QR_CODE_DELIMITER + estate + QR_CODE_DELIMITER + division + QR_CODE_DELIMITER + runningAccount;

        return header;
    }

    private String getLineQrCode(int id, String block, String bpnDate, double qtyGoodWeight, double qtyBadWeight, double qtyPoorWeight, double qtyTotalWeight){
        String line = "";

        bpnDate = new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_QR_CODE_SPBS);

        line = String.valueOf(id) + QR_CODE_DELIMITER +
                block + QR_CODE_DELIMITER +
                bpnDate + QR_CODE_DELIMITER +
                String.valueOf(qtyGoodWeight) + QR_CODE_DELIMITER +
                String.valueOf(qtyBadWeight) + QR_CODE_DELIMITER +
                String.valueOf(qtyPoorWeight) + QR_CODE_DELIMITER +
                String.valueOf(qtyTotalWeight);

        return  line;
    }

    private String addNol(Double value, int decimalDigits){
        String valueString = String.valueOf(value);
        String temp = "";

        try{
            int dot = valueString.indexOf(".");

            if(dot > -1 ){
                String beforeDot = valueString.substring(0, dot + 1);
                String afterDot = valueString.substring(dot + 1, valueString.length());

                temp = beforeDot + addChar(afterDot, "0", decimalDigits, 1);
            }else{
                temp = valueString;
            }
        }catch(NullPointerException e){
            e.printStackTrace();

            temp = valueString;
        }

        return temp;
    }

    private String addChar(String value, String addString, int digits, int leftOrRight){
        String temp = value;

        if(value.length() < digits){
            for(int i = 0; i < (digits - value.length()); i++){
                if(leftOrRight == -1){
                    temp = addString + temp;
                }else if(leftOrRight == 1){
                    temp = temp + addString;
                }
            }
        }else{
            temp = value.substring(0, digits);
        }

        return temp;
    }
}
