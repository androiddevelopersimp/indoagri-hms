package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.bkm.BKMAbsentActivity;
import com.simp.hms.adapter.AdapterGang;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.Employee;
import com.simp.hms.model.Gang;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterEmployeeGangActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private EditText edtMasterEmployeeGangSearch;
	private ListView lsvMasterEmployeeGang;
	
	private List<Gang> listGang;
	private AdapterGang adapter;

	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	boolean isSearch = false;
	String companyCode;
	String estate;
	String division;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_employee_gang);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterEmployeeGang = (ListView) findViewById(R.id.lsvMasterEmployeeGang);
		edtMasterEmployeeGangSearch = (EditText) findViewById(R.id.edtMasterEmployeeGangSearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_gang));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtMasterEmployeeGangSearch.addTextChangedListener(this);
		lsvMasterEmployeeGang.setOnItemClickListener(this);
		
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
		companyCode = getIntent().getExtras().getString(Employee.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(Employee.XML_ESTATE, "");
		division = getIntent().getExtras().getString(Employee.XML_DIVISION, "");
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		Gang gang = (Gang) adapter.getItem(pos);
		
		if(isSearch){
			setResult(RESULT_OK, new Intent(MasterEmployeeGangActivity.this, BKMAbsentActivity.class)
				.putExtra(Gang.XML_GANG_CODE, gang.getCode()));
			finish();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<Gang>, List<Gang>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterEmployeeGangActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<Gang> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterEmployeeGangActivity.this);
			List<Gang> listTemp = new ArrayList<Gang>();
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(true, Employee.TABLE_NAME, new String [] {Employee.XML_GANG}, 
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?" + " and " +
					Employee.XML_DIVISION + "=?", 
					new String [] {companyCode, estate, division},
					Employee.XML_GANG, null, Employee.XML_GANG, null);
			database.closeTransaction();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					Employee employee = (Employee) listObject.get(i);
					
					if(!TextUtils.isEmpty(employee.getGang().trim())){
						listTemp.add(new Gang(employee.getGang()));
					}
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<Gang> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				listGang = listTemp;
				adapter = new AdapterGang(MasterEmployeeGangActivity.this, listGang, R.layout.item_gang);
			}else{
				adapter = null;
			}
			
			lsvMasterEmployeeGang.setAdapter(adapter);
		}
	}  

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
}
