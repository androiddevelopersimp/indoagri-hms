package com.simp.hms.activity.other;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.main.MainActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.handler.XmlHandler;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.MessageStatus;
import com.simp.hms.model.UserLogin;

import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ExportActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogDateListener{
	private Toolbar tbrMain;
	private TextView txtExportDate;
	private CheckBox cbxExportBPN;
	private CheckBox cbxExportTaksasi;
	private CheckBox cbxExportSPBS;
	private CheckBox cbxExportBKM;
	private CheckBox cbxExportAncakPanen;
	private CheckBox cbxExportSpta;
	private CheckBox cbxExportUsbOtg;
	private Button btnExportSubmit;
	
//	private UsbManager manager;
    private static final String ACTION_USB_PERMISSION = "com.simp.hms.USB_PERMISSION";
    private boolean isUsbOtgAvailable = false;
    private boolean isUsbOtgPermit = true;
    
    String exportDate = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_export);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtExportDate = (TextView) findViewById(R.id.txtExportDate);
		cbxExportBPN = (CheckBox) findViewById(R.id.cbxExportBPN);
		cbxExportTaksasi = (CheckBox) findViewById(R.id.cbxExportTaksasi);
		cbxExportSPBS = (CheckBox) findViewById(R.id.cbxExportSPBS);
		cbxExportBKM = (CheckBox) findViewById(R.id.cbxExportBKM);
		cbxExportAncakPanen = (CheckBox) findViewById(R.id.cbxExportAncakPanen);
//		cbxExportSpta = (CheckBox) findViewById(R.id.cbxExportSpta);
		cbxExportUsbOtg = (CheckBox) findViewById(R.id.cbxExportUsbOtg);
		btnExportSubmit = (Button) findViewById(R.id.btnExportSubmit);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		//add by adit 20161221

		cbxExportBPN.setEnabled(false);
		cbxExportTaksasi.setEnabled(false);
		cbxExportSPBS.setEnabled(false);
		cbxExportBKM.setEnabled(false);
		cbxExportAncakPanen.setEnabled(false);

		txtExportDate.setOnClickListener(this);
		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.export));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnExportSubmit.setOnClickListener(this);
		
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);  
        filter.addAction(Environment.MEDIA_MOUNTED);
        filter.addAction(Environment.MEDIA_UNMOUNTED);
        
        registerReceiver(mUsbReceiver, filter);
        
		exportDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtExportDate.setText(new DateLocal(exportDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

		//add by adit 20161221
		String role_user=GetLogonUserRole();
		if(role_user.equalsIgnoreCase("Foreman")){
			cbxExportBKM.setEnabled(true);
			cbxExportTaksasi.setEnabled(true);
			cbxExportBKM.setChecked(true);
			cbxExportTaksasi.setChecked(true);
		}
		else if (role_user.equalsIgnoreCase("Clerk")){
			cbxExportBPN.setEnabled(true);
			cbxExportSPBS.setEnabled(true);
			cbxExportBPN.setChecked(true);
			cbxExportSPBS.setChecked(true);
		}
		else if(role_user.equalsIgnoreCase("Field Assistant")){
			cbxExportAncakPanen.setEnabled(true);
			cbxExportAncakPanen.setChecked(true);
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		
		case R.id.txtExportDate:
			new DialogDate(ExportActivity.this, getResources().getString(R.string.tanggal), new Date(), R.id.txtExportDate).show();
			break;
		case R.id.btnExportSubmit:
			FolderHandler folderHandler = new FolderHandler(ExportActivity.this);
			List<MessageStatus> lstMsgStatus = new ArrayList<MessageStatus>();
			   
			boolean isUsbOtgSave = isUsbOtgAvailable && isUsbOtgPermit && cbxExportUsbOtg.isChecked();
			
//			Toast.makeText(getApplicationContext(), isUsbOtgAvailable + ":" + isUsbOtgPermit + ":" + cbxExportUsbOtg.isChecked(), Toast.LENGTH_SHORT).show();
			
			if(folderHandler.isSDCardWritable() && folderHandler.init()){
				String status = null;
				
				if(cbxExportBPN.isChecked()){
					MessageStatus messageStatus = new XmlHandler(ExportActivity.this).createXML(R.id.cbxExportBPN, status, isUsbOtgSave, exportDate);
					
					if(messageStatus != null){
						lstMsgStatus.add(messageStatus);
					}
				}  
				
				if(cbxExportTaksasi.isChecked()){
					MessageStatus messageStatus = new XmlHandler(ExportActivity.this).createXML(R.id.cbxExportTaksasi, status, isUsbOtgSave, exportDate);
					
					if(messageStatus != null){
						lstMsgStatus.add(messageStatus);
					}
				}
				
				if(cbxExportSPBS.isChecked()){
					MessageStatus messageStatus = new XmlHandler(ExportActivity.this).createXML(R.id.cbxExportSPBS, status, isUsbOtgSave, exportDate);
					
					if(messageStatus != null){
						lstMsgStatus.add(messageStatus);
					}
				}
				
				if(cbxExportBKM.isChecked()){
					MessageStatus messageStatus = new XmlHandler(ExportActivity.this).createXML(R.id.cbxExportBKM, status, isUsbOtgSave, exportDate);
					
					if(messageStatus != null){
						lstMsgStatus.add(messageStatus);
					}
				}
				
				if(cbxExportAncakPanen.isChecked()){
					MessageStatus messageStatus = new XmlHandler(ExportActivity.this).createXML(R.id.cbxExportAncakPanen, status, isUsbOtgSave, exportDate);
					
					if(messageStatus != null){
						lstMsgStatus.add(messageStatus);
					}
				}

//				if(cbxExportSpta.isChecked()){
//					MessageStatus messageStatus = new XmlHandler(ExportActivity.this).createXML(R.id.cbxExportSpta, status, isUsbOtgSave, exportDate);
//
//					if(messageStatus != null){
//						lstMsgStatus.add(messageStatus);
//					}
//				}
				
				if(lstMsgStatus.size() > 0){
					String message = "";
					
					for(int i = 0; i < lstMsgStatus.size(); i++){
						MessageStatus msgStatus = (MessageStatus) lstMsgStatus.get(i);
						
						message = message + "\n" + msgStatus.getMenu() + ": " + msgStatus.getMessage();
					}
					
					new DialogNotification(ExportActivity.this, getResources().getString(R.string.informasi),
					message, false).show();
				}
			}else{
				new DialogNotification(ExportActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.error_sd_card), false).show();
			}
			break;
		default:
			break;   
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		if(is_finish){
			finish();
		}
	}
	
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
    	 
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_ATTACHED)){
            	cbxExportUsbOtg.setChecked(true);
            	isUsbOtgAvailable = true;
            }else if(action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_DETACHED)){
            	cbxExportUsbOtg.setChecked(false);
            	isUsbOtgAvailable = false;
            }
            
            if(action.equalsIgnoreCase(ACTION_USB_PERMISSION) && isUsbOtgAvailable){
            	synchronized (this) {
                  UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                  cbxExportUsbOtg.setChecked(false);
                  isUsbOtgPermit = false;

                  if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                      if (device != null) {
                      	cbxExportUsbOtg.setChecked(true);
                      	isUsbOtgPermit = true;
                      }
                  }
              }
            }

        }
    };

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		unregisterReceiver(mUsbReceiver);
	}

	@Override
	public void onDateOK(Date date, int id) {
		exportDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

		txtExportDate.setText(new DateLocal(exportDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}

	// Add By Adit 20161221
	private String GetLogonUserRole(){
		DatabaseHandler database = new DatabaseHandler(ExportActivity.this);

		database.openTransaction();
		UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();

		String role = "";
		if(userLogin != null) {

			role=userLogin.getRoleId();
		}

		return  role;
	}
    
}
