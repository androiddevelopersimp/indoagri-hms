package com.simp.hms.activity.spbs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterSPBSReportNumber;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSReportNumber;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Utils;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SPBSReportNumberActivity extends BaseActivity implements OnItemClickListener, OnClickListener, DialogDateListener{
	private Toolbar tbrMain;
	private TextView txtSpbsReportNumberSpbsDate;
	private Button btnSpbsReportNumberSearch;
	private ListView lsvSpbsReportNumber;

	private TextView txtSpbsReportNumberTotJanjang;
	private TextView txtSpbsReportNumberTotLooseFruit;
	
	private List<SPBSReportNumber> lstSPBSReport;
	private AdapterSPBSReportNumber adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	DatabaseHandler database = new DatabaseHandler(SPBSReportNumberActivity.this);
	
	String spbsDate = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_spbs_report_number);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtSpbsReportNumberSpbsDate = (TextView) findViewById(R.id.txtSpbsReportNumberSpbsDate);
		btnSpbsReportNumberSearch = (Button) findViewById(R.id.btnSpbsReportNumberSearch);
		lsvSpbsReportNumber = (ListView) findViewById(R.id.lsvSpbsReportNumber);

		txtSpbsReportNumberTotJanjang = (TextView) findViewById(R.id.txtSpbsReportNumberTotJanjang);
		txtSpbsReportNumberTotLooseFruit = (TextView) findViewById(R.id.txtSpbsReportNumberTotLooseFruit);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spbs));
		btnActionBarright.setVisibility(View.INVISIBLE);
		txtSpbsReportNumberSpbsDate.setOnClickListener(this);
		btnSpbsReportNumberSearch.setOnClickListener(this);
		lsvSpbsReportNumber.setOnItemClickListener(this);
		
		spbsDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtSpbsReportNumberSpbsDate.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	
		adapter = new AdapterSPBSReportNumber(SPBSReportNumberActivity.this, new ArrayList<SPBSReportNumber>(), R.layout.item_spbs_report_number);
		lsvSpbsReportNumber.setAdapter(adapter);
	}  
  
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		SPBSReportNumber spbsReport = (SPBSReportNumber) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(SPBSReportNumber.TABLE_NAME, spbsReport);
		
		startActivity(new Intent(SPBSReportNumberActivity.this, SPBSReportBlockActivity.class)
				.putExtras(bundle)
				.putExtra(SPBSHeader.XML_SPBS_DATE, spbsDate));
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtSpbsReportNumberSpbsDate:
			new DialogDate(SPBSReportNumberActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtSpbsReportNumberSpbsDate).show();
			break;
		case R.id.btnSpbsReportNumberSearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;   
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSReportNumber>, List<SPBSReportNumber>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(SPBSReportNumberActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override 
		protected List<SPBSReportNumber> doInBackground(Void... voids) {
			
			String companyCode = "";
			String estate = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				nik = userLogin.getNik();
			}
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(true, SPBSHeader.TABLE_NAME, null, 
					SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
					SPBSHeader.XML_ESTATE + "=?" + " and " +
					SPBSHeader.XML_SPBS_DATE + "=?" + " and " +   
					SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
					SPBSHeader.XML_CROP + "=?", 
					new String [] {companyCode, estate, spbsDate, nik, "01"},
					null, null, SPBSHeader.XML_SPBS_NUMBER, null);
			database.closeTransaction();
			
			
//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, SPBSHeader.TABLE_NAME, null, 
//					null, null, null, null, SPBSHeader.XML_SPBS_NUMBER, null);
//			database.closeTransaction();
			
			List<SPBSReportNumber> listTemp = new ArrayList<SPBSReportNumber>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					SPBSHeader spbsHeader = (SPBSHeader) listObject.get(i);
					
					String year = spbsHeader.getYear();
					String spbsNumber = spbsHeader.getSpbsNumber();
					String driver = spbsHeader.getDriver();
					String licensePlate = spbsHeader.getLicensePlate();
					String destdesc = spbsHeader.getDestType();
					
					companyCode = spbsHeader.getCompanyCode();
					estate = spbsHeader.getEstate();
					spbsDate = spbsHeader.getSpbsDate();

					
					double qtyJanjangAngkut = getQty(year, companyCode, estate, spbsNumber, spbsDate, BPNQuantity.JANJANG_CODE);
					double qtyLooseFruitAngkut = getQty(year, companyCode, estate, spbsNumber, spbsDate, BPNQuantity.LOOSE_FRUIT_CODE);
					
					listTemp.add(new SPBSReportNumber(year, companyCode, estate, "01", spbsNumber, nik, driver, licensePlate, qtyJanjangAngkut, qtyLooseFruitAngkut, destdesc));
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<SPBSReportNumber> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			adapter = new AdapterSPBSReportNumber(SPBSReportNumberActivity.this, listTemp, R.layout.item_spbs_report_number);
			lsvSpbsReportNumber.setAdapter(adapter);
			
			int totJanjangAngkut = 0;
			double totLooseFruitAngkut = 0;
			
			if(listTemp != null && listTemp.size() > 0){
				for(int i = 0; i < listTemp.size(); i++){
					SPBSReportNumber spbsReport = (SPBSReportNumber) listTemp.get(i);
					
					if(spbsReport != null){
						totJanjangAngkut += (int) spbsReport.getQtyJanjangAngkut();
						totLooseFruitAngkut += spbsReport.getQtyLooseFruitAngkut();
					}
				}
			}
			
			txtSpbsReportNumberTotJanjang.setText(String.valueOf(totJanjangAngkut));
			txtSpbsReportNumberTotLooseFruit.setText(String.valueOf(Utils.round(totLooseFruitAngkut, 2)));
			
//			if(listTemp.size() > 0){
//				lstSPBSReport = listTemp;
//				adapter = new AdapterSPBSReportNumber(SPBSReportNumberActivity.this, lstSPBSReport, R.layout.item_spbs_report_number);
//			}else{
//				adapter = null;
//			}
//			
//			lsvSpbsReportNumber.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onDateOK(Date date, int id) {
		spbsDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtSpbsReportNumberSpbsDate.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}
	
	
	private double getQty(String year, String companyCode, String estate, String spbsNumber, String spbsDate, String achievementCode){
		double qty = 0;
		
		database.openTransaction();
		List<Object> lstQty = database.getListData(false, SPBSLine.TABLE_NAME, null, 
				SPBSLine.XML_YEAR + "=?" + " and " +
				SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
				SPBSLine.XML_ESTATE + "=?" + " and " +
				SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
				SPBSLine.XML_SPBS_DATE + "=?" + " and " +
				SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
				SPBSLine.XML_CROP + "=?", 
				new String [] {year, companyCode, estate, spbsNumber, spbsDate, achievementCode, "01"}, 
				null, null, null, null);
		database.closeTransaction();
		
		if(lstQty.size() > 0){
			for(int i = 0; i < lstQty.size(); i++){
				SPBSLine SPBSLine = (SPBSLine) lstQty.get(i);
				
				qty = qty + SPBSLine.getQuantityAngkut();
			}
		}
		
		return qty;
	}
}
