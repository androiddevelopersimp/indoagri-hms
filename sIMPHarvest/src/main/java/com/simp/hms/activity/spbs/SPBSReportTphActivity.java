package com.simp.hms.activity.spbs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterSPBSReportTph;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSReportBlock;
import com.simp.hms.model.SPBSReportTph;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SPBSReportTphActivity extends BaseActivity implements OnItemClickListener, OnClickListener, DialogDateListener, TextWatcher{
	private Toolbar tbrMain;
	private TextView edtSpbsReportTphSearch;
	private ListView lsvSpbsReportTph;
	
	private List<SPBSReportTph> lstSPBSReport;
	private AdapterSPBSReportTph adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	DatabaseHandler database = new DatabaseHandler(SPBSReportTphActivity.this);
	
	String year = "";
	String companyCode = "";
	String estate = "";
	String crop = "";
	String spbsNumber = "";
	String spbsDate = "";
	String block = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_spbs_report_tph);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		edtSpbsReportTphSearch = (TextView) findViewById(R.id.edtSpbsReportTphSearch);
		lsvSpbsReportTph = (ListView) findViewById(R.id.lsvSpbsReportTph);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spbs));
		btnActionBarright.setVisibility(View.INVISIBLE);
		edtSpbsReportTphSearch.setOnClickListener(this);
		lsvSpbsReportTph.setOnItemClickListener(this);
		
		SPBSReportBlock spbsReport = null;
		
		if(getIntent().getExtras() != null){
			spbsReport = getIntent().getExtras().getParcelable(SPBSReportBlock.TABLE_NAME);
			spbsDate = getIntent().getExtras().getString(SPBSHeader.XML_SPBS_DATE);
		}
		
		if(spbsReport != null){
			year = spbsReport.getYear();
			companyCode = spbsReport.getCompanyCode();
			estate = spbsReport.getEstate();
			crop = spbsReport.getCrop();
			spbsNumber = spbsReport.getSpbsNumber();
			block = spbsReport.getBlock();
		}
		
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
		
		edtSpbsReportTphSearch.addTextChangedListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {  
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSReportTph>, List<SPBSReportTph>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(SPBSReportTphActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override 
		protected List<SPBSReportTph> doInBackground(Void... voids) {
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(true, SPBSLine.TABLE_NAME, 
					new String [] {SPBSLine.XML_TPH, SPBSLine.XML_BPN_DATE}, 
					SPBSLine.XML_YEAR + "=?" + " and " +
					SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
					SPBSLine.XML_ESTATE + "=?" + " and " +
					SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
					SPBSLine.XML_SPBS_DATE + "=?" + " and " +   
					SPBSLine.XML_BLOCK + "=?" + " and " +
					SPBSLine.XML_CROP + "=?", 
					new String [] {year, companyCode, estate, spbsNumber, spbsDate, block, "01"},
					SPBSLine.XML_TPH + ", " + SPBSLine.XML_BPN_DATE, null, SPBSLine.XML_TPH + ", " + SPBSLine.XML_BPN_DATE, null);
			database.closeTransaction();
			
			List<SPBSReportTph> listTemp = new ArrayList<SPBSReportTph>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					SPBSLine spbsLine = (SPBSLine) listObject.get(i);
					
					String tph = spbsLine.getTph();    
					String bpnDate = spbsLine.getBpnDate();
					
					double qtyJanjang = getQty(year, companyCode, estate, spbsNumber, spbsDate, block, tph, bpnDate, BPNQuantity.JANJANG_CODE);
					double qtyLooseFruit = getQty(year, companyCode, estate, spbsNumber, spbsDate, block, tph, bpnDate, BPNQuantity.LOOSE_FRUIT_CODE);
					
					listTemp.add(new SPBSReportTph(year, companyCode, estate, "01", spbsNumber, spbsDate, block, tph, bpnDate, qtyJanjang, qtyLooseFruit));
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<SPBSReportTph> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				lstSPBSReport = listTemp;
				adapter = new AdapterSPBSReportTph(SPBSReportTphActivity.this, lstSPBSReport, R.layout.item_spbs_report_tph);
			}else{
				adapter = null;
			}
			
			lsvSpbsReportTph.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onDateOK(Date date, int id) {
		spbsDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		edtSpbsReportTphSearch.setText(spbsDate);
	}
	
	
	private double getQty(String year, String companyCode, String estate, String spbsNumber, String spbsDate, String block, String tph, String bpnDate, String achievementCode){
		double qty = 0;
		
		database.openTransaction();
		List<Object> lstQty = database.getListData(false, SPBSLine.TABLE_NAME, null, 
				SPBSLine.XML_YEAR + "=?" + " and " +
				SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
				SPBSLine.XML_ESTATE + "=?" + " and " +
				SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
				SPBSLine.XML_SPBS_DATE + "=?" + " and " +
				SPBSLine.XML_BLOCK + "=?" + " and " +
				SPBSLine.XML_TPH + "=?" + " and " +
				SPBSLine.XML_BPN_DATE + "=?" + " and " +
				SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
				SPBSLine.XML_CROP + "=?", 
				new String [] {year, companyCode, estate, spbsNumber, spbsDate, block, tph, bpnDate, achievementCode, "01"}, 
				null, null, null, null);
		database.closeTransaction();
		
		if(lstQty.size() > 0){
			for(int i = 0; i < lstQty.size(); i++){
				SPBSLine SPBSLine = (SPBSLine) lstQty.get(i);
				
				//qty = qty + SPBSLine.getQuantity();
				qty = qty + SPBSLine.getQuantityAngkut();
			}
		}
		
		return qty;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {}

	@Override
	public void afterTextChanged(Editable editable) {
		adapter.getFilter().filter(editable);
		adapter.notifyDataSetChanged();
	}
}
