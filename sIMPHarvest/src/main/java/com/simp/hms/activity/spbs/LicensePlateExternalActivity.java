package com.simp.hms.activity.spbs;


import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.Employee;
import com.simp.hms.model.RunningAccount;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class LicensePlateExternalActivity extends BaseActivity implements OnClickListener, DialogNotificationListener {
	private Toolbar tbrMain;
	private TextView txtLicensePlateExternalRunacct;
	private EditText edtLicensePlateExternalName;
	private Button btnLicensePlateExternalSubmit;

	String crop = "";
	String spbsactivity = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_license_plate_external);
		
		registerBaseActivityReceiver();

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtLicensePlateExternalRunacct = (TextView) findViewById(R.id.txtLicensePlateExternalRunacct);
		edtLicensePlateExternalName = (EditText) findViewById(R.id.edtLicensePlateExternalName);
		btnLicensePlateExternalSubmit = (Button) findViewById(R.id.btnLicensePlateExternalSubmit);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spbs_license_plate_external));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnLicensePlateExternalSubmit.setOnClickListener(this);
		
		Bundle bundle = getIntent().getExtras();
		edtLicensePlateExternalName.setText(bundle != null ? bundle.getString(RunningAccount.XML_LICENSE_PLATE, "") : "");
		crop = getIntent().getExtras().getString(BPNHeader.XML_CROP, "");
		spbsactivity = getIntent().getExtras().getString("SPBSActivity", "");
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		case R.id.btnLicensePlateExternalSubmit:
			String runAcc = txtLicensePlateExternalRunacct.getText().toString().trim();
			String licensePlate = edtLicensePlateExternalName.getText().toString().trim();
			
			if(!TextUtils.isEmpty(runAcc) && !TextUtils.isEmpty(licensePlate)){
				if(crop.equalsIgnoreCase("01")) {
					if(spbsactivity.equalsIgnoreCase("SPBSActivity")) {
						Intent intent = new Intent(LicensePlateExternalActivity.this, SPBSActivity.class);
						intent.putExtra(RunningAccount.XML_RUNNING_ACCOUNT, runAcc);
						intent.putExtra(RunningAccount.XML_LICENSE_PLATE, licensePlate);

						setResult(RESULT_OK, intent);
						finish();
					}else if(spbsactivity.equalsIgnoreCase("SPBSDatecsActivity")){
						Intent intent = new Intent(LicensePlateExternalActivity.this, SPBSDatecsActivity.class);
						intent.putExtra(RunningAccount.XML_RUNNING_ACCOUNT, runAcc);
						intent.putExtra(RunningAccount.XML_LICENSE_PLATE, licensePlate);

						setResult(RESULT_OK, intent);
						finish();
					}
				}else if(crop.equalsIgnoreCase("04")){
					Intent intent = new Intent(LicensePlateExternalActivity.this, SPUActivity.class);
					intent.putExtra(RunningAccount.XML_RUNNING_ACCOUNT, runAcc);
					intent.putExtra(RunningAccount.XML_LICENSE_PLATE, licensePlate);

					setResult(RESULT_OK, intent);
					finish();
				}
			}else{
				new DialogNotification(LicensePlateExternalActivity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.Invalid_data), false).show();;
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onOK(boolean is_finish) {
	
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		unRegisterBaseActivityReceiver();
		animOnFinish();
	}

	
}
