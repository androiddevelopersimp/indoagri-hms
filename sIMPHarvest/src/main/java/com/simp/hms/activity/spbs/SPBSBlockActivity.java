package com.simp.hms.activity.spbs;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.anim;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.adapter.AdapterFragment;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.fragment.SPBSBlockFragment;
import com.simp.hms.model.FragmentItems;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.service.GPSService;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class SPBSBlockActivity extends FragmentActivity implements OnClickListener, OnPageChangeListener {
	private Toolbar tbrMain;
	private ImageButton btnSpbsTphBlockPrev;
	private TextView txtSpbsTphBlock;
	private ImageButton btnSpbsTphBlockNext;
	private ViewPager vprSpbsThpBlock;
	
	List<FragmentItems> listFragmentItems;
	AdapterFragment adapter;
	DatabaseHandler database = new DatabaseHandler(SPBSBlockActivity.this);
	GPSService gps;
	
	String imei = "";
	String year = "";
	String companyCode = "";
	String estate = "";
	String division = "";
	String spbsNumber = "";
	String spbsDate = "";
	String block = "";
	String crop = "";
	String clerk = "";
	int status = 0;
	ArrayList<String> listBlock = new ArrayList<String>();
	
	int selectedIndex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

		setContentView(R.layout.activity_spbs_block);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		btnSpbsTphBlockPrev = (ImageButton) findViewById(R.id.btnSpbsTphBlockPrev);
		txtSpbsTphBlock = (TextView) findViewById(R.id.txtSpbsTphBlock);
		btnSpbsTphBlockNext = (ImageButton) findViewById(R.id.btnSpbsTphBlockNext);
		vprSpbsThpBlock = (ViewPager) findViewById(R.id.vprSpbsThpBlock);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spbs));
		btnActionBarRight.setVisibility(View.INVISIBLE);   
		btnSpbsTphBlockPrev.setOnClickListener(this);
		btnSpbsTphBlockNext.setOnClickListener(this);
		vprSpbsThpBlock.setOnPageChangeListener(this);
		
//		vprSpbsThpBlock.setOffscreenPageLimit(Integer.MAX_VALUE);
		
		imei = getIntent().getExtras().getString(SPBSHeader.XML_IMEI, "");
		year = getIntent().getExtras().getString(SPBSHeader.XML_YEAR, "");
		companyCode = getIntent().getExtras().getString(SPBSHeader.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(SPBSHeader.XML_ESTATE, "");
		division = getIntent().getExtras().getString(SPBSHeader.XML_DIVISION, "");
		spbsDate = getIntent().getExtras().getString(SPBSHeader.XML_SPBS_DATE, "");
		spbsNumber = getIntent().getExtras().getString(SPBSHeader.XML_SPBS_NUMBER);
		block = getIntent().getExtras().getString(SPBSLine.XML_BLOCK, "");
		crop = getIntent().getExtras().getString(SPBSHeader.XML_CROP, "");
		clerk = getIntent().getExtras().getString(SPBSHeader.XML_CLERK, "");
		status = getIntent().getExtras().getInt(SPBSHeader.XML_STATUS, 0);
		listBlock = getIntent().getExtras().getStringArrayList(SPBSHeader.XML_BLOCKS);
	
//		gps = new GPSService(getApplicationContext());
//		GPSTriggerService.spbsBlockActivity = this;
		
		getData();
	}
	
	private void getData(){
		listFragmentItems = new ArrayList<FragmentItems>();
		
		for(int i = 0; i < listBlock.size(); i++){
			if(block.equalsIgnoreCase(listBlock.get(i))){
				selectedIndex = i;
			}
			
			Bundle bundle = new Bundle();
			bundle.putString(SPBSLine.XML_IMEI, imei);
			bundle.putString(SPBSLine.XML_YEAR, year);
			bundle.putString(SPBSLine.XML_COMPANY_CODE, companyCode);
			bundle.putString(SPBSLine.XML_ESTATE, estate);
			bundle.putString(SPBSLine.XML_SPBS_NUMBER, spbsNumber);
			bundle.putString(SPBSLine.XML_SPBS_DATE, spbsDate);
			bundle.putString(SPBSLine.XML_BLOCK, listBlock.get(i));
			bundle.putString(SPBSLine.XML_CROP, crop);
			bundle.putString(SPBSHeader.XML_CLERK, clerk);
			bundle.putInt(SPBSLine.XML_STATUS, status);
			
			SPBSBlockFragment spbsBlockFragment = new SPBSBlockFragment();
			spbsBlockFragment.setArguments(bundle);
			
			listFragmentItems.add(new FragmentItems(listBlock.get(i), spbsBlockFragment));
		}
		
		adapter = new AdapterFragment(getSupportFragmentManager());
		adapter.setListFragment(listFragmentItems);
		
		txtSpbsTphBlock.setText(block);
		vprSpbsThpBlock.setAdapter(adapter);
		vprSpbsThpBlock.setCurrentItem(selectedIndex);
		
		setVisibleButtonPage();
	}
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnSpbsTphBlockPrev:
			if(selectedIndex > 0){
				selectedIndex--;
				vprSpbsThpBlock.setCurrentItem(selectedIndex);
			}
			
			setVisibleButtonPage();
			break;
		case R.id.btnSpbsTphBlockNext:
			if(selectedIndex < listBlock.size() - 1){
				selectedIndex++;
				vprSpbsThpBlock.setCurrentItem(selectedIndex);
			}
			
			setVisibleButtonPage();
			break;
		default:
			break;
		}
	}
  
	@Override
	public void onPageScrollStateChanged(int pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int pos, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int pos) {
		selectedIndex = pos;
		String block = listBlock.get(pos);
		txtSpbsTphBlock.setText(block);
		
		setVisibleButtonPage();
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_OK);
		finish();
	}

	private void setVisibleButtonPage(){
		if(selectedIndex == 0){
			btnSpbsTphBlockPrev.setVisibility(View.INVISIBLE);
		}else{
			btnSpbsTphBlockPrev.setVisibility(View.VISIBLE);
		}
		
		if(selectedIndex == (listBlock.size() - 1)){
			btnSpbsTphBlockNext.setVisibility(View.INVISIBLE);
		}else{
			btnSpbsTphBlockNext.setVisibility(View.VISIBLE);
		}
	}
}

