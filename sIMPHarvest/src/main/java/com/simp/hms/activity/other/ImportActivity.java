package com.simp.hms.activity.other;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.FileXMLHandler;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.handler.XmlHandler;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.FileXML;
import com.simp.hms.model.MessageStatus;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.TaksasiHeader;

import android.app.ActionBar;
import android.database.sqlite.SQLiteException;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

public class ImportActivity extends BaseActivity implements OnClickListener, DialogNotificationListener{
	private Toolbar tbrMain;
	private CheckBox cbxImportBPN;
	private CheckBox cbxImportTaksasi;
	private CheckBox cbxImportSPBS;
	private CheckBox cbxImportBKM;
	private Button btnImportSubmit;
	
	DatabaseHandler database = new DatabaseHandler(ImportActivity.this);
	ReadDataAsynctask readDataAsync;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_import);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		cbxImportBPN = (CheckBox) findViewById(R.id.cbxImportBPN);
		cbxImportTaksasi = (CheckBox) findViewById(R.id.cbxImportTaksasi);
		cbxImportSPBS = (CheckBox) findViewById(R.id.cbxImportSPBS);
		cbxImportBKM = (CheckBox) findViewById(R.id.cbxImportBKM);
		btnImportSubmit = (Button) findViewById(R.id.btnImportSubmit);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.import_report));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnImportSubmit.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnImportSubmit:
			FolderHandler folderHandler = new FolderHandler(ImportActivity.this);
			
			if(folderHandler.isSDCardWritable() && folderHandler.init()){
				
				readDataAsync = new ReadDataAsynctask(folderHandler);
				readDataAsync.execute();

			}else{
				new DialogNotification(ImportActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.error_sd_card), false).show();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		if(is_finish){
			finish();
		}
	}
	
	private class ReadDataAsynctask extends AsyncTask<Void, Void, MessageStatus>{
		FolderHandler folderHandler;
		DialogProgress dialogProgress;
		
		public ReadDataAsynctask(FolderHandler folderHandler) {
			this.folderHandler = folderHandler;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(ImportActivity.this, getResources().getString(R.string.wait));
				dialogProgress.show();
			}
		}
		
		@Override
		protected MessageStatus doInBackground(Void... voids) {
			FileXMLHandler fileRestoreHandler = new FileXMLHandler(ImportActivity.this);
			List<FileXML> lstRestore;
			MessageStatus msgStatus = null;
			
			lstRestore = fileRestoreHandler.getFiles(folderHandler.getFileRestoreNew(), folderHandler.getFileRestoreBackup(), ".xml");
			
			if(lstRestore.size() > 0){
				msgStatus = ParsingXML(lstRestore);
			}
			
			lstRestore = fileRestoreHandler.getFiles(folderHandler.getFileRestoreBackup(), null, ".xml");
			
			for(int i = 0; i < lstRestore.size(); i++){
				File file = new File(lstRestore.get(i).getFileNew());
				
				if(file.exists()){
					long lastModified = file.lastModified();
					long currentDate = new Date().getTime();
					
					long timeDiff = (currentDate - lastModified)/(24*60*60*1000);
					
					if(timeDiff > 30){
						file.delete(); 
					}
				}
			}
			
			return msgStatus;   
		}  
 
		@Override
		protected void onPostExecute(MessageStatus msgStatus) {
			super.onPostExecute(msgStatus);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null; 
			}
			
			if(msgStatus != null){
				new DialogNotification(ImportActivity.this, getResources().getString(R.string.informasi),
						msgStatus.getMessage(), false).show();
			}
			
		}
	}   

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(readDataAsync != null && readDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			readDataAsync.cancel(true);
		}
	}
	
	public MessageStatus ParsingXML(List<FileXML> lstFile){
		MessageStatus msgStatus = new MessageStatus();
		
		for(int i = 0; i < lstFile.size(); i++){
			XmlHandler xmlHandler = new XmlHandler(ImportActivity.this);
			
			StringBuilder textFile = new StringBuilder();
			
			try{
				BufferedReader br = new BufferedReader(new FileReader(new File(lstFile.get(i).getFileNew())));
				String line;
				
				while((line = br.readLine()) != null){
					textFile.append(line);
				}

				br.close();
			}catch (IOException e){  
				e.printStackTrace();
			}

			String xml = textFile.toString();
			boolean process = false;
			
			if(cbxImportBPN.isChecked()){
				if(xml.contains(BPNHeader.XML_DOCUMENT_RESTORE)){
					process = true;
				}
			}
			
			if(cbxImportTaksasi.isChecked()){
				if(xml.contains(TaksasiHeader.XML_DOCUMENT_RESTORE)){
					process = true;
				}
			}
			
			if(cbxImportSPBS.isChecked()){
				if(xml.contains(SPBSHeader.XML_DOCUMENT_RESTORE)){
					process = true;
				}
			}
			
			if(cbxImportBKM.isChecked()){
				if(xml.contains(BKMHeader.XML_DOCUMENT_RESTORE)){
					process = true;
				}
			}
			
			if(process){
				try {
					InputStream is = new BOMInputStream(new ByteArrayInputStream(xml.getBytes("UTF-8")),false, ByteOrderMark.UTF_8 );
					
					try{
						MessageStatus msg = xmlHandler.importXML(is);
	
						if(msg.isStatus()){	
							File from = new File(lstFile.get(i).getFileNew());
							
							if(from.exists()){
								File to = new File(lstFile.get(i).getPathBackup() + File.separator +  from.getName());
								from.renameTo(to);
								
								MediaScannerConnection.scanFile(ImportActivity.this, new String[] { from.getAbsolutePath() }, null, null);
								MediaScannerConnection.scanFile(ImportActivity.this, new String[] { to.getAbsolutePath() }, null, null);
							}
						}
						
						if(TextUtils.isEmpty(msgStatus.getMessage())){
							msgStatus.setMessage(msg.getMenu() + ": " + msg.getMessage());
						}else{
							msgStatus.setMessage(msgStatus.getMessage() + "\n" + msg.getMenu() + ": " + msg.getMessage());
						} 
					}catch(SQLiteException e){  
						e.printStackTrace();
						msgStatus.setMessage(e.getMessage());  
					}
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					msgStatus.setMessage(e.getMessage());
				}
			}  
		}
		
		return msgStatus;
	}
}
