package com.simp.hms.activity.other;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.bpn.BPNActivity;
import com.simp.hms.activity.master.MasterBlockHdrcActivity;
import com.simp.hms.adapter.AdapterBlockPlanning;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BLKPLT;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BlockPlanningActivity extends BaseActivity implements OnItemClickListener, OnClickListener, 
TextWatcher, DialogConfirmListener, DialogNotificationListener{
	private Toolbar tbrMain;
	private EditText edtBlockPlanningSearch;
	private ListView lsvBlockPlanning;
	private Button btnBlockPlanningAddBlock;
	
	private List<BlockPlanning> lstBlockPlanning = new ArrayList<BlockPlanning>();
	private AdapterBlockPlanning adapter;
	
	DatabaseHandler database = new DatabaseHandler(BlockPlanningActivity.this);
	
	String companyCode = "";
	String estate = "";
	String division = "";
	String nik = "";
	String croptype = "";
	
	boolean isReadOnly = false;
	
	private final int MST_BLOCK_PLANNING = 101;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		registerBaseActivityReceiver();
		animOnStart();    

		setContentView(R.layout.activity_block_planning);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		edtBlockPlanningSearch = (EditText) findViewById(R.id.edtBlockPlanningSearch);
		lsvBlockPlanning = (ListView) findViewById(R.id.lsvBlockPlanning);
		btnBlockPlanningAddBlock = (Button) findViewById(R.id.btnBlockPlanningAddBlock);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.block_planning_block_planning));
		btnActionBarRight.setText(getResources().getString(R.string.save));
		btnActionBarRight.setVisibility(View.VISIBLE);
		btnActionBarRight.setOnClickListener(this);
		edtBlockPlanningSearch.addTextChangedListener(this);
		lsvBlockPlanning.setOnItemClickListener(this);
		btnBlockPlanningAddBlock.setOnClickListener(this);

		Bundle bundle = getIntent().getExtras();

		if(bundle != null){
			isReadOnly = bundle.getBoolean("readOnly", false);
			croptype = getIntent().getExtras().getString("croptype");
		}

		adapter = new AdapterBlockPlanning(BlockPlanningActivity.this, lstBlockPlanning, R.layout.item_block_planning, isReadOnly);
		lsvBlockPlanning.setAdapter(adapter);

		Init();
		loadData();

		btnActionBarRight.setVisibility(isReadOnly ? View.GONE : View.VISIBLE);
		btnBlockPlanningAddBlock.setVisibility(isReadOnly ? View.GONE : View.VISIBLE);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		if(isReadOnly){
			BlockPlanning blockPlanning = (BlockPlanning) adapter.getItem(pos);
			
			setResult(RESULT_OK, new Intent(BlockPlanningActivity.this, BPNActivity.class)
			.putExtra(BlockPlanning.XML_BLOCK, blockPlanning.getBlock()));
			finish();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:  
			if(adapter.getCount() > 0){
				try{
					database.openTransaction();
					
					database.deleteData(BlockPlanning.TABLE_NAME, 
							BlockPlanning.XML_CREATED_BY + " = ? COLLATE NOCASE",
							new String [] {nik});
					
					for(int i = 0; i < adapter.getCount(); i++){
						BlockPlanning blockPlanning = (BlockPlanning) adapter.getItem(i);
						database.setData(blockPlanning);
					}
					
					database.commitTransaction();
					
					new DialogNotification(BlockPlanningActivity.this, getResources().getString(R.string.informasi),
							getResources().getString(R.string.save_successed), false).show();
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					database.closeTransaction();
				}
			}
			break;
		case R.id.btnBlockPlanningAddBlock:
			startActivityForResult(new Intent(BlockPlanningActivity.this, MasterBlockHdrcActivity.class)
			.putExtra(BlockHdrc.XML_COMPANY_CODE, companyCode)
			.putExtra(BlockHdrc.XML_ESTATE, estate)
			.putExtra(BlockHdrc.XML_DIVISION, division)
			.putExtra(Constanta.SEARCH, true),
			MST_BLOCK_PLANNING);
			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
	
	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	
		if(resultCode == RESULT_OK){
			if(requestCode == MST_BLOCK_PLANNING){
				String block = data.getExtras().getString(BlockPlanning.XML_BLOCK);
				String createdDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_ONLY);
				String createdBy = nik;
				
				BlockPlanning blockPlanning = new BlockPlanning(block, createdDate, createdBy);
				adapter.addData(blockPlanning);
			}  
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		adapter.deleteData(id);
	}
	
	private void Init(){
		database.openTransaction();
		UserLogin userLogin = (UserLogin) database.getDataFirst(false, 
				UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();
		
		if(userLogin != null){
			companyCode = userLogin.getCompanyCode();
			estate = userLogin.getEstate();
			division = userLogin.getDivision();   
			nik = userLogin.getNik();
		}
	}
	
	private void loadData(){
		String createdDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_ONLY);
		
		try{
			database.openTransaction();
			
			database.deleteData(BlockPlanning.TABLE_NAME, 
					BlockPlanning.XML_CREATED_DATE + " != ? COLLATE NOCASE", 
					new String [] {createdDate});
			
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
		}finally{
			database.closeTransaction();
		}
		
		database.openTransaction();
		List<Object> lstObject = database.getListData(false, BlockPlanning.TABLE_NAME, null, 
				BlockPlanning.XML_CREATED_BY + " = ? COLLATE NOCASE " + " and " +
				BlockPlanning.XML_CREATED_DATE + " = ? COLLATE NOCASE",
				new String [] {nik, createdDate}, 
				null, null, BlockPlanning.XML_BLOCK, null);
		database.closeTransaction();
		
		if(lstObject != null && lstObject.size() > 0){
			for (Object object : lstObject) {
				BlockPlanning blockPlanning = (BlockPlanning) object;

				if(croptype == null || croptype.equalsIgnoreCase("")) {
					database.openTransaction();
					BLKPLT blkplt = (BLKPLT) database.getDataFirst(false, BLKPLT.TABLE_NAME, null,
							BLKPLT.XML_BLOCK + "=?",
							new String[]{blockPlanning.getBlock()},
							null, null, null, null);
					database.closeTransaction();

					if (blkplt != null)
						adapter.addData(blockPlanning);
				}else{
					database.openTransaction();
					BLKPLT blkplt = (BLKPLT) database.getDataFirst(false, BLKPLT.TABLE_NAME, null,
							BLKPLT.XML_BLOCK + "=?" + " and " +
									BLKPLT.XML_CROP_TYPE + "=?",
							new String[]{blockPlanning.getBlock(), croptype},
							null, null, null, null);
					database.closeTransaction();

					if (blkplt != null)
						adapter.addData(blockPlanning);
				}
			}
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
		
	}
}
