package com.simp.hms.activity.spu;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.spbs.SPBSBlockActivity;
import com.simp.hms.adapter.AdapterFragment;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.fragment.SPBSBlockFragment;
import com.simp.hms.fragment.SPUBlockFragmentPODS;
import com.simp.hms.model.FragmentItems;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.service.GPSService;

import java.util.ArrayList;
import java.util.List;

public class SPUBlockActivity extends FragmentActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private Toolbar tbrMain;
    private ImageButton btnSpuTphBlockPrev;
    private TextView txtSpuTphBlock;
    private ImageButton btnSpuTphBlockNext;
    private ViewPager vprSpuThpBlock;

    List<FragmentItems> listFragmentItems;
    AdapterFragment adapter;
    DatabaseHandler database = new DatabaseHandler(SPUBlockActivity.this);
    GPSService gps;

    String imei = "";
    String year = "";
    String companyCode = "";
    String estate = "";
    String division = "";
    String spbsNumber = "";
    String spbsDate = "";
    String block = "";
    String crop = "";
    String clerk = "";
    int status = 0;
    ArrayList<String> listBlock = new ArrayList<String>();

    int selectedIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spu_block);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        btnSpuTphBlockPrev = (ImageButton) findViewById(R.id.btnSpuTphBlockPrev);
        txtSpuTphBlock = (TextView) findViewById(R.id.txtSpuTphBlock);
        btnSpuTphBlockNext = (ImageButton) findViewById(R.id.btnSpuTphBlockNext);
        vprSpuThpBlock = (ViewPager) findViewById(R.id.vprSpuThpBlock);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText("SPU");
        btnActionBarRight.setVisibility(View.INVISIBLE);
        btnSpuTphBlockPrev.setOnClickListener(this);
        btnSpuTphBlockNext.setOnClickListener(this);
        vprSpuThpBlock.setOnPageChangeListener(this);

        imei = getIntent().getExtras().getString(SPBSHeader.XML_IMEI, "");
        year = getIntent().getExtras().getString(SPBSHeader.XML_YEAR, "");
        companyCode = getIntent().getExtras().getString(SPBSHeader.XML_COMPANY_CODE, "");
        estate = getIntent().getExtras().getString(SPBSHeader.XML_ESTATE, "");
        division = getIntent().getExtras().getString(SPBSHeader.XML_DIVISION, "");
        spbsDate = getIntent().getExtras().getString(SPBSHeader.XML_SPBS_DATE, "");
        spbsNumber = getIntent().getExtras().getString(SPBSHeader.XML_SPBS_NUMBER);
        block = getIntent().getExtras().getString(SPBSLine.XML_BLOCK, "");
        crop = getIntent().getExtras().getString(SPBSHeader.XML_CROP, "");
        clerk = getIntent().getExtras().getString(SPBSHeader.XML_CLERK, "");
        status = getIntent().getExtras().getInt(SPBSHeader.XML_STATUS, 0);
        listBlock = getIntent().getExtras().getStringArrayList(SPBSHeader.XML_BLOCKS);

        getData();
    }

    private void getData(){
        listFragmentItems = new ArrayList<FragmentItems>();

        for(int i = 0; i < listBlock.size(); i++){
            if(block.equalsIgnoreCase(listBlock.get(i))){
                selectedIndex = i;
            }

            Bundle bundle = new Bundle();
            bundle.putString(SPBSLine.XML_IMEI, imei);
            bundle.putString(SPBSLine.XML_YEAR, year);
            bundle.putString(SPBSLine.XML_COMPANY_CODE, companyCode);
            bundle.putString(SPBSLine.XML_ESTATE, estate);
            bundle.putString(SPBSLine.XML_SPBS_NUMBER, spbsNumber);
            bundle.putString(SPBSLine.XML_SPBS_DATE, spbsDate);
            bundle.putString(SPBSLine.XML_BLOCK, listBlock.get(i));
            bundle.putString(SPBSLine.XML_CROP, crop);
            bundle.putString(SPBSHeader.XML_CLERK, clerk);
            bundle.putInt(SPBSLine.XML_STATUS, status);

            SPUBlockFragmentPODS spuBlockFragment = new SPUBlockFragmentPODS();
            spuBlockFragment.setArguments(bundle);

            listFragmentItems.add(new FragmentItems(listBlock.get(i), spuBlockFragment));
        }

        adapter = new AdapterFragment(getSupportFragmentManager());
        adapter.setListFragment(listFragmentItems);

        txtSpuTphBlock.setText(block);
        vprSpuThpBlock.setAdapter(adapter);
        vprSpuThpBlock.setCurrentItem(selectedIndex);

        setVisibleButtonPage();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSpbsTphBlockPrev:
                if(selectedIndex > 0){
                    selectedIndex--;
                    vprSpuThpBlock.setCurrentItem(selectedIndex);
                }

                setVisibleButtonPage();
                break;
            case R.id.btnSpbsTphBlockNext:
                if(selectedIndex < listBlock.size() - 1){
                    selectedIndex++;
                    vprSpuThpBlock.setCurrentItem(selectedIndex);
                }

                setVisibleButtonPage();
                break;
            default:
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int pos) {
        selectedIndex = pos;
        String block = listBlock.get(pos);
        txtSpuTphBlock.setText(block);

        setVisibleButtonPage();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    private void setVisibleButtonPage(){
        if(selectedIndex == 0){
            btnSpuTphBlockPrev.setVisibility(View.INVISIBLE);
        }else{
            btnSpuTphBlockPrev.setVisibility(View.VISIBLE);
        }

        if(selectedIndex == (listBlock.size() - 1)){
            btnSpuTphBlockNext.setVisibility(View.INVISIBLE);
        }else{
            btnSpuTphBlockNext.setVisibility(View.VISIBLE);
        }
    }
}
