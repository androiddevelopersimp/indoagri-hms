package com.simp.hms.activity.spbs;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class LoadingActivity extends BaseActivity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		animOnStart();
		
		setTheme(R.style.AppBaseTheme);
		setContentView(R.layout.activity_angkut);
		
		ActionBar act_bar = this.getActionBar();
		LayoutInflater lyt_inflater = LayoutInflater.from(LoadingActivity.this);
		
		act_bar.setDisplayShowHomeEnabled(false);
		act_bar.setDisplayShowTitleEnabled(false);
		
		View viw_action_bar = lyt_inflater.inflate(R.layout.action_bar, null);
		
		ImageButton btn_action_bar_left = (ImageButton) viw_action_bar.findViewById(R.id.btnActionBarLeft);
		TextView txt_action_bar_title = (TextView) viw_action_bar.findViewById(R.id.txtActionBarTitle);
		TextView btn_action_bar_right = (TextView) viw_action_bar.findViewById(R.id.btnActionBarRight);
		
		act_bar.setCustomView(viw_action_bar);
		act_bar.setDisplayShowCustomEnabled(true);
		
		btn_action_bar_left.setOnClickListener(this);
		txt_action_bar_title.setText(getResources().getString(R.string.spbs));
		btn_action_bar_right.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:
			
			break;
		default:
			break;
		}	
	}
}
