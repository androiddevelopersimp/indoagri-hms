package com.simp.hms.activity.bpn;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.other.BlockPlanningActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.AbsoluteEditTextWatcher;
import com.simp.hms.handler.DecimalDigitsInputFilter;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.Employee;
import com.simp.hms.model.ForemanActive;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BPNCocoaActivity extends BaseActivity implements View.OnClickListener, DialogConfirmListener , DialogNotificationListener, DialogDateListener, View.OnFocusChangeListener {
    private Toolbar tbrMain;
    private LinearLayout lytHarvestBookRoot;
    private TextView txtHarvestBookHarvestDate;
    private TextView txtHarvestBookForeman;
    private TextView txtHarvestBookClerk;
    private TextView txtHarvestBookBlock;
    private TextView txtHarvestBookCrop;
    private TextView txtHarvestBookHarvester;
    private EditText edtHarvestBookTph;

    private EditText edtHarvestBookPod;
    private EditText edtHarvestBookGoodQty;
    private EditText edtHarvestBookGoodWeight;
    private EditText edtHarvestBookBadQty;
    private EditText edtHarvestBookBadWeight;
    private EditText edtHarvestBookPoorQty;
    private EditText edtHarvestBookPoorWeight;

    private TextView txtHarvestBookGpsKoordinat;
    private EditText edtHarvestBookBlockDummy;

    private HashMap<String, BPNQuality> mapBPNQuality = new HashMap<String, BPNQuality>();

    GPSService gps;
    GpsHandler gpsHandler;

    private final int MST_FOREMAN = 101;
    private final int MST_CLERK = 102;
    private final int MST_HARVESTER = 103;
    private final int MST_BLOCK = 104;
    private final int REQUEST_CAMERA = 105;
    private final int MST_GANG = 106;

    String bpnId = "";
    String imei = "";
    String companyCode = "";
    String estate = "";
    String bpnDate = "";
    String division = "";
    String gang = "";
    String block = "";
    String tph = "";
    String nikHarvester = "";
    String harvester = "";
    String nikForeman = "";
    String foreman = "";
    String nikClerk = "";
    String clerk = "";
    String crop = "04";

    int pod = 0;

    String goodQtyCode = BPNQuantity.GOOD_QTY_CODE;
    int goodQty = 0;
    String goodWeightCode = BPNQuantity.GOOD_WEIGHT_CODE;
    int goodWeight = 0;

    String badQtyCode = BPNQuantity.BAD_QTY_CODE;
    int badQty = 0;
    String badWeightCode = BPNQuantity.BAD_WEIGHT_CODE;
    int badWeight = 0;

    String poorQtyCode = BPNQuantity.POOR_QTY_CODE;
    int poorQty = 0;
    String poorWeightCode = BPNQuantity.POOR_WEIGHT_CODE;
    int poorWeight = 0;

    String gpsKoordinat = "0.0:0.0";
    String photo = "";

    Bitmap bmp = null;
    int status = 0;
    String spbsNumber = "";
    double latitude = 0.0;
    double longitude = 0.0;
    int useGerdang = 0;

    boolean init = false;
    boolean isEdit = false;
    BPNHeader bpnHeader = null;

    DatabaseHandler database = new DatabaseHandler(BPNCocoaActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //animOnStart();
        setContentView(R.layout.activity_bpn_cocoa);

        tbrMain = (Toolbar)findViewById(R.id.tbrMain);
        lytHarvestBookRoot = (LinearLayout) findViewById(R.id.lytHarvestBookRoot);
        txtHarvestBookHarvestDate = (TextView)findViewById(R.id.txtHarvestBookHarvestDate);
        txtHarvestBookForeman = (TextView)findViewById(R.id.txtHarvestBookForeman);
        txtHarvestBookClerk = (TextView)findViewById(R.id.txtHarvestBookClerk);
        txtHarvestBookBlock = (TextView)findViewById(R.id.txtHarvestBookBlock);
        txtHarvestBookCrop = (TextView)findViewById(R.id.txtHarvestBookCrop);
        txtHarvestBookHarvester = (TextView)findViewById(R.id.txtHarvestBookHarvester);
        edtHarvestBookTph = (EditText)findViewById(R.id.edtHarvestBookTph);

        edtHarvestBookPod = (EditText)findViewById(R.id.edtHarvestBookPod);
        edtHarvestBookGoodQty = (EditText)findViewById(R.id.edtHarvestBookGoodQty);
        edtHarvestBookGoodWeight = (EditText)findViewById(R.id.edtHarvestBookGoodWeight);
        edtHarvestBookBadQty = (EditText)findViewById(R.id.edtHarvestBookBadQty);
        edtHarvestBookBadWeight = (EditText)findViewById(R.id.edtHarvestBookBadWeight);
        edtHarvestBookPoorQty = (EditText)findViewById(R.id.edtHarvestBookPoorQty);
        edtHarvestBookPoorWeight = (EditText)findViewById(R.id.edtHarvestBookPoorWeight);

        edtHarvestBookBadWeight.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
        edtHarvestBookBadWeight.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
        edtHarvestBookPoorWeight.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
        txtHarvestBookGpsKoordinat = (TextView) findViewById(R.id.txtHarvestBookGpsKoordinat);
        edtHarvestBookBlockDummy = (EditText) findViewById(R.id.edtHarvestBookBlockDummy);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText("BPN Cocoa");
        btnActionBarRight.setText(getResources().getString(R.string.save));
        btnActionBarRight.setOnClickListener(this);

        txtHarvestBookHarvestDate.setOnClickListener(this);
        txtHarvestBookForeman.setOnClickListener(this);
        txtHarvestBookBlock.setOnClickListener(this);
        txtHarvestBookHarvester.setOnClickListener(this);
        edtHarvestBookTph.setOnFocusChangeListener(this);

        edtHarvestBookPod.setOnFocusChangeListener(this);
        edtHarvestBookGoodWeight.setOnFocusChangeListener(this);
        edtHarvestBookBadWeight.setOnFocusChangeListener(this);
        edtHarvestBookPoorWeight.setOnFocusChangeListener(this);

        edtHarvestBookPod.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(0)});
        edtHarvestBookGoodWeight.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(0)});
        edtHarvestBookBadWeight.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(0)});
        edtHarvestBookPoorWeight.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(0)});

        gps = new GPSService(BPNCocoaActivity.this);
        gpsHandler = new GpsHandler(BPNCocoaActivity.this);

        GPSTriggerService.bpnCocoaActivity = this;

        gpsHandler.startGPS();

        if(getIntent().getExtras() != null){
            bpnHeader = (BPNHeader) getIntent().getParcelableExtra(BPNHeader.TABLE_NAME);
        }

        if(bpnHeader !=null){
            isEdit = true;

            bpnId = bpnHeader.getBpnId();
            imei = bpnHeader.getImei();
            companyCode = bpnHeader.getCompanyCode();
            estate = bpnHeader.getEstate();
            bpnDate = bpnHeader.getBpnDate();
            division = bpnHeader.getDivision();
            gang = bpnHeader.getGang();
            block = bpnHeader.getLocation();
            tph = bpnHeader.getTph();
            nikHarvester = bpnHeader.getNikHarvester();
            harvester = bpnHeader.getHarvester();
            nikForeman = bpnHeader.getNikForeman();
            foreman = bpnHeader.getForeman();
            nikClerk = bpnHeader.getNikClerk();
            clerk = bpnHeader.getClerk();
            crop = bpnHeader.getCrop();
            gpsKoordinat = bpnHeader.getGpsKoordinat();
            photo = bpnHeader.getPhoto();
            status = bpnHeader.getStatus();
            spbsNumber = bpnHeader.getSpbsNumber();
            useGerdang = (bpnHeader.isUseGerdang() == 0) ? 0: 1;

            database.openTransaction();
            List<Object> listObjQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                    BPNQuantity.XML_BPN_ID + "=?",
                    new String [] {bpnId}, null, null, null, null);
            database.closeTransaction();

            if(listObjQty.size() > 0) {
                for(int i = 0; i < listObjQty.size(); i++) {
                    BPNQuantity bpnQuantity = (BPNQuantity) listObjQty.get(i);

                    if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.GOOD_QTY_CODE)){
                        goodQtyCode = bpnQuantity.getAchievementCode();
                        goodQty = (int)bpnQuantity.getQuantity();
                    }else if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.GOOD_WEIGHT_CODE)){
                        goodWeightCode = bpnQuantity.getAchievementCode();
                        goodWeight = (int)bpnQuantity.getQuantity();

                    }else if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.BAD_QTY_CODE)){
                        badQtyCode= bpnQuantity.getAchievementCode();
                        badQty = (int)bpnQuantity.getQuantity();
                    }else if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.BAD_WEIGHT_CODE)){
                        badWeightCode = bpnQuantity.getAchievementCode();
                        badWeight = (int)bpnQuantity.getQuantity();

                    }else if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.POOR_QTY_CODE)){
                        poorQtyCode= bpnQuantity.getAchievementCode();
                        poorQty = (int)bpnQuantity.getQuantity();
                    }else if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.POOR_WEIGHT_CODE)){
                        poorWeightCode = bpnQuantity.getAchievementCode();
                        poorWeight =(int)bpnQuantity.getQuantity();
                    }

                }

                pod = (int)(goodQty + badQty + poorQty);
            }

        }else{
            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false,
                    UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
                nikClerk = userLogin.getNik();
                clerk = userLogin.getName();
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                division = userLogin.getDivision();
                gang = userLogin.getGang();
            }

            imei = new DeviceHandler(BPNCocoaActivity.this).getImei();
            bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);

            database.openTransaction();
            ForemanActive foremanActive = (ForemanActive) database.getDataFirst(false, ForemanActive.TABLE_NAME, null, null,
                    null, null, null, null, null);
            database.closeTransaction();

            if(foremanActive == null){
                database.openTransaction();
                Employee employee = (Employee) database.getDataFirst(false,
                        Employee.TABLE_NAME, null,
                        Employee.XML_COMPANY_CODE + "=?" + " and " +
                                Employee.XML_ESTATE + "=?" + " and " +
                                Employee.XML_DIVISION + "=?" + " and " +
                                Employee.XML_GANG + "=?" + " and " +
                                Employee.XML_ROLE_ID + "=?" + " and " +
                                Employee.XML_NIK + "!=?",
                        new String [] {companyCode, estate, division, gang, "LEADER", nikClerk},
                        null, null, null, null);
                database.closeTransaction();

                if(employee != null){
                    foremanActive = new ForemanActive(0, employee.getCompanyCode(), employee.getEstate(), employee.getFiscalYear(),
                            employee.getFiscalPeriod(), employee.getNik(), employee.getName(), employee.getTermDate(),
                            employee.getDivision(), employee.getRoleId(), employee.getJobPos(), employee.getGang(),
                            employee.getCostCenter(), employee.getEmpType(), employee.getValidFrom(), employee.getHarvesterCode());

                    nikForeman = foremanActive.getNik();
                    foreman = foremanActive.getName();

                }
            }else{
                nikForeman = foremanActive.getNik();
                foreman = foremanActive.getName();

                if(!TextUtils.isEmpty(foremanActive.getGang())){
                    gang = foremanActive.getGang();
                }
            }

            gpsKoordinat = getLocation();
        }

        initInputView();

        edtHarvestBookPod.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateQty("");
            }
        });

        edtHarvestBookGoodWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateQty(goodWeightCode);
            }
        });

        edtHarvestBookBadWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateQty(badWeightCode);
            }
        });

        edtHarvestBookPoorWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) { calculateQty(poorWeightCode); }
        });

    }

    private void initInputView(){
        bpnId = (TextUtils.isEmpty(bpnId)) ? new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + imei.substring(8, imei.length() - 1) : bpnId;

        txtHarvestBookHarvestDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtHarvestBookForeman.setText(foreman);
        txtHarvestBookClerk.setText(clerk);
        txtHarvestBookBlock.setText(TextUtils.isEmpty(block) ? "" : block);
        txtHarvestBookCrop.setText(crop);
        txtHarvestBookHarvester.setText(harvester);
        edtHarvestBookTph.setText(tph);

        edtHarvestBookPod.setText(String.valueOf(pod));

        edtHarvestBookGoodQty.setText(String.valueOf(goodQty));
        edtHarvestBookGoodWeight.setText(String.valueOf(goodWeight));
        edtHarvestBookBadQty.setText(String.valueOf(badQty));
        edtHarvestBookBadWeight.setText(String.valueOf(badWeight));
        edtHarvestBookPoorQty.setText(String.valueOf(poorQty));
        edtHarvestBookPoorWeight.setText(String.valueOf(poorWeight));

        /*edtHarvestBookGoodQty.setText(String.valueOf(1));
        edtHarvestBookGoodWeight.setText(String.valueOf(2));
        edtHarvestBookBadQty.setText(String.valueOf(3));
        edtHarvestBookBadWeight.setText(String.valueOf(4));
        edtHarvestBookPoorQty.setText(String.valueOf(5));
        edtHarvestBookPoorWeight.setText(String.valueOf(6));*/

        txtHarvestBookGpsKoordinat.setText(gpsKoordinat);
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> listNikFilter = new ArrayList<String>();

        switch(view.getId()){
            case R.id.txtHarvestBookHarvestDate:
                new DialogDate(BPNCocoaActivity.this, getResources().getString(R.string.tanggal), new Date(), R.id.txtHarvestBookHarvestDate).show();
                break;
            case R.id.txtHarvestBookForeman:
                if(status == 0){
                    listNikFilter.add(nikClerk);
                    listNikFilter.add(nikHarvester);

                    startActivityForResult(new Intent(BPNCocoaActivity.this, MasterEmployeeActivity.class)
                            .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                            .putExtra(Employee.XML_ESTATE, estate)
                            .putExtra(Employee.XML_DIVISION, division)
                            .putExtra(Employee.XML_GANG, "HC")
                            .putExtra(Employee.XML_NIK, listNikFilter)
                            .putExtra(Constanta.SEARCH, true)
                            .putExtra(Constanta.TYPE, EmployeeType.FOREMAN.getId())
                            .putExtra(BPNHeader.XML_CROP, crop)
                            .putExtra("Activity", "BPN"), MST_FOREMAN);
                }else{
                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.txtHarvestBookBlock:
                if(status == 0){
                    startActivityForResult(new Intent(BPNCocoaActivity.this, BlockPlanningActivity.class)
                            .putExtra("readOnly", true)
                            .putExtra("croptype", "04"), MST_BLOCK);
                }else{
                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.txtHarvestBookHarvester:
                if(status == 0){
                    listNikFilter.add(nikClerk);
                    listNikFilter.add(nikForeman);

                    startActivityForResult(new Intent(BPNCocoaActivity.this, MasterEmployeeActivity.class)
                            .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                            .putExtra(Employee.XML_ESTATE, estate)
                            .putExtra(Employee.XML_DIVISION, division)
                            .putExtra(Employee.XML_GANG, gang)
                            .putExtra(Employee.XML_NIK, listNikFilter)
                            .putExtra(Constanta.SEARCH, true)
                            .putExtra(Constanta.TYPE, EmployeeType.HARVESTER.getId())
                            .putExtra(BPNHeader.XML_CROP, crop)
                            .putExtra("Activity", "BPN"), MST_HARVESTER);
                }else{
                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnActionBarRight:
                tph = edtHarvestBookTph.getText().toString().trim();
                goodQty = new Converter(edtHarvestBookGoodQty.getText().toString().trim()).StrToInt();
                goodWeight = new Converter(edtHarvestBookGoodWeight.getText().toString().trim()).StrToInt();
                badQty = new Converter(edtHarvestBookBadQty.getText().toString().trim()).StrToInt();
                badWeight = new Converter(edtHarvestBookBadWeight.getText().toString().trim()).StrToInt();
                poorQty = new Converter(edtHarvestBookPoorQty.getText().toString().trim()).StrToInt();
                poorWeight  = new Converter(edtHarvestBookPoorWeight.getText().toString().trim()).StrToInt();
                int PODint = new Converter(edtHarvestBookPod.getText().toString().trim()).StrToInt();
                if(PODint<=0){
                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                            "Weight dan POD tidak boleh kosong", false).show();
                    return;
                }
                if(goodWeight == 0 && poorWeight ==0 && badWeight ==0){
                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                            "Weight dan POD tidak boleh kosong", false).show();
                    return;
                }
                else{
                    long todayDate = new Date().getTime();

                    boolean IsBpnDateValid = ValidateBPNDate(bpnDate);
                    if(!bpnDate.isEmpty() && !foreman.isEmpty() && IsBpnDateValid && !clerk.isEmpty() && !harvester.isEmpty() && !block.isEmpty() && !tph.isEmpty()) {
                        database.openTransaction();
                        BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
                                BPNHeader.XML_BPN_ID + "=?",
                                new String [] {bpnId},
                                null, null, null, null);
                        database.closeTransaction();


                        if(bpnHeader != null){
                            if(bpnHeader.getStatus() == 1){
                                init = false;
                                new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                                        getResources().getString(R.string.data_already_export), false).show();
                            }else{
                                if(PODint<=0){
                                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                                            "Weight dan POD tidak boleh kosong", false).show();
                                    return;
                                }
                                if(goodWeight == 0 && poorWeight ==0 && badWeight ==0){
                                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                                            "Weight dan POD tidak boleh kosong", false).show();
                                    return;
                                }else {
                                    init = false;
                                    new DialogConfirm(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                                            getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                                }
                                }
                        }else{
                            try{
                                database.openTransaction();
                                database.setData(new BPNHeader(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                        nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, useGerdang, crop, gpsKoordinat, photo, status, spbsNumber,
                                        todayDate, clerk, todayDate, clerk));

                                database.deleteData(BPNQuantity.TABLE_NAME,
                                        BPNQuantity.XML_BPN_ID + "=?",
                                        new String [] {bpnId});

                                //Good
                                database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                        nikHarvester, crop, goodQtyCode, goodQty, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                                database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                        nikHarvester, crop, goodWeightCode, goodWeight, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                                //Bad
                                database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                        nikHarvester, crop, badQtyCode, badQty, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                                database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                        nikHarvester, crop, badWeightCode, badWeight, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                                //Poor
                                database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                        nikHarvester, crop, poorQtyCode, poorQty, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                                database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                        nikHarvester, crop, poorWeightCode, poorWeight, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                                ForemanActive foremanActive = new ForemanActive(0, "", "", 0, 0, nikForeman, foreman, "", "", "", "", gang, "", "", "", "");
                                database.deleteData(ForemanActive.TABLE_NAME, null, null);
                                database.setData(foremanActive);

                                database.commitTransaction();

                                init = true;
                                new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                                        getResources().getString(R.string.save_successed), false).show();

                            }catch(SQLiteException e){
                                e.printStackTrace();
                                database.closeTransaction();
                                init = false;
                                new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                                        e.getMessage(), false).show();
                            }finally{
                                database.closeTransaction();
                            }
                        }
                    }
                }

                break;
            default:
                if(gps != null){
                    gps.stopUsingGPS();
                }

                gpsHandler.stopGPS();

                finish();
                animOnFinish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode ==  RESULT_OK) {
            if(requestCode == MST_FOREMAN){
                foreman = data.getExtras().getString(Employee.XML_NAME);
                nikForeman = data.getExtras().getString(Employee.XML_NIK);
                txtHarvestBookForeman.setText(foreman);
                gang = data.getExtras().getString(Employee.XML_GANG);
            }else if(requestCode == MST_BLOCK){
                block = data.getExtras().getString(BlockPlanning.XML_BLOCK);
                crop = "04";

                txtHarvestBookBlock.setText(block);
                txtHarvestBookCrop.setText(crop);
            }else if(requestCode == MST_HARVESTER){
                harvester = data.getExtras().getString(Employee.XML_NAME);
                nikHarvester = data.getExtras().getString(Employee.XML_NIK);

                txtHarvestBookHarvester.setText(harvester);
                edtHarvestBookTph.requestFocus();
            }
        }
    }

    @Override
    public void onBackPressed() {
        new DialogConfirm(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                getResources().getString(R.string.exit), null, 1).show();
    }

    @Override
    public void onOK(boolean is_finish) {
        if(isEdit){
            Intent intent = new Intent(BPNCocoaActivity.this, BPNHistoryActivity.class);
            intent.putExtra(BPNHeader.TABLE_NAME, bpnHeader);

            setResult(RESULT_OK, intent);
            finish();
        }else{
            if(init){
                bpnId = "";
                nikHarvester = "";
                harvester = "";
                tph = "";
                goodQty = 0;
                goodWeight = 0;
                badQty = 0;
                badWeight = 0;
                poorQty = 0;
                poorWeight = 0;
                gpsKoordinat = getLocation();
                useGerdang = 0;
                photo = "";
                initInputView();
            }else{
                edtHarvestBookTph.requestFocus();
            }
        }
    }

    @Override
    public void onConfirmOK(Object object, int btnId) {
        switch(btnId) {
            case R.id.btnActionBarRight:
                long todayDate = new Date().getTime();
                int PODint = new Converter(edtHarvestBookPod.getText().toString().trim()).StrToInt();
                goodQty = new Converter(edtHarvestBookGoodQty.getText().toString().trim()).StrToInt();
                goodWeight = new Converter(edtHarvestBookGoodWeight.getText().toString().trim()).StrToInt();
                badQty = new Converter(edtHarvestBookBadQty.getText().toString().trim()).StrToInt();
                badWeight = new Converter(edtHarvestBookBadWeight.getText().toString().trim()).StrToInt();
                poorQty = new Converter(edtHarvestBookPoorQty.getText().toString().trim()).StrToInt();
                poorWeight  = new Converter(edtHarvestBookPoorWeight.getText().toString().trim()).StrToInt();
                if(PODint<=0){
                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                            "Weight dan POD tidak boleh kosong", false).show();
                    return;
                }
                if(goodWeight == 0 && poorWeight ==0 && badWeight ==0){
                    new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                            "Weight dan POD tidak boleh kosong", false).show();
                    return;
                }else {
                    try {
                        database.openTransaction();

                        bpnHeader = new BPNHeader(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, useGerdang, crop, gpsKoordinat, photo, status, spbsNumber,
                                todayDate, clerk, todayDate, clerk);

                        database.updateData(bpnHeader,
                                BPNHeader.XML_BPN_ID + "=?",
                                new String[]{bpnId});

                        database.deleteData(BPNQuantity.TABLE_NAME,
                                BPNQuantity.XML_BPN_ID + "=?",
                                new String[]{bpnId});

                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                nikHarvester, crop, goodQtyCode, goodQty, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                nikHarvester, crop, goodWeightCode, goodWeight, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                nikHarvester, crop, badQtyCode, badQty, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                nikHarvester, crop, badWeightCode, badWeight, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                nikHarvester, crop, poorQtyCode, poorQty, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                nikHarvester, crop, poorWeightCode, poorWeight, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                        ForemanActive foremanActive = new ForemanActive(0, "", "", 0, 0, nikForeman, foreman, "", "", "", "", gang, "", "", "", "");
                        database.deleteData(ForemanActive.TABLE_NAME, null, null);
                        database.setData(foremanActive);

                        database.commitTransaction();

                        init = true;
                        new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.save_successed), false).show();

                    } catch (SQLiteConstraintException e) {
                        e.printStackTrace();
                        database.closeTransaction();
                        new DialogNotification(BPNCocoaActivity.this, getResources().getString(R.string.informasi), e.getMessage(), false).show();
                    } finally {
                        database.closeTransaction();
                    }
                }
                break;
            default:
                if(gps != null){
                    gps.stopUsingGPS();
                }

                gpsHandler.stopGPS();

                finish();
                animOnFinish();
                break;

        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        bpnDate=new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
        txtHarvestBookHarvestDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        EditText edt = (EditText) view;

        edt.setSelection(edt.getText().toString().length());
    }

    public void updateGpsKoordinat(Location location){
        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }

        txtHarvestBookGpsKoordinat.setText(gpsKoordinat);
    }

    private String getLocation(){
        String gpsKoordinat = "0.0:0.0";

        if(gps != null){
            Location location = gps.getLocation();
            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                gpsKoordinat = latitude + ":" + longitude;
            }
        }

        return gpsKoordinat;
    }

    public void calculateQty(String weightcode){
        double pod = new Converter(edtHarvestBookPod.getText().toString().trim()).StrToInt();

        double goodweight = new Converter(edtHarvestBookGoodWeight.getText().toString().trim()).StrToDouble();
        double badweight = new Converter(edtHarvestBookBadWeight.getText().toString().trim()).StrToDouble();
        double poorweight = new Converter(edtHarvestBookPoorWeight.getText().toString().trim()).StrToDouble();

        double totalweight = (goodweight + badweight +  poorweight);

        if(totalweight > 0 ) {
            double goodqtydouble = Math.round((goodweight / (goodweight + badweight + poorweight)) * pod);
            goodQty = (int)goodqtydouble;

            double badqtydouble = Math.round((badweight / (goodweight + badweight + poorweight)) * pod);
            badQty = (int)badqtydouble;

            double poorqtydouble = Math.round((poorweight / (goodweight + badweight + poorweight)) * pod);
            poorQty = (int)poorqtydouble;
        }

        double totalQty = goodQty + badQty + poorQty;

        edtHarvestBookGoodQty.setText(String.valueOf(goodQty));
        edtHarvestBookBadQty.setText(String.valueOf(badQty));
        edtHarvestBookPoorQty.setText(String.valueOf(poorQty));

        if(goodweight > 0 && weightcode.equalsIgnoreCase(goodWeightCode)) {
            if ((pod != totalQty) && (totalweight > 0)) {
                goodQty = (int) pod - (badQty + poorQty);
                edtHarvestBookGoodQty.setText(String.valueOf(goodQty));
            } else {
                edtHarvestBookGoodQty.setText(String.valueOf(goodQty));
            }
        }

        if(poorweight > 0 && weightcode.equalsIgnoreCase(poorWeightCode)) {
            if ((pod != totalQty) && (totalweight > 0)) {
                poorQty = (int) pod - (goodQty + badQty);
                edtHarvestBookPoorQty.setText(String.valueOf(poorQty));
            } else {
                edtHarvestBookPoorQty.setText(String.valueOf(poorQty));
            }
        }

        if(badweight > 0 && weightcode.equalsIgnoreCase(badWeightCode)) {
            if ((pod != totalQty) && (totalweight > 0)) {
                badQty = (int) pod - (goodQty + poorQty);
                edtHarvestBookBadQty.setText(String.valueOf(badQty));
            } else {
                edtHarvestBookBadQty.setText(String.valueOf(badQty));
            }
        }


        if(weightcode.equalsIgnoreCase("") && pod != totalQty && totalweight > 0){
            badQty = (int) pod - (goodQty + poorQty);
            edtHarvestBookBadQty.setText(String.valueOf(badQty));
        }
    }

    private boolean ValidateBPNDate(String _bpnDate){
        boolean isValid=true;
        try
        {
            Calendar cal=Calendar.getInstance();
            Date curr_date=cal.getTime();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date BPNDate =  df.parse(_bpnDate);

            long result=curr_date.getTime()-BPNDate.getTime();
            long sec=result/1000;
            long minutes=sec/60;
            long hour=minutes/60;
            long day=hour/24;
            if(day>1 || day	<0){
                isValid=false;
            }

        }catch (ParseException ex){
            ex.printStackTrace();
        }


        return  isValid;
    }
}
