package com.simp.hms.activity.ancakpanen;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.master.MasterEmployeeGangActivity;
import com.simp.hms.activity.other.BlockPlanningActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.AncakPanenHeader;
import com.simp.hms.model.AncakPanenQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.Employee;
import com.simp.hms.model.Gang;
import com.simp.hms.model.Penalty;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;
import com.simp.hms.widget.EditTextCustom;

import android.app.ActionBar;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
   
public class AncakPanenActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogConfirmListener, OnFocusChangeListener, OnCheckedChangeListener{
	private Toolbar tbrMain;
	private LinearLayout lytAncakPanenRoot;
	private TextView txtAncakPanenAncakDate;
	private TextView txtAncakPanenForeman;
	private TextView txtAncakPanenClerk;
	private TextView txtAncakPanenGang;
	private TextView txtAncakPanenHarvester;
	private TextView txtAncakPanenBlock;
	private TextView txtAncakPanenCrop;
	private EditText edtAncakPanenTph;
	private TextView txtAncakPanenGpsKoordinat;	
	
	private EditText edtAncakPanenBlockDummy;
	
	private ScrollView sclAncakPanenPenalty;
	private LinearLayout lytAncakPanenPenalty;
	
	private HashMap<String, AncakPanenQuality> mapAncakPanenQuality = new HashMap<String, AncakPanenQuality>(0);
	
	GPSService gps;
	GpsHandler gpsHandler;
	
	private final int MST_FOREMAN = 101;
	private final int MST_CLERK = 102;
	private final int MST_HARVESTER = 103;
	private final int MST_BLOCK = 104;
	private final int REQUEST_CAMERA = 105;
	private final int MST_GANG = 106;
	
	String ancakPanenId = "";
	String imei = "";
	String companyCode = "";
	String estate = "";
	String ancakDate = "";
	String division = "";
	String gang = "";
	String block = "";
	String tph = "";
	String nikHarvester = "";
	String harvester = "";
	String nikForeman = "";
	String foreman = "";
//	String gangForeman = "";
	String nikClerk = "";
	String clerk = "";
	String crop = "";  
	String qtyJanjangCode = BPNQuantity.JANJANG_CODE;
//	int qtyJanjang = 0;
//	String qtyLooseFruitCode = BPNQuantity.LOOSE_FRUIT_CODE;
//	double qtyLooseFruit = 0;
//	String qlyMentahCode = BPNQuality.MENTAH_CODE;
//	int qlyMentah = 0;
//	String qlyBusukCode = BPNQuality.BUSUK_CODE;
//	int qlyBusuk = 0;
//	String qlyTangkaiPanjangCode = BPNQuality.TANGKAI_PANJANG_CODE;
//	int qlyTangkaiPanjang = 0;
	String gpsKoordinat = "0.0:0.0";
	int status = 0;
	double latitude = 0.0;
	double longitude = 0.0;
	
	boolean init = false;
	boolean isEdit = false;
	AncakPanenHeader ancakPanenHeader = null;
	
	DatabaseHandler database = new DatabaseHandler(AncakPanenActivity.this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();
		setContentView(R.layout.activity_ancak_panen);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lytAncakPanenRoot = (LinearLayout) findViewById(R.id.lytAncakPanenRoot);
		txtAncakPanenAncakDate = (TextView) findViewById(R.id.txtAncakPanenHarvestDate);
		txtAncakPanenForeman = (TextView) findViewById(R.id.txtAncakPanenForeman);
		txtAncakPanenClerk = (TextView) findViewById(R.id.txtAncakPanenClerk);
		txtAncakPanenGang = (TextView) findViewById(R.id.txtAncakPanenGang);
		txtAncakPanenHarvester = (TextView) findViewById(R.id.txtAncakPanenHarvester);
		txtAncakPanenBlock = (TextView) findViewById(R.id.txtAncakPanenBlock);
		txtAncakPanenCrop = (TextView) findViewById(R.id.txtAncakPanenCrop);
		edtAncakPanenTph = (EditText) findViewById(R.id.edtAncakPanenTph);
		txtAncakPanenGpsKoordinat = (TextView) findViewById(R.id.txtAncakPanenGpsKoordinat);
		
		edtAncakPanenBlockDummy = (EditText) findViewById(R.id.edtAncakPanenBlockDummy);
		
		sclAncakPanenPenalty = (ScrollView) findViewById(R.id.sclAncakPanenPenalty);
		lytAncakPanenPenalty = (LinearLayout) findViewById(R.id.lytAncakPanenPenalty);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.ancak_panen));
		btnActionBarRight.setText(getResources().getString(R.string.save));
		btnActionBarRight.setOnClickListener(this);
		     
		txtAncakPanenClerk.setOnClickListener(this);
		txtAncakPanenGang.setOnClickListener(this);
		txtAncakPanenHarvester.setOnClickListener(this);
		txtAncakPanenBlock.setOnClickListener(this);
		edtAncakPanenTph.setOnFocusChangeListener(this);
		
		gps = new GPSService(AncakPanenActivity.this);
		gpsHandler = new GpsHandler(AncakPanenActivity.this);
		
		GPSTriggerService.ancakPanenActivity = this;
		
		gpsHandler.startGPS();    
		
		if(getIntent().getExtras() != null){
			ancakPanenHeader = (AncakPanenHeader) getIntent().getParcelableExtra(AncakPanenHeader.TABLE_NAME);
		}
		
		if(ancakPanenHeader != null){
			isEdit = true;
			
			ancakPanenId = ancakPanenHeader.getAncakPanenId();
			imei = ancakPanenHeader.getImei();
			companyCode = ancakPanenHeader.getCompanyCode();
			estate = ancakPanenHeader.getEstate();
			ancakDate = ancakPanenHeader.getAncakDate();
			division = ancakPanenHeader.getDivision();
			gang = ancakPanenHeader.getGang();
			block = ancakPanenHeader.getLocation();
			tph = ancakPanenHeader.getTph();
			nikHarvester = ancakPanenHeader.getNikHarvester();
			harvester = ancakPanenHeader.getHarvester();
			nikForeman = ancakPanenHeader.getNikForeman();
			foreman = ancakPanenHeader.getForeman();
			nikClerk = ancakPanenHeader.getNikClerk();
			clerk = ancakPanenHeader.getClerk();
			crop = ancakPanenHeader.getCrop();
			gpsKoordinat = ancakPanenHeader.getGpsKoordinat();
			status = ancakPanenHeader.getStatus();
			
			
			if(status == 1){
				setViewAndChildrenEnabled(lytAncakPanenRoot, false);
				setViewAndChildrenEnabled(lytAncakPanenPenalty, false);
			}else{
				setViewAndChildrenEnabled(lytAncakPanenRoot, true);
				setViewAndChildrenEnabled(lytAncakPanenPenalty, true);
			}
		}else{
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, 
					UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				nikForeman = userLogin.getNik();
				foreman = userLogin.getName();
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();   
				gang = userLogin.getGang();
			}
			
			imei = new DeviceHandler(AncakPanenActivity.this).getImei();
			ancakDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
			
			gpsKoordinat = getLocation();
		}
		
		initInputView();
	}

	private void initInputView(){
		ancakPanenId = (TextUtils.isEmpty(ancakPanenId)) ? new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + imei.substring(8, imei.length() - 1) : ancakPanenId;
		txtAncakPanenAncakDate.setText(new DateLocal(ancakDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		txtAncakPanenForeman.setText(foreman);
		txtAncakPanenGang.setText(gang);
		txtAncakPanenClerk.setText(clerk);
		txtAncakPanenHarvester.setText(harvester);
//		txtAncakPanenBlock.setText(block);
		txtAncakPanenCrop.setText(crop);
		edtAncakPanenTph.setText(tph);

		txtAncakPanenBlock.setText(TextUtils.isEmpty(block) ? "" : block);
		txtAncakPanenGpsKoordinat.setText(gpsKoordinat);
		edtAncakPanenBlockDummy.requestFocus();
		
		lytAncakPanenPenalty.removeAllViews();
		mapAncakPanenQuality.clear();
		
		database.openTransaction();
		List<Object> listPenalty = database.getListData(false, Penalty.TABLE_NAME, null, 
				Penalty.XML_IS_ANCAK + "= ?", 
				new String [] {"1"},
				null, null, Penalty.XML_PENALTY_CODE, null);
		database.closeTransaction();      
		
		if(listPenalty != null && listPenalty.size() > 0){
			for(int i = 0; i < listPenalty.size(); i++){
				Penalty penalty = (Penalty) listPenalty.get(i);
				
				database.openTransaction();
				Object obj = database.getDataFirst(false, AncakPanenQuality.TABLE_NAME, null, 
						AncakPanenQuality.XML_ANCAK_PANEN_ID + "=?" + " and " +
								AncakPanenQuality.XML_QUALITY_CODE + "=?", 
						new String [] {ancakPanenId, penalty.getPenaltyCode()}, 
						null, null, null, null);
				database.closeTransaction();
				
				AncakPanenQuality ancakPanenQuality = (AncakPanenQuality) obj;
				
				if(obj != null){
					AddChild(ancakPanenQuality, penalty);
				}else{
					ancakPanenQuality = new AncakPanenQuality(0, ancakPanenId, imei, companyCode, estate, ancakDate, division, gang, 
							block, tph, nikHarvester, crop, qtyJanjangCode, penalty.getPenaltyCode(), 0, status, new Date().getTime(), foreman, 0, "");
					
					AddChild(ancakPanenQuality, penalty);
				}
			}
		}
	}
	
	@Override
	public void onClick(View view) {
		ArrayList<String> listNikFilter = new ArrayList<String>();
		
		switch (view.getId()) {
		case R.id.txtAncakPanenClerk:
			if(status == 0){
				listNikFilter.add(nikForeman);
				listNikFilter.add(nikHarvester);
				
				startActivityForResult(new Intent(AncakPanenActivity.this, MasterEmployeeActivity.class)
				.putExtra(Employee.XML_COMPANY_CODE, companyCode)
				.putExtra(Employee.XML_ESTATE, estate)
				.putExtra(Employee.XML_DIVISION, division)
				.putExtra(Employee.XML_GANG, "HC")
				.putExtra(Employee.XML_NIK, listNikFilter)
				.putExtra(Constanta.SEARCH, true)
				.putExtra(Constanta.TYPE, EmployeeType.CLERK.getId())
								.putExtra("Activity", "AncakPanen"),
				MST_CLERK);
			}else{
				new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.data_already_export), false).show();
			}
			break;
		case R.id.txtAncakPanenGang:
			startActivityForResult(new Intent(AncakPanenActivity.this, MasterEmployeeGangActivity.class)
			.putExtra(Constanta.SEARCH, true)
			.putExtra(Employee.XML_COMPANY_CODE, companyCode)
			.putExtra(Employee.XML_ESTATE, estate)
			.putExtra(Employee.XML_DIVISION, division), 
			MST_GANG);
			break;
		case R.id.txtAncakPanenHarvester:
			if(status == 0){   
				listNikFilter.add(nikClerk);
				listNikFilter.add(nikForeman);
				
				startActivityForResult(new Intent(AncakPanenActivity.this, MasterEmployeeActivity.class)
				.putExtra(Employee.XML_COMPANY_CODE, companyCode)
				.putExtra(Employee.XML_ESTATE, estate)
				.putExtra(Employee.XML_DIVISION, division)
				.putExtra(Employee.XML_GANG, gang)
				.putExtra(Employee.XML_NIK, listNikFilter)
				.putExtra(Constanta.SEARCH, true)
				.putExtra(Constanta.TYPE, EmployeeType.HARVESTER.getId())
								.putExtra("Activity", "AncakPanen"),
				MST_HARVESTER);
			}else{
				new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.data_already_export), false).show();
			}
			break;
		case R.id.txtAncakPanenBlock:
			if(status == 0){
				startActivityForResult(new Intent(AncakPanenActivity.this, BlockPlanningActivity.class).putExtra("readOnly", true), MST_BLOCK);
			}else{
				new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.data_already_export), false).show();
			}
			break;
		case R.id.btnActionBarRight:
			tph = edtAncakPanenTph.getText().toString().trim();
			
			long todayDate = new Date().getTime();
			  
			if(!ancakDate.isEmpty() && !foreman.isEmpty() && !clerk.isEmpty() && !harvester.isEmpty() && !block.isEmpty() && !gang.isEmpty() ){
				database.openTransaction();
				AncakPanenHeader ancakPanenHeader = (AncakPanenHeader) database.getDataFirst(false, AncakPanenHeader.TABLE_NAME, null,
						AncakPanenHeader.XML_ANCAK_PANEN_ID+ "=?", 
						new String [] {ancakPanenId}, 
						null, null, null, null);
				database.closeTransaction();
				
				if(ancakPanenHeader != null){
					if(ancakPanenHeader.getStatus() == 1){
						init = false;
						new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
								getResources().getString(R.string.data_already_export), false).show();
					}else{
						init = false;
						new DialogConfirm(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
								getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
					}
				}else{
					
					try{
						
						database.openTransaction();
						database.setData(new AncakPanenHeader(0, ancakPanenId, imei, companyCode, estate, ancakDate, division, gang, block, tph, 
								nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, crop, gpsKoordinat, status,
								todayDate, clerk, todayDate, clerk));

						database.deleteData(AncakPanenQuality.TABLE_NAME, 
								AncakPanenQuality.XML_ANCAK_PANEN_ID + "=?", 
								new String [] {ancakPanenId});
						
						boolean isQualityValid = false;
						for(Map.Entry<String, AncakPanenQuality> entry: mapAncakPanenQuality.entrySet()){
							AncakPanenQuality ancakPanenQuality = entry.getValue();
							
							if(ancakPanenQuality.getQuantity() > 0){
								ancakPanenQuality.setLocation(block);
								ancakPanenQuality.setNikHarvester(nikHarvester);
								ancakPanenQuality.setCrop(crop);
								ancakPanenQuality.setTph(tph);

								database.setData(ancakPanenQuality);
								
								isQualityValid = true;
							}
						}
						
						if(isQualityValid){
							database.commitTransaction();
							
							init = true;
							new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
									getResources().getString(R.string.save_successed), false).show();
						}else{
							new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
									getResources().getString(R.string.Invalid_data), false).show();
						}
					}catch(SQLiteException e){
						e.printStackTrace();
						database.closeTransaction();
						init = false;
						new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
								e.getMessage(), false).show();
					}finally{
						database.closeTransaction();
					}
				}
			}else{
				init = false;
				new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.Invalid_data), false).show();
			}
			break; 
		default:  
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode ==  RESULT_OK){
			if(requestCode == MST_CLERK){
				clerk = data.getExtras().getString(Employee.XML_NAME);
				nikClerk = data.getExtras().getString(Employee.XML_NIK);
				
				txtAncakPanenClerk.setText(clerk);
			}else if(requestCode == MST_BLOCK){
//				block = data.getExtras().getString(BlockHdrc.XML_BLOCK);
				block = data.getExtras().getString(BlockPlanning.XML_BLOCK);
				crop = "01";
				
				txtAncakPanenBlock.setText(block);
				txtAncakPanenCrop.setText(crop);
			}else if(requestCode == MST_HARVESTER){
				harvester = data.getExtras().getString(Employee.XML_NAME);
				nikHarvester = data.getExtras().getString(Employee.XML_NIK);
				  
				txtAncakPanenHarvester.setText(harvester);
				edtAncakPanenTph.requestFocus();
			}else if(requestCode == MST_GANG){
				gang = data.getExtras().getString(Gang.XML_GANG_CODE);
				txtAncakPanenGang.setText(gang);
			}
		}
	}
	
	@Override
	public void onBackPressed() {
//		super.onBackPressed();
		
		new DialogConfirm(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
				getResources().getString(R.string.exit), null, 1).show();
	}
	
	public void updateGpsKoordinat(Location location){
		if(location != null){
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			
			gpsKoordinat = latitude + ":" + longitude;
		}
		
		txtAncakPanenGpsKoordinat.setText(gpsKoordinat);
	}

	@Override
	public void onConfirmOK(Object object, int btnId) {
		
		switch(btnId){
		case R.id.btnActionBarRight:
			long todayDate = new Date().getTime();
			
			try{
				database.openTransaction();

				ancakPanenHeader = new AncakPanenHeader(0, ancakPanenId, imei, companyCode, estate, ancakDate, division, gang, block, tph, 
						nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, crop, gpsKoordinat, status,
						todayDate, clerk, todayDate, clerk);
				
				database.updateData(ancakPanenHeader, 
						AncakPanenHeader.XML_ANCAK_PANEN_ID + "=?", 
						new String [] {ancakPanenId});

				database.deleteData(AncakPanenQuality.TABLE_NAME, 
						AncakPanenQuality.XML_ANCAK_PANEN_ID + "=?", 
						new String [] {ancakPanenId});

				boolean isQualityValid = false;
				for(Map.Entry<String, AncakPanenQuality> entry: mapAncakPanenQuality.entrySet()){
					AncakPanenQuality ancakPanenQuality = entry.getValue();
					
					if(ancakPanenQuality.getQuantity() > 0){
						ancakPanenQuality.setLocation(block);
						ancakPanenQuality.setNikHarvester(nikHarvester);
						ancakPanenQuality.setCrop(crop);
						ancakPanenQuality.setTph(tph);

						database.setData(ancakPanenQuality);
						
						isQualityValid = true;
					}
				}
				
				if(isQualityValid){
					database.commitTransaction();
					
					init = true;
					new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
							getResources().getString(R.string.save_successed), false).show();
				}else{
					new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), 
							getResources().getString(R.string.Invalid_data), false).show();
				}
			}catch(SQLiteConstraintException e){
				e.printStackTrace();
				database.closeTransaction();
				new DialogNotification(AncakPanenActivity.this, getResources().getString(R.string.informasi), e.getMessage(), false).show();
			}finally{
				database.closeTransaction();
			}
			break;
		default:
			if(gps != null){
				gps.stopUsingGPS();
			}
			
			gpsHandler.stopGPS();
			
			finish();
			animOnFinish();
			break;
		}
	}

	@Override
	public void onFocusChange(View view, boolean arg1) {
		EditText edt = (EditText) view;
		
		edt.setSelection(edt.getText().toString().length());
	}
	
	private String getLocation(){
		String gpsKoordinat = "0.0:0.0";
		   
		if(gps != null){
			Location location = gps.getLocation();
			if(location != null){
				latitude = location.getLatitude();
				longitude = location.getLongitude();
				
				gpsKoordinat = latitude + ":" + longitude;
			}
		}
		
		return gpsKoordinat;
	}
	
	@Override
	public void onOK(boolean is_finish) {
		if(isEdit){
			Intent intent = new Intent(AncakPanenActivity.this, AncakPanenHistoryActivity.class);
			intent.putExtra(AncakPanenHeader.TABLE_NAME, ancakPanenHeader);
			
			setResult(RESULT_OK, intent);
			finish();
		}else{
			if(init){
				ancakPanenId = "";
				nikHarvester = "";
				harvester = "";
				tph = "";
				gpsKoordinat = getLocation();
				initInputView();
			}else{
				edtAncakPanenTph.requestFocus();
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

	}
	
	private void AddChild(final AncakPanenQuality ancakPanenQuality, final Penalty penalty){
		final TextView txtPenaltyItemName;
		final EditTextCustom edtPenaltyItemCount;
		final TextView txtPenaltyItemUom;
		
		final View child = getLayoutInflater().inflate(R.layout.item_penalty, null);
		
		txtPenaltyItemName = (TextView) child.findViewById(R.id.txtPenaltyItemName);
		edtPenaltyItemCount = (EditTextCustom) child.findViewById(R.id.edtPenaltyItemCount);
		txtPenaltyItemUom = (TextView) child.findViewById(R.id.txtPenaltyItemUom);
		
		txtPenaltyItemName.setText(penalty.getPenaltyDesc());
		edtPenaltyItemCount.setText(ancakPanenQuality.getQuantity() > 0 ? String.valueOf((int) ancakPanenQuality.getQuantity()) : "");
		txtPenaltyItemUom.setText(penalty.getUom());
		
		edtPenaltyItemCount.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			
			@Override
			public void afterTextChanged(Editable editable) {
				String qty = editable.toString().trim();
				
				ancakPanenQuality.setQuantity(new Converter(qty).StrToDouble());
			}
		});
		
		mapAncakPanenQuality.put(penalty.getPenaltyCode(), ancakPanenQuality);
		lytAncakPanenPenalty.addView(child);
	}
}