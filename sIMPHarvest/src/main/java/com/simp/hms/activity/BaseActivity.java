package com.simp.hms.activity;

import com.simp.hms.R;
import com.simp.hms.database.SharedPreferencesHandler;
import com.simp.hms.handler.LanguageHandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
  
public class BaseActivity extends AppCompatActivity {
	public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "com.simp.hms.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
	private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
	public static final IntentFilter INTENT_FILTER = createIntentFilter();
           
	private static IntentFilter createIntentFilter(){
		IntentFilter filter = new IntentFilter();
		filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
		return filter;  
	}        

	protected void registerBaseActivityReceiver() {
		registerReceiver(baseActivityReceiver, INTENT_FILTER);
	}

	protected void unRegisterBaseActivityReceiver() {
		unregisterReceiver(baseActivityReceiver);
	}

	public class BaseActivityReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)){
				finish();
			}   
		}
	} 

	protected void closeAllActivities(){
		sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
	}
	
	protected void animOnStart(){
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}
	
	protected void animOnFinish(){
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		animOnFinish();
	}
	
	protected void getConfig(){
		String countryCode = new SharedPreferencesHandler(BaseActivity.this).getCountryCode();
		new LanguageHandler(BaseActivity.this).updateLanguage(countryCode);
	}
	
	public void setViewAndChildrenEnabled(View view, boolean enabled) {
	    view.setEnabled(enabled);
	    if (view instanceof ViewGroup) {
	        ViewGroup viewGroup = (ViewGroup) view;
	        for (int i = 0; i < viewGroup.getChildCount(); i++) {
	            View child = viewGroup.getChildAt(i);
	            setViewAndChildrenEnabled(child, enabled);
	        }
	    }
	}
	
	public int dp2px(int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				getResources().getDisplayMetrics());
	}
}

