package com.simp.hms.activity.ancakpanen;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.simp.hms.R;
import com.simp.hms.R.drawable;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterAncakPanenHistory;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.AncakPanenHeader;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.UserLogin;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;   

public class AncakPanenHistoryActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogDateListener, DialogNotificationListener, DialogConfirmListener{
	private Toolbar tbrMain;
	private TextView txtAncakPanenHistoryAncakDate;
	private Button btnAncakPanenHistorySearch;
	private SwipeMenuListView lsvAncakPanenHistory;
	private EditText edtAncakPanenSearch;
	
	private List<AncakPanenHeader> lstAncakPanenHeader;
	private AdapterAncakPanenHistory adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	private DialogConfirm dialogConfirm;
	
	DatabaseHandler database = new DatabaseHandler(AncakPanenHistoryActivity.this);
	
	String ancakPanenDate = "";
	
	final int ANCAK_PANEN_DETAIL = 101;
	
	int posSelected = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_ancak_panen_history);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtAncakPanenHistoryAncakDate = (TextView) findViewById(R.id.txtAncakPanenHistoryAncakDate);
		btnAncakPanenHistorySearch = (Button) findViewById(R.id.btnAncakPanenHistorySearch);
		lsvAncakPanenHistory = (SwipeMenuListView) findViewById(R.id.lsvAncakPanenHistory);
		edtAncakPanenSearch = (EditText) findViewById(R.id.edtAncakPanenHistorySearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.ancak_panen));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		txtAncakPanenHistoryAncakDate.setOnClickListener(this);
		btnAncakPanenHistorySearch.setOnClickListener(this);
		edtAncakPanenSearch.addTextChangedListener(this);
		lsvAncakPanenHistory.setOnItemClickListener(this);
		
//		getDataAsync = new GetDataAsyncTask();
//		getDataAsync.execute();
		
		ancakPanenDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtAncakPanenHistoryAncakDate.setText(new DateLocal(ancakPanenDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				// create "open" item
				SwipeMenuItem openItem = new SwipeMenuItem(
						getApplicationContext());
				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(
						getApplicationContext());
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				deleteItem.setIcon(R.drawable.ic_delete_pressed);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};
		
		lsvAncakPanenHistory.setMenuCreator(creator);
		lsvAncakPanenHistory.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(int pos, SwipeMenu menu, int index) {
				dialogConfirm = new DialogConfirm(AncakPanenHistoryActivity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.data_delete), null, R.id.lsvAncakPanenHistory);
				dialogConfirm.show();
				
				posSelected = pos;
				return false;
			}
		});
		
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		AncakPanenHeader ancakPanenHeader = (AncakPanenHeader) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(AncakPanenHeader.TABLE_NAME, ancakPanenHeader);
		
		Intent intent = new Intent(AncakPanenHistoryActivity.this, AncakPanenActivity.class);
		intent.putExtras(bundle);

		startActivityForResult(intent, ANCAK_PANEN_DETAIL);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtAncakPanenHistoryAncakDate:
			new DialogDate(AncakPanenHistoryActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtAncakPanenHistoryAncakDate).show();
			break;
		case R.id.btnAncakPanenHistorySearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;   
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<AncakPanenHeader>, List<AncakPanenHeader>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(AncakPanenHistoryActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<AncakPanenHeader> doInBackground(Void... voids) {
			
			String companyCode = "";
			String estate = "";
			String division = "";
			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}
			   
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, AncakPanenHeader.TABLE_NAME, null, 
					AncakPanenHeader.XML_COMPANY_CODE + "=?" + " and " +
					AncakPanenHeader.XML_ESTATE + "=?" + " and " +
					AncakPanenHeader.XML_DIVISION + "=?" + " and " +
					AncakPanenHeader.XML_NIK_FOREMAN + "=?" + " and " +
					AncakPanenHeader.XML_ANCAK_DATE + "=?", 
					new String [] {companyCode, estate, division, nik, ancakPanenDate},
					null, null, AncakPanenHeader.XML_CREATED_DATE + " desc", null);
			database.closeTransaction();
			
			List<AncakPanenHeader> listTemp = new ArrayList<AncakPanenHeader>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					AncakPanenHeader AncakPanenHeader = (AncakPanenHeader) listObject.get(i);
					
					listTemp.add(AncakPanenHeader);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<AncakPanenHeader> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
//			if(listTemp.size() > 0){
				lstAncakPanenHeader = listTemp;
				adapter = new AdapterAncakPanenHistory(AncakPanenHistoryActivity.this, lstAncakPanenHeader, R.layout.item_ancak_panen_history);
//			}else{
//				adapter = null;
//			}
			
			lsvAncakPanenHistory.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}       
 
	@Override
	public void onDateOK(Date date, int id) {
		ancakPanenDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtAncakPanenHistoryAncakDate.setText(new DateLocal(ancakPanenDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_OK && requestCode == ANCAK_PANEN_DETAIL){
			AncakPanenHeader ancakPanenHeader = data.getExtras().getParcelable(AncakPanenHeader.TABLE_NAME);
			
			adapter.updateData(ancakPanenHeader);
		}
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		if(R.id.lsvAncakPanenHistory == id){
			adapter.deleteData(posSelected);
		}
	}
	
	
}
