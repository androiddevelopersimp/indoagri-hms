package com.simp.hms.activity.main;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.MenuAdapter;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.AssetsHandler;
import com.simp.hms.handler.BackupHandler;
import com.simp.hms.handler.FileEncryptionHandler;
import com.simp.hms.handler.FileXMLHandler;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.BLKPLT;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.FileXML;
import com.simp.hms.model.Menu;
import com.simp.hms.model.MenuData;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.TaksasiLine;
import com.simp.hms.model.UserLogin;
import com.simp.hms.widget.GridSpacingItemDecoration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends BaseActivity {
    private Toolbar tbrMain;
    private TextView txtMainMenuTime;
    private TextView txtMainMenuDate;
    private TextView txtMainMenuUserName;
    private TextView txtMainMenuUserNik;
    private TextView txtMainMenuUserRole;
    private RecyclerView rcvMainMenu;

    private MenuAdapter adapter;
    private LoadMenuTask loadMenuTask;

    DatabaseHandler database = new DatabaseHandler(MainActivity.this);
    BackupTask backupTask;

    MyTask myTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerBaseActivityReceiver();
        animOnStart();

        setContentView(R.layout.activity_main);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        txtMainMenuTime = (TextView) findViewById(R.id.txtMainMenuTime);
        txtMainMenuDate = (TextView) findViewById(R.id.txtMainMenuDate);
        txtMainMenuUserName = (TextView) findViewById(R.id.txtMainMenuUserName);
        txtMainMenuUserNik = (TextView) findViewById(R.id.txtMainMenuUserNik);
        txtMainMenuUserRole = (TextView) findViewById(R.id.txtMainMenuUserRole);
        rcvMainMenu = (RecyclerView) findViewById(R.id.rcvMainMenu);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        txtActionBarTitle.setText(getResources().getString(R.string.app_name));
        btnActionBarRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backupTask = new BackupTask();
                backupTask.execute();
            }
        });

        new BackupHandler(MainActivity.this).startBackup();

        Thread myThread = null;
        Runnable runnable = new CountDownRunner();
        myThread= new Thread(runnable);
        myThread.start();

//        int columnCount = Utils.getColumnCount(MainActivity.this);
        int columnCount = 4;

        adapter = new MenuAdapter(MainActivity.this, new ArrayList<Menu>());

        rcvMainMenu.setAdapter(adapter);
        rcvMainMenu.setHasFixedSize(true);
        rcvMainMenu.setLayoutManager(new GridLayoutManager(MainActivity.this, columnCount, GridLayoutManager.VERTICAL, false));
        rcvMainMenu.addItemDecoration(new GridSpacingItemDecoration(MainActivity.this,
                columnCount, (int) getResources().getDimension(R.dimen.padding_normal), true));

        initMainMenu();
    }

    private void loadMenu(String filename){
        loadMenuTask = new LoadMenuTask();
        loadMenuTask.execute(filename);
    }

    private class LoadMenuTask extends AsyncTask<String, Void, MenuData>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected MenuData doInBackground(String... strings) {
            MenuData menuData = null;

            try {
                String fileName = strings[0];

                AssetsHandler assetsHandler = new AssetsHandler(MainActivity.this, fileName);
                String json = assetsHandler.getJsonFromAsset();
                menuData = new Gson().fromJson(json, MenuData.class);
            }catch (NullPointerException e){
                e.printStackTrace();
            }

            return menuData;
        }

        @Override
        protected void onPostExecute(MenuData menuData) {
            super.onPostExecute(menuData);

            if(menuData != null){
                try {
                    List<Menu> lstMenu = menuData.getMenus();

                    if(lstMenu != null) {
                        Collections.sort(lstMenu);

                        for (int i = 0; i < lstMenu.size(); i++) {
                            Menu menu = lstMenu.get(i);

                            adapter.addData(menu, i);
                        }
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            loadMenuTask = null;
        }
    }

    class MyTask extends AsyncTask<Void, Void, Void>{
        DialogProgress dlgProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dlgProgress = new DialogProgress(MainActivity.this, getResources().getString(R.string.wait));
                dlgProgress.show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            deleteDatabase();
            deleteFileXML(30);
            //deletePhoto(-1);
            deleteDatabase(30);
            //updatePhoto();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(dlgProgress != null && dlgProgress.isShowing()){
                dlgProgress.dismiss();
            }
        }

    }

    private void deleteDatabase(){
        deleteBPN(30);
        deleteBKM(30);
        deleteSPBS(30);
        deleteTaksasi(30);
    }

    private void deleteBPN(int databaseDel){
        try{
            database.openTransaction();

            database.deleteData(BPNQuantity.TABLE_NAME,
                    "date(" + BPNQuantity.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " < " + "date('now', '-" + databaseDel + " day')",
                    null);

            database.deleteData(BPNQuality.TABLE_NAME,
                    "date(" + BPNQuality.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);

            database.deleteData(BPNHeader.TABLE_NAME,
                    "date(" + BPNHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);

            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
        }
    }

    private void deleteBKM(int databaseDel){
        try{
            database.openTransaction();
            database.deleteData(BKMLine.TABLE_NAME,
                    "date(" + BKMLine.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);
            database.deleteData(BKMOutput.TABLE_NAME,
                    "date(" + BKMOutput.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);
            database.deleteData(BKMHeader.TABLE_NAME,
                    "date(" + BKMHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
        }
    }

    private void deleteSPBS(int databaseDel){
        try{
            database.openTransaction();
            database.deleteData(SPBSLine.TABLE_NAME,
                    "date(" + SPBSLine.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);
            database.deleteData(SPBSHeader.TABLE_NAME,
                    "date(" + SPBSHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
        }
    }

    private void deleteTaksasi(int databaseDel){
        try{
            database.openTransaction();
            database.deleteData(TaksasiLine.TABLE_NAME,
                    "date(" + TaksasiLine.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);
            database.deleteData(TaksasiHeader.TABLE_NAME,
                    "date(" + TaksasiHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')",
                    null);
            database.commitTransaction();
        }catch(SQLiteException e){
            e.printStackTrace();
            database.closeTransaction();
        }finally{
            database.closeTransaction();
        }
    }

    private void deleteFileXML(int fileDel){
        FolderHandler folderHandler = new FolderHandler(MainActivity.this);
        FileXMLHandler fileMasterHandler = new FileXMLHandler(MainActivity.this);
        List<FileXML> lstFiles;

        if(folderHandler.isSDCardWritable() && folderHandler.init()){
            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileMasterNew(), null, ".xml");
            deleteFile(lstFiles, fileDel);

            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileMasterBackup(), null, ".xml");
            deleteFile(lstFiles, fileDel);

            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileReportNew(), null, ".xml");
            deleteFile(lstFiles, fileDel);

            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileReportBackup(), null, ".xml");
            deleteFile(lstFiles, fileDel);

            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileRestoreNew(), null, ".xml");
            deleteFile(lstFiles, fileDel);

            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileRestoreBackup(), null, ".xml");
            deleteFile(lstFiles, fileDel);

            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileDatabaseExport(), null, ".xml");
            deleteFile(lstFiles, fileDel);

            lstFiles = fileMasterHandler.getFiles(folderHandler.getFileDatabaseImport(), null, ".xml");
            deleteFile(lstFiles, fileDel);
        }
    }

    private void deleteDatabase(int fileDel){
        File[] files = getExternalFilesDirs(Environment.DIRECTORY_DOWNLOADS);
        File folder = new File(files[files.length - 1], FolderHandler.DATABASE);

        if(folder.exists()){
            String[] fileDatabase = folder.list();
            List<FileXML> lstXML = new ArrayList<FileXML>();

            for(int i = 0; i < fileDatabase.length; i++){
                FileXML file = new FileXML(folder.getAbsolutePath() + "/" + fileDatabase[i], "");

                lstXML.add(file);
            }

            deleteFile(lstXML, fileDel);
        }
    }

    private void deleteFile(List<FileXML> lstMaster, int fileDel){
        try{
            for(int i = 0; i < lstMaster.size(); i++){
                File file = new File(lstMaster.get(i).getFileNew());

                if(file.exists()){
                    long lastModified = file.lastModified();
                    long currentDate = new Date().getTime();

                    long timeDiff = (currentDate - lastModified)/(24*60*60*1000);

                    if(timeDiff > fileDel){
                        file.delete();
                        MediaScannerConnection.scanFile(MainActivity.this, new String[]{file.getAbsolutePath()}, null, null);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private class BackupTask extends AsyncTask<Void, Void, Boolean>{

        DialogProgress dialogProgress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialogProgress = new DialogProgress(MainActivity.this, getResources().getString(R.string.wait));
            dialogProgress.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                return copyAppDbToDownloadFolder();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
            }

            if(result){
                Toast.makeText(MainActivity.this, getResources().getString(R.string.backup_successed), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(MainActivity.this, getResources().getString(R.string.backup_failed), Toast.LENGTH_SHORT).show();
            }

            try{
                database.openTransaction();
                database.deleteData(UserLogin.TABLE_NAME, null, null);
                database.commitTransaction();
                database.closeTransaction();

                closeAllActivities();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                animOnFinish();
            }catch(SQLiteException e){
                e.printStackTrace();
                database.closeTransaction();
            }
        }
    }

    private boolean copyAppDbToDownloadFolder() throws IOException {
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
//
        if(mExternalStorageAvailable && mExternalStorageWriteable){
            boolean success = false;

            success = new FileEncryptionHandler(getApplicationContext()).encrypt(FileEncryptionHandler.STORAGE_INTERNAL);
            success &= new FileEncryptionHandler(getApplicationContext()).encrypt(FileEncryptionHandler.STORAGE_EXTERNAL);

            return success;
        }

        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {}

    @Override
    protected void onDestroy() {
        super.onDestroy();

        new BackupHandler(MainActivity.this).stopBackup();
        unRegisterBaseActivityReceiver();

        if(loadMenuTask != null && loadMenuTask.getStatus() != AsyncTask.Status.FINISHED){
            loadMenuTask.cancel(true);
            loadMenuTask = null;
        }

        if(myTask != null && myTask.getStatus() != AsyncTask.Status.FINISHED){
            myTask.cancel(true);
            myTask = null;
        }
    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            public void run() {
                try{

                    Calendar cal = Calendar.getInstance(Locale.getDefault());

                    int days = cal.get(Calendar.DAY_OF_WEEK);
                    int dates = cal.get(Calendar.DATE);
                    int months = cal.get(Calendar.MONTH);
                    int years = cal.get(Calendar.YEAR);

                    int hours = cal.get(Calendar.HOUR);
                    int minutes = cal.get(Calendar.MINUTE);
                    int seconds = cal.get(Calendar.SECOND);
                    int amPm = cal.get(Calendar.AM_PM);

                    switch (amPm) {
                        case Calendar.AM:

                            break;
                        case Calendar.PM:
                            hours = hours + 12;
                        default:
                            break;
                    }

                    txtMainMenuTime.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));

                    String hari = "";

                    switch (days) {
                        case Calendar.SUNDAY:
                            hari = getResources().getString(R.string.minggu);
                            break;
                        case Calendar.MONDAY:
                            hari = getResources().getString(R.string.senin);
                            break;
                        case Calendar.TUESDAY:
                            hari = getResources().getString(R.string.selasa);
                            break;
                        case Calendar.WEDNESDAY:
                            hari = getResources().getString(R.string.rabu);
                            break;
                        case Calendar.THURSDAY:
                            hari = getResources().getString(R.string.kamis);
                            break;
                        case Calendar.FRIDAY:
                            hari = getResources().getString(R.string.jumat);
                            break;
                        case Calendar.SATURDAY:
                            hari = getResources().getString(R.string.sabtu);
                            break;
                        default:
                            break;
                    }


                    String bulan = "";

                    switch (months) {
                        case Calendar.JANUARY:
                            bulan = getResources().getString(R.string.januari);
                            break;
                        case Calendar.FEBRUARY:
                            bulan = getResources().getString(R.string.februari);
                            break;
                        case Calendar.MARCH:
                            bulan = getResources().getString(R.string.maret);
                            break;
                        case Calendar.APRIL:
                            bulan = getResources().getString(R.string.april);
                            break;
                        case Calendar.MAY:
                            bulan = getResources().getString(R.string.mei);
                            break;
                        case Calendar.JUNE:
                            bulan = getResources().getString(R.string.juni);
                            break;
                        case Calendar.JULY:
                            bulan = getResources().getString(R.string.juli);
                            break;
                        case Calendar.AUGUST:
                            bulan = getResources().getString(R.string.agustus);
                            break;
                        case Calendar.SEPTEMBER:
                            bulan = getResources().getString(R.string.september);
                            break;
                        case Calendar.OCTOBER:
                            bulan = getResources().getString(R.string.oktober);
                            break;
                        case Calendar.NOVEMBER:
                            bulan = getResources().getString(R.string.november);
                            break;
                        case Calendar.DECEMBER:
                            bulan = getResources().getString(R.string.desember);
                            break;
                        default:
                            break;
                    }

                    txtMainMenuDate.setText(hari + " " + dates + " " + bulan + " " + years);

                }catch (Exception e) {}
            }
        });
    }

    class CountDownRunner implements Runnable{
        // @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }

    private String digitFormat(int value, int digit){
        String str = String.valueOf(value);

        int l = str.length();

        if(str.length() < digit){
            for(int i = l; i < digit; i++){
                str = "0" + str;
            }
        }

        return str;
    }

    @Override
    protected void onResume() {
        super.onResume();

        myTask = new MyTask();
        myTask.execute();
    }

    private void initMainMenu(){
        DatabaseHandler database = new DatabaseHandler(MainActivity.this);

        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        String role = "";
        if(userLogin != null){
            txtMainMenuUserName.setText(userLogin.getName());
            txtMainMenuUserNik.setText(userLogin.getNik());
            txtMainMenuUserRole.setText(userLogin.getRoleId());
            role = userLogin.getRoleId();
        }

        database.openTransaction();
        int blkpltsawitCount = database.getCropType("01");
        database.closeTransaction();

        database.openTransaction();
        int blkpltkaretCount = database.getCropType("02");
        database.closeTransaction();

        database.openTransaction();
        int blkpltcocoaCount = database.getCropType("04");
        database.closeTransaction();

        if(role.equalsIgnoreCase("Foreman")){
            loadMenu("json/menu_fr.json");
        }else if(role.equalsIgnoreCase("Clerk")){
            if(blkpltsawitCount > 0 && blkpltkaretCount == 0 && blkpltcocoaCount == 0)
                loadMenu("json/menu_cl.json");
            else if(blkpltsawitCount == 0 && blkpltkaretCount > 0 && blkpltcocoaCount == 0)
                loadMenu("json/menu_cl_karet.json");
            else if(blkpltsawitCount > 0 && blkpltkaretCount > 0 && blkpltcocoaCount == 0)
                loadMenu("json/menu_cl_sawit_karet.json");
            else if(blkpltsawitCount == 0 && blkpltkaretCount == 0 && blkpltcocoaCount > 0)
                loadMenu("json/menu_cl_cocoa.json");
            else if(blkpltsawitCount > 0 && blkpltkaretCount == 0 && blkpltcocoaCount > 0)
                loadMenu("json/menu_cl_sawit_cocoa.json");
            else if(blkpltsawitCount == 0 && blkpltkaretCount > 0 && blkpltcocoaCount > 0)
                loadMenu("json/menu_cl_karet_cocoa.json");
            else if(blkpltsawitCount > 0 && blkpltkaretCount > 0 && blkpltcocoaCount > 0)
                loadMenu("json/menu_cl_sawit_karet_cocoa.json");
                //loadMenu("json/menu_cl_sawit_karet.json");

        }else if(role.equalsIgnoreCase("Field Assistant")){
            loadMenu("json/menu_fa.json");
        }else {
            loadMenu("json/menu_default.json");
        }
    }
}
