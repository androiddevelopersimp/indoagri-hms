package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.bkm.BKMAbsentActivity;
import com.simp.hms.adapter.AdapterAbsentType;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.AbsentType;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterAbsentTypeActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterAbsentType;
	private EditText edtMasterAbsentTypeSearch;
	
	private List<AbsentType> listAbsentType;
	private AdapterAbsentType adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	boolean isSearch = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_absent_type);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterAbsentType = (ListView) findViewById(R.id.lsvMasterAbsentType);
		edtMasterAbsentTypeSearch = (EditText) findViewById(R.id.edtMasterAbsentTypeSearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_absent_type));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtMasterAbsentTypeSearch.addTextChangedListener(this);
		lsvMasterAbsentType.setOnItemClickListener(this);
		
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
	}

	@Override
	protected void onStart() {
		super.onStart();   
		   
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		AbsentType absentType = (AbsentType) adapter.getItem(pos);
		
		if(isSearch){
			setResult(RESULT_OK, new Intent(MasterAbsentTypeActivity.this, BKMAbsentActivity.class)
			.putExtra(AbsentType.XML_ABSENT_TYPE, absentType.getAbsentType())
			.putExtra(AbsentType.XML_HKRLLO, absentType.getHkrllo())
			.putExtra(AbsentType.XML_HKRLHI, absentType.getHkrlhi())
			.putExtra(AbsentType.XML_HKVLHI, absentType.getHkvlhi()));
			finish();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<AbsentType>, List<AbsentType>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterAbsentTypeActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<AbsentType> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterAbsentTypeActivity.this);
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, AbsentType.TABLE_NAME, null, 
					AbsentType.XML_ABSENT_TYPE + "!=?" ,
					new String [] {"KE"},
					null, null, AbsentType.XML_ABSENT_TYPE, null);
			database.closeTransaction();
			
			List<AbsentType> listTemp = new ArrayList<AbsentType>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					AbsentType absentType = (AbsentType) listObject.get(i);
					
					listTemp.add(absentType);
				}
			}
			
			return listTemp;
		}

		@Override
		protected void onPostExecute(List<AbsentType> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				listAbsentType = listTemp;
				adapter = new AdapterAbsentType(MasterAbsentTypeActivity.this, listAbsentType, R.layout.item_absent_type);
			}else{
				adapter = null;
			}
			
			lsvMasterAbsentType.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {  
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
}
