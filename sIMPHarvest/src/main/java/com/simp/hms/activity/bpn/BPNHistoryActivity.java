package com.simp.hms.activity.bpn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.simp.hms.R;
import com.simp.hms.R.drawable;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNHistory;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.ForemanActive;
import com.simp.hms.model.UserLogin;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class BPNHistoryActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogDateListener, DialogNotificationListener, DialogConfirmListener{
	private Toolbar tbrMain;
	private TextView txtHarvestBookHistoryBpnDate;
	private Button btnHarvestBookHistorySearch;
	private SwipeMenuListView lsvHarvestBookHistory;
	private EditText edtHarvestBookSearch;
	
	private List<BPNHeader> lstBPNHeader;
	private AdapterBPNHistory adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	private DialogConfirm dialogConfirm;
	
	DatabaseHandler database = new DatabaseHandler(BPNHistoryActivity.this);
	
	String bpnDate = "";
	
	final int BPN_DETAIL = 101;
	
	int posSelected = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_bpn_history);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtHarvestBookHistoryBpnDate = (TextView) findViewById(R.id.txtHarvestBookHistoryBpnDate);
		btnHarvestBookHistorySearch = (Button) findViewById(R.id.btnHarvestBookHistorySearch);
		lsvHarvestBookHistory = (SwipeMenuListView) findViewById(R.id.lsvHarvestBookHistory);
		edtHarvestBookSearch = (EditText) findViewById(R.id.edtHarvestBookHistorySearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);
		
		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		txtHarvestBookHistoryBpnDate.setOnClickListener(this);
		btnHarvestBookHistorySearch.setOnClickListener(this);
		edtHarvestBookSearch.addTextChangedListener(this);
		lsvHarvestBookHistory.setOnItemClickListener(this);
		
//		getDataAsync = new GetDataAsyncTask();
//		getDataAsync.execute();
		
		bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtHarvestBookHistoryBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				// create "open" item
				SwipeMenuItem openItem = new SwipeMenuItem(
						getApplicationContext());
				/*
				// set item background
				openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
						0xCE)));
				// set item width
				openItem.setWidth(dp2px(90));
				// set item title
				openItem.setTitle("Open");
				// set item title fontsize
				openItem.setTitleSize(18);
				// set item title font color
				openItem.setTitleColor(Color.WHITE);
				// add to menu
				menu.addMenuItem(openItem);
				*/

				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(
						getApplicationContext());
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				deleteItem.setIcon(R.drawable.ic_delete_pressed);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};
		
		lsvHarvestBookHistory.setMenuCreator(creator);
		lsvHarvestBookHistory.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(int pos, SwipeMenu menu, int index) {
				dialogConfirm = new DialogConfirm(BPNHistoryActivity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.data_delete), null, R.id.lsvHarvestBookHistory);
				dialogConfirm.show();
				
				posSelected = pos;
				return false;
			}
		});
		
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		BPNHeader bpnHeader = (BPNHeader) adapter.getItem(pos);

		if(bpnHeader != null) {
			Bundle bundle = new Bundle();
			bundle.putParcelable(BPNHeader.TABLE_NAME, bpnHeader);

			if(bpnHeader.getCrop().equalsIgnoreCase("02")) {
				Intent intent = new Intent(BPNHistoryActivity.this, BPNKaretActivity.class);
				intent.putExtras(bundle);

				//		startActivity(new Intent(BPNHistoryActivity.this, BPNActivity.class)
				//				.putExtras(bundle));
				startActivityForResult(intent, BPN_DETAIL);

			}else if(bpnHeader.getCrop().equalsIgnoreCase("04")){
				Intent intent = new Intent(BPNHistoryActivity.this, BPNCocoaPODActivity.class);
				intent.putExtras(bundle);

				//		startActivity(new Intent(BPNHistoryActivity.this, BPNActivity.class)
				//				.putExtras(bundle));
				startActivityForResult(intent, BPN_DETAIL);
			}
			else {
				Intent intent = new Intent(BPNHistoryActivity.this, BPNActivity.class);
				intent.putExtras(bundle);

				//		startActivity(new Intent(BPNHistoryActivity.this, BPNActivity.class)
				//				.putExtras(bundle));
				startActivityForResult(intent, BPN_DETAIL);
			}
		}else{
			new DialogNotification(BPNHistoryActivity.this, getResources().getString(R.string.informasi),
					"Data not found", false).show();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtHarvestBookHistoryBpnDate:
			new DialogDate(BPNHistoryActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtHarvestBookHistoryBpnDate).show();
			break;
		case R.id.btnHarvestBookHistorySearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;   
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BPNHeader>, List<BPNHeader>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(BPNHistoryActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<BPNHeader> doInBackground(Void... voids) {
			
			String companyCode = "";
			String estate = "";
			String division = "";
			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}
			   
//			database.openTransaction();
//			ForemanActive foremanActive = (ForemanActive) database.getDataFirst(false, ForemanActive.TABLE_NAME, null, null, null, null, null, null, null);
//			database.closeTransaction();
//			
//			if(foremanActive != null){
//				gang = foremanActive.getGang();
//			}
			
//			database.openTransaction();
//			List<Object> listObject =  database.getListData(false, BPNHeader.TABLE_NAME, null, 
//					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//					BPNHeader.XML_ESTATE + "=?" + " and " +
//					BPNHeader.XML_DIVISION + "=?" + " and " +
//					BPNHeader.XML_GANG + "=?" + " and " +
//					BPNHeader.XML_NIK_CLERK + "=?" + " and " +
//					BPNHeader.XML_BPN_DATE + "=?", 
//					new String [] {companyCode, estate, division, gang, nik, bpnDate},
//					null, null, BPNHeader.XML_CREATED_DATE + " desc", null);
//			database.closeTransaction();
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, BPNHeader.TABLE_NAME, null, 
					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
					BPNHeader.XML_ESTATE + "=?" + " and " +
					BPNHeader.XML_DIVISION + "=?" + " and " +
					BPNHeader.XML_NIK_CLERK + "=?" + " and " +
					BPNHeader.XML_BPN_DATE + "=?", 
					new String [] {companyCode, estate, division, nik, bpnDate},
					null, null, BPNHeader.XML_CREATED_DATE + " desc", null);
			database.closeTransaction();
			
			List<BPNHeader> listTemp = new ArrayList<BPNHeader>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BPNHeader bpnHeader = (BPNHeader) listObject.get(i);
					
					listTemp.add(bpnHeader);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BPNHeader> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
//			if(listTemp.size() > 0){
				lstBPNHeader = listTemp;
				adapter = new AdapterBPNHistory(BPNHistoryActivity.this, lstBPNHeader, R.layout.item_bpn_history);
//			}else{
//				adapter = null;
//			}
			
			lsvHarvestBookHistory.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}       
 
	@Override
	public void onDateOK(Date date, int id) {
		bpnDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtHarvestBookHistoryBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_OK && requestCode == BPN_DETAIL){
			BPNHeader bpnHeader = data.getExtras().getParcelable(BPNHeader.TABLE_NAME);
			
			adapter.updateData(bpnHeader);
		}
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		if(R.id.lsvHarvestBookHistory == id){
			adapter.deleteData(posSelected);
		}
	}
	
	
}
