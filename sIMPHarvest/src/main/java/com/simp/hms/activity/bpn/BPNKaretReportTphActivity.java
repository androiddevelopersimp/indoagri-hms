package com.simp.hms.activity.bpn;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNKaretReportTph;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNKaretReportBlock;
import com.simp.hms.model.BPNKaretReportTph;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.UserLogin;

import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BPNKaretReportTphActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher {
    private Toolbar tbrMain;
    private EditText edtBPNReportTphSearch;
    private ListView lsvBPNReportTph;

    private List<BPNKaretReportTph> lstBPNReport;
    private AdapterBPNKaretReportTph adapter;

    private BPNKaretReportTphActivity.GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(BPNKaretReportTphActivity.this);

    String companyCode = "";
    String estate = "";
    String division = "";
    String gang = "";
    String nikHarvester = "";
    String bpnDate = "";
    String block = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        animOnStart();
        setContentView(R.layout.activity_bpn_karet_report_tph);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        edtBPNReportTphSearch = (EditText) findViewById(R.id.edtBpnReportTphSearch);
        lsvBPNReportTph = (ListView) findViewById(R.id.lsvBpnReportTph);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
        btnActionBarright.setVisibility(View.INVISIBLE);
        edtBPNReportTphSearch.addTextChangedListener(this);
        lsvBPNReportTph.setOnItemClickListener(this);

        BPNKaretReportBlock bpnReport = null;

        if(getIntent().getExtras() != null){
            bpnReport = (BPNKaretReportBlock) getIntent().getParcelableExtra(BPNKaretReportBlock.TABLE_NAME);
            bpnDate = getIntent().getExtras().getString(BPNHeader.XML_BPN_DATE);
            nikHarvester = getIntent().getExtras().getString(BPNHeader.XML_NIK_HARVESTER);
            block = getIntent().getExtras().getString(BPNHeader.XML_LOCATION);
        }

        if(bpnReport != null){
            companyCode = bpnReport.getCompanyCode();
            estate = bpnReport.getEstate();
            division = bpnReport.getDivision();
            gang = bpnReport.getGang();
        }

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtBpnReportHarvesterBpnDate:
                new DialogDate(BPNKaretReportTphActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtHarvestBookHistoryBpnDate).show();
                break;
            case R.id.btnBpnReportHarvesterSearch:
                /*getDataAsync = new BPNReportTphActivity.GetDataAsyncTask();
                getDataAsync.execute();*/
                break;
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<BPNKaretReportTph>, List<BPNKaretReportTph>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(BPNKaretReportTphActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<BPNKaretReportTph> doInBackground(Void... voids) {

//			String gang = "";
            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
//				gang = userLogin.getGang();
                nik = userLogin.getNik();
            }

//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
//					new String [] {BPNHeader.XML_BPN_ID, BPNHeader.XML_TPH, BPNHeader.XML_CREATED_DATE},
//					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//					BPNHeader.XML_ESTATE + "=?" + " and " +
//					BPNHeader.XML_DIVISION + "=?" + " and " +
//					BPNHeader.XML_GANG + "=?" + " and " +
//					BPNHeader.XML_BPN_DATE + "=?" + " and " +
//					BPNHeader.XML_NIK_HARVESTER + "=?" + " and " +
//					BPNHeader.XML_LOCATION + "=?",
//					new String [] {companyCode, estate, division, gang, bpnDate, nikHarvester, block},
//					BPNHeader.XML_BPN_ID + ", " + BPNHeader.XML_TPH + ", " + BPNHeader.XML_CREATED_DATE, null, BPNHeader.XML_TPH + ", " + BPNHeader.XML_CREATED_DATE, null);
//			database.closeTransaction();

            database.openTransaction();
            List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
                    new String [] {BPNHeader.XML_BPN_ID, BPNHeader.XML_TPH, BPNHeader.XML_CREATED_DATE},
                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BPNHeader.XML_ESTATE + "=?" + " and " +
                            BPNHeader.XML_DIVISION + "=?" + " and " +
                            BPNHeader.XML_BPN_DATE + "=?" + " and " +
                            BPNHeader.XML_NIK_HARVESTER + "=?" + " and " +
                            BPNHeader.XML_LOCATION + "=?",
                    new String [] {companyCode, estate, division, bpnDate, nikHarvester, block},
                    BPNHeader.XML_BPN_ID + ", " + BPNHeader.XML_TPH + ", " + BPNHeader.XML_CREATED_DATE, null, BPNHeader.XML_TPH + ", " + BPNHeader.XML_CREATED_DATE, null);
            database.closeTransaction();

            List<BPNKaretReportTph> listTemp = new ArrayList<BPNKaretReportTph>();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    BPNHeader bpnHeader = (BPNHeader) listObject.get(i);

                    String id = bpnHeader.getBpnId();
                    String tph = bpnHeader.getTph();
                    long createdDate = bpnHeader.getCreatedDate();

                    double qtyLatexWet = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.LATEX_WET_CODE);
                    double qtyLatexDRC = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.LATEX_DRC_CODE);
                    double qtyLatexDry = (double) ((qtyLatexWet * qtyLatexDRC) / 100);

                    double qtyLumpWet = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.LUMP_WET_CODE);
                    double qtyLumpDRC = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.LUMP_DRC_CODE);
                    double qtyLumpDry = (double) ((qtyLumpWet * qtyLumpDRC) / 100);

                    double qtySlabWet = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.SLAB_WET_CODE);
                    double qtySlabDRC = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.SLAB_DRC_CODE);
                    double qtySlabDry = (double) ((qtyLumpWet * qtyLumpDRC) / 100);

                    double qtyTreelaceWet = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.TREELACE_WET_CODE);
                    double qtyTreelaceDRC = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.TREELACE_DRC_CODE);
                    double qtyTreelaceDry = (double) ((qtyLumpWet * qtyLumpDRC) / 100);

                    listTemp.add(new BPNKaretReportTph(id, companyCode, estate, division, tph, createdDate,
                                                        qtyLatexWet, qtyLatexDRC, qtyLatexDry,
                                                        qtyLumpWet, qtyLumpDRC, qtyLumpDry,
                                                        qtySlabWet, qtySlabDRC, qtySlabDry,
                                                        qtyTreelaceWet, qtyTreelaceDRC, qtyTreelaceDry));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<BPNKaretReportTph> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(listTemp.size() > 0){
                lstBPNReport = listTemp;
                adapter = new AdapterBPNKaretReportTph(BPNKaretReportTphActivity.this, lstBPNReport, R.layout.item_bpn_karet_report_tph);
            }else{
                adapter = null;
            }

            lsvBPNReportTph.setAdapter(adapter);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) { }

    @Override
    public void afterTextChanged(Editable filter) {
        adapter.getFilter().filter(filter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    private double getQty(String id, String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String block, String tph, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                        BPNQuantity.XML_TPH + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        BPNQuantity.XML_CROP + "=?",
                new String [] {id, companyCode, estate, division, bpnDate, nikHarvester, block, tph, achievementCode, "02"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }

}
