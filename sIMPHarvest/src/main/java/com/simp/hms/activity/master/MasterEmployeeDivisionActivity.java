package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.bkm.BKMAbsentActivity;
import com.simp.hms.adapter.AdapterDivision;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.Division;
import com.simp.hms.model.Employee;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterEmployeeDivisionActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterEmployeeDivision;
	
	private List<Division> listDivision;
	private AdapterDivision adapter;

	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	boolean isSearch = false;
	String companyCode;
	String estate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_employee_division);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterEmployeeDivision = (ListView) findViewById(R.id.lsvMasterEmployeeDivision);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_division));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		lsvMasterEmployeeDivision.setOnItemClickListener(this);
		
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
		companyCode = getIntent().getExtras().getString(Employee.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(Employee.XML_ESTATE, "");
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		Division division = (Division) adapter.getItem(pos);
		
		if(isSearch){
			setResult(RESULT_OK, new Intent(MasterEmployeeDivisionActivity.this, BKMAbsentActivity.class)
				.putExtra(Division.XML_DIVISION, division.getCode()));
			finish();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<Division>, List<Division>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterEmployeeDivisionActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<Division> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterEmployeeDivisionActivity.this);
			List<Division> listTemp = new ArrayList<Division>();
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(true, Employee.TABLE_NAME, new String [] {Employee.XML_DIVISION}, 
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?", 
					new String [] {companyCode, estate},
					Employee.XML_DIVISION, null, Employee.XML_DIVISION, null);
			database.closeTransaction();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					Employee employee = (Employee) listObject.get(i);
					
					if(!TextUtils.isEmpty(employee.getDivision().trim())){
						listTemp.add(new Division(employee.getDivision()));
					}
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<Division> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				listDivision = listTemp;
				adapter = new AdapterDivision(MasterEmployeeDivisionActivity.this, listDivision, R.layout.item_division);
			}else{
				adapter = null;
			}
			
			lsvMasterEmployeeDivision.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
}
