package com.simp.hms.activity.bpn;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNCocoaReportHarvester;
import com.simp.hms.adapter.AdapterBPNReportHarvester;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BPNCocoaReportHarvester;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BPNReportHarvester;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BPNCocoaReportHarvesterActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, DialogDateListener {
    private Toolbar tbrMain;
    private TextView txtBpnReportHarvesterBpnDate;
    private Button btnBpnReportHarvesterSearch;
    private ListView lsvBpnReportHarvester;

    private TextView txtBpnReportHarvesterGoodQty;
    private TextView txtBpnReportHarvesterGoodWeight;
    private TextView txtBpnReportHarvesterBadQty;
    private TextView txtBpnReportHarvesterBadWeight;
    private TextView txtBpnReportHarvesterPoorQty;
    private TextView txtBpnReportHarvesterPoorWeight;

    private List<BPNReportHarvester> lstBPNReport;
    private AdapterBPNCocoaReportHarvester adapter;

    private BPNCocoaReportHarvesterActivity.GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(BPNCocoaReportHarvesterActivity.this);

    String bpnDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_bpn_cocoa_report_harvester);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        txtBpnReportHarvesterBpnDate = (TextView) findViewById(R.id.txtBpnReportHarvesterBpnDate);
        btnBpnReportHarvesterSearch = (Button) findViewById(R.id.btnBpnReportHarvesterSearch);
        lsvBpnReportHarvester = (ListView) findViewById(R.id.lsvBpnReportHarvester);

        txtBpnReportHarvesterGoodQty = (TextView) findViewById(R.id.txtBpnReportHarvesterGoodQty);
        txtBpnReportHarvesterGoodWeight = (TextView) findViewById(R.id.txtBpnReportHarvesterGoodWeight);
        txtBpnReportHarvesterBadQty = (TextView) findViewById(R.id.txtBpnReportHarvesterBadQty);
        txtBpnReportHarvesterBadWeight = (TextView) findViewById(R.id.txtBpnReportHarvesterBadWeight);
        txtBpnReportHarvesterPoorQty = (TextView) findViewById(R.id.txtBpnReportHarvesterPoorQty);
        txtBpnReportHarvesterPoorWeight = (TextView) findViewById(R.id.txtBpnReportHarvesterPoorWeight);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
        btnActionBarRight.setVisibility(View.INVISIBLE);
        txtBpnReportHarvesterBpnDate.setOnClickListener(this);
        btnBpnReportHarvesterSearch.setOnClickListener(this);
        lsvBpnReportHarvester.setOnItemClickListener(this);

        bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        txtBpnReportHarvesterBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

        adapter = new AdapterBPNCocoaReportHarvester(BPNCocoaReportHarvesterActivity.this, new ArrayList<BPNCocoaReportHarvester>(), R.layout.item_bpn_cocoa_report_harvester);
        lsvBpnReportHarvester.setAdapter(adapter);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        BPNCocoaReportHarvester bpnReport = (BPNCocoaReportHarvester) adapter.getItem(pos);

        Bundle bundle = new Bundle();
        bundle.putParcelable(BPNCocoaReportHarvester.TABLE_NAME, bpnReport);

        startActivity(new Intent(BPNCocoaReportHarvesterActivity.this, BPNCocoaReportBlockActivity.class)
                .putExtras(bundle)
                .putExtra(BPNHeader.XML_BPN_DATE, bpnDate));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtBpnReportHarvesterBpnDate:
                new DialogDate(BPNCocoaReportHarvesterActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtBpnReportHarvesterBpnDate).show();
                break;
            case R.id.btnBpnReportHarvesterSearch:
                getDataAsync = new BPNCocoaReportHarvesterActivity.GetDataAsyncTask();
                getDataAsync.execute();
                break;
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        bpnDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

        txtBpnReportHarvesterBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }


    private class GetDataAsyncTask extends AsyncTask<Void, List<BPNCocoaReportHarvester>, List<BPNCocoaReportHarvester>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(BPNCocoaReportHarvesterActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }


        @Override
        protected List<BPNCocoaReportHarvester> doInBackground(Void... voids) {
            String companyCode = "";
            String estate = "";
            String division = "";
            String gang = "";
            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                division = userLogin.getDivision();
                gang = userLogin.getGang();
                nik = userLogin.getNik();
            }

            database.openTransaction();
            List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
                    new String [] {BPNHeader.XML_NIK_HARVESTER, BPNHeader.XML_HARVESTER},
                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BPNHeader.XML_ESTATE + "=?" + " and " +
                            BPNHeader.XML_DIVISION + "=?" + " and " +
                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                            BPNHeader.XML_BPN_DATE + "=?" + " and " +
                            BPNHeader.XML_CROP + "=?",
                    new String [] {companyCode, estate, division, nik, bpnDate, "04"},
                    BPNHeader.XML_NIK_HARVESTER + ", " + BPNHeader.XML_HARVESTER, null, BPNHeader.XML_HARVESTER, null);
            database.closeTransaction();

            List<BPNCocoaReportHarvester> listTemp = new ArrayList<BPNCocoaReportHarvester>();


            if(listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    BPNHeader bpnHeader = (BPNHeader) listObject.get(i);

                    String nikHarvester = bpnHeader.getNikHarvester();
                    String harvester = bpnHeader.getHarvester();

                    double qtyGoodQty = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.GOOD_QTY_CODE);
                    double qtyGoodWeight = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.GOOD_WEIGHT_CODE);
                    double qtyBadQty = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.BAD_QTY_CODE);
                    double qtyBadWeight = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.BAD_WEIGHT_CODE);
                    double qtyPoorQty = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.POOR_QTY_CODE);
                    double qtyPoorWeight = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.POOR_WEIGHT_CODE);

                    listTemp.add(new BPNCocoaReportHarvester(companyCode, estate, division, gang, nikHarvester, harvester,
                            qtyGoodQty, qtyGoodWeight,
                            qtyBadQty, qtyBadWeight,
                            qtyPoorQty, qtyPoorWeight));
                }
            }
            return listTemp;
        }

        @Override
        protected void onPostExecute(List<BPNCocoaReportHarvester> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            adapter = new AdapterBPNCocoaReportHarvester(BPNCocoaReportHarvesterActivity.this, listTemp, R.layout.item_bpn_cocoa_report_harvester);
            lsvBpnReportHarvester.setAdapter(adapter);

            double totGoodQty = 0;
            double totGoodWeight = 0;
            double totBadQty = 0;
            double totBadWeight = 0;
            double totPoorQty = 0;
            double totPoorWeight = 0;



            if(listTemp != null && listTemp.size() > 0){
                for(int i = 0; i < listTemp.size(); i++){
                    BPNCocoaReportHarvester bpnReport = listTemp.get(i);

                    if(bpnReport != null){
                        totGoodQty += (double) bpnReport.getQtyGoodQty();
                        totGoodWeight += (double) bpnReport.getQtyGoodWeight();

                        totBadQty += (double) bpnReport.getQtyBadQty();
                        totBadWeight += (double) bpnReport.getQtyBadWeight();

                        totPoorQty += (double) bpnReport.getQtyPoorQty();
                        totPoorWeight += (double) bpnReport.getQtyPoorWeight();
                    }
                }
            }


            txtBpnReportHarvesterGoodQty.setText(String.valueOf(Utils.round(totGoodQty, 2)));
            txtBpnReportHarvesterGoodWeight.setText(String.valueOf(Utils.round(totGoodWeight, 2)));
            txtBpnReportHarvesterBadQty.setText(String.valueOf(Utils.round(totBadQty, 2)));
            txtBpnReportHarvesterBadWeight.setText(String.valueOf(Utils.round(totBadWeight, 2)));
            txtBpnReportHarvesterPoorQty.setText(String.valueOf(Utils.round(totPoorQty, 2)));
            txtBpnReportHarvesterPoorWeight.setText(String.valueOf(Utils.round(totPoorWeight, 2)));

//			if(listTemp.size() > 0){
//				lstBPNReport = listTemp;
//				adapter = new AdapterBPNReportHarvester(BPNReportHarvesterActivity.this, lstBPNReport, R.layout.item_bpn_report_harvester);
//			}else{
//				adapter = null;
//			}
//
//			lsvBpnReportHarvester.setAdapter(adapter);
        }
    }

    private double getQty(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        BPNQuantity.XML_CROP + "=?",
                new String [] {companyCode, estate, division, bpnDate, nikHarvester, achievementCode, "04"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }
}
