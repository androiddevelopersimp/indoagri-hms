package com.simp.hms.activity.spbs;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.datecs.api.emsr.EMSR;
import com.datecs.api.printer.Printer;
import com.datecs.api.printer.Printer.ConnectionListener;
import com.datecs.api.printer.ProtocolAdapter;
import com.datecs.api.universalreader.UniversalReader;
import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterBlockHdrcActivity;
import com.simp.hms.activity.master.MasterDivisionAssistantActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.master.MasterSPBSDestinationActivity;
import com.simp.hms.activity.master.MasterVehicleActivity;
import com.simp.hms.adapter.AdapterSPBSSummary;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.database.SharedPreferencesHandler;
import com.simp.hms.datecs.network.PrinterServer;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BLKRISET;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.ConfigApp;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.DeviceAlias;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.model.Employee;
import com.simp.hms.model.NomorFormat;
import com.simp.hms.model.Penalty;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.model.SPBSDestination;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSPrint;
import com.simp.hms.model.SPBSRunningNumber;
import com.simp.hms.model.SPBSSummary;
import com.simp.hms.model.UserLogin;
import com.simp.hms.model.Vendor;
import com.simp.hms.routines.Constanta;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;
import com.simp.hms.zebra.DemoSleeper;
import com.simp.hms.zebra.SettingsHelper;
import com.simp.hms.zebra.UIHelper;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class SPBSDatecsActivity extends BaseActivity implements OnItemClickListener, OnClickListener,
        DialogNotificationListener, DialogConfirmListener, DialogDateListener {
    private Toolbar tbrMain;
    private TextView txtSpbsNumber;
    private TextView txtSpbsDate;
    private TextView txtSpbsDest;
    private TextView txtSpbsClerk;
    private TextView txtSpbsDivision;
    private TextView txtSpbsCrop;
    private TextView txtSpbsAssistant;
    private TextView txtSpbsDriver;
    private Button btnSpbsDriverIn;
    private Button btnSpbsDriverEx;
    private TextView txtSpbsKernet;
    private TextView txtSpbsLicensePlate;
    private Button btnSpbsLicensePlateIn;
    private Button btnSpbsLicensePlateEx;
    private Button btnSpbsLoadBpn;
    private ListView lsvSpbsSummary;
    private Button btnSpbsAddBlock;

    private List<SPBSSummary> listSpbsSummary = new ArrayList<SPBSSummary>();
    private AdapterSPBSSummary adapter = null;

    GetDataAsyncTask getDataAsync;
    DialogProgress dialogProgress;
    GPSService gps;
    GpsHandler gpsHandler;

    private UIHelper helper = new UIHelper(this);
    DatabaseHandler database = new DatabaseHandler(SPBSDatecsActivity.this);

    BluetoothAdapter bluetoothAdapter = null;
    BluetoothDevice bluetoothDevice = null;

    String companyCode = "";
    String estate = "";
    String division = "";
    String divisionLogin = "";
    String gang = "";
    String nikClerk = "";
    String clerk = "";
    String alias = "";
    String imei = "";
    String imeiSerial = "000000";
    String year = "";
    String yearTwoDigits = "";
    String crop = "01";
    String spbsNumber = "";
    String spbsNumberTemp = "";
    String spbsDate = "";
    String nikDriver = "";
    String driver = "";
    String nikKernet = "";
    String kernet = "";
    String licensePlate = "";
    String runningAccount = "";
    String nikAssistant = "-";
    String assistant = "";
    String month = "";
    String monthAlphabet = "";
    int id = 1;
    int lastNumber = 0;
    String gpsKoordinat = "0.0:0.0";
    int isSave = 0;
    int status = 0;
    double latitude = 0;
    double longitude = 0;
    long todayDate = 0;
    String divisionRun = "";
    String destId = "";
    String destDesc = "";
    String destType = "";
    String lifnr = "";
    String vendorName = "";

    final int MST_ASS = 101;
    final int MST_BLOCK = 102;
    final int MST_TPH = 103;
    final int MST_DRIVER = 104;
    final int MST_RUN_ACC = 105;
    final int MST_KERNET = 106;
    final int INP_DRIVER_EX = 107;
    final int INP_LICENSE_EX = 108;
    final int MST_DEST = 109;

    final String QR_CODE_DELIMITER = ";";
    final String QR_CODE_HEADER_DELIMITER = ";";
    final String QR_CODE_LINE_DELIMITER = ";";

    boolean isNew = true;
    boolean isCancelSaveInPrint = false;

    String qtyJanjangCode = BPNQuantity.JANJANG_CODE;
    String qtyLooseFruitCode = BPNQuantity.LOOSE_FRUIT_CODE;

    Connection printerConnection;

    // Message type & name received from the BluetoothPrintService handler
    public static final int MESSAGE_DEVICE_NAME = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_READ = 3;
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private ProtocolAdapter mProtocolAdapter;
    private ProtocolAdapter.Channel mPrinterChannel;
    private ProtocolAdapter.Channel mUniversalChannel;
    private Printer mPrinter;
    private PrinterServer mPrinterServer;
    private BluetoothSocket mBtSocket;
    private Socket mNetSocket;

    private interface PrinterRunnable {
        public void run(ProgressDialog dialog, Printer printer) throws IOException, ParseException;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_spbs);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        txtSpbsNumber = (TextView) findViewById(R.id.txtSpbsNumber);
        txtSpbsDate = (TextView) findViewById(R.id.txtSpbsDate);
        txtSpbsDest = (TextView) findViewById(R.id.txtSpbsDest);
        txtSpbsClerk = (TextView) findViewById(R.id.txtSpbsClerk);
        txtSpbsDivision = (TextView) findViewById(R.id.txtSpbsDivision);
        txtSpbsCrop = (TextView) findViewById(R.id.txtSpbsCrop);
        txtSpbsAssistant = (TextView) findViewById(R.id.txtSpbsAssistant);
        txtSpbsDriver = (TextView) findViewById(R.id.txtSpbsDriver);
        btnSpbsDriverIn = (Button) findViewById(R.id.btnSpbsDriverIn);
        btnSpbsDriverEx = (Button) findViewById(R.id.btnSpbsDriverEx);
        txtSpbsKernet = (TextView) findViewById(R.id.txtSpbsKernet);
        txtSpbsLicensePlate = (TextView) findViewById(R.id.txtSpbsLicensePlate);
        btnSpbsLicensePlateIn = (Button) findViewById(R.id.btnSpbsLicensePlateIn);
        btnSpbsLicensePlateEx = (Button) findViewById(R.id.btnSpbsLicensePlateEx);
        btnSpbsLoadBpn = (Button) findViewById(R.id.btnSpbsLoadBpn);
        lsvSpbsSummary = (ListView) findViewById(R.id.lsvSpbsSummary);
        btnSpbsAddBlock = (Button) findViewById(R.id.btnSpbsAddBlock);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);
        TextView btnActionBarPrint = (TextView) tbrMain.findViewById(R.id.btnActionBarPrint);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.spbs));
        btnActionBarRight.setText(getResources().getString(R.string.save));
        btnActionBarRight.setOnClickListener(this);
        btnActionBarPrint.setOnClickListener(this);
        btnActionBarPrint.setVisibility(View.VISIBLE);
        txtSpbsDest.setOnClickListener(this);
        txtSpbsDivision.setOnClickListener(this);
        btnSpbsDriverIn.setOnClickListener(this);
        btnSpbsDriverEx.setOnClickListener(this);
        txtSpbsKernet.setOnClickListener(this);
        btnSpbsLicensePlateIn.setOnClickListener(this);
        btnSpbsLicensePlateEx.setOnClickListener(this);
        btnSpbsLoadBpn.setOnClickListener(this);
        lsvSpbsSummary.setOnItemClickListener(this);
        btnSpbsAddBlock.setOnClickListener(this);

//		database.openTransaction();
//		database.deleteData(DeviceAlias.TABLE_NAME, null, null);
//		database.deleteData(SPBSHeader.TABLE_NAME, null, null);
//		database.deleteData(SPBSLine.TABLE_NAME, null, null);
//		database.commitTransaction();
//		database.closeTransaction();

        deleteUnSaved();

        SPBSHeader spbsHeader = null;

        gps = new GPSService(SPBSDatecsActivity.this);
        gpsHandler = new GpsHandler(SPBSDatecsActivity.this);

        GPSTriggerService.spbsDatecsActivity = this;

        gpsHandler.startGPS();

        if (getIntent().getExtras() != null) {
            spbsHeader = (SPBSHeader) getIntent().getParcelableExtra(SPBSHeader.TABLE_NAME);
        }

        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if (userLogin != null) {
            companyCode = userLogin.getCompanyCode();
            estate = userLogin.getEstate();
            divisionLogin = userLogin.getDivision();
            gang = userLogin.getGang();
            nikClerk = userLogin.getNik();
            clerk = userLogin.getName();
        }

        if (spbsHeader != null) {
            imei = spbsHeader.getImei();
            year = spbsHeader.getYear();
            companyCode = spbsHeader.getCompanyCode();
            estate = spbsHeader.getEstate();
            division = spbsHeader.getDivision();
            spbsNumber = spbsHeader.getSpbsNumber();
            spbsDate = spbsHeader.getSpbsDate();
            crop = spbsHeader.getCrop();
            destId = spbsHeader.getDestId();
            destDesc = spbsHeader.getDestDesc();
            destType = spbsHeader.getDestType();

            database.openTransaction();
            spbsHeader = (SPBSHeader) database.getDataFirst(false, SPBSHeader.TABLE_NAME, null,
                    SPBSHeader.XML_YEAR + "=?" + " and " +
                            SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                            SPBSHeader.XML_CROP + "=?" + " and " +
                            SPBSHeader.XML_SPBS_NUMBER + "=?",
                    new String[]{year, companyCode, estate, crop, spbsNumber},
                    null, null, null, null);
            database.closeTransaction();

            nikClerk = spbsHeader.getNikClerk();
            clerk = spbsHeader.getClerk();
            nikAssistant = spbsHeader.getNikAssistant();
            nikDriver = spbsHeader.getNikDriver();
            driver = spbsHeader.getDriver();
            nikKernet = spbsHeader.getNikKernet();
            kernet = spbsHeader.getKernet();
            licensePlate = spbsHeader.getLicensePlate();
            runningAccount = spbsHeader.getRunningAccount();
            assistant = spbsHeader.getAssistant();
            status = spbsHeader.getStatus();
            isSave = 1;
            isNew = false;
            alias = spbsHeader.getSpbsNumber().substring(spbsHeader.getSpbsNumber().length() - 6, spbsHeader.getSpbsNumber().length() - 3);
            lifnr = spbsHeader.getLifnr();

            txtSpbsNumber.setText(spbsNumber);
        } else {
            imei = new DeviceHandler(SPBSDatecsActivity.this).getImei();
            year = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_YEAR_ONLY);
            spbsDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
            month = String.valueOf(new Converter(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_MONTH_ONLY)).StrToInt());
            monthAlphabet = new DateLocal(new Date()).getMonthAlphabet();
            todayDate = new Date().getTime();
            spbsNumber = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID);
            isNew = true;

            SPBSRunningNumber spbsNumberMax = getAlias(estate, divisionLogin, year, month, imei);

            if (spbsNumberMax != null) {
                alias = spbsNumberMax.getDeviceAlias();
                lastNumber = new Converter(spbsNumberMax.getRunningNumber()).StrToInt() + 1;
                id = spbsNumberMax.getId();
                divisionRun = spbsNumberMax.getDivision();
            } else {
                alias = "";
                lastNumber = 1;
            }

            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
            }

            gpsKoordinat = String.valueOf(latitude) + ":" + String.valueOf(longitude);
            isSave = 0;
        }

        adapter = new AdapterSPBSSummary(SPBSDatecsActivity.this, listSpbsSummary, R.layout.item_spbs_summary);
        lsvSpbsSummary.setAdapter(adapter);

        initView();

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

    private void initView() {
        txtSpbsDate.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtSpbsDest.setText(destDesc);
        txtSpbsDivision.setText(destDesc);
        txtSpbsClerk.setText(clerk);

        String tempDivision = division;
        if (!TextUtils.isEmpty(lifnr))
            tempDivision = division + "," + lifnr;
        txtSpbsDivision.setText(tempDivision);

        txtSpbsCrop.setText(crop);
        txtSpbsAssistant.setText(assistant);
        txtSpbsDriver.setText(driver);
        txtSpbsKernet.setText(kernet);
        txtSpbsLicensePlate.setText(licensePlate);

        GetDefaultSPBSDestination();
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        ArrayList<String> listBlock = new ArrayList<String>();

        for (int i = 0; i < adapter.getCount(); i++) {
            SPBSSummary spbsSummary = (SPBSSummary) adapter.getItem(i);

            listBlock.add(spbsSummary.getBlock());
        }

        SPBSSummary spbsSummary = (SPBSSummary) adapter.getItem(pos);

        startActivityForResult(new Intent(SPBSDatecsActivity.this, SPBSBlockActivity.class)
                .putExtra(SPBSHeader.XML_BLOCKS, listBlock)
                .putExtra(SPBSLine.XML_BLOCK, spbsSummary.getBlock())
                .putExtra(SPBSHeader.XML_IMEI, imei)
                .putExtra(SPBSHeader.XML_YEAR, year)
                .putExtra(SPBSHeader.XML_COMPANY_CODE, companyCode)
                .putExtra(SPBSHeader.XML_ESTATE, estate)
                .putExtra(SPBSHeader.XML_DIVISION, division)
                .putExtra(SPBSHeader.XML_CROP, crop)
                .putExtra(SPBSHeader.XML_SPBS_NUMBER, spbsNumber)
                .putExtra(SPBSHeader.XML_SPBS_DATE, spbsDate)
                .putExtra(SPBSHeader.XML_CLERK, clerk)
                .putExtra(SPBSHeader.XML_STATUS, status), MST_TPH);
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> listNikFilter = new ArrayList<String>();

        switch (view.getId()) {
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:
                if (status == 0) {
                    if (listSpbsSummary.size() > 0 && !TextUtils.isEmpty(division) &&
                            !TextUtils.isEmpty(nikDriver) && !TextUtils.isEmpty(licensePlate) && !TextUtils.isEmpty(alias)) {
                        if (!isNew) {
                            new DialogConfirm(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                        } else {
                            try {
                                String realSPBSNumber = new NomorFormat(companyCode, estate, division, year, monthAlphabet, String.valueOf(alias), String.valueOf(lastNumber)).getSPBSFormat();

                                database.openTransaction();

                                //Fernando -> Check tidak boleh ada >1 Langsir dari 1 BPN
                                boolean checkLangsir = false;
                                if (destType.equalsIgnoreCase("LANGSIR")) {
                                    String bpnIDLangsir = "";
                                    for (int i = 0; i < listSpbsSummary.size(); i++) {
                                        SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);

                                        SPBSLine spbsLineCurrent = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                        SPBSLine.XML_CROP + "=?",
                                                new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
                                                null, null, null, null);

                                        if (spbsLineCurrent != null)
                                            bpnIDLangsir = spbsLineCurrent.getBpnId();
                                    }

                                    List<Object> listspbsheaderLangsir = database.getListData(false, SPBSHeader.TABLE_NAME, null,
                                            SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                    SPBSHeader.XML_CROP + "=?" + " and " +
                                                    SPBSHeader.XML_DEST_TYPE + "=?",
                                            new String[]{companyCode, estate, crop, "LANGSIR"},
                                            null, null, null, null);

                                    if (listspbsheaderLangsir.size() > 0) {
                                        for (int m = 0; m < listspbsheaderLangsir.size(); m++) {
                                            SPBSHeader spbsheaderLangsir = (SPBSHeader) listspbsheaderLangsir.get(m);

                                            List<Object> listspbslineLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                    SPBSLine.XML_BPN_ID + "=?" + " and " +
                                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_NUMBER + "=?",
                                                    new String[]{bpnIDLangsir, year, companyCode, estate, crop, spbsheaderLangsir.getSpbsNumber()},
                                                    null, null, null, null);

                                            double qtyLangsirRemain = 0;
                                            if (listspbslineLangsir.size() > 0) {
                                                for (int n = 0; n < listspbslineLangsir.size(); n++) {
                                                    SPBSLine spbslineLangsir = (SPBSLine) listspbslineLangsir.get(n);
                                                    qtyLangsirRemain = (double) (qtyLangsirRemain + spbslineLangsir.getQuantityRemaining());
                                                }
                                            }

                                            if (qtyLangsirRemain > 0) {
                                                checkLangsir = true;
                                                break;
                                            }
                                        }
                                    }

                                }


                                if (!checkLangsir) {
                                    //modified by Adit 20161223
                                    long result = database.setData(new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPBSNumber, spbsDate, destId, destDesc, destType, division,
                                            nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                            gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr));

                                    if (result == 0) {

                                        //update spbs hdr
                                        SPBSHeader objSpbsHdr = new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPBSNumber, spbsDate, destId, destDesc, destType, division,
                                                nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                                gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr);
                                        database.updateData(objSpbsHdr,
                                                SPBSHeader.XML_IMEI + "=?" + " and " +
                                                        SPBSHeader.XML_YEAR + "=?" + " and " +
                                                        SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                        SPBSHeader.XML_CROP + "=?" + " and " +
                                                        SPBSHeader.XML_SPBS_NUMBER + "=?",
                                                new String[]{imei, year, companyCode, estate, crop, spbsNumber}
                                        );
                                    }
                                    for (int i = 0; i < listSpbsSummary.size(); i++) {
                                        SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);

                                        List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                        SPBSLine.XML_CROP + "=?",
                                                new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
                                                null, null, null, null);

                                        if (listObject.size() > 0) {
                                            for (int j = 0; j < listObject.size(); j++) {
                                                SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                                spbsLine.setSpbsNumber(realSPBSNumber);
                                                spbsLine.setIsSave(1);

                                                String bpnId = spbsLine.getBpnId();
                                                String spbsRef = spbsLine.getSpbsRef();

                                                //Fernando, New SPBS Load --> Save New - 1
                                                BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                                        BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                                BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                                BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                                BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                                BPNQuantity.XML_TPH + "=?" + " and " +
                                                                BPNQuantity.XML_CROP + "=?" + " and " +
                                                                BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                        new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()},
                                                        null, null, null, null);

                                                if (TextUtils.isEmpty(spbsRef)) {
                                                    if (bpnQuantity != null) {
                                                        spbsLine.setQuantity(bpnQuantity.getQuantity());

                                                        if (destType.equalsIgnoreCase("LANGSIR")) {
                                                            spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                                        }
                                                    }
                                                }

                                                database.updateData(spbsLine,
                                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                                SPBSLine.XML_ID + "=?",
                                                        new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});


                                                if (!TextUtils.isEmpty(spbsRef)) {
                                                    String number = spbsRef.split("_")[0];
                                                    String lineId = spbsRef.split("_")[1];

                                                    List<Object> listObjectRef = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                                    SPBSLine.XML_ID + "= ?",
                                                            new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode(), lineId},
                                                            null, null, null, null);

                                                    if (listObjectRef != null && listObjectRef.size() > 0) {
                                                        for (int a = 0; a < listObjectRef.size(); a++) {
                                                            SPBSLine spbsLineRef = (SPBSLine) listObjectRef.get(a);

                                                            spbsLineRef.setSpbsNext(realSPBSNumber + "_" + spbsLine.getId());

                                                            //Get Total QtyAngkut from another SPBS where source from Langsir with SPBSRef
                                                            List<Object> listObjectSPBSRefLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                            SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                            SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                                    new String[]{year, companyCode, estate, spbsRef, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                                            spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                                                    null, null, null, null);

                                                            double qtyTotalAngkutLangsir = 0;
                                                            for (int b = 0; b < listObjectSPBSRefLangsir.size(); b++) {
                                                                SPBSLine spbsLineRefLangsir = (SPBSLine) listObjectSPBSRefLangsir.get(a);
                                                                qtyTotalAngkutLangsir = (double) (qtyTotalAngkutLangsir + spbsLineRefLangsir.getQuantityAngkut());
                                                            }

                                                            double qtyJanjangRemaining = 0;
                                                            qtyJanjangRemaining = (double) (spbsLineRef.getQuantityAngkut() - qtyTotalAngkutLangsir);

                                                            spbsLineRef.setQuantityRemaining(qtyJanjangRemaining);

                                                            database.updateData(spbsLineRef,
                                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                            SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
                                                                            SPBSLine.XML_ID + " = ? " + " and " +
                                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                            SPBSLine.XML_ACHIEVEMENT_CODE + " = ?",
                                                                    new String[]{year, companyCode, estate, spbsLineRef.getSpbsNumber(), String.valueOf(spbsLineRef.getId()),
                                                                            spbsSummary.getBlock(), crop, spbsLine.getTph(), spbsLine.getBpnDate(), spbsLineRef.getAchievementCode()});
                                                        }
                                                    }
                                                }
                                                //Mpdified by Fernando --> New Load SPBS, SPBS Number tidak disimpan di BPN Header
											/*else{
												BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
													BPNHeader.XML_BPN_ID + "=?",
													new String [] {bpnId},
													null, null, null, null);

												if(bpnHeader != null){
													bpnHeader.setSpbsNumber(realSPBSNumber);

													database.updateData(bpnHeader,
														BPNHeader.XML_BPN_ID + "=?",
														new String [] {bpnId});
												}
											}*/

                                                //Mpdified by Fernando -> Update BPNQuantity column QuantityRemaining
                                                if (TextUtils.isEmpty(spbsRef)) {
                                                    double qtyJanjangRemaining = 0;
                                                    if (bpnQuantity != null) {
                                                        qtyJanjangRemaining = (double) (bpnQuantity.getQuantityRemaining() - spbsLine.getQuantityAngkut());
                                                        bpnQuantity.setQuantityRemaining(qtyJanjangRemaining);
                                                        bpnQuantity.setModifiedDate(new Date().getTime());
                                                        bpnQuantity.setModifiedBy(clerk);
                                                        bpnQuantity.setModifiedDateStr(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));

                                                        database.updateData(bpnQuantity,
                                                                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                                        BPNQuantity.XML_TPH + "=?" + " and " +
                                                                        BPNQuantity.XML_CROP + "=?" + " and " +
                                                                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                                new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()});
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //							if(destType.equalsIgnoreCase("MILL")){
                                    database.deleteData(SPBSRunningNumber.TABLE_NAME,
                                            SPBSRunningNumber.XML_ID + "=?" + " and " +
                                                    SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                                                    SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                                                    SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                                                    SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                                    SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                                            new String[]{String.valueOf(id), estate, year, month, imei, alias});

                                    database.setData(new SPBSRunningNumber(0, id, estate, divisionRun, year, month, imei, String.valueOf(lastNumber), alias));
                                    //							}

                                    database.deleteData(DeviceAlias.TABLE_NAME, null, null);
                                    database.setData(new DeviceAlias(alias));

                                    database.commitTransaction();

                                    txtSpbsNumber.setText(realSPBSNumber);
                                    spbsNumber = realSPBSNumber;
                                    isSave = 1;
                                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                            getResources().getString(R.string.save_successed), false).show();
                                } else {
                                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                            getResources().getString(R.string.langsir_already_exist), false).show();
                                }
                            } catch (SQLiteException e) {
                                e.printStackTrace();
                                database.closeTransaction();
                                new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                        e.getMessage(), false).show();
                            } finally {
                                database.closeTransaction();
                            }
                        }
                    } else {
                        new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.Invalid_data), false).show();
                    }
                } else {
                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnActionBarPrint:
                if (isSave == 1) {
                    //SaveSPBSBeforePrint();
                    connectToPrinter();

                } else {
                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_printing), false).show();
                }
                break;
            case R.id.txtSpbsDest:
                if (status == 0 && isSave == 0) {
                    startActivityForResult(new Intent(SPBSDatecsActivity.this, MasterSPBSDestinationActivity.class)
                                    .putExtra(SPBSDestination.XML_ESTATE, estate)
                                    .putExtra(Constanta.SEARCH, true),
                            MST_DEST);
                }

//			else{
//				new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi),
//						getResources().getString(R.string.data_already_export), false).show();
//			}
                break;
            case R.id.txtSpbsDivision:
                if (status == 0) {
                    startActivityForResult(new Intent(SPBSDatecsActivity.this, MasterDivisionAssistantActivity.class)
                                    .putExtra(DivisionAssistant.XML_ESTATE, estate)
                                    .putExtra(Constanta.SEARCH, true)
                                    .putExtra(BPNHeader.XML_CROP, crop)
                                    .putExtra("SPBSActivity", "SPBSDatecsActivity"),
                            MST_ASS);
                } else {
                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnSpbsDriverIn:
                startActivityForResult(new Intent(SPBSDatecsActivity.this, MasterEmployeeActivity.class)
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, "70")
                                .putExtra(Employee.XML_GANG, "")
                                .putExtra(Employee.XML_NIK, listNikFilter)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(Constanta.TYPE, EmployeeType.DRIVER.getId())
                                .putExtra(BPNHeader.XML_CROP, crop)
                                .putExtra("Activity", "SP")
                                .putExtra("SPBSActivity", "SPBSDatecsActivity"),
                        MST_DRIVER);
                break;
            case R.id.btnSpbsDriverEx:
                startActivityForResult(new Intent(SPBSDatecsActivity.this, DriverExternalActivity.class)
                                .putExtra(Employee.XML_NAME, driver)
                                .putExtra(BPNHeader.XML_CROP, crop)
                                .putExtra("SPBSActivity", "SPBSDatecsActivity")
                        , INP_DRIVER_EX);
                break;
            case R.id.txtSpbsKernet:
                startActivityForResult(new Intent(SPBSDatecsActivity.this, SPBSKernetActivity.class)
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, "70")
                                .putExtra(Employee.XML_GANG, "")
                                .putExtra(Employee.XML_NIK, nikKernet)
                                .putExtra(Employee.XML_NAME, kernet)
                                .putExtra(BPNHeader.XML_CROP, crop)
                                .putExtra("Activity", "SP")
                                .putExtra("SPBSActivity", "SPBSDatecsActivity"),
                        MST_KERNET);
                break;
            case R.id.btnSpbsLicensePlateIn:
                startActivityForResult(new Intent(SPBSDatecsActivity.this, MasterVehicleActivity.class)
                                .putExtra(RunningAccount.XML_COMPANY_CODE, companyCode)
                                .putExtra(RunningAccount.XML_ESTATE, estate)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(BPNHeader.XML_CROP, crop)
                                .putExtra("SPBSActivity", "SPBSDatecsActivity"),
                        MST_RUN_ACC);
                break;
            case R.id.btnSpbsLicensePlateEx:
                startActivityForResult(new Intent(SPBSDatecsActivity.this, LicensePlateExternalActivity.class)
                                .putExtra(RunningAccount.XML_LICENSE_PLATE, licensePlate)
                                .putExtra(BPNHeader.XML_CROP, crop)
                                .putExtra("SPBSActivity", "SPBSDatecsActivity"),
                        INP_LICENSE_EX);
                break;
            case R.id.btnSpbsLoadBpn:
                if (status == 0) {
                    if (!TextUtils.isEmpty(nikAssistant)) {
                        new DialogConfirm(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.data_reseted), null, R.id.btnSpbsLoadBpn).show();
                    } else {
                        new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.select_assistant), false).show();
                    }
                } else {
                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnSpbsAddBlock:
                if (status == 0) {
                    if (!TextUtils.isEmpty(division)) {
                        startActivityForResult(new Intent(SPBSDatecsActivity.this, MasterBlockHdrcActivity.class)
                                        .putExtra(BlockHdrc.XML_COMPANY_CODE, companyCode)
                                        .putExtra(BlockHdrc.XML_ESTATE, estate)
                                        .putExtra(BlockHdrc.XML_DIVISION, division)
                                        .putExtra(Constanta.SEARCH, true),
                                MST_BLOCK);
                    } else {
                        new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.select_assistant), false).show();
                    }
                } else {
                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            default:
                break;
        }
    }

    //add by adit - SaveBeforePriny
    private void SaveSPBSBeforePrint() {
        int isError = 0;
        if (status == 0) {
            if (listSpbsSummary.size() > 0 && !TextUtils.isEmpty(division) &&
                    !TextUtils.isEmpty(nikDriver) && !TextUtils.isEmpty(licensePlate) && !TextUtils.isEmpty(alias)) {
                if (!isNew) {
                    new DialogConfirm(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                } else {
                    try {
                        String realSPBSNumber = new NomorFormat(companyCode, estate, division, year, monthAlphabet, String.valueOf(alias), String.valueOf(lastNumber)).getSPBSFormat();

                        database.openTransaction();
//						database.setData(new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPBSNumber, spbsDate, destId, destDesc, destType, division,
//								nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
//								gpsKoordinat, 1, 0, todayDate, clerk, 0, ""));

                        //Fernando -> Check tidak boleh ada >1 Langsir dari 1 BPN
                        boolean checkLangsir = false;
                        if (destType.equalsIgnoreCase("LANGSIR")) {
                            List<Object> listspbsheaderLangsir = database.getListData(false, SPBSHeader.TABLE_NAME, null,
                                    SPBSHeader.XML_YEAR + "=?" + " and " +
                                            SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                                            SPBSHeader.XML_CROP + "=?" + " and " +
                                            SPBSHeader.XML_DEST_TYPE + "=?",
                                    new String[]{year, companyCode, estate, crop, "LANGSIR"},
                                    null, null, null, null);

                            if (listspbsheaderLangsir.size() > 0) {
                                for (int m = 0; m < listspbsheaderLangsir.size(); m++) {
                                    SPBSHeader spbsheaderLangsir = (SPBSHeader) listspbsheaderLangsir.get(m);

                                    List<Object> listspbslineLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?",
                                            new String[]{year, companyCode, estate, crop, spbsheaderLangsir.getSpbsNumber()},
                                            null, null, null, null);

                                    double qtyLangsirRemain = 0;
                                    if (listspbslineLangsir.size() > 0) {
                                        for (int n = 0; n < listspbslineLangsir.size(); n++) {
                                            SPBSLine spbslineLangsir = (SPBSLine) listspbslineLangsir.get(n);
                                            qtyLangsirRemain = (double) (qtyLangsirRemain + spbslineLangsir.getQuantityRemaining());
                                        }
                                    }

                                    if (qtyLangsirRemain > 0)
                                        checkLangsir = true;
                                }
                            }
                        }

                        if (!checkLangsir) {


                            long result = database.setData(new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPBSNumber, spbsDate, destId, destDesc, destType, division,
                                    nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                    gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr));
                            if (result == 0) {
                                //update
                                //update spbs hdr
                                SPBSHeader objSpbsHdr = new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPBSNumber, spbsDate, destId, destDesc, destType, division,
                                        nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                        gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr);
                                database.updateData(objSpbsHdr,
                                        SPBSHeader.XML_IMEI + "=?" + " and " +
                                                SPBSHeader.XML_YEAR + "=?" + " and " +
                                                SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                SPBSHeader.XML_CROP + "=?" + " and " +
                                                SPBSHeader.XML_SPBS_NUMBER + "=?",
                                        new String[]{imei, year, companyCode, estate, crop, spbsNumber}
                                );
                            }

                            for (int i = 0; i < listSpbsSummary.size(); i++) {
                                SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);

                                List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?",
                                        new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
                                        null, null, null, null);

                                if (listObject.size() > 0) {
                                    for (int j = 0; j < listObject.size(); j++) {
                                        SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                        spbsLine.setSpbsNumber(realSPBSNumber);
                                        spbsLine.setIsSave(1);

                                        //Fernando, New SPBS Load --> Save New - 2
                                        String bpnId = spbsLine.getBpnId();
                                        BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                        BPNQuantity.XML_TPH + "=?" + " and " +
                                                        BPNQuantity.XML_CROP + "=?" + " and " +
                                                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()},
                                                null, null, null, null);

                                        String spbsRef = spbsLine.getSpbsRef();
                                        if (TextUtils.isEmpty(spbsRef)) {
                                            if (bpnQuantity != null) {
                                                spbsLine.setQuantity(bpnQuantity.getQuantity());

                                                if (destType.equalsIgnoreCase("LANGSIR")) {
                                                    spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                                }
                                            }
                                        }

                                        database.updateData(spbsLine,
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                        SPBSLine.XML_CROP + "=?" + " and " +
                                                        SPBSLine.XML_TPH + "=?" + " and " +
                                                        SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ID + "=?",
                                                new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                        spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});

                                        if (!TextUtils.isEmpty(spbsRef)) {
                                            String number = spbsRef.split("_")[0];
                                            String lineId = spbsRef.split("_")[1];

                                            List<Object> listObjectRef = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                            SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ID + "= ?",
                                                    new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                            spbsLine.getBpnDate(), spbsLine.getAchievementCode(), lineId},
                                                    null, null, null, null);

                                            if (listObjectRef != null && listObjectRef.size() > 0) {
                                                for (int a = 0; a < listObjectRef.size(); a++) {
                                                    SPBSLine spbsLineRef = (SPBSLine) listObjectRef.get(a);

                                                    spbsLineRef.setSpbsNext(realSPBSNumber + "_" + spbsLine.getId());

                                                    //Get Total QtyAngkut from another SPBS where source from Langsir with SPBSRef
                                                    List<Object> listObjectSPBSRefLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                    SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                            new String[]{year, companyCode, estate, spbsRef, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                                            null, null, null, null);

                                                    double qtyTotalAngkutLangsir = 0;
                                                    for (int b = 0; b < listObjectSPBSRefLangsir.size(); b++) {
                                                        SPBSLine spbsLineRefLangsir = (SPBSLine) listObjectSPBSRefLangsir.get(a);
                                                        qtyTotalAngkutLangsir = (double) (qtyTotalAngkutLangsir + spbsLineRefLangsir.getQuantityAngkut());
                                                    }

                                                    double qtyJanjangRemaining = 0;
                                                    qtyJanjangRemaining = (double) (spbsLineRef.getQuantityAngkut() - qtyTotalAngkutLangsir);

                                                    spbsLineRef.setQuantityRemaining(qtyJanjangRemaining);

                                                    database.updateData(spbsLineRef,
                                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                    SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
                                                                    SPBSLine.XML_ID + " = ? " + " and " +
                                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                    SPBSLine.XML_ACHIEVEMENT_CODE + " = ?",
                                                            new String[]{year, companyCode, estate, spbsLineRef.getSpbsNumber(), String.valueOf(spbsLineRef.getId()),
                                                                    spbsSummary.getBlock(), crop, spbsLine.getTph(), spbsLine.getBpnDate(), spbsLineRef.getAchievementCode()});
                                                }
                                            }
                                        }

                                        //Mpdified by Fernando --> New Load SPBS, SPBS Number tidak disimpan di BPN Header
										/*else {
											BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
													BPNHeader.XML_BPN_ID + "=?",
													new String[]{bpnId},
													null, null, null, null);

											if (bpnHeader != null) {
												bpnHeader.setSpbsNumber(realSPBSNumber);

												database.updateData(bpnHeader,
														BPNHeader.XML_BPN_ID + "=?",
														new String[]{bpnId});
											}
										}*/

                                        //Mpdified by Fernando -> Update BPNQuantity column QuantityRemaining
                                        if (TextUtils.isEmpty(spbsRef)) {
                                            double qtyJanjangRemaining = 0;
                                            if (bpnQuantity != null) {
                                                qtyJanjangRemaining = (double) (bpnQuantity.getQuantityRemaining() - spbsLine.getQuantityAngkut());
                                                bpnQuantity.setQuantityRemaining(qtyJanjangRemaining);
                                                bpnQuantity.setModifiedDate(new Date().getTime());
                                                bpnQuantity.setModifiedBy(clerk);
                                                bpnQuantity.setModifiedDateStr(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));

                                                database.updateData(bpnQuantity,
                                                        BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                                BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                                BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                                BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                                BPNQuantity.XML_TPH + "=?" + " and " +
                                                                BPNQuantity.XML_CROP + "=?" + " and " +
                                                                BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                        new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()});
                                            }
                                        }
                                    }
                                }
                            }

//							if(destType.equalsIgnoreCase("MILL")){
                            database.deleteData(SPBSRunningNumber.TABLE_NAME,
                                    SPBSRunningNumber.XML_ID + "=?" + " and " +
                                            SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                                            SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                                            SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                                            SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                            SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                                    new String[]{String.valueOf(id), estate, year, month, imei, alias});

                            database.setData(new SPBSRunningNumber(0, id, estate, divisionRun, year, month, imei, String.valueOf(lastNumber), alias));
//							}

                            database.deleteData(DeviceAlias.TABLE_NAME, null, null);
                            database.setData(new DeviceAlias(alias));

                            database.commitTransaction();

                            txtSpbsNumber.setText(realSPBSNumber);
                            spbsNumber = realSPBSNumber;
                            isSave = 1;
                            new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.save_successed), false).show();


                        } else {
                            new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.langsir_already_exist), false).show();

                        }
                    } catch (SQLiteException e) {
                        isError += 1;
                        e.printStackTrace();
                        database.closeTransaction();
                        new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                e.getMessage(), false).show();
                    } finally {
                        database.closeTransaction();
                    }

                    if (isError <= 0)
                        connectToPrinter();
                }
            } else {
                new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                        getResources().getString(R.string.Invalid_data), false).show();
            }
        } else {
            new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                    getResources().getString(R.string.data_already_export), false).show();
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSSummary>, List<SPBSSummary>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!isFinishing()) {
                dialogProgress = new DialogProgress(SPBSDatecsActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<SPBSSummary> doInBackground(Void... voids) {
            List<SPBSSummary> listTemp = new ArrayList<SPBSSummary>();

            database.openTransaction();
            List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, new String[]{SPBSLine.XML_BLOCK},
                    SPBSLine.XML_YEAR + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?" + " and " +
                            SPBSLine.XML_CROP + "=?" + " and " +
                            SPBSLine.XML_SPBS_NUMBER + "=?",
                    new String[]{year, companyCode, estate, crop, spbsNumber},
                    SPBSLine.XML_BLOCK, null, null, null);

            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    SPBSLine spbsLine = (SPBSLine) listObject.get(i);

                    List<Object> listQtyJanjang = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                            new String[]{year, companyCode, estate, crop, spbsNumber, spbsLine.getBlock(), BPNQuantity.JANJANG_CODE},
                            null, null, null, null);

                    List<Object> listQtyLooseFruit = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                            new String[]{year, companyCode, estate, crop, spbsNumber, spbsLine.getBlock(), BPNQuantity.LOOSE_FRUIT_CODE},
                            null, null, null, null);

                    double qtyJanjang = 0;
                    double qtyLooseFruit = 0;

                    if (listQtyJanjang.size() > 0) {
                        for (int a = 0; a < listQtyJanjang.size(); a++) {
                            SPBSLine spbsTemp = (SPBSLine) listQtyJanjang.get(a);

                            qtyJanjang = qtyJanjang + spbsTemp.getQuantityAngkut();
                        }
                    }

                    if (listQtyLooseFruit.size() > 0) {
                        for (int a = 0; a < listQtyLooseFruit.size(); a++) {
                            SPBSLine spbsTemp = (SPBSLine) listQtyLooseFruit.get(a);

                            qtyLooseFruit = qtyLooseFruit + spbsTemp.getQuantityAngkut();
                        }
                    }

                    listTemp.add(new SPBSSummary(spbsNumber, spbsDate, spbsLine.getBlock(), qtyJanjang, qtyLooseFruit));
                }
            }
            database.closeTransaction();

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<SPBSSummary> listTemp) {
            super.onPostExecute(listTemp);

            if (dialogProgress != null && dialogProgress.isShowing()) {
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            for (SPBSSummary spbsSummary : listTemp) {
                adapter.addOrUpdateData(spbsSummary);
            }
        }
    }

    @Override
    public void onBackPressed() {
        new DialogConfirm(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                getResources().getString(R.string.exit), null, 1).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED) {
            getDataAsync.cancel(true);
        }

        if (mBtSocket != null && mBtSocket.isConnected())
            try {
                mBtSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == MST_ASS) {
                division = data.getExtras().getString(DivisionAssistant.XML_DIVISION);

                String[] separated = division.split(",");
                if (separated.length == 2) {
                    division = separated[0];
                    lifnr = separated[1];
                }

                nikAssistant = "-";
                assistant = data.getExtras().getString(DivisionAssistant.XML_ASSISTANT_NAME, "");
                txtSpbsDivision.setText(data.getExtras().getString(DivisionAssistant.XML_DIVISION));
                txtSpbsAssistant.setText(assistant);
            } else if (requestCode == MST_BLOCK) {
                String block = data.getExtras().getString(BlockHdrc.XML_BLOCK, "");

                if (!TextUtils.isEmpty(block)) {
                    boolean found = false;
                    for (int i = 0; i < listSpbsSummary.size(); i++) {
                        SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);

                        if (block.equalsIgnoreCase(spbsSummary.getBlock())) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        if (adapter == null) {
                            listSpbsSummary = new ArrayList<SPBSSummary>();
                            listSpbsSummary.add(new SPBSSummary(spbsNumber, spbsDate, block, 0, 0));
                            adapter = new AdapterSPBSSummary(SPBSDatecsActivity.this, listSpbsSummary, R.layout.item_spbs_summary);
                            lsvSpbsSummary.setAdapter(adapter);
                        } else {
                            adapter.addOrUpdateData(new SPBSSummary(spbsNumber, spbsDate, block, 0, 0));
                        }
                    }
                }
            } else if (requestCode == MST_TPH) {
                getDataAsync = new GetDataAsyncTask();
                getDataAsync.execute();
            } else if (requestCode == MST_DRIVER || requestCode == INP_DRIVER_EX) {
                nikDriver = data.getExtras().getString(Employee.XML_NIK);
                driver = data.getExtras().getString(Employee.XML_NAME);
                txtSpbsDriver.setText(driver);
            } else if (requestCode == MST_KERNET) {
                nikKernet = data.getExtras().getString(Employee.XML_NIK);
                kernet = data.getExtras().getString(Employee.XML_NAME);
                txtSpbsKernet.setText(kernet);
            } else if (requestCode == MST_RUN_ACC || requestCode == INP_LICENSE_EX) {
                runningAccount = data.getExtras().getString(RunningAccount.XML_RUNNING_ACCOUNT);
                licensePlate = data.getExtras().getString(RunningAccount.XML_LICENSE_PLATE);
                txtSpbsLicensePlate.setText(licensePlate);
            } else if (requestCode == MST_DEST) {
                destId = data.getExtras().getString(SPBSDestination.XML_DEST_ID);
                destDesc = data.getExtras().getString(SPBSDestination.XML_DEST_DESC);
                destType = data.getExtras().getString(SPBSDestination.XML_DEST_TYPE);
                txtSpbsDest.setText(destDesc);
            }
        }
    }

    @Override
    public void onOK(boolean is_finish) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConfirmOK(Object object, int id) {
        switch (id) {
            case R.id.btnActionBarRight:
                long todayDate = new Date().getTime();

                try {
                    database.openTransaction();

                    database.updateData(new SPBSHeader(0, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, destId, destDesc, destType, division,
                                    nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                    gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr),
                            SPBSHeader.XML_YEAR + "=?" + " and " +
                                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSHeader.XML_ESTATE + "=?" + " and " +
                                    SPBSHeader.XML_CROP + "=?" + " and " +
                                    SPBSHeader.XML_SPBS_NUMBER + "=?",
                            new String[]{year, companyCode, estate, crop, spbsNumber});

                    for (int i = 0; i < listSpbsSummary.size(); i++) {
                        SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);

                        List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                SPBSLine.XML_YEAR + "=?" + " and " +
                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                        SPBSLine.XML_CROP + "=?",
                                new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
                                null, null, null, null);

                        if (listObject.size() > 0) {
                            for (int j = 0; j < listObject.size(); j++) {
                                SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                spbsLine.setIsSave(1);
                                spbsLine.setModifiedDate(todayDate);
                                spbsLine.setModifiedBy(clerk);

                                String bpnId = spbsLine.getBpnId();

                                //Fernando, New SPBS Load --> Update Data - 2
                                BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                        BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                BPNQuantity.XML_TPH + "=?" + " and " +
                                                BPNQuantity.XML_CROP + "=?" + " and " +
                                                BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                        new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()},
                                        null, null, null, null);

                                String spbsRef = spbsLine.getSpbsRef();
                                String spbsNext = spbsLine.getSpbsNext();
                                if (TextUtils.isEmpty(spbsRef)) {
                                    if (bpnQuantity != null && TextUtils.isEmpty(spbsNext)) {
                                        spbsLine.setQuantity(bpnQuantity.getQuantity());

                                        //Tidak boleh diedit lagi Qty Angkut awal yang tujuanya Langsir, sedangkan data Langsir sudah disebar
                                        if (destType.equalsIgnoreCase("LANGSIR")) {
                                            spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                        }
                                    }
                                } else {
                                    String number = spbsRef.split("_")[0];
                                    SPBSLine spbsLangsir = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                            new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                            null, null, null, null);

                                    if (spbsLangsir != null) {
                                        spbsLine.setQuantity(spbsLangsir.getQuantityAngkut());
                                    }
                                    spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                }

                                database.updateData(spbsLine,
                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                SPBSLine.XML_ID + "=?",
                                        new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});


                                if (!TextUtils.isEmpty(spbsRef)) {
                                    String number = spbsRef.split("_")[0];
                                    String lineId = spbsRef.split("_")[1];

                                    List<Object> listObjectRef = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ID + "= ?",
                                            new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode(), lineId},
                                            null, null, null, null);

                                    if (listObjectRef != null && listObjectRef.size() > 0) {
                                        for (int a = 0; a < listObjectRef.size(); a++) {
                                            SPBSLine spbsLineRef = (SPBSLine) listObjectRef.get(a);

                                            //Get Total QtyAngkut from another SPBS where source from Langsir with SPBSRef
                                            List<Object> listObjectSPBSRefLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                            SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                    new String[]{year, companyCode, estate, spbsRef, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                            spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                                    null, null, null, null);

                                            double qtyTotalAngkutLangsir = 0;
                                            for (int b = 0; b < listObjectSPBSRefLangsir.size(); b++) {
                                                SPBSLine spbsLineRefLangsir = (SPBSLine) listObjectSPBSRefLangsir.get(b);
                                                qtyTotalAngkutLangsir = (double) (qtyTotalAngkutLangsir + spbsLineRefLangsir.getQuantityAngkut());
                                            }

                                            double qtyJanjangRemaining = 0;
                                            qtyJanjangRemaining = (double) (spbsLineRef.getQuantityAngkut() - qtyTotalAngkutLangsir);

                                            spbsLineRef.setQuantityRemaining(qtyJanjangRemaining);

                                            if (TextUtils.isEmpty(spbsLineRef.getSpbsNext()))
                                                spbsLineRef.setSpbsNext(spbsLine.getSpbsNumber() + "_" + spbsLine.getId());

                                            database.updateData(spbsLineRef,
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
                                                            SPBSLine.XML_ID + " = ? " + " and " +
                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                            SPBSLine.XML_ACHIEVEMENT_CODE + " = ?",
                                                    new String[]{year, companyCode, estate, spbsLineRef.getSpbsNumber(), String.valueOf(spbsLineRef.getId()),
                                                            spbsSummary.getBlock(), crop, spbsLine.getTph(), spbsLine.getBpnDate(), spbsLineRef.getAchievementCode()});
                                        }
                                    }
                                }


                                if (!TextUtils.isEmpty(bpnId)) {
								/*BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
									BPNHeader.XML_BPN_ID + "=?",
									new String [] {bpnId},
									null, null, null, null);

								if(bpnHeader != null){
									bpnHeader.setSpbsNumber(spbsNumber);

									database.updateData(bpnHeader,
										BPNHeader.XML_BPN_ID + "=?",
										new String [] {bpnId});
								}*/

                                    //Modified By Fernando -> Update BPNQuantity column QuantityRemaining
                                    if (TextUtils.isEmpty(spbsRef) && TextUtils.isEmpty(spbsNext)) {
                                        double qtyAngkutCurrent = 0;
                                        List<Object> listSPBSLineCurrent = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                SPBSLine.XML_BPN_ID + "=?" + " and " +
                                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                        SPBSLine.XML_CROP + "=?" + " and " +
                                                        SPBSLine.XML_TPH + "=?" + " and " +
                                                        SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                new String[]{bpnId, year, companyCode, estate, crop, spbsLine.getTph(), bpnQuantity.getBpnDate(), spbsSummary.getBlock(), "", spbsLine.getAchievementCode()},
                                                null, null, null, null);
                                        for (int k = 0; k < listSPBSLineCurrent.size(); k++) {
                                            SPBSLine spbsLineCurrent = (SPBSLine) listSPBSLineCurrent.get(k);
                                            qtyAngkutCurrent = qtyAngkutCurrent + spbsLineCurrent.getQuantityAngkut();
                                        }

                                        double qtyJanjangRemaining = 0;
                                        if (bpnQuantity != null) {

                                            //1
                                            double penalty = getPenalty(bpnId, companyCode, estate, spbsLine.getBpnDate(), bpnQuantity.getDivision(), bpnQuantity.getGang(), spbsSummary.getBlock(), spbsLine.getAchievementCode());
                                            qtyJanjangRemaining = (double) ((bpnQuantity.getQuantity() - penalty) - qtyAngkutCurrent);
                                            bpnQuantity.setQuantityRemaining(qtyJanjangRemaining);
                                            bpnQuantity.setModifiedDate(new Date().getTime());
                                            bpnQuantity.setModifiedBy(clerk);
                                            bpnQuantity.setModifiedDateStr(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));

                                            database.updateData(bpnQuantity,
                                                    BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                            BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                            BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                            BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                            BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                            BPNQuantity.XML_TPH + "=?" + " and " +
                                                            BPNQuantity.XML_CROP + "=?" + " and " +
                                                            BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                    new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()});
                                        }
                                    }
                                }

                            }
                        }
                    }

                    database.commitTransaction();

                    txtSpbsNumber.setText(spbsNumber);

                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_successed), false).show();
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();
                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            e.getMessage(), false).show();
                } finally {
                    database.closeTransaction();
                }
                break;

            case R.id.btnSpbsLoadBpn:
                new DialogDate(SPBSDatecsActivity.this, getResources().getString(R.string.tanggal), new Date(), R.id.btnSpbsLoadBpn).show();
                break;

            case R.id.btnActionBarPrint:
                //update data
                todayDate = new Date().getTime();

                try {
                    database.openTransaction();

                    database.updateData(new SPBSHeader(0, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, destId, destDesc, destType, division,
                                    nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                    gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr),
                            SPBSHeader.XML_YEAR + "=?" + " and " +
                                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSHeader.XML_ESTATE + "=?" + " and " +
                                    SPBSHeader.XML_CROP + "=?" + " and " +
                                    SPBSHeader.XML_SPBS_NUMBER + "=?",
                            new String[]{year, companyCode, estate, crop, spbsNumber});

                    for (int i = 0; i < listSpbsSummary.size(); i++) {
                        SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);

                        List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                SPBSLine.XML_YEAR + "=?" + " and " +
                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                        SPBSLine.XML_CROP + "=?",
                                new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
                                null, null, null, null);

                        if (listObject.size() > 0) {
                            for (int j = 0; j < listObject.size(); j++) {
                                SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                spbsLine.setIsSave(1);
                                spbsLine.setModifiedDate(todayDate);
                                spbsLine.setModifiedBy(clerk);

                                String bpnId = spbsLine.getBpnId();

                                //Fernando, New SPBS Load --> Update Data - 2
                                BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                        BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                BPNQuantity.XML_TPH + "=?" + " and " +
                                                BPNQuantity.XML_CROP + "=?" + " and " +
                                                BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                        new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()},
                                        null, null, null, null);

                                String spbsRef = spbsLine.getSpbsRef();
                                String spbsNext = spbsLine.getSpbsNext();
                                if (TextUtils.isEmpty(spbsRef)) {
                                    if (bpnQuantity != null && TextUtils.isEmpty(spbsNext)) {
                                        spbsLine.setQuantity(bpnQuantity.getQuantity());

                                        //Tidak boleh diedit lagi Qty Angkut awal yang tujuanya Langsir, sedangkan data Langsir sudah disebar
                                        if (destType.equalsIgnoreCase("LANGSIR")) {
                                            spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                        }
                                    }
                                } else {
                                    String number = spbsRef.split("_")[0];
                                    SPBSLine spbsLangsir = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                            new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                            null, null, null, null);

                                    if (spbsLangsir != null) {
                                        spbsLine.setQuantity(spbsLangsir.getQuantityAngkut());
                                    }
                                    spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                }

                                database.updateData(spbsLine,
                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                SPBSLine.XML_ID + "=?",
                                        new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});

                                if (!TextUtils.isEmpty(spbsRef)) {
                                    String number = spbsRef.split("_")[0];
                                    String lineId = spbsRef.split("_")[1];

                                    List<Object> listObjectRef = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ID + "= ?",
                                            new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode(), lineId},
                                            null, null, null, null);

                                    if (listObjectRef != null && listObjectRef.size() > 0) {
                                        for (int a = 0; a < listObjectRef.size(); a++) {
                                            SPBSLine spbsLineRef = (SPBSLine) listObjectRef.get(a);

                                            //Get Total QtyAngkut from another SPBS where source from Langsir with SPBSRef
                                            List<Object> listObjectSPBSRefLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                            SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                    new String[]{year, companyCode, estate, spbsRef, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                            spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                                    null, null, null, null);

                                            double qtyTotalAngkutLangsir = 0;
                                            for (int b = 0; b < listObjectSPBSRefLangsir.size(); b++) {
                                                SPBSLine spbsLineRefLangsir = (SPBSLine) listObjectSPBSRefLangsir.get(b);
                                                qtyTotalAngkutLangsir = (double) (qtyTotalAngkutLangsir + spbsLineRefLangsir.getQuantityAngkut());
                                            }

                                            double qtyJanjangRemaining = 0;
                                            qtyJanjangRemaining = (double) (spbsLineRef.getQuantityAngkut() - qtyTotalAngkutLangsir);

                                            spbsLineRef.setQuantityRemaining(qtyJanjangRemaining);

                                            if (TextUtils.isEmpty(spbsLineRef.getSpbsNext()))
                                                spbsLineRef.setSpbsNext(spbsLine.getSpbsNumber() + "_" + spbsLine.getId());

                                            database.updateData(spbsLineRef,
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
                                                            SPBSLine.XML_ID + " = ? " + " and " +
                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                            SPBSLine.XML_ACHIEVEMENT_CODE + " = ?",
                                                    new String[]{year, companyCode, estate, spbsLineRef.getSpbsNumber(), String.valueOf(spbsLineRef.getId()),
                                                            spbsSummary.getBlock(), crop, spbsLine.getTph(), spbsLine.getBpnDate(), spbsLineRef.getAchievementCode()});
                                        }
                                    }
                                }

                                if (!TextUtils.isEmpty(bpnId)) {
                                    //Modified By Fernando -> SPBS Number tidak disimpan ke BPNHeader
									/*BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
											BPNHeader.XML_BPN_ID + "=?",
											new String [] {bpnId},
											null, null, null, null);

									if(bpnHeader != null){
										bpnHeader.setSpbsNumber(spbsNumber);

										database.updateData(bpnHeader,
												BPNHeader.XML_BPN_ID + "=?",
												new String [] {bpnId});
									}*/

                                    //Modified By Fernando -> Update BPNQuantity column QuantityRemaining
                                    if (TextUtils.isEmpty(spbsRef) && TextUtils.isEmpty(spbsNext)) {
                                        double qtyAngkutCurrent = 0;
                                        List<Object> listSPBSLineCurrent = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                SPBSLine.XML_BPN_ID + "=?" + " and " +
                                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                        SPBSLine.XML_CROP + "=?" + " and " +
                                                        SPBSLine.XML_TPH + "=?" + " and " +
                                                        SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                new String[]{bpnId, year, companyCode, estate, crop, spbsLine.getTph(), bpnQuantity.getBpnDate(), spbsSummary.getBlock(), "", spbsLine.getAchievementCode()},
                                                null, null, null, null);
                                        for (int k = 0; k < listSPBSLineCurrent.size(); k++) {
                                            SPBSLine spbsLineCurrent = (SPBSLine) listSPBSLineCurrent.get(k);
                                            qtyAngkutCurrent = qtyAngkutCurrent + spbsLineCurrent.getQuantityAngkut();
                                        }

                                        double qtyJanjangRemaining = 0;
                                        if (bpnQuantity != null) {

                                            //2
                                            double penalty = getPenalty(bpnId, companyCode, estate, spbsLine.getBpnDate(), bpnQuantity.getDivision(), bpnQuantity.getGang(), spbsSummary.getBlock(), spbsLine.getAchievementCode());
                                            qtyJanjangRemaining = (double) ((bpnQuantity.getQuantity() - penalty) - qtyAngkutCurrent);
                                            bpnQuantity.setQuantityRemaining(qtyJanjangRemaining);
                                            bpnQuantity.setModifiedDate(new Date().getTime());
                                            bpnQuantity.setModifiedBy(clerk);
                                            bpnQuantity.setModifiedDateStr(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));

                                            database.updateData(bpnQuantity,
                                                    BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                            BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                            BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                            BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                            BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                            BPNQuantity.XML_TPH + "=?" + " and " +
                                                            BPNQuantity.XML_CROP + "=?" + " and " +
                                                            BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                    new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()});
                                        }
                                    }
                                }

                            }
                        }
                    }

                    database.commitTransaction();

                    txtSpbsNumber.setText(spbsNumber);

//					new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
//							getResources().getString(R.string.save_successed), false).show();
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();
                    new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                            e.getMessage(), false).show();
                } finally {
                    database.closeTransaction();
                }
                connectToPrinter();
                break;

            case R.id.btnSpbsSummaryItemDelete:
                try {
                    SPBSSummary spbsSummary = (SPBSSummary) object;

                    database.openTransaction();

                    List<Object> lstObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                    SPBSLine.XML_BLOCK + "=?",
                            new String[]{spbsNumber, spbsDate, spbsSummary.getBlock()},
                            null, null, null, null);

                    int delCount = 0;
                    boolean isSPBSNext = false;

                    if (lstObject != null && lstObject.size() > 0) {
                        for (Object obj : lstObject) {
                            SPBSLine spbsLine = (SPBSLine) obj;

                            String block = spbsLine.getBlock();
                            String bpnId = spbsLine.getBpnId();
                            String bpnDate = spbsLine.getBpnDate();
                            String tph = spbsLine.getTph();
                            String spbsRef = spbsLine.getSpbsRef();
                            String spbsNext = spbsLine.getSpbsNext();
                            String achievmentcode = spbsLine.getAchievementCode();

                            boolean del = false;

                            if (TextUtils.isEmpty(spbsNext)) {
                                if (!TextUtils.isEmpty(spbsRef)) {
                                    String spbsNumber = spbsRef.split("_")[0];
                                    String lineId = spbsRef.split("_")[1];

                                    SPBSLine spbsLineRef = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_ID + " = ? " + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + " = ? " + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + " =?",
                                            new String[]{lineId, spbsNumber, achievmentcode},
                                            null, null, null, null);

                                    if (spbsLineRef != null) {
                                        if (!TextUtils.isEmpty(spbsLineRef.getSpbsNext())) {
                                            //spbsLineRef.setQuantityRemaining((double) (spbsLineRef.getQuantityRemaining() + spbsLine.getQuantityAngkut()));
                                            spbsLineRef.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                            spbsLineRef.setSpbsNext("");
                                        }

                                        database.updateData(spbsLineRef,
                                                SPBSLine.XML_ID + " = ? " + " and " +
                                                        SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
                                                        SPBSLine.XML_ACHIEVEMENT_CODE + " =?",
                                                new String[]{lineId, spbsNumber, achievmentcode});

                                        del = true;
                                    }
                                } else {
								/*BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
										BPNHeader.XML_BPN_ID + "=?",
										new String [] {bpnId}, null, null, null, null);

								if(bpnHeader != null){
									bpnHeader.setSpbsNumber("");
									database.updateData(bpnHeader,
											BPNHeader.XML_BPN_ID + "=?",
											new String [] {bpnId});

									del = true;
								}*/

                                    //Fernando, New Load SPBS - Delete Item
                                    double qtyAngkutRemaining = 0;
                                    BPNQuantity bpnQuantityAngkut = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                            BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                    BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                    BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                    BPNQuantity.XML_TPH + "=?" + " and " +
                                                    BPNQuantity.XML_CROP + "=?" + " and " +
                                                    BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                            new String[]{bpnId, bpnDate, companyCode, estate, block, tph, crop, achievmentcode},
                                            null, null, null, null);

                                    if (bpnQuantityAngkut != null) {
                                        List<Object> listObjectSPBSCurrentAngkut = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                SPBSLine.XML_BPN_ID + " = ? " + " and " +
                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                        SPBSLine.XML_TPH + "=?" + " and " +
                                                        SPBSLine.XML_CROP + "=?" + " and " +
                                                        SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                new String[]{bpnId, companyCode, estate, tph, crop, "", achievmentcode},
                                                null, null, null, null);

                                        double qtyCurrentAngkut = 0;
                                        if (listObjectSPBSCurrentAngkut.size() > 0) {
                                            for (int i = 0; i < listObjectSPBSCurrentAngkut.size(); i++) {
                                                SPBSLine spbsLineCurrentAngkut = (SPBSLine) listObjectSPBSCurrentAngkut.get(i);
                                                qtyCurrentAngkut = (double) (qtyCurrentAngkut + spbsLineCurrentAngkut.getQuantityAngkut());
                                            }
                                        }

                                        //3
                                        double penalty = getPenalty(bpnId, companyCode, estate, spbsLine.getBpnDate(), bpnQuantityAngkut.getDivision(), bpnQuantityAngkut.getGang(), spbsSummary.getBlock(), spbsLine.getAchievementCode());
                                        qtyAngkutRemaining = (double) ((bpnQuantityAngkut.getQuantity() - penalty) - qtyCurrentAngkut) + spbsLine.getQuantityAngkut();
                                        bpnQuantityAngkut.setQuantityRemaining(qtyAngkutRemaining);
                                        bpnQuantityAngkut.setModifiedDate(new Date().getTime());
                                        bpnQuantityAngkut.setModifiedBy(clerk);
                                        bpnQuantityAngkut.setModifiedDateStr(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));

                                        database.updateData(bpnQuantityAngkut,
                                                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                        BPNQuantity.XML_TPH + "=?" + " and " +
                                                        BPNQuantity.XML_CROP + "=?" + " and " +
                                                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                new String[]{bpnId, bpnDate, companyCode, estate, block, tph, crop, achievmentcode});

                                        del = true;
                                    }
                                }

                                if (del) {
                                    database.deleteData(SPBSLine.TABLE_NAME,
                                            SPBSLine.XML_ID + "=?" + " and " +
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                    SPBSLine.XML_BPN_ID + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?",
                                            new String[]{String.valueOf(spbsLine.getId()), year, companyCode, estate, crop, spbsNumber, spbsDate, block, tph, achievmentcode, bpnId, spbsLine.getBpnDate()});
                                    //delCount++;
                                }
                            } else {
                                isSPBSNext = true;
                                break;
                            }
                        }
                    }

                    if (isSPBSNext) {
                        new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.data_delete_cannot_all), false).show();
                    } else {

                        database.commitTransaction();

                        List<Object> lstObjectDelete = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                        SPBSLine.XML_BLOCK + "=?",
                                new String[]{spbsNumber, spbsDate, spbsSummary.getBlock()},
                                null, null, null, null);

                        if (delCount == lstObjectDelete.size()) {
                            adapter.deleteData(spbsSummary);
                        } else {
                            new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.data_delete_cannot_all_langsir), false).show();
                        }

                    }

                } catch (SQLiteException e) {
                    e.printStackTrace();
                } finally {
                    database.closeTransaction();
                }
                break;

            default:
                if (getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED) {
                    getDataAsync.cancel(true);
                }

                if (dialogProgress != null && dialogProgress.isShowing()) {
                    dialogProgress.dismiss();
                    dialogProgress = null;
                }

                if (gps != null) {
                    gps.stopUsingGPS();
                }

                deleteUnSaved();
                gpsHandler.stopGPS();

                if (status == 0) {
                    if (listSpbsSummary.size() > 0 && !TextUtils.isEmpty(division) &&
                            !TextUtils.isEmpty(nikDriver) && !TextUtils.isEmpty(licensePlate) && !TextUtils.isEmpty(alias)) {
                        //if (!isNew) {
                        //	new DialogConfirm(SPBSActivity.this, getResources().getString(R.string.informasi),
                        //			getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                        //} else {
                        todayDate = new Date().getTime();
                        String spbsNumberCurrent = spbsNumber.substring(0, 4);
                        if (spbsNumberCurrent.equalsIgnoreCase(estate)) {
                            try {
                                database.openTransaction();
                                database.updateData(new SPBSHeader(0, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, destId, destDesc, destType, division,
                                                nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
                                                gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr),
                                        SPBSHeader.XML_YEAR + "=?" + " and " +
                                                SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                SPBSHeader.XML_CROP + "=?" + " and " +
                                                SPBSHeader.XML_SPBS_NUMBER + "=?",
                                        new String[]{year, companyCode, estate, crop, spbsNumber});

                                for (int i = 0; i < listSpbsSummary.size(); i++) {
                                    SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);

                                    List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                            SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?",
                                            new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
                                            null, null, null, null);

                                    if (listObject.size() > 0) {
                                        for (int j = 0; j < listObject.size(); j++) {
                                            SPBSLine spbsLine = (SPBSLine) listObject.get(j);

                                            spbsLine.setIsSave(1);
                                            spbsLine.setModifiedDate(todayDate);
                                            spbsLine.setModifiedBy(clerk);

                                            //Fernando, New SPBS Load --> Update Data - 1
                                            String bpnId = spbsLine.getBpnId();
                                            BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                                    BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                            BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                            BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                            BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                            BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                            BPNQuantity.XML_TPH + "=?" + " and " +
                                                            BPNQuantity.XML_CROP + "=?" + " and " +
                                                            BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                    new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()},
                                                    null, null, null, null);

                                            String spbsRef = spbsLine.getSpbsRef();
                                            String spbsNext = spbsLine.getSpbsNext();
                                            if (TextUtils.isEmpty(spbsRef)) {
                                                if (bpnQuantity != null && TextUtils.isEmpty(spbsNext)) {
                                                    spbsLine.setQuantity(bpnQuantity.getQuantity());

                                                    //Masalah jika diedit lagi Qty Angkut awal yang tujuanya Langsir, sedangkan data Langsir sudah disebar
                                                    if (destType.equalsIgnoreCase("LANGSIR")) {
                                                        spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                                    }
                                                }
                                            } else {
										/*double qtyRemaining = (double)(spbsLine.getQuantity() - spbsLine.getQuantityAngkut());
										spbsLine.setQuantityRemaining(qtyRemaining);*/

                                                String number = spbsRef.split("_")[0];
                                                SPBSLine spbsLangsir = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                        new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                                        null, null, null, null);

                                                if (spbsLangsir != null) {
                                                    spbsLine.setQuantity(spbsLangsir.getQuantityAngkut());
                                                }
                                                spbsLine.setQuantityRemaining(spbsLine.getQuantityAngkut());
                                            }

                                            database.updateData(spbsLine,
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ESTATE + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                            SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                            SPBSLine.XML_BLOCK + "=?" + " and " +
                                                            SPBSLine.XML_CROP + "=?" + " and " +
                                                            SPBSLine.XML_TPH + "=?" + " and " +
                                                            SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                            SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                            SPBSLine.XML_ID + "=?",
                                                    new String[]{year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                            spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});

                                            if (!TextUtils.isEmpty(spbsRef)) {
                                                String number = spbsRef.split("_")[0];
                                                String lineId = spbsRef.split("_")[1];

                                                List<Object> listObjectRef = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                        SPBSLine.XML_YEAR + "=?" + " and " +
                                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                                SPBSLine.XML_ID + "= ?",
                                                        new String[]{year, companyCode, estate, number, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode(), lineId},
                                                        null, null, null, null);

                                                if (listObjectRef != null && listObjectRef.size() > 0) {
                                                    for (int a = 0; a < listObjectRef.size(); a++) {
                                                        SPBSLine spbsLineRef = (SPBSLine) listObjectRef.get(a);

                                                        //Get Total QtyAngkut from another SPBS where source from Langsir with SPBSRef
                                                        List<Object> listObjectSPBSRefLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                        SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                        SPBSLine.XML_CROP + "=?" + " and " +
                                                                        SPBSLine.XML_TPH + "=?" + " and " +
                                                                        SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                                new String[]{year, companyCode, estate, spbsRef, spbsSummary.getBlock(), crop, spbsLine.getTph(),
                                                                        spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                                                null, null, null, null);

                                                        double qtyTotalAngkutLangsir = 0;
                                                        for (int b = 0; b < listObjectSPBSRefLangsir.size(); b++) {
                                                            SPBSLine spbsLineRefLangsir = (SPBSLine) listObjectSPBSRefLangsir.get(b);
                                                            qtyTotalAngkutLangsir = (double) (qtyTotalAngkutLangsir + spbsLineRefLangsir.getQuantityAngkut());
                                                        }

                                                        double qtyJanjangRemaining = 0;
                                                        qtyJanjangRemaining = (double) (spbsLineRef.getQuantityAngkut() - qtyTotalAngkutLangsir);
                                                        spbsLineRef.setQuantityRemaining(qtyJanjangRemaining);

                                                        if (TextUtils.isEmpty(spbsLineRef.getSpbsNext()))
                                                            spbsLineRef.setSpbsNext(spbsLine.getSpbsNumber() + "_" + spbsLine.getId());

                                                        database.updateData(spbsLineRef,
                                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                        SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
                                                                        SPBSLine.XML_ID + " = ? " + " and " +
                                                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                        SPBSLine.XML_CROP + "=?" + " and " +
                                                                        SPBSLine.XML_TPH + "=?" + " and " +
                                                                        SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                        SPBSLine.XML_ACHIEVEMENT_CODE + " = ?",
                                                                new String[]{year, companyCode, estate, spbsLineRef.getSpbsNumber(), String.valueOf(spbsLineRef.getId()),
                                                                        spbsSummary.getBlock(), crop, spbsLine.getTph(), spbsLine.getBpnDate(), spbsLineRef.getAchievementCode()});
                                                    }
                                                }
                                            }


                                            if (!TextUtils.isEmpty(bpnId)) {
										/*BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
												BPNHeader.XML_BPN_ID + "=?",
												new String [] {bpnId},
												null, null, null, null);

										if(bpnHeader != null){
											bpnHeader.setSpbsNumber(spbsNumber);

											database.updateData(bpnHeader,
													BPNHeader.XML_BPN_ID + "=?",
													new String [] {bpnId});
										}*/

                                                //Modified by Fernando -> Update BPN Quantity Remaining
                                                if (TextUtils.isEmpty(spbsRef) && TextUtils.isEmpty(spbsNext)) {
                                                    double qtyAngkutCurrent = 0;
                                                    List<Object> listSPBSLineCurrent = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                                            SPBSLine.XML_BPN_ID + "=?" + " and " +
                                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                                    SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                                            new String[]{bpnId, year, companyCode, estate, crop, spbsLine.getTph(), bpnQuantity.getBpnDate(), spbsSummary.getBlock(), "", spbsLine.getAchievementCode()},
                                                            null, null, null, null);
                                                    for (int k = 0; k < listSPBSLineCurrent.size(); k++) {
                                                        SPBSLine spbsLineCurrent = (SPBSLine) listSPBSLineCurrent.get(k);
                                                        qtyAngkutCurrent = qtyAngkutCurrent + spbsLineCurrent.getQuantityAngkut();
                                                    }

                                                    double qtyJanjangRemaining = 0;
                                                    if (bpnQuantity != null) {

                                                        //4
                                                        double penalty = getPenalty(bpnId, companyCode, estate, spbsLine.getBpnDate(), bpnQuantity.getDivision(), bpnQuantity.getGang(), spbsSummary.getBlock(), spbsLine.getAchievementCode());
                                                        qtyJanjangRemaining = (double) ((bpnQuantity.getQuantity() - penalty) - qtyAngkutCurrent);
                                                        bpnQuantity.setQuantityRemaining(qtyJanjangRemaining);
                                                        bpnQuantity.setModifiedDate(new Date().getTime());
                                                        bpnQuantity.setModifiedBy(clerk);
                                                        bpnQuantity.setModifiedDateStr(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));

                                                        database.updateData(bpnQuantity,
                                                                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                                        BPNQuantity.XML_TPH + "=?" + " and " +
                                                                        BPNQuantity.XML_CROP + "=?" + " and " +
                                                                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                                                new String[]{bpnId, spbsLine.getBpnDate(), companyCode, estate, spbsSummary.getBlock(), spbsLine.getTph(), crop, spbsLine.getAchievementCode()});
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }

                                database.commitTransaction();

                                txtSpbsNumber.setText(spbsNumber);

                                //new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi),
                                //		getResources().getString(R.string.save_successed), false).show();
                                Toast.makeText(getApplicationContext(), "Save/Update SPBS Success", Toast.LENGTH_SHORT).show();
                                finish();
                                animOnFinish();
                            } catch (SQLiteException e) {
                                e.printStackTrace();
                                database.closeTransaction();
                                new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                        e.getMessage(), false).show();
                            } finally {
                                database.closeTransaction();
                            }
                        } else {
                            finish();
                            animOnFinish();
                        }
                        //	}
                    } else {
                        //new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi),
                        //		getResources().getString(R.string.Invalid_data), false).show();
                        finish();
                        animOnFinish();
                    }
                } else {
                    //new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi),
                    //		getResources().getString(R.string.data_already_export), false).show();
                    finish();
                    animOnFinish();
                }
                break;
        }
    }

    public void updateGpsKoordinat(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }
    }

    private void deleteUnSaved() {
        database.openTransaction();
        database.deleteData(SPBSLine.TABLE_NAME,
                SPBSLine.XML_IS_SAVE + "=?",
                new String[]{"0"});
        database.commitTransaction();
        database.closeTransaction();
    }

    private String getHeaderQrCode(String spbsNumber, String spbsDate, String estate, String division, String licensePlate,
                                   String driver, String nikDriver, String runningAccount, int pages, int maxPages, String lifnr) {
        String header = "";

        spbsDate = new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_QR_CODE_SPBS);

//		header = spbsNumber + "\n" +
//				 spbsDate + "\n" +
//				 estate + QR_CODE_DELIMITER + division + "\n" +
//				 nikClerk + "\n" +
//				 String.valueOf(pages) + QR_CODE_DELIMITER + String.valueOf(maxPages) + "\n";

        //   header = spbsNumber + QR_CODE_DELIMITER + licensePlate + QR_CODE_DELIMITER + driver + "-" + nikDriver + QR_CODE_DELIMITER +
        //         spbsDate + QR_CODE_DELIMITER + estate + QR_CODE_DELIMITER + division + QR_CODE_DELIMITER + runningAccount;
        header = spbsNumber + QR_CODE_DELIMITER + licensePlate + QR_CODE_DELIMITER + driver + "-" + nikDriver + QR_CODE_DELIMITER + lifnr + QR_CODE_DELIMITER +
                "10" + QR_CODE_DELIMITER + spbsDate + QR_CODE_DELIMITER + estate + QR_CODE_DELIMITER + division + QR_CODE_DELIMITER + nikClerk + "-" + clerk + QR_CODE_DELIMITER + runningAccount;

        return header;
    }

    private String getLineQrCode(int id, String block, String bpnDate, int qtyJanjang, double qtyLooseFruit) {
        String line = "";

        bpnDate = new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_QR_CODE_SPBS);

//		line = String.valueOf(id) + QR_CODE_DELIMITER +
//			   block + QR_CODE_DELIMITER +
//			   bpnDate + QR_CODE_DELIMITER +
//			   String.valueOf(qtyJanjang) + QR_CODE_DELIMITER +
//			   String.valueOf(qtyLooseFruit) + "\n";

        line = String.valueOf(id) + QR_CODE_DELIMITER +
                block + QR_CODE_DELIMITER +
                bpnDate + QR_CODE_DELIMITER +
                String.valueOf(qtyJanjang) + QR_CODE_DELIMITER +
//                String.valueOf( String.format("%.0f", qtyLooseFruit));
                addNol(qtyLooseFruit, 2);

        return line;
    }

    private void connectToPrinter() {
        try {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (!bluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }


            database.openTransaction();
            Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            BluetoothDevice pairedDevice;
            Iterator<BluetoothDevice> iter = (bluetoothAdapter.getBondedDevices()).iterator();
            while (iter.hasNext()) {
                pairedDevice = iter.next();
                if (bluetooth != null) {
                    if (pairedDevice.getAddress().equalsIgnoreCase(bluetooth.getAddress())) {
                        bluetoothDevice = pairedDevice;
                        break;
                    }
                }
            }

            if (bluetoothDevice != null) {
                if (mBtSocket != null && mBtSocket.isConnected()) {
                    collectSPBS();
                } else {
                    establishBluetoothConnection(bluetoothDevice.getAddress());
                }
            } else {
                new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                        getResources().getString(R.string.bluetooth_not_found), false).show();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    // The Handler that gets information back from the BluetoothPrintService
    private final MyHandler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<SPBSDatecsActivity> mActivity;

        public MyHandler(SPBSDatecsActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            SPBSDatecsActivity activity = mActivity.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }

    private void handleMessage(Message msg) {
        new connTask().execute();
    }

    class connTask extends AsyncTask<Void, Void, Void> {
        String name;
        String address;
        boolean isPaired;
        boolean isSelected;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            super.onPostExecute(voids);
            collectSPBS();
        }
    }

    private void collectSPBS() {
        List<SPBSPrint> listSpbsPrint = new ArrayList<SPBSPrint>();

        database.openTransaction();

        List<Object> listSpbsLineBlock = database.getListData(true, SPBSLine.TABLE_NAME,
                new String[]{SPBSLine.XML_BLOCK, SPBSLine.XML_BPN_DATE},
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?",
                new String[]{year, companyCode, estate, crop, spbsNumber},
                SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null, SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null);
        database.closeTransaction();

        if (listSpbsLineBlock.size() > 0) {
            for (int i = 0; i < listSpbsLineBlock.size(); i++) {
                SPBSLine spbsLineBlock = (SPBSLine) listSpbsLineBlock.get(i);

                String block = spbsLineBlock.getBlock();
                String bpnDate = spbsLineBlock.getBpnDate();

                database.openTransaction();

                List<Object> listSpbsLineQtyJanjang = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String[]{year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.JANJANG_CODE},
                        null, null, null, null);
                database.closeTransaction();

                int qtyJanjang = 0;

                if (listSpbsLineQtyJanjang.size() > 0) {
                    for (int k = 0; k < listSpbsLineQtyJanjang.size(); k++) {
                        SPBSLine spbsLine = (SPBSLine) listSpbsLineQtyJanjang.get(k);

                        qtyJanjang = (int) (qtyJanjang + spbsLine.getQuantityAngkut());
                    }
                }

                database.openTransaction();

                List<Object> listSpbsLineQtyLooseFruit = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String[]{year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.LOOSE_FRUIT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyLooseFruit = 0;

                if (listSpbsLineQtyLooseFruit.size() > 0) {
                    for (int k = 0; k < listSpbsLineQtyLooseFruit.size(); k++) {
                        SPBSLine spbsLine = (SPBSLine) listSpbsLineQtyLooseFruit.get(k);

                        qtyLooseFruit = qtyLooseFruit + spbsLine.getQuantityAngkut();
                    }
                }

                SPBSPrint spbsPrint = new SPBSPrint(block, bpnDate, qtyJanjang, qtyLooseFruit);
                listSpbsPrint.add(spbsPrint);
            }

            final int MAX_LINE = 10;

            if (listSpbsPrint.size() > 0) {
                int id = 1;
                int maxPages = 1;
                int pages = 1;
                int posStart = 0;
                int posEnd = 0;

                if (listSpbsPrint.size() % MAX_LINE == 0) {
                    maxPages = (int) listSpbsPrint.size() / MAX_LINE;
                } else {
                    maxPages = ((int) listSpbsPrint.size() / MAX_LINE) + 1;
                }

                for (int i = 0; i < listSpbsPrint.size(); i++) {
                    posEnd = i;

                    if ((id == MAX_LINE) || (i == (listSpbsPrint.size() - 1))) {
                        try {
                            int retVal = printSPBS(listSpbsPrint, posStart, posEnd, pages, maxPages);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        id = 1;
                        posStart = i + 1;
                        pages++;
                    }

                    id++;
                }
            }
        }
    }

    private int printSPBS(final List<SPBSPrint> listSpbsPrint, final int posStart, final int posEnd, final int pages, final int maxPages) throws IOException {
        runTask(new PrinterRunnable() {
            @Override
            public void run(ProgressDialog dialog, Printer printer) throws IOException, ParseException {
                StringBuffer textBuffer = new StringBuffer();

                String headerQrCode = getHeaderQrCode(spbsNumber, spbsDate, estate, division, licensePlate, driver, nikDriver, runningAccount, pages, maxPages, lifnr);
                String lineQrCode = "";

//            	long print_date=new Date().getTime();
                String strPrintDate = new DateLocal(String.valueOf(todayDate), DateLocal.FORMAT_MASTER_XML).getDateString(DateLocal.FORMAT_MASTER_XML);
                for (int i = posStart; i <= posEnd; i++) {
                    SPBSPrint spbsPrint = listSpbsPrint.get(i);

                    if (i == posStart) {
                        // lineQrCode = getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
                        lineQrCode = getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
                    } else {
                        lineQrCode = lineQrCode + QR_CODE_LINE_DELIMITER + getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
                    }
                }

                //Find Company & Estate Name -- -- add by Fernando 201901007
                String companyName = "";
                String estateName = "";
                database.openTransaction();
                ConfigApp configApp = (ConfigApp) database.getDataFirst(false, ConfigApp.TABLE_NAME, null,
                        ConfigApp.XML_COMPANY_CODE + "=?" + " and " +
                                ConfigApp.XML_ESTATE + "=?",
                        new String[]{companyCode, estate}, null, null, null, null);
                if (configApp != null) {
                    companyName = configApp.getCompanyName();
                    estateName = configApp.getEstateName();
                }
                database.closeTransaction();

                //Find Vendor Name -- add by Fernando 20171005
                database.openTransaction();
                Vendor vendor = (Vendor) database.getDataFirst(false, Vendor.TABLE_NAME, null,
                        Vendor.XML_ESTATE + "=?" + " and " +
                                Vendor.XML_LIFNR + "=?",
                        new String[]{estate, lifnr}, null, null, null, null);

                if (vendor != null) {
                    vendorName = vendor.getName();
                }

                database.closeTransaction();

                //add by adit 20161229
                textBuffer.append("{right} {i} " + strPrintDate);
                textBuffer.append("{br}");
                textBuffer.append("{reset}" + companyName);
                textBuffer.append("{br}");
                textBuffer.append(estateName);
                textBuffer.append("{br}");
                textBuffer.append("{reset}{center}{b}SPBS Ticket");
                textBuffer.append("{br}");

                textBuffer.append("{br}");
                textBuffer.append("{reset}");
//                textBuffer.append("SPBS Number     : " + (destType.equalsIgnoreCase("MILL")? spbsNumber : "LANGSIR"));
                textBuffer.append("SPBS Number     : " + (destType.equalsIgnoreCase("MILL") ? spbsNumber : (destType.equalsIgnoreCase("LANGSIR") ? "LANGSIR" : spbsNumber + "/EKSTERNAL")));
                textBuffer.append("{br}");
                textBuffer.append("SPBS Date       : " + new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS));
                textBuffer.append("{br}");
//                textBuffer.append("Estate/Division : " + estate + " / " + division);
                textBuffer.append("Estate/Division : " + (destType.equalsIgnoreCase("MILL") ? (estate + " / " + division) : (destType.equalsIgnoreCase("LANGSIR") ? (estate + " / " + division) : "")));
                textBuffer.append("{br}");
                textBuffer.append("Clerk           : " + nikClerk + " - " + addChar(clerk, " ", 15, 1));
                textBuffer.append("{br}");
                textBuffer.append("License Plate   : " + licensePlate);
                textBuffer.append("{br}");
//                textBuffer.append("Running Account : " + runningAccount);
                textBuffer.append("Running Account : " + (destType.equalsIgnoreCase("MILL") ? runningAccount : (destType.equalsIgnoreCase("LANGSIR") ? runningAccount : "")));
                textBuffer.append("{br}");
                textBuffer.append("Driver          : " + nikDriver + " - " + addChar(driver, " ", 15, 1));
                textBuffer.append("{br}");
//				textBuffer.append("Vendor          : " + lifnr + " - " + vendorName);
                textBuffer.append("Vendor          : " + (destType.equalsIgnoreCase("MILL") ? (lifnr + " - " + vendorName) : (destType.equalsIgnoreCase("LANGSIR") ? (lifnr + " - " + vendorName) : "")));
                textBuffer.append("{br}");
                textBuffer.append("Pages           : " + String.valueOf(pages) + " of " + String.valueOf(maxPages));
                textBuffer.append("{br}");
                textBuffer.append("-----------------------------------------------");
                textBuffer.append("{br}");
                textBuffer.append(addChar("  Block", " ", 8, 1) + "   " + addChar("Harvest Date", " ", 12, 1) + "   " + addChar("Janjang", " ", 7, 1) + "   " + "Loose Fruit");
                textBuffer.append("{br}");
                textBuffer.append("-----------------------------------------------");
                textBuffer.append("{br}");

                int totalqtyJanjang = 0;
                double totalqtyLooseFruit = 0.0;
                for (int i = posStart; i <= posEnd; i++) {
                    SPBSPrint spbsPrint = listSpbsPrint.get(i);

                    String block = spbsPrint.getBlock();
                    String bpnDate = new DateLocal(spbsPrint.getBpnDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS);
                    String qtyJanjang = String.valueOf((int) spbsPrint.getQtyJanjang());
                    String qtyLooseFruitString = addNol(spbsPrint.getQtyLooseFruit(), 2);

                    //Check RSPO Block -- add by Fernando 20180831
                    database.openTransaction();
                    String bpnDateStr = spbsPrint.getBpnDate();

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        long bpndate = format.parse(spbsPrint.getBpnDate()).getTime();

                        BLKRISET blkriset = (BLKRISET) database.getDataFirst(false, BLKRISET.TABLE_NAME, null,
                                BLKRISET.XML_BLOCK + "=?" + " and " +
                                        BLKRISET.XML_START_DATE_LONG + "<=?" + " and " +
                                        BLKRISET.XML_END_DATE_LONG + ">=?",
                                new String[]{block, String.valueOf(bpndate), String.valueOf(bpndate)}, null, null, null, null);

                        if (blkriset != null) {
                            block = block + "-R";
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    database.closeTransaction();
                    if (!destType.equalsIgnoreCase("MILL") && !destType.equalsIgnoreCase("LANGSIR")) {
                        textBuffer.append(addChar(block, " ", 8, -1) + "  " + addChar("", " ", 12, -1) + "    " + addChar(qtyJanjang, " ", 7, -1) + "    " + addChar(qtyLooseFruitString, " ", 7, -1));
                    } else {
                        textBuffer.append(addChar(block, " ", 8, -1) + "  " + addChar(bpnDate, " ", 12, -1) + "    " + addChar(qtyJanjang, " ", 7, -1) + "    " + addChar(qtyLooseFruitString, " ", 7, -1));
                    }
                    textBuffer.append("{br}");
                    totalqtyJanjang += spbsPrint.getQtyJanjang();
                    totalqtyLooseFruit += spbsPrint.getQtyLooseFruit();
                }

                //Penambahan total - kamal 20191017
                textBuffer.append("{br}");
                textBuffer.append("-----------------------------------------------");
                textBuffer.append("{br}");
                textBuffer.append(addChar("", " ", 8, -1) + "  " + addChar("Total :", " ", 12, -1) + "    " + addChar(String.valueOf((int) totalqtyJanjang), " ", 7, -1) + "    " + addChar(addNol(totalqtyLooseFruit, 2), " ", 7, -1));
                textBuffer.append("{br}");

                textBuffer.append("{br}");
                String qrType = new SharedPreferencesHandler(SPBSDatecsActivity.this).getQR();
                String qrCode = "";
                if (qrType.equalsIgnoreCase("full")) {
                    qrCode = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
                } else {
                    qrCode = spbsNumber + ";" + licensePlate + ";" + driver + ";" + lifnr;
                }
//            	String qrCode = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
//            	String qrCode = spbsNumber + ";" + licensePlate + ";" + driver;

                printer.reset();
                printer.printTaggedText(textBuffer.toString());
                printer.setBarcode(Printer.ALIGN_CENTER, false, 2, Printer.HRI_NONE, 100);

                if (destType.equalsIgnoreCase("MILL")) {
                    printer.printQRCode(14, 1, qrCode);
                }
                printer.feedPaper(110);

                printer.flush();
            }
        }, R.string.wait);


        return 0;
    }

    private String addChar(String value, String addString, int digits, int leftOrRight) {
        String temp = value;

        if (value.length() < digits) {
            for (int i = 0; i < (digits - value.length()); i++) {
                if (leftOrRight == -1) {
                    temp = addString + temp;
                } else if (leftOrRight == 1) {
                    temp = temp + addString;
                }
            }
        } else {
            temp = value.substring(0, digits);
        }

        return temp;
    }

    private String addNol(Double value, int decimalDigits) {
        String valueString = String.valueOf(value);
        String temp = "";

        try {
            int dot = valueString.indexOf(".");

            if (dot > -1) {
                String beforeDot = valueString.substring(0, dot + 1);
                String afterDot = valueString.substring(dot + 1, valueString.length());

                temp = beforeDot + addChar(afterDot, "0", decimalDigits, 1);
            } else {
                temp = valueString;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();

            temp = valueString;
        }

        return temp;
    }

    private double getQty(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String tph, String achievementCode) {
        double qty = 0;

        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_GANG + "=?" + " and " +
                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                        BPNQuantity.XML_TPH + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                new String[]{bpnId, companyCode, estate, bpnDate, division, gang, block, tph, achievementCode},
                null, null, null, null);

        if (lstQty.size() > 0) {
            for (int i = 0; i < lstQty.size(); i++) {
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        List<Object> listspbslineLooseFruitExist = database.getListData(false, SPBSLine.TABLE_NAME, null,
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_BLOCK + "=?" + " and " +
                        SPBSLine.XML_BPN_ID + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?" + " and " +
                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                new String[]{year, companyCode, estate, block, bpnId, crop, qtyLooseFruitCode},
                null, null, null, null);

        double qtyLooseFruitExist = 0;
        for (int k = 0; k < listspbslineLooseFruitExist.size(); k++) {
            SPBSLine spbsLineLooseFruitExist = (SPBSLine) listspbslineLooseFruitExist.get(k);
            qtyLooseFruitExist = qtyLooseFruitExist + spbsLineLooseFruitExist.getQuantityAngkut();
        }

        qty = qty - qtyLooseFruitExist;

        return qty;
    }

    private double getQtyRemaining(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String tph, String achievementCode) {
        double qty = 0;

        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_GANG + "=?" + " and " +
                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                        BPNQuantity.XML_TPH + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                new String[]{bpnId, bpnDate, companyCode, estate, bpnDate, division, gang, block, tph, achievementCode},
                null, null, null, null);

        if (lstQty.size() > 0) {
            for (int i = 0; i < lstQty.size(); i++) {
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantityRemaining();
            }
        }

        return qty;
    }

    private double getQly(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String tph, String achievementCode, String qualityCode) {
        double qty = 0;

        List<Object> lstQly = database.getListData(false, BPNQuality.TABLE_NAME, null,
                BPNQuality.XML_BPN_ID + "=?" + " and " +
                        BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuality.XML_ESTATE + "=?" + " and " +
                        BPNQuality.XML_BPN_DATE + "=?" + " and " +
                        BPNQuality.XML_DIVISION + "=?" + " and " +
                        BPNQuality.XML_GANG + "=?" + " and " +
                        BPNQuality.XML_LOCATION + "=?" + " and " +
                        BPNQuality.XML_TPH + "=?" + " and " +
                        BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        BPNQuality.XML_QUALITY_CODE + "=?",
                new String[]{bpnId, companyCode, estate, bpnDate, division, gang, block, tph, achievementCode, qualityCode},
                null, null, null, null);

        if (lstQly.size() > 0) {
            for (int i = 0; i < lstQly.size(); i++) {
                BPNQuality bpnQuantity = (BPNQuality) lstQly.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }

    private double getPenalty(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String achievementCode) {
        double qty = 0;

        List<Object> lstPenalty = database.getListData(false, Penalty.TABLE_NAME, null,
                Penalty.XML_IS_LOADING + "=?",
                new String[]{"0"},
                null, null, null, null);

        if (lstPenalty != null && lstPenalty.size() > 0) {
            for (Object obj : lstPenalty) {
                Penalty penalty = (Penalty) obj;

                if (penalty != null) {

                    BPNQuality bpnQuality = (BPNQuality) database.getDataFirst(false, BPNQuality.TABLE_NAME, null,
                            BPNQuality.XML_BPN_ID + "=?" + " and " +
                                    BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                    BPNQuality.XML_ESTATE + "=?" + " and " +
                                    BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                    BPNQuality.XML_DIVISION + "=?" + " and " +
                                    BPNQuality.XML_GANG + "=?" + " and " +
                                    BPNQuality.XML_LOCATION + "=?" + " and " +
                                    BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                    BPNQuality.XML_QUALITY_CODE + "=?",
                            new String[]{bpnId, companyCode, estate, bpnDate, division, gang, block, achievementCode, penalty.getPenaltyCode()},
                            null, null, null, null);

                    if (bpnQuality != null)
                        qty = qty + bpnQuality.getQuantity();
                }
            }
        }

        return qty;
    }

    @Override
    public void onDateOK(Date date, int id) {

        String filterDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
        long todayDate = new Date().getTime();

        try {
            database.openTransaction();

            List<Object> lstSpbsLine = database.getListData(false, SPBSLine.TABLE_NAME, null,
                    SPBSLine.XML_YEAR + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?" + " and " +
                            SPBSLine.XML_CROP + "=?" + " and " +
                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                            SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                            SPBSLine.XML_BPN_DATE + "=?",
                    new String[]{year, companyCode, estate, crop, spbsNumber, spbsDate, filterDate},
                    null, null, null, null);

            if (lstSpbsLine.size() > 0) {
                for (int i = 0; i < lstSpbsLine.size(); i++) {
                    SPBSLine spbsLine = (SPBSLine) lstSpbsLine.get(i);

                    String bpnId = spbsLine.getBpnId();
                    String spbsRef = spbsLine.getSpbsRef();
                    String spbsNext = spbsLine.getSpbsNext();

                    boolean del = false;

                    if (spbsRef.isEmpty()) {
						/*BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
							BPNHeader.XML_BPN_ID + "=?",
							new String [] {bpnId},
							null, null, null, null);

						if(bpnHeader != null){
							bpnHeader.setSpbsNumber("");

							database.updateData(bpnHeader,
								BPNHeader.XML_BPN_ID + " = ?",
								new String [] {bpnHeader.getBpnId()});

							del = true;
						}*/
                    } else {
                        if (TextUtils.isEmpty(spbsNext)) {
                            String spbsNumber = spbsRef.split("_")[0];
                            String lineId = spbsRef.split("_")[1];

                            SPBSLine spbsLineRef = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                    SPBSLine.XML_ID + " = ? " + " and " +
                                            SPBSLine.XML_SPBS_NUMBER + " = ? ",
                                    new String[]{lineId, spbsNumber},
                                    null, null, null, null);

                            if (spbsLineRef != null) {
                                spbsLineRef.setSpbsNext("");

                                database.updateData(spbsLineRef,
                                        SPBSLine.XML_ID + " = ? " + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + " = ?",
                                        new String[]{lineId, spbsNumber});

                                del = true;
                            }
                        }
                    }

                    if (del) {
                        database.deleteData(SPBSLine.TABLE_NAME,
                                SPBSLine.XML_ID + "=?" + " and " +
                                        SPBSLine.XML_SPBS_NUMBER + "=?",
                                new String[]{String.valueOf(spbsLine.getId()), spbsLine.getSpbsNumber()});
                    }
                }
            }

            List<Object> listBlock = database.getListData(true, BPNHeader.TABLE_NAME, null,
                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BPNHeader.XML_ESTATE + "=?" + " and " +
                            //BPNHeader.XML_DIVISION + "=?" + " and " +
                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
							/*BPNHeader.XML_SPBS_NUMBER + " is null" + " or " +
							BPNHeader.XML_SPBS_NUMBER + "=?" + " and " +*/
                            BPNHeader.XML_CROP + "=?" + " and " +
                            BPNHeader.XML_BPN_DATE + "=?",
                    new String[]{companyCode, estate, nikClerk, crop, filterDate},
                    null, null, null, null);

            List<Object> listBlockDivision = database.getListData(true, BlockHdrc.TABLE_NAME, null,
                    BlockHdrc.XML_COMPANY_CODE + "=?" + " and " +
                            BlockHdrc.XML_ESTATE + "=?" + " and " +
                            BlockHdrc.XML_DIVISION + "=?",
                    new String[]{companyCode, estate, division},
                    null, null, null, null);

            List<Object> listBlockFilterDivistion = new ArrayList<Object>();

            for (int j = 0; j < listBlock.size(); j++) {
                BPNHeader bpnHeader = (BPNHeader) listBlock.get(j);
                for (int q = 0; q < listBlockDivision.size(); q++) {
                    BlockHdrc blockHdrc = (BlockHdrc) listBlockDivision.get(q);
                    if (bpnHeader.getLocation().equalsIgnoreCase(blockHdrc.getBlock())) {
                        listBlockFilterDivistion.add(bpnHeader);
                    }
                }
            }

            int spbsLineid = 0;
            if (listBlockFilterDivistion != null && listBlockFilterDivistion.size() > 0) {
                for (int i = 0; i < listBlockFilterDivistion.size(); i++) {
                    BPNHeader bpnHeader = (BPNHeader) listBlockFilterDivistion.get(i);

                    //Modified by Fernando -> Cek BPNQuantity Remaining > 0
                    BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                            BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                    BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                    BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                            new String[]{bpnHeader.getBpnId(), filterDate, companyCode, estate, qtyJanjangCode},
                            null, null, null, null);

                    double qtyRemaining = 0;
                    if (bpnQuantity != null)
                        qtyRemaining = (double) bpnQuantity.getQuantityRemaining();

                    BPNQuantity bpnQuantityLooseFruit = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                            BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                    BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                    BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                            new String[]{bpnHeader.getBpnId(), filterDate, companyCode, estate, qtyLooseFruitCode},
                            null, null, null, null);

                    boolean isLooseFruitAvailable = true;
                    double qtyLooseFruitRemaining = 0;

                    if (bpnQuantityLooseFruit != null) {
                        qtyLooseFruitRemaining = (double) bpnQuantityLooseFruit.getQuantity();

                        List<Object> listspbslineLooseFruitExist = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                SPBSLine.XML_YEAR + "=?" + " and " +
                                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                        SPBSLine.XML_ESTATE + "=?" + " and " +
                                        SPBSLine.XML_BLOCK + "=?" + " and " +
                                        SPBSLine.XML_BPN_ID + "=?" + " and " +
                                        SPBSLine.XML_CROP + "=?" + " and " +
                                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                new String[]{year, companyCode, estate, bpnQuantityLooseFruit.getLocation(), bpnHeader.getBpnId(), crop, qtyLooseFruitCode},
                                null, null, null, null);

                        double qtyLooseFruitExist = 0;
                        for (int k = 0; k < listspbslineLooseFruitExist.size(); k++) {
                            SPBSLine spbsLineLooseFruitExist = (SPBSLine) listspbslineLooseFruitExist.get(k);
                            qtyLooseFruitExist = qtyLooseFruitExist + spbsLineLooseFruitExist.getQuantityAngkut();
                        }

                        qtyLooseFruitRemaining = qtyLooseFruitRemaining - qtyLooseFruitExist;
                    }

                    if (qtyRemaining > 0 || qtyLooseFruitRemaining > 0) {
                        if (spbsLineid == 0)
                            spbsLineid = 1;
                        else
                            spbsLineid = spbsLineid + 1;

                        String imei = bpnHeader.getImei();
                        String companyCode = bpnHeader.getCompanyCode();
                        String estate = bpnHeader.getEstate();
                        String bpnId = bpnHeader.getBpnId();
                        String bpnDt = bpnHeader.getBpnDate();
                        String division = bpnHeader.getDivision();
                        String gang = bpnHeader.getGang();
                        String block = bpnHeader.getLocation();
                        String crop = bpnHeader.getCrop();
                        String tph = bpnHeader.getTph();
                        String gpsKoordinat = bpnHeader.getGpsKoordinat();

                        double qtyJanjang = getQtyRemaining(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.JANJANG_CODE);
                        double qtyLooseFruit = getQty(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.LOOSE_FRUIT_CODE);

						/*double penalty = getPenalty(bpnId, companyCode, estate, bpnDt, division, gang, block, BPNQuantity.JANJANG_CODE);
						double qtyMinusPenalty = qtyJanjang - penalty;*/

                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.JANJANG_CODE, qtyJanjang, qtyJanjang, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));

                        database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
                                tph, bpnDt, BPNQuantity.LOOSE_FRUIT_CODE, qtyLooseFruit, qtyLooseFruit, 0, "Kg", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));
                    }
                }
            }

            List<Object> listSpbsLangsir = database.getListData(false, SPBSHeader.TABLE_NAME, null,
                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                            SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
                            SPBSHeader.XML_SPBS_NUMBER + "!=?" + " and " +
                            SPBSHeader.XML_SPBS_DATE + "=?" + " and " +
                            SPBSHeader.XML_DEST_TYPE + "!=? COLLATE NOCASE",
                    new String[]{companyCode, estate, nikClerk, spbsNumber, filterDate, "MILL"},
                    null, null, null, null);

            if (listSpbsLangsir != null && listSpbsLangsir.size() > 0) {
                for (int i = 0; i < listSpbsLangsir.size(); i++) {
                    SPBSHeader spbsHeader = (SPBSHeader) listSpbsLangsir.get(i);

                    List<Object> listSpbsLineLangsir = database.getListData(false, SPBSLine.TABLE_NAME, null,
                            SPBSLine.XML_IMEI + "=?" + " and " +
                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_SPBS_REF + "=?" + " and " +
                                    SPBSLine.XML_SPBS_DATE + "=?",
                            new String[]{spbsHeader.getImei(), spbsHeader.getYear(), spbsHeader.getCompanyCode(),
                                    spbsHeader.getEstate(), spbsHeader.getCrop(), spbsHeader.getSpbsNumber(), "", spbsHeader.getSpbsDate()},
                            null, null, SPBSLine.XML_ID, null);

                    String spbsLineIDPrev = "";
                    double qtyAngkutRemaining = 0;
                    if (listSpbsLineLangsir != null && listSpbsLineLangsir.size() > 0) {
                        for (int j = 0; j < listSpbsLineLangsir.size(); j++) {
                            SPBSLine spbsLine = (SPBSLine) listSpbsLineLangsir.get(j);

                            if (spbsLineid > 0) {
                                if (TextUtils.isEmpty(spbsLineIDPrev)) {
                                    spbsLineid += 1;
                                    spbsLineIDPrev = String.valueOf(spbsLine.getId());

                                    if (spbsLine.getAchievementCode().equalsIgnoreCase("01"))
                                        qtyAngkutRemaining = (double) spbsLine.getQuantityRemaining();

                                } else {
                                    if (!spbsLineIDPrev.equalsIgnoreCase(String.valueOf(spbsLine.getId()))) {
                                        spbsLineid += 1;

                                        if (spbsLine.getAchievementCode().equalsIgnoreCase("01"))
                                            qtyAngkutRemaining = (double) spbsLine.getQuantityRemaining();
                                    }

                                    spbsLineIDPrev = String.valueOf(spbsLine.getId());
                                }
                            } else {
                                spbsLineid = 1;
                                spbsLineIDPrev = String.valueOf(spbsLine.getId());

                                if (spbsLine.getAchievementCode().equalsIgnoreCase("01"))
                                    qtyAngkutRemaining = (double) spbsLine.getQuantityRemaining();
                            }

                            String spbsRef = spbsLine.getSpbsNumber() + "_" + spbsLine.getId();

                            if (qtyAngkutRemaining > 0) {
                                if (spbsLine.getAchievementCode().equalsIgnoreCase(BPNQuantity.JANJANG_CODE)) {
                                    database.setData(new SPBSLine(0, spbsLineid, spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(),
                                            spbsLine.getEstate(), spbsLine.getCrop(), spbsNumber, spbsDate, spbsLine.getBlock(),
                                            spbsLine.getTph(), spbsLine.getBpnDate(), BPNQuantity.JANJANG_CODE, spbsLine.getQuantityRemaining(), spbsLine.getQuantityRemaining(),
                                            0, "EA", gpsKoordinat, isSave, 0, spbsLine.getBpnId(), spbsRef, "", todayDate, clerk, 0, "", spbsLine.getQuantityRemaining()));
                                } else if (spbsLine.getAchievementCode().equalsIgnoreCase(BPNQuantity.LOOSE_FRUIT_CODE)) {
                                    database.setData(new SPBSLine(0, spbsLineid, spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(),
                                            spbsLine.getEstate(), spbsLine.getCrop(), spbsNumber, spbsDate, spbsLine.getBlock(),
                                            spbsLine.getTph(), spbsLine.getBpnDate(), BPNQuantity.LOOSE_FRUIT_CODE, spbsLine.getQuantityRemaining(), spbsLine.getQuantityRemaining(),
                                            0, "Kg", gpsKoordinat, isSave, 0, spbsLine.getBpnId(), spbsRef, "", todayDate, clerk, 0, "", spbsLine.getQuantityRemaining()));
                                }
                            }
                        }
                    }

                }
            }

//			else{
//				new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi),
//						getResources().getString(R.string.data_empty), false).show();
//			}

            database.commitTransaction();
        } catch (SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
            new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                    e.getMessage(), false).show();
        } finally {
            database.closeTransaction();
        }

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

    private SPBSRunningNumber getAlias(String estate, String division, String year, String month, String imei) {
        DatabaseHandler database = new DatabaseHandler(SPBSDatecsActivity.this);
        SPBSRunningNumber spbsNumberMax = null;

        database.openTransaction();
        DeviceAlias deviceAlias = (DeviceAlias) database.getDataFirst(false, DeviceAlias.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if (deviceAlias == null) {
            database.openTransaction();
            spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                    SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                            SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                            SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                            SPBSRunningNumber.XML_IMEI + "=?",
                    new String[]{estate, year, month, imei},
                    null, null, SPBSRunningNumber.XML_ID + " desc", null);
            database.closeTransaction();

            if (spbsNumberMax == null) {
                database.openTransaction();
                SPBSRunningNumber tempNumber = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                        SPBSRunningNumber.XML_IMEI + "=?",
                        new String[]{imei},
                        null, null, SPBSRunningNumber.XML_ID + " desc", null);
                database.closeTransaction();

                if (tempNumber != null) {
                    String alias = tempNumber.getDeviceAlias();
                    int id = tempNumber.getId() + 1;
                    spbsNumberMax = new SPBSRunningNumber(0, id, estate, division, year, month, imei, "0", alias);
                }
            }
        } else {
            database.openTransaction();
            spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                    SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                            SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                            SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                            SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                            SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                    new String[]{estate, year, month, imei, deviceAlias.getDeviceAlias()},
                    null, null, SPBSRunningNumber.XML_ID + " desc", null);
            database.closeTransaction();

            if (spbsNumberMax == null) {
                database.openTransaction();
                SPBSRunningNumber tempNumber = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                        SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                        new String[]{imei, deviceAlias.getDeviceAlias()},
                        null, null, SPBSRunningNumber.XML_ID + " desc", null);
                database.closeTransaction();

                if (tempNumber != null) {
                    String alias = tempNumber.getDeviceAlias();
                    int id = tempNumber.getId() + 1;
                    spbsNumberMax = new SPBSRunningNumber(0, id, estate, division, year, month, imei, "0", alias);
                }
            }
        }

        return spbsNumberMax;
    }

    //datecs

    private synchronized void closePrinterServer() {
        PrinterServer ps = mPrinterServer;
        mPrinterServer = null;
        if (ps != null) {
            try {
                ps.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected void initPrinter(InputStream inputStream, OutputStream outputStream) throws IOException {
        // Here you can enable various debug information
        //ProtocolAdapter.setDebug(true);
        Printer.setDebug(true);
        EMSR.setDebug(true);

        // Check if printer is into protocol mode. Ones the object is created it can not be released
        // without closing base streams.
        mProtocolAdapter = new ProtocolAdapter(inputStream, outputStream);
        if (mProtocolAdapter.isProtocolEnabled()) {
            // Into protocol mode we can callbacks to receive printer notifications
            mProtocolAdapter.setPrinterListener(new ProtocolAdapter.PrinterListener() {
                @Override
                public void onThermalHeadStateChanged(boolean overheated) {
                }

                @Override
                public void onPaperStateChanged(boolean hasPaper) {
                }

                @Override
                public void onBatteryStateChanged(boolean lowBattery) {
                }
            });

            mProtocolAdapter.setBarcodeListener(new ProtocolAdapter.BarcodeListener() {
                @Override
                public void onReadBarcode() {
                }
            });

            mProtocolAdapter.setCardListener(new ProtocolAdapter.CardListener() {
                @Override
                public void onReadCard(boolean encrypted) {
                }
            });

            // Get printer instance
            mPrinterChannel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_PRINTER);
            mPrinter = new Printer(mPrinterChannel.getInputStream(), mPrinterChannel.getOutputStream());

            // Check if printer has encrypted magnetic head
            mUniversalChannel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_UNIVERSAL_READER);
            new UniversalReader(mUniversalChannel.getInputStream(), mUniversalChannel.getOutputStream());

        } else {
            // Protocol mode it not enables, so we should use the row streams.
            mPrinter = new Printer(mProtocolAdapter.getRawInputStream(),
                    mProtocolAdapter.getRawOutputStream());
        }

        mPrinter.setConnectionListener(new ConnectionListener() {
            @Override
            public void onDisconnect() {
                closePrinterServer();
            }
        });

    }

    private void establishBluetoothConnection(final String address) {
        final ProgressDialog dialog = new ProgressDialog(SPBSDatecsActivity.this);
        dialog.setTitle(getString(R.string.informasi));
        dialog.setMessage(getString(R.string.wait));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        closePrinterServer();

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        final Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                btAdapter.cancelDiscovery();

                try {
                    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                    BluetoothDevice btDevice = btAdapter.getRemoteDevice(address);

                    InputStream in = null;
                    OutputStream out = null;

                    try {
                        BluetoothSocket btSocket = btDevice.createRfcommSocketToServiceRecord(uuid);
                        btSocket.connect();

                        mBtSocket = btSocket;
                        in = mBtSocket.getInputStream();
                        out = mBtSocket.getOutputStream();
                    } catch (IOException e) {
                        return;
                    }

                    try {
                        initPrinter(in, out);

                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                collectSPBS();
                            }
                        });
                    } catch (IOException e) {
                        return;
                    }
                } finally {
                    dialog.dismiss();
                }
            }
        });
        t.start();
    }

    private void runTask(final PrinterRunnable r, final int msgResId) {
        final ProgressDialog dialog = new ProgressDialog(SPBSDatecsActivity.this);
        dialog.setTitle(getString(R.string.informasi));
        dialog.setMessage(getString(msgResId));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    r.run(dialog, mPrinter);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                } finally {
                    dialog.dismiss();
                }
            }
        });
        t.start();
    }

    private void GetDefaultSPBSDestination() {
        DatabaseHandler database = new DatabaseHandler(SPBSDatecsActivity.this);

        database.openTransaction();
        List<Object> listObject = database.getListData(false, SPBSDestination.TABLE_NAME, null,
                SPBSDestination.XML_ESTATE + "=?",
                new String[]{estate},
                null, null, null, null);
        database.closeTransaction();

        if (listObject != null && listObject.size() == 1) {
            SPBSDestination spbsDestination = (SPBSDestination) listObject.get(0);

            destId = spbsDestination.getDestId();
            destDesc = spbsDestination.getDestDesc();
            destType = spbsDestination.getDestType();
            txtSpbsDest.setText(destDesc);
        }
    }

    private void PrintOut() {
        DialogNotification dialogNotification;
        database.openTransaction();
        Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        printerConnection = new BluetoothConnection(bluetooth.getAddress());

        try {
            helper.showLoadingDialog("Connecting...");
            printerConnection.open();

            ZebraPrinter printer = null;

            if (printerConnection.isConnected()) {
                printer = ZebraPrinterFactory.getInstance(printerConnection);

                if (printer != null) {
                    PrinterLanguage pl = printer.getPrinterControlLanguage();
                    if (pl == PrinterLanguage.CPCL) {
                        helper.showErrorDialogOnGuiThread("This demo will not work for CPCL printers!");
                    } else {
                        // [self.connectivityViewController setStatus:@"Building receipt in ZPL..." withColor:[UIColor
                        // cyanColor]];
                        dialogNotification = new DialogNotification(SPBSDatecsActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.wait), false);
                        dialogNotification.show();
                        //sendTestLabel();
                        collectSPBSZebra(dialogNotification);

                    }
                    printerConnection.close();
                    saveSettings();
                }
            }
        } catch (ConnectionException e) {
            helper.showErrorDialogOnGuiThread(e.getMessage());
        } catch (ZebraPrinterLanguageUnknownException e) {
            helper.showErrorDialogOnGuiThread("Could not detect printer language");
        } finally {
            helper.dismissLoadingDialog();
        }
    }

    private void saveSettings() {
        database.openTransaction();
        Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();
        Toast.makeText(getApplicationContext(), bluetooth.getName(), Toast.LENGTH_SHORT).show();
        SettingsHelper.saveBluetoothAddress(SPBSDatecsActivity.this, bluetooth.getAddress());
        SettingsHelper.saveIp(SPBSDatecsActivity.this, "");
        SettingsHelper.savePort(SPBSDatecsActivity.this, "");
    }

    private void SendPrintOut(List<SPBSPrint> listSpbsPrint, int posStart, int posEnd, int pages, int maxPages) {
        try {
            byte[] configLabel = printSPBSZEBRA(listSpbsPrint, posStart, posEnd, pages, maxPages).getBytes();
            printerConnection.write(configLabel);
            DemoSleeper.sleep(1500);
            if (printerConnection instanceof BluetoothConnection) {
                DemoSleeper.sleep(500);
            }
        } catch (ConnectionException e) {
        }
    }

    private void collectSPBSZebra(DialogNotification dialogNotification) {
        List<SPBSPrint> listSpbsPrint = new ArrayList<SPBSPrint>();

        database.openTransaction();

        List<Object> listSpbsLineBlock = database.getListData(true, SPBSLine.TABLE_NAME,
                new String[]{SPBSLine.XML_BLOCK, SPBSLine.XML_BPN_DATE},
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?",
                new String[]{year, companyCode, estate, crop, spbsNumber},
                SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null, SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null);
        database.closeTransaction();

        if (listSpbsLineBlock.size() > 0) {
            for (int i = 0; i < listSpbsLineBlock.size(); i++) {
                SPBSLine spbsLineBlock = (SPBSLine) listSpbsLineBlock.get(i);

                String block = spbsLineBlock.getBlock();
                String bpnDate = spbsLineBlock.getBpnDate();

                database.openTransaction();

                List<Object> listSpbsLineQtyJanjang = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String[]{year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.JANJANG_CODE},
                        null, null, null, null);
                database.closeTransaction();

                int qtyJanjang = 0;

                if (listSpbsLineQtyJanjang.size() > 0) {
                    for (int k = 0; k < listSpbsLineQtyJanjang.size(); k++) {
                        SPBSLine spbsLine = (SPBSLine) listSpbsLineQtyJanjang.get(k);

                        qtyJanjang = (int) (qtyJanjang + spbsLine.getQuantityAngkut());
                    }
                }

                database.openTransaction();

                List<Object> listSpbsLineQtyLooseFruit = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_YEAR + "=?" + " and " +
                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                SPBSLine.XML_CROP + "=?" + " and " +
                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                        new String[]{year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.LOOSE_FRUIT_CODE},
                        null, null, null, null);
                database.closeTransaction();

                double qtyLooseFruit = 0;

                if (listSpbsLineQtyLooseFruit.size() > 0) {
                    for (int k = 0; k < listSpbsLineQtyLooseFruit.size(); k++) {
                        SPBSLine spbsLine = (SPBSLine) listSpbsLineQtyLooseFruit.get(k);

                        qtyLooseFruit = qtyLooseFruit + spbsLine.getQuantityAngkut();
                    }
                }

                SPBSPrint spbsPrint = new SPBSPrint(block, bpnDate, qtyJanjang, qtyLooseFruit);
                listSpbsPrint.add(spbsPrint);
            }

            final int MAX_LINE = 15;

            if (listSpbsPrint.size() > 0) {
                int id = 1;
                int maxPages = 1;
                int pages = 1;
                int posStart = 0;
                int posEnd = 0;

                if (listSpbsPrint.size() % MAX_LINE == 0) {
                    maxPages = (int) listSpbsPrint.size() / MAX_LINE;
                } else {
                    maxPages = ((int) listSpbsPrint.size() / MAX_LINE) + 1;
                }

                for (int i = 0; i < listSpbsPrint.size(); i++) {
                    posEnd = i;

                    if ((id == MAX_LINE) || (i == (listSpbsPrint.size() - 1))) {
                        SendPrintOut(listSpbsPrint, posStart, posEnd, pages, maxPages);
                        id = 1;
                        posStart = i + 1;
                        pages++;
                    }

                    id++;
                }
            }
        }
    }

    private String printSPBSZEBRA(List<SPBSPrint> listSpbsPrint, int posStart, int posEnd, int pages, int maxPages) {
        String headerQrCode = getHeaderQrCode(spbsNumber, spbsDate, estate, division, licensePlate, driver, nikDriver, runningAccount, pages, maxPages, lifnr);
        String lineQrCode = "";
        for (int i = posStart; i <= posEnd; i++) {
            SPBSPrint spbsPrint = listSpbsPrint.get(i);

            if (i == posStart) {
                lineQrCode = getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
            } else {
                lineQrCode = lineQrCode + QR_CODE_LINE_DELIMITER + getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
            }
        }
        String qrType = new SharedPreferencesHandler(SPBSDatecsActivity.this).getQR();
        String qrCode = "";
        if (qrType.equalsIgnoreCase("full")) {
            qrCode = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
        } else {
            qrCode = spbsNumber + ";" + licensePlate + ";" + driver;
        }
        Date date = new Date();
        String Tanggal = new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm ");
        String dateString = DateLocal.FORMAT_REPORT_VIEW2.format(date);

        String tmpHeader = "^XA" +

                "^PON^PW790^MNN^LL%d^LH0,0" + "\r\n" +

                "^FO5,50" + "\r\n" + "^A0,N,25,25" + "\r\n" + "^FB900,3,0,C,0" + "^FD" + dateString + "^FS" + "\r\n" +
                "^FO5,100" + "\r\n" + "^A0,N,50,50" + "\r\n" + "^FB600,3,0,C,0" + "^FDSPBS Ticket^FS" + "\r\n" +

                "^FO5,180" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDSPBS Number^FS" + "\r\n" +

                "^FO230,180" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,210" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDSPBS Date^FS" + "\r\n" +

                "^FO230,210" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,240" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDSPBS Estate/Division^FS" + "\r\n" +

                "^FO230,240" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,270" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDClerk^FS" + "\r\n" +

                "^FO230,270" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,300" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDLicense Plate^FS" + "\r\n" +

                "^FO230,300" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,330" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDRunning Account^FS" + "\r\n" +

                "^FO230,330" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,360" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDDriver^FS" + "\r\n" +

                "^FO230,360" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,390" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDPages^FS" + "\r\n" +

                "^FO230,390" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD: %s^FS" + "\r\n" +

                "^FO5,420" + "\r\n" + "^GB750,5,5,B,0^FS" + "\r\n" +

                "^FO5,430" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDBlock^FS" + "\r\n" +

                "^FO130,430" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDHarvest Date^FS" + "\r\n" +

                "^FO330,430" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDJanjang^FS" + "\r\n" +

                "^FO450,430" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FDLoose Fruit^FS" + "\r\n" +

                "^FO5,450" + "\r\n" + "^GB750,5,5,B,0^FS" + "\r\n";

        int headerHeight = 480;
        String body = String.format("^LH0,%d", headerHeight);
        int heightOfOneLine = 30;
        int i = 0;
        int totalqtyJanjang = 0;
        double totalqtyLooseFruit = 0.0;
        for (int h = posStart; h <= posEnd; h++) {
            SPBSPrint spbsPrint = listSpbsPrint.get(h);

            String block = spbsPrint.getBlock();
            String bpnDate = new DateLocal(spbsPrint.getBpnDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS);
            String qtyJanjang = String.valueOf((int) spbsPrint.getQtyJanjang());
            String qtyLooseFruitString = addNol(spbsPrint.getQtyLooseFruit(), 2);
            String lineItem = "^FO5,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n" + "^FO130,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS"
                    + "\r\n" + "^FO330,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS" + "\r\n" + "^FO450,%d" + "\r\n" + "^A0,N,24,24" + "\r\n" + "^FD%s^FS";
            int totalHeight = i++ * heightOfOneLine;
            body += String.format(lineItem, totalHeight, block, totalHeight, bpnDate, totalHeight, qtyJanjang, totalHeight, qtyLooseFruitString);
        }
        long totalBodyHeight = (listSpbsPrint.size() + 1) * heightOfOneLine;
        long footerStartPosition = headerHeight + totalBodyHeight;
        String footer = String.format("^LH0,%d" + "\r\n" +
                "^FO120,20" + "\r\n" + "^BQN,2,10" + "\r\n" + "^FDQA," + spbsNumber + ";" + licensePlate + ";" + driver + "^FS" + "r\n" +
                "^XZ", footerStartPosition);
        long footerHeight = 400;
        long labelLength = headerHeight + totalBodyHeight + footerHeight;
//		String spbsnumber = (destType.equalsIgnoreCase("MILL")? spbsNumber  : "LANGSIR");
        String spbsnumber = (destType.equalsIgnoreCase("MILL") ? spbsNumber : (destType.equalsIgnoreCase("LANGSIR") ? "LANGSIR" : spbsNumber + "/EKSTERNAL"));
        String Estate = estate + " / " + division;
        String Clerk = nikClerk + " - " + addChar(clerk, " ", 15, 1);
        String LicensePlate = licensePlate;
        String RunningAccount = runningAccount;
        String Driver = nikDriver + " - " + addChar(driver, " ", 15, 1);
        String Pages = String.valueOf(pages) + " of " + String.valueOf(maxPages);
        String header = String.format(tmpHeader, labelLength, spbsnumber, Tanggal, Estate, Clerk, LicensePlate, RunningAccount, Driver, Pages);
        String wholeZplLabel = String.format("%s%s%s", header, body, footer);
        return wholeZplLabel;
    }
}


