package com.simp.hms.activity.spbskranibluetooth;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.simp.hms.R;
import com.simp.hms.adapter.AdapterSPBSKrani;
import com.simp.hms.adapter.AdapterSPBSWB;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSWB;
import com.simp.hms.model.UserLogin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SPBSWBScanActivity extends AppCompatActivity implements AdapterSPBSWB.OnWBClickedListener, View.OnClickListener, DialogDateListener {

    private static Integer LAUNCH_ACTIVITY_QRCODE = 100;
    private int refresh = 0;

    private Toolbar tbrMain;
    private Button btnScanQR,btnSpbsHistoryKraniSearch;
    private TextView txtSearchDateSpbsKrani;
    private ListView listDetailSpbs;
    private AdapterSPBSWB adapter;

    private List<SPBSWB> listSpbsWB;
    private DialogProgress dialogProgress;
    DatabaseHandler database;

    private String spbsDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spbswbscan);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);
        txtSearchDateSpbsKrani = (TextView) findViewById(R.id.txtDateKrani);
        btnSpbsHistoryKraniSearch = (Button) findViewById(R.id.btnSpbsHistoryKraniSearch);
        btnScanQR = (Button) findViewById(R.id.btnScanWB);
        listDetailSpbs = (ListView) findViewById(R.id.listStatusSpbsKrani);

        txtActionBarTitle.setText("SPBS WB");
        btnActionBarRight.setVisibility(View.INVISIBLE);

        listSpbsWB = new ArrayList<SPBSWB>();
        adapter = new AdapterSPBSWB(SPBSWBScanActivity.this, listSpbsWB, R.layout.item_spbs_joinwb);
        adapter.setOnWBClickListener(this);
        listDetailSpbs.setAdapter(adapter);

        txtSearchDateSpbsKrani.setOnClickListener(this);
        btnSpbsHistoryKraniSearch.setOnClickListener(this);
        btnScanQR.setOnClickListener(this);

        database = new DatabaseHandler(SPBSWBScanActivity.this);
        spbsDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        txtSearchDateSpbsKrani.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

        new GetDataAsyncTask().execute();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LAUNCH_ACTIVITY_QRCODE) {
            new GetDataAsyncTask().execute();
        } else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents() == null) {
                    Toast.makeText(getApplicationContext(), "Cancelled", Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(SPBSWBScanActivity.this, SPBSWBQRCodeActivity.class);
                    i.putExtra("WBRESULT", result.getContents());
                    startActivityForResult(i, LAUNCH_ACTIVITY_QRCODE);
                    refresh = 1;
                }
            }
        }

    }

    @Override
    public void onItemClick(SPBSWB spbswb) {
        Intent i = new Intent(SPBSWBScanActivity.this, SPBSWBDetilActivity.class);
        i.putExtra("DATAWB", spbswb);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnScanWB:
                IntentIntegrator integrator = new IntentIntegrator(SPBSWBScanActivity.this);
                integrator.setOrientationLocked(false);
                integrator.setPrompt("Scan QR code");
                integrator.setBeepEnabled(false);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.initiateScan();
                break;
            case R.id.txtDateKrani:
                new DialogDate(SPBSWBScanActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtDateKrani).show();
                break;
            case R.id.btnSpbsHistoryKraniSearch:
                new GetDataAsyncTask().execute();
                break;
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        spbsDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
        txtSearchDateSpbsKrani.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSWB>, List<SPBSWB>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogProgress = new DialogProgress(SPBSWBScanActivity.this, getResources().getString(R.string.loading));
            dialogProgress.show();
        }

        @Override
        protected List<SPBSWB> doInBackground(Void... voids) {
            listSpbsWB.clear();
            database.openTransaction();
            listSpbsWB.addAll(database.getSPBSJoinWB(spbsDate));
            database.closeTransaction();

            return listSpbsWB;
        }

        @Override
        protected void onPostExecute(List<SPBSWB> lstTemp) {
            super.onPostExecute(lstTemp);
            refresh = 0;
            adapter.notifyDataSetChanged();
            if (dialogProgress != null) {
                dialogProgress.dismiss();
                dialogProgress = null;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
