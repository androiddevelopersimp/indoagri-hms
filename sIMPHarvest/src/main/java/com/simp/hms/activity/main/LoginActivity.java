package com.simp.hms.activity.main;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.json.JSONObject;

import com.simp.hms.R;
import com.simp.hms.R.array;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.Utils.DeviceUtils;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.other.RestoreDatabase2Activity;
import com.simp.hms.activity.other.SettingsMasterDownloadActivity;
import com.simp.hms.adapter.AdapterRole;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.CryptoHandler;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.DownloadAppsAsyncTask;
import com.simp.hms.handler.JsonHandler;
import com.simp.hms.handler.NetworkHandler;
import com.simp.hms.handler.UpdateHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.Apps;
import com.simp.hms.model.Employee;
import com.simp.hms.model.ForemanActive;
import com.simp.hms.model.Role;
import com.simp.hms.model.UserApp;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogConfirmListener {
	private EditText edt_login_username;
	private EditText edt_login_password;
	private Spinner spn_login_role;
	private Button btn_login_submit;
	private TextView txt_login_restore;
	private TextView txt_login_app_version;
	private ImageButton imgLoginSettings;
	
	private CheckUpdateAsyncTask checkAysncTask;
	private DialogProgress dialogProgress;
	private AdapterRole adapter;
	
	private Apps apps;
	
	String role;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();
		     
		setTheme(R.style.AppBaseTheme_NoTitleBar);
		setContentView(R.layout.activity_login);
		
		registerBaseActivityReceiver();
		
		edt_login_username = (EditText) findViewById(R.id.edt_login_username);
		edt_login_password = (EditText) findViewById(R.id.edt_login_password);
		spn_login_role = (Spinner) findViewById(R.id.spn_login_role);
		btn_login_submit = (Button) findViewById(R.id.btn_login_submit);
		txt_login_restore = (TextView) findViewById(R.id.txt_login_restore);
		txt_login_app_version = (TextView) findViewById(R.id.txt_login_app_version);
		imgLoginSettings = (ImageButton) findViewById(R.id.imgLoginSettings);
		
//		edt_login_username.setText("198800673");
//		edt_login_password.setText("password");
		//edt_login_username.setText(DeviceUtils.getUniqueIMEIId(LoginActivity.this));
		btn_login_submit.setOnClickListener(this);
		txt_login_restore.setOnClickListener(this);
		imgLoginSettings.setOnClickListener(this);
		
		txt_login_app_version.setText(String.format(getResources().getString(R.string.settings_version),
				new DeviceHandler(LoginActivity.this).getAppVersion())+" - "+DeviceUtils.getUniqueIMEIId(LoginActivity.this));
		
		createSpinner();
	}

	@Override   
	public void onClick(View view) {    
		switch (view.getId()) {
		case R.id.btn_login_submit:
			if(new NetworkHandler(LoginActivity.this).isNetworkConnected()){
				checkAysncTask = new CheckUpdateAsyncTask();
				checkAysncTask.execute();
			}else{
				login();
			}     
			
			break;
		case R.id.txt_login_restore:
			startActivity(new Intent(LoginActivity.this, RestoreDatabase2Activity.class));
			break;
		case R.id.imgLoginSettings:
			startActivity(new Intent(LoginActivity.this, SettingsMasterDownloadActivity.class));
			break;
		default:
			break;            
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		edt_login_username.requestFocus();
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		switch (id) {
		case R.id.btn_login_submit:
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			break;
		case 1:
			if(new NetworkHandler(LoginActivity.this).isNetworkConnected()){
				new DownloadAppsAsyncTask(LoginActivity.this, Constanta.SERVER + apps.getFilename()).execute();
			} 
			break;
		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(checkAysncTask != null && checkAysncTask.getStatus() != AsyncTask.Status.FINISHED){
			checkAysncTask.cancel(true);
			checkAysncTask = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		unRegisterBaseActivityReceiver();
	}
	
	
	private void login(){
		LocationManager location_manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		if(location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			DatabaseHandler database = new DatabaseHandler(LoginActivity.this);
			   
			String username = edt_login_username.getText().toString().trim();
			String password = edt_login_password.getText().toString().trim();  
	
			long today = new Date().getTime();
			               
			database.openTransaction();
			UserApp userApp = (UserApp) database.getDataFirst(false, UserApp.TABLE_NAME, null, 
					UserApp.XML_USERNAME + "=? " + " and " +
					UserApp.XML_PASSWORD + "=?" + " and " +
					UserApp.XML_VALID_TO + ">=?",
					new String [] {username, password, String.valueOf(today)},
					null, null, null, null);   
			database.closeTransaction();  
			
			if(userApp != null){
				String nik = userApp.getNik();
				
				database.openTransaction();
				Employee employee = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null, 
						Employee.XML_NIK + "=?",
						new String [] {nik}, 
						null, null, null, null);
				database.closeTransaction();
				
				if(employee != null){
//					try{
//						database.openTransaction();
//						database.deleteData(UserLogin.TABLE_NAME, null, null); 
//						database.commitTransaction();
//					}catch(SQLiteException e){
//						e.printStackTrace();
//						database.closeTransaction();
//						new DialogNotification(LoginActivity.this, getResources().getString(R.string.informasi), e.getMessage(), false).show();
//					}finally{
//						database.closeTransaction();
//					}
//					  
					try{ 
						database.openTransaction();
						database.deleteData(UserLogin.TABLE_NAME, null, null); 
						database.deleteData(ForemanActive.TABLE_NAME, null, null);
						database.setData(new UserLogin(employee.getRowId(), employee.getCompanyCode(), employee.getEstate(), 
								employee.getFiscalYear(), employee.getFiscalPeriod(), employee.getNik(), employee.getName(), employee.getTermDate(), 
								employee.getDivision(), role, employee.getJobPos(), employee.getGang(), employee.getCostCenter(), 
								employee.getEmpType(), employee.getValidFrom(), employee.getHarvesterCode()));
						database.commitTransaction();
						
						startActivity(new Intent(LoginActivity.this, MainActivity.class));
						finish();
						animOnFinish();  
					}catch(SQLiteException e){
						e.printStackTrace();
						database.closeTransaction();
						new DialogNotification(LoginActivity.this, getResources().getString(R.string.informasi), e.getMessage(), false).show();
					}finally{
						database.closeTransaction();
					}
				}else{
					new DialogNotification(LoginActivity.this, getResources().getString(R.string.informasi), 
							getResources().getString(R.string.invalid_login), false).show();
				}
			}else{
				new DialogNotification(LoginActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.invalid_login), false).show();
			}
		}else{
			new DialogConfirm(LoginActivity.this, getResources().getString(R.string.informasi), 
					getResources().getString(R.string.layanan_lokasi), null, R.id.btn_login_submit).show();
		}
	}
	
	private class CheckUpdateAsyncTask extends AsyncTask<Void, Void, String>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(LoginActivity.this, getResources().getString(R.string.wait));
				dialogProgress.show();
			}
		}

		@Override
		protected String doInBackground(Void... params) {
			
			String json = new UpdateHandler().checkVersion();
			
			return json;
		}
		   
		@Override
		protected void onPostExecute(String json) {
			super.onPostExecute(json);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();  
				dialogProgress = null;
			}
			
			if(!TextUtils.isEmpty(json)){
				Log.d("tag", json);
				
				apps = new JsonHandler(json).getApps();
				
				if(apps != null){
					String curVersion = new DeviceHandler(LoginActivity.this).getCodeVersion();
					
					if(!apps.getVersion().equals(curVersion)){
						new DialogConfirm(LoginActivity.this, getResources().getString(R.string.informasi), 
								getResources().getString(R.string.apps_update), null, 1).show();
					}else{
						login();
					}
				}else{
					login();
				}
				
			}else{
				login();
			}
		}
	}
	
	private void createSpinner(){
		String [] strRoles = getResources().getStringArray(R.array.roles);
		List<Role> lstRole = new ArrayList<Role>();
		
		for(String rol : strRoles){
			Role role = new Role(rol);
			
			lstRole.add(role);
		}
		
		role = lstRole.get(0).getName();
		
		adapter = new AdapterRole(LoginActivity.this, lstRole, R.layout.item_role);
		spn_login_role.setAdapter(adapter);
		
		spn_login_role.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				role = ((TextView) view.findViewById(R.id.txtItemRole)).getText().toString();
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}
}
