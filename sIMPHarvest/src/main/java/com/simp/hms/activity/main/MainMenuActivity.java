package com.simp.hms.activity.main;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.ancakpanen.AncakPanenActivity;
import com.simp.hms.activity.ancakpanen.AncakPanenHistoryActivity;
import com.simp.hms.activity.bkm.BKMAbsentActivity;
import com.simp.hms.activity.bkm.BKMOutputActivity;
import com.simp.hms.activity.bkm.BKMOutputHistoryActivity;
import com.simp.hms.activity.bkm.BKMReportHarvesterActivity;
import com.simp.hms.activity.bpn.BPNActivity;
import com.simp.hms.activity.bpn.BPNHistoryActivity;
import com.simp.hms.activity.bpn.BPNReportHarvesterActivity;
import com.simp.hms.activity.other.BlockPlanningActivity;
import com.simp.hms.activity.other.ExportActivity;
import com.simp.hms.activity.other.ImportActivity;
import com.simp.hms.activity.other.SettingsUniversalActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spbs.SPBSHistoryActivity;
import com.simp.hms.activity.spbs.SPBSReportNumberActivity;
import com.simp.hms.activity.spbs.SPBSWoosimActivity;
import com.simp.hms.activity.spta.SPTAActivity;
import com.simp.hms.activity.taksasi.TaksasiActivity;
import com.simp.hms.activity.taksasi.TaksasiHistoryActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.BackupHandler;
import com.simp.hms.handler.FileEncryptionHandler;
import com.simp.hms.handler.FileXMLHandler;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.model.AncakPanenHeader;
import com.simp.hms.model.AncakPanenQuality;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.BLKPLT;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.FileXML;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.TaksasiLine;
import com.simp.hms.model.UserLogin;
import com.simp.hms.widget.DashboardLayout;

import android.app.ActionBar;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
   
public class MainMenuActivity extends BaseActivity implements OnClickListener{
	private TextView txtMainMenuTime;
	private TextView txtMainMenuDate;
	private TextView txtMainMenuUserName;
	private TextView txtMainMenuUserNik;
	private TextView txtMainMenuUserRole;
	private Button btnMainMenuBlockPlanning;
	private Button btnMainMenuSPTA;
	private Button btnMainMenuTaksasi;
	private Button btnMainMenuTaksasiHistory;
	private Button btnMainMenuBPN;
	private Button btnMainMenuBPNKaret;
	private Button btnMainMenuBPNHistory;
	private Button btnMainMenuSPBS;
	private Button btnMainMenuSPBSHistory;
	private Button btnMainMenuBKMAbsent;
	private Button btnMainMenuBKMOutput;
	private Button btnMainMenuBKMOutputHistory; 
	private Button btnMainMenuExport;
	private Button btnMainMenuSettings;
	private Button btnMainMenuBpnReport;
	private Button btnMainMenuBkmReport;
	private Button btnMainMenuSpbsReport;
	private Button btnMainMenuImport;
	private Button btnMainMenuAncakPanen;
	private Button btnMainMenuAncakPanenHistory;
	
	private DashboardLayout dbdMainMenu;
	
	DatabaseHandler database = new DatabaseHandler(MainMenuActivity.this);
	BackupTask backupTask;
	
	MyTask myTask;
	
//	private final int DATABASE_DEL = 30;
//	private final int FILE_DEL = 30;
	
	private final int [] ROLE_DEFAULT = {
		R.id.btnMainMenuBlockPlanning2,
		R.id.btnMainMenuTaksasi2, 
		R.id.btnMainMenuTaksasiHistory2,
		R.id.btnMainMenuBPN2,
		R.id.btnMainMenuBPNKaret,
		R.id.btnMainMenuBPNHistory2, 
		R.id.btnMainMenuBpnReport2,
		R.id.btnMainMenuSPBS2, 
		R.id.btnMainMenuSPBSHistory2, 
		R.id.btnMainMenuSpbsReport2,
		R.id.btnMainMenuBKMAbsent2,
		R.id.btnMainMenuBKMOutput2, 
		R.id.btnMainMenuBKMOutputHistory2, 
		R.id.btnMainMenuBkmReport2,
		R.id.btnMainMenuSPTA2,
		R.id.btnMainMenuExport2, 
		R.id.btnMainMenuSettings2, 
		R.id.btnMainMenuImport2
	};
	
	private final int [] ROLE_FOREMAN = {
		R.id.btnMainMenuBlockPlanning2,
		R.id.btnMainMenuTaksasi2, 
		R.id.btnMainMenuTaksasiHistory2, 
		R.id.btnMainMenuBKMAbsent2,
		R.id.btnMainMenuBKMOutput2, 
		R.id.btnMainMenuBKMOutputHistory2, 
		R.id.btnMainMenuBkmReport2,

		R.id.btnMainMenuAncakPanen2,
		R.id.btnMainMenuAncakPanenHistory2,
		R.id.btnMainMenuExport2, 
		R.id.btnMainMenuSettings2, 
		R.id.btnMainMenuImport2
	};
	
	private final int [] ROLE_CLERK = {
		R.id.btnMainMenuBlockPlanning2,
		R.id.btnMainMenuBPN2, 
		R.id.btnMainMenuBPNHistory2, 
		R.id.btnMainMenuBpnReport2,
		R.id.btnMainMenuSPBS2, 
		R.id.btnMainMenuSPBSHistory2, 
		R.id.btnMainMenuSpbsReport2, 
		R.id.btnMainMenuExport2, 
		R.id.btnMainMenuSettings2, 
		R.id.btnMainMenuImport2
	};

	private final int [] ROLE_CLERK_KARET = {
			R.id.btnMainMenuBlockPlanning2,
			id.btnMainMenuBPNKaret,
			R.id.btnMainMenuBPNHistory2,
			R.id.btnMainMenuBpnReport2,
			R.id.btnMainMenuSPBS2,
			R.id.btnMainMenuSPBSHistory2,
			R.id.btnMainMenuSpbsReport2,
			R.id.btnMainMenuExport2,
			R.id.btnMainMenuSettings2,
			R.id.btnMainMenuImport2
	};

	private final int [] ROLE_CLERK_SAWIT_KARET = {
			R.id.btnMainMenuBlockPlanning2,
			R.id.btnMainMenuBPN2,
			id.btnMainMenuBPNKaret,
			R.id.btnMainMenuBPNHistory2,
			R.id.btnMainMenuBpnReport2,
			R.id.btnMainMenuSPBS2,
			R.id.btnMainMenuSPBSHistory2,
			R.id.btnMainMenuSpbsReport2,
			R.id.btnMainMenuExport2,
			R.id.btnMainMenuSettings2,
			R.id.btnMainMenuImport2
	};
	
	private final int [] ROLE_FIELD_ASSINTANT = {
		R.id.btnMainMenuAncakPanen2,
		R.id.btnMainMenuAncakPanenHistory2,
		R.id.btnMainMenuSPTA2,
		R.id.btnMainMenuExport2, 
		R.id.btnMainMenuSettings2, 
		R.id.btnMainMenuImport2
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		   
		animOnStart();
		
		setTheme(R.style.AppBaseTheme);
		setContentView(R.layout.activity_main_menu);
		
		registerBaseActivityReceiver();
		
		txtMainMenuTime = (TextView) findViewById(R.id.txtMainMenuTime);
		txtMainMenuDate = (TextView) findViewById(R.id.txtMainMenuDate);
		txtMainMenuUserName = (TextView) findViewById(R.id.txtMainMenuUserName);
		txtMainMenuUserNik = (TextView) findViewById(R.id.txtMainMenuUserNik);
		txtMainMenuUserRole = (TextView) findViewById(R.id.txtMainMenuUserRole);
		
		btnMainMenuBlockPlanning = (Button) findViewById(R.id.btnMainMenuBlockPlanning2);
		btnMainMenuTaksasi = (Button) findViewById(R.id.btnMainMenuTaksasi2);
		btnMainMenuTaksasiHistory = (Button) findViewById(R.id.btnMainMenuTaksasiHistory2);
		btnMainMenuBPN = (Button) findViewById(R.id.btnMainMenuBPN2);
		btnMainMenuBPNKaret = (Button) findViewById(id.btnMainMenuBPNKaret);
		btnMainMenuBPNHistory = (Button) findViewById(R.id.btnMainMenuBPNHistory2);
		btnMainMenuSPBS = (Button) findViewById(R.id.btnMainMenuSPBS2);
		btnMainMenuSPBSHistory = (Button) findViewById(R.id.btnMainMenuSPBSHistory2);
		btnMainMenuBKMAbsent = (Button) findViewById(R.id.btnMainMenuBKMAbsent2);
		btnMainMenuBKMOutput = (Button) findViewById(R.id.btnMainMenuBKMOutput2);
		btnMainMenuBKMOutputHistory = (Button) findViewById(R.id.btnMainMenuBKMOutputHistory2);
		btnMainMenuExport = (Button) findViewById(R.id.btnMainMenuExport2);
		btnMainMenuSettings = (Button) findViewById(R.id.btnMainMenuSettings2);
		btnMainMenuBpnReport = (Button) findViewById(R.id.btnMainMenuBpnReport2);
		btnMainMenuBkmReport = (Button) findViewById(R.id.btnMainMenuBkmReport2);
		btnMainMenuSpbsReport = (Button) findViewById(R.id.btnMainMenuSpbsReport2);
		btnMainMenuImport = (Button) findViewById(R.id.btnMainMenuImport2);
		btnMainMenuAncakPanen = (Button) findViewById(R.id.btnMainMenuAncakPanen2);
		btnMainMenuAncakPanenHistory = (Button) findViewById(R.id.btnMainMenuAncakPanenHistory2);
		btnMainMenuSPTA = (Button) findViewById(R.id.btnMainMenuSPTA2);
		
		dbdMainMenu = (DashboardLayout) findViewById(R.id.dbdMainMenu);
		
		ActionBar actionBar = this.getActionBar();
		LayoutInflater lytInflater = LayoutInflater.from(MainMenuActivity.this);
		
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		
		View viewActionBar = lytInflater.inflate(R.layout.action_bar, null);
		
		ImageButton btnActionBarLeft = (ImageButton) viewActionBar.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) viewActionBar.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) viewActionBar.findViewById(R.id.btnActionBarRight);
		
		actionBar.setCustomView(viewActionBar);
		actionBar.setDisplayShowCustomEnabled(true);
		
		txtActionBarTitle.setText(getResources().getString(R.string.app_name));
		btnActionBarRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) { 
				backupTask = new BackupTask();
				backupTask.execute();				
			}   
		});
		
		btnMainMenuBlockPlanning.setOnClickListener(this);
		btnMainMenuTaksasi.setOnClickListener(this);
		btnMainMenuTaksasiHistory.setOnClickListener(this);
		btnMainMenuBPN.setOnClickListener(this);
		btnMainMenuBPNKaret.setOnClickListener(this);
		btnMainMenuBPNHistory.setOnClickListener(this);
		btnMainMenuSPBS.setOnClickListener(this);
		btnMainMenuSPBSHistory.setOnClickListener(this);
		btnMainMenuBKMAbsent.setOnClickListener(this);
		btnMainMenuBKMOutput.setOnClickListener(this);
		btnMainMenuBKMOutputHistory.setOnClickListener(this);
		btnMainMenuExport.setOnClickListener(this);
		btnMainMenuSettings.setOnClickListener(this);
		btnMainMenuBpnReport.setOnClickListener(this);
		btnMainMenuBkmReport.setOnClickListener(this);
		btnMainMenuSpbsReport.setOnClickListener(this);
		btnMainMenuImport.setOnClickListener(this);
		btnMainMenuAncakPanen.setOnClickListener(this);
		btnMainMenuAncakPanenHistory.setOnClickListener(this);
		btnMainMenuSPTA.setOnClickListener(this);
		
		initMainMenu();
		new BackupHandler(MainMenuActivity.this).startBackup();
		
		Thread myThread = null;
		Runnable runnable = new CountDownRunner();
		myThread= new Thread(runnable);   
		myThread.start();
	}   
	
	class MyTask extends AsyncTask<Void, Void, Void>{
		DialogProgress dlgProgress;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dlgProgress = new DialogProgress(MainMenuActivity.this, getResources().getString(R.string.wait));
				dlgProgress.show();
			}
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			deleteDatabase();
			deleteFileXML(30);   
			//deletePhoto(-1);
			deleteDatabase(30);  
			//updatePhoto();
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if(dlgProgress != null && dlgProgress.isShowing()){
				dlgProgress.dismiss();
			}
		}
		
	}
	
	private void initMainMenu(){     
		DatabaseHandler database = new DatabaseHandler(MainMenuActivity.this);
		  
		database.openTransaction();
		UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();
		
		String role = "";
		if(userLogin != null){
			txtMainMenuUserName.setText(userLogin.getName());
			txtMainMenuUserNik.setText(userLogin.getNik());
			txtMainMenuUserRole.setText(userLogin.getRoleId());
			role = userLogin.getRoleId();
		}
		
		int [] visibleMenu;
		
		if(role.equalsIgnoreCase("Foreman")){
			visibleMenu = ROLE_FOREMAN;
		}else if(role.equalsIgnoreCase("Clerk")){
			visibleMenu = ROLE_CLERK;

			database.openTransaction();
			BLKPLT blkpltsawit = (BLKPLT) database.getDataFirst(true, BLKPLT.TABLE_NAME, null,
					BLKPLT.XML_CROP_TYPE + "=?",
					new String [] {"01"},
					null, null, null, null);
			database.closeTransaction();

			database.openTransaction();
			BLKPLT blkpltkaret = (BLKPLT) database.getDataFirst(true, BLKPLT.TABLE_NAME, null,
					BLKPLT.XML_CROP_TYPE + "=?",
					new String [] {"02"},
					null, null, null, null);
			database.closeTransaction();


			if(blkpltsawit == null && blkpltkaret !=null)
				visibleMenu = ROLE_CLERK_KARET;
			else if(blkpltsawit !=null && blkpltkaret !=null)
				visibleMenu = ROLE_CLERK_SAWIT_KARET;

		}else if(role.equalsIgnoreCase("Field Assistant")){
			visibleMenu = ROLE_FIELD_ASSINTANT;
		}else {
			visibleMenu = ROLE_DEFAULT;
		}
		
		for(int i = 0; i < ROLE_DEFAULT.length; i++){
			int visible = View.GONE;
			
			for(int j = 0; j < visibleMenu.length; j++){
				if(ROLE_DEFAULT[i] == visibleMenu[j]){
					visible = View.VISIBLE;
					break;
				}
			}
			
			Button btn = (Button) findViewById(ROLE_DEFAULT[i]);
			btn.setVisibility(visible);
		}
	}
	
	private void updatePhoto(){
		
		try{
			database.openTransaction();
			
			List<Object> lstObject = database.getListData(false, BPNHeader.TABLE_NAME, null, null, null, null, null, null, null);
			
			if(lstObject != null && lstObject.size() > 0){
				for(int i = 0; i < lstObject.size(); i++){
					BPNHeader bpnheader = (BPNHeader) lstObject.get(i);
					
					bpnheader.setPhoto("");
					
					database.updateData(bpnheader, 
							BPNHeader.XML_BPN_ID + "=?", 
							new String [] {bpnheader.getBpnId()});
				}
			}
			
			
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
		}finally{
			database.closeTransaction();
		}
	}
	
	
	private void deleteDatabase(){
		deleteBPN(30);
		deleteBKM(30);
		deleteSPBS(30);
		deleteTaksasi(30);
		deleteAncakPanen(30);
	}
	
	private void deleteBPN(int databaseDel){
		try{
			database.openTransaction();

			database.deleteData(BPNQuantity.TABLE_NAME,
					"date(" + BPNQuantity.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " < " + "date('now', '-" + databaseDel + " day')", 
					null);
			
			database.deleteData(BPNQuality.TABLE_NAME,
					"date(" + BPNQuality.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			
			database.deleteData(BPNHeader.TABLE_NAME,
					"date(" + BPNHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
		}finally{
			database.closeTransaction();
		}
	}
	
	private void deleteBKM(int databaseDel){
		try{
			database.openTransaction();
			database.deleteData(BKMLine.TABLE_NAME,
					"date(" + BKMLine.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			database.deleteData(BKMOutput.TABLE_NAME,
					"date(" + BKMOutput.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			database.deleteData(BKMHeader.TABLE_NAME,
					"date(" + BKMHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
		}finally{
			database.closeTransaction();
		}
	}
	
	private void deleteSPBS(int databaseDel){
		try{
			database.openTransaction();
			database.deleteData(SPBSLine.TABLE_NAME,
					"date(" + SPBSLine.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);				
			database.deleteData(SPBSHeader.TABLE_NAME,
					"date(" + SPBSHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
		}finally{
			database.closeTransaction();
		}
	}
	
	private void deleteTaksasi(int databaseDel){
		try{
			database.openTransaction();
			database.deleteData(TaksasiLine.TABLE_NAME,
					"date(" + TaksasiLine.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);				
			database.deleteData(TaksasiHeader.TABLE_NAME,
					"date(" + TaksasiHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
		}finally{
			database.closeTransaction();
		}
	}
	
	private void deleteAncakPanen(int databaseDel){
		try{
			database.openTransaction();
			
			database.deleteData(AncakPanenQuality.TABLE_NAME,
					"date(" + AncakPanenQuality.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			
			database.deleteData(AncakPanenHeader.TABLE_NAME,
					"date(" + AncakPanenHeader.XML_CREATED_DATE + "/1000,'unixepoch','localtime')" + " <= " + "date('now', '-" + databaseDel + " day')", 
					null);
			
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
		}finally{
			database.closeTransaction();
		}
	}
	
	private void deleteFileXML(int fileDel){
		FolderHandler folderHandler = new FolderHandler(MainMenuActivity.this);
		FileXMLHandler fileMasterHandler = new FileXMLHandler(MainMenuActivity.this);
		List<FileXML> lstFiles;  
		
		if(folderHandler.isSDCardWritable() && folderHandler.init()){
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileMasterNew(), null, ".xml");
			deleteFile(lstFiles, fileDel);
			
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileMasterBackup(), null, ".xml");
			deleteFile(lstFiles, fileDel);
			
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileReportNew(), null, ".xml");
			deleteFile(lstFiles, fileDel);
			
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileReportBackup(), null, ".xml");
			deleteFile(lstFiles, fileDel);
			
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileRestoreNew(), null, ".xml");
			deleteFile(lstFiles, fileDel);
			
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileRestoreBackup(), null, ".xml");
			deleteFile(lstFiles, fileDel);
			
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileDatabaseExport(), null, ".xml");
			deleteFile(lstFiles, fileDel);
			
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFileDatabaseImport(), null, ".xml");
			deleteFile(lstFiles, fileDel);
		}
	}
	
	private void deletePhoto(int fileDel){
		FolderHandler folderHandler = new FolderHandler(MainMenuActivity.this);
		FileXMLHandler fileMasterHandler = new FileXMLHandler(MainMenuActivity.this);
		List<FileXML> lstFiles;  
		
		if(folderHandler.isSDCardWritable() && folderHandler.init()){
			lstFiles = fileMasterHandler.getFiles(folderHandler.getFilePhoto(), null, ".jpg");
			deleteFile(lstFiles, fileDel);
		}
	}
	
	private void deleteDatabase(int fileDel){
		File [] files = getExternalFilesDirs(Environment.DIRECTORY_DOWNLOADS);
		File folder = new File(files[files.length - 1], FolderHandler.DATABASE);
		
		if(folder.exists()){
			String[] fileDatabase = folder.list();
			List<FileXML> lstXML = new ArrayList<FileXML>();
			
			for(int i = 0; i < fileDatabase.length; i++){
				FileXML file = new FileXML(folder.getAbsolutePath() + "/" + fileDatabase[i], "");
				
				lstXML.add(file);
			}
			
			deleteFile(lstXML, fileDel);
		}
	}
	
	private void deleteFile(List<FileXML> lstMaster, int fileDel){
		try{
			for(int i = 0; i < lstMaster.size(); i++){
				File file = new File(lstMaster.get(i).getFileNew());
				
				if(file.exists()){
					long lastModified = file.lastModified();
					long currentDate = new Date().getTime();
					
					long timeDiff = (currentDate - lastModified)/(24*60*60*1000);
					
					if(timeDiff > fileDel){
						file.delete(); 
						 MediaScannerConnection.scanFile(MainMenuActivity.this, new String[] { file.getAbsolutePath() }, null, null);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	  
	@Override
	protected void onPause() {
		super.onPause();
	}  

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnMainMenuBlockPlanning2:
			startActivity(new Intent(MainMenuActivity.this, BlockPlanningActivity.class).putExtra("readOnly", false));
			break;
		case R.id.btnMainMenuTaksasi2:
			startActivity(new Intent(MainMenuActivity.this, TaksasiActivity.class));
			break;
		case R.id.btnMainMenuTaksasiHistory2:
			startActivity(new Intent(MainMenuActivity.this, TaksasiHistoryActivity.class));
			break;
		case R.id.btnMainMenuBPN2:
			startActivity(new Intent(MainMenuActivity.this, BPNActivity.class));
			break;
		case id.btnMainMenuBPNKaret:
			startActivity(new Intent(MainMenuActivity.this, BPNActivity.class));
			break;
		case R.id.btnMainMenuBPNHistory2:
			startActivity(new Intent(MainMenuActivity.this, BPNHistoryActivity.class));
			break;
		case R.id.btnMainMenuSPBS2:
			
			database.openTransaction();
			Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			String NamePrinter = bluetooth.getName();
			if(NamePrinter.equalsIgnoreCase("woosim")){
				startActivity(new Intent(MainMenuActivity.this, SPBSWoosimActivity.class));
			}else if(NamePrinter.equalsIgnoreCase("DPP-350")){
				startActivity(new Intent(MainMenuActivity.this, SPBSDatecsActivity.class));
			}else {
				startActivity(new Intent(MainMenuActivity.this, SPBSActivity.class));
			}
			
			break;
		case R.id.btnMainMenuSPBSHistory2:
			startActivity(new Intent(MainMenuActivity.this, SPBSHistoryActivity.class));
			break;
		case R.id.btnMainMenuBKMAbsent2:
			startActivity(new Intent(MainMenuActivity.this, BKMAbsentActivity.class));
			break;
		case R.id.btnMainMenuBKMOutput2:
			startActivity(new Intent(MainMenuActivity.this, BKMOutputActivity.class));
			break;    
		case R.id.btnMainMenuBKMOutputHistory2:
			startActivity(new Intent(MainMenuActivity.this, BKMOutputHistoryActivity.class));
			break;
		case R.id.btnMainMenuExport2:
			startActivity(new Intent(MainMenuActivity.this, ExportActivity.class));
			break;
		case R.id.btnMainMenuSettings2:
//			startActivity(new Intent(MainMenuActivity.this, SettingsActivity.class));
//			startActivity(new Intent(MainMenuActivity.this, SettingsWoosimActivity.class));
			startActivity(new Intent(MainMenuActivity.this, SettingsUniversalActivity.class));
			break;
		case R.id.btnMainMenuBpnReport2:
			startActivity(new Intent(MainMenuActivity.this, BPNReportHarvesterActivity.class));
			break;
		case R.id.btnMainMenuBkmReport2:
			startActivity(new Intent(MainMenuActivity.this, BKMReportHarvesterActivity.class));
			break;
		case R.id.btnMainMenuSpbsReport2:
			startActivity(new Intent(MainMenuActivity.this, SPBSReportNumberActivity.class));
			break;
		case R.id.btnMainMenuImport2:
			startActivity(new Intent(MainMenuActivity.this, ImportActivity.class));
			break;
		case R.id.btnMainMenuAncakPanen2:
			startActivity(new Intent(MainMenuActivity.this, AncakPanenActivity.class));
			break;
		case R.id.btnMainMenuAncakPanenHistory2:
			startActivity(new Intent(MainMenuActivity.this, AncakPanenHistoryActivity.class));
			break;
		case R.id.btnMainMenuSPTA2:
			startActivity(new Intent(MainMenuActivity.this, SPTAActivity.class));
			break;
		default:
			break;
		}
	}
	   
	private class BackupTask extends AsyncTask<Void, Void, Boolean>{

		DialogProgress dialogProgress;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dialogProgress = new DialogProgress(MainMenuActivity.this, getResources().getString(R.string.wait));
			dialogProgress.show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			try {
				return copyAppDbToDownloadFolder();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
			}
			
			if(result){
				Toast.makeText(MainMenuActivity.this, getResources().getString(R.string.backup_successed), Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(MainMenuActivity.this, getResources().getString(R.string.backup_failed), Toast.LENGTH_SHORT).show();
			}
			
			try{
				database.openTransaction();
				database.deleteData(UserLogin.TABLE_NAME, null, null);
				database.commitTransaction();
				database.closeTransaction();
				
				closeAllActivities();
				startActivity(new Intent(MainMenuActivity.this, LoginActivity.class));
				animOnFinish();
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();
			} 
		}
	}
	
	private boolean copyAppDbToDownloadFolder() throws IOException {
    	boolean mExternalStorageAvailable = false;
    	boolean mExternalStorageWriteable = false;
    	String state = Environment.getExternalStorageState();
    	
    	if (Environment.MEDIA_MOUNTED.equals(state)) {
    	    // We can read and write the media
    	    mExternalStorageAvailable = mExternalStorageWriteable = true;
    	} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
    	    // We can only read the media
    	    mExternalStorageAvailable = true;
    	    mExternalStorageWriteable = false;
    	} else {
    	    // Something else is wrong. It may be one of many other states, but all we need
    	    //  to know is we can neither read nor write
    	    mExternalStorageAvailable = mExternalStorageWriteable = false;
    	}
//    	
    	if(mExternalStorageAvailable && mExternalStorageWriteable){
        	boolean success = false;
        	
    		success = new FileEncryptionHandler(getApplicationContext()).encrypt(FileEncryptionHandler.STORAGE_INTERNAL);
    		success &= new FileEncryptionHandler(getApplicationContext()).encrypt(FileEncryptionHandler.STORAGE_EXTERNAL);
    		
    		return success;
    	}
    	
    	return false;
	}
	
	@Override
	public void onBackPressed() {}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		new BackupHandler(MainMenuActivity.this).stopBackup();
		unRegisterBaseActivityReceiver();
		
		if(myTask != null && myTask.getStatus() != AsyncTask.Status.FINISHED){
			myTask.cancel(true);
			myTask = null;
		}
		
	}

	public void doWork() {
	    runOnUiThread(new Runnable() {
	        public void run() {
	            try{
//	                TextView txtCurrentTime= (TextView)findViewById(R.id.lbltime);
//                    Date dt = new Date();
//                    int hours = dt.getHours();
//                    int minutes = dt.getMinutes();
//                    int seconds = dt.getSeconds();
//                    String curTime = hours + ":" + minutes + ":" + seconds;
//                    txtMainMenuDate.setText(curTime);
                    
                    Calendar cal = Calendar.getInstance(Locale.getDefault());
                    
                    int days = cal.get(Calendar.DAY_OF_WEEK);
                    int dates = cal.get(Calendar.DATE);
                    int months = cal.get(Calendar.MONTH);
                    int years = cal.get(Calendar.YEAR);
                    
                    int hours = cal.get(Calendar.HOUR);
                    int minutes = cal.get(Calendar.MINUTE);
                    int seconds = cal.get(Calendar.SECOND);
                    int amPm = cal.get(Calendar.AM_PM);

                    switch (amPm) {
						case Calendar.AM:
							
							break;
						case Calendar.PM:
							hours = hours + 12;
						default:
							break;
					}
                    
                    txtMainMenuTime.setText(digitFormat(hours, 2) + ":" + digitFormat(minutes,2) + ":" + digitFormat(seconds,2));
                    
                    String hari = "";
                    
                    switch (days) {
					case Calendar.SUNDAY:
						hari = getResources().getString(R.string.minggu);
						break;
					case Calendar.MONDAY:
						hari = getResources().getString(R.string.senin);
						break;
					case Calendar.TUESDAY:
						hari = getResources().getString(R.string.selasa);
						break;
					case Calendar.WEDNESDAY:
						hari = getResources().getString(R.string.rabu);
						break;
					case Calendar.THURSDAY:
						hari = getResources().getString(R.string.kamis);
						break;
					case Calendar.FRIDAY:
						hari = getResources().getString(R.string.jumat);
						break;
					case Calendar.SATURDAY:
						hari = getResources().getString(R.string.sabtu);
						break;
					default:
						break;
					}
                    
                    
                    String bulan = "";
                    
                    switch (months) {
					case Calendar.JANUARY:
						bulan = getResources().getString(R.string.januari);
						break;
					case Calendar.FEBRUARY:
						bulan = getResources().getString(R.string.februari);
						break;
					case Calendar.MARCH:
						bulan = getResources().getString(R.string.maret);
						break;
					case Calendar.APRIL:
						bulan = getResources().getString(R.string.april);
						break;
					case Calendar.MAY:
						bulan = getResources().getString(R.string.mei);
						break;
					case Calendar.JUNE:
						bulan = getResources().getString(R.string.juni);
						break;
					case Calendar.JULY:
						bulan = getResources().getString(R.string.juli);
						break;
					case Calendar.AUGUST:
						bulan = getResources().getString(R.string.agustus);
						break;
					case Calendar.SEPTEMBER:
						bulan = getResources().getString(R.string.september);
						break;
					case Calendar.OCTOBER:
						bulan = getResources().getString(R.string.oktober);
						break;
					case Calendar.NOVEMBER:
						bulan = getResources().getString(R.string.november);
						break;
					case Calendar.DECEMBER:
						bulan = getResources().getString(R.string.desember);
						break;
					default:
						break;
					}
                    
                    txtMainMenuDate.setText(hari + " " + dates + " " + bulan + " " + years);
                    
	            }catch (Exception e) {}
	        }
	    });
	}


	class CountDownRunner implements Runnable{
	    // @Override
	    public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
	    }
	}
	
	
	private String digitFormat(int value, int digit){
		String str = String.valueOf(value);
		
		int l = str.length();
		
		if(str.length() < digit){
			for(int i = l; i < digit; i++){
				str = "0" + str;
			}
		}
		
		return str;
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		initMenu();
		
		myTask = new MyTask();
		myTask.execute();
	}
	
	private void initMenu(){
		btnMainMenuTaksasi.setText(getResources().getString(R.string.taksasi_taksasi));
		btnMainMenuTaksasiHistory.setText(getResources().getString(R.string.taksasi_history));
		btnMainMenuBPN.setText(getResources().getString(R.string.buku_panen));
		btnMainMenuBPNKaret.setText("BPN Karet");
		btnMainMenuBPNHistory.setText(getResources().getString(R.string.buku_panen_history));
		btnMainMenuBpnReport.setText(getResources().getString(R.string.report_bpn));
		btnMainMenuSPBS.setText(getResources().getString(R.string.spbs));
		btnMainMenuSPBSHistory.setText(getResources().getString(R.string.spbs_history));
		btnMainMenuSpbsReport.setText(getResources().getString(R.string.report_spbs));
		btnMainMenuBKMAbsent.setText(getResources().getString(R.string.bkm_absent));
		btnMainMenuBKMOutput.setText(getResources().getString(R.string.bkm_output));
		btnMainMenuBKMOutputHistory.setText(getResources().getString(R.string.bkm_output_history));
		btnMainMenuBkmReport.setText(getResources().getString(R.string.report_bkm));
		btnMainMenuExport.setText(getResources().getString(R.string.export));
		btnMainMenuSettings.setText(getResources().getString(R.string.settings));
		btnMainMenuImport.setText(getResources().getString(R.string.import_report));
	}
	
}
