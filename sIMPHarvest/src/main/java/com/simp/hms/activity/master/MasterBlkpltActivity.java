package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBlkplt;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BLKPLT;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterBlkpltActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsv_master_blkplt;
	private EditText edt_master_blkplt_search;
	
	private List<BLKPLT> lst_blkplt;
	private AdapterBlkplt adapter;
	
	private GetDataAsyncTask getData;
	private DialogProgress dialogProgress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_blkplt);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsv_master_blkplt = (ListView) findViewById(R.id.lsv_master_blkplt);
		edt_master_blkplt_search = (EditText) findViewById(R.id.edt_master_blkplt_search);

		ImageButton btn_action_bar_left = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txt_action_bar_title = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btn_action_bar_right = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btn_action_bar_left.setOnClickListener(this);
		txt_action_bar_title.setText(getResources().getString(R.string.master_blkplt));
		btn_action_bar_right.setVisibility(View.INVISIBLE);
		edt_master_blkplt_search.addTextChangedListener(this);
		lsv_master_blkplt.setOnItemClickListener(this);
	}
	
	
	
	@Override
	protected void onStart() {
		super.onStart();
		
		getData = new GetDataAsyncTask();
		getData.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
		
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BLKPLT>, List<BLKPLT>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterBlkpltActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<BLKPLT> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterBlkpltActivity.this);
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, BLKPLT.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			List<BLKPLT> lstTemp = new ArrayList<BLKPLT>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BLKPLT blkplt = (BLKPLT) listObject.get(i);
					
					lstTemp.add(blkplt);
				}
			}
			
			return lstTemp;
		}
		
		@Override
		protected void onPostExecute(List<BLKPLT> lst_load) {
			super.onPostExecute(lst_load);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(lst_load.size() > 0){
				lst_blkplt = lst_load;
				adapter = new AdapterBlkplt(MasterBlkpltActivity.this, lst_blkplt, R.layout.item_blkplt);
			}else{
				adapter = null;
			}
			
			lsv_master_blkplt.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getData != null && getData.getStatus() != AsyncTask.Status.FINISHED){
			getData.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getData != null && getData.getStatus() != AsyncTask.Status.FINISHED){
			getData.cancel(true);
		}
	}
	
}
