package com.simp.hms.activity.spu;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterSPUReportBlockPOD;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BJR;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPUReportBlockPOD;
import com.simp.hms.model.SPUReportNumberPOD;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SPUPODReportBlockActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, DialogDateListener, TextWatcher {
    private Toolbar tbrMain;
    private TextView edtSpuReportBlockSearch;
    private ListView lsvSpuReportBlock;

    private List<SPUReportBlockPOD> lstSPUReport;
    private AdapterSPUReportBlockPOD adapter;

    private GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(SPUPODReportBlockActivity.this);

    String year = "";
    String companyCode = "";
    String estate = "";
    String crop = "";
    String spuNumber = "";
    String spuDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_spu_report_block_pod);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        edtSpuReportBlockSearch = (TextView) findViewById(R.id.edtSpuReportBlockSearch);
        lsvSpuReportBlock = (ListView) findViewById(R.id.lsvSpuReportBlock);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.spu));
        btnActionBarright.setVisibility(View.INVISIBLE);
        edtSpuReportBlockSearch.setOnClickListener(this);
        lsvSpuReportBlock.setOnItemClickListener(this);

        SPUReportNumberPOD spuReport = null;

        if(getIntent().getExtras() != null){
            spuReport = getIntent().getExtras().getParcelable(SPUReportNumberPOD.TABLE_NAME);
            spuDate = getIntent().getExtras().getString(SPBSHeader.XML_SPBS_DATE);
        }

        if(spuReport != null){
            year = spuReport.getYear();
            companyCode = spuReport.getCompanyCode();
            estate = spuReport.getEstate();
            crop = spuReport.getCrop();
            spuNumber = spuReport.getSpuNumber();
        }

        getDataAsync = new SPUPODReportBlockActivity.GetDataAsyncTask();
        getDataAsync.execute();

        edtSpuReportBlockSearch.addTextChangedListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        SPUReportBlockPOD spuReport = (SPUReportBlockPOD) adapter.getItem(pos);

        Bundle bundle = new Bundle();
        bundle.putParcelable(SPUReportBlockPOD.TABLE_NAME, spuReport);

        startActivity(new Intent(SPUPODReportBlockActivity.this, SPUPODReportTphActivity.class)
                .putExtras(bundle)
                .putExtra(SPBSHeader.XML_SPBS_DATE, spuDate));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        spuDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

        edtSpuReportBlockSearch.setText(spuDate);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable editable) {
        adapter.getFilter().filter(editable);
        adapter.notifyDataSetChanged();
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPUReportBlockPOD>, List<SPUReportBlockPOD>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(SPUPODReportBlockActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<SPUReportBlockPOD> doInBackground(Void... voids) {
            database.openTransaction();
            List<Object> listObject =  database.getListData(true, SPBSLine.TABLE_NAME,
                    new String [] {SPBSLine.XML_BLOCK},
                    SPBSLine.XML_YEAR + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?" + " and " +
                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                            SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                            SPBSLine.XML_CROP + "=?",
                    new String [] {year, companyCode, estate, spuNumber, spuDate, "04"},
                    SPBSLine.XML_BLOCK, null, SPBSLine.XML_BLOCK, null);
            database.closeTransaction();

            List<SPUReportBlockPOD> listTemp = new ArrayList<SPUReportBlockPOD>();

            if(listObject.size() > 0){
                double qtyEstimasi = 0;
                for(int i = 0; i < listObject.size(); i++){
                    SPBSLine spuLine = (SPBSLine) listObject.get(i);
                    String block = spuLine.getBlock();
                    double qtyPODQty = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POD_QTY_CODE);
                    double qtyGoodQty = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.GOOD_QTY_CODE);
                    double qtyGoodWeight = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.GOOD_WEIGHT_CODE);
                    double qtyBadQty = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.BAD_QTY_CODE);
                    double qtyBadWeight = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.BAD_WEIGHT_CODE);
                    double qtyPoorQty = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POOR_QTY_CODE);
                    double qtyPoorWeight = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POOR_WEIGHT_CODE);

                    database.openTransaction();
                    List<Object> BJRValue = database.getListData(false, BJR.TABLE_NAME, null,
                            BJR.XML_COMPANY_CODE + "=?" + " and " +
                                    BJR.XML_ESTATE + "=?" + " and " +
                                    BJR.XML_BLOCK + "=?" + " " ,
                            new String [] {companyCode, estate,  block},
                            null, null, BJR.XMl_EFF_DATE+" DESC", "1");

                    double qtyBJR = 0;
                    database.closeTransaction();
                    if(BJRValue.size() > 0){
                        for(int a = 0; a < BJRValue.size(); a++){
                            BJR bjr = (BJR) BJRValue.get(a);

                            qtyBJR = bjr.getBjr();
                            qtyEstimasi = qtyPODQty*qtyBJR;
                        }
                    }else{
                        qtyEstimasi = qtyPODQty;
                    }
                    listTemp.add(new SPUReportBlockPOD(year, companyCode, estate, "04", spuNumber, spuDate, block,
                                                        qtyGoodQty, qtyGoodWeight,qtyBadQty,qtyBadWeight, qtyPoorQty, qtyPoorWeight,qtyPODQty,qtyEstimasi));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<SPUReportBlockPOD> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(listTemp.size() > 0){
                lstSPUReport = listTemp;
                adapter = new AdapterSPUReportBlockPOD(SPUPODReportBlockActivity.this, lstSPUReport, R.layout.item_spu_report_block_pod);
            }else{
                adapter = null;
            }

            lsvSpuReportBlock.setAdapter(adapter);
        }
    }

    private double getQty(String year, String companyCode, String estate, String spbsNumber, String spbsDate, String block, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, SPBSLine.TABLE_NAME, null,
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                        SPBSLine.XML_BLOCK + "=?" + " and " +
                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?",
                new String [] {year, companyCode, estate, spbsNumber, spbsDate, block, achievementCode, "04"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                SPBSLine SPBSLine = (SPBSLine) lstQty.get(i);

                qty = qty + SPBSLine.getQuantityAngkut();
            }
        }

        return qty;
    }
}
