package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBjr;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BJR;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterBjrActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterBjr;
	private EditText edtMasterBjrSearch;
	
	private List<BJR> listBjr;
	private AdapterBjr adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_bjr);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterBjr = (ListView) findViewById(R.id.lsv_master_bjr);
		edtMasterBjrSearch = (EditText) findViewById(R.id.edtMasterAbsentTypeSearch);

		ImageButton btn_action_bar_left = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txt_action_bar_title = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btn_action_bar_right = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btn_action_bar_left.setOnClickListener(this);
		txt_action_bar_title.setText(getResources().getString(R.string.master_bjr));
		btn_action_bar_right.setVisibility(View.INVISIBLE);
		edtMasterBjrSearch.addTextChangedListener(this);
		lsvMasterBjr.setOnItemClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
		
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BJR>, List<BJR>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterBjrActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<BJR> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterBjrActivity.this);
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, BJR.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			List<BJR> listTemp = new ArrayList<BJR>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BJR bjr = (BJR) listObject.get(i);
					
					listTemp.add(bjr);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BJR> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				listBjr = listTemp;
				adapter = new AdapterBjr(MasterBjrActivity.this, listBjr, R.layout.item_bjr);
			}else{
				adapter = null;
			}
			
			lsvMasterBjr.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
}
