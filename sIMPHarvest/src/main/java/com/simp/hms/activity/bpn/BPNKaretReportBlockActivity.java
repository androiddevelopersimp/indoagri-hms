package com.simp.hms.activity.bpn;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import android.widget.AdapterView.OnItemClickListener;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;


import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNKaretReportBlock;
import com.simp.hms.adapter.AdapterBPNReportBlock;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNKaretReportBlock;
import com.simp.hms.model.BPNKaretReportHarvester;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BPNReportBlock;
import com.simp.hms.model.BPNReportHarvester;
import com.simp.hms.model.UserLogin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BPNKaretReportBlockActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher {
    private Toolbar tbrMain;
    private EditText edtBpnReportBlockSearch;
    private ListView lsvBpnReportBlock;

    private List<BPNKaretReportBlock> lstBPNReport;
    private AdapterBPNKaretReportBlock adapter;

    private BPNKaretReportBlockActivity.GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(BPNKaretReportBlockActivity.this);

    String companyCode = "";
    String estate = "";
    String division = "";
    String gang = "";
    String nikClerk = "";
    String nikHarvester = "";
    String bpnDate = "";
    String block = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_bpn_karet_report_block);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        edtBpnReportBlockSearch = (EditText) findViewById(R.id.edtBpnReportBlockSearch);
        lsvBpnReportBlock = (ListView) findViewById(R.id.lsvBpnReportBlock);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
        btnActionBarright.setVisibility(View.INVISIBLE);
        edtBpnReportBlockSearch.addTextChangedListener(this);
        lsvBpnReportBlock.setOnItemClickListener(this);

        BPNKaretReportHarvester bpnReport = null;

        if(getIntent().getExtras() != null){
            bpnReport = (BPNKaretReportHarvester) getIntent().getParcelableExtra(BPNKaretReportHarvester.TABLE_NAME);
            bpnDate = getIntent().getExtras().getString(BPNHeader.XML_BPN_DATE);
        }

        if(bpnReport != null){
            companyCode = bpnReport.getCompanyCode();
            estate = bpnReport.getEstate();
            division = bpnReport.getDivision();
            gang = bpnReport.getGang();
            nikHarvester = bpnReport.getNik();

        }

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        BPNKaretReportBlock bpnReport = (BPNKaretReportBlock) adapter.getItem(pos);

        Bundle bundle = new Bundle();
        bundle.putParcelable(BPNKaretReportBlock.TABLE_NAME, bpnReport);

        startActivity(new Intent(BPNKaretReportBlockActivity.this, BPNKaretReportTphActivity.class)
                .putExtras(bundle)
                .putExtra(BPNHeader.XML_BPN_DATE, bpnDate)
                .putExtra(BPNHeader.XML_NIK_HARVESTER, nikHarvester)
                .putExtra(BPNHeader.XML_LOCATION, block));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtBpnReportHarvesterBpnDate:
                new DialogDate(BPNKaretReportBlockActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtHarvestBookHistoryBpnDate).show();
                break;
            case R.id.btnBpnReportHarvesterSearch:
                getDataAsync = new BPNKaretReportBlockActivity.GetDataAsyncTask();
                getDataAsync.execute();
                break;
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    @Override
    public void afterTextChanged(Editable filter) {
        adapter.getFilter().filter(filter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

    @Override
    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<BPNKaretReportBlock>, List<BPNKaretReportBlock>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(BPNKaretReportBlockActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<BPNKaretReportBlock> doInBackground(Void... voids) {

//			String gang = "";
            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
//				gang = userLogin.getGang();
                nik = userLogin.getNik();
            }

//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
//					new String [] {BPNHeader.XML_LOCATION},
//					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//					BPNHeader.XML_ESTATE + "=?" + " and " +
//					BPNHeader.XML_DIVISION + "=?" + " and " +
//					BPNHeader.XML_GANG + "=?" + " and " +
//					BPNHeader.XML_NIK_CLERK + "=?" + " and " +
//					BPNHeader.XML_BPN_DATE + "=?" + " and " +
//					BPNHeader.XML_NIK_HARVESTER + "=?",
//					new String [] {companyCode, estate, division, gang, nik, bpnDate, nikHarvester},
//					BPNHeader.XML_LOCATION, null, BPNHeader.XML_LOCATION, null);
//			database.closeTransaction();

            database.openTransaction();
            List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
                    new String [] {BPNHeader.XML_LOCATION, "min (" + BPNHeader.XML_CREATED_DATE + ") as " + BPNHeader.XML_CREATED_DATE},
                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BPNHeader.XML_ESTATE + "=?" + " and " +
                            BPNHeader.XML_DIVISION + "=?" + " and " +
                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                            BPNHeader.XML_BPN_DATE + "=?" + " and " +
                            BPNHeader.XML_NIK_HARVESTER + "=?",
                    new String [] {companyCode, estate, division, nik, bpnDate, nikHarvester},
                    BPNHeader.XML_LOCATION, null, BPNHeader.XML_CREATED_DATE, null);
            database.closeTransaction();

            List<BPNKaretReportBlock> listTemp = new ArrayList<BPNKaretReportBlock>();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    BPNHeader bpnHeader = (BPNHeader) listObject.get(i);

                    block = bpnHeader.getLocation();

                    double qtyLatexWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.LATEX_WET_CODE);
                    double qtyLatexDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.LATEX_DRC_CODE);
                    double qtyLatexDry = (double) ((qtyLatexWet * qtyLatexDRC) / 100);

                    double qtyLumpWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.LUMP_WET_CODE);
                    double qtyLumpDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.LUMP_DRC_CODE);
                    double qtyLumpDry = (double) ((qtyLumpWet * qtyLumpDRC) / 100);

                    double qtySlabWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.SLAB_WET_CODE);
                    double qtySlabDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.SLAB_DRC_CODE);
                    double qtySlabDry = (double) ((qtyLumpWet * qtyLumpDRC) / 100);

                    double qtyTreelaceWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.TREELACE_WET_CODE);
                    double qtyTreelaceDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.TREELACE_DRC_CODE);
                    double qtyTreelaceDry = (double) ((qtyLumpWet * qtyLumpDRC) / 100);

                    listTemp.add(new BPNKaretReportBlock(companyCode, estate, division, gang, block,
                            qtyLatexWet, qtyLatexDRC, qtyLatexDry,
                            qtyLumpWet, qtyLumpDRC, qtyLumpDry,
                            qtySlabWet, qtySlabDRC, qtySlabDry,
                            qtyTreelaceWet, qtyTreelaceDRC, qtyTreelaceDry));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<BPNKaretReportBlock> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(listTemp.size() > 0){
                lstBPNReport = listTemp;
                adapter = new AdapterBPNKaretReportBlock(BPNKaretReportBlockActivity.this, lstBPNReport, R.layout.item_bpn_karet_report_block);
            }else{
                adapter = null;
            }

            lsvBpnReportBlock.setAdapter(adapter);
        }
    }

    private double getQty(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String block, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        BPNQuantity.XML_CROP + "=?",
                new String [] {companyCode, estate, division, bpnDate, nikHarvester, block, achievementCode, "01"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }
}
