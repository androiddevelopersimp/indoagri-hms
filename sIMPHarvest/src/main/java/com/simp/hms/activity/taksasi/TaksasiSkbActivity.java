package com.simp.hms.activity.taksasi;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.anim;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.adapter.AdapterFragment;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.fragment.TaksasiSkbFragment;
import com.simp.hms.model.Converter;
import com.simp.hms.model.FragmentItems;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.TaksasiLine;
import com.simp.hms.service.GPSService;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class TaksasiSkbActivity extends FragmentActivity implements OnClickListener, OnPageChangeListener {
	private Toolbar tbrMain;
	private ImageButton btnTaksasiSkbPrev;
	private TextView txtTaksasiSkbBarisSkb;
	private ImageButton btnTaksasiSkbNext;
	private ViewPager vprTaksasiSkbBarisSkb;
	
	List<FragmentItems> listFragmentItems;
	AdapterFragment adapter;
	DatabaseHandler database = new DatabaseHandler(TaksasiSkbActivity.this);
	GPSService gps;
	
	String imei = "";
	String year = "";
	String companyCode = "";
	String estate = "";
	String division = "";
	String taksasiDate = "";
	String block = "";
	String crop = "";
	int barisSkb = 0;
	String foreman = "";
	int status = 0;
	ArrayList<String> listBarisSkb = new ArrayList<String>();
	
	int selectedIndex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

		setContentView(R.layout.activity_taksasi_skb);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		btnTaksasiSkbPrev = (ImageButton) findViewById(R.id.btnTaksasiSkbPrev);
		txtTaksasiSkbBarisSkb = (TextView) findViewById(R.id.txtTaksasiSkbBarisSkb);
		btnTaksasiSkbNext = (ImageButton) findViewById(R.id.btnTaksasiSkbNext);
		vprTaksasiSkbBarisSkb = (ViewPager) findViewById(R.id.vprTaksasiSkbBarisSkb);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.taksasi_taksasi));
		btnActionBarRight.setVisibility(View.INVISIBLE);   
		btnTaksasiSkbPrev.setOnClickListener(this);
		btnTaksasiSkbNext.setOnClickListener(this);
		vprTaksasiSkbBarisSkb.setOnPageChangeListener(this);
		
//		vprSpbsThpBlock.setOffscreenPageLimit(Integer.MAX_VALUE);
		
		imei = getIntent().getExtras().getString(TaksasiHeader.XML_IMEI, "");
		companyCode = getIntent().getExtras().getString(TaksasiHeader.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(TaksasiHeader.XML_ESTATE, "");
		division = getIntent().getExtras().getString(TaksasiHeader.XML_DIVISION, "");
		taksasiDate = getIntent().getExtras().getString(TaksasiHeader.XML_TAKSASI_DATE, "");
		block = getIntent().getExtras().getString(TaksasiHeader.XML_BLOCK, "");
		crop = getIntent().getExtras().getString(TaksasiHeader.XML_CROP, "");
		foreman = getIntent().getExtras().getString(TaksasiHeader.XML_FOREMAN, "");
		status = getIntent().getExtras().getInt(TaksasiHeader.XML_STATUS, 0);
		barisSkb = getIntent().getExtras().getInt(TaksasiLine.XML_BARIS_SKB, 0);
		listBarisSkb = getIntent().getExtras().getStringArrayList(TaksasiHeader.XML_BARIS_SKBS);
	
		getData();
	}
	
	private void getData(){
		listFragmentItems = new ArrayList<FragmentItems>();
		
		for(int i = 0; i < listBarisSkb.size(); i++){
			if(String.valueOf(barisSkb).equalsIgnoreCase(listBarisSkb.get(i))){
				selectedIndex = i;
			}
			
			Bundle bundle = new Bundle();
			bundle.putString(TaksasiLine.XML_IMEI, imei);
			bundle.putString(TaksasiLine.XML_COMPANY_CODE, companyCode);
			bundle.putString(TaksasiLine.XML_ESTATE, estate);
			bundle.putString(TaksasiLine.XML_DIVISION, division);
			bundle.putString(TaksasiLine.XML_TAKSASI_DATE, taksasiDate);
			bundle.putString(TaksasiLine.XML_BLOCK, block);
			bundle.putString(TaksasiLine.XML_CROP, crop);
			bundle.putInt(TaksasiLine.XML_BARIS_SKB, new Converter(listBarisSkb.get(i)).StrToInt());
			bundle.putString(TaksasiHeader.XML_FOREMAN, foreman);
			bundle.putInt(TaksasiLine.XML_STATUS, status);
			
			TaksasiSkbFragment taksasiSkbFragment = new TaksasiSkbFragment();
			taksasiSkbFragment.setArguments(bundle);
			
			listFragmentItems.add(new FragmentItems(listBarisSkb.get(i), taksasiSkbFragment));
		}
		
		adapter = new AdapterFragment(getSupportFragmentManager());
		adapter.setListFragment(listFragmentItems);
		 
		txtTaksasiSkbBarisSkb.setText(String.valueOf(barisSkb));
		vprTaksasiSkbBarisSkb.setAdapter(adapter);
		vprTaksasiSkbBarisSkb.setCurrentItem(selectedIndex);
		
		setVisibleButtonPage();
	}
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnTaksasiSkbPrev:
			if(selectedIndex > 0){
				selectedIndex--;
				vprTaksasiSkbBarisSkb.setCurrentItem(selectedIndex);
			}
			
			setVisibleButtonPage();
			break;
		case R.id.btnTaksasiSkbNext:
			if(selectedIndex < listBarisSkb.size() - 1){
				selectedIndex++;
				vprTaksasiSkbBarisSkb.setCurrentItem(selectedIndex);
			}
			
			setVisibleButtonPage();
			break;
		default:
			break;
		}
	}
  
	@Override
	public void onPageScrollStateChanged(int pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int pos, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int pos) {
		selectedIndex = pos;
		String block = listBarisSkb.get(pos);
		txtTaksasiSkbBarisSkb.setText(block);
		
		setVisibleButtonPage();
	}

	@Override
	public void onBackPressed() {
		setResult(RESULT_OK);
		finish();
	}

	private void setVisibleButtonPage(){
		if(selectedIndex == 0){
			btnTaksasiSkbPrev.setVisibility(View.INVISIBLE);
		}else{
			btnTaksasiSkbPrev.setVisibility(View.VISIBLE);
		}
		
		if(selectedIndex == (listBarisSkb.size() - 1)){
			btnTaksasiSkbNext.setVisibility(View.INVISIBLE);
		}else{
			btnTaksasiSkbNext.setVisibility(View.VISIBLE);
		}
	}
}

