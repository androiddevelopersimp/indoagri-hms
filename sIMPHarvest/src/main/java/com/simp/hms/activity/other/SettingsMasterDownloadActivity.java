package com.simp.hms.activity.other;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterMasterDownload;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.FileXMLHandler;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.handler.ParsingHandler;
import com.simp.hms.model.FileXML;
import com.simp.hms.model.MasterDownload;
import com.simp.hms.model.MessageStatus;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsMasterDownloadActivity extends BaseActivity implements OnClickListener {
	private Toolbar tbrMain;
	private ListView lsvSettingsMasterDownload;
	private CheckBox cbxSettingsMasterDownUsbOtg;
	private Button btnSettingsMasterDownloadSync;
	
	private List<MasterDownload> lstMasterDownload;
	private AdapterMasterDownload adapter;
	
	private DialogProgress dialogProgress;
	
    private static final String ACTION_USB_PERMISSION = "com.simp.hms.USB_PERMISSION";
    private boolean isUsbOtgAvailable = false;
    private boolean isUsbOtgPermit = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_settings_master_download);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvSettingsMasterDownload = (ListView) findViewById(R.id.lsvSettingsMasterDownload);
		cbxSettingsMasterDownUsbOtg = (CheckBox) findViewById(R.id.cbxSettingsMasterDownUsbOtg);
		btnSettingsMasterDownloadSync = (Button) findViewById(R.id.btnSettingsMasterDownloadSync);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.settings_sync_master));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnSettingsMasterDownloadSync.setOnClickListener(this);
		
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);  
        filter.addAction(Environment.MEDIA_MOUNTED);
        filter.addAction(Environment.MEDIA_UNMOUNTED);
        
        registerReceiver(mUsbReceiver, filter);
		
		new GetDataAsyncTask().execute();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnSettingsMasterDownloadSync:
			boolean isUsbOtg = isUsbOtgAvailable && isUsbOtgPermit && cbxSettingsMasterDownUsbOtg.isChecked();
			
			FolderHandler folderHandler = new FolderHandler(SettingsMasterDownloadActivity.this);
			
			if(folderHandler.isSDCardWritable() && folderHandler.init()){
				new ReadDataAsynctask(folderHandler, isUsbOtg).execute();

			}else{  
				new DialogNotification(SettingsMasterDownloadActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.error_sd_card), true).show();
			} 
			break;

		default:
			break;
		}
		
	}
	
	private class ReadDataAsynctask extends AsyncTask<Void, String, List<MessageStatus>>{
		FolderHandler folderHandler;
		boolean isUsbOtg;
		
		public ReadDataAsynctask(FolderHandler folderHandler, boolean isUsbOtg) {
			this.folderHandler = folderHandler;
			this.isUsbOtg = isUsbOtg;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(SettingsMasterDownloadActivity.this, getResources().getString(R.string.wait));
				dialogProgress.show();
			}
//			
//			if(isUsbOtg){
//				Toast.makeText(SettingsMasterDownloadActivity.this, "ada", Toast.LENGTH_SHORT).show();
//			}else{
//				Toast.makeText(SettingsMasterDownloadActivity.this, "tidak ada", Toast.LENGTH_SHORT).show();
//			}
		}
		
		@Override
		protected List<MessageStatus> doInBackground(Void... voids) {
			FileXMLHandler fileMasterHandler = new FileXMLHandler(SettingsMasterDownloadActivity.this);
			List<FileXML> lstMaster;
			List<MessageStatus> lstMsgStatus = null;
			
			if(isUsbOtg){
				String sourceFolder = "/storage/UsbDriveA/" + FolderHandler.ROOT + "/" + FolderHandler.MASTER + "/" + FolderHandler.NEW;
				
				List<FileXML> lstFileOtg = fileMasterHandler.getFiles(sourceFolder, "", ".xml");
				
				try{
					for(FileXML fileXml : lstFileOtg){
						String fileNameSource = fileXml.getFileNew();
						
						Uri uri = Uri.parse(fileNameSource);
						String fileName = uri.getLastPathSegment();
						
						String fileNameDest = folderHandler.getFileMasterNew() + "/"+ fileName;
						
						FileInputStream fis = new FileInputStream(new File(fileNameSource));
					    FileOutputStream fos = new FileOutputStream(new File(fileNameDest));
					    
			            fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
			            fis.close();
			            fos.close();
			            
			            MediaScannerConnection.scanFile(SettingsMasterDownloadActivity.this, new String[] {new File(fileNameDest).getAbsolutePath() }, null, null);
			            
			            publishProgress(fileName);
					}
				}catch(Exception e){
					e.printStackTrace();
					publishProgress(e.getMessage());
				}
			}
			
			lstMaster = fileMasterHandler.getFiles(folderHandler.getFileMasterNew(), folderHandler.getFileMasterBackup(), ".xml");
			
			if(lstMaster.size() > 0){
				lstMsgStatus = new ParsingHandler(SettingsMasterDownloadActivity.this).ParsingXML(lstMaster);
			}
			
			return lstMsgStatus;   
		}
		
		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);   
		}

		@Override
		protected void onPostExecute(List<MessageStatus> lstMsgStatus) {
			super.onPostExecute(lstMsgStatus);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null; 
			}
			
			if(lstMsgStatus != null && lstMsgStatus.size() > 0){
				String message = "";
				for(MessageStatus msg : lstMsgStatus){
					if(msg != null) {
						message = message + "\n" + msg.getMenu() + ": " + msg.getMessage();
					}
				}
				
				if(!TextUtils.isEmpty(message))
					Toast.makeText(SettingsMasterDownloadActivity.this, message, Toast.LENGTH_SHORT).show();
				
				
				new GetDataAsyncTask().execute();
			}
		}
	}  

	private class GetDataAsyncTask extends AsyncTask<Void, MasterDownload, List<MasterDownload>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(SettingsMasterDownloadActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<MasterDownload> doInBackground(Void... params) {
			DatabaseHandler database = new DatabaseHandler(SettingsMasterDownloadActivity.this);

			database.openTransaction();
			List<Object> lstObject = database.getListData(false, MasterDownload.TABLE_NAME, null,
					null, null,null, null, MasterDownload.XML_NAME, null);
			database.closeTransaction();  
			
			List<MasterDownload> lstMaster = new ArrayList<MasterDownload>();
			
			if(lstObject.size() > 0){
				for(int i = 0; i < lstObject.size(); i++){
					MasterDownload md = (MasterDownload) lstObject.get(i);
					
					lstMaster.add(md);
				}
			}
			
			return lstMaster;
		}
		
		@Override
		protected void onPostExecute(List<MasterDownload> lstTemp) {
			super.onPostExecute(lstTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			

			lstMasterDownload = lstTemp;
			adapter = new AdapterMasterDownload(SettingsMasterDownloadActivity.this, lstMasterDownload, R.layout.item_master_download);
			lsvSettingsMasterDownload.setAdapter(adapter);
		}
	}

	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
    	 
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_ATTACHED)){
            	cbxSettingsMasterDownUsbOtg.setChecked(true);
            	isUsbOtgAvailable = true;
            }else if(action.equalsIgnoreCase(UsbManager.ACTION_USB_DEVICE_DETACHED)){
            	cbxSettingsMasterDownUsbOtg.setChecked(false);
            	isUsbOtgAvailable = false;
            }
            
            if(action.equalsIgnoreCase(ACTION_USB_PERMISSION) && isUsbOtgAvailable){
            	synchronized (this) {
                  UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                  cbxSettingsMasterDownUsbOtg.setChecked(false);
                  isUsbOtgPermit = false;

                  if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                      if (device != null) {
                    	  cbxSettingsMasterDownUsbOtg.setChecked(true);
                      	  isUsbOtgPermit = true;
                      }
                  }
              }
            }

        }
    };

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		unregisterReceiver(mUsbReceiver);
	}
}
