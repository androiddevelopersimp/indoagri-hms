package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterSKB;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.SKB;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterSKBActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsv_master_skb;
	private EditText edt_master_skb_search;
	
	private List<SKB> lst_skb;
	private AdapterSKB adapter;
	
	private GetDataAsyncTask get_data;
	private DialogProgress dialogProgress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_skb);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsv_master_skb = (ListView) findViewById(R.id.lsv_master_skb);
		edt_master_skb_search = (EditText) findViewById(R.id.edt_master_skb_search);

		ImageButton btn_action_bar_left = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txt_action_bar_title = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btn_action_bar_right = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btn_action_bar_left.setOnClickListener(this);
		txt_action_bar_title.setText(getResources().getString(R.string.master_skb));
		btn_action_bar_right.setVisibility(View.INVISIBLE);
		edt_master_skb_search.addTextChangedListener(this);
		lsv_master_skb.setOnItemClickListener(this);
		
		getData();
	}
	
	private void getData(){  
//		DatabaseHandler database = new DatabaseHandler(MasterSKBActivity.this);
//		List<Object> lst_object;
//		
//		lst_object = database.getData(SKB.TABLE_NAME, null, null, null, null, null, SKB.XML_ID);
//		
//		if(lst_object.size() > 0){
//			lst_skb = new ArrayList<SKB>();
//			
//			for(int i = 0; i < lst_object.size(); i++){
//				SKB skb = (SKB) lst_object.get(i);
//				
//				lst_skb.add(skb);
//			}
//			
//			adapter = new AdapterSKB(MasterSKBActivity.this, lst_skb, R.layout.item_skb);
//		}else{
//			adapter = null;
//		}
//		
//		lsv_master_skb.setAdapter(adapter);
		
		get_data = new GetDataAsyncTask();
		get_data.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
		
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<SKB>, List<SKB>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterSKBActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<SKB> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterSKBActivity.this);

			database.openTransaction();
			List<Object> lst_object =  database.getListData(false, SKB.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			List<SKB> lst_skb = new ArrayList<SKB>();
			
			if(lst_object.size() > 0){
				for(int i = 0; i < lst_object.size(); i++){
					SKB skb = (SKB) lst_object.get(i);
					
					lst_skb.add(skb);
				}
			}
			
			return lst_skb;
		}
		
		@Override
		protected void onPostExecute(List<SKB> lst_load) {
			super.onPostExecute(lst_load);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(lst_load.size() > 0){
				lst_skb = lst_load;
				adapter = new AdapterSKB(MasterSKBActivity.this, lst_skb, R.layout.item_skb);
			}else{
				adapter = null;
			}
			
			lsv_master_skb.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(get_data != null && get_data.getStatus() != AsyncTask.Status.FINISHED){
			get_data.cancel(true);
		}
		   
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(get_data != null && get_data.getStatus() != AsyncTask.Status.FINISHED){
			get_data.cancel(true);
		}
	}
	
}
