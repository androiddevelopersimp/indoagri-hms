package com.simp.hms.activity.bpn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNReportBlock;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BPNReportBlock;
import com.simp.hms.model.BPNReportHarvester;
import com.simp.hms.model.UserLogin;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class BPNReportBlockActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private EditText edtBpnReportBlockSearch;
	private ListView lsvBpnReportBlock;
	
	private List<BPNReportBlock> lstBPNReport;
	private AdapterBPNReportBlock adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	DatabaseHandler database = new DatabaseHandler(BPNReportBlockActivity.this);
	
	String companyCode = "";
	String estate = "";
	String division = "";
	String gang = "";
	String nikClerk = "";
	String nikHarvester = "";
	String bpnDate = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_bpn_report_block);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		edtBpnReportBlockSearch = (EditText) findViewById(R.id.edtBpnReportBlockSearch);
		lsvBpnReportBlock = (ListView) findViewById(R.id.lsvBpnReportBlock);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
		btnActionBarright.setVisibility(View.INVISIBLE);
		edtBpnReportBlockSearch.addTextChangedListener(this);
		lsvBpnReportBlock.setOnItemClickListener(this);
		
		BPNReportHarvester bpnReport = null;
		
		if(getIntent().getExtras() != null){
			bpnReport = (BPNReportHarvester) getIntent().getParcelableExtra(BPNReportHarvester.TABLE_NAME);
			bpnDate = getIntent().getExtras().getString(BPNHeader.XML_BPN_DATE);
		}
		
		if(bpnReport != null){
			companyCode = bpnReport.getCompanyCode();
			estate = bpnReport.getEstate();
			division = bpnReport.getDivision();
			gang = bpnReport.getGang();
			nikHarvester = bpnReport.getNik();
			
		}
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		BPNReportBlock bpnReport = (BPNReportBlock) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(BPNReportBlock.TABLE_NAME, bpnReport);
		
		startActivity(new Intent(BPNReportBlockActivity.this, BPNReportTphActivity.class)
				.putExtras(bundle)
				.putExtra(BPNHeader.XML_BPN_DATE, bpnDate)
				.putExtra(BPNHeader.XML_NIK_HARVESTER, nikHarvester));
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtBpnReportHarvesterBpnDate:
			new DialogDate(BPNReportBlockActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtHarvestBookHistoryBpnDate).show();
			break;
		case R.id.btnBpnReportHarvesterSearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;   
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BPNReportBlock>, List<BPNReportBlock>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(BPNReportBlockActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override 
		protected List<BPNReportBlock> doInBackground(Void... voids) {
			
//			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
//				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}  
			
//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME, 
//					new String [] {BPNHeader.XML_LOCATION}, 
//					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//					BPNHeader.XML_ESTATE + "=?" + " and " +
//					BPNHeader.XML_DIVISION + "=?" + " and " +
//					BPNHeader.XML_GANG + "=?" + " and " +
//					BPNHeader.XML_NIK_CLERK + "=?" + " and " +
//					BPNHeader.XML_BPN_DATE + "=?" + " and " +
//					BPNHeader.XML_NIK_HARVESTER + "=?", 
//					new String [] {companyCode, estate, division, gang, nik, bpnDate, nikHarvester},
//					BPNHeader.XML_LOCATION, null, BPNHeader.XML_LOCATION, null);
//			database.closeTransaction();
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME, 
					new String [] {BPNHeader.XML_LOCATION, "min (" + BPNHeader.XML_CREATED_DATE + ") as " + BPNHeader.XML_CREATED_DATE}, 
					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
					BPNHeader.XML_ESTATE + "=?" + " and " +
					BPNHeader.XML_DIVISION + "=?" + " and " +
					BPNHeader.XML_NIK_CLERK + "=?" + " and " +
					BPNHeader.XML_BPN_DATE + "=?" + " and " +
					BPNHeader.XML_NIK_HARVESTER + "=?", 
					new String [] {companyCode, estate, division, nik, bpnDate, nikHarvester},
					BPNHeader.XML_LOCATION, null, BPNHeader.XML_CREATED_DATE, null);
			database.closeTransaction();
			
			List<BPNReportBlock> listTemp = new ArrayList<BPNReportBlock>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BPNHeader bpnHeader = (BPNHeader) listObject.get(i);
					
					String block = bpnHeader.getLocation();

					double qtyJanjang = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.JANJANG_CODE);
					double qtyLooseFruit = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.LOOSE_FRUIT_CODE);
					double qtyMentah = getQly(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.JANJANG_CODE, BPNQuality.MENTAH_CODE);
					double qtyBusuk = getQly(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.JANJANG_CODE, BPNQuality.BUSUK_CODE);  
					double qtyTangkaiPanjang = getQly(companyCode, estate, division, gang, bpnDate, nikHarvester, block, BPNQuantity.JANJANG_CODE, BPNQuality.TANGKAI_PANJANG_CODE); 
					
					
					listTemp.add(new BPNReportBlock(companyCode, estate, division, gang, block, qtyJanjang, qtyLooseFruit, qtyMentah, qtyBusuk, qtyTangkaiPanjang));
				}
			}
			  
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BPNReportBlock> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				lstBPNReport = listTemp;
				adapter = new AdapterBPNReportBlock(BPNReportBlockActivity.this, lstBPNReport, R.layout.item_bpn_report_block);
			}else{
				adapter = null;
			}
			
			lsvBpnReportBlock.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
	private double getQty(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String block, String achievementCode){
		double qty = 0;
		
		database.openTransaction();
		List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null, 
				BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
				BPNQuantity.XML_ESTATE + "=?" + " and " +
				BPNQuantity.XML_DIVISION + "=?" + " and " +
				BPNQuantity.XML_BPN_DATE + "=?" + " and " +
				BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
				BPNQuantity.XML_LOCATION + "=?" + " and " +
				BPNQuantity.XML_ACHIEVEMENT_CODE + "=?" + " and " +
				BPNQuantity.XML_CROP + "=?", 
				new String [] {companyCode, estate, division, bpnDate, nikHarvester, block, achievementCode, "01"}, 
				null, null, null, null);
		database.closeTransaction();
		
		if(lstQty.size() > 0){
			for(int i = 0; i < lstQty.size(); i++){
				BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);
				
				qty = qty + bpnQuantity.getQuantity();
			}
		}
		
		return qty;
	}
	
	private double getQly(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String block, String achievementCode, String qualityCode){
		double qly = 0;
		
		database.openTransaction();
		List<Object> lstQly = database.getListData(false, BPNQuality.TABLE_NAME, null, 
				BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
				BPNQuality.XML_ESTATE + "=?" + " and " +
				BPNQuality.XML_DIVISION + "=?" + " and " +
				BPNQuality.XML_BPN_DATE + "=?" + " and " +
				BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +  
				BPNQuality.XML_LOCATION + "=?" + " and " + 
				BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
				BPNQuality.XML_QUALITY_CODE + "=?" + " and " +
				BPNQuality.XML_CROP + "=?",
				new String [] {companyCode, estate, division, bpnDate, nikHarvester, block, achievementCode, qualityCode, "01"},
				null, null, null, null);
		database.closeTransaction();
		
		if(lstQly.size() > 0){
			for(int i = 0; i < lstQly.size(); i++){
				BPNQuality bpnQuality = (BPNQuality) lstQly.get(i);
				
				qly = qly + bpnQuality.getQuantity();
			}
		}
		
		return qly;
	}
}
