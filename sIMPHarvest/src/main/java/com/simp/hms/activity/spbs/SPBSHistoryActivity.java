package com.simp.hms.activity.spbs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.simp.hms.R;
import com.simp.hms.R.drawable;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.activity.spu.SPUPODActivity;
import com.simp.hms.adapter.AdapterSPBSHistory;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.UserLogin;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


public class SPBSHistoryActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogNotificationListener, DialogDateListener, DialogConfirmListener{
	private Toolbar tbrMain;
	private TextView txtSpbsHistorySpbsDate;
	private Button btnSpbsHistorySearch;
	private SwipeMenuListView lsvSpbsHistory;
	private EditText edtSpbsHistorySearch;
	
	private List<SPBSHeader> lstSpbsHeader;
	private AdapterSPBSHistory adapter;

	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	private DialogConfirm dialogConfirm;
	
	DatabaseHandler database = new DatabaseHandler(SPBSHistoryActivity.this);
	
	String spbsDate = "";
	
	int posSelected = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_spbs_history);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtSpbsHistorySpbsDate = (TextView) findViewById(R.id.txtSpbsHistorySpbsDate);
		btnSpbsHistorySearch = (Button) findViewById(R.id.btnSpbsHistorySearch);
		lsvSpbsHistory = (SwipeMenuListView) findViewById(R.id.lsvSpbsHistory);
		edtSpbsHistorySearch = (EditText) findViewById(R.id.edtSpbsHistorySearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText("Surat Pengantar History");
		btnActionBarRight.setVisibility(View.INVISIBLE);
		txtSpbsHistorySpbsDate.setOnClickListener(this);
		btnSpbsHistorySearch.setOnClickListener(this);
		edtSpbsHistorySearch.addTextChangedListener(this);
		lsvSpbsHistory.setOnItemClickListener(this);
		
//		getDataAsync = new GetDataAsyncTask();
//		getDataAsync.execute();
		
		spbsDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtSpbsHistorySpbsDate.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		
//		SwipeMenuCreator creator = new SwipeMenuCreator() {
//
//			@Override
//			public void create(SwipeMenu menu) {
//				// create "open" item
//				SwipeMenuItem openItem = new SwipeMenuItem(
//						getApplicationContext());
//				// create "delete" item
//				SwipeMenuItem deleteItem = new SwipeMenuItem(
//						getApplicationContext());
//				// set item background
//				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
//						0x3F, 0x25)));
//				// set item width
//				deleteItem.setWidth(dp2px(90));
//				// set a icon
//				deleteItem.setIcon(R.drawable.ic_delete_pressed);
//				// add to menu
//				menu.addMenuItem(deleteItem);
//			}
//		};
		
		//lsvSpbsHistory.setMenuCreator(creator);
//		lsvSpbsHistory.setOnMenuItemClickListener(new OnMenuItemClickListener() {
//
//			@Override
//			public boolean onMenuItemClick(int pos, SwipeMenu menu, int index) {
//				dialogConfirm = new DialogConfirm(SPBSHistoryActivity.this,
//						getResources().getString(R.string.informasi),
//						getResources().getString(R.string.data_delete), null, R.id.lsvSpbsHistory);
//				dialogConfirm.show();
//
//				posSelected = pos;
//				return false;
//			}
//		});
	}
  
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		SPBSHeader spbsHeader = (SPBSHeader) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(SPBSHeader.TABLE_NAME, spbsHeader);
		
		database.openTransaction();
		Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();

		if(spbsHeader.getCrop().equalsIgnoreCase("01")) {
			if (bluetooth != null && bluetooth.getName().equalsIgnoreCase("woosim")) {
				startActivity(new Intent(SPBSHistoryActivity.this, SPBSWoosimActivity.class).putExtras(bundle));
			} else if (bluetooth != null && bluetooth.getName().equalsIgnoreCase("DPP-350")) {
				startActivity(new Intent(SPBSHistoryActivity.this, SPBSDatecsActivity.class).putExtras(bundle));
			} else {
				startActivity(new Intent(SPBSHistoryActivity.this, SPBSActivity.class).putExtras(bundle));
			}
		}else if(spbsHeader.getCrop().equalsIgnoreCase("04")){
			startActivity(new Intent(SPBSHistoryActivity.this, SPUPODActivity.class).putExtras(bundle));
		}
	}
  
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtSpbsHistorySpbsDate:
			new DialogDate(SPBSHistoryActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtSpbsHistorySpbsDate).show();
			break;
		case R.id.btnSpbsHistorySearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:    
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSHeader>, List<SPBSHeader>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dialogProgress = new DialogProgress(SPBSHistoryActivity.this, getResources().getString(R.string.loading));
			dialogProgress.show();
		}

		@Override
		protected List<SPBSHeader> doInBackground(Void... voids) {
			List<SPBSHeader> listTemp = new ArrayList<SPBSHeader>();
			List<Object> listObject;
			
			String companyCode = "";
			String estate = "";
			String division = "";
			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}
			
			database.openTransaction();
			listObject =  database.getListData(false, SPBSHeader.TABLE_NAME, null, 
					SPBSHeader.XML_COMPANY_CODE + "=?" + " and " + 
					SPBSHeader.XML_ESTATE + "=?" + " and " +
					SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
					SPBSHeader.XML_SPBS_DATE + "=?", 
					new String [] {companyCode, estate, nik, spbsDate}, 
					null, null, SPBSHeader.XML_CREATED_DATE + " desc", null);
			database.closeTransaction();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					SPBSHeader spbsHeader = (SPBSHeader) listObject.get(i);
					
					listTemp.add(spbsHeader);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<SPBSHeader> lstTemp) {
			super.onPostExecute(lstTemp);
			
			if(dialogProgress != null){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
//			if(lstTemp.size() > 0){
				lstSpbsHeader = lstTemp;
				adapter = new AdapterSPBSHistory(SPBSHistoryActivity.this, lstSpbsHeader, R.layout.item_spbs_history);
//			}else{
//				adapter = null;
//			}
			
			lsvSpbsHistory.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDateOK(Date date, int id) {
		spbsDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtSpbsHistorySpbsDate.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		if(R.id.lsvSpbsHistory == id){
			adapter.deleteData(posSelected);
		}
	}
}
