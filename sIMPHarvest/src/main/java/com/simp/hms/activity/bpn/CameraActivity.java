package com.simp.hms.activity.bpn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.model.BPNHeader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class CameraActivity extends Activity implements Callback {
	private SurfaceView srf_preview;
	private ImageView img_preview;   
	private LinearLayout lnr_action;
	
	private SurfaceHolder srf_holder;
	private Camera cmr_preview;
	private String filename = "";   
	
	private final int WIDTH_DEFAULT = 320;   
	private final int HEIGHT_DEFAULT = 240;
	private final String TAG = "camera";  
	
	private boolean is_taken = false;
	   
	private double latitude;
	private double longitude;
	private String photo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_camera);
		
		srf_preview = (SurfaceView) findViewById(R.id.srf_preview);
		img_preview = (ImageView) findViewById(R.id.img_preview);
		lnr_action = (LinearLayout) findViewById(R.id.lnr_action);
//		
//		latitude = Converter.StrToDouble(getIntent().getExtras().getString(DatabaseStructure.BUKU_PANEN_LATITUDE));
//		longitude = Converter.StrToDouble(getIntent().getExtras().getString(DatabaseStructure.BUKU_PANEN_LONGITUDE));
		photo = getIntent().getExtras().getString(BPNHeader.XML_PHOTO);
//		
		latitude = 0;
		longitude = 0;
		photo = "";
		
		Log.d("tag", ":" + latitude);
		Log.d("tag", ":" + longitude);
		
		srf_holder = srf_preview.getHolder();
		srf_holder.addCallback(this);
		srf_holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);		
		
//		resizeView();
		showTakeAction();	
	} 
	
	private void resizeView(){
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		
		int width = metrics.widthPixels;
		int height = metrics.heightPixels;
		
		if(width * HEIGHT_DEFAULT > height * WIDTH_DEFAULT){
			int scaled_width = (WIDTH_DEFAULT * height)/HEIGHT_DEFAULT;			
			width = (width + scaled_width)/2 - (width - scaled_width)/2;			
		}else{
			int scaled_height = (HEIGHT_DEFAULT * width)/WIDTH_DEFAULT;
			height = (height + scaled_height)/2 - (height - scaled_height)/2;
		}
		  
		RelativeLayout.LayoutParams layout_params = new RelativeLayout.LayoutParams(width, height);
		layout_params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		
		srf_preview.setLayoutParams(layout_params);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if(!is_taken){
			Camera.Parameters params = cmr_preview.getParameters();
			
			params.setPreviewSize(WIDTH_DEFAULT, HEIGHT_DEFAULT);
	
			int const_width = WIDTH_DEFAULT;
			int const_height = HEIGHT_DEFAULT;   
			
			params.setPictureSize(const_width, const_height);
			params.setJpegQuality(100);
			
			cmr_preview.setDisplayOrientation(90);
			cmr_preview.setParameters(params);
			
			initCamera();
		}else{
//			Drawable drawable = Drawable.createFromPath(filename);
//			srf_preview.setBackgroundDrawable(drawable);
		} 
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		cmr_preview = Camera.open();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		cmr_preview.stopPreview();
		cmr_preview.release();
		cmr_preview = null;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
	
	
	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	private void showPreviewAction(){
		LayoutInflater infalter = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		View view = infalter.inflate(R.layout.layout_take_action, null);
		Button btnCameraTake = (Button) view.findViewById(R.id.btnCameraTake);
		Button btnCameraSave = (Button) view.findViewById(R.id.btnCameraSave);
		Button btnCameraRetake = (Button) view.findViewById(R.id.btnCameraRetake);
		Button btnCameraCancel = (Button) view.findViewById(R.id.btnCameraCancel);
		
		btnCameraTake.setVisibility(View.GONE);
		btnCameraSave.setVisibility(View.VISIBLE);
		btnCameraRetake.setVisibility(View.VISIBLE);  
		btnCameraCancel.setVisibility(View.VISIBLE);
		
		btnCameraSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(CameraActivity.this, BPNActivity.class);
				intent.putExtra(BPNHeader.XML_PHOTO, filename);
				
				setResult(RESULT_OK, intent);
				finish();
			}
		});
		
		btnCameraTake.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				is_taken = true;
			}
		});
		
		btnCameraRetake.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				is_taken = false;				
				initCamera();
				showTakeAction();
				
				File file = new File(filename);	
				
				if(file.exists())
					file.delete();
			}
		});
		
		btnCameraCancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				is_taken = false;
				File file = new File(filename);	
				
				if(file.exists())
					file.delete();
				
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		
		lnr_action.removeAllViews();
		lnr_action.addView(view);
	}

	private void initCamera(){
		try {
			srf_preview.setVisibility(View.VISIBLE);
			img_preview.setVisibility(View.GONE);
			cmr_preview.setPreviewDisplay(srf_holder);
			cmr_preview.startPreview();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void showTakeAction(){
		LayoutInflater infalter = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		View view = infalter.inflate(R.layout.layout_take_action, null);
		Button btnCameraTake = (Button) view.findViewById(R.id.btnCameraTake);
		Button btnCameraSave = (Button) view.findViewById(R.id.btnCameraSave);
		Button btnCameraRetake = (Button) view.findViewById(R.id.btnCameraRetake);
		Button btnCameraCancel = (Button) view.findViewById(R.id.btnCameraCancel);
		
		btnCameraTake.setVisibility(View.VISIBLE);
		btnCameraSave.setVisibility(View.GONE);
		btnCameraRetake.setVisibility(View.GONE);
		btnCameraCancel.setVisibility(View.GONE);
		
		btnCameraTake.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				is_taken = true;
				cmr_preview.takePicture(mShutterCallback, mRawCallback, mPictureCallback);
			}
		});
		lnr_action.removeAllViews();
		lnr_action.addView(view);
	}
	
	
	AutoFocusCallback mAutoFocusCallback = new AutoFocusCallback() {		
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
			Toast.makeText(getApplicationContext(), "Auto focus", Toast.LENGTH_SHORT).show();
		}
	};
	
	ShutterCallback mShutterCallback = new ShutterCallback(){
		  @Override
		  public void onShutter() {}
	};
	
	PictureCallback mPictureCallback = new PictureCallback() {
		public void onPictureTaken(byte[] imageData, Camera camera) {

			if (imageData != null) {
				showPreviewAction();
				boolean stored = storeByteImage(imageData);
				
				if(stored){
					srf_preview.setVisibility(View.INVISIBLE);
					img_preview.setVisibility(View.VISIBLE);
					/*
					try {
						ExifInterface exif = new ExifInterface(filename);
						createExifData(exif);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					*/
					Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
					img_preview.setImageBitmap(bitmap);  
				}
			}
		}
	};
	
	PictureCallback mRawCallback = new PictureCallback() {  
		//@Override
		public void onPictureTaken(byte[] data, Camera camera) {

		}
	};
	
	public boolean storeByteImage(byte[] imageData) {	
		FolderHandler folderHandler = new FolderHandler(CameraActivity.this);
		
		if(folderHandler.init() && folderHandler.isSDCardWritable()){
			File dest = new File(folderHandler.getFilePhoto(), System.currentTimeMillis() + ".jpg");
			filename = dest.getAbsolutePath();
			
			try{
				FileOutputStream fos = new FileOutputStream(dest);
				
				Bitmap bmp = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
				
			    int width = bmp.getWidth();
			    int height = bmp.getHeight();
			    float scaleWidth = ((float) WIDTH_DEFAULT) / width;
			    float scaleHeight = ((float) HEIGHT_DEFAULT) / height;
			    
			    Matrix matrix = new Matrix();
			    matrix.postScale(scaleWidth, scaleHeight);
				
			    bmp = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, false);
				
				ExifInterface exif = new ExifInterface(dest.toString());
				
				if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")){
					bmp = rotate(bmp, 90);
			    } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")){
			        bmp = rotate(bmp, 270);
			    } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")){
			        bmp = rotate(bmp, 180);
			    } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")){
			        bmp = rotate(bmp, 90);
			    }
				
				exif.setAttribute(ExifInterface.TAG_IMAGE_WIDTH, String.valueOf(WIDTH_DEFAULT));
				exif.setAttribute(ExifInterface.TAG_IMAGE_LENGTH, String.valueOf(HEIGHT_DEFAULT));
		        exif.setAttribute(ExifInterface.TAG_MAKE, "HMS");
		        exif.setAttribute(ExifInterface.TAG_MODEL, "HMS");
		        
		        exif.setAttribute(ExifInterface.TAG_DATETIME, (new Date(System.currentTimeMillis())).toString());
				
				exif.saveAttributes();
				
				bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);

				fos.flush();
		        fos.close();
		        
		        MediaScannerConnection.scanFile(CameraActivity.this, new String[] { dest.getAbsolutePath() }, null, null);
			
			}catch(FileNotFoundException e){
				e.printStackTrace();
				return false;
			}catch(IOException e){
				e.printStackTrace();
				return false;
			}
			return true;
		}else{
			return false;
		}
	}	
	
	public void createExifData(ExifInterface exif){
        if (latitude < 0) {
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
            latitude = -latitude;
        } else {
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
        }
        
        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, formatLatLongString(latitude));
        
        if (longitude < 0) {
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
            longitude = -longitude;
        } else {
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
        }
        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, formatLatLongString(longitude));
               
        exif.setAttribute(ExifInterface.TAG_MAKE, "SIMP Harvest");
        exif.setAttribute(ExifInterface.TAG_MODEL, "SIMP Harvest");
        
        exif.setAttribute(ExifInterface.TAG_DATETIME, (new Date(System.currentTimeMillis())).toString()); // set the date & time
        
        try {
			exif.saveAttributes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	private static String formatLatLongString(double d) {
	        // format latitude and longitude according to exif format
	        StringBuilder b = new StringBuilder();
	        b.append((int) d);
	        b.append("/1,");
	        d = (d - (int) d) * 60;
	        b.append((int) d);
	        b.append("/1,");
	        d = (d - (int) d) * 60000;
	        b.append((int) d);
	        b.append("/1000");
	        return b.toString();
	 }
	
	private Bitmap rotate(Bitmap bitmap, int degree) {
		    int w = bitmap.getWidth();
		    int h = bitmap.getHeight();

		    Matrix mtx = new Matrix();
		    mtx.postRotate(degree);

		    return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
		}
//	public void createExifData(ExifInterface exif){
//		double lat = -6.185289;
//		double lon = 106.825094;
//
//		int num1Lat = (int)Math.floor(lat);
//	    int num2Lat = (int)Math.floor((lat - num1Lat) * 60);
//	    double num3Lat = (lat - ((double)num1Lat+((double)num2Lat/60))) * 3600000;
//
//	    int num1Lon = (int)Math.floor(lon);
//	    int num2Lon = (int)Math.floor((lon - num1Lon) * 60);
//	    double num3Lon = (lon - ((double)num1Lon+((double)num2Lon/60))) * 3600000;
//
//	    exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, num1Lat+"/1,"+num2Lat+"/1,"+num3Lat+"/1000");
//	    exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, num1Lon+"/1,"+num2Lon+"/1,"+num3Lon+"/1000");
//
//	    if (lat > 0) {
//	        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N"); 
//	    } else {
//	        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
//	    }
//
//	    if (lon > 0) {
//	        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");    
//	    } else {
//	    exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
//	    }
//                
//        exif.setAttribute(ExifInterface.TAG_MAKE, "Anka");
//        exif.setAttribute(ExifInterface.TAG_MODEL, "Appsol");                
//        exif.setAttribute(ExifInterface.TAG_DATETIME, (new Date(System.currentTimeMillis())).toString());
//        
//        try {
//        	exif.saveAttributes();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
