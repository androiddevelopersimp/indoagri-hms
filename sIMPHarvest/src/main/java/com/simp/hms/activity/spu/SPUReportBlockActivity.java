package com.simp.hms.activity.spu;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spbs.SPBSReportBlockActivity;
import com.simp.hms.activity.spbs.SPBSReportTphActivity;
import com.simp.hms.adapter.AdapterSPBSReportBlock;
import com.simp.hms.adapter.AdapterSPUReportBlock;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSReportBlock;
import com.simp.hms.model.SPBSReportNumber;
import com.simp.hms.model.SPUReportBlock;
import com.simp.hms.model.SPUReportNumber;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SPUReportBlockActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, DialogDateListener, TextWatcher {
    private Toolbar tbrMain;
    private TextView edtSpuReportBlockSearch;
    private ListView lsvSpuReportBlock;

    private List<SPUReportBlock> lstSPUReport;
    private AdapterSPUReportBlock adapter;

    private GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(SPUReportBlockActivity.this);

    String year = "";
    String companyCode = "";
    String estate = "";
    String crop = "";
    String spuNumber = "";
    String spuDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_spu_report_block);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        edtSpuReportBlockSearch = (TextView) findViewById(R.id.edtSpuReportBlockSearch);
        lsvSpuReportBlock = (ListView) findViewById(R.id.lsvSpuReportBlock);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.spu));
        btnActionBarright.setVisibility(View.INVISIBLE);
        edtSpuReportBlockSearch.setOnClickListener(this);
        lsvSpuReportBlock.setOnItemClickListener(this);

        SPUReportNumber spuReport = null;

        if(getIntent().getExtras() != null){
            spuReport = getIntent().getExtras().getParcelable(SPUReportNumber.TABLE_NAME);
            spuDate = getIntent().getExtras().getString(SPBSHeader.XML_SPBS_DATE);
        }

        if(spuReport != null){
            year = spuReport.getYear();
            companyCode = spuReport.getCompanyCode();
            estate = spuReport.getEstate();
            crop = spuReport.getCrop();
            spuNumber = spuReport.getSpuNumber();
        }

        getDataAsync = new SPUReportBlockActivity.GetDataAsyncTask();
        getDataAsync.execute();

        edtSpuReportBlockSearch.addTextChangedListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        SPUReportBlock spuReport = (SPUReportBlock) adapter.getItem(pos);

        Bundle bundle = new Bundle();
        bundle.putParcelable(SPUReportBlock.TABLE_NAME, spuReport);

        startActivity(new Intent(SPUReportBlockActivity.this, SPUReportTphActivity.class)
                .putExtras(bundle)
                .putExtra(SPBSHeader.XML_SPBS_DATE, spuDate));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        spuDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

        edtSpuReportBlockSearch.setText(spuDate);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable editable) {
        adapter.getFilter().filter(editable);
        adapter.notifyDataSetChanged();
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPUReportBlock>, List<SPUReportBlock>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(SPUReportBlockActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<SPUReportBlock> doInBackground(Void... voids) {
            database.openTransaction();
            List<Object> listObject =  database.getListData(true, SPBSLine.TABLE_NAME,
                    new String [] {SPBSLine.XML_BLOCK},
                    SPBSLine.XML_YEAR + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?" + " and " +
                            SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                            SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                            SPBSLine.XML_CROP + "=?",
                    new String [] {year, companyCode, estate, spuNumber, spuDate, "04"},
                    SPBSLine.XML_BLOCK, null, SPBSLine.XML_BLOCK, null);
            database.closeTransaction();

            List<SPUReportBlock> listTemp = new ArrayList<SPUReportBlock>();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    SPBSLine spuLine = (SPBSLine) listObject.get(i);

                    String block = spuLine.getBlock();

                    double qtyGoodQty = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.GOOD_QTY_CODE);
                    double qtyGoodWeight = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.GOOD_WEIGHT_CODE);
                    double qtyBadQty = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.BAD_QTY_CODE);
                    double qtyBadWeight = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.BAD_WEIGHT_CODE);
                    double qtyPoorQty = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POOR_QTY_CODE);
                    double qtyPoorWeight = getQty(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POOR_WEIGHT_CODE);

                    listTemp.add(new SPUReportBlock(year, companyCode, estate, "04", spuNumber, spuDate, block,
                                                        qtyGoodQty, qtyGoodWeight,qtyBadQty,qtyBadWeight, qtyPoorQty, qtyPoorWeight));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<SPUReportBlock> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(listTemp.size() > 0){
                lstSPUReport = listTemp;
                adapter = new AdapterSPUReportBlock(SPUReportBlockActivity.this, lstSPUReport, R.layout.item_spu_report_block);
            }else{
                adapter = null;
            }

            lsvSpuReportBlock.setAdapter(adapter);
        }
    }

    private double getQty(String year, String companyCode, String estate, String spbsNumber, String spbsDate, String block, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, SPBSLine.TABLE_NAME, null,
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                        SPBSLine.XML_BLOCK + "=?" + " and " +
                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?",
                new String [] {year, companyCode, estate, spbsNumber, spbsDate, block, achievementCode, "04"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                SPBSLine SPBSLine = (SPBSLine) lstQty.get(i);

                qty = qty + SPBSLine.getQuantityAngkut();
            }
        }

        return qty;
    }
}
