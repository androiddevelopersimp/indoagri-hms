package com.simp.hms.activity.other;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBluetooth;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.database.DatabaseHelper;
import com.simp.hms.database.SharedPreferencesHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.DownloadAppsAsyncTask;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.handler.JsonHandler;
import com.simp.hms.handler.LanguageHandler;
import com.simp.hms.handler.NetworkHandler;
import com.simp.hms.handler.UpdateHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.Apps;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.DeviceAlias;
import com.simp.hms.model.SPBSRunningNumber;
import com.simp.hms.routines.Constanta;
import com.simp.hms.service.BluetoothPrintService;
import com.simp.hms.service.GPSTriggerService;

import android.app.ActionBar;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteException;
import android.media.MediaScannerConnection;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsWoosimActivity extends BaseActivity implements OnClickListener, OnCheckedChangeListener, 
OnItemClickListener, android.widget.RadioGroup.OnCheckedChangeListener, DialogNotificationListener, DialogConfirmListener {
	private Toolbar tbrMain;
	private TextView txtSettingsSyncMaster;
	private TextView txtSettingsUpdateApp;
	private TextView txtSettingsLanguage;
	private RadioGroup rgpSettingsLanguage;
	private RadioButton rbnSettingsLanguageEn;
	private RadioButton rbnSettingsLanguageIn;
	private CheckBox cbxSettingsWifi;
	private CheckBox cbxSettingsBluetooth;
	private TextView txtSettingsBluetoothMessage;
	private ListView lsvSettingsBluetooth;
	private TextView txtSettingsLocationServices;
	private TextView txtSettingsGpsServices;
	private CheckBox cbxSettingsGpsServices;
	private TextView txtSettingsAppVersion;
	private Button btnSettingsBackupDatabase;
	private Button btnSettingsRestoreDatabase;
	private TextView txtSettingsDeviceAlias;
	private Button btnSettingsDeviceAliasReset;
	
	List<Bluetooth> listBluetooth = new ArrayList<Bluetooth>();
	AdapterBluetooth adapter = new AdapterBluetooth(SettingsWoosimActivity.this, listBluetooth, R.layout.item_bluetooth);
	
	DatabaseHandler database = new DatabaseHandler(SettingsWoosimActivity.this);
	
	private static final String TAG = "BluetoothConnectMenu";
    private static final int REQUEST_ENABLE_BT = 2;

	private BluetoothAdapter bluetoothAdapter;
	private Vector<BluetoothDevice> remoteDevices;
	private BroadcastReceiver searchFinish;
	private BroadcastReceiver searchStart;
	private BroadcastReceiver discoveryResult;
	
	private BluetoothPrintService mPrintService = null;
	
	private GpsHandler gpsHandler;
	
	int idSelected = 0;
	
	DialogProgress dialogProgress;
	Apps apps;
	
	CheckUpdateAsyncTask checkAysncTask;
	
	public static final String DEVICE_NAME = "device_name";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_settings);
		
		registerBaseActivityReceiver();

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtSettingsSyncMaster = (TextView) findViewById(R.id.txtSettingsSyncMaster);
		txtSettingsUpdateApp = (TextView) findViewById(R.id.txtSettingsUpdateApp);
		txtSettingsLanguage = (TextView) findViewById(R.id.txtSettingsLanguage);
		rgpSettingsLanguage = (RadioGroup) findViewById(R.id.rgpSettingsLanguage);
		rbnSettingsLanguageEn = (RadioButton) findViewById(R.id.rbnSettingsLanguageEn);
		rbnSettingsLanguageIn = (RadioButton) findViewById(R.id.rbnSettingsLanguageIn);
		cbxSettingsWifi = (CheckBox) findViewById(R.id.cbxSettingsWifi);
		cbxSettingsBluetooth = (CheckBox) findViewById(R.id.cbxSettingsBluetooth);
		txtSettingsBluetoothMessage = (TextView) findViewById(R.id.txtSettingsBluetoothMessage);
		lsvSettingsBluetooth = (ListView) findViewById(R.id.lsvSettingsBluetooth);
		txtSettingsLocationServices = (TextView) findViewById(R.id.txtSettingsLocationServices);
		txtSettingsGpsServices = (TextView) findViewById(R.id.txtSettingsGpsServices);
		cbxSettingsGpsServices = (CheckBox) findViewById(R.id.cbxSettingsGpsServices);
		txtSettingsAppVersion = (TextView) findViewById(R.id.txtSettingsAppVersion);
		btnSettingsBackupDatabase = (Button) findViewById(R.id.btnSettingsDownloadDatabase);
		btnSettingsRestoreDatabase = (Button) findViewById(R.id.btnSettingsRestoreDatabase);
		txtSettingsDeviceAlias = (TextView) findViewById(R.id.txtSettingsDeviceAlias);
		btnSettingsDeviceAliasReset = (Button) findViewById(R.id.btnSettingsDeviceAliasReset);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.settings));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnActionBarRight.setOnClickListener(this);
		txtSettingsSyncMaster.setOnClickListener(this);
		txtSettingsUpdateApp.setOnClickListener(this);
		rgpSettingsLanguage.setOnCheckedChangeListener(this);
		cbxSettingsWifi.setOnCheckedChangeListener(this);
		cbxSettingsBluetooth.setOnCheckedChangeListener(this);
		lsvSettingsBluetooth.setOnItemClickListener(this);
		txtSettingsLocationServices.setOnClickListener(this);
		cbxSettingsGpsServices.setOnCheckedChangeListener(this);
		btnSettingsBackupDatabase.setOnClickListener(this);
		btnSettingsRestoreDatabase.setOnClickListener(this);
		btnSettingsDeviceAliasReset.setOnClickListener(this);
		
	    IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
	    registerReceiver(broadcastReceiver, filter);
	    
	    gpsHandler = new GpsHandler(SettingsWoosimActivity.this);
	    
	    getLanguage();
	    
	    cbxSettingsGpsServices.setChecked(isMyServiceRunning(GPSTriggerService.class));
	    
	    initMenu();
	    loadSettingFile();
		bluetoothSetup();
		getDeviceAlias();
		
		txtSettingsAppVersion.setText(String.format(getResources().getString(R.string.settings_version),
				new DeviceHandler(SettingsWoosimActivity.this).getAppVersion()));
		
		adapter = new AdapterBluetooth(SettingsWoosimActivity.this, listBluetooth, R.layout.item_bluetooth);
		lsvSettingsBluetooth.setAdapter(adapter);
		
		addPairedDevices();
		
		discoveryResult = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String key;
				BluetoothDevice remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				if(remoteDevice != null){
					
					Bluetooth device = new Bluetooth();
					
					if(remoteDevice.getBondState() != BluetoothDevice.BOND_BONDED){
						key = remoteDevice.getName() +"\n["+remoteDevice.getAddress()+"]";
						
						device.setName(remoteDevice.getName());
						device.setAddress(remoteDevice.getAddress());
						device.setPaired(false);
					}
					else{
						key = remoteDevice.getName() +"\n["+remoteDevice.getAddress()+"] [Paired]";
						
						device.setName(remoteDevice.getName());
						device.setAddress(remoteDevice.getAddress());
						device.setPaired(true);
					}
					

					remoteDevices.add(remoteDevice);
					add(device);
				}    
			}
		};
		registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));
		
		searchStart = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
			}
		};
		registerReceiver(searchStart, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
		
		searchFinish = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
			}
		};
		registerReceiver(searchFinish, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
		
		WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		
		cbxSettingsWifi.setChecked(wifiManager.isWifiEnabled());
		cbxSettingsBluetooth.setChecked(isBluetoothAvailable());
	}
	
    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupPrint() will then be called during onActivityResult
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mPrintService == null){
            	 mPrintService = new BluetoothPrintService(mHandler);	
            }
        }
    }

	@Override
	protected void onResume() {
		super.onResume();
		
        if (mPrintService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mPrintService.getState() == BluetoothPrintService.STATE_NONE) {
                // Start the Bluetooth print services
                mPrintService.start();
            }
        }
	}

	private void initMenu(){
		txtSettingsLanguage.setText(getResources().getString(R.string.settings_language));
		txtSettingsLocationServices.setText(getResources().getString(R.string.settings_location_services));
		txtSettingsGpsServices.setText(getResources().getString(R.string.settings_gps_services));
	}
	
	@Override
	public void onClick(View v) {  
		switch (v.getId()) {
		case R.id.txtSettingsSyncMaster:
			startActivity(new Intent(SettingsWoosimActivity.this, SettingsMasterDownloadActivity.class));
			break;
		case R.id.txtSettingsUpdateApp:
			if(new NetworkHandler(SettingsWoosimActivity.this).isNetworkConnected()){
				checkAysncTask = new CheckUpdateAsyncTask();
				checkAysncTask.execute();
			}else{
				new DialogNotification(SettingsWoosimActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.internet_connection_error), false).show();
			}
			break;
		case R.id.txtSettingsLocationServices:
			startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			break;
		case R.id.btnSettingsDownloadDatabase:
			try {
				copyAppDbToDownloadFolder();
			} catch (IOException e) {   
				e.printStackTrace();
	    		new DialogNotification(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
						e.getMessage(), false).show();  
			}
			break; 
		case R.id.btnSettingsRestoreDatabase:
				startActivity(new Intent(SettingsWoosimActivity.this, RestoreDatabaseActivity.class));
			break;
		case R.id.btnSettingsDeviceAliasReset:
			new DialogConfirm(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
					getResources().getString(R.string.settings_device_alias_dialod), null, R.id.btnSettingsDeviceAliasReset).show();
			break;
		default:   
			break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		
		switch (buttonView.getId()) {
		case R.id.cbxSettingsWifi:
			WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
			wifiManager.setWifiEnabled(isChecked);
			break;
		case R.id.cbxSettingsBluetooth:
			
			if(bluetoothAdapter == null)
				bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();   

			if(bluetoothAdapter != null){
				if(isChecked){
					txtSettingsBluetoothMessage.setVisibility(View.GONE);
					lsvSettingsBluetooth.setVisibility(View.VISIBLE);
					bluetoothAdapter.enable();
					
					if(bluetoothAdapter.isDiscovering()){
						bluetoothAdapter.cancelDiscovery();
					}
					
					clearBtDevData();
					listBluetooth.clear();
					adapter.clear();
					bluetoothAdapter.startDiscovery();	
					
					addPairedDevices();
				}else{
					txtSettingsBluetoothMessage.setVisibility(View.VISIBLE);
					lsvSettingsBluetooth.setVisibility(View.GONE);
					
					if(bluetoothAdapter.isDiscovering()){
						bluetoothAdapter.cancelDiscovery();
					}
					
					bluetoothAdapter.disable();
				}
			}else{
				new DialogNotification(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.bluetooth_not_support), false).show();
			}
			break;
		case R.id.cbxSettingsGpsServices:
			if(isChecked){
				gpsHandler.startGPS();;
			}else{
				gpsHandler.stopGPS();
			}
			break;
		default:
			break;
		}
	}   
	
	private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        final String action = intent.getAction();

	        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
	            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
	                                                 BluetoothAdapter.ERROR);
	            switch (state) {
	            case BluetoothAdapter.STATE_OFF:
	            	cbxSettingsBluetooth.setChecked(false);
	                break;  
	            case BluetoothAdapter.STATE_TURNING_OFF:
	                break;
	            case BluetoothAdapter.STATE_ON:
	            	cbxSettingsBluetooth.setChecked(true);
	                break;
	            case BluetoothAdapter.STATE_TURNING_ON:
	                break;
	            }
	        }
	    }
	};
	
	private void bluetoothSetup()
	{
		// Initialize
		clearBtDevData();
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		if (bluetoothAdapter == null) {
		    // Device does not support Bluetooth
			return;
		}
		
		if (!bluetoothAdapter.isEnabled()) {
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT); 
		}	
	}
	
	private static final String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "//temp";
	private static final String fileName = dir + "//BTPrinter";
	private String lastConnAddr;
	
	private void loadSettingFile(){
		int rin = 0;
		char [] buf = new char[128];
		try{	
			FileReader fReader = new FileReader(fileName);
			rin = fReader.read(buf);
			if(rin > 0){
				lastConnAddr = new String(buf,0,rin);
			}
			fReader.close();
		}
		catch (FileNotFoundException e){
			Log.i(TAG, "Connection history not exists.");
		}
		catch (IOException e){
			Log.e(TAG, e.getMessage(), e);
		}	
	}
	
	private void saveSettingFile(){
		try{
			File tempDir = new File(dir);
			if(!tempDir.exists()){
				tempDir.mkdir();
			}
			
			FileWriter fWriter = new FileWriter(fileName);
			if(lastConnAddr != null)
				fWriter.write(lastConnAddr);
			fWriter.close();
		}
		catch (FileNotFoundException e){
			Log.e(TAG, e.getMessage(), e);
		}
		catch (IOException e){
			Log.e(TAG, e.getMessage(), e);
		}	
	}
	
	// clear device data used list.
	private void clearBtDevData(){
		remoteDevices = new Vector<BluetoothDevice>();
	}	
	// add paired device to list
	private void addPairedDevices(){
		BluetoothDevice pairedDevice;
		
		database.openTransaction();
		Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, 
				Bluetooth.XML_IS_PAIRED + "=?" + " and " +
				Bluetooth.XML_IS_SELECTED + "=?", 
				new String [] {"1", "1"}, 
				null, null, null, null);
		database.closeTransaction();
		
		Iterator<BluetoothDevice> iter = (bluetoothAdapter.getBondedDevices()).iterator();
		while(iter.hasNext()){
			pairedDevice = iter.next();

			remoteDevices.add(pairedDevice);
			
			boolean isSelected = false;
			if(bluetooth != null){
				if(pairedDevice.getAddress().equalsIgnoreCase(bluetooth.getAddress())){
					isSelected = true;
				}
			}
			
			add(new Bluetooth(pairedDevice.getName(), pairedDevice.getAddress(), true, isSelected));
		}
	}
	
	// Bluetooth Disconnection method.
	private void btDisconn(final BluetoothDevice btDev){
		 if (mPrintService != null) mPrintService.start();
	}
	
//	// Bluetooth Connection Task.
//	class connTask extends AsyncTask<BluetoothDevice, Void, Integer>{
//		String name;
//		String address;
//		boolean isPaired;
//		boolean isSelected;
//		
//		private final ProgressDialog dialog = new ProgressDialog(SettingsWoosimActivity.this);
//		
//		@Override
//		protected void onPreExecute(){
//			dialog.setTitle("Bluetooth");
//			dialog.setMessage("Connecting...");
//			dialog.show();
//			super.onPreExecute();
//		}
//		
//		@Override
//		protected Integer doInBackground(BluetoothDevice... params){
//			Integer retVal = null;
//			
//			try{
//				
//				name = params[0].getName();
//				address = params[0].getAddress();
//				
//				bluetoothPort.connect(params[0]);
//				lastConnAddr = params[0].getAddress();
//				retVal = new Integer(0);
//			}catch (IOException e){
//				Log.e(TAG, e.getMessage());
//				retVal = new Integer(-1);
//			}
//			return retVal;
//		}
//		
//		@Override
//		protected void onPostExecute(Integer result){
//			if(result.intValue() == 0){ // Connection success.
//				RequestHandler rh = new RequestHandler();				
//				hThread = new Thread(rh);
//				hThread.start();
//				// UI
//
//				if(dialog.isShowing())
//					dialog.dismiss();				
//				Toast.makeText(SettingsWoosimActivity.this, "Connected", Toast.LENGTH_SHORT).show();
//				
//				adapter.updateCheckbox(idSelected);
//				
//				try{
//					database.openTransaction();
//					database.deleteData(Bluetooth.TABLE_NAME, null, null);
//					database.setData(new Bluetooth(name, address, true, true));
//					database.commitTransaction();
//				}catch(SQLiteException e){
//					e.printStackTrace();
//					database.closeTransaction();
//				}finally{
//					database.closeTransaction();
//				}
//			}
//			else{
//				if(dialog.isShowing())
//					dialog.dismiss();		
//				
//				Toast.makeText(SettingsWoosimActivity.this, "Connection failed", Toast.LENGTH_SHORT).show();
//			}
//			super.onPostExecute(result);
//		}
//	}
	
	@Override
	protected void onDestroy(){
		saveSettingFile();
		
		unregisterReceiver(searchFinish);
		unregisterReceiver(searchStart);
		unregisterReceiver(discoveryResult);
		unregisterReceiver(broadcastReceiver);
		unRegisterBaseActivityReceiver();
		
		if (mPrintService != null) mPrintService.stop();
		
		super.onDestroy();
	}
	
	private void add(Bluetooth device){
		adapter.add(device);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		BluetoothDevice bluetoothDevice = remoteDevices.elementAt(position);
		Bluetooth bluetooth = (Bluetooth) adapter.getItem(position);
		
		if(bluetoothAdapter.isDiscovering()){
			bluetoothAdapter.cancelDiscovery();
		}
		
		if(bluetooth.isSelected()){
			adapter.updateCheckbox(-1);
			
			try{
				database.openTransaction();
				database.deleteData(Bluetooth.TABLE_NAME, null, null);
				database.commitTransaction();
			}catch(SQLiteException e){   
				e.printStackTrace();
				database.closeTransaction();
			}finally{
				database.closeTransaction();
			}
		}else{
			idSelected = position;
	        mPrintService.connect(bluetoothDevice, true);
		}
	}
	
    // The Handler that gets information back from the BluetoothPrintService
    private final MyHandler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<SettingsWoosimActivity> mActivity;

        public MyHandler(SettingsWoosimActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
        	SettingsWoosimActivity activity = mActivity.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }
    
    private void handleMessage(Message msg) {		
		try{
	        String deviceName = msg.getData().getString(Bluetooth.XML_NAME);
	        String deviceAddress = msg.getData().getString(Bluetooth.XML_ADDRESS);
			
			database.openTransaction();
			database.deleteData(Bluetooth.TABLE_NAME, null, null);
			database.setData(new Bluetooth(deviceName, deviceAddress, true, true));
			database.commitTransaction();
			
			if(!TextUtils.isEmpty(deviceName)){
				adapter.updateCheckbox(idSelected);
		        Toast.makeText(getApplicationContext(), "Connected to " + deviceName, Toast.LENGTH_SHORT).show();
			}else{
				adapter.updateCheckbox(-1);
			}
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
		}finally{
			database.closeTransaction();
		}
    }
	
	public boolean isBluetoothAvailable() {
	    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    return (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled());
	}
	
	private boolean isMyServiceRunning(Class<?> serviceClass) {
		Log.d("tag", serviceClass.getName());
		
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	    	Log.d("tag", service.service.getClassName());
	    	
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return true;   
	        }
	    }
	    return false;
	}
	
	private void getLanguage(){
		String countryCode = new SharedPreferencesHandler(SettingsWoosimActivity.this).getCountryCode();
		
		try{
			if(countryCode.equalsIgnoreCase("in")){
				rbnSettingsLanguageIn.setChecked(true);
			}else{
				rbnSettingsLanguageEn.setChecked(true);
			}
		}catch(NullPointerException e){
			rbnSettingsLanguageEn.setChecked(true);
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if(checkedId == R.id.rbnSettingsLanguageEn){
			new SharedPreferencesHandler(SettingsWoosimActivity.this).setCountryCode("us");
			new LanguageHandler(SettingsWoosimActivity.this).updateLanguage("us");
		}else if(checkedId == R.id.rbnSettingsLanguageIn){
			new SharedPreferencesHandler(SettingsWoosimActivity.this).setCountryCode("in");
			new LanguageHandler(SettingsWoosimActivity.this).updateLanguage("in");
		}
		
		initMenu();
	}
	
	public void copyAppDbToDownloadFolder() throws IOException {
	    try {
	    	FolderHandler folderHandler = new FolderHandler(SettingsWoosimActivity.this);
	    	
	    	if(folderHandler.isSDCardWritable() && folderHandler.init()){
		        File backupDB = new File(folderHandler.getFileDatabaseExport(), DatabaseHelper.dbName);
		        File currentDB = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);
		        
		        if (currentDB.exists()) {
		            FileInputStream fis = new FileInputStream(currentDB);
		            FileOutputStream fos = new FileOutputStream(backupDB);
		            fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
		            // or fis.getChannel().transferTo(0, fis.getChannel().size(), fos.getChannel());
		            fis.close();
		            fos.close();

		            MediaScannerConnection.scanFile(SettingsWoosimActivity.this, new String[] { backupDB.getAbsolutePath() }, null, null);
		            
		    		new DialogNotification(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
							getResources().getString(R.string.export_successed), false).show();
		            
		        } else{
		    		new DialogNotification(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
							getResources().getString(R.string.export_failed), false).show();
		        }
	    	}else{
	    		new DialogNotification(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.error_sd_card), false).show();
	    	} 
	    } catch (IOException e) {
    		new DialogNotification(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
					e.getMessage(), false).show();
	    }
	}
	
	private class CheckUpdateAsyncTask extends AsyncTask<Void, Void, String>{
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(SettingsWoosimActivity.this, getResources().getString(R.string.wait));
				dialogProgress.show();
			}
		}

		@Override
		protected String doInBackground(Void... params) {
			
			String json = new UpdateHandler().checkVersion();
			
			return json;
		}
		   
		@Override
		protected void onPostExecute(String json) {
			super.onPostExecute(json);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();  
				dialogProgress = null;
			}
			
			if(!TextUtils.isEmpty(json)){
				Log.d("tag", json);
				
				apps = new JsonHandler(json).getApps();
				
				if(apps != null){
					String curVersion = new DeviceHandler(SettingsWoosimActivity.this).getCodeVersion();
					
					if(!apps.getVersion().equals(curVersion)){
						new DialogConfirm(SettingsWoosimActivity.this, getResources().getString(R.string.informasi), 
								getResources().getString(R.string.apps_update), null, R.id.txtSettingsUpdateApp).show();
					}else{
						new DialogNotification(SettingsWoosimActivity.this, 
								getResources().getString(R.string.informasi), 
								getResources().getString(R.string.apps_update_not_availabe), 
								false).show();
					}
				}else{
					new DialogNotification(SettingsWoosimActivity.this, 
							getResources().getString(R.string.informasi), 
							getResources().getString(R.string.internet_connection_error), 
							false).show();
				}
				
			}else{
				new DialogNotification(SettingsWoosimActivity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.internet_connection_error), 
						false).show();
			}
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		switch (id) {
		case R.id.txtSettingsUpdateApp:
			if(new NetworkHandler(SettingsWoosimActivity.this).isNetworkConnected()){
				new DownloadAppsAsyncTask(SettingsWoosimActivity.this, Constanta.SERVER + apps.getFilename()).execute();
			} 			
			break;
		case R.id.btnSettingsDeviceAliasReset:
			DatabaseHandler database = new DatabaseHandler(SettingsWoosimActivity.this);
			boolean success = false;
			try{
				database.openTransaction();
				database.deleteData(DeviceAlias.TABLE_NAME, null, null);
				database.commitTransaction();
				txtSettingsDeviceAlias.setText("");
				success = true;
			}catch(SQLiteException e){
				e.printStackTrace();
			}finally{
				database.closeTransaction();
			}
			
			if(success){
				getDeviceAlias();
			}
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(checkAysncTask != null && checkAysncTask.getStatus() != AsyncTask.Status.FINISHED){
			checkAysncTask.cancel(true);
			checkAysncTask = null;
		}
	}
	
	private void getDeviceAlias(){
		DatabaseHandler database = new DatabaseHandler(SettingsWoosimActivity.this);
		String alias = "";
		
		database.openTransaction();
		DeviceAlias deviceAlias = (DeviceAlias) database.getDataFirst(false, DeviceAlias.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();
		
		if(deviceAlias == null){
			String imei = new DeviceHandler(SettingsWoosimActivity.this).getImei();
			
			database.openTransaction();
			SPBSRunningNumber spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null, 
					SPBSRunningNumber.XML_IMEI + "=?", 
					new String [] {imei}, 
					null, null, SPBSRunningNumber.XML_ID + " desc", null);
			
			database.closeTransaction();
			
			if(spbsNumberMax != null){
				alias = spbsNumberMax.getDeviceAlias();
			}
		}else{
			alias = deviceAlias.getDeviceAlias();
		}
		
		txtSettingsDeviceAlias.setText(alias);
	}
}
