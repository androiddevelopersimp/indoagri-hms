package com.simp.hms.activity.spbskranibluetooth;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.adapter.AdapterSPBSKrani;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.BluetoothHandler;
import com.simp.hms.handler.XmlHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BJR;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.ResponseSendSPBS;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.UserLogin;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static android.bluetooth.BluetoothAdapter.getDefaultAdapter;
import static android.content.ContentValues.TAG;

public class SPBSKraniActivity extends BaseActivity implements View.OnClickListener, DialogDateListener,
        AdapterSPBSKrani.OnClickedListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener, DialogNotificationListener {

    private static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //SerialPortService ID // MY_UUID is the app's UUID string, also used by the client code.
    private boolean CONTINUE_READ_WRITE = true;
    private boolean CONNECTION_ENSTABLISHED = false;
    private boolean IS_UNPAIRED_DEVICE = false;
    private boolean DEVICES_IN_LIST = true;
    private static int INTENT_ONE = 10;

    private Toolbar tbrMain;
    private Button btnSpbsHistoryKraniSearch;
    private CheckBox cbxSettingsBluetoothKrani;
    private LinearLayout linearLayoutKrani;
    private LinearLayout linearBluetoothDevices;
    private TextView lblBluetooth;
    private TextView txtSearchDateSpbsKrani;
    private ListView listBluetoothDevices;
    private ListView listDetailSpbs;
    private DialogProgress dialogProgress;
    private DialogProgress dialogProgressPaired;

    private List<SPBSHeader> listSpbsHeader;
    private String xml = "";
    private String spbsDate = "";
    List<String> listItemUnpairDevice;


    //adapter
    private AdapterSPBSKrani adapter;
    private ArrayAdapter<String> listAdapterUnpairDevice;

    //send bluetooth
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket socket;
    private InputStream is;
    private OutputStream os;
    private BluetoothDevice remoteDevice;
    private Set<BluetoothDevice> pairedDevices;
    private ArrayList<BluetoothDevice> arrayListBluetoothDevices = null;
    private BluetoothHandler bluetoothHandler;

    private GetDataAsyncTask getDataAsync;


    DatabaseHandler database = new DatabaseHandler(SPBSKraniActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spbs_krani);

        animOnStart();

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        listDetailSpbs = (ListView) findViewById(R.id.listStatusSpbsKrani);
        linearLayoutKrani = (LinearLayout) findViewById(R.id.linearKrani);
        lblBluetooth = (TextView) findViewById(R.id.lblBluetoothKrani);
        txtSearchDateSpbsKrani = (TextView) findViewById(R.id.txtDateKrani);
        btnSpbsHistoryKraniSearch = (Button) findViewById(R.id.btnSpbsHistoryKraniSearch);
        cbxSettingsBluetoothKrani = (CheckBox) findViewById(R.id.cbxSettingsBluetoothKrani);
        linearBluetoothDevices = (LinearLayout) findViewById(R.id.lBluetoothDevices);
        listBluetoothDevices = (ListView) findViewById(R.id.listBluetoothDevices);

        txtActionBarTitle.setText("SPBS Krani");
        btnActionBarRight.setVisibility(View.INVISIBLE);

        listItemUnpairDevice = new ArrayList<>();
        listAdapterUnpairDevice = new ArrayAdapter<>(SPBSKraniActivity.this, android.R.layout.simple_list_item_1, listItemUnpairDevice);
        listBluetoothDevices.setAdapter(listAdapterUnpairDevice);

        listSpbsHeader = new ArrayList<>();
        adapter = new AdapterSPBSKrani(SPBSKraniActivity.this, listSpbsHeader, R.layout.item_spbs_krani);
        listDetailSpbs.setAdapter(adapter);

        spbsDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        txtSearchDateSpbsKrani.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

        bluetoothHandler = new BluetoothHandler();
        arrayListBluetoothDevices = new ArrayList<>();

        adapter.setOnItemClickListener(this);
        txtSearchDateSpbsKrani.setOnClickListener(this);
        btnSpbsHistoryKraniSearch.setOnClickListener(this);
        cbxSettingsBluetoothKrani.setOnCheckedChangeListener(this);
        listBluetoothDevices.setOnItemClickListener(this);

        isBluetoothAvailable();
        registerBroadcast();
    }

    private void registerBroadcast() {
        // Register for broadcasts when a device is discovered.
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
    }

    public void isBluetoothAvailable() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            cbxSettingsBluetoothKrani.setChecked(true);
        } else {
            cbxSettingsBluetoothKrani.setChecked(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtDateKrani:
// kamal 30-10-2019 : permintaan untuk validasi spbs harus di kirim pada hari yang sama
//                new DialogDate(SPBSKraniActivity.this, getResources().getString(R.string.tanggal),
//                        new Date(), R.id.txtDateKrani).show();
                break;
            case R.id.btnSpbsHistoryKraniSearch:
                getDataAsync = new GetDataAsyncTask();
                getDataAsync.execute();
                break;
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        spbsDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
        txtSearchDateSpbsKrani.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    @Override
    public void onItemClicked(int posisition) {
        final XmlHandler xmlHandler = new XmlHandler(getApplicationContext());
        SPBSHeader item = (SPBSHeader) adapter.getItem(posisition);
        if (item != null) {
            final String data = xmlHandler.createSPBSHeader(item);
            int maxStream = 1000;
            Handler handler1 = new Handler();
            for (int i = 0; i <= data.length() / maxStream; i++) {
                final int start = i * maxStream;
                int end = (i + 1) * maxStream;
                end = end > data.length() ? data.length() : end;
                sendBtnClick(data.substring(start, end));//yang akan di kirim
            }

        }
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        switch (compoundButton.getId()) {
            case R.id.cbxSettingsBluetoothKrani:
                if (isChecked) {
                    if (!bluetoothAdapter.isEnabled()) {
                        Log.i("CEK LOG", "NOT ENABLE");
                        Intent bluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(bluetoothIntent, INTENT_ONE);
                    } else {
                        Log.i("CEK LOG", "ENABLE");
                        arrayListBluetoothDevices.clear();
                        listItemUnpairDevice.clear();
                        listAdapterUnpairDevice.notifyDataSetChanged();
                        linearBluetoothDevices.setVisibility(View.VISIBLE);
                        if (bluetoothAdapter.isDiscovering()) {
                            bluetoothAdapter.cancelDiscovery();
                        }
                        bluetoothAdapter.startDiscovery();
                    }
                } else {
                    if (bluetoothAdapter.isEnabled()) {
                        bluetoothAdapter.disable();
                    }
                    linearBluetoothDevices.setVisibility(View.GONE);
                    closeBT();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                Log.i("CEK LOG", "RESULT" + resultCode);
                if (resultCode == RESULT_OK) {
                    Log.i("CEK LOG", "ENABLE");
                    arrayListBluetoothDevices.clear();
                    listItemUnpairDevice.clear();
                    listAdapterUnpairDevice.notifyDataSetChanged();
                    linearBluetoothDevices.setVisibility(View.VISIBLE);
                    if (bluetoothAdapter.isDiscovering()) {
                        bluetoothAdapter.cancelDiscovery();
                    }
                    bluetoothAdapter.startDiscovery();
                } else if (resultCode == RESULT_CANCELED) {
                    Log.i("CEK LOG", "RESULT CANCELED");
                    cbxSettingsBluetoothKrani.setChecked(false);
                }
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
        final String name = (String) adapterView.getItemAtPosition(i);
        new DialogConfirm(SPBSKraniActivity.this, "Koneksi Bluetooth",
                "Äpakah yakin ingin terhubung ke [" + name + "] ?", null, new DialogConfirmListener() {
            @Override
            public void onConfirmOK(Object object, int id) {
                final BluetoothDevice bdDevice = arrayListBluetoothDevices.get(i);
                Log.i("Connect Bluetooth", "PAIRED TO " + bdDevice.getName());
                if (bluetoothHandler.unpairDevice(bdDevice)) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            bluetoothHandler.pairDevice(bdDevice);
                        }
                    }, 500);

                } else {
                    bluetoothHandler.pairDevice(bdDevice);
                }
            }
        }).show();
    }

    @Override
    public void onOK(boolean is_finish) {

    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSHeader>, List<SPBSHeader>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialogProgress = new DialogProgress(SPBSKraniActivity.this, getResources().getString(R.string.loading));
            dialogProgress.show();
        }

        @Override
        protected List<SPBSHeader> doInBackground(Void... voids) {
            listSpbsHeader.clear();
            List<SPBSHeader> listTemp = new ArrayList<SPBSHeader>();
            List<Object> listObject;

            String companyCode = "";
            String estate = "";
            String division = "";
            String gang = "";
            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if (userLogin != null) {
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                division = userLogin.getDivision();
                gang = userLogin.getGang();
                nik = userLogin.getNik();
            }

            database.openTransaction();
            listObject = database.getListData(false, SPBSHeader.TABLE_NAME, null,
                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                            SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
                            SPBSHeader.XML_SPBS_DATE + "=?",
                    new String[]{companyCode, estate, nik, spbsDate},
                    null, null, SPBSHeader.XML_CREATED_DATE + " desc", null);
            for (Object object : listObject) {
                SPBSHeader spbsHeader = (SPBSHeader) object;
                List<SPBSLine> spbsLines = new ArrayList<>();
                List<Object> objectListSPBSDetail = database.getListData(false, SPBSLine.TABLE_NAME, null,
                        SPBSLine.XML_SPBS_NUMBER + "=?", new String[]{spbsHeader.getSpbsNumber()}, null, null, SPBSLine.XML_ID, null);
                for (Object objectSPBSDetail : objectListSPBSDetail) {
                    SPBSLine spbsLine = (SPBSLine) objectSPBSDetail;
                    BJR bjr = (BJR) database.getDataFirst(false, BJR.TABLE_NAME, null,
                            BJR.XML_ESTATE + "=? and " +
                                    BJR.XML_BLOCK + "=? ",
                            new String[]{spbsLine.getEstate(), spbsLine.getBlock()},
                            null, null, null, null);
                    if (bjr != null) {
                        spbsLine.setBjr(bjr.getBjr());
                    }

                    spbsLines.add(spbsLine);
                }
                spbsHeader.setSpbsLines(spbsLines);

                listSpbsHeader.add(spbsHeader);
            }
            database.closeTransaction();

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<SPBSHeader> lstTemp) {
            super.onPostExecute(lstTemp);

            if (dialogProgress != null) {
                dialogProgress.dismiss();
                dialogProgress = null;
            }
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        closeBT();
        animOnFinish();
    }

    public void closeBT() //for closing opened communications, cleaning used resources
    {
        /*if(adapter == null)
            return;*/

        CONTINUE_READ_WRITE = false;
        CONNECTION_ENSTABLISHED = false;

        if (remoteDevice != null) {
            bluetoothHandler.unpairDevice(remoteDevice);
        }

        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }

        if (is != null) {
            try {
                is.close();
            } catch (Exception e) {
            }
            is = null;
        }

        if (os != null) {
            try {
                os.close();
            } catch (Exception e) {
            }
            os = null;
        }

        if (socket != null) {
            try {
                socket.close();
            } catch (Exception e) {
            }
            socket = null;
        }

        try {
            Handler mHandler = new Handler();
            mHandler.removeCallbacksAndMessages(writter);
            mHandler.removeCallbacksAndMessages(clientConnecter);
            Log.i(TAG, "Threads ended...");
        } catch (Exception e) {
            Log.e(TAG, "Attemp for closing threads was unsucessfull.");
        }

    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                if (deviceName != null) {
                    if (arrayListBluetoothDevices.size() < 1) // this checks if the size of bluetooth device is 0,then add the
                    {
                        arrayListBluetoothDevices.add(device);
                        listItemUnpairDevice.add(device.getName());
                        listAdapterUnpairDevice.notifyDataSetChanged();
                    } else {
                        boolean flag = true;    // flag to indicate that particular device is already in the arlist or not
                        for (int i = 0; i < arrayListBluetoothDevices.size(); i++) {
                            if (device.getAddress().equals(arrayListBluetoothDevices.get(i).getAddress())) {
                                flag = false;
                            }
                        }
                        if (flag == true) {
                            arrayListBluetoothDevices.add(device);
                            listItemUnpairDevice.add(device.getName());
                            listAdapterUnpairDevice.notifyDataSetChanged();
                        }
                    }
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.i("CEK LOG", "discovery Finished ");
            } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Log.i("CEK LOG", " " + device.getBondState());
                if (device.getBondState() == BluetoothDevice.BOND_BONDING) {
                    dialogProgressPaired = new DialogProgress(SPBSKraniActivity.this, "Mencoba terhubung ke " + device.getName());
                    dialogProgressPaired.setCanceledOnTouchOutside(false);
                    dialogProgressPaired.show();
                } else if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.v("CEK LOG", "PAIRING SUCCESS ");
////                    listItemUnpairDevice.clear();
////                    listAdapterUnpairDevice.notifyDataSetChanged();
////                    listDevice();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 3s = 3000ms
                            pairedDevices = bluetoothAdapter.getBondedDevices(); //list of devices
                            selectBTdevice(device.getName());
                            connectBT();
                            if (dialogProgressPaired != null && dialogProgressPaired.isShowing()) {
                                dialogProgressPaired.dismiss();
                            }
                            linearBluetoothDevices.setVisibility(View.GONE);
                        }
                    }, 3000);
                } else {
                    if (dialogProgressPaired != null && dialogProgressPaired.isShowing()) {
                        dialogProgressPaired.dismiss();
                    }
                }
            }
        }
    };

    public void selectBTdevice(String name) //for selecting device from list which is used in procedures
    {
        if (pairedDevices.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Selecting was unsucessful, no devices in list.", Toast.LENGTH_SHORT).show();
        }

        for (BluetoothDevice bt : pairedDevices) //foreach
        {
            if (name.equals(bt.getName())) {
                remoteDevice = bt;
            }
        }
    }

    public void connectBT() {
        if (bluetoothAdapter == null) {
            bluetoothAdapter = getDefaultAdapter();
            Log.i(TAG, "Backup way of getting adapter was used!");
        }

        CONTINUE_READ_WRITE = true; //writer tiebreaker
        socket = null; //resetting if was used previously
        is = null; //resetting if was used previously
        os = null; //resetting if was used previously

        if (pairedDevices.isEmpty() || remoteDevice == null) {
            Toast.makeText(this, "Paired device is not selected, choose one", Toast.LENGTH_SHORT).show();
            return;
        } else {
            lblBluetooth.setText("Bluetooth (Menunggu terhubung ke " + remoteDevice.getName() + " ...)");
            new Thread(clientConnecter).start();
        }
    }

    public void sendBtnClick(String message) //sends text from text button
    {
        if (CONNECTION_ENSTABLISHED == false) {
            Toast.makeText(getApplicationContext(), "Belum Tehubung ke Driver.", Toast.LENGTH_SHORT).show(); //usually problem server-client decision
        } else {
            String textToSend = message;
            byte[] b = textToSend.getBytes();
            try {
                os.write(b);
            } catch (IOException e) {
                Log.e(TAG, "sendBtnClick: " + e.getMessage());
            }
        }
    }

    private Runnable clientConnecter = new Runnable() {
        @Override
        public void run() {
            try {
                socket = remoteDevice.createRfcommSocketToServiceRecord(MY_UUID);
                socket.connect();
                CONNECTION_ENSTABLISHED = true; //protect from failing

                Log.i(TAG, "Client is connected...");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { //Show message on UIThread
                        lblBluetooth.setText("Bluetooth (Siap Kirim Data ke " + remoteDevice.getName() + " )");
                    }
                });

                os = socket.getOutputStream();
                is = socket.getInputStream();
                new Thread(writter).start();
                Log.i(TAG, "Preparation for reading was done");

                int bufferSize = 1024;
                int bytesRead = -1;
                byte[] buffer = new byte[bufferSize];

                while (CONTINUE_READ_WRITE) //Keep reading the messages while connection is open...
                {
                    final StringBuilder sb = new StringBuilder();
                    bytesRead = is.read(buffer);
                    if (bytesRead != -1) {
                        String result = "";
                        while ((bytesRead == bufferSize) && (buffer[bufferSize - 1] != 0)) {
                            result = result + new String(buffer, 0, bytesRead - 1);
                            bytesRead = is.read(buffer);
                        }
                        result = result + new String(buffer, 0, bytesRead);
                        sb.append(result);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() { //Show message on UIThread
                            if (sb.toString().endsWith("</RESPONSE_SPBS>")) {
                                xml += sb.toString();
                                android.util.Log.i("TrackingFlow", "XML RESPONSE: " + xml);
                                getDataXML(xml);
                                xml = "";
                            } else {
                                xml += sb.toString();
                            }
                        }
                    });
                }
            } catch (IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CONTINUE_READ_WRITE = false;
                        CONNECTION_ENSTABLISHED = false;
                        lblBluetooth.setText("Bluetooth (Koneksi Terputus)");
                    }
                });
                try {
                    if (socket != null) {
                        socket.close();
                    }
                } catch (Exception ex) {
                }

                Log.e(TAG, "Client not connected...");
                closeBT();
                e.printStackTrace();
            }
        }
    };

    private Runnable writter = new Runnable() {

        @Override
        public void run() {
            while (CONTINUE_READ_WRITE) //reads from open stream
            {
                try {
                    os.flush();
                    Thread.sleep(2000);
                } catch (Exception e) {
                    Log.e(TAG, "Writer failed in flushing output stream...");
                    CONTINUE_READ_WRITE = false;
                }
            }
        }
    };

    public void getDataXML(String data) {
        InputStream inputStream = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
        XmlHandler xmlHandler = new XmlHandler(SPBSKraniActivity.this);
        ResponseSendSPBS responseSPBS = xmlHandler.parseResponseSPBS(inputStream);

//        if (responseSPBS.getSpbsStatus().equals("9")) {
//            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
//                    "Data Gagal Terkirim", false).show();
//        } else if (responseSPBS.getSpbsStatus().equals("3")) {
//            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
//                    "Data Yang Sudah diVerifikasi Tidak Bisa di Edit, Silahkan Sesuaikan Data dengan Driver", false).show();
//        } else if (responseSPBS.getSpbsStatus().equals("10")) {
//            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
//                    "Driver tidak sesuai dengan data spbs", false).show();
//        } else {
//            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
//                    "Data Sudah Terkirim", false).show();
//        }

        if (responseSPBS.getSpbsStatus().equals("1")) {
            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
                    "Data Sudah Terkirim", false).show();
        } else if (responseSPBS.getSpbsStatus().equals("2")) {
            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
                    "Data Sudah Terupdate", false).show();
        } else if (responseSPBS.getSpbsStatus().equals("3")) {
            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
                    "Data Gagal Terkirim \n status data sudah diverifikasi, silahkan scan data wb dari driver", false).show();
        } else if (responseSPBS.getSpbsStatus().equals("4")) {
            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
                    "Data Gagal Terkirim", false).show();
        } else if (responseSPBS.getSpbsStatus().equals("5")) {
            new DialogNotification(SPBSKraniActivity.this, getResources().getString(R.string.informasi),
                    "Data Gagal Terkirim \n Driver tidak sesuai dengan data spbs", false).show();
        }

        database.openTransaction();
        database.updateRawData(SPBSHeader.TABLE_NAME,
                "UPDATE " + SPBSHeader.TABLE_NAME
                        + " SET " + SPBSHeader.XML_SEND_STATUS + " = " + SPBSHeader.XML_SEND_STATUS + " + 1 "
                        + " WHERE " + SPBSHeader.XML_SPBS_NUMBER + " = '" + responseSPBS.getSpbsNo() + "'");
        database.commitTransaction();
        database.closeTransaction();

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

}
