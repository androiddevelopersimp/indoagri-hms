package com.simp.hms.activity.spbs;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.adapter.AdapterSPBSKernet;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.Employee;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SPBSKernetActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogConfirmListener{
	private Toolbar tbrMain;
	private EditText edtSpbsKernetSearch;
	private ListView lsvSpbsKernet;
	private Button btnSpbsKernetAddKernet;
	
	private List<Employee> lstKernet = new ArrayList<Employee>();
	private AdapterSPBSKernet adapter;
	
	DatabaseHandler database = new DatabaseHandler(SPBSKernetActivity.this);
	
	String companyCode = "";
	String estate = "";
	String division = "";
	String gang = "";
	String nikKernets = "";
	String kernets = "";
	String crop = "";
	String activity = "";
	String spbsactivity = "";
	
	private final int MST_KERNET = 101;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_spbs_kernet);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		edtSpbsKernetSearch = (EditText) findViewById(R.id.edtSpbsKernetSearch);
		lsvSpbsKernet = (ListView) findViewById(R.id.lsvSpbsKernet);
		btnSpbsKernetAddKernet = (Button) findViewById(R.id.btnSpbsKernetAddKernet);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spbs_kernet));
		btnActionBarRight.setText(getResources().getString(R.string.save));
		btnActionBarRight.setVisibility(View.VISIBLE);
		btnActionBarRight.setOnClickListener(this);
		edtSpbsKernetSearch.addTextChangedListener(this);
		lsvSpbsKernet.setOnItemClickListener(this);
		btnSpbsKernetAddKernet.setOnClickListener(this);
		
		companyCode = getIntent().getExtras().getString(Employee.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(Employee.XML_ESTATE, "");
		division = getIntent().getExtras().getString(Employee.XML_DIVISION, "70");
		gang = getIntent().getExtras().getString(Employee.XML_GANG, "");
		nikKernets = getIntent().getExtras().getString(Employee.XML_NIK, "");
		kernets = getIntent().getExtras().getString(Employee.XML_NAME, "");
		crop = getIntent().getExtras().getString(BPNHeader.XML_CROP, "");
		activity = getIntent().getExtras().getString("Activity", "");
		spbsactivity = getIntent().getExtras().getString("SPBSActivity", "");
		
		adapter = new AdapterSPBSKernet(SPBSKernetActivity.this, lstKernet, R.layout.item_spbs_kernet);
		lsvSpbsKernet.setAdapter(adapter);
		  
		parsingKernet();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
    
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:  
			String nik = "";
			String name = "";
			
			if(lstKernet.size() > 0){
				for(int i = 0; i < lstKernet.size(); i++){
					Employee employee = (Employee) lstKernet.get(i);
					
					if(i == 0){
						nik = nik + employee.getNik();
						name = name + employee.getName();
					}else{
						nik = nik + ", " + employee.getNik();
						name = name + ", " + employee.getName();
					}
				}
			}
			if(crop.equalsIgnoreCase("01")) {
				setResult(RESULT_OK, new Intent(SPBSKernetActivity.this, SPBSActivity.class)
						.putExtra(Employee.XML_NIK, nik)
						.putExtra(Employee.XML_NAME, name));
				finish();
			}else if (crop.equalsIgnoreCase("04")) {
				setResult(RESULT_OK, new Intent(SPBSKernetActivity.this, SPUActivity.class)
						.putExtra(Employee.XML_NIK, nik)
						.putExtra(Employee.XML_NAME, name));
				finish();
			}
			break;
		case R.id.btnSpbsKernetAddKernet:
			ArrayList<String> listNikFilter = new ArrayList<String>();
			
			if(lstKernet.size() > 0){
				for(int i = 0; i < lstKernet.size(); i++){
					Employee employee = (Employee) lstKernet.get(i);
					
					listNikFilter.add(employee.getNik());
				}
			}
			
			startActivityForResult(new Intent(SPBSKernetActivity.this, MasterEmployeeActivity.class)
					.putExtra(Employee.XML_COMPANY_CODE, companyCode)
					.putExtra(Employee.XML_ESTATE, estate)
					.putExtra(Employee.XML_DIVISION, "70")
					.putExtra(Employee.XML_GANG, "")
					.putExtra(Employee.XML_NIK, listNikFilter)
					.putExtra(Constanta.SEARCH, true)
					.putExtra(Constanta.TYPE, EmployeeType.KERNET.getId())
					.putExtra(BPNHeader.XML_CROP, crop)
					.putExtra("Activity", activity)
					.putExtra("SPBSActivity", spbsactivity)
					,MST_KERNET);
			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private void parsingKernet(){
		
		if(!TextUtils.isEmpty(nikKernets)){
			String [] niks = nikKernets.split(",");
			String [] names = kernets.split(",");
			
			if(niks.length > 1){
				for(int i = 0; i < niks.length; i++){
					lstKernet.add(new Employee(0, "", "", 0, 0, niks[i].trim(), names[i].trim(), "", "", "", "", "", "", "", "", ""));
				}
			}else{
				lstKernet.add(new Employee(0, "", "", 0, 0, niks[0].trim(), names[0].trim(), "", "", "", "", "", "", "", "", ""));
			}
			
			adapter = new AdapterSPBSKernet(SPBSKernetActivity.this, lstKernet, R.layout.item_spbs_kernet);
			lsvSpbsKernet.setAdapter(adapter);
		}
	}
	
	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	
		if(resultCode == RESULT_OK){
			if(requestCode == MST_KERNET){
				String nikKernet = data.getExtras().getString(Employee.XML_NIK);
				String kernet = data.getExtras().getString(Employee.XML_NAME);
				
				if(lstKernet.size() > 0){			
					adapter.addData(new Employee(0, "", "", 0, 0, nikKernet, kernet, "", "", "", "", "", "", "", "", ""));
				}else{
					lstKernet.add(new Employee(0, "", "", 0, 0, nikKernet, kernet, "", "", "", "", "", "", "", "", ""));
					adapter = new AdapterSPBSKernet(SPBSKernetActivity.this, lstKernet, R.layout.item_spbs_kernet);
					lsvSpbsKernet.setAdapter(adapter);
				}
			}  
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		adapter.deleteData(id);
	}
	
	
}
