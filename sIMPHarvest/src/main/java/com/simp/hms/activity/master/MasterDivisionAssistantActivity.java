package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.adapter.AdapterDivisionAssistant;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterDivisionAssistantActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterAssistantDivision;
	private EditText edtMasterAssistantDivisionSearch;
	
	private List<DivisionAssistant> listAssistantDivision;
	private AdapterDivisionAssistant adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	boolean isSearch = false;
	
	String estate;
	String crop;
	String spbsactivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_division_assistant);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterAssistantDivision = (ListView) findViewById(R.id.lsvMasterAssistantDivision);
		edtMasterAssistantDivisionSearch = (EditText) findViewById(R.id.edtMasterAssistantDivisionSearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_assistant_division));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtMasterAssistantDivisionSearch.addTextChangedListener(this);
		lsvMasterAssistantDivision.setOnItemClickListener(this);
		
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
		estate = getIntent().getExtras().getString(DivisionAssistant.XML_ESTATE, "");
		crop = getIntent().getExtras().getString(BPNHeader.XML_CROP, "");
		spbsactivity = getIntent().getExtras().getString("SPBSActivity", "");
	}

	@Override
	protected void onStart() {
		super.onStart();   
		   
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		DivisionAssistant assistantDivision = (DivisionAssistant) adapter.getItem(pos);
		
		if(isSearch){
			String division = "";
			if(assistantDivision.getLifnr() != "")
				division = assistantDivision.getDivision() + "," + assistantDivision.getLifnr();
			else
				division = assistantDivision.getDivision();

			if(crop.equalsIgnoreCase("01")){
				if(spbsactivity.equalsIgnoreCase("SPBSActivity")) {
					setResult(RESULT_OK, new Intent(MasterDivisionAssistantActivity.this, SPBSActivity.class)
							.putExtra(DivisionAssistant.XML_DIVISION, division)
							.putExtra(DivisionAssistant.XML_ASSISTANT_NAME, assistantDivision.getAssistantName()));
					finish();
				}else if(spbsactivity.equalsIgnoreCase("SPBSDatecsActivity")){
					setResult(RESULT_OK, new Intent(MasterDivisionAssistantActivity.this, SPBSDatecsActivity.class)
							.putExtra(DivisionAssistant.XML_DIVISION, division)
							.putExtra(DivisionAssistant.XML_ASSISTANT_NAME, assistantDivision.getAssistantName()));
					finish();
				}
			}else if(crop.equalsIgnoreCase("04")) {
				setResult(RESULT_OK, new Intent(MasterDivisionAssistantActivity.this, SPUActivity.class)
						.putExtra(DivisionAssistant.XML_DIVISION, division)
						.putExtra(DivisionAssistant.XML_ASSISTANT_NAME, assistantDivision.getAssistantName()));
				finish();
			}
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<DivisionAssistant>, List<DivisionAssistant>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterDivisionAssistantActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<DivisionAssistant> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterDivisionAssistantActivity.this);
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, DivisionAssistant.TABLE_NAME, null, 
					DivisionAssistant.XML_ESTATE + "=?", 
					new String [] {estate}, 
					null, null, DivisionAssistant.XML_DIVISION, null);
			database.closeTransaction();
			
			List<DivisionAssistant> listTemp = new ArrayList<DivisionAssistant>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					DivisionAssistant assistantDivision = (DivisionAssistant) listObject.get(i);
					
					listTemp.add(assistantDivision);
				}
			}
			
			return listTemp;
		}

		@Override
		protected void onPostExecute(List<DivisionAssistant> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				listAssistantDivision = listTemp;
				adapter = new AdapterDivisionAssistant(MasterDivisionAssistantActivity.this, listAssistantDivision, R.layout.item_division_assistant);
			}else{
				adapter = null;
			}
			
			lsvMasterAssistantDivision.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {  
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
}
