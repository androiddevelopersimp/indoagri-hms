package com.simp.hms.activity.spbs;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.database.DatabaseHelper;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.FileEncryptionHandler;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.handler.MD5Handler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.Employee;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class DriverExternalActivity extends BaseActivity implements OnClickListener, DialogNotificationListener {
	private Toolbar tbrMain;
	private TextView txtDriverExternalNik;
	private EditText edtDriverExternalName;
	private Button btnDriverExternalSubmit;

	String crop = "";
	String spbsactivity = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_driver_external);
		
		registerBaseActivityReceiver();

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtDriverExternalNik = (TextView) findViewById(R.id.txtDriverExternalNik);
		edtDriverExternalName = (EditText) findViewById(R.id.edtDriverExternalName);
		btnDriverExternalSubmit = (Button) findViewById(R.id.btnDriverExternalSubmit);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spbs_driver_external));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnDriverExternalSubmit.setOnClickListener(this);
		
		Bundle bundle = getIntent().getExtras();
		edtDriverExternalName.setText(bundle != null ? bundle.getString(Employee.XML_NAME, "") : "");
		crop = getIntent().getExtras().getString(BPNHeader.XML_CROP, "");
		spbsactivity = getIntent().getExtras().getString("SPBSActivity", "");
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		case R.id.btnDriverExternalSubmit:
			String nik = txtDriverExternalNik.getText().toString().trim();
			String name = edtDriverExternalName.getText().toString().trim();
			
			if(!TextUtils.isEmpty(nik) && !TextUtils.isEmpty(name)){
				if(crop.equalsIgnoreCase("01")) {
					if(spbsactivity.equalsIgnoreCase("SPBSActivity")) {
						Intent intent = new Intent(DriverExternalActivity.this, SPBSActivity.class);
						intent.putExtra(Employee.XML_NIK, nik);
						intent.putExtra(Employee.XML_NAME, name);

						setResult(RESULT_OK, intent);
						finish();
					}else if(spbsactivity.equalsIgnoreCase("SPBSDatecsActivity")){
						Intent intent = new Intent(DriverExternalActivity.this, SPBSDatecsActivity.class);
						intent.putExtra(Employee.XML_NIK, nik);
						intent.putExtra(Employee.XML_NAME, name);

						setResult(RESULT_OK, intent);
						finish();
					}
				}else if(crop.equalsIgnoreCase("04")){
					Intent intent = new Intent(DriverExternalActivity.this, SPUActivity.class);
					intent.putExtra(Employee.XML_NIK, nik);
					intent.putExtra(Employee.XML_NAME, name);

					setResult(RESULT_OK, intent);
					finish();
				}
			}else{
				new DialogNotification(DriverExternalActivity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.Invalid_data), false).show();;
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onOK(boolean is_finish) {
	
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		unRegisterBaseActivityReceiver();
		animOnFinish();
	}

	
}
