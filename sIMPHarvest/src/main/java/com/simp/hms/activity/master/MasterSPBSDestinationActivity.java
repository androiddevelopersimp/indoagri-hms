package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.adapter.AdapterDivisionAssistant;
import com.simp.hms.adapter.AdapterSPBSDestination;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.model.SPBSDestination;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterSPBSDestinationActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterSpbsDestination;
	private EditText edtMasterSpbsDestinationSearch;
	
	private List<SPBSDestination> listSPBSDestination;
	private AdapterSPBSDestination adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	boolean isSearch = false;
	
	String estate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_spbs_destination);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterSpbsDestination = (ListView) findViewById(R.id.lsvMasterSpbsDestination);
		edtMasterSpbsDestinationSearch = (EditText) findViewById(R.id.edtMasterSpbsDestinationSearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_spbs_destination));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtMasterSpbsDestinationSearch.addTextChangedListener(this);
		lsvMasterSpbsDestination.setOnItemClickListener(this);
		
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
		estate = getIntent().getExtras().getString(DivisionAssistant.XML_ESTATE, "");
	}

	@Override
	protected void onStart() {
		super.onStart();   
		   
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		SPBSDestination spbsDestination = (SPBSDestination) adapter.getItem(pos);
		
		if(isSearch){
			setResult(RESULT_OK, new Intent(MasterSPBSDestinationActivity.this, SPBSActivity.class)
			.putExtra(SPBSDestination.XML_DEST_ID, spbsDestination.getDestId())
			.putExtra(SPBSDestination.XML_DEST_DESC, spbsDestination.getDestDesc())
			.putExtra(SPBSDestination.XML_DEST_TYPE, spbsDestination.getDestType()));
			finish();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSDestination>, List<SPBSDestination>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterSPBSDestinationActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<SPBSDestination> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterSPBSDestinationActivity.this);
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, SPBSDestination.TABLE_NAME, null, 
					SPBSDestination.XML_ESTATE + "=?", 
					new String [] {estate}, 
					null, null, SPBSDestination.XML_DEST_TYPE, null);
			database.closeTransaction();
			
			List<SPBSDestination> listTemp = new ArrayList<SPBSDestination>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					SPBSDestination spbsDestination = (SPBSDestination) listObject.get(i);
					
					listTemp.add(spbsDestination);
				}
			}
			
			return listTemp;
		}

		@Override
		protected void onPostExecute(List<SPBSDestination> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				listSPBSDestination = listTemp;
				adapter = new AdapterSPBSDestination(MasterSPBSDestinationActivity.this, listSPBSDestination, R.layout.item_spbs_destination);
			}else{
				adapter = null;
			}
			
			lsvMasterSpbsDestination.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {  
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
}
