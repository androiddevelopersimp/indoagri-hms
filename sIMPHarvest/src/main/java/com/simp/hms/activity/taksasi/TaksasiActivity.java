package com.simp.hms.activity.taksasi;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterBlockHdrcActivity;
import com.simp.hms.adapter.AdapterTaksasiSummary;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BJR;
import com.simp.hms.model.BLKPLT;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SKB;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.TaksasiLine;
import com.simp.hms.model.TaksasiSummary;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.routines.Utils;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class TaksasiActivity extends BaseActivity implements OnItemClickListener, OnClickListener, DialogNotificationListener, DialogConfirmListener {
    private Toolbar tbrMain;
    private TextView txtTaksasiDate;
    private TextView txtTaksasiForeman;
    private TextView txtTaksasiBlock;
    private TextView txtTaksasiProdTrees;
    private TextView txtTaksasiBjr;
    private ListView lsvTaksasiSummary;
    private TextView txtTaksasiTotQtyJanjang;
    private TextView txtTaksasiTotProsentase;
    private TextView txtTaksasiTotKgTaksasi;
    private Button btnTaksasiInsert;
    private Button btnTaksasiRow;

    private List<TaksasiSummary> listTaksasiSummary = new ArrayList<TaksasiSummary>();
    private AdapterTaksasiSummary adapter = null;

    GetDataAsyncTask getDataAsync;
    DialogProgress dialogProgress;
    GPSService gps;
    GpsHandler gpsHandler;

    DatabaseHandler database = new DatabaseHandler(TaksasiActivity.this);

    String companyCode = "";
    String estate = "";
    String division = "";
    String nikForeman = "";
    String foreman = "";

    String imei = "";
    String taksasiDate = "";
    String block = "";
    String crop = "01";
    double prodTrees = 0;
    double bjr = 0;
    String gpsKoordinat = "0.0:0.0";
    int isSave = 0;
    int status = 0;
    double latitude = 0;
    double longitude = 0;
    long todayDate = 0;
    String skbdate = "";

    final int MST_BLOCK = 101;
    final int MST_BARIS_SKB = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_taksasi);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        txtTaksasiDate = (TextView) findViewById(R.id.txtTaksasiDate);
        txtTaksasiForeman = (TextView) findViewById(R.id.txtTaksasiForeman);
        txtTaksasiBlock = (TextView) findViewById(R.id.txtTaksasiBlock);
        txtTaksasiProdTrees = (TextView) findViewById(R.id.txtTaksasiProdTrees);
        txtTaksasiBjr = (TextView) findViewById(R.id.txtTaksasiBjr);
        lsvTaksasiSummary = (ListView) findViewById(R.id.lsvTaksasiSummary);
        txtTaksasiTotQtyJanjang = (TextView) findViewById(R.id.txtTaksasiTotQtyJanjang);
        txtTaksasiTotProsentase = (TextView) findViewById(R.id.txtTaksasiTotProsentase);
        txtTaksasiTotKgTaksasi = (TextView) findViewById(R.id.txtTaksasiTotKgTaksasi);
        btnTaksasiInsert = (Button) findViewById(R.id.btnTaksasiInsert);
        btnTaksasiRow = (Button) findViewById(R.id.btnTaksasiRow);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.taksasi_taksasi));
        btnActionBarRight.setText(getResources().getString(R.string.save));
        btnActionBarRight.setOnClickListener(this);
        txtTaksasiBlock.setOnClickListener(this);
        lsvTaksasiSummary.setOnItemClickListener(this);
        btnTaksasiInsert.setOnClickListener(this);
        btnTaksasiRow.setOnClickListener(this);

//		database.openTransaction();
//		database.deleteData(TaksasiHeader.TABLE_NAME, null, null);
//		database.deleteData(TaksasiLine.TABLE_NAME, null, null);
//		database.commitTransaction();
//		database.closeTransaction();

        deleteUnSaved();

        TaksasiHeader taksasiHeader = null;

        gps = new GPSService(TaksasiActivity.this);
        gpsHandler = new GpsHandler(TaksasiActivity.this);

        GPSTriggerService.taksasiActivity = this;

        gpsHandler.startGPS();

        if (getIntent().getExtras() != null) {
            taksasiHeader = (TaksasiHeader) getIntent().getParcelableExtra(TaksasiHeader.TABLE_NAME);
        }

        if (taksasiHeader != null) {
            imei = taksasiHeader.getImei();
            companyCode = taksasiHeader.getCompanyCode();
            estate = taksasiHeader.getEstate();
            division = taksasiHeader.getDivision();
            taksasiDate = taksasiHeader.getTaksasiDate();
            block = taksasiHeader.getBlock();
            crop = taksasiHeader.getCrop();
            nikForeman = taksasiHeader.getNikForeman();
            foreman = taksasiHeader.getForeman();
            prodTrees = taksasiHeader.getProdTrees();
            bjr = taksasiHeader.getBjr();
            status = taksasiHeader.getStatus();
            isSave = 1;
        } else {
            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if (userLogin != null) {
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                division = userLogin.getDivision();
                nikForeman = userLogin.getNik();
                foreman = userLogin.getName();
            }

            imei = new DeviceHandler(TaksasiActivity.this).getImei();
            taksasiDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
            todayDate = new Date().getTime();

            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
            }

            gpsKoordinat = String.valueOf(latitude) + ":" + String.valueOf(longitude);
            isSave = 0;
            status = 0;
        }

        initView();

        adapter = new AdapterTaksasiSummary(TaksasiActivity.this, listTaksasiSummary, R.layout.item_taksasi_summary);
        lsvTaksasiSummary.setAdapter(adapter);

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

    private void initView() {
        txtTaksasiDate.setText(new DateLocal(taksasiDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtTaksasiForeman.setText(foreman);
        txtTaksasiBlock.setText(block);

        txtTaksasiProdTrees.setText(String.valueOf(prodTrees));
        txtTaksasiBjr.setText(String.valueOf(bjr));
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        ArrayList<String> listBarisSKB = new ArrayList<String>();

        for (int i = 0; i < adapter.getCount(); i++) {
            TaksasiSummary taksasiSummary = (TaksasiSummary) adapter.getItem(i);

            listBarisSKB.add(String.valueOf(taksasiSummary.getBarisSkb()));
        }

        TaksasiSummary taksasiSummary = (TaksasiSummary) adapter.getItem(pos);

        startActivityForResult(new Intent(TaksasiActivity.this, TaksasiSkbActivity.class)
                .putExtra(TaksasiHeader.XML_BARIS_SKBS, listBarisSKB)
                .putExtra(TaksasiLine.XML_BARIS_SKB, taksasiSummary.getBarisSkb())
                .putExtra(TaksasiHeader.XML_IMEI, imei)
                .putExtra(TaksasiHeader.XML_COMPANY_CODE, companyCode)
                .putExtra(TaksasiHeader.XML_ESTATE, estate)
                .putExtra(TaksasiHeader.XML_DIVISION, division)
                .putExtra(TaksasiHeader.XML_TAKSASI_DATE, taksasiDate)
                .putExtra(TaksasiHeader.XML_BLOCK, block)
                .putExtra(TaksasiHeader.XML_CROP, crop)
                .putExtra(TaksasiHeader.XML_FOREMAN, foreman)
                .putExtra(TaksasiHeader.XML_STATUS, status), MST_BARIS_SKB);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:
                if (status == 0) {
                    if (listTaksasiSummary.size() > 0 && !TextUtils.isEmpty(block)) {
                        database.openTransaction();
//					TaksasiHeader taksasiHeader = (TaksasiHeader) database.getDataFirst(false, TaksasiHeader.TABLE_NAME, null,
//							TaksasiHeader.XML_IMEI + "=?" + " and " +
//							TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
//							TaksasiHeader.XML_ESTATE + "=?" + " and " +
//							TaksasiHeader.XML_DIVISION + "=?" + " and " +  
//							TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
//							TaksasiHeader.XML_BLOCK + "=?" + " and " +
//							TaksasiHeader.XML_CROP + "=?", 
//							new String [] {imei, companyCode, estate, division, taksasiDate, block, crop}, 
//							null, null, null, null);
//					database.closeTransaction();

                        TaksasiHeader taksasiHeader = (TaksasiHeader) database.getDataFirst(false, TaksasiHeader.TABLE_NAME, null,
                                TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                        TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                        TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
                                        TaksasiHeader.XML_BLOCK + "=?" + " and " +
                                        TaksasiHeader.XML_CROP + "=?",
                                new String[]{companyCode, estate, division, taksasiDate, block, crop},
                                null, null, null, null);
                        database.closeTransaction();

                        if (taksasiHeader != null) {
                            new DialogConfirm(TaksasiActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                        } else {
                            try {
                                database.openTransaction();
                                database.setData(new TaksasiHeader(0, imei, companyCode, estate, division, taksasiDate, block, crop,
                                        nikForeman, foreman, prodTrees, bjr, gpsKoordinat, 1, 0, todayDate, foreman, todayDate, foreman));

                                for (int i = 0; i < listTaksasiSummary.size(); i++) {
                                    TaksasiSummary taksasiSummary = (TaksasiSummary) listTaksasiSummary.get(i);

//								List<Object> listObject = database.getListData(false, TaksasiLine.TABLE_NAME, null, 
//										TaksasiLine.XML_IMEI + "=?"  + " and " +
//										TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
//									    TaksasiLine.XML_ESTATE + "=?" + " and " +
//									    TaksasiLine.XML_DIVISION + "=?" + " and " +
//									    TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
//									    TaksasiLine.XML_BLOCK + "=?" + " and " +
//									    TaksasiLine.XML_CROP + "=?" + " and " +
//									    TaksasiLine.XML_BARIS_SKB + "=?",
//									    new String [] {imei, companyCode, estate, division, taksasiDate, block, crop, taksasiSummary.getBarisSkb()},
//									    null, null, null, null);

                                    List<Object> listObject = database.getListData(false, TaksasiLine.TABLE_NAME, null,
                                            TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                    TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                    TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                    TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                    TaksasiLine.XML_CROP + "=?" + " and " +
                                                    TaksasiLine.XML_BARIS_SKB + "=?",
                                            new String[]{companyCode, estate, division, taksasiDate, block, crop, String.valueOf(taksasiSummary.getBarisSkb())},
                                            null, null, null, null);

                                    if (listObject.size() > 0) {
                                        for (int j = 0; j < listObject.size(); j++) {
                                            TaksasiLine taksasiLine = (TaksasiLine) listObject.get(j);

                                            int barisSkb = taksasiLine.getBarisSkb();
                                            int barisBlock = taksasiLine.getBarisBlock();
                                            int lineSkb = taksasiLine.getLineSkb();

                                            taksasiLine.setIsSave(1);

//										database.updateData(taksasiLine, 
//												TaksasiLine.XML_IMEI + "=?"  + " and " +
//												TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
//											    TaksasiLine.XML_ESTATE + "=?" + " and " +
//											    TaksasiLine.XML_DIVISION + "=?" + " and " +
//											    TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
//											    TaksasiLine.XML_BLOCK + "=?" + " and " +
//											    TaksasiLine.XML_CROP + "=?" + " and " +
//											    TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
//											    TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
//											    TaksasiLine.XML_LINE_SKB + "=?",
//											    new String [] {imei, companyCode, estate, division, taksasiDate, 
//												block, crop, barisSkb, barisBlock, String.valueOf(lineSkb)});

                                            database.updateData(taksasiLine,
                                                    TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                            TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                            TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                            TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                            TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                            TaksasiLine.XML_CROP + "=?" + " and " +
                                                            TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                                            TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                                            TaksasiLine.XML_LINE_SKB + "=?",
                                                    new String[]{companyCode, estate, division, taksasiDate,
                                                            block, crop, String.valueOf(barisSkb), String.valueOf(barisBlock), String.valueOf(lineSkb)});
                                        }
                                    }
                                }

                                database.commitTransaction();
                                isSave = 1;

                                new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                                        getResources().getString(R.string.save_successed), false).show();
                            } catch (SQLiteException e) {
                                e.printStackTrace();
                                database.closeTransaction();
                                new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                                        e.getMessage(), false).show();
                            } finally {
                                database.closeTransaction();
                            }
                        }
                    } else {
                        new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.Invalid_data), false).show();
                    }
                } else {
                    new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.txtTaksasiBlock:
                if (status == 0) {
                    startActivityForResult(new Intent(TaksasiActivity.this, MasterBlockHdrcActivity.class)
                                    .putExtra(BlockHdrc.XML_COMPANY_CODE, companyCode)
                                    .putExtra(BlockHdrc.XML_ESTATE, estate)
                                    .putExtra(BlockHdrc.XML_DIVISION, division)
                                    .putExtra(Constanta.SEARCH, true),
                            MST_BLOCK);
                } else {
                    new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnTaksasiInsert:
                if (adapter != null) {
                    ArrayList<String> listBarisSKB = new ArrayList<String>();

                    for (int i = 0; i < adapter.getCount(); i++) {
                        TaksasiSummary taksasiSummary = (TaksasiSummary) adapter.getItem(i);

                        listBarisSKB.add(String.valueOf(taksasiSummary.getBarisSkb()));
                    }

                    if (listTaksasiSummary.size() > 0) {
                        TaksasiSummary taksasiSummary = (TaksasiSummary) listTaksasiSummary.get(0);

                        startActivityForResult(new Intent(TaksasiActivity.this, TaksasiSkbActivity.class)
                                .putExtra(TaksasiHeader.XML_BARIS_SKBS, listBarisSKB)
                                .putExtra(TaksasiLine.XML_BARIS_SKB, taksasiSummary.getBarisSkb())
                                .putExtra(TaksasiHeader.XML_IMEI, imei)
                                .putExtra(TaksasiHeader.XML_COMPANY_CODE, companyCode)
                                .putExtra(TaksasiHeader.XML_ESTATE, estate)
                                .putExtra(TaksasiHeader.XML_DIVISION, division)
                                .putExtra(TaksasiHeader.XML_TAKSASI_DATE, taksasiDate)
                                .putExtra(TaksasiHeader.XML_BLOCK, block)
                                .putExtra(TaksasiHeader.XML_CROP, crop)
                                .putExtra(TaksasiHeader.XML_FOREMAN, foreman)
                                .putExtra(TaksasiHeader.XML_STATUS, status), MST_BARIS_SKB);
                    }
                } else {
                    new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_empty), false).show();
                }
                break;
            case R.id.btnTaksasiRow:
                if (!TextUtils.isEmpty(block)) {
                    showAddSKBRow();
                }
                break;
            default:
                break;
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<TaksasiSummary>, List<TaksasiSummary>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!isFinishing()) {
                dialogProgress = new DialogProgress(TaksasiActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<TaksasiSummary> doInBackground(Void... voids) {
            List<TaksasiSummary> listTemp = new ArrayList<TaksasiSummary>();
            List<Object> listObject = new ArrayList<Object>();

//			database.openTransaction();
//			listObject =  database.getListData(false, TaksasiLine.TABLE_NAME, 
//					new String [] {TaksasiLine.XML_BARIS_SKB,  "SUM (" + TaksasiLine.XML_QTY_JANJANG + ") as " + TaksasiLine.XML_QTY_JANJANG}, 
//					TaksasiLine.XML_IMEI + "=?" + " and " +
//					TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
//					TaksasiLine.XML_ESTATE + "=?" + " and " +
//					TaksasiLine.XML_DIVISION + "=?" + " and " +
//					TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
//					TaksasiLine.XML_BLOCK + "=?" + " and " +
//					TaksasiLine.XML_CROP + "=?",  
//					new String [] {imei, companyCode, estate, division, taksasiDate, block, crop}, 
//					TaksasiLine.XML_BARIS_SKB, null, null, null);
//			database.closeTransaction();  

            database.openTransaction();
            listObject = database.getListData(false, TaksasiLine.TABLE_NAME,
                    new String[]{TaksasiLine.XML_BARIS_SKB, "SUM (" + TaksasiLine.XML_QTY_JANJANG + ") as " + TaksasiLine.XML_QTY_JANJANG},
                    TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                            TaksasiLine.XML_ESTATE + "=?" + " and " +
                            TaksasiLine.XML_DIVISION + "=?" + " and " +
                            TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                            TaksasiLine.XML_BLOCK + "=?" + " and " +
                            TaksasiLine.XML_CROP + "=?",
                    new String[]{companyCode, estate, division, taksasiDate, block, crop},
                    TaksasiLine.XML_BARIS_SKB, null, TaksasiLine.XML_BARIS_SKB, null);
            database.closeTransaction();

            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    TaksasiLine taksasiLine = (TaksasiLine) listObject.get(i);

                    int barisSkb = taksasiLine.getBarisSkb();
                    int qtyJanjang = taksasiLine.getQtyJanjang();
                    int qtyPokok = 0;
                    double kgTaksasi = 0;

                    database.openTransaction();
                    List<Object> listBarisBlock = database.getListData(true, SKB.TABLE_NAME,
                            new String[]{SKB.XML_BARIS_BLOCK, SKB.XML_JUMLAH_POKOK},
                            SKB.XML_COMPANY_CODE + "=?" + " and " +
                                    SKB.XML_ESTATE + "=?" + " and " +
                                    SKB.XML_BLOCK + "=?" + " and " +
                                    SKB.XML_BARIS_SKB + "=?" + " and " +
                                    SKB.XML_VALID_TO + "=?",
                            new String[]{companyCode, estate, block, String.valueOf(barisSkb), taksasiDate},
                            SKB.XML_BARIS_BLOCK + ", " + SKB.XML_JUMLAH_POKOK, null, SKB.XML_BARIS_BLOCK, null);
                    database.closeTransaction();

                    if (listBarisBlock.size() > 0) {
                        for (int x = 0; x < listBarisBlock.size(); x++) {
                            SKB skb = (SKB) listBarisBlock.get(x);

                            qtyPokok = qtyPokok + (int) skb.getJumlahPokok();
                        }
                    }


                    if (qtyJanjang > 0 && qtyPokok > 0) {
                        kgTaksasi = ((double) qtyJanjang / qtyPokok) * 100;
                    }

                    listTemp.add(new TaksasiSummary(taksasiLine.getBarisSkb(), qtyJanjang, qtyPokok, kgTaksasi));
                }
            }

            if (!TextUtils.isEmpty(block)) {
                database.openTransaction();
                listObject = database.getListData(true, SKB.TABLE_NAME,
                        new String[]{SKB.XML_BARIS_SKB},
                        SKB.XML_COMPANY_CODE + "=?" + " and " +
                                SKB.XML_ESTATE + "=?" + " and " +
                                SKB.XML_BLOCK + "=?" + " and " +
                                SKB.XML_VALID_TO + "=?",
                        new String[]{companyCode, estate, block, taksasiDate},
                        SKB.XML_BARIS_SKB, null, SKB.XML_BARIS_SKB, null);
                database.closeTransaction();

                if (listObject.size() > 0) {
                    for (int i = 0; i < listObject.size(); i++) {
                        SKB skb = (SKB) listObject.get(i);

                        boolean found = false;
                        for (int j = 0; j < listTemp.size(); j++) {
                            TaksasiSummary taksasiSummary = (TaksasiSummary) listTemp.get(j);

                            if (skb.getBarisSkb() == taksasiSummary.getBarisSkb()) {
                                found = true;
                                break;
                            }
                        }

                        if (!found) {
                            int qtyPokok = 0;

                            database.openTransaction();
                            List<Object> listBarisBlock = database.getListData(true, SKB.TABLE_NAME,
                                    new String[]{SKB.XML_BARIS_BLOCK, SKB.XML_JUMLAH_POKOK},
                                    SKB.XML_COMPANY_CODE + "=?" + " and " +
                                            SKB.XML_ESTATE + "=?" + " and " +
                                            SKB.XML_BLOCK + "=?" + " and " +
                                            SKB.XML_BARIS_SKB + "=?" + " and " +
                                            SKB.XML_VALID_TO + "=?",
                                    new String[]{companyCode, estate, block, String.valueOf(skb.getBarisSkb()), taksasiDate},
                                    SKB.XML_BARIS_BLOCK + ", " + SKB.XML_JUMLAH_POKOK, null, SKB.XML_BARIS_BLOCK, null);
                            database.closeTransaction();

                            if (listBarisBlock.size() > 0) {
                                for (int x = 0; x < listBarisBlock.size(); x++) {
                                    SKB skb2 = (SKB) listBarisBlock.get(x);

                                    qtyPokok = qtyPokok + (int) skb2.getJumlahPokok();
                                }
                            }

                            listTemp.add(new TaksasiSummary(skb.getBarisSkb(), 0, qtyPokok, 0));
                        }
                    }
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<TaksasiSummary> listTemp) {
            super.onPostExecute(listTemp);

            if (dialogProgress != null && dialogProgress.isShowing()) {
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            for (int i = 0; i < listTemp.size(); i++) {
                TaksasiSummary taksasiSummary = (TaksasiSummary) listTemp.get(i);

                adapter.addData(taksasiSummary);
            }


//			if(listTemp.size() > 0){
//				if(listTaksasiSummary.size() > 0){
//					for(int i = 0; i < listTaksasiSummary.size(); i++){
//						TaksasiSummary taksasiSummaryCurrent = (TaksasiSummary) listTaksasiSummary.get(i);
//						
//						boolean found = false;
//						for(int j = 0; j < listTemp.size(); j++){
//							TaksasiSummary taksasiSummaryNew = (TaksasiSummary) listTemp.get(j);
//							
//							if(taksasiSummaryCurrent.getBarisSkb() == taksasiSummaryNew.getBarisSkb()){
//								found = true;
//								break;
//							}
//						}
//						
//						if(!found){
//							listTemp.add(taksasiSummaryCurrent);
//						}
//					}
//				}
//				
//				listTaksasiSummary = listTemp;
//				
//				adapter = new AdapterTaksasiSummary(TaksasiActivity.this, listTaksasiSummary, R.layout.item_taksasi_summary);
//			}else{
//				adapter = null;
//			}

//			if(listSpbsSummary.size() == 0){
//				adapter = new AdapterTaksasiSummary(TaksasiActivity.this, listTemp, R.layout.item_taksasi_summary);
//				lsvTaksasiSummary.setAdapter(adapter);
//			}  

            double totJanjang = 0;
            double totPokok = 0;
//			double totProsentase = 0;

            for (int i = 0; i < adapter.getCount(); i++) {
                TaksasiSummary taksasiSummary = (TaksasiSummary) adapter.getItem(i);

                if (taksasiSummary.getQtyJanjang() > 0) {
                    totJanjang += taksasiSummary.getQtyJanjang();
                    totPokok += taksasiSummary.getQtyPokok();
//					totProsentase += taksasiSummary.getTaksasiKg();
                }
            }

            double totProsentase = totPokok > 0 ? (totJanjang / totPokok) * 100 : 0;
            double totJanjangTaksasi = (totProsentase * prodTrees) / 100;
            double totTonase = (totJanjangTaksasi * bjr) / 1000;

//			totKgTaksasi = totProsentase * prodTrees * bjr / 100;
//			
            txtTaksasiTotQtyJanjang.setText(String.valueOf((int) totJanjangTaksasi));
            txtTaksasiTotProsentase.setText(new DecimalFormat("##.##").format(Utils.round(totProsentase, 2)));
            txtTaksasiTotKgTaksasi.setText(new DecimalFormat("###,###,###,###.##").format(Utils.round(totTonase, 2)));
        }
    }

    @Override
    public void onBackPressed() {
//		super.onBackPressed();

        new DialogConfirm(TaksasiActivity.this, getResources().getString(R.string.informasi),
                getResources().getString(R.string.exit), null, 1).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED) {
            getDataAsync.cancel(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == MST_BLOCK) {
                block = data.getExtras().getString(BlockHdrc.XML_BLOCK);

                database.openTransaction();
                BLKPLT blkpltBlock = (BLKPLT) database.getDataFirst(false, BLKPLT.TABLE_NAME, null,
                        BLKPLT.XML_COMPANY_CODE + "=?" + " and " +
                                BLKPLT.XML_ESTATE + "=?" + " and " +
                                BLKPLT.XML_BLOCK + "=?",
                        new String[]{companyCode, estate, block},
                        null, null, null, null);
                database.closeTransaction();

                if (blkpltBlock != null) {
                    prodTrees = blkpltBlock.getProdTrees();
                }

                database.openTransaction();
                BJR bjrBlock = (BJR) database.getDataFirst(false, BJR.TABLE_NAME, null,
                        BJR.XML_COMPANY_CODE + "=?" + " and " +
                                BJR.XML_ESTATE + "=?" + " and " +
                                BJR.XML_BLOCK + "=?",
                        new String[]{companyCode, estate, block},
                        null, null, null, null);
                database.closeTransaction();

                if (bjrBlock != null) {
                    bjr = bjrBlock.getBjr();
                }

                txtTaksasiBlock.setText(block);
                txtTaksasiProdTrees.setText(String.valueOf(prodTrees));
                txtTaksasiBjr.setText(String.valueOf(bjr));

                getDataAsync = new GetDataAsyncTask();
                getDataAsync.execute();
            } else if (requestCode == MST_BARIS_SKB) {
                getDataAsync = new GetDataAsyncTask();
                getDataAsync.execute();
            }
        }
    }

    @Override
    public void onOK(boolean is_finish) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConfirmOK(Object object, int id) {
        switch (id) {
            case R.id.btnActionBarRight:
                try {
                    database.openTransaction();

                    database.updateData(new TaksasiHeader(0, imei, companyCode, estate, division, taksasiDate, block, crop,
                                    nikForeman, foreman, prodTrees, bjr,
                                    gpsKoordinat, 1, 0, todayDate, foreman, todayDate, foreman),
                            TaksasiHeader.XML_IMEI + "=?" + " and " +
                                    TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                    TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                    TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                    TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
                                    TaksasiHeader.XML_BLOCK + "=?" + " and " +
                                    TaksasiHeader.XML_CROP + "=?",
                            new String[]{imei, companyCode, estate, division, taksasiDate, block, crop});

                    for (int i = 0; i < listTaksasiSummary.size(); i++) {
                        TaksasiSummary taksasiSummary = (TaksasiSummary) listTaksasiSummary.get(i);

                        List<Object> listObject = database.getListData(false, TaksasiLine.TABLE_NAME, null,
                                TaksasiLine.XML_IMEI + "=?" + " and " +
                                        TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                        TaksasiLine.XML_ESTATE + "=?" + " and " +
                                        TaksasiLine.XML_DIVISION + "=?" + " and " +
                                        TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                        TaksasiLine.XML_BLOCK + "=?" + " and " +
                                        TaksasiLine.XML_CROP + "=?" + " and " +
                                        TaksasiLine.XML_BARIS_SKB + "=?",
                                new String[]{imei, companyCode, estate, division, taksasiDate, block, crop, String.valueOf(taksasiSummary.getBarisSkb())},
                                null, null, null, null);

                        if (listObject.size() > 0) {
                            for (int j = 0; j < listObject.size(); j++) {
                                TaksasiLine taksasiLine = (TaksasiLine) listObject.get(j);

                                int barisSkb = taksasiLine.getBarisSkb();
                                int barisBlock = taksasiLine.getBarisBlock();
                                int lineSkb = taksasiLine.getLineSkb();

                                taksasiLine.setIsSave(1);
                                taksasiLine.setModifiedDate(todayDate);
                                taksasiLine.setModifiedBy(foreman);

                                database.updateData(taksasiLine,
                                        TaksasiLine.XML_IMEI + "=?" + " and " +
                                                TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_CROP + "=?" + " and " +
                                                TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                                TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_LINE_SKB + "=?",
                                        new String[]{imei, companyCode, estate, division, taksasiDate, block,
                                                crop, String.valueOf(barisSkb), String.valueOf(barisBlock), String.valueOf(lineSkb)});
                            }
                        }
                    }

                    database.commitTransaction();

                    new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_successed), false).show();
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();
                    new DialogNotification(TaksasiActivity.this, getResources().getString(R.string.informasi),
                            e.getMessage(), false).show();
                } finally {
                    database.closeTransaction();
                }
                break;
            default:
                if (getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED) {
                    getDataAsync.cancel(true);
                }

                if (dialogProgress != null && dialogProgress.isShowing()) {
                    dialogProgress.dismiss();
                    dialogProgress = null;
                }

                if (gps != null) {
                    gps.stopUsingGPS();
                }

                deleteUnSaved();
                gpsHandler.stopGPS();

                finish();
                animOnFinish();
                break;
        }
    }

    public void updateGpsKoordinat(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }
    }

    private void deleteUnSaved() {
        database.openTransaction();
        database.deleteData(TaksasiLine.TABLE_NAME,
                TaksasiLine.XML_IS_SAVE + "=?",
                new String[]{"0"});
        database.commitTransaction();
        database.closeTransaction();
    }

    public void showAddSKBRow() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View dialoglayout = getLayoutInflater().inflate(R.layout.dialog_taksasi_add_skb, null);
        dialogBuilder.setView(dialoglayout);

        final EditText edtTaksasiSkbRow = (EditText) dialoglayout.findViewById(R.id.edtTaksasiSkbRow);
        final EditText edtTaksasiPokok = (EditText) dialoglayout.findViewById(R.id.edtTaksasiPokok);
        final EditText edtTaksasiBarisBlok1 = (EditText) dialoglayout.findViewById(R.id.edtTaksasiBarisBlok1);
        final EditText edtTaksasiBarisBlok2 = (EditText) dialoglayout.findViewById(R.id.edtTaksasiBarisBlok2);
        Button btnNotificationSpbsDetailOk = (Button) dialoglayout.findViewById(R.id.btnNotificationSpbsDetailOk);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

        edtTaksasiSkbRow.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtTaksasiBarisBlok1.setHint(editable + "_");
                edtTaksasiBarisBlok2.setHint(editable + "_");
            }
        });

        btnNotificationSpbsDetailOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int skbRow = stringToInt(edtTaksasiSkbRow.getText().toString());
                int pokok = stringToInt(edtTaksasiPokok.getText().toString());
                int linesbk1 = stringToInt(edtTaksasiBarisBlok1.getText().toString());
                int linesbk2 = stringToInt(edtTaksasiBarisBlok2.getText().toString());

                if (Math.abs(linesbk1 - linesbk2) != 1) {
                    return;
                }

                if (skbRow > 0 && pokok > 0) {
                    skbRow(skbRow, pokok, linesbk1, linesbk2);
                    alertDialog.dismiss();
                } else {
                    alertDialog.dismiss();
                }
            }
        });


    }

    public void skbRow(int skbRow, int pokok, int lineskb1, int lineskb2) {
        int pokok1 = (int) Math.ceil(pokok / 2.0);
        int pokok2 = pokok - pokok1;

        List<SKB> skbList = new ArrayList<>();
        SKB skb1 = new SKB();
        skb1.setCompanyCode(companyCode);
        skb1.setEstate(estate);
        skb1.setBlock(block);
        skb1.setValidFrom(taksasiDate);
        skb1.setValidTo(taksasiDate);
        skb1.setBarisSkb(skbRow);
        skb1.setBarisBlok(lineskb1);
        skb1.setJumlahPokok(pokok1);
        skb1.setLineSkb(pokok1);
        skbList.add(skb1);

        SKB skb2 = new SKB();
        skb2.setCompanyCode(companyCode);
        skb2.setEstate(estate);
        skb2.setBlock(block);
        skb2.setValidFrom(taksasiDate);
        skb2.setValidTo(taksasiDate);
        skb2.setBarisSkb(skbRow);
        skb2.setBarisBlok(lineskb2);
        skb2.setJumlahPokok(pokok2);
        skb2.setLineSkb(pokok2);
        skbList.add(skb2);

        saveSKBRow(skbList);

    }

    public void saveSKBRow(List<SKB> list) {
        try {
            for (SKB skb : list) {
                database.openTransaction();
                database.setData(skb);
                database.commitTransaction();
                database.closeTransaction();
            }
        } catch (Exception e) {
            Log.e(ContentValues.TAG, "saveSKBRow: " + e.getMessage());
        } finally {
            getDataAsync = new GetDataAsyncTask();
            getDataAsync.execute();
        }
    }

    public int stringToInt(String number) {
        try {
            return Integer.valueOf(number);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

}


