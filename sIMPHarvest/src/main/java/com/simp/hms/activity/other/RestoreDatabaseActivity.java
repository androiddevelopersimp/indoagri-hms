package com.simp.hms.activity.other;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterAbsentType;
import com.simp.hms.adapter.AdapterRestoreDatabase;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.database.DatabaseHelper;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.FileEncryptionHandler;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.AbsentType;
import com.simp.hms.model.RestoreDatabase;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class RestoreDatabaseActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogConfirmListener, DialogNotificationListener{
	private Toolbar tbrMain;
	private ListView lsvRestoreDatabase;
	private EditText edtRestoreDatabaseSearch;
	
	private List<RestoreDatabase> lstRestoreDatabase;
	private AdapterRestoreDatabase adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	RestoreDatabase restoreDatabase;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_restore_database);
		
		registerBaseActivityReceiver();

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvRestoreDatabase = (ListView) findViewById(R.id.lsvRestoreDatabase);
		edtRestoreDatabaseSearch = (EditText) findViewById(R.id.edtRestoreDatabaseSearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.settings_restore_database));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtRestoreDatabaseSearch.addTextChangedListener(this);
		lsvRestoreDatabase.setOnItemClickListener(this);
	}

	@Override  
	protected void onStart() {
		super.onStart();   
		   
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		restoreDatabase =  (RestoreDatabase) adapter.getItem(pos);
		
		new DialogConfirm(RestoreDatabaseActivity.this, 
				getResources().getString(R.string.title), 
				getResources().getString(R.string.databsae_replaced), null, 0).show();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<RestoreDatabase>, List<RestoreDatabase>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(RestoreDatabaseActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<RestoreDatabase> doInBackground(Void... voids) {
			List<RestoreDatabase> lstTemp = new ArrayList<RestoreDatabase>();
			
			FolderHandler folderHandler = new FolderHandler(RestoreDatabaseActivity.this);
			
			if(folderHandler.init()){   
				
				File[] files = getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
				File folder = new File(files[files.length -1], folderHandler.DATABASE);
				
				if(folder.exists()){
					String [] lstFile = folder.list();
					
					for(int i = 0; i < lstFile.length; i++){
						Uri uri = Uri.parse(lstFile[i]);
						
						RestoreDatabase restoreDatabase = new RestoreDatabase(folder.getAbsolutePath() + "/" + lstFile[i], uri.getLastPathSegment());
						
						lstTemp.add(restoreDatabase);
					}
				}
			}
			
			return lstTemp;
		}

		@Override
		protected void onPostExecute(List<RestoreDatabase> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				lstRestoreDatabase = listTemp;
				adapter = new AdapterRestoreDatabase(RestoreDatabaseActivity.this, lstRestoreDatabase, R.layout.item_restore_database);
			}else{
				adapter = null;
			}
			
			lsvRestoreDatabase.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {  
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		unRegisterBaseActivityReceiver();
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		FolderHandler folderHandler = new FolderHandler(RestoreDatabaseActivity.this);
		  
		if(folderHandler.isSDCardWritable() && folderHandler.init()){
			try {
				if(new FileEncryptionHandler(RestoreDatabaseActivity.this).decrypt(restoreDatabase.getFilePath())){
					
					File database = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);
					File databaseImport = new File(folderHandler.getFileDatabaseImport(), restoreDatabase.getFileName());
					
					if(databaseImport.exists()){
			            String outFileName = database.getAbsolutePath(); 
			            OutputStream myOutput = new FileOutputStream(outFileName);
			            InputStream myInput = new FileInputStream(databaseImport);
		   
			            byte[] buffer = new byte[1024];  
			            int length;
			            while ((length = myInput.read(buffer)) > 0){
			                  myOutput.write(buffer, 0, length);    
			            }
			            
			            myInput.close();
			            myOutput.flush();
			            myOutput.close();
			            
			            databaseImport.delete();
			            
			            new DialogNotification(RestoreDatabaseActivity.this, 
			            		getResources().getString(R.string.title), 
			            		getResources().getString(R.string.restore_successed) + "\n" +
			            		getResources().getString(R.string.apps_restart), true).show();
					}
				}else{
		            new DialogNotification(RestoreDatabaseActivity.this, 
		            		getResources().getString(R.string.title), 
		            		getResources().getString(R.string.restore_failed), false).show();
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		if(is_finish){
			DatabaseHandler database = new DatabaseHandler(RestoreDatabaseActivity.this);
			
			try{
				database.openTransaction();
				database.deleteData(UserLogin.TABLE_NAME, null, null);
				database.commitTransaction();
			}catch(SQLiteException e){
				e.printStackTrace();
			}finally{
				database.closeTransaction();
			}
			
			closeAllActivities();
		}
	}
}
