package com.simp.hms.activity.spbs;

import android.app.ActionBar;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterBlockHdrcActivity;
import com.simp.hms.activity.master.MasterDivisionAssistantActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.master.MasterSPBSDestinationActivity;
import com.simp.hms.activity.master.MasterVehicleActivity;
import com.simp.hms.adapter.AdapterSPBSSummary;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.DeviceAlias;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.model.Employee;
import com.simp.hms.model.NomorFormat;
import com.simp.hms.model.Penalty;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.model.SPBSDestination;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSPrint;
import com.simp.hms.model.SPBSRunningNumber;
import com.simp.hms.model.SPBSSummary;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.service.BluetoothPrintService;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;
import com.woosim.printer.WoosimBarcode;
import com.woosim.printer.WoosimCmd;

import org.apache.http.util.ByteArrayBuffer;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SPBSUniversalActivity extends BaseActivity implements OnItemClickListener, OnClickListener, 
DialogNotificationListener, DialogConfirmListener, DialogDateListener{
	private Toolbar tbrMain;
	private TextView txtSpbsNumber;
	private TextView txtSpbsDate;
	private TextView txtSpbsDest;
	private TextView txtSpbsClerk; 
	private TextView txtSpbsDivision; 
	private TextView txtSpbsCrop;
	private TextView txtSpbsAssistant;  
	private TextView txtSpbsDriver;
	private Button btnSpbsDriverIn;
	private Button btnSpbsDriverEx;
	private TextView txtSpbsKernet;
	private TextView txtSpbsLicensePlate;  
	private Button btnSpbsLicensePlateIn;
	private Button btnSpbsLicensePlateEx;
	private Button btnSpbsLoadBpn;
	private ListView lsvSpbsSummary;
	private Button btnSpbsAddBlock;
	      
	private List<SPBSSummary> listSpbsSummary = new ArrayList<SPBSSummary>();
	private AdapterSPBSSummary adapter = null;
	
	GetDataAsyncTask getDataAsync;
	DialogProgress dialogProgress;
	GPSService gps;
	GpsHandler gpsHandler;
	
	DatabaseHandler database = new DatabaseHandler(SPBSUniversalActivity.this);
	
	BluetoothAdapter bluetoothAdapter = null;
	BluetoothDevice bluetoothDevice = null;
	BluetoothSocket bluetoothSocket = null;
	BluetoothPrintService mPrintService = null;
	  
	String companyCode = "";   
	String estate = "";
	String division = "";
	String divisionLogin = "";
	String gang = "";
	String nikClerk = "";
	String clerk = "";
	String alias = "";
	String imei = "";
	String imeiSerial = "000000";
	String year = "";
	String yearTwoDigits = "";
	String crop = "01";
	String spbsNumber = "";
	String spbsNumberTemp = "";
	String spbsDate = "";
	String nikDriver = "";
	String driver = "";
	String nikKernet = "";
	String kernet = "";
	String licensePlate = "";
	String runningAccount = "";
	String nikAssistant = "-";
	String assistant = "";   
	String month = "";
	String monthAlphabet = "";
	int id = 1;
	int lastNumber = 0;
	String gpsKoordinat = "0.0:0.0";
	int isSave = 0;
	int status = 0;  
	double latitude = 0;
	double longitude = 0;
	long todayDate = 0;
	String divisionRun = "";
	String destId = "";
	String destDesc = "";
	String destType = "";
	String lifnr = "";
	
	final int MST_ASS = 101;
	final int MST_BLOCK = 102;
	final int MST_TPH = 103;
	final int MST_DRIVER = 104;
	final int MST_RUN_ACC = 105;
	final int MST_KERNET = 106;
	final int INP_DRIVER_EX = 107;
	final int INP_LICENSE_EX = 108;
	final int MST_DEST = 109;
	
	final String QR_CODE_DELIMITER = ";";
	final String QR_CODE_HEADER_DELIMITER = "|";
	final String QR_CODE_LINE_DELIMITER = "*";
	
	boolean isNew = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_spbs);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtSpbsNumber = (TextView) findViewById(R.id.txtSpbsNumber);
		txtSpbsDate = (TextView) findViewById(R.id.txtSpbsDate);
		txtSpbsDest = (TextView) findViewById(R.id.txtSpbsDest);
		txtSpbsClerk = (TextView) findViewById(R.id.txtSpbsClerk);
		txtSpbsDivision = (TextView) findViewById(R.id.txtSpbsDivision);
		txtSpbsCrop = (TextView) findViewById(R.id.txtSpbsCrop);
		txtSpbsAssistant = (TextView) findViewById(R.id.txtSpbsAssistant);
		txtSpbsDriver = (TextView) findViewById(R.id.txtSpbsDriver);
		btnSpbsDriverIn = (Button) findViewById(R.id.btnSpbsDriverIn);
		btnSpbsDriverEx = (Button) findViewById(R.id.btnSpbsDriverEx);
		txtSpbsKernet = (TextView) findViewById(R.id.txtSpbsKernet);
		txtSpbsLicensePlate = (TextView) findViewById(R.id.txtSpbsLicensePlate);
		btnSpbsLicensePlateIn = (Button) findViewById(R.id.btnSpbsLicensePlateIn);
		btnSpbsLicensePlateEx = (Button) findViewById(R.id.btnSpbsLicensePlateEx);
		btnSpbsLoadBpn = (Button) findViewById(R.id.btnSpbsLoadBpn);
		lsvSpbsSummary = (ListView) findViewById(R.id.lsvSpbsSummary);
		btnSpbsAddBlock = (Button) findViewById(R.id.btnSpbsAddBlock);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);
		TextView btnActionBarPrint = (TextView) tbrMain.findViewById(R.id.btnActionBarPrint);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spbs));
		btnActionBarRight.setText(getResources().getString(R.string.save));
		btnActionBarRight.setOnClickListener(this);
		btnActionBarPrint.setOnClickListener(this);
		btnActionBarPrint.setVisibility(View.VISIBLE);
		txtSpbsDest.setOnClickListener(this);
		txtSpbsDivision.setOnClickListener(this);
		btnSpbsDriverIn.setOnClickListener(this);
		btnSpbsDriverEx.setOnClickListener(this);
		txtSpbsKernet.setOnClickListener(this);
		btnSpbsLicensePlateIn.setOnClickListener(this);
		btnSpbsLicensePlateEx.setOnClickListener(this);
		btnSpbsLoadBpn.setOnClickListener(this);
		lsvSpbsSummary.setOnItemClickListener(this);
		btnSpbsAddBlock.setOnClickListener(this);
		
//		database.openTransaction();
//		database.deleteData(DeviceAlias.TABLE_NAME, null, null);
//		database.deleteData(SPBSHeader.TABLE_NAME, null, null);
//		database.deleteData(SPBSLine.TABLE_NAME, null, null);
//		database.commitTransaction();
//		database.closeTransaction();
		
		deleteUnSaved();
		
		SPBSHeader spbsHeader = null;
		
		gps = new GPSService(SPBSUniversalActivity.this);
		gpsHandler = new GpsHandler(SPBSUniversalActivity.this);  
		
		GPSTriggerService.spbsUniversalActivity = this;
		 
		gpsHandler.startGPS();
		
		if(getIntent().getExtras() != null){
			spbsHeader = (SPBSHeader) getIntent().getParcelableExtra(SPBSHeader.TABLE_NAME);
		}
		
		database.openTransaction();
		UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();
		
		if(userLogin != null){
			companyCode = userLogin.getCompanyCode();
			estate = userLogin.getEstate();
			divisionLogin = userLogin.getDivision();
			gang = userLogin.getGang();
			nikClerk  = userLogin.getNik();
			clerk = userLogin.getName();
		}
		
		if(spbsHeader != null){
			imei = spbsHeader.getImei();
			year = spbsHeader.getYear();
			companyCode = spbsHeader.getCompanyCode();
			estate = spbsHeader.getEstate();
			division = spbsHeader.getDivision();
			spbsNumber = spbsHeader.getSpbsNumber();
			spbsDate = spbsHeader.getSpbsDate();
			crop = spbsHeader.getCrop();
			destId = spbsHeader.getDestId();
			destDesc = spbsHeader.getDestDesc();
			destType = spbsHeader.getDestType();
			
			database.openTransaction();
			spbsHeader = (SPBSHeader) database.getDataFirst(false, SPBSHeader.TABLE_NAME, null, 
					SPBSHeader.XML_YEAR + "=?" + " and " +
					SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +  
					SPBSHeader.XML_ESTATE + "=?" + " and " +
					SPBSHeader.XML_CROP + "=?" + " and " +
					SPBSHeader.XML_SPBS_NUMBER + "=?", 
					new String [] {year, companyCode, estate, crop, spbsNumber}, 
					null, null, null, null);
			database.closeTransaction();
			
			nikClerk  = spbsHeader.getNikClerk();
			clerk = spbsHeader.getClerk();
			nikAssistant = spbsHeader.getNikAssistant();
			nikDriver = spbsHeader.getNikDriver();
			driver = spbsHeader.getDriver();
			nikKernet = spbsHeader.getNikKernet();
			kernet = spbsHeader.getKernet();
			licensePlate = spbsHeader.getLicensePlate();
			runningAccount = spbsHeader.getRunningAccount();
			assistant = spbsHeader.getAssistant();
			status = spbsHeader.getStatus();
			isSave = 1;
			isNew = false;
			alias = spbsHeader.getSpbsNumber().substring(spbsHeader.getSpbsNumber().length() - 6, spbsHeader.getSpbsNumber().length() - 3);
			
			txtSpbsNumber.setText(spbsNumber);
		}else{
			imei = new DeviceHandler(SPBSUniversalActivity.this).getImei();
			year = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_YEAR_ONLY);
			spbsDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
			month = String.valueOf(new Converter(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_MONTH_ONLY)).StrToInt());
			monthAlphabet = new DateLocal(new Date()).getMonthAlphabet();
			todayDate = new Date().getTime();
			spbsNumber = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID);
			isNew = true;
			  
			SPBSRunningNumber spbsNumberMax = getAlias(estate, divisionLogin, year, month, imei);
			
			if(spbsNumberMax != null){
				alias = spbsNumberMax.getDeviceAlias();
				lastNumber = new Converter(spbsNumberMax.getRunningNumber()).StrToInt() + 1;
				id = spbsNumberMax.getId();
				divisionRun = spbsNumberMax.getDivision();
			}else{
				alias = "";
				lastNumber = 1;
			}
			
			if(gps.canGetLocation()){
				latitude = gps.getLatitude();
				longitude = gps.getLongitude();
			}
			
			gpsKoordinat = String.valueOf(latitude) + ":" + String.valueOf(longitude);
			isSave = 0;
		}
		
		adapter = new AdapterSPBSSummary(SPBSUniversalActivity.this, listSpbsSummary, R.layout.item_spbs_summary);
		lsvSpbsSummary.setAdapter(adapter);
		
		initView();
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	private void initView(){
		txtSpbsDate.setText(new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		txtSpbsDest.setText(destDesc);
		txtSpbsDivision.setText(destDesc);
		txtSpbsClerk.setText(clerk);
		txtSpbsDivision.setText(division);
		txtSpbsCrop.setText(crop);
		txtSpbsAssistant.setText(assistant);
		txtSpbsDriver.setText(driver);
		txtSpbsKernet.setText(kernet);
		txtSpbsLicensePlate.setText(licensePlate);

		GetDefaultSPBSDestination();
	}
	
	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		ArrayList<String> listBlock = new ArrayList<String>();
		
		for(int i = 0; i < adapter.getCount(); i++){
			SPBSSummary spbsSummary = (SPBSSummary) adapter.getItem(i);
			
			listBlock.add(spbsSummary.getBlock());
		}
		
		SPBSSummary spbsSummary = (SPBSSummary) adapter.getItem(pos);
		
		startActivityForResult(new Intent(SPBSUniversalActivity.this, SPBSBlockActivity.class)
		.putExtra(SPBSHeader.XML_BLOCKS, listBlock)
		.putExtra(SPBSLine.XML_BLOCK, spbsSummary.getBlock())
		.putExtra(SPBSHeader.XML_IMEI, imei)
		.putExtra(SPBSHeader.XML_YEAR, year)
		.putExtra(SPBSHeader.XML_COMPANY_CODE, companyCode)
		.putExtra(SPBSHeader.XML_ESTATE, estate)
		.putExtra(SPBSHeader.XML_DIVISION, division)
		.putExtra(SPBSHeader.XML_CROP, crop)
		.putExtra(SPBSHeader.XML_SPBS_NUMBER, spbsNumber)
		.putExtra(SPBSHeader.XML_SPBS_DATE, spbsDate)
		.putExtra(SPBSHeader.XML_CLERK, clerk)
		.putExtra(SPBSHeader.XML_STATUS, status), MST_TPH);
	}

	@Override
	public void onClick(View view) {
		ArrayList<String> listNikFilter = new ArrayList<String>();
		
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:
			if(status == 0){   
				if(listSpbsSummary.size() > 0 && !TextUtils.isEmpty(division) && 
						!TextUtils.isEmpty(nikDriver) && !TextUtils.isEmpty(licensePlate) && !TextUtils.isEmpty(alias)){					
					if(!isNew){
						new DialogConfirm(SPBSUniversalActivity.this, getResources().getString(R.string.informasi), 
								getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
					}else{
						try{
							String realSPBSNumber = new NomorFormat(companyCode, estate, division, year, monthAlphabet, String.valueOf(alias), String.valueOf(lastNumber)).getSPBSFormat();

							database.openTransaction();
							database.setData(new SPBSHeader(0, imei, year, companyCode, estate, crop, realSPBSNumber, spbsDate, destId, destDesc, destType, division, 
									nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
									gpsKoordinat, 1, 0, todayDate, clerk, 0, "", lifnr));
							
							for(int i = 0; i < listSpbsSummary.size(); i++){
								SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);
								
								List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null, 
										SPBSLine.XML_YEAR + "=?"+ " and " +
									    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
									    SPBSLine.XML_ESTATE + "=?" + " and " +
									    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
									    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
									    SPBSLine.XML_BLOCK + "=?" + " and " +
									    SPBSLine.XML_CROP + "=?",
									    new String [] {year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
									    null, null, null, null);
								
								if(listObject.size() > 0){
									for(int j = 0; j < listObject.size(); j++){
										SPBSLine spbsLine = (SPBSLine) listObject.get(j);
										
										spbsLine.setSpbsNumber(realSPBSNumber);
										spbsLine.setIsSave(1);

										database.updateData(spbsLine, 
												SPBSLine.XML_YEAR + "=?"+ " and " +
											    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
											    SPBSLine.XML_ESTATE + "=?" + " and " +
											    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
											    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
											    SPBSLine.XML_BLOCK + "=?" + " and " +
											    SPBSLine.XML_CROP + "=?" + " and " +
											    SPBSLine.XML_TPH + "=?" + " and " +
											    SPBSLine.XML_BPN_DATE + "=?" + " and " +
											    SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
											    SPBSLine.XML_ID + "=?",
											    new String [] {year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop, spbsLine.getTph(),
												spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});
										
										String bpnId = spbsLine.getBpnId();
										String spbsRef = spbsLine.getSpbsRef();
										
										if(!TextUtils.isEmpty(spbsRef)){
											String number = spbsRef.split("_")[0];
											String lineId = spbsRef.split("_")[1];
											
											List<Object> listObjectRef = database.getListData(false, SPBSLine.TABLE_NAME, null, 
													SPBSLine.XML_YEAR + "=?"+ " and " +
												    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
												    SPBSLine.XML_ESTATE + "=?" + " and " +
												    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
													SPBSLine.XML_ID + "= ?", 
													new String [] {year, companyCode, estate, number, lineId},
													null, null, null, null);
											
											if(listObjectRef != null && listObjectRef.size() >0){
												for(int a = 0; a < listObjectRef.size(); a++){
													SPBSLine spbsLineRef = (SPBSLine) listObjectRef.get(a);
													
													spbsLineRef.setSpbsNext(realSPBSNumber + "_" + spbsLine.getId());
													
													database.updateData(spbsLineRef, 
															SPBSLine.XML_YEAR + "=?"+ " and " +
														    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
														    SPBSLine.XML_ESTATE + "=?" + " and " +
															SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
															SPBSLine.XML_ID + " = ? " + " and " +
															SPBSLine.XML_ACHIEVEMENT_CODE + " = ?", 
															new String [] {year, companyCode, estate, spbsLineRef.getSpbsNumber(), String.valueOf(spbsLineRef.getId()), spbsLineRef.getAchievementCode()});
												}
											}
										}else{
											BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null, 
												BPNHeader.XML_BPN_ID + "=?", 
												new String [] {bpnId}, 
												null, null, null, null);
											
											if(bpnHeader != null){
												bpnHeader.setSpbsNumber(realSPBSNumber);
												
												database.updateData(bpnHeader, 
													BPNHeader.XML_BPN_ID + "=?", 
													new String [] {bpnId});
											}
										}
									}
								}
							}
							
//							if(destType.equalsIgnoreCase("MILL")){
								database.deleteData(SPBSRunningNumber.TABLE_NAME,
										SPBSRunningNumber.XML_ID + "=?" + " and " +
										SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
										SPBSRunningNumber.XML_YEAR + "=?" + " and " +
										SPBSRunningNumber.XML_MONTH + "=?" + " and " +
										SPBSRunningNumber.XML_IMEI + "=?" + " and " +
										SPBSRunningNumber.XML_DEVICE_ALIAS + "=?", 
										new String [] {String.valueOf(id), estate, year, month, imei, alias});
								
								database.setData(new SPBSRunningNumber(0, id, estate, divisionRun, year, month, imei, String.valueOf(lastNumber), alias));
//							}
							
							database.deleteData(DeviceAlias.TABLE_NAME, null, null);				
							database.setData(new DeviceAlias(alias));
							
							database.commitTransaction();
							
							txtSpbsNumber.setText(realSPBSNumber);
							spbsNumber = realSPBSNumber;
							isSave = 1;
							new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
									getResources().getString(R.string.save_successed), false).show();
						}catch(SQLiteException e){
							e.printStackTrace();
							database.closeTransaction();
							new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
									e.getMessage(), false).show();
						}finally{
							database.closeTransaction();
						}
					}
				}else{
					new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
							getResources().getString(R.string.Invalid_data), false).show();
				}
			}else{
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.data_already_export), false).show();
			}
			break;
		case R.id.btnActionBarPrint:
			if(isSave == 1){
				connectToPrinter();
			}else{
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.save_printing), false).show();
			}
			break;
		case R.id.txtSpbsDest:
			if(status == 0 && isSave == 0){
				startActivityForResult(new Intent(SPBSUniversalActivity.this, MasterSPBSDestinationActivity.class)
				.putExtra(SPBSDestination.XML_ESTATE, estate)
				.putExtra(Constanta.SEARCH, true), 
				MST_DEST);
			}
			
//			else{
//				new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi),
//						getResources().getString(R.string.data_already_export), false).show();
//			}
			break;
		case R.id.txtSpbsDivision:
			if(status == 0){
				startActivityForResult(new Intent(SPBSUniversalActivity.this, MasterDivisionAssistantActivity.class)
				.putExtra(DivisionAssistant.XML_ESTATE, estate)
				.putExtra(Constanta.SEARCH, true), 
				MST_ASS);
			}else{    
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.data_already_export), false).show();
			}
			break;   
		case R.id.btnSpbsDriverIn:
			startActivityForResult(new Intent(SPBSUniversalActivity.this, MasterEmployeeActivity.class)
			.putExtra(Employee.XML_COMPANY_CODE, companyCode)
			.putExtra(Employee.XML_ESTATE, estate)
			.putExtra(Employee.XML_DIVISION, "70")
			.putExtra(Employee.XML_GANG, "")
			.putExtra(Employee.XML_NIK, listNikFilter)
			.putExtra(Constanta.SEARCH, true)
			.putExtra(Constanta.TYPE, EmployeeType.DRIVER.getId()), 
			MST_DRIVER);
			break;
		case R.id.btnSpbsDriverEx:
			startActivityForResult(new Intent(SPBSUniversalActivity.this, DriverExternalActivity.class)
			.putExtra(Employee.XML_NAME, driver)
			, INP_DRIVER_EX);
			break;
		case R.id.txtSpbsKernet:
			startActivityForResult(new Intent(SPBSUniversalActivity.this, SPBSKernetActivity.class)
			.putExtra(Employee.XML_COMPANY_CODE, companyCode)
			.putExtra(Employee.XML_ESTATE, estate)
			.putExtra(Employee.XML_DIVISION, "70")
			.putExtra(Employee.XML_GANG, "")
			.putExtra(Employee.XML_NIK, nikKernet)
			.putExtra(Employee.XML_NAME,  kernet),
			MST_KERNET);
			break;
		case R.id.btnSpbsLicensePlateIn:
			startActivityForResult(new Intent(SPBSUniversalActivity.this, MasterVehicleActivity.class)
			.putExtra(RunningAccount.XML_COMPANY_CODE, companyCode)
			.putExtra(RunningAccount.XML_ESTATE, estate)
			.putExtra(Constanta.SEARCH, true),
			MST_RUN_ACC);
			break;
		case R.id.btnSpbsLicensePlateEx:
			startActivityForResult(new Intent(SPBSUniversalActivity.this, LicensePlateExternalActivity.class)
			.putExtra(RunningAccount.XML_LICENSE_PLATE, licensePlate),
			INP_LICENSE_EX);
			break;
		case R.id.btnSpbsLoadBpn:
			if(status == 0){
				if(!TextUtils.isEmpty(nikAssistant)){
					new DialogConfirm(SPBSUniversalActivity.this, getResources().getString(R.string.informasi), 
							getResources().getString(R.string.data_reseted), null, R.id.btnSpbsLoadBpn).show();
				}else{
					new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
							getResources().getString(R.string.select_assistant), false).show();
				}
			}else{
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.data_already_export), false).show();
			}
			break;
		case R.id.btnSpbsAddBlock:
			if(status == 0){
				if(!TextUtils.isEmpty(division)){
					startActivityForResult(new Intent(SPBSUniversalActivity.this, MasterBlockHdrcActivity.class)
					.putExtra(BlockHdrc.XML_COMPANY_CODE, companyCode)
					.putExtra(BlockHdrc.XML_ESTATE, estate)
					.putExtra(BlockHdrc.XML_DIVISION, division)
					.putExtra(Constanta.SEARCH, true),
					MST_BLOCK);
				}else{
					new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
							getResources().getString(R.string.select_assistant), false).show();
				}
			}else{
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.data_already_export), false).show();
			}
			break;
		default:
			break;
		}
	}

	private class GetDataAsyncTask extends AsyncTask<Void, List<SPBSSummary>, List<SPBSSummary>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(SPBSUniversalActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<SPBSSummary> doInBackground(Void... voids) {
			List<SPBSSummary> listTemp = new ArrayList<SPBSSummary>();
			  
			database.openTransaction();
			List<Object> listObject =  database.getListData(false, SPBSLine.TABLE_NAME, new String [] {SPBSLine.XML_BLOCK}, 
					SPBSLine.XML_YEAR + "=?" + " and " +
					SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
					SPBSLine.XML_ESTATE + "=?" + " and " +
					SPBSLine.XML_CROP + "=?" + " and " +
					SPBSLine.XML_SPBS_NUMBER + "=?",  
					new String [] {year, companyCode, estate, crop, spbsNumber}, 
					SPBSLine.XML_BLOCK, null, null, null);
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					SPBSLine spbsLine = (SPBSLine) listObject.get(i);
					
					List<Object> listQtyJanjang = database.getListData(false, SPBSLine.TABLE_NAME, null, 
							SPBSLine.XML_YEAR + "=?" + " and " +
							SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
							SPBSLine.XML_ESTATE + "=?" + " and " +
							SPBSLine.XML_CROP + "=?" + " and " +
							SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
							SPBSLine.XML_BLOCK + "=?" + " and " +
							SPBSLine.XML_ACHIEVEMENT_CODE + "=?", 
							new String [] {year, companyCode, estate, crop, spbsNumber, spbsLine.getBlock(), BPNQuantity.JANJANG_CODE},
							null, null, null, null);
					
					List<Object> listQtyLooseFruit = database.getListData(false, SPBSLine.TABLE_NAME, null, 
							SPBSLine.XML_YEAR + "=?" + " and " +
							SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
							SPBSLine.XML_ESTATE + "=?" + " and " +
							SPBSLine.XML_CROP + "=?" + " and " +
							SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
							SPBSLine.XML_BLOCK + "=?" + " and " +
							SPBSLine.XML_ACHIEVEMENT_CODE + "=?", 
							new String [] { year, companyCode, estate, crop, spbsNumber, spbsLine.getBlock(), BPNQuantity.LOOSE_FRUIT_CODE},
							null, null, null, null);
					
					double qtyJanjang = 0;
					double qtyLooseFruit = 0;
					
					if(listQtyJanjang.size() > 0){
						for(int a = 0; a < listQtyJanjang.size(); a++){
							SPBSLine spbsTemp = (SPBSLine) listQtyJanjang.get(a);
							
							qtyJanjang = qtyJanjang + spbsTemp.getQuantityAngkut();
						}
					}
					
					if(listQtyLooseFruit.size() > 0){
						for(int a = 0; a < listQtyLooseFruit.size(); a++){
							SPBSLine spbsTemp = (SPBSLine) listQtyLooseFruit.get(a);
							
							qtyLooseFruit = qtyLooseFruit + spbsTemp.getQuantityAngkut();
						}
					}
					
					listTemp.add(new SPBSSummary(spbsNumber, spbsDate, spbsLine.getBlock(), qtyJanjang, qtyLooseFruit) );
				}
			}
			database.closeTransaction();
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<SPBSSummary> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}

			for(SPBSSummary spbsSummary : listTemp){
				adapter.addOrUpdateData(spbsSummary);
			}
		}
	}

	@Override
	public void onBackPressed() {
		new DialogConfirm(SPBSUniversalActivity.this, getResources().getString(R.string.informasi), 
				getResources().getString(R.string.exit), null, 1).show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if (mPrintService != null) mPrintService.stop();
	}
  
	@Override  
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_OK){
			if(requestCode == MST_ASS){
				division = data.getExtras().getString(DivisionAssistant.XML_DIVISION);
				nikAssistant = "-";
				assistant = data.getExtras().getString(DivisionAssistant.XML_ASSISTANT_NAME, "");
				txtSpbsDivision.setText(division);
				txtSpbsAssistant.setText(assistant);
			}else if(requestCode == MST_BLOCK){
				String block = data.getExtras().getString(BlockHdrc.XML_BLOCK, "");
				
				if(!TextUtils.isEmpty(block)){
					boolean found = false;
					for(int i = 0; i < listSpbsSummary.size(); i++){
						SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);
						
						if(block.equalsIgnoreCase(spbsSummary.getBlock())){
							found = true;
							break;
						}
					}       
					    
					if(!found){
						if(adapter == null){
							listSpbsSummary = new ArrayList<SPBSSummary>();
							listSpbsSummary.add(new SPBSSummary(spbsNumber, spbsDate, block, 0, 0));
							adapter = new AdapterSPBSSummary(SPBSUniversalActivity.this, listSpbsSummary, R.layout.item_spbs_summary);
							lsvSpbsSummary.setAdapter(adapter);
						}else{
							adapter.addOrUpdateData(new SPBSSummary(spbsNumber, spbsDate, block, 0, 0));
						}
					}
				}
			}else if(requestCode == MST_TPH){
				getDataAsync = new GetDataAsyncTask();
				getDataAsync.execute();
			}else if(requestCode == MST_DRIVER || requestCode == INP_DRIVER_EX){
				nikDriver = data.getExtras().getString(Employee.XML_NIK);
				driver = data.getExtras().getString(Employee.XML_NAME);
				txtSpbsDriver.setText(driver);			
			}else if(requestCode == MST_KERNET){
				nikKernet = data.getExtras().getString(Employee.XML_NIK);
				kernet = data.getExtras().getString(Employee.XML_NAME);
				txtSpbsKernet.setText(kernet);
			}else if(requestCode == MST_RUN_ACC || requestCode == INP_LICENSE_EX){
				runningAccount = data.getExtras().getString(RunningAccount.XML_RUNNING_ACCOUNT);
				licensePlate = data.getExtras().getString(RunningAccount.XML_LICENSE_PLATE);
				txtSpbsLicensePlate.setText(licensePlate);
			}else if(requestCode == MST_DEST){
				destId = data.getExtras().getString(SPBSDestination.XML_DEST_ID);
				destDesc = data.getExtras().getString(SPBSDestination.XML_DEST_DESC);
				destType = data.getExtras().getString(SPBSDestination.XML_DEST_TYPE);
				txtSpbsDest.setText(destDesc);
			}
		}
	}  

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		switch (id) {
		case R.id.btnActionBarRight:
			long todayDate = new Date().getTime();
			
			try{
				database.openTransaction();
				
				database.updateData(new SPBSHeader(0, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, destId, destDesc, destType, division, 
						nikAssistant, assistant, nikClerk, clerk, nikDriver, driver, nikKernet, kernet, licensePlate, runningAccount,
						gpsKoordinat, 1, 0, todayDate, clerk, todayDate, clerk, lifnr),
						SPBSHeader.XML_YEAR + "=?" + " and " +
						SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +  
						SPBSHeader.XML_ESTATE + "=?" + " and " +
						SPBSHeader.XML_CROP + "=?" + " and " +
						SPBSHeader.XML_SPBS_NUMBER + "=?", 
						new String [] {year, companyCode, estate, crop, spbsNumber});
				
				for(int i = 0; i < listSpbsSummary.size(); i++){
					SPBSSummary spbsSummary = (SPBSSummary) listSpbsSummary.get(i);
					
					List<Object> listObject = database.getListData(false, SPBSLine.TABLE_NAME, null, 
							SPBSLine.XML_YEAR + "=?"+ " and " +
						    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
						    SPBSLine.XML_ESTATE + "=?" + " and " +
						    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
						    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
						    SPBSLine.XML_BLOCK + "=?" + " and " +
						    SPBSLine.XML_CROP + "=?",
						    new String [] {year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop},
						    null, null, null, null);
					
					if(listObject.size() > 0){
						for(int j = 0; j < listObject.size(); j++){
							SPBSLine spbsLine = (SPBSLine) listObject.get(j);
							
							spbsLine.setIsSave(1);
							spbsLine.setModifiedDate(todayDate);
							spbsLine.setModifiedBy(clerk);

							database.updateData(spbsLine, 
									SPBSLine.XML_YEAR + "=?"+ " and " +
								    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
								    SPBSLine.XML_ESTATE + "=?" + " and " +
								    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
								    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
								    SPBSLine.XML_BLOCK + "=?" + " and " +
								    SPBSLine.XML_CROP + "=?" + " and " +
								    SPBSLine.XML_TPH + "=?" + " and " +
								    SPBSLine.XML_BPN_DATE + "=?" + " and " +
								    SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
								    SPBSLine.XML_ID + "=?",
								    new String [] {year, companyCode, estate, spbsNumber, spbsDate, spbsSummary.getBlock(), crop, spbsLine.getTph(), 
									spbsLine.getBpnDate(), spbsLine.getAchievementCode(), String.valueOf(spbsLine.getId())});

							
							String bpnId = spbsLine.getBpnId();
							
							if(!TextUtils.isEmpty(bpnId)){   
								BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null, 
									BPNHeader.XML_BPN_ID + "=?", 
									new String [] {bpnId}, 
									null, null, null, null);
								
								if(bpnHeader != null){
									bpnHeader.setSpbsNumber(spbsNumber);
									
									database.updateData(bpnHeader, 
										BPNHeader.XML_BPN_ID + "=?", 
										new String [] {bpnId});
								}
							}
						
						}
					}
				}
				
				database.commitTransaction();
				
				txtSpbsNumber.setText(spbsNumber);
				
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.save_successed), false).show();
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
						e.getMessage(), false).show();
			}finally{
				database.closeTransaction();
			}
			break;
		case R.id.btnSpbsLoadBpn:  
			new DialogDate(SPBSUniversalActivity.this, getResources().getString(R.string.tanggal), new Date(), R.id.btnSpbsLoadBpn).show();
			break;
		case R.id.btnSpbsSummaryItemDelete:
			try{  
				SPBSSummary spbsSummary = (SPBSSummary) object;
				
				database.openTransaction();
				
				List<Object> lstObject = database.getListData(false, SPBSLine.TABLE_NAME, null,
						SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
						SPBSLine.XML_SPBS_DATE + "=?" + " and " +
						SPBSLine.XML_BLOCK + "=?", 
						new String [] {spbsNumber, spbsDate, spbsSummary.getBlock()}, 
						null, null, null, null);
				
				int delCount = 0;
				
				if(lstObject != null && lstObject.size() > 0){
					for(Object obj : lstObject){
						SPBSLine spbsLine = (SPBSLine) obj;
						
						String bpnId =  spbsLine.getBpnId();
						String spbsRef = spbsLine.getSpbsRef();
						String spbsNext = spbsLine.getSpbsNext();
						
						boolean del = false;
						
						if(TextUtils.isEmpty(spbsNext)){
							if(!TextUtils.isEmpty(spbsRef)){
								String spbsNumber = spbsRef.split("_")[0];
								String lineId = spbsRef.split("_")[1];
								
								SPBSLine spbsLineRef = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null, 
										SPBSLine.XML_ID + " = ? " + " and " +
										SPBSLine.XML_SPBS_NUMBER + " = ? " + " and " +
										SPBSLine.XML_ACHIEVEMENT_CODE + " =?", 
										new String [] {lineId, spbsNumber, spbsLine.getAchievementCode()},
										null, null, null, null);
								
								if(spbsLineRef != null){
									spbsLineRef.setSpbsNext("");
									
									database.updateData(spbsLineRef, 
											SPBSLine.XML_ID + " = ? " + " and " +
											SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
											SPBSLine.XML_ACHIEVEMENT_CODE + " =?",
											new String [] {lineId, spbsNumber, spbsLine.getAchievementCode()});
									
									del = true;
								}
							}else{
								BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null, 
										BPNHeader.XML_BPN_ID + "=?", 
										new String [] {bpnId}, null, null, null, null);
								
								if(bpnHeader != null){
									bpnHeader.setSpbsNumber("");
									database.updateData(bpnHeader, 
											BPNHeader.XML_BPN_ID + "=?",
											new String [] {bpnId});
									
									del = true;
								}
							}
						}
						
						if(del){
							database.deleteData(SPBSLine.TABLE_NAME, 
									SPBSLine.XML_YEAR + "=?" + " and " +
									SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
									SPBSLine.XML_ESTATE + "=?" + " and " +
									SPBSLine.XML_CROP + "=?" + " and " +
									SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
									SPBSLine.XML_SPBS_DATE + "=?" + " and " +
									SPBSLine.XML_BLOCK + "=?" + " and " +
									SPBSLine.XML_ID + "=?", 
									new String [] {year, companyCode, estate, crop, spbsNumber, spbsDate, spbsSummary.getBlock(), String.valueOf(spbsLine.getId())});
							
							delCount++;
						}
					}
				}
				
				database.commitTransaction();
				
				if(delCount == lstObject.size()){ 
					adapter.deleteData(spbsSummary);
				}else{
					new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi),
					getResources().getString(R.string.data_delete_cannot_all), false).show();
				}
				
			}catch(SQLiteException e){
				e.printStackTrace();
			}finally{
				database.closeTransaction();
			}
			break;
		default:
			if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
				getDataAsync.cancel(true);
			}
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(gps != null){
				gps.stopUsingGPS();
			}
			
			deleteUnSaved();
			gpsHandler.stopGPS();
			
			finish();
			animOnFinish();
			break;
		}
	}

	public void updateGpsKoordinat(Location location){
		if(location != null){
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			
			gpsKoordinat = latitude + ":" + longitude;
		}
	}
	
	private void deleteUnSaved(){
		database.openTransaction();
		database.deleteData(SPBSLine.TABLE_NAME, 
			SPBSLine.XML_IS_SAVE + "=?", 
			new String [] {"0"});
		database.commitTransaction();
		database.closeTransaction();
	}
	
	private String getHeaderQrCode(String spbsNumber, String spbsDate, String estate, String division, String licensePlate, 
			String driver, String nikDriver, String runningAccount, int pages, int maxPages){
		String header = "";
		
		spbsDate = new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_QR_CODE_SPBS);
		
//		header = spbsNumber + "\n" + 
//				 spbsDate + "\n" +
//				 estate + QR_CODE_DELIMITER + division + "\n" +
//				 nikClerk + "\n" + 
//				 String.valueOf(pages) + QR_CODE_DELIMITER + String.valueOf(maxPages) + "\n";
		
		header = spbsNumber + QR_CODE_DELIMITER + licensePlate + QR_CODE_DELIMITER + driver + "-" + nikDriver + QR_CODE_DELIMITER +
				spbsDate + QR_CODE_DELIMITER + estate + QR_CODE_DELIMITER + division + QR_CODE_DELIMITER + runningAccount;
		
		return header;
	}	
	
	private String getLineQrCode(int id, String block, String bpnDate, int qtyJanjang, double qtyLooseFruit){
		String line = "";
		
		bpnDate = new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_QR_CODE_SPBS);
		
//		line = String.valueOf(id) + QR_CODE_DELIMITER +
//			   block + QR_CODE_DELIMITER + 
//			   bpnDate + QR_CODE_DELIMITER + 
//			   String.valueOf(qtyJanjang) + QR_CODE_DELIMITER +
//			   String.valueOf(qtyLooseFruit) + "\n";
		
		line = String.valueOf(id) + QR_CODE_DELIMITER +
				   block + QR_CODE_DELIMITER + 
				   bpnDate + QR_CODE_DELIMITER + 
				   String.valueOf(qtyJanjang) + QR_CODE_DELIMITER +
				   String.valueOf(qtyLooseFruit);
		
		return line;
	}

	
	private void collectSPBS(){
		List<SPBSPrint> listSpbsPrint = new ArrayList<SPBSPrint>();
		
		database.openTransaction();
		
		List<Object> listSpbsLineBlock = database.getListData(true, SPBSLine.TABLE_NAME, 
				new String [] {SPBSLine.XML_BLOCK, SPBSLine.XML_BPN_DATE}, 
				SPBSLine.XML_YEAR + "=?" + " and " +
				SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
				SPBSLine.XML_ESTATE + "=?" + " and " +
				SPBSLine.XML_CROP + "=?" + " and " +
				SPBSLine.XML_SPBS_NUMBER + "=?", 
				new String [] {year, companyCode, estate, crop, spbsNumber}, 
				SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null, SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_BPN_DATE, null);
		database.closeTransaction();
		
		if(listSpbsLineBlock.size() > 0){
			for(int i = 0; i < listSpbsLineBlock.size(); i++){
				SPBSLine spbsLineBlock = (SPBSLine) listSpbsLineBlock.get(i);
				
				String block = spbsLineBlock.getBlock();
				String bpnDate = spbsLineBlock.getBpnDate();
				
				database.openTransaction();
				
				List<Object> listSpbsLineQtyJanjang = database.getListData(false, SPBSLine.TABLE_NAME, null,
						SPBSLine.XML_YEAR + "=?" + " and " +
						SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
						SPBSLine.XML_ESTATE + "=?" + " and " +
						SPBSLine.XML_CROP + "=?" + " and " +
						SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
						SPBSLine.XML_BLOCK + "=?" + " and " + 
						SPBSLine.XML_BPN_DATE + "=?" + " and " +
						SPBSLine.XML_ACHIEVEMENT_CODE + "=?", 
						new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.JANJANG_CODE}, 
						null, null, null, null);
				database.closeTransaction();
				
				int qtyJanjang = 0;
				
				if(listSpbsLineQtyJanjang.size() > 0){
					for(int k = 0; k < listSpbsLineQtyJanjang.size(); k++){
						SPBSLine spbsLine = (SPBSLine) listSpbsLineQtyJanjang.get(k);
						
						qtyJanjang = (int) (qtyJanjang + spbsLine.getQuantityAngkut());
					}
				}
				
				database.openTransaction();
				
				List<Object> listSpbsLineQtyLooseFruit = database.getListData(false, SPBSLine.TABLE_NAME, null,
						SPBSLine.XML_YEAR + "=?" + " and " +
						SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
						SPBSLine.XML_ESTATE + "=?" + " and " +
						SPBSLine.XML_CROP + "=?" + " and " +
						SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
						SPBSLine.XML_BLOCK + "=?" + " and " + 
						SPBSLine.XML_BPN_DATE + "=?" + " and " +
						SPBSLine.XML_ACHIEVEMENT_CODE + "=?", 
						new String [] {year, companyCode, estate, crop, spbsNumber, block, bpnDate, BPNQuantity.LOOSE_FRUIT_CODE}, 
						null, null, null, null);
				database.closeTransaction();
				
				double qtyLooseFruit = 0;
				
				if(listSpbsLineQtyLooseFruit.size() > 0){
					for(int k = 0; k < listSpbsLineQtyLooseFruit.size(); k++){
						SPBSLine spbsLine = (SPBSLine) listSpbsLineQtyLooseFruit.get(k);
						
						qtyLooseFruit = qtyLooseFruit + spbsLine.getQuantityAngkut();
					}
				}
				  
				SPBSPrint spbsPrint = new SPBSPrint(block, bpnDate, qtyJanjang, qtyLooseFruit);
				listSpbsPrint.add(spbsPrint);
			}
			 
			final int MAX_LINE = 5;
			  
			if(listSpbsPrint.size() > 0){
				int id = 1;
				int maxPages = 1;
				int pages = 1;
				int posStart = 0;
				int posEnd = 0;  
				
				if(listSpbsPrint.size() % MAX_LINE == 0){
					maxPages = (int) listSpbsPrint.size() / MAX_LINE;
				}else{
					maxPages = ((int) listSpbsPrint.size() / MAX_LINE) + 1;
				}

				for(int i = 0; i < listSpbsPrint.size(); i++){
					posEnd = i;
					
					if((id == MAX_LINE) || (i == (listSpbsPrint.size()-1))){

						try {
							if(bluetoothDevice != null){
								if(bluetoothDevice.getName().equalsIgnoreCase("WOOSIM")){
									int retVal = printSPBSWoosim(listSpbsPrint, posStart, posEnd, pages, maxPages);
								}else if(bluetoothDevice.getName().equalsIgnoreCase("DPP-350")){
									int retVal = printSPBSDatecs(listSpbsPrint, posStart, posEnd, pages, maxPages);
								}
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						
						id = 1;
						posStart = i+1;
						pages++;
					}
					
					id++;
				}
			}
		}
	}
	
	private int printSPBSWoosim(List<SPBSPrint> listSpbsPrint, int posStart, int posEnd, int pages, int maxPages) throws IOException {
    	String headerQrCode = getHeaderQrCode(spbsNumber, spbsDate, estate, division, licensePlate, driver, nikDriver, runningAccount, pages, maxPages);
    	String lineQrCode = "";
    	
    	for(int i = posStart; i <= posEnd; i++){
    		SPBSPrint spbsPrint = listSpbsPrint.get(i);
    		
    		if( i == posStart){
    			lineQrCode = getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
    		}else{
    			lineQrCode = lineQrCode + QR_CODE_LINE_DELIMITER + getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
    		}
    	}   
    	
    	byte[] Enter = WoosimCmd.printData();
    	byte[] Center = WoosimCmd.setTextAlign(WoosimCmd.ALIGN_CENTER);
    	byte[] Left = WoosimCmd.setTextAlign(WoosimCmd.ALIGN_LEFT);
    	byte[] Right = WoosimCmd.setTextAlign(WoosimCmd.ALIGN_RIGHT);
    	byte[] L1 = WoosimCmd.printLineFeed(1);
    	byte[] RESET = WoosimCmd.initPrinter();
    	byte[] MV80 = WoosimCmd.moveAbsPosition(80);
    	byte[] MV150 = WoosimCmd.moveAbsPosition(150);
    	byte[] MV200 = WoosimCmd.moveAbsPosition(200);
    	byte[] MV201 = WoosimCmd.moveAbsPosition(201);
    	byte[] MV350 = WoosimCmd.moveAbsPosition(350);
    	byte[] MV400 = WoosimCmd.moveAbsPosition(400);
    	byte[] MV450 = WoosimCmd.moveAbsPosition(450);
    	byte[] MV650 = WoosimCmd.moveAbsPosition(650);
    	byte[] BOLD = WoosimCmd.setTextStyle(true, false, false, 0, 0);
    	byte[] bytFontLarge = WoosimCmd.setTextStyle(true, false, false, 2,1);
    	
    	ByteArrayBuffer Buffer = new ByteArrayBuffer(1024);


		byte[] bytHeader = ("SPBS Ticket").getBytes();
		byte[] bytSPBSNumber = ("SPBS Number     : " + (destType.equalsIgnoreCase("MILL")? spbsNumber : "LANGSIR")).getBytes();
		byte[] bytSPBSDate = ("SPBS Date       : " + new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS)).getBytes();
		byte[] bytEstate = ("Estate/Division : " + estate + " / " + division).getBytes();
		byte[] bytClerk = ("Clerk           : " + nikClerk + " - " + addChar(clerk, " ", 15, 1)).getBytes();
		byte[] bytLicensePlate = ("License Plate   : " + licensePlate).getBytes();
		byte[] bytRunningAccount = ("Running Account : " + runningAccount).getBytes();
		byte[] bytDriver = ("Driver          : " + nikDriver + " - " + addChar(driver, " ", 15, 1)).getBytes();
		byte[] bytPage = ("Pages           : " + String.valueOf(pages) + " of " + String.valueOf(maxPages)).getBytes();
		byte[] bytLineDot = ("-----------------------------------------------").getBytes();
		byte[] bytColTitle = (addChar(" Block", " ", 8, 1) + "   " + addChar("Harvest Date", " ", 12, 1) + "   " + addChar("Janjang", " ", 7, 1) + "   " + "Loose Fruit").getBytes();

		// Put To Array
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(BOLD, 0, BOLD.length);
		Buffer.append(Center, 0, Center.length);
		Buffer.append(bytHeader, 0, bytHeader.length);
		Buffer.append(L1, 0, L1.length);
		Buffer.append(RESET, 0, RESET.length);
		Buffer.append(bytSPBSNumber, 0, bytSPBSNumber.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytSPBSDate, 0, bytSPBSDate.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytEstate, 0, bytEstate.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytClerk, 0, bytClerk.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytLicensePlate, 0, bytLicensePlate.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytRunningAccount, 0, bytRunningAccount.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytDriver, 0, bytDriver.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytPage, 0, bytPage.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytLineDot, 0, bytLineDot.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytColTitle, 0, bytColTitle.length);
		Buffer.append(Enter, 0, Enter.length);
		Buffer.append(bytLineDot, 0, bytLineDot.length);
		Buffer.append(Enter, 0, Enter.length);
		
    	for(int i = posStart; i <= posEnd; i++){
    		SPBSPrint spbsPrint = listSpbsPrint.get(i);
    		
    		String block = spbsPrint.getBlock();
    		String bpnDate =  new DateLocal(spbsPrint.getBpnDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS);
    		String qtyJanjang = String.valueOf((int)spbsPrint.getQtyJanjang());
    		String qtyLooseFruitString = addNol(spbsPrint.getQtyLooseFruit(), 2);
    		 
    		byte [] detail = (addChar(block, " ", 7, -1) + "  " + addChar(bpnDate, " ", 12, -1) + "    " + addChar(qtyJanjang, " ", 7, -1) + "    " + addChar(qtyLooseFruitString, " ", 7, -1)).getBytes();
    	
    		Buffer.append(detail, 0, detail.length);
    		Buffer.append(Enter, 0, Enter.length);
    	}
		
//		String qr = spbsNumber + ";" + licensePlate + ";" + driver;
		String qr = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
		
		byte [] bytQr = qr.getBytes();
		byte[] bytQRCode = WoosimBarcode.create2DBarcodeQRCode(0, (byte)0x48, 4, bytQr);
		   
		Buffer.append(L1, 0, L1.length);
		Buffer.append(bytQRCode, 0, bytQRCode.length);
		Buffer.append(L1, 0, L1.length);
		Buffer.append(L1, 0, L1.length);
		Buffer.append(Enter, 0, Enter.length);

		sendData(Buffer.toByteArray());
	
		return 1;
	}
    
	private int printSPBSDatecs(final List<SPBSPrint> listSpbsPrint, final int posStart, final int posEnd, final int pages, final int maxPages) throws IOException {    	
        StringBuffer textBuffer = new StringBuffer();
        
    	String headerQrCode = getHeaderQrCode(spbsNumber, spbsDate, estate, division, licensePlate, driver, nikDriver, runningAccount, pages, maxPages); 
    	String lineQrCode = "";
    	
    	for(int i = posStart; i <= posEnd; i++){
    		SPBSPrint spbsPrint = listSpbsPrint.get(i);
    		
    		if( i == posStart){
    			lineQrCode = getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
    		}else{
    			lineQrCode = lineQrCode + QR_CODE_LINE_DELIMITER + getLineQrCode(i + 1, spbsPrint.getBlock(), spbsPrint.getBpnDate(), spbsPrint.getQtyJanjang(), spbsPrint.getQtyLooseFruit());
    		}
    	}  
        
        textBuffer.append("{reset}{center}{b}SPBS Ticket");
        textBuffer.append("{br}");
        textBuffer.append("{br}");
        textBuffer.append("{reset}");
        textBuffer.append("SPBS Number     : " + (destType.equalsIgnoreCase("MILL")? spbsNumber : "LANGSIR"));
        textBuffer.append("{br}");
        textBuffer.append("SPBS Date       : " + new DateLocal(spbsDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS));
        textBuffer.append("{br}");
        textBuffer.append("Estate/Division : " + estate + " / " + division);
        textBuffer.append("{br}");
        textBuffer.append("Clerk           : " + nikClerk + " - " + addChar(clerk, " ", 15, 1));
        textBuffer.append("{br}");
        textBuffer.append("License Plate   : " + licensePlate);
        textBuffer.append("{br}");
        textBuffer.append("Running Account : " + runningAccount);
        textBuffer.append("{br}");
        textBuffer.append("Driver          : " + nikDriver + " - " + addChar(driver, " ", 15, 1));
        textBuffer.append("{br}");
        textBuffer.append("Pages           : " + String.valueOf(pages) + " of " + String.valueOf(maxPages));
        textBuffer.append("{br}");
        textBuffer.append("-----------------------------------------------");
        textBuffer.append("{br}");
        textBuffer.append(addChar("  Block", " ", 8, 1) + "   " + addChar("Harvest Date", " ", 12, 1) + "   " + addChar("Janjang", " ", 7, 1) + "   " + "Loose Fruit");
        textBuffer.append("{br}");
        textBuffer.append("-----------------------------------------------");
        textBuffer.append("{br}");

    	for(int i = posStart; i <= posEnd; i++){
    		SPBSPrint spbsPrint = listSpbsPrint.get(i);
    		
    		String block = spbsPrint.getBlock();
    		String bpnDate =  new DateLocal(spbsPrint.getBpnDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_PRINT_SPBS);
    		String qtyJanjang = String.valueOf((int)spbsPrint.getQtyJanjang());
    		String qtyLooseFruitString = addNol(spbsPrint.getQtyLooseFruit(), 2);
    		 
    		textBuffer.append(addChar(block, " ", 8, -1) + "  " + addChar(bpnDate, " ", 12, -1) + "    " + addChar(qtyJanjang, " ", 7, -1) + "    " + addChar(qtyLooseFruitString, " ", 7, -1));
    		textBuffer.append("{br}");
    	}
        
    	textBuffer.append("{br}");
    	
    	String qrCode = headerQrCode + QR_CODE_HEADER_DELIMITER + lineQrCode;
//            	String qrCode = spbsNumber + ";" + licensePlate + ";" + driver;
    	
    	sendData(textBuffer.toString().getBytes());

//        printer.reset();
//        printer.printTaggedText(textBuffer.toString());
//        
//        printer.setBarcode(Printer.ALIGN_CENTER, false, 2, Printer.HRI_NONE, 255);
//        printer.printQRCode(14, 4, qrCode);
//        printer.feedPaper(110);
//        
//        printer.flush();                                      
    	
    	return 0;
    }
	
    private String addChar(String value, String addString, int digits, int leftOrRight){
    	String temp = value;
    	
    	if(value.length() < digits){
    		for(int i = 0; i < (digits - value.length()); i++){
    			if(leftOrRight == -1){
    				temp = addString + temp;
    			}else if(leftOrRight == 1){
    				temp = temp + addString;
    			}
    		}
    	}else{
    		temp = value.substring(0, digits);
    	}
    	
    	return temp;
    }
    
    private String addNol(Double value, int decimalDigits){
    	String valueString = String.valueOf(value);
    	String temp = "";
    	
    	try{
    		int dot = valueString.indexOf(".");
    	
        	if(dot > -1 ){
        		String beforeDot = valueString.substring(0, dot + 1);
        		String afterDot = valueString.substring(dot + 1, valueString.length());

        		temp = beforeDot + addChar(afterDot, "0", decimalDigits, 1);
        	}else{
        		temp = valueString;
        	}
    	}catch(NullPointerException e){
    		e.printStackTrace();
    		
    		temp = valueString;
    	}
    	
    	return temp;
    }
    
    private double getQty(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String tph, String achievementCode){
		double qty = 0;
    	
    	List<Object> lstQty=  database.getListData(false, BPNQuantity.TABLE_NAME, null, 
    		BPNQuantity.XML_BPN_ID + "=?" + " and " +
			BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
			BPNQuantity.XML_ESTATE + "=?" + " and " +
			BPNQuantity.XML_BPN_DATE + "=?" + " and " +
			BPNQuantity.XML_DIVISION + "=?" + " and " +
			BPNQuantity.XML_GANG + "=?" + " and " +
			BPNQuantity.XML_LOCATION + "=?" + " and " +
			BPNQuantity.XML_TPH + "=?" + " and " +
			BPNQuantity.XML_ACHIEVEMENT_CODE + "=?", 
			new String [] {bpnId, companyCode, estate, bpnDate, division, gang, block, tph, achievementCode}, 
			null, null, null, null);
		
		if(lstQty.size() > 0){
			for(int i = 0; i < lstQty.size(); i++){
				BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);
				
				qty = qty + bpnQuantity.getQuantity();
			}
		}
		
		return qty;
    }
    
    private double getQly(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String tph, String achievementCode, String qualityCode){
		double qty = 0;
    	
    	List<Object> lstQly=  database.getListData(false, BPNQuality.TABLE_NAME, null, 
    		BPNQuality.XML_BPN_ID + "=?" + " and " +
			BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
			BPNQuality.XML_ESTATE + "=?" + " and " +
			BPNQuality.XML_BPN_DATE + "=?" + " and " +
			BPNQuality.XML_DIVISION + "=?" + " and " +
			BPNQuality.XML_GANG + "=?" + " and " +
			BPNQuality.XML_LOCATION + "=?" + " and " +
			BPNQuality.XML_TPH + "=?" + " and " +
			BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
			BPNQuality.XML_QUALITY_CODE + "=?", 
			new String [] {bpnId, companyCode, estate, bpnDate, division, gang, block, tph, achievementCode, qualityCode}, 
			null, null, null, null);
		
		if(lstQly.size() > 0){
			for(int i = 0; i < lstQly.size(); i++){
				BPNQuality bpnQuantity = (BPNQuality) lstQly.get(i);
				
				qty = qty + bpnQuantity.getQuantity();
			}
		}
		
		return qty;
    }
    
    private double getPenalty(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String achievementCode){
		double qty = 0;
		
		List<Object> lstPenalty = database.getListData(false, Penalty.TABLE_NAME, null, 
				Penalty.XML_IS_LOADING + "=?", 
				new String [] {"0"}, 
				null, null, null, null);
		
		if(lstPenalty != null && lstPenalty.size() > 0){
			for(Object obj : lstPenalty){
				Penalty penalty = (Penalty) obj;
				
				if(penalty != null){
				
			    	BPNQuality bpnQuality =  (BPNQuality) database.getDataFirst(false, BPNQuality.TABLE_NAME, null, 
			        		BPNQuality.XML_BPN_ID + "=?" + " and " +
			    			BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
			    			BPNQuality.XML_ESTATE + "=?" + " and " +
			    			BPNQuality.XML_BPN_DATE + "=?" + " and " +
			    			BPNQuality.XML_DIVISION + "=?" + " and " +
			    			BPNQuality.XML_GANG + "=?" + " and " +
			    			BPNQuality.XML_LOCATION + "=?" + " and " +
			    			BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
			    			BPNQuality.XML_QUALITY_CODE + "=?", 
			    			new String [] {bpnId, companyCode, estate, bpnDate, division, gang, block, achievementCode, penalty.getPenaltyCode()}, 
			    			null, null, null, null);
			    	
			    	if(bpnQuality != null)
			    		qty = qty + bpnQuality.getQuantity();
				}
			}
		}
		
		return qty;
    }

	@Override
	public void onDateOK(Date date, int id) {

		String filterDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		long todayDate = new Date().getTime();
		
		try{
			database.openTransaction();
			
			List<Object> lstSpbsLine = database.getListData(false, SPBSLine.TABLE_NAME, null, 
					SPBSLine.XML_YEAR + "=?" + " and " +
					SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
					SPBSLine.XML_ESTATE + "=?" + " and " +
					SPBSLine.XML_CROP + "=?" + " and " +
					SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
					SPBSLine.XML_SPBS_DATE + "=?" + " and " + 
					SPBSLine.XML_BPN_DATE + "=?",
					new String [] {year, companyCode, estate, crop, spbsNumber, spbsDate, filterDate}, 
					null, null, null, null);
			
			if(lstSpbsLine.size() > 0){
				for(int i = 0; i < lstSpbsLine.size(); i++){
					SPBSLine spbsLine = (SPBSLine) lstSpbsLine.get(i);
					
					String bpnId = spbsLine.getBpnId();
					String spbsRef = spbsLine.getSpbsRef();
					String spbsNext = spbsLine.getSpbsNext();
					
					boolean del = false;
					
					if(spbsRef.isEmpty()){
						BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null, 
							BPNHeader.XML_BPN_ID + "=?", 
							new String [] {bpnId}, 
							null, null, null, null);
						
						if(bpnHeader != null){
							bpnHeader.setSpbsNumber("");
							
							database.updateData(bpnHeader, 
								BPNHeader.XML_BPN_ID + " = ?", 
								new String [] {bpnHeader.getBpnId()});
							
							del = true;
						}
					}else{
						if(TextUtils.isEmpty(spbsNext)){
							String spbsNumber = spbsRef.split("_")[0];
							String lineId = spbsRef.split("_")[1];
							
							SPBSLine spbsLineRef = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null, 
									SPBSLine.XML_ID + " = ? " + " and " +
									SPBSLine.XML_SPBS_NUMBER + " = ? ", 
									new String [] {lineId, spbsNumber},
									null, null, null, null);
							
							if(spbsLineRef != null){
								spbsLineRef.setSpbsNext("");
								
								database.updateData(spbsLineRef, 
										SPBSLine.XML_ID + " = ? " + " and " +
										SPBSLine.XML_SPBS_NUMBER + " = ?", 
										new String [] {lineId, spbsNumber});
								
								del = true;
							}
						}
					}
					
					if(del){
						database.deleteData(SPBSLine.TABLE_NAME, 
								SPBSLine.XML_ID + "=?" + " and " +
								SPBSLine.XML_SPBS_NUMBER + "=?",
								new String [] {String.valueOf(spbsLine.getId()), spbsLine.getSpbsNumber()});
					}
				}
			}

			List<Object> listBlock = database.getListData(true, BPNHeader.TABLE_NAME, null, 
				BPNHeader.XML_COMPANY_CODE + "=?" + " and " +      
				BPNHeader.XML_ESTATE + "=?" + " and " +
				BPNHeader.XML_DIVISION + "=?" + " and " +
				BPNHeader.XML_NIK_CLERK + "=?" + " and " +
				BPNHeader.XML_SPBS_NUMBER + " is null" + " or " +
				BPNHeader.XML_SPBS_NUMBER + "=?" + " and " +
				BPNHeader.XML_BPN_DATE + "=?",
				new String [] {companyCode, estate, division, nikClerk, "", filterDate}, 
				null, null, null, null);
			
			if(listBlock != null && listBlock.size() > 0){				
				for(int i = 0; i < listBlock.size(); i++){
					BPNHeader bpnHeader = (BPNHeader) listBlock.get(i);
					
					int spbsLineid = i+1;
					String imei = bpnHeader.getImei();
					String companyCode = bpnHeader.getCompanyCode();
					String estate = bpnHeader.getEstate();
					String bpnId = bpnHeader.getBpnId();
					String bpnDt = bpnHeader.getBpnDate();
					String division = bpnHeader.getDivision();
					String gang = bpnHeader.getGang();
					String block = bpnHeader.getLocation();
					String crop = bpnHeader.getCrop();
					String tph = bpnHeader.getTph();
					String gpsKoordinat = bpnHeader.getGpsKoordinat();
					       
					double qtyJanjang = getQty(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.JANJANG_CODE);
					double qtyLooseFruit = getQty(bpnId, companyCode, estate, bpnDt, division, gang, block, tph, BPNQuantity.LOOSE_FRUIT_CODE);
					
					double penalty = getPenalty(bpnId, companyCode, estate, bpnDt, division, gang, block, BPNQuantity.JANJANG_CODE);
					double qtyMinusPenalty = qtyJanjang - penalty;
					
					database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block, 
							tph, bpnDt, BPNQuantity.JANJANG_CODE, qtyMinusPenalty, qtyMinusPenalty, 0, "EA", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));
					    
					database.setData(new SPBSLine(0, spbsLineid, imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block, 
							tph, bpnDt, BPNQuantity.LOOSE_FRUIT_CODE, qtyLooseFruit, qtyLooseFruit, 0, "Kg", gpsKoordinat, isSave, 0, bpnId, "", "", todayDate, clerk, 0, "", 0));
				}
			}
			
			List<Object> listSpbsLangsir = database.getListData(false, SPBSHeader.TABLE_NAME, null, 
				SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +      
				SPBSHeader.XML_ESTATE + "=?" + " and " +
				SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
				SPBSHeader.XML_SPBS_NUMBER + "!=?" + " and " +
				SPBSHeader.XML_SPBS_DATE + "=?" + " and " +
				SPBSHeader.XML_DEST_TYPE + "!=? COLLATE NOCASE", 
				new String [] {companyCode, estate, nikClerk, spbsNumber, filterDate, "MILL"}, 
				null, null, null, null);
			
			if(listSpbsLangsir != null && listSpbsLangsir.size() > 0){
				for(int i = 0; i < listSpbsLangsir.size(); i++){
					SPBSHeader spbsHeader = (SPBSHeader) listSpbsLangsir.get(i);
					
					List<Object> listSpbsLine = database.getListData(false, SPBSLine.TABLE_NAME, null, 
							SPBSLine.XML_IMEI + "=?" + " and " +
							SPBSLine.XML_YEAR + "=?" + " and " +
							SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
							SPBSLine.XML_ESTATE + "=?" + " and " +
							SPBSLine.XML_CROP + "=?" + " and " +
							SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
							SPBSLine.XML_SPBS_DATE + "=?",
							new String [] {spbsHeader.getImei(), spbsHeader.getYear(), spbsHeader.getCompanyCode(), 
							spbsHeader.getEstate(), spbsHeader.getCrop(), spbsHeader.getSpbsNumber(), spbsHeader.getSpbsDate()}, 
							null, null, SPBSLine.XML_ID, null);
					
					if(listSpbsLine != null && listSpbsLine.size() > 0){
						for(int j = 0; j < listSpbsLine.size(); j++){
							SPBSLine spbsLine = (SPBSLine) listSpbsLine.get(j);
							
							if(TextUtils.isEmpty(spbsLine.getSpbsNext())){
								String spbsRef = spbsLine.getSpbsNumber() + "_" + spbsLine.getId();
								
								if(spbsLine.getAchievementCode().equalsIgnoreCase( BPNQuantity.JANJANG_CODE)){
									database.setData(new SPBSLine(0, spbsLine.getId(), spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(), 
										spbsLine.getEstate(), spbsLine.getCrop(), spbsNumber, spbsDate, spbsLine.getBlock(), 
										spbsLine.getTph(), spbsLine.getBpnDate(), BPNQuantity.JANJANG_CODE, spbsLine.getQuantity(), spbsLine.getQuantity(),
										0, "EA", gpsKoordinat, isSave, 0, spbsLine.getBpnId(), spbsRef, "", todayDate, clerk, 0, "", 0));
								}else if(spbsLine.getAchievementCode().equalsIgnoreCase(BPNQuantity.LOOSE_FRUIT_CODE)){
									database.setData(new SPBSLine(0, spbsLine.getId(), spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(), 
										spbsLine.getEstate(), spbsLine.getCrop(), spbsNumber, spbsDate, spbsLine.getBlock(), 
										spbsLine.getTph(), spbsLine.getBpnDate(), BPNQuantity.LOOSE_FRUIT_CODE, spbsLine.getQuantity(), spbsLine.getQuantity(),
										0, "Kg", gpsKoordinat, isSave, 0, spbsLine.getBpnId(), spbsRef, "", todayDate, clerk, 0, "", 0));
								}
							}
						}
					}
				}
			}
			
//			else{
//				new DialogNotification(SPBSActivity.this, getResources().getString(R.string.informasi), 
//						getResources().getString(R.string.data_empty), false).show();
//			}
			
			database.commitTransaction();
		}catch(SQLiteException e){
			e.printStackTrace();
			database.closeTransaction();
			new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi), 
					e.getMessage(), false).show();
		}finally{
			database.closeTransaction();
		}
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}
	
	private SPBSRunningNumber getAlias(String estate, String division, String year, String month, String imei){
		DatabaseHandler database = new DatabaseHandler(SPBSUniversalActivity.this);
		SPBSRunningNumber spbsNumberMax = null;
		
		database.openTransaction();
		DeviceAlias deviceAlias = (DeviceAlias) database.getDataFirst(false, DeviceAlias.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();
		
		if(deviceAlias == null){
			database.openTransaction();
			spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null, 
					SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
					SPBSRunningNumber.XML_YEAR + "=?" + " and " +
					SPBSRunningNumber.XML_MONTH + "=?" + " and " +
					SPBSRunningNumber.XML_IMEI + "=?", 
					new String [] {estate, year, month, imei}, 
					null, null, SPBSRunningNumber.XML_ID + " desc", null);			
			database.closeTransaction();
			
			if(spbsNumberMax == null){
				database.openTransaction();
				SPBSRunningNumber tempNumber = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null, 
						SPBSRunningNumber.XML_IMEI + "=?", 
						new String [] {imei}, 
						null, null, SPBSRunningNumber.XML_ID + " desc", null);			
				database.closeTransaction();
				
				if(tempNumber != null){
					String alias = tempNumber.getDeviceAlias();
					int id = tempNumber.getId() + 1;
					spbsNumberMax = new SPBSRunningNumber(0, id, estate, division, year, month, imei, "0", alias);
				}    
			}
		}else{
			database.openTransaction();
			spbsNumberMax = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null, 
					SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
					SPBSRunningNumber.XML_YEAR + "=?" + " and " +
					SPBSRunningNumber.XML_MONTH + "=?" + " and " +
					SPBSRunningNumber.XML_IMEI + "=?" + " and " +
					SPBSRunningNumber.XML_DEVICE_ALIAS + "=?", 
					new String [] {estate, year, month, imei, deviceAlias.getDeviceAlias()}, 
					null, null, SPBSRunningNumber.XML_ID + " desc", null);
			database.closeTransaction();

			
			if(spbsNumberMax == null){
				database.openTransaction();
				SPBSRunningNumber tempNumber = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null, 
						SPBSRunningNumber.XML_IMEI + "=?" + " and " +
						SPBSRunningNumber.XML_DEVICE_ALIAS + "=?", 
						new String [] {imei, deviceAlias.getDeviceAlias()}, 
						null, null, SPBSRunningNumber.XML_ID + " desc", null);			
				database.closeTransaction();
				
				if(tempNumber != null){
					String alias = tempNumber.getDeviceAlias();
					int id = tempNumber.getId() + 1;
					spbsNumberMax = new SPBSRunningNumber(0, id, estate, division, year, month, imei, "0", alias);
				}
			}
		}
		
		return spbsNumberMax;
	}

    private final MyHandler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<SPBSUniversalActivity> mActivity;

        public MyHandler(SPBSUniversalActivity spbsUniversalActivity) {
            mActivity = new WeakReference<>(spbsUniversalActivity);
        }

        @Override
        public void handleMessage(Message msg) {
        	SPBSUniversalActivity activity = mActivity.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }

    private void handleMessage(Message msg) {
    	collectSPBS();
    }
	
	private void connectToPrinter(){
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if(bluetoothAdapter == null){
			return;
		}
		
		if (!bluetoothAdapter.isEnabled()) {
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    startActivityForResult(enableBtIntent, 1); 
		}
		
		database.openTransaction();
		Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();
		
		if(bluetooth != null){
			bluetoothDevice = bluetoothAdapter.getRemoteDevice(bluetooth.getAddress());
			
			if(bluetoothDevice.getBondState() == BluetoothDevice.BOND_BONDED){			
				if(mPrintService == null) mPrintService = new BluetoothPrintService(mHandler);
				
		        if (mPrintService != null) {
		            if (mPrintService.getState() == BluetoothPrintService.STATE_NONE) {
		                mPrintService.start();   
		            }      
		        }
		        
		        if(mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED){
					mPrintService.connect(bluetoothDevice, true);
				}else{
					collectSPBS();
				}
				
			}else{
				new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.bluetooth_not_found), false).show();
			}
		}else{
			new DialogNotification(SPBSUniversalActivity.this, getResources().getString(R.string.informasi), 
					getResources().getString(R.string.bluetooth_not_found), false).show();
		}
	}

    private void sendData(byte[] data) {
        // Check that we're actually connected before trying printing
        if (mPrintService.getState() != BluetoothPrintService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.bluetooth_not_found, Toast.LENGTH_SHORT).show();
            return;
        }
        // Check that there's actually something to send
        if (data.length > 0)
            mPrintService.write(data);
    }

	private void GetDefaultSPBSDestination() {
		DatabaseHandler database = new DatabaseHandler(SPBSUniversalActivity.this);

		database.openTransaction();
		List<Object> listObject =  database.getListData(false, SPBSDestination.TABLE_NAME, null,
				SPBSDestination.XML_ESTATE + "=?",
				new String [] {estate},
				null, null, null, null);
		database.closeTransaction();

		if(listObject != null && listObject.size() == 1){
			SPBSDestination spbsDestination = (SPBSDestination) listObject.get(0);

			destId = spbsDestination.getDestId();
			destDesc = spbsDestination.getDestDesc();
			destType = spbsDestination.getDestType();
			txtSpbsDest.setText(destDesc);
		}
	}
}


