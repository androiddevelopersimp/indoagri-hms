package com.simp.hms.activity.spbskranibluetooth;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.model.SPBSWB;

public class SPBSWBDetilActivity extends AppCompatActivity {

    TextView txtWbNumber;
    TextView txtTransactionType;
    TextView txtWbDate;
    TextView txtWbTimeIn;
    TextView txtWbTimeOut;
    TextView txtWbDeduction;
    TextView txtWbNettWeight;
    TextView txtSPBSNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spbswbdetil);
        SPBSWB spbswb = (SPBSWB) getIntent().getSerializableExtra("DATAWB");

        initToolbar();
        initView();
        if (spbswb != null) {
            setView(spbswb);
        }

    }

    public void initToolbar() {
        Toolbar tbrMain = (Toolbar) findViewById(R.id.tbrMain);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        txtActionBarTitle.setText("SPBS WB Detil");
        btnActionBarRight.setVisibility(View.INVISIBLE);
    }

    public void initView() {
        txtWbNumber = (TextView) findViewById(R.id.txtWbNumber);
        txtTransactionType = (TextView) findViewById(R.id.txtWBTransactionType);
        txtWbDate = (TextView) findViewById(R.id.txtWbDate);
        txtWbTimeIn = (TextView) findViewById(R.id.txtWbTimeIn);
        txtWbTimeOut = (TextView) findViewById(R.id.txtWbTimeOut);
        txtWbDeduction = (TextView) findViewById(R.id.txtWbDeduction);
        txtWbNettWeight = (TextView) findViewById(R.id.txtNettWeight);
        txtSPBSNo = (TextView) findViewById(R.id.txtSPBSNo);

    }

    public void setView(SPBSWB spbswb) {
        txtWbNumber.setText(spbswb.getWbNumber());
        txtTransactionType.setText(spbswb.getTransactionType());
        txtWbDate.setText(spbswb.getWbDate());
        txtWbTimeIn.setText(spbswb.getTimeIn());
        txtWbTimeOut.setText(spbswb.getTimeOut());
        txtWbDeduction.setText(spbswb.getDeduction());
        txtWbNettWeight.setText(spbswb.getNetto());
        txtSPBSNo.setText(spbswb.getSpbsNumber());
    }
}
