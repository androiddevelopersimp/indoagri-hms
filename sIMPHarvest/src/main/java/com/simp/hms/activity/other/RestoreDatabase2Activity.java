package com.simp.hms.activity.other;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.database.DatabaseHelper;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.FileEncryptionHandler;
import com.simp.hms.handler.FolderHandler;
import com.simp.hms.handler.MD5Handler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.DateLocal;

import android.app.ActionBar;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class RestoreDatabase2Activity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogConfirmListener {
	private Toolbar tbrMain;
	private EditText edtRestoreDatabase2Password;
	private Button btnRestoreDatabase2Submit;
	
	private RestoreAsyncTask RestoreAsync;
	private DialogProgress dialogProgress;

	public static final String ROOT = "HMS";
	public static final String DATABASE = "DATABASE";
	public static final String IMPORT = "IMPORT";
	private File fileRoot;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_restore_database2);
		
		registerBaseActivityReceiver();

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		edtRestoreDatabase2Password = (EditText) findViewById(R.id.edtRestoreDatabase2Password);
		btnRestoreDatabase2Submit = (Button) findViewById(R.id.btnRestoreDatabase2Submit);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.settings_restore_database));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnRestoreDatabase2Submit.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		case R.id.btnRestoreDatabase2Submit:
//			String password = edtRestoreDatabase2Password.getText().toString().trim();
			//String password = "12345";
			String password = "IndoAgri2015Anka";
			
			if(!TextUtils.isEmpty(password)){
//				String today = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_RESTORE);
//				List<String> lstToken = new MD5Handler(new DeviceHandler(RestoreDatabase2Activity.this).getImei(), today).generateToken();
//				
				boolean found = true;
//				for(int i = 0; i < lstToken.size(); i++){
//					Log.d("tag", lstToken.get(i));
//					
//					if(lstToken.get(i).equals(password)){
//						found = true;
//						break;
//					}
//				}
				
				if(found){
					new DialogConfirm(RestoreDatabase2Activity.this, 
							getResources().getString(R.string.informasi), 
							getResources().getString(R.string.databsae_replaced), 
							null, R.id.btnRestoreDatabase2Submit).show();
				}else{
					new DialogNotification(RestoreDatabase2Activity.this, 
							getResources().getString(R.string.informasi), 
							getResources().getString(R.string.restore_invalid), false).show();
				}
			}else{
				new DialogNotification(RestoreDatabase2Activity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.restore_invalid), false).show();
			}
			break;
		default:
			break;
		}
	}

/*	private class RestoreAsyncTask extends AsyncTask<Void, Void, Boolean>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(RestoreDatabase2Activity.this, getResources().getString(R.string.wait));
				dialogProgress.show();
			}
		}

		@Override
		protected Boolean doInBackground(Void... voids) {
			
			if(isFoundRestoreDatabase()){
				return restoreDatabase();
			}
			
			return false;
		}

		@Override
		protected void onPostExecute(Boolean success) {
			super.onPostExecute(success);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(success){
				new DialogNotification(RestoreDatabase2Activity.this, 
						getResources().getString(R.string.informasi),
						getResources().getString(R.string.restore_successed) + "\n" +
						getResources().getString(R.string.apps_restart), true).show();
			}else{
				new DialogNotification(RestoreDatabase2Activity.this, 
						getResources().getString(R.string.informasi),
						getResources().getString(R.string.restore_failed), false).show();
			}

		}
	}*/

	private class RestoreAsyncTask extends AsyncTask<Void, Void, Boolean>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if(!isFinishing()){
				dialogProgress = new DialogProgress(RestoreDatabase2Activity.this, getResources().getString(R.string.wait));
				dialogProgress.show();
			}
		}

		@Override
		protected Boolean doInBackground(Void... voids) {

			return Restore();
		}

		@Override
		protected void onPostExecute(Boolean success) {
			super.onPostExecute(success);

			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}

			if(success){
				new DialogNotification(RestoreDatabase2Activity.this,
						getResources().getString(R.string.informasi),
						getResources().getString(R.string.restore_successed) + "\n" +
								getResources().getString(R.string.apps_restart), true).show();
			}else{
				new DialogNotification(RestoreDatabase2Activity.this,
						getResources().getString(R.string.informasi),
						getResources().getString(R.string.restore_failed), false).show();
			}

		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(RestoreAsync != null && RestoreAsync.getStatus() != AsyncTask.Status.FINISHED){
			RestoreAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {  
		super.onDestroy();
		
		unRegisterBaseActivityReceiver();
		
		if(RestoreAsync != null && RestoreAsync.getStatus() != AsyncTask.Status.FINISHED){
			RestoreAsync.cancel(true);
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		if(!is_finish){
			edtRestoreDatabase2Password.requestFocus();
		}else{
			closeAllActivities();
		}
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		new RestoreAsyncTask().execute();
	}
	
	private boolean isFoundRestoreDatabase(){
		boolean found = false;
		FolderHandler folderHandler = new FolderHandler(RestoreDatabase2Activity.this);
		  
		if(folderHandler.isSDCardWritable()){

            File[] files = getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
			fileRoot = new File(Environment.getExternalStorageDirectory(), ROOT);
            if(files.length > 1){

				//File folder = new File(files[files.length-1], FolderHandler.DATABASE);
				File folder = new File(files[0], FolderHandler.DATABASE);

				if (!folder.exists()) {
					folder.mkdirs();
				}

				if(folder.exists()){
					File file = new File(folder, DatabaseHelper.dbName);
					found = file.exists();
				}
            }
		}
		
		return found;
	}
	
	private boolean restoreDatabase(){
		try{
			FolderHandler folderHandler = new FolderHandler(RestoreDatabase2Activity.this);
			  
			if(folderHandler.isSDCardWritable() && folderHandler.init()){
	            File[] files = getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
	            
	            if(files.length > 1){
	            	//File folder = new File(files[files.length-1], FolderHandler.DATABASE);
					File folder = new File(files[0], FolderHandler.DATABASE);

	            	if(folder.exists()){
	            		File file = new File(folder, DatabaseHelper.dbName);

	            		if(file.exists()){
	            			if(new FileEncryptionHandler(RestoreDatabase2Activity.this).decrypt(file.getAbsolutePath())){

	        					File database = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);
	        					File databaseImport = new File(folderHandler.getFileDatabaseImport(), DatabaseHelper.dbName);

	        					if(databaseImport.exists()){
	        			            String outFileName = database.getAbsolutePath();
	        			            OutputStream myOutput = new FileOutputStream(outFileName);
	        			            InputStream myInput = new FileInputStream(databaseImport);

	        			            byte[] buffer = new byte[1024];
	        			            int length;
	        			            while ((length = myInput.read(buffer)) > 0){
	        			                  myOutput.write(buffer, 0, length);
	        			            }

	        			            myInput.close();
	        			            myOutput.flush();
	        			            myOutput.close();

//	        			            databaseImport.delete();

	    	            			return true;
	        					}
	        				}
	            		}
	            	}
	            }
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return false;
	}

	private String isExistFolder(File fileFolder){
		boolean isSuccess = false;

		if(fileFolder.exists()){
			isSuccess = true;
		}
		if(isSuccess){
			return fileFolder.getPath();
		}else{
			return null;
		}
	}

	private boolean Restore(){
		boolean result = false;
		try {

			FolderHandler folderHandler = new FolderHandler(RestoreDatabase2Activity.this);
			File sd = null;
			if(folderHandler.init()){
				sd = new File(folderHandler.getFileDatabaseImport());
			}

			if (sd.canWrite()) {
				File backupDB = new File(folderHandler.getFileDatabaseImport(), DatabaseHelper.dbName);
				File currentDB = getApplicationContext().getDatabasePath(DatabaseHelper.dbName);
				if (currentDB.exists()) {
					FileChannel src = new FileInputStream(backupDB).getChannel();
					FileChannel dst = new FileOutputStream(currentDB).getChannel();
					dst.transferFrom(src, 0, src.size());
					src.close();
					dst.close();
					/*  Toast.makeText(getApplicationContext(), "Database Restored successfully", Toast.LENGTH_SHORT).show();*/
					result = true;
				}
				else{
					result = false;
				}
			}
		} catch (Exception e) {
			result = false;
		}
		return result;
	}
}
