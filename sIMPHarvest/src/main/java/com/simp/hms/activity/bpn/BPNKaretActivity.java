package com.simp.hms.activity.bpn;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.other.BlockPlanningActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.AbsoluteEditTextWatcher;
import com.simp.hms.handler.DecimalDigitsInputFilter;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.Employee;
import com.simp.hms.model.ForemanActive;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.routines.Utils;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;
import com.simp.hms.widget.EditTextCustom;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BPNKaretActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogConfirmListener, DialogDateListener, OnFocusChangeListener {
    private Toolbar tbrMain;
    private LinearLayout lytHarvestBookRoot;
    private TextView txtHarvestBookHarvestDate;
    private TextView txtHarvestBookForeman;
    private TextView txtHarvestBookClerk;
    private TextView txtHarvestBookBlock;
    private TextView txtHarvestBookCrop;
    private TextView txtHarvestBookHarvester;
    private EditText edtHarvestBookTph;

    private EditTextCustom edtHarvestBookLatexWet;
    private EditTextCustom edtHarvestBookLatexDRC;
    private EditTextCustom edtHarvestBookLumpWet;
    private EditTextCustom edtHarvestBookLumpDRC;
    private EditTextCustom edtHarvestBookSlabWet;
    private EditTextCustom edtHarvestBookSlabDRC;
    private EditTextCustom edtHarvestBookTreelaceWet;
    private EditTextCustom edtHarvestBookTreelaceDRC;
    private TextView txtHarvestBookGpsKoordinat;

    private EditText edtHarvestBookBlockDummy;

    private HashMap<String, BPNQuality> mapBPNQuality = new HashMap<String, BPNQuality>();

    GPSService gps;
    GpsHandler gpsHandler;

    private final int MST_FOREMAN = 101;
    private final int MST_CLERK = 102;
    private final int MST_HARVESTER = 103;
    private final int MST_BLOCK = 104;
    private final int REQUEST_CAMERA = 105;
    private final int MST_GANG = 106;

    String bpnId = "";
    String imei = "";
    String companyCode = "";
    String estate = "";
    String bpnDate = "";
    String division = "";
    String gang = "";
    String block = "";
    String tph = "";
    String nikHarvester = "";
    String harvester = "";
    String nikForeman = "";
    String foreman = "";
    String nikClerk = "";
    String clerk = "";
    String crop = "02";

    String qtyWetLatexCode = BPNQuantity.LATEX_WET_CODE;
    double qtyWetLatex = 0;
    String qtyDrcLatexCode = BPNQuantity.LATEX_DRC_CODE;
    double qtyDrcLatex = 0;

    String qtyWetLumpCode = BPNQuantity.LUMP_WET_CODE;
    double qtyWetLump = 0;
    String qtyDrcLumpCode = BPNQuantity.LUMP_DRC_CODE;
    double qtyDrcLump = 0;

    String qtyWetSlabCode = BPNQuantity.SLAB_WET_CODE;
    double qtyWetSlab = 0;
    String qtyDrcSlabCode = BPNQuantity.SLAB_DRC_CODE;
    double qtyDrcSlab = 0;

    String qtyWetTreelaceCode = BPNQuantity.TREELACE_WET_CODE;
    double qtyWetTreelace = 0;
    String getDrcTreelaceCode = BPNQuantity.TREELACE_DRC_CODE;
    double qtyDrcTreelace = 0;

    String gpsKoordinat = "0.0:0.0";
    String photo = "";

    Bitmap bmp = null;
    int status = 0;
    String spbsNumber = "";
    double latitude = 0.0;
    double longitude = 0.0;
    int useGerdang = 0;

    boolean init = false;
    boolean isEdit = false;
    BPNHeader bpnHeader = null;

    DatabaseHandler database = new DatabaseHandler(BPNKaretActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();
        setContentView(R.layout.activity_bpn_karet);

        tbrMain = (Toolbar)findViewById(R.id.tbrMain);
        lytHarvestBookRoot = (LinearLayout) findViewById(R.id.lytHarvestBookRoot);
        txtHarvestBookHarvestDate = (TextView)findViewById(R.id.txtHarvestBookHarvestDate);
        txtHarvestBookForeman = (TextView)findViewById(R.id.txtHarvestBookForeman);
        txtHarvestBookClerk = (TextView)findViewById(R.id.txtHarvestBookClerk);
        txtHarvestBookBlock = (TextView)findViewById(R.id.txtHarvestBookBlock);
        txtHarvestBookCrop = (TextView)findViewById(R.id.txtHarvestBookCrop);
        txtHarvestBookHarvester = (TextView)findViewById(R.id.txtHarvestBookHarvester);
        edtHarvestBookTph = (EditText)findViewById(R.id.edtHarvestBookTph);
        edtHarvestBookLatexWet = (EditTextCustom)findViewById(R.id.edtHarvestBookLatexWet);
        edtHarvestBookLatexDRC = (EditTextCustom)findViewById(R.id.edtHarvestBookLatexDRC);
        edtHarvestBookLumpWet = (EditTextCustom)findViewById(R.id.edtHarvestBookLumpWet);
        edtHarvestBookLumpDRC = (EditTextCustom)findViewById(R.id.edtHarvestBookLumpDRC);
        edtHarvestBookSlabWet = (EditTextCustom)findViewById(R.id.edtHarvestBookSlabWet);
        edtHarvestBookSlabDRC = (EditTextCustom)findViewById(R.id.edtHarvestBookSlabDRC);
        edtHarvestBookTreelaceWet = (EditTextCustom)findViewById(R.id.edtHarvestBookTreelaceWet);
        edtHarvestBookTreelaceDRC = (EditTextCustom)findViewById(R.id.edtHarvestBookTreelaceDRC);
        txtHarvestBookGpsKoordinat = (TextView) findViewById(R.id.txtHarvestBookGpsKoordinat);

        edtHarvestBookBlockDummy = (EditText) findViewById(R.id.edtHarvestBookBlockDummy);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText("BPN Karet");
        btnActionBarRight.setText(getResources().getString(R.string.save));
        btnActionBarRight.setOnClickListener(this);

        txtHarvestBookHarvestDate.setOnClickListener(this);
        txtHarvestBookForeman.setOnClickListener(this);
        txtHarvestBookBlock.setOnClickListener(this);
        txtHarvestBookHarvester.setOnClickListener(this);
        edtHarvestBookTph.setOnFocusChangeListener(this);

        edtHarvestBookLatexWet.setOnFocusChangeListener(this);
        edtHarvestBookLatexDRC.setOnFocusChangeListener(this);
        edtHarvestBookLumpWet.setOnFocusChangeListener(this);
        edtHarvestBookLumpDRC.setOnFocusChangeListener(this);
        edtHarvestBookSlabWet.setOnFocusChangeListener(this);
        edtHarvestBookSlabDRC.setOnFocusChangeListener(this);
        edtHarvestBookTreelaceWet.setOnFocusChangeListener(this);
        edtHarvestBookTreelaceDRC.setOnFocusChangeListener(this);

        edtHarvestBookLatexWet.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});
        edtHarvestBookLatexDRC.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});
        edtHarvestBookLumpWet.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});
        edtHarvestBookLumpDRC.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});
        edtHarvestBookSlabWet.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});
        edtHarvestBookSlabDRC.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});
        edtHarvestBookTreelaceWet.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});
        edtHarvestBookTreelaceDRC.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(2)});

        edtHarvestBookLatexWet.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookLatexWet, 1));
        edtHarvestBookLatexDRC.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookLatexDRC, 1));
        edtHarvestBookLumpWet.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookLumpWet, 1));
        edtHarvestBookLumpDRC.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookLumpDRC, 1));
        edtHarvestBookSlabWet.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookSlabWet, 1));
        edtHarvestBookSlabDRC.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookSlabDRC, 1));
        edtHarvestBookTreelaceWet.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookTreelaceWet, 1));
        edtHarvestBookTreelaceDRC.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookTreelaceDRC, 1));


        gps = new GPSService(BPNKaretActivity.this);
        gpsHandler = new GpsHandler(BPNKaretActivity.this);

        GPSTriggerService.bpnKaretActivity = this;

        gpsHandler.startGPS();


        if(getIntent().getExtras() != null){
            bpnHeader = (BPNHeader) getIntent().getParcelableExtra(BPNHeader.TABLE_NAME);
        }

        if(bpnHeader !=null){
            isEdit = true;

            bpnId = bpnHeader.getBpnId();
            imei = bpnHeader.getImei();
            companyCode = bpnHeader.getCompanyCode();
            estate = bpnHeader.getEstate();
            bpnDate = bpnHeader.getBpnDate();
            division = bpnHeader.getDivision();
            gang = bpnHeader.getGang();
            block = bpnHeader.getLocation();
            tph = bpnHeader.getTph();
            nikHarvester = bpnHeader.getNikHarvester();
            harvester = bpnHeader.getHarvester();
            nikForeman = bpnHeader.getNikForeman();
            foreman = bpnHeader.getForeman();
            nikClerk = bpnHeader.getNikClerk();
            clerk = bpnHeader.getClerk();
            crop = bpnHeader.getCrop();
            gpsKoordinat = bpnHeader.getGpsKoordinat();
            photo = bpnHeader.getPhoto();
            status = bpnHeader.getStatus();
            spbsNumber = bpnHeader.getSpbsNumber();
            useGerdang = (bpnHeader.isUseGerdang() == 0) ? 0: 1;

            database.openTransaction();
            List<Object> listObjQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                    BPNQuantity.XML_BPN_ID + "=?",
                    new String [] {bpnId}, null, null, null, null);
            database.closeTransaction();

            if(listObjQty.size() > 0){
                for(int i = 0; i < listObjQty.size(); i++){
                    BPNQuantity bpnQuantity = (BPNQuantity) listObjQty.get(i);

                    if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.LATEX_WET_CODE)){
                        qtyWetLatexCode = bpnQuantity.getAchievementCode();
                        qtyWetLatex = bpnQuantity.getQuantity();
                    }else if(bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.LATEX_DRC_CODE)){
                        qtyDrcLatexCode= bpnQuantity.getAchievementCode();
                        qtyDrcLatex = bpnQuantity.getQuantity();
                    }else if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.LUMP_WET_CODE)) {
                        qtyWetLumpCode = bpnQuantity.getAchievementCode();
                        qtyWetLump = bpnQuantity.getQuantity();
                    }else if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.LUMP_DRC_CODE)) {
                        qtyDrcLumpCode = bpnQuantity.getAchievementCode();
                        qtyDrcLump = bpnQuantity.getQuantity();
                    }else if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.SLAB_WET_CODE)) {
                        qtyWetSlabCode = bpnQuantity.getAchievementCode();
                        qtyWetSlab = bpnQuantity.getQuantity();
                    }else if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.SLAB_DRC_CODE)) {
                        qtyDrcSlabCode = bpnQuantity.getAchievementCode();
                        qtyDrcSlab = bpnQuantity.getQuantity();
                    }else if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.TREELACE_WET_CODE)) {
                        qtyWetTreelaceCode = bpnQuantity.getAchievementCode();
                        qtyWetTreelace = bpnQuantity.getQuantity();
                    }else if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.TREELACE_DRC_CODE)) {
                        getDrcTreelaceCode  = bpnQuantity.getAchievementCode();
                        qtyDrcTreelace = bpnQuantity.getQuantity();
                    }
                }
            }

        }else{
            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false,
                    UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
                nikClerk = userLogin.getNik();
                clerk = userLogin.getName();
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                division = userLogin.getDivision();
                gang = userLogin.getGang();
            }

            imei = new DeviceHandler(BPNKaretActivity.this).getImei();
            bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);

            database.openTransaction();
            ForemanActive foremanActive = (ForemanActive) database.getDataFirst(false, ForemanActive.TABLE_NAME, null, null,
                    null, null, null, null, null);
            database.closeTransaction();

            if(foremanActive == null){
                database.openTransaction();
                Employee employee = (Employee) database.getDataFirst(false,
                        Employee.TABLE_NAME, null,
                        Employee.XML_COMPANY_CODE + "=?" + " and " +
                                Employee.XML_ESTATE + "=?" + " and " +
                                Employee.XML_DIVISION + "=?" + " and " +
                                Employee.XML_GANG + "=?" + " and " +
                                Employee.XML_ROLE_ID + "=?" + " and " +
                                Employee.XML_NIK + "!=?",
                        new String [] {companyCode, estate, division, gang, "LEADER", nikClerk},
                        null, null, null, null);
                database.closeTransaction();

                if(employee != null){
                    foremanActive = new ForemanActive(0, employee.getCompanyCode(), employee.getEstate(), employee.getFiscalYear(),
                            employee.getFiscalPeriod(), employee.getNik(), employee.getName(), employee.getTermDate(),
                            employee.getDivision(), employee.getRoleId(), employee.getJobPos(), employee.getGang(),
                            employee.getCostCenter(), employee.getEmpType(), employee.getValidFrom(), employee.getHarvesterCode());

                    nikForeman = foremanActive.getNik();
                    foreman = foremanActive.getName();

                }
            }else{
                nikForeman = foremanActive.getNik();
                foreman = foremanActive.getName();

                if(!TextUtils.isEmpty(foremanActive.getGang())){
                    gang = foremanActive.getGang();
                }
            }

            gpsKoordinat = getLocation();
        }

        initInputView();
    }

    private void initInputView(){
        bpnId = (TextUtils.isEmpty(bpnId)) ? new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + imei.substring(8, imei.length() - 1) : bpnId;

        txtHarvestBookHarvestDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtHarvestBookForeman.setText(foreman);
        txtHarvestBookClerk.setText(clerk);
        txtHarvestBookBlock.setText(TextUtils.isEmpty(block) ? "" : block);
        txtHarvestBookCrop.setText(crop);
        txtHarvestBookHarvester.setText(harvester);
        edtHarvestBookTph.setText(tph);

        edtHarvestBookLatexWet.setText(qtyWetLatex > 0 ? String.valueOf(Utils.round(qtyWetLatex, 2)) : "");
        edtHarvestBookLatexDRC.setText(qtyDrcLatex > 0 ? String.valueOf(Utils.round(qtyDrcLatex, 2)) : "");
        edtHarvestBookLumpWet.setText(qtyWetLump > 0 ? String.valueOf(Utils.round(qtyWetLump, 2)) : "");
        edtHarvestBookLumpDRC.setText(qtyDrcLump > 0 ? String.valueOf(Utils.round(qtyDrcLump, 2)) : "");
        edtHarvestBookSlabWet.setText(qtyWetSlab > 0 ? String.valueOf(Utils.round(qtyWetSlab, 2)) : "");
        edtHarvestBookSlabDRC.setText(qtyDrcSlab > 0 ? String.valueOf(Utils.round(qtyDrcSlab, 2)) : "");
        edtHarvestBookTreelaceWet.setText(qtyWetTreelace > 0 ? String.valueOf(Utils.round(qtyWetTreelace, 2)) : "");
        edtHarvestBookTreelaceDRC.setText(qtyDrcTreelace > 0 ? String.valueOf(Utils.round(qtyDrcTreelace, 2)) : "");

        txtHarvestBookGpsKoordinat.setText(gpsKoordinat);
        /*edtHarvestBookBlockDummy.requestFocus();*/

    }


    @Override
    public void onClick(View view) {
        ArrayList<String> listNikFilter = new ArrayList<String>();

        switch (view.getId()) {

            case R.id.txtHarvestBookHarvestDate:
                new DialogDate(BPNKaretActivity.this, getResources().getString(R.string.tanggal), new Date(), R.id.txtHarvestBookHarvestDate).show();
                break;
            case R.id.txtHarvestBookForeman:
                if(status == 0){
                    listNikFilter.add(nikClerk);
                    listNikFilter.add(nikHarvester);

                    startActivityForResult(new Intent(BPNKaretActivity.this, MasterEmployeeActivity.class)
                                    .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                    .putExtra(Employee.XML_ESTATE, estate)
                                    .putExtra(Employee.XML_DIVISION, division)
                                    .putExtra(Employee.XML_GANG, "HC")
                                    .putExtra(Employee.XML_NIK, listNikFilter)
                                    .putExtra(Constanta.SEARCH, true)
                                    .putExtra(Constanta.TYPE, EmployeeType.FOREMAN.getId())
                                    .putExtra(BPNHeader.XML_CROP, crop)
                                    .putExtra("Activity", "BPN"), MST_FOREMAN);
                }else{
                    new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.txtHarvestBookBlock:
                if(status == 0){
                    startActivityForResult(new Intent(BPNKaretActivity.this, BlockPlanningActivity.class)
                            .putExtra("readOnly", true)
                            .putExtra("croptype", "02"), MST_BLOCK);
                }else{
                    new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.txtHarvestBookHarvester:
                if(status == 0){
                    listNikFilter.add(nikClerk);
                    listNikFilter.add(nikForeman);

                    startActivityForResult(new Intent(BPNKaretActivity.this, MasterEmployeeActivity.class)
                                    .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                    .putExtra(Employee.XML_ESTATE, estate)
                                    .putExtra(Employee.XML_DIVISION, division)
                                    .putExtra(Employee.XML_GANG, gang)
                                    .putExtra(Employee.XML_NIK, listNikFilter)
                                    .putExtra(Constanta.SEARCH, true)
                                    .putExtra(Constanta.TYPE, EmployeeType.HARVESTER.getId())
                                    .putExtra(BPNHeader.XML_CROP, crop)
                                    .putExtra("Activity", "BPN"), MST_HARVESTER);
                }else{
                    new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnActionBarRight:
                tph = edtHarvestBookTph.getText().toString().trim();
                qtyWetLatex = new Converter(edtHarvestBookLatexWet.getText().toString().trim()).StrToInt();
                qtyDrcLatex = new Converter(edtHarvestBookLatexDRC.getText().toString().trim()).StrToInt();
                qtyWetLump = new Converter(edtHarvestBookLumpWet.getText().toString().trim()).StrToInt();
                qtyDrcLump = new Converter(edtHarvestBookLumpDRC.getText().toString().trim()).StrToInt();
                qtyWetSlab = new Converter(edtHarvestBookSlabWet.getText().toString().trim()).StrToInt();
                qtyDrcSlab = new Converter(edtHarvestBookSlabDRC.getText().toString().trim()).StrToInt();
                qtyWetTreelace = new Converter(edtHarvestBookTreelaceWet.getText().toString().trim()).StrToInt();
                qtyDrcTreelace = new Converter(edtHarvestBookTreelaceDRC.getText().toString().trim()).StrToInt();

                long todayDate = new Date().getTime();

                boolean IsBpnDateValid = ValidateBPNDate(bpnDate);
                if(!bpnDate.isEmpty() && !foreman.isEmpty() && IsBpnDateValid && !clerk.isEmpty() && !harvester.isEmpty() && !block.isEmpty() && !tph.isEmpty()){
                    database.openTransaction();
                    BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
                            BPNHeader.XML_BPN_ID + "=?",
                            new String [] {bpnId},
                            null, null, null, null);
                    database.closeTransaction();

                    if(bpnHeader != null){
                        if(bpnHeader.getStatus() == 1){
                            init = false;
                            new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.data_already_export), false).show();
                        }else{
                            init = false;
                            new DialogConfirm(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                        }
                    }else{
                        try{
                            database.openTransaction();
                            database.setData(new BPNHeader(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, useGerdang, crop, gpsKoordinat, photo, status, spbsNumber,
                                    todayDate, clerk, todayDate, clerk));

                            database.deleteData(BPNQuantity.TABLE_NAME,
                                    BPNQuantity.XML_BPN_ID + "=?",
                                    new String [] {bpnId});

                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, qtyWetLatexCode, qtyWetLatex, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, qtyDrcLatexCode, qtyDrcLatex, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, qtyWetLumpCode, qtyWetLump, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, qtyDrcLumpCode, qtyDrcLump, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, qtyWetSlabCode, qtyWetSlab, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, qtyDrcSlabCode, qtyDrcSlab, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, qtyWetTreelaceCode, qtyWetTreelace, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                            database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                    nikHarvester, crop, getDrcTreelaceCode, qtyDrcTreelace, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));


                            ForemanActive foremanActive = new ForemanActive(0, "", "", 0, 0, nikForeman, foreman, "", "", "", "", gang, "", "", "", "");
                            database.deleteData(ForemanActive.TABLE_NAME, null, null);
                            database.setData(foremanActive);

                            database.commitTransaction();

                            init = true;
                            new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.save_successed), false).show();


                        }catch(SQLiteException e){
                            e.printStackTrace();
                            database.closeTransaction();
                            init = false;
                            new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                                    e.getMessage(), false).show();
                        }finally{
                            database.closeTransaction();
                        }

                    }

                }else{
                    init = false;
                    new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.Invalid_data), false).show();
                }


                break;
            default:
                if(gps != null){
                    gps.stopUsingGPS();
                }

                gpsHandler.stopGPS();

                finish();
                animOnFinish();
                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode ==  RESULT_OK) {
            if(requestCode == MST_FOREMAN){
                foreman = data.getExtras().getString(Employee.XML_NAME);
                nikForeman = data.getExtras().getString(Employee.XML_NIK);
                txtHarvestBookForeman.setText(foreman);
            }else if(requestCode == MST_BLOCK){
                block = data.getExtras().getString(BlockPlanning.XML_BLOCK);
                crop = "02";

                txtHarvestBookBlock.setText(block);
                txtHarvestBookCrop.setText(crop);
            }else if(requestCode == MST_HARVESTER){
                harvester = data.getExtras().getString(Employee.XML_NAME);
                nikHarvester = data.getExtras().getString(Employee.XML_NIK);

                txtHarvestBookHarvester.setText(harvester);
                edtHarvestBookTph.requestFocus();
            }
        }
    }

    @Override
    public void onBackPressed() {
        new DialogConfirm(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                getResources().getString(R.string.exit), null, 1).show();
    }

    @Override
    public void onOK(boolean is_finish) {
        if(isEdit){
            Intent intent = new Intent(BPNKaretActivity.this, BPNHistoryActivity.class);
            intent.putExtra(BPNHeader.TABLE_NAME, bpnHeader);

            setResult(RESULT_OK, intent);
            finish();
        }else{
            if(init){
                bpnId = "";
                nikHarvester = "";
                harvester = "";
                tph = "";
                qtyWetLatex = 0;
                qtyDrcLatex = 0;
                qtyWetLump = 0;
                qtyDrcLump = 0;
                qtyWetSlab = 0;
                qtyDrcSlab = 0;
                qtyWetTreelace = 0;
                qtyDrcTreelace = 0;
                gpsKoordinat = getLocation();
                useGerdang = 0;
                photo = "";
                initInputView();
            }else{
                edtHarvestBookTph.requestFocus();
            }
        }
    }

    @Override
    public void onConfirmOK(Object object, int btnId) {
        switch(btnId) {
            case R.id.btnActionBarRight:
                long todayDate = new Date().getTime();
                try {
                    database.openTransaction();

                    bpnHeader = new BPNHeader(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, useGerdang, crop, gpsKoordinat, photo, status, spbsNumber,
                            todayDate, clerk, todayDate, clerk);

                    database.updateData(bpnHeader,
                            BPNHeader.XML_BPN_ID + "=?",
                            new String [] {bpnId});

                    database.deleteData(BPNQuantity.TABLE_NAME,
                            BPNQuantity.XML_BPN_ID + "=?",
                            new String [] {bpnId});

                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyWetLatexCode, qtyWetLatex, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyDrcLatexCode, qtyDrcLatex, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyWetLumpCode, qtyWetLump, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyDrcLumpCode, qtyDrcLump, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyWetSlabCode, qtyWetSlab, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyDrcSlabCode, qtyDrcSlab, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyWetTreelaceCode, qtyWetTreelace, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));
                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, getDrcTreelaceCode, qtyDrcTreelace, 0, 0, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                    ForemanActive foremanActive = new ForemanActive(0, "", "", 0, 0, nikForeman, foreman, "", "", "", "", gang, "", "", "", "");
                    database.deleteData(ForemanActive.TABLE_NAME, null, null);
                    database.setData(foremanActive);

                    database.commitTransaction();

                    init = true;
                    new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_successed), false).show();

                }catch(SQLiteConstraintException e){
                    e.printStackTrace();
                    database.closeTransaction();
                    new DialogNotification(BPNKaretActivity.this, getResources().getString(R.string.informasi), e.getMessage(), false).show();
                }finally{
                    database.closeTransaction();
                }

                break;
            default:
                if(gps != null){
                    gps.stopUsingGPS();
                }

                gpsHandler.stopGPS();

                finish();
                animOnFinish();
                break;

        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        bpnDate=new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
        txtHarvestBookHarvestDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        EditText edt = (EditText) view;

        edt.setSelection(edt.getText().toString().length());
    }

    public void updateGpsKoordinat(Location location){
        if(location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }

        txtHarvestBookGpsKoordinat.setText(gpsKoordinat);
    }

    private String getLocation(){
        String gpsKoordinat = "0.0:0.0";

        if(gps != null){
            Location location = gps.getLocation();
            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                gpsKoordinat = latitude + ":" + longitude;
            }
        }

        return gpsKoordinat;
    }

    private boolean ValidateBPNDate(String _bpnDate){
        boolean isValid=true;
        try
        {
            Calendar cal=Calendar.getInstance();
            Date curr_date=cal.getTime();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date BPNDate =  df.parse(_bpnDate);

            long result=curr_date.getTime()-BPNDate.getTime();
            long sec=result/1000;
            long minutes=sec/60;
            long hour=minutes/60;
            long day=hour/24;
            if(day>1 || day	<0){
                isValid=false;
            }

        }catch (ParseException ex){
            ex.printStackTrace();
        }


        return  isValid;
    }

}
