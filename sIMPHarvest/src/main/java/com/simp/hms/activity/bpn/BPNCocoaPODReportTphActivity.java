package com.simp.hms.activity.bpn;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNCocoaReportTphPOD;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BJR;
import com.simp.hms.model.BPNCocoaReportBlockPOD;
import com.simp.hms.model.BPNCocoaReportTphPOD;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.UserLogin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BPNCocoaPODReportTphActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, TextWatcher {

    private Toolbar tbrMain;
    private EditText edtBPNReportTphSearch;
    private ListView lsvBPNReportTph;

    private List<BPNCocoaReportTphPOD> lstBPNReport;
    private AdapterBPNCocoaReportTphPOD adapter;

    private BPNCocoaPODReportTphActivity.GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(BPNCocoaPODReportTphActivity.this);

    String companyCode = "";
    String estate = "";
    String division = "";
    String gang = "";
    String nikHarvester = "";
    String bpnDate = "";
    String block = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bpn_cocoa_report_tph_pod);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        edtBPNReportTphSearch = (EditText) findViewById(R.id.edtBpnReportTphSearch);
        lsvBPNReportTph = (ListView) findViewById(R.id.lsvBpnReportTph);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
        btnActionBarright.setVisibility(View.INVISIBLE);
        edtBPNReportTphSearch.addTextChangedListener(this);
        lsvBPNReportTph.setOnItemClickListener(this);

        BPNCocoaReportBlockPOD bpnReport = null;

        if(getIntent().getExtras() != null){
            bpnReport = (BPNCocoaReportBlockPOD) getIntent().getParcelableExtra(BPNCocoaReportBlockPOD.TABLE_NAME);
            bpnDate = getIntent().getExtras().getString(BPNHeader.XML_BPN_DATE);
            nikHarvester = getIntent().getExtras().getString(BPNHeader.XML_NIK_HARVESTER);
            block = getIntent().getExtras().getString(BPNHeader.XML_LOCATION);
        }

        if(bpnReport != null){
            companyCode = bpnReport.getCompanyCode();
            estate = bpnReport.getEstate();
            division = bpnReport.getDivision();
            gang = bpnReport.getGang();
        }

        getDataAsync = new GetDataAsyncTask();
        getDataAsync.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtBpnReportHarvesterBpnDate:
                new DialogDate(BPNCocoaPODReportTphActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtHarvestBookHistoryBpnDate).show();
                break;
            case R.id.btnBpnReportHarvesterSearch:
                /*getDataAsync = new BPNReportTphActivity.GetDataAsyncTask();
                getDataAsync.execute();*/
                break;
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) { }

    @Override
    public void afterTextChanged(Editable filter) {
        adapter.getFilter().filter(filter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<BPNCocoaReportTphPOD>, List<BPNCocoaReportTphPOD>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(BPNCocoaPODReportTphActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<BPNCocoaReportTphPOD> doInBackground(Void... voids) {

            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
//				gang = userLogin.getGang();
                nik = userLogin.getNik();
            }

            database.openTransaction();
            List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
                    new String [] {BPNHeader.XML_BPN_ID, BPNHeader.XML_TPH, BPNHeader.XML_CREATED_DATE,BPNHeader.XML_LOCATION},
                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BPNHeader.XML_ESTATE + "=?" + " and " +
                            BPNHeader.XML_DIVISION + "=?" + " and " +
                            BPNHeader.XML_BPN_DATE + "=?" + " and " +
                            BPNHeader.XML_NIK_HARVESTER + "=?" + " and " +
                            BPNHeader.XML_LOCATION + "=?",
                    new String [] {companyCode, estate, division, bpnDate, nikHarvester, block},
                    BPNHeader.XML_BPN_ID + ", " + BPNHeader.XML_TPH + ", " + BPNHeader.XML_CREATED_DATE, null, BPNHeader.XML_TPH + ", " + BPNHeader.XML_CREATED_DATE, null);
            database.closeTransaction();

            List<BPNCocoaReportTphPOD> listTemp = new ArrayList<BPNCocoaReportTphPOD>();

            if(listObject.size() > 0){
                double qtyEstimasi = 0;
                for(int i = 0; i < listObject.size(); i++){
                    BPNHeader bpnHeader = (BPNHeader) listObject.get(i);

                    String id = bpnHeader.getBpnId();
                    String tph = bpnHeader.getTph();
                    long createdDate = bpnHeader.getCreatedDate();
                    double qtyPODQty = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.POD_QTY_CODE);
                    double qtyGoodQty = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.GOOD_QTY_CODE);
                    double qtyGoodWeight = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.GOOD_WEIGHT_CODE);
                    double qtyBadQty = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.BAD_QTY_CODE);
                    double qtyBadWeight = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.BAD_WEIGHT_CODE);
                    double qtyPoorQty = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.POOR_QTY_CODE);
                    double qtyPoorWeight = getQty(id, companyCode, estate, division, gang, bpnDate, nikHarvester, block, tph, BPNQuantity.POOR_WEIGHT_CODE);
                    database.openTransaction();
                    List<Object> BJRValue = database.getListData(false, BJR.TABLE_NAME, null,
                            BJR.XML_COMPANY_CODE + "=?" + " and " +
                                    BJR.XML_ESTATE + "=?" + " and " +
                                    BJR.XML_BLOCK + "=?" + " " ,
                            new String [] {companyCode, estate,  block},
                            null, null, BJR.XMl_EFF_DATE+" DESC", "1");

                    double qtyBJR = 0;
                    database.closeTransaction();
                    if(BJRValue.size() > 0){
                        for(int a = 0; a < BJRValue.size(); a++){
                            BJR bjr = (BJR) BJRValue.get(a);

                            qtyBJR = bjr.getBjr();
                            qtyEstimasi = qtyPODQty*qtyBJR;
                        }
                    }else{
                        qtyEstimasi = qtyPODQty;
                    }
                    listTemp.add(new BPNCocoaReportTphPOD(id, companyCode, estate, division, tph, createdDate,
                            qtyGoodQty, qtyGoodWeight,
                            qtyBadQty, qtyBadWeight,
                            qtyPoorQty, qtyPoorWeight,
                            qtyPODQty, qtyEstimasi));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<BPNCocoaReportTphPOD> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            if(listTemp.size() > 0){
                lstBPNReport = listTemp;
                adapter = new AdapterBPNCocoaReportTphPOD(BPNCocoaPODReportTphActivity.this, lstBPNReport, R.layout.item_bpn_cocoa_report_tph_pod);
            }else{
                adapter = null;
            }

            lsvBPNReportTph.setAdapter(adapter);
        }
    }

    private double getQty(String id, String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String block, String tph, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_BPN_ID + "=?" + " and " +
                        BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                        BPNQuantity.XML_LOCATION + "=?" + " and " +
                        BPNQuantity.XML_TPH + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        BPNQuantity.XML_CROP + "=?",
                new String [] {id, companyCode, estate, division, bpnDate, nikHarvester, block, tph, achievementCode, "04"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }
}
