 package com.simp.hms.activity.main;

 import android.content.Intent;
 import android.database.sqlite.SQLiteException;
 import android.os.AsyncTask;
 import android.os.Bundle;
 import android.os.Handler;
 import android.widget.Toast;

 import com.simp.hms.R;
 import com.simp.hms.activity.BaseActivity;
 import com.simp.hms.database.DatabaseHandler;
 import com.simp.hms.dialog.DialogNotification;
 import com.simp.hms.dialog.DialogProgress;
 import com.simp.hms.handler.FileXMLHandler;
 import com.simp.hms.handler.FolderHandler;
 import com.simp.hms.handler.ParsingHandler;
 import com.simp.hms.listener.DialogNotificationListener;
 import com.simp.hms.model.AbsentType;
 import com.simp.hms.model.BJR;
 import com.simp.hms.model.BLKRISET;
 import com.simp.hms.model.BLKSBC;
 import com.simp.hms.model.BLKSBCDetail;
 import com.simp.hms.model.BLKSUGC;
 import com.simp.hms.model.BlockHdrc;
 import com.simp.hms.model.ConfigApp;
 import com.simp.hms.model.DayOff;
 import com.simp.hms.model.DivisionAssistant;
 import com.simp.hms.model.Employee;
 import com.simp.hms.model.FileXML;
 import com.simp.hms.model.GroupHead;
 import com.simp.hms.model.MasterDownload;
 import com.simp.hms.model.MessageStatus;
 import com.simp.hms.model.Penalty;
 import com.simp.hms.model.RunningAccount;
 import com.simp.hms.model.SKB;
 import com.simp.hms.model.SPBSDestination;
 import com.simp.hms.model.SPBSRunningNumber;
 import com.simp.hms.model.SPTARunningNumber;
 import com.simp.hms.model.UserApp;
 import com.simp.hms.model.UserLogin;
 import com.simp.hms.model.Vendor;

 import java.util.ArrayList;
 import java.util.Date;
 import java.util.List;

public class SplashScreenActivity extends BaseActivity implements DialogNotificationListener {

	private DialogProgress dialogProgress;
	
	DatabaseHandler database;
	ReadDataAsynctask readDataAsync;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {  
		getConfig();   
		
		super.onCreate(savedInstanceState);
		setTheme(R.style.AppBaseTheme_NoTitleBar);
		setContentView(R.layout.activity_splash_screen);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				initMaster();

				if(!isFinishing()){
					FolderHandler folderHandler = new FolderHandler(SplashScreenActivity.this);

					if(folderHandler.isSDCardWritable() && folderHandler.init()){
						readDataAsync = new ReadDataAsynctask(folderHandler);
						readDataAsync.execute();

					}else{
						new DialogNotification(SplashScreenActivity.this, getResources().getString(R.string.informasi),
								getResources().getString(R.string.error_sd_card), true).show();
					}
				}
			}
		}, 3000);
	}

	@Override
	public void onOK(boolean is_finish) {
		if(is_finish){
			closeAllActivities(); 
		}
	}
	
	private class ReadDataAsynctask extends AsyncTask<Void, Void, List<MessageStatus>>{
		FolderHandler folderHandler;
		
		public ReadDataAsynctask(FolderHandler folderHandler) {
			this.folderHandler = folderHandler;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(SplashScreenActivity.this, getResources().getString(R.string.wait));
				dialogProgress.show();
			}
		}
		
		@Override
		protected List<MessageStatus> doInBackground(Void... voids) {
			FileXMLHandler fileMasterHandler = new FileXMLHandler(SplashScreenActivity.this);
			List<FileXML> lstMaster;
			List<MessageStatus> lstMsg = new ArrayList<MessageStatus>();
			
			lstMaster = fileMasterHandler.getFiles(folderHandler.getFileMasterNew(), folderHandler.getFileMasterBackup(), ".xml");
			
			if(lstMaster.size() > 0){
				lstMsg = new ParsingHandler(SplashScreenActivity.this).ParsingXML(lstMaster);
			}
			
			return lstMsg;   
		}
 
		@Override
		protected void onPostExecute(List<MessageStatus> lstMsg) {
			super.onPostExecute(lstMsg);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null; 
			}    
			
			if(lstMsg.size() > 0){
				String msg = "";  
				
				for(int i = 0; i < lstMsg.size(); i++){
					MessageStatus msgStatus = (MessageStatus) lstMsg.get(i);
					
					if(msgStatus != null){
						msg = msg + "\n" + msgStatus.getMenu() + ": " + msgStatus.getMessage();
					}
				}   
				
				Toast.makeText(SplashScreenActivity.this, msg, Toast.LENGTH_LONG).show();
			}
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){  
				long today = new Date().getTime();
				
				database.openTransaction();
				UserApp userApp = (UserApp) database.getDataFirst(false, UserApp.TABLE_NAME, null, 
						UserApp.XML_NIK + "=?" + " and " +
						UserApp.XML_VALID_TO + ">=?", 
						new String [] {userLogin.getNik(), String.valueOf(today)}, 
						null, null, null, null);
				database.closeTransaction();

				if(userApp != null){
					startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
					finish(); 
				}else{
					startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
					finish();
				}
			}else{
				startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
				finish();  
			}
			
			animOnFinish();
		}
	}   

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(readDataAsync != null && readDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			readDataAsync.cancel(true);
		}
	}
	
	private void initMaster(){
		List<MasterDownload> lstMaster = new ArrayList<MasterDownload>();
		database = new DatabaseHandler(SplashScreenActivity.this);
		
		lstMaster.add(new MasterDownload(0, AbsentType.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, BJR.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, BLKSBC.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, BLKSBCDetail.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, BlockHdrc.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, DayOff.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, DivisionAssistant.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, Employee.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, SKB.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, SPBSRunningNumber.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, UserApp.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, RunningAccount.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, Penalty.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, SPBSDestination.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, SPTARunningNumber.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, Vendor.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, BLKSUGC.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, GroupHead.ALIAS, "", 0, 0));
		lstMaster.add(new MasterDownload(0, BLKRISET.ALIAS,"",0,0));
		lstMaster.add(new MasterDownload(0, ConfigApp.ALIAS,"",0,0));

		for(int i = 0; i < lstMaster.size(); i++){
			try{
				MasterDownload mdNew = (MasterDownload) lstMaster.get(i);
				String name = mdNew.getName();
				
				database.openTransaction();
				
				MasterDownload mdCurr = (MasterDownload) database.getDataFirst(false, MasterDownload.TABLE_NAME, null,
						MasterDownload.XML_NAME + "=?", 
						new String [] {name}, 
						null, null, null, null);
				
				if(mdCurr == null){
					database.setData(mdNew);
				}
				
				database.commitTransaction();
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();
			}finally{
				database.closeTransaction();
			}
		}
	}
}


