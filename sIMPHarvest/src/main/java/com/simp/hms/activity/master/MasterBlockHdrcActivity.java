package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.bpn.BPNActivity;
import com.simp.hms.adapter.AdapterBlockHdrc;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterBlockHdrcActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterBlockHdrc;
	private EditText edtMasterBlockHdrcSearch;
	private Button btnMasterBlockHdrcLoadMore;
	
	private List<BlockHdrc> listBlockHdrc = new ArrayList<BlockHdrc>();
	private AdapterBlockHdrc adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	boolean isSearch = false;
	String companyCode;
	String estate;
	String division;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_block_hdrc);
		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterBlockHdrc = (ListView) findViewById(R.id.lsvMasterBlockHdrc);
		edtMasterBlockHdrcSearch = (EditText) findViewById(R.id.edtMasterBlockHdrcSearch);
		btnMasterBlockHdrcLoadMore = (Button) findViewById(R.id.btnMasterBlockHdrcLoadMore);

		
		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);
		
		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_block));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtMasterBlockHdrcSearch.addTextChangedListener(this);
		lsvMasterBlockHdrc.setOnItemClickListener(this);
		btnMasterBlockHdrcLoadMore.setOnClickListener(this);
		
		companyCode = getIntent().getExtras().getString(BlockHdrc.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(BlockHdrc.XML_ESTATE, "");
		division = getIntent().getExtras().getString(BlockHdrc.XML_DIVISION, "");
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
		
		adapter = new AdapterBlockHdrc(MasterBlockHdrcActivity.this, listBlockHdrc, R.layout.item_block_hdrc);
		lsvMasterBlockHdrc.setAdapter(adapter);
		lsvMasterBlockHdrc.setScrollingCacheEnabled(false);
		
		getData();
	}

	private void getData(){
		getDataAsync = new GetDataAsyncTask(true, BlockHdrc.TABLE_NAME, new String [] {BlockHdrc.XML_BLOCK}, 
				BlockHdrc.XML_COMPANY_CODE + "=?" + " and " +
				BlockHdrc.XML_ESTATE + "=?" + " and " +
				BlockHdrc.XML_DIVISION + "=?",
				new String [] {companyCode, estate, division}, 
				BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
		getDataAsync.execute();
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		BlockHdrc block_hdrc = (BlockHdrc) adapter.getItem(pos);
		
		if(isSearch){
			setResult(RESULT_OK, new Intent(MasterBlockHdrcActivity.this, BPNActivity.class)
			.putExtra(BlockHdrc.XML_BLOCK, block_hdrc.getBlock()));
			finish();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		case R.id.btnMasterBlockHdrcLoadMore:
			getDataAsync = new GetDataAsyncTask(true, BlockHdrc.TABLE_NAME, new String [] {BlockHdrc.XML_BLOCK}, 
					BlockHdrc.XML_COMPANY_CODE + "=?" + " and " +
					BlockHdrc.XML_ESTATE + "=?" + " and " +
					BlockHdrc.XML_DIVISION + "!=?",
					new String [] {companyCode, estate, division}, 
					BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
			getDataAsync.execute();
			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BlockHdrc>, List<BlockHdrc>>{
		boolean distinct;
		String tableName;
		String [] columns;
		String whereClause;
		String [] whereArgs;
		String groupBy;
		String having;
		String orderBy;
		String limit;
		
		public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
				String groupBy, String having, String orderBy, String limit){
			this.distinct = distinct;
			this.tableName = tableName;
			this.columns = columns;
			this.whereClause = whereClause;
			this.whereArgs = whereParams;
			this.groupBy = groupBy;
			this.having = having;
			this.orderBy = orderBy;
			this.limit = limit;
		}
		

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterBlockHdrcActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<BlockHdrc> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterBlockHdrcActivity.this);
			List<BlockHdrc> listTemp = new ArrayList<BlockHdrc>();
			List<Object> listObject;
			
			database.openTransaction();
			if(isSearch){
				listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
			}else{
				listObject =  database.getListData(true, BlockHdrc.TABLE_NAME, 
						new String [] {BlockHdrc.XML_BLOCK}, 
						null, null, 
						BlockHdrc.XML_BLOCK, null, BlockHdrc.XML_BLOCK, null);
			}
			database.closeTransaction();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BlockHdrc blockHdrc = (BlockHdrc) listObject.get(i);
					
					listTemp.add(blockHdrc);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BlockHdrc> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			for(int i = 0; i < listTemp.size(); i++){
				BlockHdrc blockHdrc = (BlockHdrc) listTemp.get(i);
				
				adapter.addData(blockHdrc);
			}
			
//			if(listTemp.size() > 0){
//				listBlockHdrc = listTemp;
//				adapter = new AdapterBlockHdrc(MasterBlockHdrcActivity.this, listBlockHdrc, R.layout.item_block_hdrc);
//			}else{
//				adapter = null;
//			}
//			
//			lsvMasterBlockHdrc.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
}
