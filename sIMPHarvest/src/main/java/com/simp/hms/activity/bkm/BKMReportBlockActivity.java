package com.simp.hms.activity.bkm;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBKMReportBlock;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.BKMReportBlock;
import com.simp.hms.model.BKMReportHarvester;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class BKMReportBlockActivity extends BaseActivity implements OnItemClickListener, TextWatcher, OnClickListener{
	private Toolbar tbrMain;
	private EditText edtBkmReportBlockSearch;
	private ListView lsvBkmReportBlock;
	
	private List<BKMReportBlock> lstBKMReport;
	private AdapterBKMReportBlock adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	DatabaseHandler database = new DatabaseHandler(BKMReportBlockActivity.this);
	
	String companyCode = "";
	String estate = "";
	String division = "";
	String gang = "";
	String nikHarvester = "";
	String bkmDate = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_bkm_report_block);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		edtBkmReportBlockSearch = (EditText) findViewById(R.id.edtBkmReportBlockSearch);
		lsvBkmReportBlock = (ListView) findViewById(R.id.lsvBkmReportBlock);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.bkm));
		btnActionBarright.setVisibility(View.INVISIBLE);
		edtBkmReportBlockSearch.addTextChangedListener(this);
		lsvBkmReportBlock.setOnItemClickListener(this); 
		
		BKMReportHarvester bkmReport = null;
		
		if(getIntent().getExtras() != null){   
			bkmReport = (BKMReportHarvester) getIntent().getParcelableExtra(BKMReportHarvester.TABLE_NAME);
			bkmDate = getIntent().getExtras().getString(BKMHeader.XML_BKM_DATE, "");
		}
		
		if(bkmReport != null){
			companyCode = bkmReport.getCompanyCode();
			estate = bkmReport.getEstate();
			division = bkmReport.getDivision();
			gang = bkmReport.getGang();
			nikHarvester = bkmReport.getNik();
		}
		
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {

	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BKMReportBlock>, List<BKMReportBlock>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(BKMReportBlockActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override 
		protected List<BKMReportBlock> doInBackground(Void... voids) {
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(true, BKMOutput.TABLE_NAME, null, 
					BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
					BKMOutput.XML_ESTATE + "=?" + " and " +
					BKMOutput.XML_DIVISION + "=?" + " and " +
					BKMOutput.XML_GANG + "=?" + " and " +
					BKMOutput.XML_BKM_DATE + "=?" + " and " +
					BKMOutput.XML_NIK + "=?", 
					new String [] {companyCode, estate, division, gang, bkmDate, nikHarvester},
					null, null, BKMOutput.XML_BLOCK, null);
			database.closeTransaction();
			
			List<BKMReportBlock> listTemp = new ArrayList<BKMReportBlock>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BKMOutput bkmHeader = (BKMOutput) listObject.get(i);
					
					String block = bkmHeader.getBlock();
					double output = bkmHeader.getOutput();
					
					listTemp.add(new BKMReportBlock(block, output));
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BKMReportBlock> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				lstBKMReport = listTemp;
				adapter = new AdapterBKMReportBlock(BKMReportBlockActivity.this, lstBKMReport, R.layout.item_bkm_report_block);
			}else{
				adapter = null;
			}
			
			lsvBkmReportBlock.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override  
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
}
