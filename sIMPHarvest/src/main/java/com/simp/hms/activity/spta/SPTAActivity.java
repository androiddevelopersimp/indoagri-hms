package com.simp.hms.activity.spta;

import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.adapter.AdapterSPTAFragment;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.fragment.SPTA1Fragment;
import com.simp.hms.fragment.SPTA2Fragment;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DeviceAlias;
import com.simp.hms.model.FragmentItems;
import com.simp.hms.model.SPTA;
import com.simp.hms.model.SPTARunningNumber;
import com.simp.hms.model.UserLogin;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class SPTAActivity extends FragmentActivity implements OnClickListener, OnPageChangeListener,
		DialogNotificationListener, DialogConfirmListener {
	private Toolbar tbrMain;
	private ImageButton btnSptaPrev;
	private TextView txtSptaTitle;
	private ImageButton btnSptaNext;
	private ViewPager vprSptaPage;
	
	List<FragmentItems> listFragmentItems;
	AdapterSPTAFragment adapter;
	DatabaseHandler database = new DatabaseHandler(SPTAActivity.this);
	GPSService gps;
	GpsHandler gpsHandler;

	private String zyear = "";
	private String imei = "";
	private String sptaNum = "";
	private String companyCode = "";
	private String estate = "";
	private String divisi = "";
	private String sptaDate = "";
	private String subDiv = "";
	private String petakId = "";
	private String vendorId = "";
	private String nopol = "";
	private String logo = "";
	private double jarak = 0;
	private String runAcc1 = "";
	private String emplId1 = "";
	private String runAcc2 = "";
	private String emplId2 = "";
	private String choppedDate = "";
	private String choppedHour = "";
	private String burnDate = "";
	private String burnHour = "";
	private String loadDate = "";
	private String loadHour = "";
	private String quality = "";
	private int insentiveGulma = 0;
	private int insentiveLangsir = 0;
	private int insentiveRoboh = 0;
	private String caneType = "";
	private int costTebang = 0;
	private int costMuat = 0;
	private int costAngkut = 0;
	private int penaltyTrash = 0;
	private String gpsKoordinat = "";
	private int isSave = 0;
	private int status = 0;
	private long createdDate = 0;
	private String createdBy = "";
	private long modifiedDate = 0;
	private String modifiedBy = "";
	private String nik = "";

	double latitude = 0;
	double longitude = 0;

	int selectedIndex = 0;

	private SPTA spta = null;
	private SPTARunningNumber sptaRunningNumber = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

		setContentView(R.layout.activity_spta);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		btnSptaPrev = (ImageButton) findViewById(R.id.btnSptaPrev);
		txtSptaTitle = (TextView) findViewById(R.id.txtSptaTitle);
		btnSptaNext = (ImageButton) findViewById(R.id.btnSptaNext);
		vprSptaPage = (ViewPager) findViewById(R.id.vprSptaPage);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.spta));
		btnActionBarRight.setText(getResources().getString(R.string.save));
		btnActionBarRight.setVisibility(View.VISIBLE);
		btnActionBarRight.setOnClickListener(this);
		btnSptaPrev.setOnClickListener(this);
		btnSptaNext.setOnClickListener(this);
		vprSptaPage.setOnPageChangeListener(this);

		gps = new GPSService(SPTAActivity.this);
		gpsHandler = new GpsHandler(SPTAActivity.this);

		GPSTriggerService.sptaActivity = this;

		gpsHandler.startGPS();

		if(getIntent().getExtras() != null){
			sptaNum = getIntent().getExtras().getString(SPTA.XML_SPTA_NUM, "");
		}

		database.openTransaction();
		UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null,
				null, null, null, null, null, null);
		database.closeTransaction();

		if(userLogin != null){
			companyCode = userLogin.getCompanyCode();
			estate = userLogin.getEstate();
			divisi = userLogin.getDivision();
			nik = userLogin.getNik();
		}

		getData();
	}
	
	private void getData(){
		spta = findSPTA();

		if(spta == null) spta = new SPTA();

		adapter = new AdapterSPTAFragment(getSupportFragmentManager(), sptaNum, companyCode, estate, divisi);
		
		vprSptaPage.setOffscreenPageLimit(2);
		vprSptaPage.setAdapter(adapter);
		vprSptaPage.setCurrentItem(0);
		
		setVisibleButtonPage();
	}
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btnSptaPrev:
				if(selectedIndex > 0){
					selectedIndex--;
					vprSptaPage.setCurrentItem(selectedIndex);
				}

				setVisibleButtonPage();
				break;
			case R.id.btnSptaNext:
				if(selectedIndex < 1){
					selectedIndex++;
					vprSptaPage.setCurrentItem(selectedIndex);
				}

				setVisibleButtonPage();
				break;
			case R.id.btnActionBarRight:
				saveData();
				break;
			default:
				break;
			}
	}
  
	@Override
	public void onPageScrollStateChanged(int pos) {
	}

	@Override
	public void onPageScrolled(int pos, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int pos) {
		switch (pos){
			case 0:
				txtSptaTitle.setText(getResources().getString(R.string.spta_page_1));
				break;
			case 1:
				txtSptaTitle.setText(getResources().getString(R.string.spta_page_2));
				break;
		}

		selectedIndex = pos;
		setVisibleButtonPage();
	}

	@Override
	public void onBackPressed() {
		new DialogConfirm(SPTAActivity.this, getResources().getString(R.string.informasi),
				getResources().getString(R.string.exit), null, 1).show();
	}

	private void setVisibleButtonPage(){
		if(selectedIndex == 0){
			btnSptaPrev.setVisibility(View.INVISIBLE);
		}else{
			btnSptaPrev.setVisibility(View.VISIBLE);
		}
		
		if(selectedIndex == (2 - 1)){
			btnSptaNext.setVisibility(View.INVISIBLE);
		}else{
			btnSptaNext.setVisibility(View.VISIBLE);
		}
	}

	public void updateGpsKoordinat(Location location){
		if(location != null){
			latitude = location.getLatitude();
			longitude = location.getLongitude();

			gpsKoordinat = latitude + ":" + longitude;
		}
	}

	private void saveData(){
		new DialogConfirm(SPTAActivity.this, getResources().getString(R.string.informasi),
				getResources().getString(R.string.data_save), null, new DialogConfirmListener() {
			@Override
			public void onConfirmOK(Object object, int id) {
				saveDataConfirm();
			}
		}).show();
	}

	private void saveDataConfirm() {
		collectSPTA();

		if(!TextUtils.isEmpty(spta.getSptaNum()) && !TextUtils.isEmpty(spta.getPetakId()) &&
				!TextUtils.isEmpty(spta.getQuality()) && !TextUtils.isEmpty(spta.getCaneType()) &&
				!TextUtils.isEmpty(spta.getNopol()) && !TextUtils.isEmpty(spta.getLogo()) &&
				!TextUtils.isEmpty(spta.getVendorId())){
			if(spta.getIsSave() == 0){
				insertSPTA();
			}else{
				new DialogConfirm(SPTAActivity.this, getResources().getString(R.string.informasi),
						getResources().getString(R.string.already_exists_replaced), null, new DialogConfirmListener() {
					@Override
					public void onConfirmOK(Object object, int id) {
						updateSPTA();
					}
				}).show();
			}
		}else{
			new DialogNotification(SPTAActivity.this, getResources().getString(R.string.informasi),
					getResources().getString(R.string.Invalid_data), false).show();
		}
	}

	private SPTA findSPTA(){
		SPTA spta = new SPTA();

		try {
			database.openTransaction();
			spta = (SPTA) database.getDataFirst(false, SPTA.TABLE_NAME, null,
					SPTA.XML_SPTA_NUM + " = ? " + " and " +
					SPTA.XML_COMPANY_CODE + " = ? " + " and " +
					SPTA.XML_ESTATE + " = ? ",
					new String[] {sptaNum, companyCode, estate},
					null, null, null, null);
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}

		return spta;
	}

	private void collectSPTA(){
		List<Fragment> lstFragment = getSupportFragmentManager().getFragments();

		for(Fragment fragment : lstFragment){
			if(fragment instanceof  SPTA1Fragment) {
				SPTA1Fragment spta1Fragment = (SPTA1Fragment) fragment;

				SPTA spta1 = spta1Fragment.getData();
				sptaRunningNumber = spta1Fragment.getSptaNumberMax();

				spta.setZyear(spta1.getZyear());
				spta.setImei(spta1.getImei());
				spta.setSptaNum(spta1.getSptaNum());
				spta.setSptaDate(spta1.getSptaDate());
				spta.setCompanyCode(spta1.getCompanyCode());
				spta.setEstate(spta1.getEstate());
				spta.setDivisi(spta1.getDivisi());
				spta.setSubDiv(spta1.getSubDiv());
				spta.setPetakId(spta1.getPetakId());
				spta.setVendorId(spta1.getVendorId());
				spta.setNopol(spta1.getNopol());
				spta.setLogo(spta1.getLogo());
				spta.setJarak(spta1.getJarak());
				spta.setRunAcc1(spta1.getRunAcc1());
				spta.setEmplId1(spta1.getEmplId1());
				spta.setRunAcc2(spta1.getRunAcc2());
				spta.setEmplId2(spta1.getEmplId2());
				spta.setChoppedDate(spta1.getChoppedDate());
				spta.setChoppedHour(spta1.getChoppedHour());
				spta.setBurnDate(spta1.getBurnDate());
				spta.setBurnHour(spta1.getBurnHour());
				spta.setLoadDate(spta1.getLoadDate());
				spta.setLoadHour(spta1.getLoadHour());
				spta.setQuality(spta1.getQuality());
				spta.setCaneType(spta1.getCaneType());
			}else if(fragment instanceof SPTA2Fragment){
				SPTA2Fragment spta2Fragment = (SPTA2Fragment) fragment;

				SPTA spta2 = spta2Fragment.getData();

				spta.setInsentiveGulma(spta2.getInsentiveGulma());
				spta.setInsentiveLangsir(spta2.getInsentiveLangsir());
				spta.setInsentiveRoboh(spta2.getInsentiveRoboh());
				spta.setCostTebang(spta2.getCostTebang());
				spta.setCostMuat(spta2.getCostMuat());
				spta.setCostAngkut(spta2.getCostAngkut());
				spta.setPenaltyTrash(spta2.getPenaltyTrash());
			}
		}
	}

	private void insertSPTA(){
		try{
			long todayDate = new Date().getTime();

			spta.setCreatedDate(todayDate);
			spta.setCreatedBy(nik);
			spta.setGpsKoordinat(gpsKoordinat);
			spta.setIsSave(1);

			database.openTransaction();
			database.setData(spta);

			int lastNumber = new Converter(sptaRunningNumber.getRunningNumber()).StrToInt() + 1;
			sptaRunningNumber.setRunningNumber(String.valueOf(lastNumber));

			database.updateData(sptaRunningNumber,
					SPTARunningNumber.XML_ESTATE + " = ? " + " and " +
							SPTARunningNumber.XML_DIVISION + " = ? " + " and " +
							SPTARunningNumber.XML_YEAR + " = ? " + " and " +
							SPTARunningNumber.XML_MONTH + " = ? " + " and " +
							SPTARunningNumber.XML_IMEI + " = ? ",
					new String[]{sptaRunningNumber.getEstate(), sptaRunningNumber.getDivision(),
							sptaRunningNumber.getYear(), sptaRunningNumber.getMonth(), sptaRunningNumber.getImei()});

			database.commitTransaction();

			Fragment fragment = getSupportFragmentManager().getFragments().get(0);

			if(fragment != null && fragment instanceof SPTA1Fragment){
				((SPTA1Fragment) fragment).updateSPTANum(spta.getSptaNum());
			}

			new DialogNotification(SPTAActivity.this, getResources().getString(R.string.informasi),
					getResources().getString(R.string.save_successed), false).show();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}
	}

	private void updateSPTA(){
		try{
			long todayDate = new Date().getTime();

			spta.setModifiedDate(todayDate);
			spta.setModifiedBy(nik);

			database.openTransaction();
			database.updateData(spta,
					SPTA.XML_SPTA_NUM + " = ? " + " and " +
							SPTA.XML_COMPANY_CODE + " = ? " + " and " +
							SPTA.XML_ESTATE + " = ? ",
					new String[]{spta.getSptaNum(), spta.getCompanyCode(), spta.getEstate()});
			database.commitTransaction();

			new DialogNotification(SPTAActivity.this, getResources().getString(R.string.informasi),
					getResources().getString(R.string.save_successed), false).show();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}
	}

	private SPTARunningNumber getRunningNumber(String estate, String division, String year, String month, String imei){
		DatabaseHandler database = new DatabaseHandler(SPTAActivity.this);
		SPTARunningNumber sptaNumberMax = null;

		database.openTransaction();
		DeviceAlias deviceAlias = (DeviceAlias) database.getDataFirst(false, DeviceAlias.TABLE_NAME,
				null, null, null, null, null, null, null);
		database.closeTransaction();

		if(deviceAlias == null){
			database.openTransaction();
			sptaNumberMax = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
					SPTARunningNumber.XML_ESTATE + "=?" + " and " +
							SPTARunningNumber.XML_YEAR + "=?" + " and " +
							SPTARunningNumber.XML_MONTH + "=?" + " and " +
							SPTARunningNumber.XML_IMEI + "=?",
					new String [] {estate, year, month, imei},
					null, null, SPTARunningNumber.XML_ID + " desc", null);
			database.closeTransaction();

			if(sptaNumberMax == null){
				database.openTransaction();
				SPTARunningNumber tempNumber = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
						SPTARunningNumber.XML_IMEI + "=?",
						new String [] {imei},
						null, null, SPTARunningNumber.XML_ID + " desc", null);
				database.closeTransaction();

				if(tempNumber != null){
					String alias = tempNumber.getDeviceAlias();
					int id = tempNumber.getId() + 1;
					sptaNumberMax = new SPTARunningNumber(0, id, estate, division, year, month, imei, "0", alias);
				}
			}
		}else{
			database.openTransaction();
			sptaNumberMax = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
					SPTARunningNumber.XML_ESTATE + "=?" + " and " +
							SPTARunningNumber.XML_YEAR + "=?" + " and " +
							SPTARunningNumber.XML_MONTH + "=?" + " and " +
							SPTARunningNumber.XML_IMEI + "=?" + " and " +
							SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
					new String [] {estate, year, month, imei, deviceAlias.getDeviceAlias()},
					null, null, SPTARunningNumber.XML_ID + " desc", null);
			database.closeTransaction();

			if(sptaNumberMax == null){
				database.openTransaction();
				SPTARunningNumber tempNumber = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
						SPTARunningNumber.XML_IMEI + "=?" + " and " +
								SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
						new String [] {imei, deviceAlias.getDeviceAlias()},
						null, null, SPTARunningNumber.XML_ID + " desc", null);
				database.closeTransaction();

				if(tempNumber != null){
					String alias = tempNumber.getDeviceAlias();
					int id = tempNumber.getId() + 1;
					sptaNumberMax = new SPTARunningNumber(0, id, estate, division, year, month, imei, "0", alias);
				}
			}
		}

		return sptaNumberMax;
	}

	@Override
	public void onOK(boolean is_finish) {

	}

	@Override
	public void onConfirmOK(Object object, int id) {
		finish();
	}
}

