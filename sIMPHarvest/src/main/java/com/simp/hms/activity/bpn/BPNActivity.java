package com.simp.hms.activity.bpn;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Calendar;

import com.simp.hms.R;
import com.simp.hms.R.drawable;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.master.MasterEmployeeGangActivity;
import com.simp.hms.activity.other.BlockPlanningActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.AbsoluteEditTextWatcher;
import com.simp.hms.handler.BitmapBase64Handler;
import com.simp.hms.handler.DecimalDigitsInputFilter;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.Employee;
import com.simp.hms.model.ForemanActive;
import com.simp.hms.model.Gang;
import com.simp.hms.model.Penalty;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.routines.Utils;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;
import com.simp.hms.widget.EditTextCustom;
import com.simp.hms.dialog.DialogDate;

import android.app.ActionBar;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class BPNActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogConfirmListener, DialogDateListener, OnFocusChangeListener, OnCheckedChangeListener {
    private Toolbar tbrMain;
    private LinearLayout lytHarvestBookRoot;
    private TextView txtHarvestBookHarvestDate;
    private TextView txtHarvestBookForeman;
    private TextView txtHarvestBookClerk;
    private TextView txtHarvestBookGang;
    private TextView txtHarvestBookHarvester;
    private TextView txtHarvestBookBlock;
    private TextView txtHarvestBookCrop;
    private EditText edtHarvestBookTph;
    private EditTextCustom edtHarvestBookQtyJanjang;
    private EditTextCustom edtHarvestBookQtyLooseFruit;
    private EditTextCustom edtHarvestBookQlyMentah;
    private EditTextCustom edtHarvestBookQlyBusuk;
    private EditTextCustom edtHarvestBookQlyTangkaiPanjang;
    private CheckBox cbxHarvestBookUseGerdang;
    private TextView txtHarvestBookGpsKoordinat;
    private TextView txtHarvestBookQtyJanjangTot;
    private TextView txtHarvestBookQtyLooseFruitTot;
    private ImageView imgHarvestBookPhoto;

    private EditText edtHarvestBookBlockDummy;

    private ScrollView sclBpnPenalty;
    private LinearLayout lytBpnPenalty;

    private HashMap<String, BPNQuality> mapBPNQuality = new HashMap<String, BPNQuality>();

    GPSService gps;
    GpsHandler gpsHandler;

    private final int MST_FOREMAN = 101;
    private final int MST_CLERK = 102;
    private final int MST_HARVESTER = 103;
    private final int MST_BLOCK = 104;
    private final int REQUEST_CAMERA = 105;
    private final int MST_GANG = 106;

    String bpnId = "";
    String imei = "";
    String companyCode = "";
    String estate = "";
    String bpnDate = "";
    String division = "";
    String gang = "";
    String block = "";
    String tph = "";
    String nikHarvester = "";
    String harvester = "";
    String nikForeman = "";
    String foreman = "";
    //	String gangForeman = "";
    String nikClerk = "";
    String clerk = "";
    String crop = "01";
    String qtyJanjangCode = BPNQuantity.JANJANG_CODE;
    int qtyJanjang = 0;
    String qtyLooseFruitCode = BPNQuantity.LOOSE_FRUIT_CODE;
    double qtyLooseFruit = 0;
    String qlyMentahCode = BPNQuality.MENTAH_CODE;
    int qlyMentah = 0;
    String qlyBusukCode = BPNQuality.BUSUK_CODE;
    int qlyBusuk = 0;
    String qlyTangkaiPanjangCode = BPNQuality.TANGKAI_PANJANG_CODE;
    int qlyTangkaiPanjang = 0;
    String gpsKoordinat = "0.0:0.0";
    String photo = "";
    Bitmap bmp = null;
    int status = 0;
    String spbsNumber = "";
    double latitude = 0.0;
    double longitude = 0.0;
    int useGerdang = 0;
    long createdatebpnheader = 0;
    String modifieddatestr = "";

    boolean init = false;
    boolean isEdit = false;
    boolean isSPBSExist = false;

    BPNHeader bpnHeader = null;

    DatabaseHandler database = new DatabaseHandler(BPNActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();
        setContentView(R.layout.activity_bpn);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        lytHarvestBookRoot = (LinearLayout) findViewById(R.id.lytHarvestBookRoot);
        txtHarvestBookHarvestDate = (TextView) findViewById(R.id.txtHarvestBookHarvestDate);
        txtHarvestBookForeman = (TextView) findViewById(R.id.txtHarvestBookForeman);
        txtHarvestBookClerk = (TextView) findViewById(R.id.txtHarvestBookClerk);
        txtHarvestBookGang = (TextView) findViewById(R.id.txtHarvestBookGang);
        txtHarvestBookHarvester = (TextView) findViewById(R.id.txtHarvestBookHarvester);
        txtHarvestBookBlock = (TextView) findViewById(R.id.txtHarvestBookBlock);
        txtHarvestBookCrop = (TextView) findViewById(R.id.txtHarvestBookCrop);
        edtHarvestBookTph = (EditText) findViewById(R.id.edtHarvestBookTph);
        edtHarvestBookQtyJanjang = (EditTextCustom) findViewById(R.id.edtHarvestBookQtyJanjang);
        edtHarvestBookQtyLooseFruit = (EditTextCustom) findViewById(R.id.edtHarvestBookQtyLooseFruit);
        edtHarvestBookQlyMentah = (EditTextCustom) findViewById(R.id.edtHarvestBookQlyMentah);
        edtHarvestBookQlyBusuk = (EditTextCustom) findViewById(R.id.edtHarvestBookQlyBusuk);
        edtHarvestBookQlyTangkaiPanjang = (EditTextCustom) findViewById(R.id.edtHarvestBookQlyTangkaiPanjang);
        cbxHarvestBookUseGerdang = (CheckBox) findViewById(R.id.cbxHarvestBookUseGerdang);
        txtHarvestBookGpsKoordinat = (TextView) findViewById(R.id.txtHarvestBookGpsKoordinat);
        txtHarvestBookQtyJanjangTot = (TextView) findViewById(R.id.txtHarvestBookQtyJanjangTot);
        txtHarvestBookQtyLooseFruitTot = (TextView) findViewById(R.id.txtHarvestBookQtyLooseFruitTot);
        imgHarvestBookPhoto = (ImageView) findViewById(R.id.imgHarvestBookPhoto);

        edtHarvestBookBlockDummy = (EditText) findViewById(R.id.edtHarvestBookBlockDummy);

        sclBpnPenalty = (ScrollView) findViewById(R.id.sclBpnPenalty);
        lytBpnPenalty = (LinearLayout) findViewById(R.id.lytBpnPenalty);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
        btnActionBarRight.setText(getResources().getString(R.string.save));
        btnActionBarRight.setOnClickListener(this);

        txtHarvestBookHarvestDate.setOnClickListener(this);
        txtHarvestBookForeman.setOnClickListener(this);
        txtHarvestBookGang.setOnClickListener(this);
        txtHarvestBookHarvester.setOnClickListener(this);
        txtHarvestBookBlock.setOnClickListener(this);
        edtHarvestBookTph.setOnFocusChangeListener(this);
        edtHarvestBookQtyJanjang.setOnFocusChangeListener(this);
        edtHarvestBookQtyLooseFruit.setOnFocusChangeListener(this);
        edtHarvestBookQlyMentah.setOnFocusChangeListener(this);
        edtHarvestBookQlyBusuk.setOnFocusChangeListener(this);
        edtHarvestBookQlyTangkaiPanjang.setOnFocusChangeListener(this);
        edtHarvestBookQtyLooseFruit.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(2)});


        //edit permintaan TPH min 001 and max 887
        edtHarvestBookTph.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String strEnteredVal = s.toString();

                if (!strEnteredVal.equals("")) {
                    int num = Integer.parseInt(strEnteredVal);
                    if (num > 0 && num < 888) {
//                        edtHarvestBookTph.setText(String.valueOf(num));
                    } else {
                        edtHarvestBookTph.setText("");
                        new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(string.data_validate_tph), false).show();
                    }

                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        edtHarvestBookQtyJanjang.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookQtyJanjang, 0));
        edtHarvestBookQtyLooseFruit.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookQtyLooseFruit, 1));
        edtHarvestBookQlyMentah.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookQlyMentah, 0));
        edtHarvestBookQlyBusuk.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookQlyBusuk, 0));
        edtHarvestBookQlyTangkaiPanjang.addTextChangedListener(new AbsoluteEditTextWatcher(edtHarvestBookQlyTangkaiPanjang, 0));
        cbxHarvestBookUseGerdang.setOnCheckedChangeListener(this);
        imgHarvestBookPhoto.setOnClickListener(this);

        gps = new GPSService(BPNActivity.this);
        gpsHandler = new GpsHandler(BPNActivity.this);

        GPSTriggerService.bpnActivity = this;

        gpsHandler.startGPS();

        if (getIntent().getExtras() != null) {
            bpnHeader = (BPNHeader) getIntent().getParcelableExtra(BPNHeader.TABLE_NAME);
        }

        if (bpnHeader != null) {
            isEdit = true;

            bpnId = bpnHeader.getBpnId();
            imei = bpnHeader.getImei();
            companyCode = bpnHeader.getCompanyCode();
            estate = bpnHeader.getEstate();
            bpnDate = bpnHeader.getBpnDate();
            division = bpnHeader.getDivision();
            gang = bpnHeader.getGang();
            block = bpnHeader.getLocation();
            tph = bpnHeader.getTph();
            nikHarvester = bpnHeader.getNikHarvester();
            harvester = bpnHeader.getHarvester();
            nikForeman = bpnHeader.getNikForeman();
            foreman = bpnHeader.getForeman();
            nikClerk = bpnHeader.getNikClerk();
            clerk = bpnHeader.getClerk();
            crop = bpnHeader.getCrop();
            gpsKoordinat = bpnHeader.getGpsKoordinat();
            photo = bpnHeader.getPhoto();
            status = bpnHeader.getStatus();
            spbsNumber = bpnHeader.getSpbsNumber();
            useGerdang = (bpnHeader.isUseGerdang() == 0) ? 0 : 1;
            createdatebpnheader = bpnHeader.getCreatedDate();

            database.openTransaction();
            List<Object> listObjQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                    BPNQuantity.XML_BPN_ID + "=?",
                    new String[]{bpnId}, null, null, null, null);
            database.closeTransaction();

            if (listObjQty.size() > 0) {
                for (int i = 0; i < listObjQty.size(); i++) {
                    BPNQuantity bpnQuantity = (BPNQuantity) listObjQty.get(i);

                    if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.JANJANG_CODE)) {
                        qtyJanjangCode = bpnQuantity.getAchievementCode();
                        qtyJanjang = (int) bpnQuantity.getQuantity();
                    } else if (bpnQuantity.getAchievementCode().equalsIgnoreCase(BPNQuantity.LOOSE_FRUIT_CODE)) {
                        qtyLooseFruitCode = bpnQuantity.getAchievementCode();
                        qtyLooseFruit = bpnQuantity.getQuantity();
                    }

                    modifieddatestr = bpnQuantity.getModifiedDateStr();
                }
            }

            if (!TextUtils.isEmpty(spbsNumber)) {
                setViewAndChildrenEnabled(lytHarvestBookRoot, false);
                setViewAndChildrenEnabled(lytBpnPenalty, false);
            } else {
                setViewAndChildrenEnabled(lytHarvestBookRoot, true);
                setViewAndChildrenEnabled(lytBpnPenalty, true);
            }
        } else {
            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false,
                    UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if (userLogin != null) {
                nikClerk = userLogin.getNik();
                clerk = userLogin.getName();
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                division = userLogin.getDivision();
                gang = userLogin.getGang();
            }

            imei = new DeviceHandler(BPNActivity.this).getImei();
            bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);

            database.openTransaction();
            ForemanActive foremanActive = (ForemanActive) database.getDataFirst(false, ForemanActive.TABLE_NAME, null, null,
                    null, null, null, null, null);
            database.closeTransaction();

            if (foremanActive == null) {
                database.openTransaction();
                Employee employee = (Employee) database.getDataFirst(false,
                        Employee.TABLE_NAME, null,
                        Employee.XML_COMPANY_CODE + "=?" + " and " +
                                Employee.XML_ESTATE + "=?" + " and " +
                                Employee.XML_DIVISION + "=?" + " and " +
                                Employee.XML_GANG + "=?" + " and " +
                                Employee.XML_ROLE_ID + "=?" + " and " +
                                Employee.XML_NIK + "!=?",
                        new String[]{companyCode, estate, division, gang, "LEADER", nikClerk},
                        null, null, null, null);
                database.closeTransaction();

                if (employee != null) {
                    foremanActive = new ForemanActive(0, employee.getCompanyCode(), employee.getEstate(), employee.getFiscalYear(),
                            employee.getFiscalPeriod(), employee.getNik(), employee.getName(), employee.getTermDate(),
                            employee.getDivision(), employee.getRoleId(), employee.getJobPos(), employee.getGang(),
                            employee.getCostCenter(), employee.getEmpType(), employee.getValidFrom(), employee.getHarvesterCode());

                    nikForeman = foremanActive.getNik();
                    foreman = foremanActive.getName();

                }
            } else {
                nikForeman = foremanActive.getNik();
                foreman = foremanActive.getName();

                if (!TextUtils.isEmpty(foremanActive.getGang())) {
                    gang = foremanActive.getGang();
                }
            }

            gpsKoordinat = getLocation();
        }

        initInputView();
    }

    private void initInputView() {
        try {
            bpnId = (TextUtils.isEmpty(bpnId)) ? new DateLocal(new Date()).getDateString(DateLocal.FORMAT_ID) + imei.substring(8, imei.length() - 1) : bpnId;
            txtHarvestBookHarvestDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
            txtHarvestBookForeman.setText(foreman);
            txtHarvestBookGang.setText(gang);
            txtHarvestBookClerk.setText(clerk);
            txtHarvestBookHarvester.setText(harvester);
            txtHarvestBookCrop.setText(crop);
            edtHarvestBookTph.setText(tph);

            edtHarvestBookQtyJanjang.setText(qtyJanjang > 0 ? String.valueOf(qtyJanjang) : "");
            edtHarvestBookQtyLooseFruit.setText(qtyLooseFruit > 0 ? String.valueOf(Utils.round(qtyLooseFruit, 2)) : "");

            txtHarvestBookBlock.setText(TextUtils.isEmpty(block) ? "" : block);
            txtHarvestBookGpsKoordinat.setText(gpsKoordinat);
            cbxHarvestBookUseGerdang.setChecked((useGerdang == 0) ? false : true);
            edtHarvestBookBlockDummy.requestFocus();

            if (!TextUtils.isEmpty(nikHarvester)) {
                int qtyJanjangTot = (int) getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.JANJANG_CODE);
                double qtyLooseFruitTot = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.LOOSE_FRUIT_CODE);

                txtHarvestBookQtyJanjangTot.setText(String.valueOf(qtyJanjangTot));
                txtHarvestBookQtyLooseFruitTot.setText(String.valueOf(qtyLooseFruitTot));
            } else {
                txtHarvestBookQtyJanjangTot.setText("");
                txtHarvestBookQtyLooseFruitTot.setText("");
            }

            if (!TextUtils.isEmpty(photo)) {
                Bitmap bmp = new BitmapBase64Handler().decodeBase64(photo);
                imgHarvestBookPhoto.setImageBitmap(bmp);
            } else {
                imgHarvestBookPhoto.setImageResource(R.drawable.ic_photo);
            }

            lytBpnPenalty.removeAllViews();
            mapBPNQuality.clear();

            database.openTransaction();
            List<Object> listPenalty = database.getListData(false, Penalty.TABLE_NAME, null,
                    Penalty.XML_IS_ANCAK + " = ?",
                    new String[]{"0"},
                    null, null, Penalty.XML_PENALTY_CODE, null);
            database.closeTransaction();

            if (listPenalty != null && listPenalty.size() > 0) {
                for (int i = 0; i < listPenalty.size(); i++) {
                    Penalty penalty = (Penalty) listPenalty.get(i);

                    database.openTransaction();
                    Object obj = database.getDataFirst(false, BPNQuality.TABLE_NAME, null,
                            BPNQuality.XML_BPN_ID + "=?" +
                                    " and " + BPNQuality.XML_QUALITY_CODE + "=?",
                            new String[]{bpnId, penalty.getPenaltyCode()},
                            null, null, null, null);
                    database.closeTransaction();

                    BPNQuality bpnQuality = (BPNQuality) obj;

                    if (obj != null) {
                        AddChild(bpnQuality, penalty);
                    } else {
                        bpnQuality = new BPNQuality(0, bpnId, imei, companyCode, estate, bpnDate, division, gang,
                                block, tph, nikHarvester, crop, qtyJanjangCode, penalty.getPenaltyCode(), 0, status, new Date().getTime(), clerk, new Date().getTime(), clerk);

                        AddChild(bpnQuality, penalty);
                    }
                }
            }

            //Fernando, check SPBS Line jika sudah ada, kolom Janjang & Loose Fruit tidak bisa diedit
            database.openTransaction();
            List<Object> listspbsline = database.getListData(false, SPBSLine.TABLE_NAME, null,
                    SPBSLine.XML_BPN_ID + "=?" + " and " +
                            SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSLine.XML_ESTATE + "=?",
                    new String[]{bpnId, companyCode, estate},
                    null, null, null, null);
            if (listspbsline.size() > 0) {
                isSPBSExist = true;
                txtHarvestBookBlock.setEnabled(false);
                txtHarvestBookHarvestDate.setEnabled(false);
                txtHarvestBookCrop.setEnabled(false);
                edtHarvestBookTph.setEnabled(false);
                edtHarvestBookQtyJanjang.setEnabled(false);
                edtHarvestBookQtyLooseFruit.setEnabled(false);
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
        } finally {
            database.closeTransaction();
        }
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> listNikFilter = new ArrayList<String>();

        switch (view.getId()) {

            //add by adit 20161221
            case R.id.txtHarvestBookHarvestDate:
                new DialogDate(BPNActivity.this, getResources().getString(R.string.tanggal), new Date(), id.txtHarvestBookHarvestDate).show();
                break;
            case R.id.txtHarvestBookForeman:
                if (status == 0) {
                    listNikFilter.add(nikClerk);
                    listNikFilter.add(nikHarvester);

                    startActivityForResult(new Intent(BPNActivity.this, MasterEmployeeActivity.class)
                                    .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                    .putExtra(Employee.XML_ESTATE, estate)
                                    .putExtra(Employee.XML_DIVISION, division)
                                    .putExtra(Employee.XML_GANG, "HC")
                                    .putExtra(Employee.XML_NIK, listNikFilter)
                                    .putExtra(Constanta.SEARCH, true)
                                    .putExtra(Constanta.TYPE, EmployeeType.FOREMAN.getId())
                                    .putExtra(BPNHeader.XML_CROP, crop)
                                    .putExtra("Activity", "BPN"),
                            MST_FOREMAN);
                } else {
                    new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.txtHarvestBookGang:
                startActivityForResult(new Intent(BPNActivity.this, MasterEmployeeGangActivity.class)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, division),
                        MST_GANG);
                break;
            case R.id.txtHarvestBookHarvester:
                if (status == 0) {
                    listNikFilter.add(nikClerk);
                    listNikFilter.add(nikForeman);

                    startActivityForResult(new Intent(BPNActivity.this, MasterEmployeeActivity.class)
                                    .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                    .putExtra(Employee.XML_ESTATE, estate)
                                    .putExtra(Employee.XML_DIVISION, division)
                                    .putExtra(Employee.XML_GANG, gang)
                                    .putExtra(Employee.XML_NIK, listNikFilter)
                                    .putExtra(Constanta.SEARCH, true)
                                    .putExtra(Constanta.TYPE, EmployeeType.HARVESTER.getId())
                                    .putExtra(BPNHeader.XML_CROP, crop)
                                    .putExtra("Activity", "BPN"),
                            MST_HARVESTER);
                } else {
                    new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.txtHarvestBookBlock:
                if (status == 0) {
                    startActivityForResult(new Intent(BPNActivity.this, BlockPlanningActivity.class)
                            .putExtra("readOnly", true)
                            .putExtra("croptype", "01"), MST_BLOCK);
                } else {
                    new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_already_export), false).show();
                }
                break;
            case R.id.btnActionBarRight:
                if (!isSPBSExist) {

                    tph = edtHarvestBookTph.getText().toString().trim();
                    qtyJanjang = new Converter(edtHarvestBookQtyJanjang.getText().toString().trim()).StrToInt();
                    qtyLooseFruit = new Converter(edtHarvestBookQtyLooseFruit.getText().toString().trim()).StrToDouble();
                    qlyMentah = new Converter(edtHarvestBookQlyMentah.getText().toString().trim()).StrToInt();
                    qlyBusuk = new Converter(edtHarvestBookQlyBusuk.getText().toString().trim()).StrToInt();
                    qlyTangkaiPanjang = new Converter(edtHarvestBookQlyTangkaiPanjang.getText().toString().trim()).StrToInt();

                    double qlyCheck = qlyMentah + qlyBusuk + qlyTangkaiPanjang;

                    long createddate = 0;
                    long modifieddate = new Date().getTime();
                    ;
                    if (isEdit) {
                        createddate = createdatebpnheader;
                    } else {
                        createddate = new Date().getTime();
                        modifieddatestr = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
                    }

                    //add by adit 20161221
                    boolean IsBpnDateValid = ValidateBPNDate(bpnDate);
                    if (!bpnDate.isEmpty() && !foreman.isEmpty() && IsBpnDateValid && !clerk.isEmpty() && !harvester.isEmpty() && !block.isEmpty() && !tph.isEmpty() && (qtyJanjang + qtyLooseFruit > 0) && !gang.isEmpty()) {
                        if (qlyCheck <= qtyJanjang) {

                            //add by adit 20161229
                            if (gang.toLowerCase().contains("hc")) {
                                database.openTransaction();
                                BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
                                        BPNHeader.XML_BPN_ID + "=?",
                                        new String[]{bpnId},
                                        null, null, null, null);
                                database.closeTransaction();

                                if (bpnHeader != null) {
                                    if (bpnHeader.getStatus() == 1) {
                                        init = false;
                                        new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                                                getResources().getString(R.string.data_already_export), false).show();
                                    } else {
                                        init = false;
                                        new DialogConfirm(BPNActivity.this, getResources().getString(R.string.informasi),
                                                getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                                    }
                                } else {

                                    try {

                                        database.openTransaction();
                                        database.setData(new BPNHeader(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                                nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, useGerdang, crop, gpsKoordinat, photo, status, spbsNumber,
                                                createddate, clerk, modifieddate, clerk));

                                        database.deleteData(BPNQuantity.TABLE_NAME,
                                                BPNQuantity.XML_BPN_ID + "=?",
                                                new String[]{bpnId});

                                        database.deleteData(BPNQuality.TABLE_NAME,
                                                BPNQuality.XML_BPN_ID + "=?",
                                                new String[]{bpnId});

                                        double penalty = 0;
                                        double penaltyResult = 0;
                                        double qtyJanjangRemaining = 0;
                                        for (Map.Entry<String, BPNQuality> entry : mapBPNQuality.entrySet()) {
                                            BPNQuality bpnQuality = entry.getValue();
                                            //add by adit 20161228
                                            bpnQuality.setGang(gang);
                                            bpnQuality.setLocation(block);
                                            bpnQuality.setNikHarvester(nikHarvester);
                                            bpnQuality.setCrop(crop);
                                            bpnQuality.setTph(tph);
                                            bpnQuality.setModifiedDate(new Date().getTime());
                                            bpnQuality.setModifiedBy(clerk);
                                            Penalty penaltyRow = (Penalty) database.getDataFirst(false, Penalty.TABLE_NAME, null,
                                                    Penalty.XML_PENALTY_CODE + "=?",
                                                    new String[]{bpnQuality.getQualityCode()},
                                                    null, null, null, null);
                                            if (penaltyRow.getIsLoading() == 0) {
                                                penalty =  bpnQuality.getQuantity();
                                            } else {
                                                penalty = 0;
                                            }
                                            penaltyResult += penalty;
//                                        penalty = penalty + bpnQuality.getQuantity();
//										BPNQuality bpnQ = (BPNQuality) database.getDataFirst(false, BPNQuality.TABLE_NAME, null,
//												BPNQuality.XML_BPN_ID+ "=?",
//												new String[]{bpnId},
//												null, null, null, null);
//										Penalty penaltyRow = (Penalty) database.getDataFirst(false, Penalty.TABLE_NAME, null,
//												Penalty.XML_PENALTY_CODE+ "=?",
//												new String[]{bpnQ.getQualityCode()},
//												null, null, null, null);
//
//										if(penaltyRow.getIsLoading()==0){
//											qtyJanjangRemaining = qtyJanjang - penalty;
//										}else{
//											qtyJanjangRemaining = qtyJanjang;
//										}
//					Log.d("tag", ":" + bpnQuality.getQualityCode() + ":" + bpnQuality.getQuantity());
                                            database.setData(bpnQuality);

                                        }
                                        qtyJanjangRemaining = qtyJanjang - penaltyResult;
                                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                                nikHarvester, crop, qtyJanjangCode, qtyJanjang, qtyJanjangRemaining, 0, createddate, clerk, modifieddate, clerk, modifieddatestr));

                                        database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                                                nikHarvester, crop, qtyLooseFruitCode, qtyLooseFruit, qtyLooseFruit, 0, createddate, clerk, modifieddate, clerk, modifieddatestr));


                                        ForemanActive foremanActive = new ForemanActive(0, "", "", 0, 0, nikForeman, foreman, "", "", "", "", gang, "", "", "", "");
                                        database.deleteData(ForemanActive.TABLE_NAME, null, null);
                                        database.setData(foremanActive);

                                        database.commitTransaction();

                                        init = true;
                                        new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                                                getResources().getString(R.string.save_successed), false).show();
                                    } catch (SQLiteException e) {
                                        e.printStackTrace();
                                        database.closeTransaction();
                                        init = false;
                                        new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                                                e.getMessage(), false).show();
                                    } finally {
                                        database.closeTransaction();
                                    }
                                }
                            } else {
                                init = false;
                                new DialogNotification(BPNActivity.this, "Information", "Gang must contain HC", false).show();
                            }

                        } else {
                            init = false;
                            new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                                    getResources().getString(R.string.buku_panen_quality_invalid), false).show();
                        }
                    } else {
                        init = false;
                        new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.Invalid_data), false).show();
                    }
                } else {
                    init = true;
                }
                break;
            case R.id.imgHarvestBookPhoto:
                Intent intent = new Intent(BPNActivity.this, CameraActivity.class);
                intent.putExtra(BPNHeader.XML_PHOTO, photo);
                startActivityForResult(intent, REQUEST_CAMERA);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == MST_FOREMAN) {
                foreman = data.getExtras().getString(Employee.XML_NAME);
                nikForeman = data.getExtras().getString(Employee.XML_NIK);
//				gangForeman = data.getExtras().getString(Employee.XML_GANG);

                txtHarvestBookForeman.setText(foreman);
            } else if (requestCode == MST_CLERK) {
                clerk = data.getExtras().getString(Employee.XML_NAME);
                nikClerk = data.getExtras().getString(Employee.XML_NIK);

                txtHarvestBookClerk.setText(clerk);
            } else if (requestCode == MST_BLOCK) {
//				block = data.getExtras().getString(BlockHdrc.XML_BLOCK);
                block = data.getExtras().getString(BlockPlanning.XML_BLOCK);
                crop = "01";

                txtHarvestBookBlock.setText(block);
                txtHarvestBookCrop.setText(crop);
            } else if (requestCode == MST_HARVESTER) {
                harvester = data.getExtras().getString(Employee.XML_NAME);
                nikHarvester = data.getExtras().getString(Employee.XML_NIK);

                txtHarvestBookHarvester.setText(harvester);
                edtHarvestBookTph.requestFocus();

                double qtyJanjangTot = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.JANJANG_CODE);
                double qtyLooseFruitTot = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.LOOSE_FRUIT_CODE);

                txtHarvestBookQtyJanjangTot.setText(String.valueOf(qtyJanjangTot));
                txtHarvestBookQtyLooseFruitTot.setText(String.valueOf(qtyLooseFruitTot));
            } else if (requestCode == REQUEST_CAMERA) {
                String filename = data.getExtras().getString(BPNHeader.XML_PHOTO);

                File file = new File(filename);

                if (file.exists()) {
                    try {
//						Uri uri = Uri.fromFile(file);
//						imgHarvestBookPhoto.setImageURI(uri);

                        bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                        photo = new BitmapBase64Handler().encodeTobase64(bmp);
                        bmp = new BitmapBase64Handler().decodeBase64(photo);
                        imgHarvestBookPhoto.setImageBitmap(bmp);

                        file.delete();

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == MST_GANG) {
                gang = data.getExtras().getString(Gang.XML_GANG_CODE);
                txtHarvestBookGang.setText(gang);
            }
        }
    }

    @Override
    public void onBackPressed() {
//		super.onBackPressed();

        new DialogConfirm(BPNActivity.this, getResources().getString(R.string.informasi),
                getResources().getString(R.string.exit), null, 1).show();
    }

    public void updateGpsKoordinat(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }

        txtHarvestBookGpsKoordinat.setText(gpsKoordinat);
    }

    @Override
    public void onConfirmOK(Object object, int btnId) {

        switch (btnId) {
            case R.id.btnActionBarRight:
                long todayDate = new Date().getTime();

                try {
                    database.openTransaction();
//				database.updateData(new BPNHeader(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph, 
//						nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, useGerdang, crop, gpsKoordinat, status, 
//						todayDate, clerk, todayDate, clerk), 
//						BPNHeader.XML_ID + "=?" + " and " +
//						BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//						BPNHeader.XML_ESTATE + "=?" + " and " +
//						BPNHeader.XML_BPN_DATE + "=?" + " and " +
//						BPNHeader.XML_DIVISION + "=?" + " and " +  
//						BPNHeader.XML_GANG + "=?" + " and " +
//						BPNHeader.XML_LOCATION + "=?" + " and " +
//						BPNHeader.XML_TPH + "=?" + " and " + 
//						BPNHeader.XML_NIK_HARVESTER + "=?", 
//						new String [] {bpnId, companyCode, estate, bpnDate, division, gang, block, tph, nikHarvester});

                    bpnHeader = new BPNHeader(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, harvester, nikForeman, foreman, nikClerk, clerk, useGerdang, crop, gpsKoordinat, photo, status, spbsNumber,
                            todayDate, clerk, todayDate, clerk);

                    database.updateData(bpnHeader,
                            BPNHeader.XML_BPN_ID + "=?",
                            new String[]{bpnId});

//				database.deleteData(BPNQuantity.TABLE_NAME, 
//						BPNQuantity.XML_ID + "=?" + " and " +
//						BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
//						BPNQuantity.XML_ESTATE + "=?" + " and " +
//						BPNQuantity.XML_BPN_DATE + "=?" + " and " +
//						BPNQuantity.XML_DIVISION + "=?" + " and " +
//						BPNQuantity.XML_GANG + "=?" + " and " +
//						BPNQuantity.XML_LOCATION + "=?" + " and " +
//						BPNQuantity.XML_TPH + "=?" + " and " + 
//						BPNQuantity.XML_NIK_HARVESTER + "=?", 
//						new String [] {bpnId, companyCode, estate, bpnDate, division, gang, block, tph, nikHarvester});

                    database.deleteData(BPNQuantity.TABLE_NAME,
                            BPNQuantity.XML_BPN_ID + "=?",
                            new String[]{bpnId});

//				database.deleteData(BPNQuality.TABLE_NAME, 
//						BPNQuality.XML_ID + "=?" + " and " +
//						BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
//						BPNQuality.XML_ESTATE + "=?" + " and " +  
//						BPNQuality.XML_BPN_DATE + "=?" + " and " +
//						BPNQuality.XML_DIVISION + "=?" + " and " +
//						BPNQuality.XML_GANG + "=?" + " and " +
//						BPNQuality.XML_LOCATION + "=?" + " and " +
//						BPNQuality.XML_TPH + "=?" + " and " +   
//						BPNQuality.XML_NIK_HARVESTER + "=?", 
//						new String [] {companyCode, estate, bpnDate, division, gang, block, tph, nikHarvester});

                    database.deleteData(BPNQuality.TABLE_NAME,
                            BPNQuality.XML_BPN_ID + "=?",
                            new String[]{bpnId});


//				database.setData(new BPNQuality(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph, 
//						nikHarvester, crop, qtyJanjangCode, qlyMentahCode, qlyMentah, status, todayDate, clerk, todayDate, clerk));
//
//				database.setData(new BPNQuality(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph, 
//						nikHarvester, crop, qtyJanjangCode, qlyBusukCode, qlyBusuk, status, todayDate, clerk, todayDate, clerk));
//
//				database.setData(new BPNQuality(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph, 
//						nikHarvester, crop, qtyJanjangCode, qlyTangkaiPanjangCode, qlyTangkaiPanjang, status, todayDate, clerk, todayDate, clerk));

                    double penalty = 0;
                    double qtyJanjangRemaining = 0;
                    double penaltyResult = 0;
                    for (Map.Entry<String, BPNQuality> entry : mapBPNQuality.entrySet()) {
                        BPNQuality bpnQuality = entry.getValue();
                        //add by adit 20161228
                        bpnQuality.setGang(gang);
                        bpnQuality.setLocation(block);
                        bpnQuality.setNikHarvester(nikHarvester);
                        bpnQuality.setCrop(crop);
                        bpnQuality.setTph(tph);

                        Penalty penaltyRow = (Penalty) database.getDataFirst(false, Penalty.TABLE_NAME, null,
                                Penalty.XML_PENALTY_CODE + "=?",
                                new String[]{bpnQuality.getQualityCode()},
                                null, null, null, null);
                        if (penaltyRow.getIsLoading() == 0) {
                            penalty =  bpnQuality.getQuantity();
                        } else {
                            penalty = 0;
                        }
                        penaltyResult += penalty;
//					Log.d("tag", ":" + bpnQuality.getQualityCode() + ":" + bpnQuality.getQuantity());
                        database.setData(bpnQuality);
                    }
                    qtyJanjangRemaining = qtyJanjang - penaltyResult;
                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyJanjangCode, qtyJanjang, qtyJanjangRemaining, status, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                    database.setData(new BPNQuantity(0, bpnId, imei, companyCode, estate, bpnDate, division, gang, block, tph,
                            nikHarvester, crop, qtyLooseFruitCode, qtyLooseFruit, qtyLooseFruit, status, todayDate, clerk, todayDate, clerk, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)));

                    ForemanActive foremanActive = new ForemanActive(0, "", "", 0, 0, nikForeman, foreman, "", "", "", "", gang, "", "", "", "");
                    database.deleteData(ForemanActive.TABLE_NAME, null, null);
                    database.setData(foremanActive);

                    database.commitTransaction();

                    init = true;
                    new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_successed), false).show();
                } catch (SQLiteConstraintException e) {
                    e.printStackTrace();
                    database.closeTransaction();
                    new DialogNotification(BPNActivity.this, getResources().getString(R.string.informasi), e.getMessage(), false).show();
                } finally {
                    database.closeTransaction();
                }
                break;
            default:
                if (gps != null) {
                    gps.stopUsingGPS();
                }

                gpsHandler.stopGPS();

                finish();
                animOnFinish();
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean arg1) {
        EditText edt = (EditText) view;

        edt.setSelection(edt.getText().toString().length());
    }

    private String getLocation() {
        String gpsKoordinat = "0.0:0.0";

        if (gps != null) {
            Location location = gps.getLocation();
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                gpsKoordinat = latitude + ":" + longitude;
            }
        }

        return gpsKoordinat;
    }

    @Override
    public void onOK(boolean is_finish) {
        if (isEdit) {
            Intent intent = new Intent(BPNActivity.this, BPNHistoryActivity.class);
            intent.putExtra(BPNHeader.TABLE_NAME, bpnHeader);

            setResult(RESULT_OK, intent);
            finish();
        } else {
            if (init) {
                bpnId = "";
                nikHarvester = "";
                harvester = "";
                tph = "";
                qtyJanjang = 0;
                qtyLooseFruit = 0;
                qlyMentah = 0;
                qlyBusuk = 0;
                qlyTangkaiPanjang = 0;
                gpsKoordinat = getLocation();
                useGerdang = 0;
                photo = "";
                initInputView();
            } else {
                edtHarvestBookTph.requestFocus();
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        useGerdang = (isChecked) ? 1 : 0;

        Log.d("tag", ":" + useGerdang);
    }

    private double getQty(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String achievementCode) {
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,

                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_GANG + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                new String[]{companyCode, estate, division, gang, bpnDate, nikHarvester, achievementCode},
                null, null, null, null);
        database.closeTransaction();

        if (lstQty.size() > 0) {
            for (int i = 0; i < lstQty.size(); i++) {
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }

    private void AddChild(final BPNQuality bpnQuality, final Penalty penalty) {
        final TextView txtPenaltyItemName;
        final EditTextCustom edtPenaltyItemCount;
        final TextView txtPenaltyItemUom;

        final View child = getLayoutInflater().inflate(R.layout.item_penalty, null);

        txtPenaltyItemName = (TextView) child.findViewById(R.id.txtPenaltyItemName);
        edtPenaltyItemCount = (EditTextCustom) child.findViewById(R.id.edtPenaltyItemCount);
        txtPenaltyItemUom = (TextView) child.findViewById(R.id.txtPenaltyItemUom);

        txtPenaltyItemName.setText(penalty.getPenaltyDesc());
        edtPenaltyItemCount.setText(bpnQuality.getQuantity() > 0 ? String.valueOf((int) bpnQuality.getQuantity()) : "");
        txtPenaltyItemUom.setText(penalty.getUom());

        if (!TextUtils.isEmpty(spbsNumber)) {
            setViewAndChildrenEnabled(edtPenaltyItemCount, false);
        } else {
            setViewAndChildrenEnabled(edtPenaltyItemCount, true);
        }

        edtPenaltyItemCount.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String qty = editable.toString().trim();
                bpnQuality.setQuantity(new Converter(qty).StrToDouble());
            }
        });

        mapBPNQuality.put(penalty.getPenaltyCode(), bpnQuality);
        lytBpnPenalty.addView(child);
    }

    @Override
    public void onDateOK(Date date, int id) {
        bpnDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
        txtHarvestBookHarvestDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    //add by adit
    private boolean ValidateBPNDate(String _bpnDate) {
        boolean isValid = true;
        try {
            Calendar cal = Calendar.getInstance();
            Date curr_date = cal.getTime();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date BPNDate = df.parse(_bpnDate);

            long result = curr_date.getTime() - BPNDate.getTime();
            long sec = result / 1000;
            long minutes = sec / 60;
            long hour = minutes / 60;
            long day = hour / 24;
            if (day > 1 || day < 0) {
                isValid = false;
            }

        } catch (ParseException ex) {
            ex.printStackTrace();
        }


        return isValid;
    }
}