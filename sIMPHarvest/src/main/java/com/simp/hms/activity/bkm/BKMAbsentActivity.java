package com.simp.hms.activity.bkm;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.master.MasterAbsentTypeActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.master.MasterEmployeeGangActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.handler.GpsHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.AbsentType;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.Employee;
import com.simp.hms.model.Gang;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.routines.Utils;
import com.simp.hms.service.GPSService;
import com.simp.hms.service.GPSTriggerService;
import com.simp.hms.widget.EditTextCustom;

import android.app.ActionBar;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class BKMAbsentActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, DialogConfirmListener /*, OnItemClickListener */ {
    private Toolbar tbrMain;
    private TextView txtBkmAbsentDate;
    private TextView txtBkmAbsentGang;
    private TextView txtBkmAbsentForeman;
    private TextView txtBkmAbsentClerk;
    private ScrollView sclBkmAbsent;
    private LinearLayout lytBkmAbsentHarvester;
    private Button btnBkmAbsentAddHarvester;
    private EditText edtBKMAbsentDummy;

    private HashMap<String, BKMLine> mapBKMLine = new HashMap<String, BKMLine>();

    GPSService gps;
    GpsHandler gpsHandler;

    private final int MST_FOREMAN = 101;
    private final int MST_CLERK = 102;
    private final int MST_HARVESTER = 103;
    private final int MST_GANG = 104;
    private final int MST_ABS = 105;
    private final int MST_ABS_TYPE = 106;

    String imei = "";
    String companyCode = "";
    String estate = "";
    String bkmDate = "";
    String division = "";
    String gang = "";
    String nikForeman = "";
    String foreman = "";
    String nikClerk = "";
    String clerk = "";
    String gpsKoordinat = "0.0:0.0";
    int status = 0;
    double latitude = 0.0;
    double longitude = 0.0;

    private Handler repeatUpdateHandler = new Handler();
    private boolean mAutoIncrement = false;
    private boolean mAutoDecrement = false;

    String nikSelected = "";
    View childSelected = null;
    TextView txtBkmAbsentItemAbsentTypeSelected = null;
    EditText edtBkmAbsentItemMandaysSelected = null;

    double absentMin = 0.01;
    double absentMax = 1;
    double absentDefault = 1;

    boolean init = false;
    boolean add = false;

    DatabaseHandler database = new DatabaseHandler(BKMAbsentActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_bkm_absent);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        txtBkmAbsentDate = (TextView) findViewById(R.id.txtBkmAbsentDate);
        txtBkmAbsentForeman = (TextView) findViewById(R.id.txtBkmAbsentForeman);
        txtBkmAbsentGang = (TextView) findViewById(R.id.txtBkmAbsentGang);
        txtBkmAbsentClerk = (TextView) findViewById(R.id.txtBkmAbsentClerk);
        sclBkmAbsent = (ScrollView) findViewById(R.id.sclBkmAbsent);
        lytBkmAbsentHarvester = (LinearLayout) findViewById(R.id.lytBkmAbsentHarvester);
        btnBkmAbsentAddHarvester = (Button) findViewById(R.id.btnBkmAbsentAddHarvester);
        edtBKMAbsentDummy = (EditText) findViewById(R.id.edtBKMAbsentDummy);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.bkm_absent));
        btnActionBarRight.setOnClickListener(this);
        btnActionBarRight.setText(getResources().getString(R.string.save));

        txtBkmAbsentGang.setOnClickListener(this);
        txtBkmAbsentClerk.setOnClickListener(this);
        btnBkmAbsentAddHarvester.setOnClickListener(this);

        gps = new GPSService(BKMAbsentActivity.this);
        gpsHandler = new GpsHandler(BKMAbsentActivity.this);

        GPSTriggerService.bkmAbsentActivity = this;

        gpsHandler.startGPS();

        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }

        gpsKoordinat = String.valueOf(latitude) + ":" + String.valueOf(longitude);

        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if (userLogin != null) {
            companyCode = userLogin.getCompanyCode();
            estate = userLogin.getEstate();
            division = userLogin.getDivision();
            gang = userLogin.getGang();
            nikForeman = userLogin.getNik();
            foreman = userLogin.getName();
        }

        imei = new DeviceHandler(BKMAbsentActivity.this).getImei();
        bkmDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);

        database.openTransaction();
        Employee employee = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
                Employee.XML_COMPANY_CODE + "=?" + " and " +
                        Employee.XML_ESTATE + "=?" + " and " +
                        Employee.XML_DIVISION + "=?" + " and " +
                        Employee.XML_GANG + "=?" + " and " +
                        Employee.XML_ROLE_ID + "=?" + " and " +
                        Employee.XML_NIK + "<>?",
                new String[]{companyCode, estate, division, gang, "Leader", nikForeman},
                null, null, null, null);
        database.closeTransaction();

        if (employee != null) {
            nikClerk = employee.getNik();
            clerk = employee.getName();
        }

        database.openTransaction();
        BKMHeader bkmHeader = (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null,
                BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                        BKMHeader.XML_ESTATE + "=?" + " and " +
                        BKMHeader.XML_BKM_DATE + "=?" + " and " +
                        BKMHeader.XML_DIVISION + "=?",
                new String[]{companyCode, estate, bkmDate, division},
                null, null, null, null);
        database.closeTransaction();

        if (bkmHeader != null) {
            nikClerk = bkmHeader.getNikClerk();
            clerk = bkmHeader.getClerk();
            gang = bkmHeader.getGang();
        }

        setMinMaxAbsentType("KJ");
        initInputView();
        getHarvesters();

        edtBKMAbsentDummy.requestFocus();

//		database.openTransaction();
//		database.deleteData(BKMHeader.TABLE_NAME, null, null);
//		database.deleteData(BKMLine.TABLE_NAME, null, null);
//		database.deleteData(BKMOutput.TABLE_NAME, null, null);
//		database.commitTransaction();  
//		database.closeTransaction();
    }

    @Override
    protected void onStart() {
        super.onStart();

//		getHarvesters();
    }

    private void initInputView() {
        txtBkmAbsentDate.setText(new DateLocal(bkmDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtBkmAbsentForeman.setText(foreman);
        txtBkmAbsentGang.setText(gang);
        txtBkmAbsentClerk.setText(clerk);
    }


    private void getHarvesters() {
//		mapBKMLine = new HashMap<String, BKMLine>();
        lytBkmAbsentHarvester.removeAllViews();
        mapBKMLine.clear();

        database.openTransaction();
        BKMHeader bkmHeader = (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null,
                BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                        BKMHeader.XML_ESTATE + "=?" + " and " +
                        BKMHeader.XML_BKM_DATE + "=?" + " and " +
                        BKMHeader.XML_DIVISION + "=?" + " and " +
                        BKMHeader.XML_GANG + "=?",
                new String[]{companyCode, estate, bkmDate, division, gang},
                null, null, null, null);
        database.closeTransaction();

//		database.openTransaction();
//		BKMHeader bkmHeader = (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null, 
//				BKMHeader.XML_COMPANY_CODE + "=?" + " and " + 
//				BKMHeader.XML_ESTATE + "=?" + " and " +
//				BKMHeader.XML_BKM_DATE + "=?" + " and " + 
//				BKMHeader.XML_DIVISION + "=?", 
//				new String [] {companyCode, estate, bkmDate, division},
//				null, null, null, null);
//		database.closeTransaction();

        if (bkmHeader != null) {
            nikClerk = bkmHeader.getNikClerk();
            clerk = bkmHeader.getClerk();
            status = bkmHeader.getStatus();
            gang = bkmHeader.getGang();

            txtBkmAbsentGang.setText(gang);

            txtBkmAbsentClerk.setText(clerk);

            database.openTransaction();
            List<Object> listObject = database.getListData(false, BKMLine.TABLE_NAME, null,
                    BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                            BKMLine.XML_ESTATE + "=?" + " and " +
                            BKMLine.XML_BKM_DATE + "=?" + " and " +
                            BKMLine.XML_DIVISION + "=?" + " and " +
                            BKMLine.XML_GANG + "=?",
                    new String[]{companyCode, estate, bkmDate, division, gang},
                    null, null, BKMLine.XML_NIK, null);
            database.closeTransaction();

            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    BKMLine bkmLine = (BKMLine) listObject.get(i);

                    Log.d("tag", "e:" + bkmLine.getGang() + ":" + bkmLine.getNik() + ":" + bkmLine.getName());

                    addChild(bkmLine);
                }
            }
        } else {
            nikClerk = "";
            clerk = "";
            txtBkmAbsentClerk.setText(clerk);
            String roleId = "MEMBER";

            database.openTransaction();
            List<Object> listObject = database.getListData(false, Employee.TABLE_NAME, null,
                    Employee.XML_COMPANY_CODE + "=?" + " and " +
                            Employee.XML_ESTATE + "=?" + " and " +
                            Employee.XML_DIVISION + "=?" + " and " +
                            Employee.XML_GANG + "=?" + " and " +
                            Employee.XML_ROLE_ID + "=?",
                    new String[]{companyCode, estate, division, gang, roleId},
                    null, null, Employee.XML_NIK, null);
            database.closeTransaction();

            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    Employee employee = (Employee) listObject.get(i);

                    if (employee.getNik() != nikClerk && employee.getNik() != nikForeman) {
                        BKMLine bkmLine = new BKMLine(0, imei, companyCode, estate, bkmDate, division, gang, employee.getNik(),
                                employee.getName(), "KJ", absentDefault, "D", 0, 0, 0, "", 0, "");


                        addChild(bkmLine);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> listNikFilter = new ArrayList<String>();

        switch (view.getId()) {
            case R.id.txtBkmAbsentGang:
                startActivityForResult(new Intent(BKMAbsentActivity.this, MasterEmployeeGangActivity.class)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, division),
                        MST_GANG);
                break;
            case R.id.txtBkmAbsentClerk:
                listNikFilter.add(nikForeman);

                startActivityForResult(new Intent(BKMAbsentActivity.this, MasterEmployeeActivity.class)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(Constanta.TYPE, EmployeeType.CLERK.getId())
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, division)
                                .putExtra(Employee.XML_NIK, listNikFilter)
                                .putExtra("Activity", "BKM"),
                        MST_CLERK);
                break;
            case R.id.btnBkmAbsentAddHarvester:
                if (mapBKMLine.size() > 0) {
                    for (Map.Entry<String, BKMLine> entry : mapBKMLine.entrySet()) {
                        String nik = entry.getKey();

                        listNikFilter.add(nik);
                    }
                }

                startActivityForResult(new Intent(BKMAbsentActivity.this, MasterEmployeeActivity.class)
                                .putExtra(Constanta.SEARCH, true)
                                .putExtra(Constanta.TYPE, EmployeeType.HARVESTER_ABSENT.getId())
                                .putExtra(Employee.XML_COMPANY_CODE, companyCode)
                                .putExtra(Employee.XML_ESTATE, estate)
                                .putExtra(Employee.XML_DIVISION, division)
                                .putExtra(Employee.XML_NIK, listNikFilter)
                                .putExtra("Activity", "BKM"),
                        MST_HARVESTER);
                break;
            case R.id.btnActionBarRight:
                if (!TextUtils.isEmpty(nikClerk)) {
                    if (mapBKMLine.size() > 0) {

                        database.openTransaction();
                        BKMHeader bkmHeader = (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null,
                                BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        BKMHeader.XML_ESTATE + "=?" + " and " +
                                        BKMHeader.XML_BKM_DATE + "=?" + " and " +
                                        BKMHeader.XML_DIVISION + "=?" + " and " +
                                        BKMHeader.XML_GANG + "=?",
                                new String[]{companyCode, estate, bkmDate, division, gang},
                                null, null, null, null);
                        database.closeTransaction();

                        if (bkmHeader != null) {
                            if (bkmHeader.getStatus() == 1) {
                                init = false;
                                new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                                        getResources().getString(R.string.data_already_export), false).show();
                            } else {
                                init = false;
                                new DialogConfirm(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                                        getResources().getString(R.string.already_exists_replaced), null, R.id.btnActionBarRight).show();
                            }
                        } else {
                            clearBKMAbsent();

                            try {
                                long todayDate = new Date().getTime();

                                database.openTransaction();
                                database.deleteData(BKMLine.TABLE_NAME,
                                        BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMLine.XML_ESTATE + "=?" + " and " +
                                                BKMLine.XML_BKM_DATE + "=?" + " and " +
                                                BKMLine.XML_DIVISION + "=?" + " and " +
                                                BKMLine.XML_GANG + "=?",
                                        new String[]{companyCode, estate, bkmDate, division, gang});
                                //
                                database.setData(new BKMHeader(0, imei, companyCode, estate, bkmDate, division, gang,
                                        nikForeman, foreman, nikClerk, clerk, gpsKoordinat, status, todayDate, foreman, todayDate, foreman));

                                for (Map.Entry<String, BKMLine> entry : mapBKMLine.entrySet()) {
                                    BKMLine bkmLine = entry.getValue();

                                    bkmLine.setCreatedDate(todayDate);
                                    bkmLine.setCreatedBy(foreman);
                                    bkmLine.setModifiedDate(todayDate);
                                    bkmLine.setModifiedBy(foreman);

                                    database.setData(bkmLine);
                                }
                                database.commitTransaction();

                                init = true;
                                new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                                        getResources().getString(R.string.save_successed), false).show();
                            } catch (SQLiteException e) {
                                e.printStackTrace();
                                database.closeTransaction();
                                init = false;
                                new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                                        e.getMessage(), false).show();
                            } finally {
                                database.closeTransaction();
                            }
                        }
                    } else {
                        new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                                getResources().getString(R.string.data_empty), false).show();
                    }
                } else {
                    new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.bkm_clerk_empty), false).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == MST_GANG) {
                gang = data.getExtras().getString(Gang.XML_GANG_CODE);
                txtBkmAbsentGang.setText(gang);

                add = false;
                getHarvesters();
            } else if (requestCode == MST_CLERK) {
                nikClerk = data.getExtras().getString(Employee.XML_NIK);
                clerk = data.getExtras().getString(Employee.XML_NAME);
                txtBkmAbsentClerk.setText(clerk);
            } else if (requestCode == MST_HARVESTER) {
                String nik = data.getExtras().getString(Employee.XML_NIK);
                String name = data.getExtras().getString(Employee.XML_NAME);

                setMinMaxAbsentType("KJ");
                BKMLine bkmLine = new BKMLine(0, imei, companyCode, estate, bkmDate, division, gang, nik, name, "KJ", absentDefault, "D", 0, 0, 0, "", 0, "");

                add = true;
                addChild(bkmLine);
            } else if (requestCode == MST_ABS_TYPE) {
                String absentType = data.getExtras().getString(AbsentType.XML_ABSENT_TYPE);
                absentMin = Utils.round(new Converter(data.getExtras().getString(AbsentType.XML_HKRLLO)).StrToDouble(), 2);
                absentMax = Utils.round(new Converter(data.getExtras().getString(AbsentType.XML_HKRLHI)).StrToDouble(), 2);
                absentDefault = Utils.round(new Converter(data.getExtras().getString(AbsentType.XML_HKVLHI)).StrToDouble(), 2);

//				Log.d("tag", absentType);
                if (nikSelected != null && txtBkmAbsentItemAbsentTypeSelected != null) {
                    BKMLine bkmLine = (BKMLine) mapBKMLine.get(nikSelected);

                    bkmLine.setAbsentType(absentType);
                    bkmLine.setMandays(absentDefault);

                    mapBKMLine.put(nikSelected, bkmLine);
                    txtBkmAbsentItemAbsentTypeSelected.setText(absentType);
                    edtBkmAbsentItemMandaysSelected.setText(String.valueOf(Utils.round(absentDefault, 2)));
                }
            }
        }
    }

    private void addChild(final BKMLine bkmLine) {
        final TextView txtBkmAbsentItemName;
        final TextView txtBkmAbsentItemAbsentType;
        final Button btnBKMAbsentItemMandaysMin;
        final EditTextCustom edtBkmAbsentItemMandays;
        final Button btnBKMAbsentItemMandaysPlus;
        final Button btnBkmAbsentItemDelete;
        final TextView txtBkmAbsentItemNumber;
        final CheckBox cbxBKMAbsentItemUseGerdang;

        final View child = getLayoutInflater().inflate(R.layout.item_bkm_absent, null);

        txtBkmAbsentItemName = (TextView) child.findViewById(R.id.txtBkmAbsentItemName);
        txtBkmAbsentItemAbsentType = (TextView) child.findViewById(R.id.txtBkmAbsentItemAbsentType);
        btnBKMAbsentItemMandaysMin = (Button) child.findViewById(R.id.btnBKMAbsentItemMandaysMinus);
        edtBkmAbsentItemMandays = (EditTextCustom) child.findViewById(R.id.edtBkmAbsentItemMandays);
        btnBKMAbsentItemMandaysPlus = (Button) child.findViewById(R.id.btnBKMAbsentItemMandaysPlus);
        btnBkmAbsentItemDelete = (Button) child.findViewById(R.id.btnBkmAbsentItemDelete);
        txtBkmAbsentItemNumber = (TextView) child.findViewById(R.id.txtBkmAbsentItemNumber);
        cbxBKMAbsentItemUseGerdang = (CheckBox) child.findViewById(R.id.cbxBKMAbsentItemUseGerdang);

        txtBkmAbsentItemName.setText(bkmLine.getName());
        txtBkmAbsentItemAbsentType.setText(bkmLine.getAbsentType());
        edtBkmAbsentItemMandays.setText(String.valueOf(Utils.round(bkmLine.getMandays(), 2)));
        cbxBKMAbsentItemUseGerdang.setChecked(bkmLine.getUseGerdang() == 1 ? true : false);

        txtBkmAbsentItemName.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                edtBkmAbsentItemMandays.requestFocus();
            }
        });

        txtBkmAbsentItemAbsentType.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                nikSelected = bkmLine.getNik();
                txtBkmAbsentItemAbsentTypeSelected = txtBkmAbsentItemAbsentType;
                edtBkmAbsentItemMandaysSelected = edtBkmAbsentItemMandays;

                startActivityForResult(new Intent(BKMAbsentActivity.this, MasterAbsentTypeActivity.class)
                                .putExtra(Constanta.SEARCH, true),
                        MST_ABS_TYPE);
            }
        });

        btnBKMAbsentItemMandaysMin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                String type = bkmLine.getAbsentType();
                decrement(edtBkmAbsentItemMandays, bkmLine);
            }
        });

        btnBKMAbsentItemMandaysMin.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                mAutoDecrement = true;
                repeatUpdateHandler.post(new RptUpdater(edtBkmAbsentItemMandays, bkmLine));

                return false;
            }
        });

        btnBKMAbsentItemMandaysMin.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) && mAutoDecrement) {
                    mAutoDecrement = false;
                }
                return false;
            }
        });

        btnBKMAbsentItemMandaysPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                increment(edtBkmAbsentItemMandays, bkmLine);
            }
        });

        btnBKMAbsentItemMandaysPlus.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                mAutoIncrement = true;
                repeatUpdateHandler.post(new RptUpdater(edtBkmAbsentItemMandays, bkmLine));

                return false;
            }
        });

        btnBKMAbsentItemMandaysPlus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) && mAutoIncrement) {
                    mAutoIncrement = false;
                }
                return false;
            }
        });

        btnBkmAbsentItemDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                edtBkmAbsentItemMandays.clearFocus();

                database.openTransaction();
                BKMOutput bkmOutput = (BKMOutput) database.getDataFirst(false, BKMOutput.TABLE_NAME, null,
                        BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                BKMOutput.XML_ESTATE + "=?" + " and " +
                                BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                BKMOutput.XML_DIVISION + "=?" + " and " +
                                BKMOutput.XML_GANG + "=?" + " and " +
                                BKMOutput.XML_NIK + "=?",
                        new String[]{companyCode, estate, bkmDate, division, gang, bkmLine.getNik()},
                        null, null, null, null);
                database.closeTransaction();

                if (bkmOutput != null) {
                    new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.data_delete_cannot), false).show();
                } else {
                    new DialogConfirm(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                            String.format(getResources().getString(R.string.data_delete_2), bkmLine.getName()),
                            null, R.id.btnBkmAbsentItemDelete).show();

                    nikSelected = bkmLine.getNik();
                    childSelected = child;
                }
            }
        });

        edtBkmAbsentItemMandays.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View arg0, boolean arg1) {
                setMinMaxAbsentType(bkmLine.getAbsentType());

                String value = edtBkmAbsentItemMandays.getText().toString().trim();
                double mandays = validValue(new Converter(value).StrToDouble());

                value = String.valueOf(mandays);

                bkmLine.setMandays(mandays);

                edtBkmAbsentItemMandays.setText(value);

                try {
                    edtBkmAbsentItemMandays.setSelection(value.length());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cbxBKMAbsentItemUseGerdang.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                bkmLine.setUseGerdang(isChecked ? 1 : 0);
                mapBKMLine.put(bkmLine.getNik(), bkmLine);
            }
        });

        edtBkmAbsentItemMandays.addTextChangedListener(new AbsoluteEditTextWatcher(edtBkmAbsentItemMandays, bkmLine));

        mapBKMLine.put(bkmLine.getNik(), bkmLine);
        lytBkmAbsentHarvester.addView(child);

        txtBkmAbsentItemNumber.setText(String.valueOf(lytBkmAbsentHarvester.getChildCount()));

        if (add) {
            sendScroll();
        }
    }

    class RptUpdater implements Runnable {
        private EditText edt;
        private BKMLine bkmLine;

        public RptUpdater(EditText edt, BKMLine bkmAbsent) {
            this.edt = edt;
            this.bkmLine = bkmAbsent;
        }

        public void run() {
            if (mAutoIncrement) {
                increment(edt, bkmLine);
                repeatUpdateHandler.postDelayed(new RptUpdater(edt, bkmLine), 200);
            } else if (mAutoDecrement) {
                decrement(edt, bkmLine);
                repeatUpdateHandler.postDelayed(new RptUpdater(edt, bkmLine), 200);
            }
        }
    }

    public void increment(EditText edt, BKMLine bkmLine) {
        double mValue;
        String value = edt.getText().toString().trim();

        setMinMaxAbsentType(bkmLine.getAbsentType());

        if (new Converter(value).StrToDouble() < absentMax) {
            mValue = new Converter(new DecimalFormat("##.##").format(new Converter(edt.getText().toString().trim()).StrToDouble() + 0.1)).StrToDouble();
            mValue = validValue(mValue);
        } else {
            mValue = absentMax;
        }

        edt.setText(String.valueOf(mValue));
        bkmLine.setMandays(mValue);
        mapBKMLine.put(bkmLine.getNik(), bkmLine);
    }

    public void decrement(EditText edt, BKMLine bkmLine) {
        double mValue;
        String value = edt.getText().toString().trim();

        setMinMaxAbsentType(bkmLine.getAbsentType());

        if (new Converter(value).StrToDouble() > absentMin) {
            mValue = new Converter(new DecimalFormat("##.##").format(new Converter(edt.getText().toString().trim()).StrToDouble() - 0.1)).StrToDouble();
            mValue = validValue(mValue);
        } else {
            mValue = absentMin;
        }

        edt.setText(String.valueOf(mValue));
        bkmLine.setMandays(mValue);
        mapBKMLine.put(bkmLine.getNik(), bkmLine);
    }

    @Override
    public void onOK(boolean is_finish) {

    }

    @Override
    public void onConfirmOK(Object object, int id) {

        switch (id) {
            case R.id.btnBkmAbsentItemDelete:
                if (!TextUtils.isEmpty(nikSelected) && childSelected != null) {
                    lytBkmAbsentHarvester.removeView(childSelected);
                    BKMLine bkmLine = mapBKMLine.remove(nikSelected);

                    if (lytBkmAbsentHarvester instanceof ViewGroup) {
                        ViewGroup viewGroup = (ViewGroup) lytBkmAbsentHarvester;
                        for (int i = 0; i < viewGroup.getChildCount(); i++) {
                            View child = viewGroup.getChildAt(i);

                            TextView txt = (TextView) child.findViewById(R.id.txtBkmAbsentItemNumber);

                            txt.setText(String.valueOf(i + 1));
                        }
                    }
                }
                break;
            case R.id.btnActionBarRight:
                clearBKMAbsent();

                try {
                    long todayDate = new Date().getTime();

                    database.openTransaction();
                    database.updateData(new BKMHeader(0, imei, companyCode, estate, bkmDate, division, gang,
                                    nikForeman, foreman, nikClerk, clerk, gpsKoordinat, status, todayDate, foreman, todayDate, foreman),
                            BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                    BKMHeader.XML_ESTATE + "=?" + " and " +
                                    BKMHeader.XML_BKM_DATE + "=?" + " and " +
                                    BKMHeader.XML_DIVISION + "=?" + " and " +
                                    BKMHeader.XML_GANG + "=?",
                            new String[]{companyCode, estate, bkmDate, division, gang});

                    database.deleteData(BKMLine.TABLE_NAME,
                            BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                    BKMLine.XML_ESTATE + "=?" + " and " +
                                    BKMLine.XML_BKM_DATE + "=?" + " and " +
                                    BKMLine.XML_DIVISION + "=?" + " and " +
                                    BKMLine.XML_GANG + "=?",
                            new String[]{companyCode, estate, bkmDate, division, gang});

                    for (Map.Entry<String, BKMLine> entry : mapBKMLine.entrySet()) {
                        BKMLine bkmLine = entry.getValue();


                        Log.d("tag", bkmLine.getGang() + ":" + bkmLine.getNik() + ":" + bkmLine.getName() + ":" + bkmLine.toString());

                        bkmLine.setCreatedDate(todayDate);
                        bkmLine.setCreatedBy(foreman);
                        bkmLine.setModifiedDate(todayDate);
                        bkmLine.setModifiedBy(foreman);

                        database.setData(bkmLine);
                    }

                    database.commitTransaction();

                    new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                            getResources().getString(R.string.save_successed), false).show();
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();
                    init = false;
                    new DialogNotification(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                            e.getMessage(), false).show();
                } finally {
                    database.closeTransaction();
                }
                break;
            default:
                if (gps != null) {
                    gps.stopUsingGPS();
                }

                gpsHandler.stopGPS();

                finish();
                animOnFinish();
                break;
        }
    }

    public void updateGpsKoordinat(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            gpsKoordinat = latitude + ":" + longitude;
        }
    }

    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        sclBkmAbsent.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        }).start();
    }

    private class AbsoluteEditTextWatcher implements TextWatcher {
        private EditText edt;
        private BKMLine bkmLine;

        String before = "";
        String after = "";
        String on = "";

        public AbsoluteEditTextWatcher(EditText edt, BKMLine bkmLine) {
            this.edt = edt;
            this.bkmLine = bkmLine;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            before = s.toString();
            setMinMaxAbsentType(bkmLine.getAbsentType());
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            on = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {
            edt.removeTextChangedListener(this);
            after = s.toString().trim();

            if (!after.equals(".")) {
                double value = new Converter(after).StrToDouble();

                if (value >= absentMin && value <= absentMax) {
                    edt.setText(after);
                    value = validValue(new Converter(after).StrToDouble());
                } else {
                    if (TextUtils.isEmpty(after)) {
                        edt.setText(after);
                        value = validValue(new Converter(after).StrToDouble());
                    } else {
                        if (checkDot(after) > 1) {
                            edt.setText(before);
                            value = validValue(new Converter(before).StrToDouble());
                        } else {
                            if (value > absentMax) {
                                edt.setText(before);
                                value = validValue(new Converter(before).StrToDouble());
                            } else {
                                edt.setText(after);
                                value = validValue(new Converter(after).StrToDouble());
                            }
                        }
                    }
                }

                bkmLine.setMandays(value);

//				if(mapBKMLine.containsKey(bkmLine.getNik())){
                mapBKMLine.put(bkmLine.getNik(), bkmLine);
//				}
            } else {
                edt.setText(before);
            }

            edt.addTextChangedListener(this);
        }
    }

    private int checkDot(String value) {
        int found = 0;

        for (int i = 0; i < value.length(); i++) {
            char a = value.charAt(i);

            if (a == '.') {
                found++;
            }
        }

        return found;
    }

    private double validValue(double value) {

        if (value < absentMin) {
            value = absentMin;
        } else if (value > absentMax) {
            value = absentMax;
        }

        return value;
    }

    private void setMinMaxAbsentType(String type) {
        database.openTransaction();
        AbsentType absentType = (AbsentType) database.getDataFirst(false, AbsentType.TABLE_NAME, null,
                AbsentType.XML_ABSENT_TYPE + "=?",
                new String[]{type}, null, null, null, null);
        database.closeTransaction();

        if (absentType != null) {
            absentMin = Utils.round(absentType.getHkrllo(), 2);
            absentMax = Utils.round(absentType.getHkrlhi(), 2);
            absentDefault = Utils.round(absentType.getHkvlhi(), 2);
        }
    }

    @Override
    public void onBackPressed() {
//		super.onBackPressed();

        new DialogConfirm(BKMAbsentActivity.this, getResources().getString(R.string.informasi),
                getResources().getString(R.string.exit), null, 1).show();
    }

    private void clearBKMAbsent() {
        try {

            database.openTransaction();

            List<Object> lstObject = database.getListData(false, BKMHeader.TABLE_NAME, null,
                    BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BKMHeader.XML_ESTATE + "=?" + " and " +
                            BKMHeader.XML_BKM_DATE + "=?" + " and " +
                            BKMHeader.XML_DIVISION + "=?" + " and " +
                            BKMHeader.XML_NIK_FOREMAN + "=?" + " and " +
                            BKMHeader.XML_GANG + "!=?",
                    new String[]{companyCode, estate, bkmDate, division, nikForeman, gang},
                    null, null, null, null);


            if (lstObject != null && lstObject.size() > 0) {
                for (int i = 0; i < lstObject.size(); i++) {
                    BKMHeader bkmHeader = (BKMHeader) lstObject.get(i);

                    database.deleteData(BKMLine.TABLE_NAME,
                            BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                    BKMLine.XML_ESTATE + "=?" + " and " +
                                    BKMLine.XML_BKM_DATE + "=?" + " and " +
                                    BKMLine.XML_DIVISION + "=?" + " and " +
                                    BKMLine.XML_GANG + "=?",
                            new String[]{companyCode, estate, bkmDate, division, bkmHeader.getGang()});

                    database.deleteData(BKMOutput.TABLE_NAME,
                            BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                    BKMOutput.XML_ESTATE + "=?" + " and " +
                                    BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                    BKMOutput.XML_DIVISION + "=?" + " and " +
                                    BKMOutput.XML_GANG + "=?",
                            new String[]{companyCode, estate, bkmDate, division, bkmHeader.getGang()});

                    database.deleteData(BKMHeader.TABLE_NAME,
                            BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                    BKMHeader.XML_ESTATE + "=?" + " and " +
                                    BKMHeader.XML_BKM_DATE + "=?" + " and " +
                                    BKMHeader.XML_DIVISION + "=?" + " and " +
                                    BKMHeader.XML_NIK_FOREMAN + "=?" + " and " +
                                    BKMHeader.XML_GANG + "=?",
                            new String[]{companyCode, estate, bkmDate, division, nikForeman, bkmHeader.getGang()});
                }
            }

            database.commitTransaction();
        } catch (SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
        } finally {
            database.closeTransaction();
        }
    }
}
