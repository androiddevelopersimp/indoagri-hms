package com.simp.hms.activity.bkm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.simp.hms.R;
import com.simp.hms.R.drawable;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBKMOutputHistory;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.UserLogin;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


public class BKMOutputHistoryActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogNotificationListener, DialogDateListener, DialogConfirmListener{
	private Toolbar tbrMain;
	private TextView txtBkmOutputHistoryBkmDate;
	private Button btnBkmOutputHistorySearch;
 	private SwipeMenuListView lsvBkmOutputHistory;
	private EditText edtBkmAbsentHistorySearch;
	
	private List<BKMOutput> lstBKMOutput;
	private AdapterBKMOutputHistory adapter;

	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	private DialogConfirm dialogConfirm;
	
	DatabaseHandler database = new DatabaseHandler(BKMOutputHistoryActivity.this);
	
	String bkmDate = "";
	
	int posSelected = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_bkm_output_history);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtBkmOutputHistoryBkmDate = (TextView) findViewById(R.id.txtBkmOutputHistoryBkmDate);
		btnBkmOutputHistorySearch = (Button) findViewById(R.id.btnBkmOutputHistorySearch);
		lsvBkmOutputHistory = (SwipeMenuListView) findViewById(R.id.lsvBkmOutputHistory);
		edtBkmAbsentHistorySearch = (EditText) findViewById(R.id.edtBkmOutputHistorySearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.bkm_output));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		txtBkmOutputHistoryBkmDate.setOnClickListener(this);
		btnBkmOutputHistorySearch.setOnClickListener(this);
		edtBkmAbsentHistorySearch.addTextChangedListener(this);
		lsvBkmOutputHistory.setOnItemClickListener(this);
		
//		getDataAsync = new GetDataAsyncTask();
//		getDataAsync.execute();
		
		bkmDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtBkmOutputHistoryBkmDate.setText(new DateLocal(bkmDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				// create "open" item
				SwipeMenuItem openItem = new SwipeMenuItem(
						getApplicationContext());
				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(
						getApplicationContext());
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				deleteItem.setIcon(R.drawable.ic_delete_pressed);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};
		
		lsvBkmOutputHistory.setMenuCreator(creator);
		lsvBkmOutputHistory.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(int pos, SwipeMenu menu, int index) {
				dialogConfirm = new DialogConfirm(BKMOutputHistoryActivity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.data_delete), null, R.id.lsvBkmOutputHistory);
				dialogConfirm.show();
				
				posSelected = pos;
				return false;
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		BKMOutput bkmOutput = (BKMOutput) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(BKMOutput.TABLE_NAME, bkmOutput);
		
		startActivity(new Intent(BKMOutputHistoryActivity.this, BKMOutputActivity.class)
				.putExtras(bundle));
	}
  
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtBkmOutputHistoryBkmDate:
			new DialogDate(BKMOutputHistoryActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtBkmOutputHistoryBkmDate).show();
			break;
		case R.id.btnBkmOutputHistorySearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;   
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:    
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BKMOutput>, List<BKMOutput>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(BKMOutputHistoryActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<BKMOutput> doInBackground(Void... voids) {
			List<BKMOutput> listTemp = new ArrayList<BKMOutput>();
			List<Object> listObject = new ArrayList<Object>();
			
			String companyCode = "";
			String estate = "";
			String division = "";
			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
//				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}
			
//			database.openTransaction();
//			BKMHeader bkmHeader =  (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null, 
//					BKMHeader.XML_COMPANY_CODE + "=?" + " and " + 
//					BKMHeader.XML_ESTATE + "=?" + " and " +
//					BKMHeader.XML_DIVISION + "=?" + " and " + 
//					BKMHeader.XML_GANG + "=?" + " and " +
//					BKMHeader.XML_NIK_FOREMAN + "=?" + " and " +
//					BKMHeader.XML_BKM_DATE + "=?", 
//					new String [] {companyCode, estate, division, gang, nik, bkmDate}, 
//					null, null, BKMHeader.XML_CREATED_DATE + " desc", null);
//			database.closeTransaction();
			
			database.openTransaction();
			BKMHeader bkmHeader =  (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null, 
					BKMHeader.XML_COMPANY_CODE + "=?" + " and " + 
					BKMHeader.XML_ESTATE + "=?" + " and " +
					BKMHeader.XML_DIVISION + "=?" + " and " + 
					BKMHeader.XML_NIK_FOREMAN + "=?" + " and " +
					BKMHeader.XML_BKM_DATE + "=?", 
					new String [] {companyCode, estate, division, nik, bkmDate}, 
					null, null, BKMHeader.XML_CREATED_DATE + " desc", null);
			database.closeTransaction();
			
			if(bkmHeader != null){
				companyCode = bkmHeader.getCompanyCode();
				estate = bkmHeader.getEstate();
				division = bkmHeader.getDivision();
				gang = bkmHeader.getGang();
				
				database.openTransaction();
				listObject = database.getListData(false, BKMOutput.TABLE_NAME, 
						new String [] {BKMOutput.XML_COMPANY_CODE, BKMOutput.XML_ESTATE, BKMOutput.XML_DIVISION, 
						BKMOutput.XML_GANG, BKMOutput.XML_BKM_DATE, BKMOutput.XML_BLOCK, BKMOutput.XML_STATUS}, 
						BKMOutput.XML_COMPANY_CODE + "=?" + " and " + 
						BKMOutput.XML_ESTATE + "=?" + " and " +
						BKMOutput.XML_DIVISION + "=?" + " and " + 
						BKMOutput.XML_GANG + "=?" + " and " +
						BKMOutput.XML_BKM_DATE + "=?", 
						new String [] {companyCode, estate, division, gang, bkmDate}, 
						BKMOutput.XML_COMPANY_CODE + ", " + BKMOutput.XML_ESTATE + ", " + BKMOutput.XML_DIVISION + ", " + 
						BKMOutput.XML_GANG + ", " + BKMOutput.XML_BKM_DATE + ", " + BKMOutput.XML_BLOCK + ", " + BKMOutput.XML_STATUS,  
						null, BKMOutput.XML_BLOCK, null);
				database.closeTransaction();
			}
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BKMOutput bkmOutput = (BKMOutput) listObject.get(i);
					
					listTemp.add(bkmOutput);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BKMOutput> lstTemp) {
			super.onPostExecute(lstTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
//			if(lstTemp.size() > 0){
				lstBKMOutput = lstTemp;
				adapter = new AdapterBKMOutputHistory(BKMOutputHistoryActivity.this, lstBKMOutput, R.layout.item_bkm_output_history);
//			}else{
//				adapter = null;
//			}
			
			lsvBkmOutputHistory.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDateOK(Date date, int id) {
		bkmDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtBkmOutputHistoryBkmDate.setText(new DateLocal(bkmDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		if(R.id.lsvBkmOutputHistory == id){
			adapter.deleteData(posSelected);
		}
	}
}
