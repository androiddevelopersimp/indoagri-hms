package com.simp.hms.activity.bkm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBKMReportHarvester;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BKMReportHarvester;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Utils;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class BKMReportHarvesterActivity extends BaseActivity implements OnItemClickListener, OnClickListener, DialogDateListener{
	private Toolbar tbrMain;
	private TextView txtBkmReportHarvesterBkmDate;
	private Button btnBkmReportHarvesterSearch;
	private ListView lsvBkmReportHarvester;
	
	private TextView txtBkmReportHarvesterTotMandays;
	private TextView txtBkmReportHarvesterTotOutput;
	
	private List<BKMReportHarvester> lstBKMReport;
	private AdapterBKMReportHarvester adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	DatabaseHandler database = new DatabaseHandler(BKMReportHarvesterActivity.this);
	
	String bkmDate = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();    

		setContentView(R.layout.activity_bkm_report_harvester);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtBkmReportHarvesterBkmDate = (TextView) findViewById(R.id.txtBkmReportHarvesterBkmDate);
		btnBkmReportHarvesterSearch = (Button) findViewById(R.id.btnBkmReportHarvesterSearch);
		lsvBkmReportHarvester = (ListView) findViewById(R.id.lsvBkmReportHarvester);
		
		txtBkmReportHarvesterTotMandays = (TextView) findViewById(R.id.txtBkmReportHarvesterTotMandays);
		txtBkmReportHarvesterTotOutput = (TextView) findViewById(R.id.txtBkmReportHarvesterTotOutput);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);
		
		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.bkm));
		btnActionBarright.setVisibility(View.INVISIBLE);
		txtBkmReportHarvesterBkmDate.setOnClickListener(this);
		btnBkmReportHarvesterSearch.setOnClickListener(this);
		lsvBkmReportHarvester.setOnItemClickListener(this);
		
		bkmDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtBkmReportHarvesterBkmDate.setText(new DateLocal(bkmDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	
		adapter = new AdapterBKMReportHarvester(BKMReportHarvesterActivity.this, new ArrayList<BKMReportHarvester>(), R.layout.item_bkm_report_harvester);
		lsvBkmReportHarvester.setAdapter(adapter);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		BKMReportHarvester bkmReport = (BKMReportHarvester) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(BKMReportHarvester.TABLE_NAME, bkmReport);
		
		startActivity(new Intent(BKMReportHarvesterActivity.this, BKMReportBlockActivity.class)
				.putExtras(bundle)
				.putExtra(BKMHeader.XML_BKM_DATE, bkmDate));
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtBkmReportHarvesterBkmDate:
			new DialogDate(BKMReportHarvesterActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtBkmReportHarvesterBkmDate).show();
			break;
		case R.id.btnBkmReportHarvesterSearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;   
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BKMReportHarvester>, List<BKMReportHarvester>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(BKMReportHarvesterActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override 
		protected List<BKMReportHarvester> doInBackground(Void... voids) {
			
			List<BKMReportHarvester> listTemp = new ArrayList<BKMReportHarvester>();
			
			String companyCode = "";
			String estate = "";
			String division = "";
			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}
			
//			database.openTransaction();   
//			BKMHeader bkmHeader =  (BKMHeader) database.getDataFirst(true, BKMHeader.TABLE_NAME, null, 
//					BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
//					BKMHeader.XML_ESTATE + "=?" + " and " +
//					BKMHeader.XML_DIVISION + "=?" + " and " +
//					BKMHeader.XML_GANG + "=?" + " and " +  
//					BKMHeader.XML_BKM_DATE + "=?" + " and " +
//					BKMHeader.XML_NIK_FOREMAN + "=?", 
//					new String [] {companyCode, estate, division, gang, bkmDate, nik},
//					null, null, null, null);
//			database.closeTransaction();
			
			database.openTransaction();   
			BKMHeader bkmHeader =  (BKMHeader) database.getDataFirst(true, BKMHeader.TABLE_NAME, null, 
					BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
					BKMHeader.XML_ESTATE + "=?" + " and " +
					BKMHeader.XML_DIVISION + "=?" + " and " +
					BKMHeader.XML_BKM_DATE + "=?" + " and " +
					BKMHeader.XML_NIK_FOREMAN + "=?", 
					new String [] {companyCode, estate, division, bkmDate, nik},
					null, null, null, null);
			database.closeTransaction();
			
//			database.openTransaction();
//			BKMHeader bkmHeader =  (BKMHeader) database.getDataFirst(true, BKMHeader.TABLE_NAME, null, 
//					null, null, null, null, null, null);
//			database.closeTransaction();
			
			if(bkmHeader != null){
				companyCode = bkmHeader.getCompanyCode();
				estate = bkmHeader.getEstate();
				division = bkmHeader.getDivision();
				gang = bkmHeader.getGang();
				bkmDate = bkmHeader.getBkmDate();
				
				database.openTransaction();
				List<Object> lstBkmLine = database.getListData(false, BKMLine.TABLE_NAME, null, 
						BKMLine.XML_COMPANY_CODE + "=?" + " and " +
						BKMLine.XML_ESTATE + "=?" + " and " +
						BKMLine.XML_DIVISION + "=?" + " and " +
						BKMLine.XML_GANG + "=?" + " and " +
						BKMLine.XML_BKM_DATE + "=?",
						new String [] {companyCode, estate, division, gang, bkmDate}, 
						null, null, BKMLine.XML_NIK, null);
				database.closeTransaction();
				
				if(lstBkmLine.size() > 0){
					for(int i = 0; i < lstBkmLine.size(); i++){
						BKMLine bkmLine = (BKMLine) lstBkmLine.get(i);
						
						String nikHarvester = bkmLine.getNik();
						String Harvester = bkmLine.getName();
						String absentType = bkmLine.getAbsentType();
						double mandays = bkmLine.getMandays();
						
						double output = 0;
						
						database.openTransaction();
						BKMOutput bkmOutput = (BKMOutput) database.getDataFirst(false, BKMOutput.TABLE_NAME, 
								new String [] {BKMOutput.XML_NIK, "SUM(" + BKMOutput.XML_OUTPUT + ") as " + BKMOutput.XML_OUTPUT}, 
								BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
								BKMOutput.XML_ESTATE + "=?" + " and " +
								BKMOutput.XML_DIVISION + "=?" + " and " +
								BKMOutput.XML_GANG + "=?" + " and " +
								BKMOutput.XML_BKM_DATE + "=?" + " and " +
								BKMOutput.XML_NIK + "=?",
								new String [] {companyCode, estate, division, gang, bkmDate, nikHarvester}, 
								null, null, null, null);
						database.closeTransaction();
						
						if(bkmOutput != null){
							output = bkmOutput.getOutput();
						}
						
						listTemp.add(new BKMReportHarvester(companyCode, estate, division, gang, nikHarvester, Harvester, absentType, mandays, output));
					}
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BKMReportHarvester> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			adapter = new AdapterBKMReportHarvester(BKMReportHarvesterActivity.this, listTemp, R.layout.item_bkm_report_harvester);
			lsvBkmReportHarvester.setAdapter(adapter);
			
			
			double totMandays = 0.0;
			double totOutput = 0.0;
			
			if(listTemp != null && listTemp.size() > 0){
				for(int i = 0; i < listTemp.size(); i++){
					BKMReportHarvester bkmReport = (BKMReportHarvester) listTemp.get(i);
					
					if(bkmReport != null){
						totMandays += bkmReport.getMandays();
						totOutput += bkmReport.getOutput();
					}
				}
			}
			
			txtBkmReportHarvesterTotMandays.setText(String.valueOf(Utils.round(totMandays, 2)));
			txtBkmReportHarvesterTotOutput.setText(String.valueOf(Utils.round(totOutput, 2)));
			
//			asd
//			if(listTemp.size() > 0){
//				lstBKMReport = listTemp;
//				adapter = new AdapterBKMReportHarvester(BKMReportHarvesterActivity.this, lstBKMReport, R.layout.item_bkm_report_harvester);
//			}else{
//				adapter = null;
//			}
//			
//			lsvBkmReportHarvester.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onDateOK(Date date, int id) {
		bkmDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtBkmReportHarvesterBkmDate.setText(new DateLocal(bkmDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}
	
	
	private double getQty(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String achievementCode){
		double qty = 0;
		
		database.openTransaction();
		List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null, 
				BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
				BPNQuantity.XML_ESTATE + "=?" + " and " +
				BPNQuantity.XML_DIVISION + "=?" + " and " +
				BPNQuantity.XML_GANG + "=?" + " and " +
				BPNQuantity.XML_BPN_DATE + "=?" + " and " +
				BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
				BPNQuantity.XML_ACHIEVEMENT_CODE + "=?", 
				new String [] {companyCode, estate, division, gang, bpnDate, nikHarvester, achievementCode}, 
				null, null, null, null);
		database.closeTransaction();
		
		if(lstQty.size() > 0){
			for(int i = 0; i < lstQty.size(); i++){
				BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);
				
				qty = qty + bpnQuantity.getQuantity();
			}
		}
		
		return qty;
	}
}
