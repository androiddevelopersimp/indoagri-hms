package com.simp.hms.activity.spta;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.simp.hms.R;
import com.simp.hms.R.drawable;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spbs.SPBSWoosimActivity;
import com.simp.hms.adapter.AdapterSPBSHistory;
import com.simp.hms.adapter.AdapterSPTAHistory;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPTA;
import com.simp.hms.model.UserLogin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SPTAHistoryActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogNotificationListener, DialogDateListener, DialogConfirmListener{
	private Toolbar tbrMain;
	private TextView txtSptaHistorySptaDate;
	private Button btnSptaHistorySearch;
	private SwipeMenuListView lsvSptaHistory;
	private EditText edtSptaHistorySearch;

	private List<SPTA> lstSpbsHeader;
	private AdapterSPTAHistory adapter;

	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	private DialogConfirm dialogConfirm;

	DatabaseHandler database = new DatabaseHandler(SPTAHistoryActivity.this);

	String sptaDate = "";

	int posSelected = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		animOnStart();

		setContentView(layout.activity_spta_history);

		tbrMain = (Toolbar) findViewById(id.tbrMain);
		txtSptaHistorySptaDate = (TextView) findViewById(id.txtSptaHistorySptaDate);
		btnSptaHistorySearch = (Button) findViewById(id.btnSptaHistorySearch);
		lsvSptaHistory = (SwipeMenuListView) findViewById(id.lsvSptaHistory);
		edtSptaHistorySearch = (EditText) findViewById(id.edtSptaHistorySearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(string.spbs));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		txtSptaHistorySptaDate.setOnClickListener(this);
		btnSptaHistorySearch.setOnClickListener(this);
		edtSptaHistorySearch.addTextChangedListener(this);
		lsvSptaHistory.setOnItemClickListener(this);

		sptaDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtSptaHistorySptaDate.setText(new DateLocal(sptaDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				// create "open" item
				SwipeMenuItem openItem = new SwipeMenuItem(
						getApplicationContext());
				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(
						getApplicationContext());
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				deleteItem.setIcon(drawable.ic_delete_pressed);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};

		lsvSptaHistory.setMenuCreator(creator);
		lsvSptaHistory.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(int pos, SwipeMenu menu, int index) {
				dialogConfirm = new DialogConfirm(SPTAHistoryActivity.this,
						getResources().getString(string.informasi),
						getResources().getString(string.data_delete), null, id.lsvSptaHistory);
				dialogConfirm.show();

				posSelected = pos;
				return false;
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		SPTA spta = (SPTA) adapter.getItem(pos);

		startActivity(new Intent(SPTAHistoryActivity.this, SPTAActivity.class)
			.putExtra(SPTA.XML_SPTA_NUM, spta.getSptaNum()));

	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case id.txtSptaHistorySptaDate:
			new DialogDate(SPTAHistoryActivity.this, getResources().getString(string.tanggal),
					new Date(), id.txtSptaHistorySptaDate).show();
			break;
		case id.btnSptaHistorySearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;
		case id.btnActionBarLeft:

			break;
		case id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<SPTA>, List<SPTA>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			dialogProgress = new DialogProgress(SPTAHistoryActivity.this, getResources().getString(string.loading));
			dialogProgress.show();
		}

		@Override
		protected List<SPTA> doInBackground(Void... voids) {
			List<SPTA> listTemp = new ArrayList<SPTA>();
			List<Object> listObject;

			String companyCode = "";
			String estate = "";
			String division = "";
			String nik = "";

			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();

			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
				nik = userLogin.getNik();
			}

			database.openTransaction();
			listObject =  database.getListData(false, SPTA.TABLE_NAME, null,
					SPTA.XML_COMPANY_CODE + " = ? " + " and " +
					SPTA.XML_ESTATE + " = ? " + " and " +
					SPTA.XML_DIVISI + " = ? " + " and " +
					SPTA.XML_CREATED_BY + " = ? " + " and " +
					SPTA.XML_SPTA_DATE + " = ? ",
					new String [] {companyCode, estate, division, nik, sptaDate},
					null, null, SPTA.XML_CREATED_DATE + " desc", null);

			database.closeTransaction();

			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					SPTA spta = (SPTA) listObject.get(i);

					listTemp.add(spta);
				}
			}

			return listTemp;
		}

		@Override
		protected void onPostExecute(List<SPTA> lstTemp) {
			super.onPostExecute(lstTemp);

			if(dialogProgress != null){
				dialogProgress.dismiss();
				dialogProgress = null;
			}

			lstSpbsHeader = lstTemp;
			adapter = new AdapterSPTAHistory(SPTAHistoryActivity.this, lstSpbsHeader, layout.item_spta_history);
			
			lsvSptaHistory.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDateOK(Date date, int id) {
		sptaDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtSptaHistorySptaDate.setText(new DateLocal(sptaDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		if(R.id.lsvSptaHistory == id){
			adapter.deleteData(posSelected);
		}
	}
}
