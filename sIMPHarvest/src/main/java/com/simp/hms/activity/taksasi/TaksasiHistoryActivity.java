package com.simp.hms.activity.taksasi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.simp.hms.R;
import com.simp.hms.R.drawable;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterTaksasiHistory;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.UserLogin;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;


public class TaksasiHistoryActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher, DialogDateListener, DialogNotificationListener, DialogConfirmListener{
	private Toolbar tbrMain;
	private TextView txtTaksasiHistoryTaksasiDate;
	private Button btnTaksasiHistorySearch;
	private SwipeMenuListView lsvTaksasiHistory;
	private EditText edtTaksasiHistorySearch;
	
	private List<TaksasiHeader> lstTaksasiHeader;
	private AdapterTaksasiHistory adapter;

	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	private DialogConfirm dialogConfirm;
	
	DatabaseHandler database = new DatabaseHandler(TaksasiHistoryActivity.this);
	
	String taksasiDate = "";
	
	int posSelected = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_taksasi_history);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtTaksasiHistoryTaksasiDate = (TextView) findViewById(R.id.txtTaksasiHistoryTaksasiDate);
		btnTaksasiHistorySearch = (Button) findViewById(R.id.btnTaksasiHistorySearch);
		lsvTaksasiHistory = (SwipeMenuListView) findViewById(R.id.lsvTaksasiHistory);
		edtTaksasiHistorySearch = (EditText) findViewById(R.id.edtTaksasiHistorySearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.taksasi_taksasi));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		txtTaksasiHistoryTaksasiDate.setOnClickListener(this);
		btnTaksasiHistorySearch.setOnClickListener(this);
		edtTaksasiHistorySearch.addTextChangedListener(this);
		lsvTaksasiHistory.setOnItemClickListener(this);
		
		taksasiDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtTaksasiHistoryTaksasiDate.setText(new DateLocal(taksasiDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				// create "open" item
				SwipeMenuItem openItem = new SwipeMenuItem(
						getApplicationContext());
				/*
				// set item background
				openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
						0xCE)));
				// set item width
				openItem.setWidth(dp2px(90));
				// set item title
				openItem.setTitle("Open");
				// set item title fontsize
				openItem.setTitleSize(18);
				// set item title font color
				openItem.setTitleColor(Color.WHITE);
				// add to menu
				menu.addMenuItem(openItem);
				*/

				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(
						getApplicationContext());
				// set item background
				deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
						0x3F, 0x25)));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				deleteItem.setIcon(R.drawable.ic_delete_pressed);
				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};
		
		lsvTaksasiHistory.setMenuCreator(creator);
		lsvTaksasiHistory.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(int pos, SwipeMenu menu, int index) {
				dialogConfirm = new DialogConfirm(TaksasiHistoryActivity.this, 
						getResources().getString(R.string.informasi), 
						getResources().getString(R.string.data_delete), null, R.id.lsvTaksasiHistory);
				dialogConfirm.show();
				
				posSelected = pos;
				
//				adapter.deleteData(pos);
//				switch (index) {
//				case 0:
//					BPNHeader bpnHeader = (BPNHeader) adapter.getItem(pos);
//
//					Bundle bundle = new Bundle();
//					bundle.putParcelable(BPNHeader.TABLE_NAME, bpnHeader);
//					
//					startActivity(new Intent(BPNHistoryActivity.this, BPNActivity.class)
//							.putExtras(bundle));
//					break;
//				case 1:
//					adapter.deleteData(pos);
//					break;
//				default:
//					break;
//				}
				
				
				return false;
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		
//		getDataAsync = new GetDataAsyncTask();
//		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		TaksasiHeader taksasiHeader = (TaksasiHeader) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(TaksasiHeader.TABLE_NAME, taksasiHeader);
		
		startActivity(new Intent(TaksasiHistoryActivity.this, TaksasiActivity.class)
				.putExtras(bundle));
	}
  
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtTaksasiHistoryTaksasiDate:
			new DialogDate(TaksasiHistoryActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtTaksasiHistoryTaksasiDate).show();
			break;
		case R.id.btnTaksasiHistorySearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:    
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<TaksasiHeader>, List<TaksasiHeader>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dialogProgress = new DialogProgress(TaksasiHistoryActivity.this, getResources().getString(R.string.loading));
			dialogProgress.show();
		}

		@Override
		protected List<TaksasiHeader> doInBackground(Void... voids) {
			List<TaksasiHeader> listTemp = new ArrayList<TaksasiHeader>();
			List<Object> listObject;
			
			String companyCode = "";
			String estate = "";
			String division = "";
			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}
			
			database.openTransaction();
			listObject =  database.getListData(false, TaksasiHeader.TABLE_NAME, null, 
					TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " + 
					TaksasiHeader.XML_ESTATE + "=?" + " and " +
					TaksasiHeader.XML_NIK_FOREMAN + "=?" + " and " +
					TaksasiHeader.XML_TAKSASI_DATE + "=?", 
					new String [] {companyCode, estate, nik, taksasiDate}, 
					null, null, TaksasiHeader.XML_CREATED_DATE + " desc", null);
			database.closeTransaction();
			
//			database.openTransaction();
//			listObject =  database.getListData(false, TaksasiHeader.TABLE_NAME, null, 
//					null, null, null, null, null, null);
//			database.closeTransaction();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					TaksasiHeader taksasiHeader = (TaksasiHeader) listObject.get(i);
					
					listTemp.add(taksasiHeader);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<TaksasiHeader> lstTemp) {
			super.onPostExecute(lstTemp);
			
			if(dialogProgress != null){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
//			if(lstTemp.size() > 0){
				lstTaksasiHeader = lstTemp;
				adapter = new AdapterTaksasiHistory(TaksasiHistoryActivity.this, lstTaksasiHeader, R.layout.item_taksasi_history);
//			}else{
//				adapter = null;
//			}
			
			lsvTaksasiHistory.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDateOK(Date date, int id) {
		taksasiDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtTaksasiHistoryTaksasiDate.setText(new DateLocal(taksasiDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		if(R.id.lsvTaksasiHistory == id){
			adapter.deleteData(posSelected);
		}
	}
}
