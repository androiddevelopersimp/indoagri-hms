package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.ancakpanen.AncakPanenActivity;
import com.simp.hms.activity.bkm.BKMAbsentActivity;
import com.simp.hms.activity.bpn.BPNActivity;
import com.simp.hms.activity.bpn.BPNCocoaActivity;
import com.simp.hms.activity.bpn.BPNKaretActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.adapter.AdapterEmployee;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.Division;
import com.simp.hms.model.Employee;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterEmployeeActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private EditText edtMasterEmployeeSearch;
	private ListView lsvMasterEmployee;
	private Button btnMasterEmployeeLoadMore;
	
	List<Employee> listEmployee = new ArrayList<Employee>();
	AdapterEmployee adapter = null;
	ArrayList<String> listNikFilter = new ArrayList<String>();
	String nikFilter = "";
	DialogProgress dialogProgress;
	
	boolean isSearch = false;
	int type;
	String companyCode;
	String estate;
	String division;
	String gang;
	String crop;
	String activity;
	String spbsactivity;
	
	GetDataAsyncTask getDataAsync;
	
	final int MST_DIVISION = 101;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_employee);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterEmployee = (ListView) findViewById(R.id.lsvMasterEmployee);
		edtMasterEmployeeSearch = (EditText) findViewById(R.id.edtMasterEmployeeSearch);
		btnMasterEmployeeLoadMore = (Button) findViewById(R.id.btnMasterEmployeeLoadMore);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_employee));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		btnActionBarRight.setText(R.string.bkm_division);
		btnActionBarRight.setOnClickListener(this);
		edtMasterEmployeeSearch.addTextChangedListener(this);
		lsvMasterEmployee.setOnItemClickListener(this);
		btnMasterEmployeeLoadMore.setOnClickListener(this);
		
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
		type = getIntent().getExtras().getInt(Constanta.TYPE);
		companyCode = getIntent().getExtras().getString(Employee.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(Employee.XML_ESTATE, "");
		division = getIntent().getExtras().getString(Employee.XML_DIVISION, "");
		gang = getIntent().getExtras().getString(Employee.XML_GANG, "");
		crop = getIntent().getExtras().getString(BPNHeader.XML_CROP, "");
		activity = getIntent().getExtras().getString("Activity", "");
        spbsactivity = getIntent().getExtras().getString("SPBSActivity", "");
		
		listNikFilter = getIntent().getExtras().getStringArrayList(Employee.XML_NIK);
			
		if(listNikFilter != null){
			if(listNikFilter.size() > 0){
				for(int i = 0; i < listNikFilter.size(); i++){
					if(i == 0){
						nikFilter = listNikFilter.get(i);
					}else{
						nikFilter = nikFilter + "," + listNikFilter.get(i);
					}
				}
			}
		}
		
		if(type == EmployeeType.HARVESTER_ABSENT.getId()){
			btnActionBarRight.setVisibility(View.VISIBLE);
		}

		adapter = new AdapterEmployee(MasterEmployeeActivity.this, listEmployee, R.layout.item_employee);
		lsvMasterEmployee.setAdapter(adapter);
		lsvMasterEmployee.setScrollingCacheEnabled(false);
		
		getData();
	}
	
	private void getData(){
		String roleId = "";
		String jobPos = "";
		
		if(type == EmployeeType.FOREMAN.getId()){
//			roleId = "LEADER";
//			
//			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 					
//					Employee.XML_COMPANY_CODE + "=?" + " and " + 
//					Employee.XML_ESTATE + "=?" + " and " + 
//					Employee.XML_DIVISION + "=?" + " and " +
//					Employee.XML_ROLE_ID + "=?" + " and " +
//					Employee.XML_GANG + " like ?", 
//					new String [] {companyCode, estate, division, roleId, "%" + gang + "%"}, 
//					null, null, Employee.XML_NAME, null);
			
			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 					
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?" + " and " + 
					Employee.XML_DIVISION + "=?", 
					new String [] {companyCode, estate, division}, 
					null, null, Employee.XML_NAME, null);
			
			
		}else if(type == EmployeeType.CLERK.getId()){
			
			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 					
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?" + " and " +
					Employee.XML_DIVISION + "=?", 
					new String [] {companyCode, estate, division}, 
					null, null, Employee.XML_NAME, null);
			
		}else if(type == EmployeeType.HARVESTER.getId()){
			roleId = "MEMBER";
			 
			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?" + " and " +
					Employee.XML_DIVISION + "=?" + " and " +
					Employee.XML_GANG + "=?" + " and " +
					Employee.XML_ROLE_ID + "=?", 
					new String [] {companyCode, estate, division, gang, roleId}, 
					null, null, Employee.XML_NAME, null);
		}else if(type == EmployeeType.HARVESTER_ABSENT.getId()){

			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?" + " and " +
					Employee.XML_DIVISION + "=?", 
					new String [] {companyCode, estate, division}, 
					null, null, Employee.XML_NAME, null);
		}else if(type == EmployeeType.DRIVER.getId()){
			/*getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?" + " and " +
					Employee.XML_DIVISION + "=?", 
					new String [] {companyCode, estate, division}, 
					null, null, Employee.XML_NAME, null);*/

			//Request Reza Putra -> Keluarkan semua table Master Employee
			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
					Employee.XML_COMPANY_CODE + "=?" + " and " +
					Employee.XML_ESTATE + "=?",
					new String [] {companyCode, estate},
					null, null, Employee.XML_NAME, null);
		}else if(type == EmployeeType.KERNET.getId()) {
			/*getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
					Employee.XML_COMPANY_CODE + "=?" + " and " +
							Employee.XML_ESTATE + "=?" + " and " +
							Employee.XML_DIVISION + "=?",
					new String[]{companyCode, estate, division},
					null, null, Employee.XML_NAME, null);*/

			//Request Reza Putra -> Keluarkan semua table Master Employee
			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
					Employee.XML_COMPANY_CODE + "=?" + " and " +
							Employee.XML_ESTATE + "=?",
					new String [] {companyCode, estate},
					null, null, Employee.XML_NAME, null);
		}else if(type == EmployeeType.OPERATOR.getId()){
			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
					Employee.XML_COMPANY_CODE + "=?" + " and " +
							Employee.XML_ESTATE + "=?" + " and " +
							Employee.XML_DIVISION + "=?",
					new String[]{companyCode, estate, division},
					null, null, Employee.XML_NAME, null);
		}else{
			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
					Employee.XML_COMPANY_CODE + "=?" + " and " + 
					Employee.XML_ESTATE + "=?" + " and " +
					Employee.XML_DIVISION + "=?", 
					new String [] {companyCode, estate, division}, 
					null, null, Employee.XML_NAME, null);
		}
		
//		getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
//				Employee.XML_COMPANY_CODE + "=?" + " and " + 
//				Employee.XML_ESTATE + "=?" + " and " +
//				Employee.XML_DIVISION + "=?", 
//				new String [] {companyCode, estate, division}, 
//				null, null, Employee.XML_NAME, null);

		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		Employee employee = (Employee) adapter.getItem(pos);
		
		if(isSearch){
			if(activity.equalsIgnoreCase("BPN")){
				if(crop.equalsIgnoreCase("01")) {
					setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, BPNActivity.class)
							.putExtra(Employee.XML_NIK, employee.getNik())
							.putExtra(Employee.XML_NAME, employee.getName())
							.putExtra(Employee.XML_GANG, employee.getGang()));
					finish();
				}else if(crop.equalsIgnoreCase("02")){
					setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, BPNKaretActivity.class)
							.putExtra(Employee.XML_NIK, employee.getNik())
							.putExtra(Employee.XML_NAME, employee.getName())
							.putExtra(Employee.XML_GANG, employee.getGang()));
					finish();
				} else if(crop.equalsIgnoreCase("04")){
					setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, BPNCocoaActivity.class)
							.putExtra(Employee.XML_NIK, employee.getNik())
							.putExtra(Employee.XML_NAME, employee.getName())
							.putExtra(Employee.XML_GANG, employee.getGang()));
					finish();
				}
			}else if(activity.equalsIgnoreCase("SP")){
				if(crop.equalsIgnoreCase("01")) {
					if(spbsactivity.equalsIgnoreCase("SPBSActivity")) {
						setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, SPBSActivity.class)
								.putExtra(Employee.XML_NIK, employee.getNik())
								.putExtra(Employee.XML_NAME, employee.getName())
								.putExtra(Employee.XML_GANG, employee.getGang()));
						finish();
					}else if(spbsactivity.equalsIgnoreCase("SPBSDatecsActivity")){
						setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, SPBSDatecsActivity.class)
								.putExtra(Employee.XML_NIK, employee.getNik())
								.putExtra(Employee.XML_NAME, employee.getName())
								.putExtra(Employee.XML_GANG, employee.getGang()));
						finish();
					}
				}else if(crop.equalsIgnoreCase("04")){
					setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, SPUActivity.class)
							.putExtra(Employee.XML_NIK, employee.getNik())
							.putExtra(Employee.XML_NAME, employee.getName())
							.putExtra(Employee.XML_GANG, employee.getGang()));
					finish();
				}
			}else if(activity.equalsIgnoreCase("BKM")){
				setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, BKMAbsentActivity.class)
						.putExtra(Employee.XML_NIK, employee.getNik())
						.putExtra(Employee.XML_NAME, employee.getName())
						.putExtra(Employee.XML_GANG, employee.getGang()));
				finish();
			}else if(activity.equalsIgnoreCase("AncakPanen")){
				setResult(RESULT_OK, new Intent(MasterEmployeeActivity.this, AncakPanenActivity.class)
						.putExtra(Employee.XML_NIK, employee.getNik())
						.putExtra(Employee.XML_NAME, employee.getName())
						.putExtra(Employee.XML_GANG, employee.getGang()));
				finish();
			}

		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:
			startActivityForResult(new Intent(MasterEmployeeActivity.this, MasterEmployeeDivisionActivity.class)
			.putExtra(Constanta.SEARCH, true)
			.putExtra(Employee.XML_COMPANY_CODE, companyCode)
			.putExtra(Employee.XML_ESTATE, estate), 
			MST_DIVISION);
			break;
		case R.id.btnMasterEmployeeLoadMore:
			String roleId;
			String jobPos;
			
//			getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
//					Employee.XML_COMPANY_CODE + "=?" + " and " + 
//					Employee.XML_ESTATE + "=?" + " and " +
//					Employee.XML_DIVISION + "!=?", 
//					new String [] {companyCode, estate, division}, 
//					null, null, Employee.XML_NAME, null);
//			
//			getDataAsync.execute();
			
			if(type == EmployeeType.FOREMAN.getId()){
//				roleId = "LEADER";
//				
//				getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 					
//						Employee.XML_COMPANY_CODE + "=?" + " and " + 
//						Employee.XML_ESTATE + "=?" + " and " + 
//						Employee.XML_DIVISION + "!=?" + " and " +
//						Employee.XML_ROLE_ID + "=?" + " and " +
//						Employee.XML_GANG + " like ?", 
//						new String [] {companyCode, estate, division, roleId, "%" + gang + "%"}, 
//						null, null, Employee.XML_NAME, null);

				getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 					
						Employee.XML_COMPANY_CODE + "=?" + " and " + 
						Employee.XML_ESTATE + "=?" + " and " + 
						Employee.XML_DIVISION + "!=?", 
						new String [] {companyCode, estate, division}, 
						null, null, Employee.XML_NAME, null);
				
				getDataAsync.execute();
			}else if(type == EmployeeType.HARVESTER.getId()){
				roleId = "MEMBER";
				
				getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
						Employee.XML_COMPANY_CODE + "=?" + " and " + 
						Employee.XML_ESTATE + "=?" + " and " +
						Employee.XML_ROLE_ID + "=?", 
						new String [] {companyCode, estate, roleId}, 
						null, null, Employee.XML_NAME, null);
				
				getDataAsync.execute();
			}else if(type == EmployeeType.CLERK.getId()){
	
				getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 					
						Employee.XML_COMPANY_CODE + "=?" + " and " + 
						Employee.XML_ESTATE + "=?" + " and " +
						Employee.XML_DIVISION + "!=?", 
						new String [] {companyCode, estate, division}, 
						null, null, Employee.XML_NAME, null);
				
				getDataAsync.execute();
			}else if(type == EmployeeType.HARVESTER_ABSENT.getId()){
				getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
						Employee.XML_COMPANY_CODE + "=?" + " and " + 
						Employee.XML_ESTATE + "=?" + " and " +
						Employee.XML_DIVISION + "!=?", 
						new String [] {companyCode, estate, division}, 
						null, null, Employee.XML_NAME, null);
				
				getDataAsync.execute();
			}else if(type == EmployeeType.KERNET.getId()){
				getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null, 
						Employee.XML_COMPANY_CODE + "=?" + " and " + 
						Employee.XML_ESTATE + "=?" + " and " +
						Employee.XML_DIVISION + "!=?", 
						new String [] {companyCode, estate, division}, 
						null, null, Employee.XML_NAME, null);
				
				getDataAsync.execute();
			}else if(type == EmployeeType.OPERATOR.getId()){
				getDataAsync = new GetDataAsyncTask(false, Employee.TABLE_NAME, null,
						Employee.XML_COMPANY_CODE + "=?" + " and " +
								Employee.XML_ESTATE + "=?" + " and " +
								Employee.XML_DIVISION + "!=?",
						new String [] {companyCode, estate, division},
						null, null, Employee.XML_NAME, null);

				getDataAsync.execute();
			}
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		try{
			adapter.getFilter().filter(filter);
			adapter.notifyDataSetChanged();
		}catch(NullPointerException e){
			e.printStackTrace();
		}
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<Employee>, List<Employee>>{
		boolean distinct;
		String tableName;
		String [] columns;  
		String whereClause;
		String [] whereArgs;
		String groupBy;
		String having;
		String orderBy;
		String limit;
		
		public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
				String groupBy, String having, String orderBy, String limit){
			this.distinct = distinct;
			this.tableName = tableName;
			this.columns = columns;
			this.whereClause = whereClause;
			this.whereArgs = whereParams;
			this.groupBy = groupBy;
			this.having = having;
			this.orderBy = orderBy;
			this.limit = limit;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterEmployeeActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<Employee> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterEmployeeActivity.this);
			List<Employee> listTemp = new ArrayList<Employee>();
			List<Object> listObject;

			database.openTransaction();
			listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs, groupBy, having, orderBy, limit);
			database.closeTransaction();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					Employee empNew = (Employee) listObject.get(i);
					boolean foundFilter = false;
					
					if(listNikFilter != null && listNikFilter.size() > 0){
						for(int j = 0; j < listNikFilter.size(); j++){
							if(empNew.getNik().equals(listNikFilter.get(j))){
								foundFilter = true;
								break;
							}
						}
					}
					
					boolean foundCurrent = false;
					if(listEmployee.size() > 0){
						for(int x = 0; x < listEmployee.size(); x++){
							Employee empCur = (Employee) listEmployee.get(x);
							
							if(empNew.getNik().equals(empCur.getNik())){
								foundCurrent = true;
								break;
							}
						}
					}
					
					if(!foundFilter && !foundCurrent){
						listTemp.add(empNew);
					}
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<Employee> lstTemp) {
			super.onPostExecute(lstTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			for(int i = 0; i < lstTemp.size(); i++){
				Employee employee = (Employee) lstTemp.get(i);
				
				adapter.addData(employee);
			}
			
//			if(listEmployee.size() > 0){
//				if(lstTemp.size() > 0){
//					if(adapter != null){
//						for(int i = 0; i < lstTemp.size(); i++){
//							Employee employee = (Employee) lstTemp.get(i);
//							
//							adapter.addData(employee);
//						}
//					}
//				}
//			}else{
//				if(lstTemp.size() > 0){
//					listEmployee = lstTemp;
//					adapter = new AdapterEmployee(MasterEmployeeActivity.this, listEmployee, R.layout.item_employee);
//				}else{
//					adapter = null;
//				}
//				
//				lsvMasterEmployee.setAdapter(adapter);
//			}
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_OK){
			if(requestCode == MST_DIVISION){
				division = data.getExtras().getString(Division.XML_DIVISION);
				
				listEmployee = new ArrayList<Employee>();
				adapter = null;
				lsvMasterEmployee.setAdapter(adapter);
				
				type = EmployeeType.HARVESTER_ABSENT.getId();
				
				getData();
			}
		}
	}
	
	
}
