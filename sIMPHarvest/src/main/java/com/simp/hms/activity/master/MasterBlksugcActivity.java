package com.simp.hms.activity.master;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spta.SPTAActivity;
import com.simp.hms.adapter.AdapterBlksugc;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.model.BLKSUGC;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.routines.Constanta;

import java.util.ArrayList;
import java.util.List;

public class MasterBlksugcActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterBlksugc;
	private EditText edtMasterBlksugcSearch;
	private Button btnMasterBlksugcLoadMore;

	private List<BLKSUGC> listBlksugc = new ArrayList<BLKSUGC>();
	private AdapterBlksugc adapter;

	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;

	boolean isSearch = false;
	String companyCode;
	String estate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		animOnStart();

		setContentView(layout.activity_master_blksugc);
		tbrMain = (Toolbar) findViewById(id.tbrMain);
		lsvMasterBlksugc = (ListView) findViewById(id.lsvMasterBlksugc);
		edtMasterBlksugcSearch = (EditText) findViewById(id.edtMasterBlksugcSearch);
		btnMasterBlksugcLoadMore = (Button) findViewById(id.btnMasterBlksugcLoadMore);


		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(string.master_blksugc));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtMasterBlksugcSearch.addTextChangedListener(this);
		lsvMasterBlksugc.setOnItemClickListener(this);
		btnMasterBlksugcLoadMore.setOnClickListener(this);

		companyCode = getIntent().getExtras().getString(BLKSUGC.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(BLKSUGC.XML_ESTATE, "");
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);

		adapter = new AdapterBlksugc(MasterBlksugcActivity.this, listBlksugc, layout.item_blksugc);
		lsvMasterBlksugc.setAdapter(adapter);
		lsvMasterBlksugc.setScrollingCacheEnabled(false);

		getData();
	}

	private void getData(){
		getDataAsync = new GetDataAsyncTask(true, BLKSUGC.TABLE_NAME, null,
				BLKSUGC.XML_COMPANY_CODE + "=?" + " and " +
						BLKSUGC.XML_ESTATE + "=?",
				new String [] {companyCode, estate},
				null, null, BLKSUGC.XML_BLOCK, null);
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		BLKSUGC blksugc = (BLKSUGC) adapter.getItem(pos);

		if(isSearch){
			setResult(RESULT_OK, new Intent(MasterBlksugcActivity.this, SPTAActivity.class)
			.putExtra(BLKSUGC.XML_BLOCK, blksugc.getBlock())
			.putExtra(BLKSUGC.XML_PHASE, blksugc.getPhase())
			.putExtra(BLKSUGC.XML_SUB_DIVISION, blksugc.getSubDivision())
			.putExtra(BLKSUGC.XML_DISTANCE, blksugc.getDistance()));
			finish();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case id.btnActionBarLeft:

			break;
		case id.btnActionBarRight:

			break;
		case id.btnMasterBlockHdrcLoadMore:
			getDataAsync = new GetDataAsyncTask(true, BLKSUGC.TABLE_NAME, null,
					BLKSUGC.XML_COMPANY_CODE + "=?" + " and " +
							BLKSUGC.XML_ESTATE + "=?",
					new String [] {companyCode, estate},
					null, null, BLKSUGC.XML_BLOCK, null);
			getDataAsync.execute();
			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BLKSUGC>, List<BLKSUGC>>{
		boolean distinct;
		String tableName;
		String [] columns;
		String whereClause;
		String [] whereArgs;
		String groupBy;
		String having;
		String orderBy;
		String limit;

		public GetDataAsyncTask(boolean distinct, String tableName, String [] columns, String whereClause, String [] whereParams,
				String groupBy, String having, String orderBy, String limit){
			this.distinct = distinct;
			this.tableName = tableName;
			this.columns = columns;
			this.whereClause = whereClause;
			this.whereArgs = whereParams;
			this.groupBy = groupBy;
			this.having = having;
			this.orderBy = orderBy;
			this.limit = limit;
		}


		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterBlksugcActivity.this, getResources().getString(string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<BLKSUGC> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterBlksugcActivity.this);
			List<BLKSUGC> listTemp = new ArrayList<BLKSUGC>();
			List<Object> listObject;
			
			database.openTransaction();
			if(isSearch){
				listObject = database.getListData(distinct, tableName, columns, whereClause, whereArgs,
						groupBy, having, orderBy, limit);
			}else{
				listObject =  database.getListData(true, BLKSUGC.TABLE_NAME,
						null,
						null, null, 
						null, null, BLKSUGC.XML_BLOCK, null);
			}
			database.closeTransaction();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BLKSUGC blksugc = (BLKSUGC) listObject.get(i);
					
					listTemp.add(blksugc);
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BLKSUGC> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			for(int i = 0; i < listTemp.size(); i++){
				BLKSUGC blksugc = (BLKSUGC) listTemp.get(i);
				
				adapter.addData(blksugc);
			}
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
}
