package com.simp.hms.activity.spu;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterSPUReportNumberPOD;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BJR;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPUReportBlockPOD;
import com.simp.hms.model.SPUReportNumberPOD;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SPUPODReportNumberActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, DialogDateListener {
    private Toolbar tbrMain;
    private TextView txtSpuReportNumberSpuDate;
    private Button btnSpuReportNumberSearch;
    private ListView lsvSpuReportNumber;

    private TextView txtSpuReportNumberTotPODQty;
    private TextView txtSpuReportNumberTotGoodWeight;
    private TextView txtSpuReportNumberTotEstimasiQty;
    private TextView txtSpuReportNumberTotPoorWeight;

    private List<SPUReportNumberPOD> lstSPUReport;
    private AdapterSPUReportNumberPOD adapter;

    private SPUPODReportNumberActivity.GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(SPUPODReportNumberActivity.this);

    String spuDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_spu_report_number_pod);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);

        txtSpuReportNumberSpuDate = (TextView) findViewById(R.id.txtSpuReportNumberSpuDate);
        btnSpuReportNumberSearch = (Button) findViewById(R.id.btnSpuReportNumberSearch);
        lsvSpuReportNumber = (ListView) findViewById(R.id.lsvSpuReportNumber);

        txtSpuReportNumberTotPODQty = (TextView) findViewById(R.id.txtSpuReportNumberTotPODQty);
        txtSpuReportNumberTotGoodWeight = (TextView) findViewById(R.id.txtSpuReportNumberTotGoodWeight);
        txtSpuReportNumberTotEstimasiQty = (TextView) findViewById(R.id.txtSpuReportNumberTotEstimasiQty);
        txtSpuReportNumberTotPoorWeight = (TextView) findViewById(R.id.txtSpuReportNumberTotPoorWeight);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.spu));
        btnActionBarright.setVisibility(View.INVISIBLE);
        txtSpuReportNumberSpuDate.setOnClickListener(this);
        btnSpuReportNumberSearch.setOnClickListener(this);
        lsvSpuReportNumber.setOnItemClickListener(this);

        spuDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        txtSpuReportNumberSpuDate.setText(new DateLocal(spuDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

        adapter = new AdapterSPUReportNumberPOD(SPUPODReportNumberActivity.this, new ArrayList<SPUReportNumberPOD>(), R.layout.item_spu_report_number_pod);
        lsvSpuReportNumber.setAdapter(adapter);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        SPUReportNumberPOD spuReport = (SPUReportNumberPOD) adapter.getItem(pos);

        Bundle bundle = new Bundle();
        bundle.putParcelable(SPUReportNumberPOD.TABLE_NAME, spuReport);

        startActivity(new Intent(SPUPODReportNumberActivity.this, SPUPODReportBlockActivity.class)
                .putExtras(bundle)
                .putExtra(SPBSHeader.XML_SPBS_DATE, spuDate));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSpuReportNumberSpuDate:
                new DialogDate(SPUPODReportNumberActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtSpbsReportNumberSpbsDate).show();
                break;
            case R.id.btnSpuReportNumberSearch:
                getDataAsync = new SPUPODReportNumberActivity.GetDataAsyncTask();
                getDataAsync.execute();
                break;
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        spuDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

        txtSpuReportNumberSpuDate.setText(new DateLocal(spuDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPUReportNumberPOD>, List<SPUReportNumberPOD>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(SPUPODReportNumberActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<SPUReportNumberPOD> doInBackground(Void... voids) {
            String companyCode = "";
            String estate = "";
            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                nik = userLogin.getNik();
            }

            database.openTransaction();
            List<Object> listObject =  database.getListData(true, SPBSHeader.TABLE_NAME, null,
                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                            SPBSHeader.XML_SPBS_DATE + "=?" + " and " +
                            SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
                            SPBSHeader.XML_CROP + "=?",
                    new String [] {companyCode, estate, spuDate, nik, "04"},
                    null, null, SPBSHeader.XML_SPBS_NUMBER, null);
            database.closeTransaction();

            List<SPUReportNumberPOD> listTemp = new ArrayList<SPUReportNumberPOD>();

            if(listObject.size() > 0){
                double TotalqtyEstimasi = 0;
                for(int i = 0; i < listObject.size(); i++){
                    SPBSHeader spuHeader = (SPBSHeader) listObject.get(i);

                    String year = spuHeader.getYear();
                    String spuNumber = spuHeader.getSpbsNumber();
                    String driver = spuHeader.getDriver();
                    String licensePlate = spuHeader.getLicensePlate();

                    companyCode = spuHeader.getCompanyCode();
                    estate = spuHeader.getEstate();
                    spuDate = spuHeader.getSpbsDate();
                    double qtyPODQty = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.POD_QTY_CODE);
                    double qtyGoodQty = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.GOOD_QTY_CODE);
                    double qtyGoodWeight = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.GOOD_WEIGHT_CODE);
                    double qtyBadQty = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.BAD_QTY_CODE);
                    double qtyBadWeight = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.BAD_WEIGHT_CODE);
                    double qtyPoorQty = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.POOR_QTY_CODE);
                    double qtyPoorWeight = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.POOR_WEIGHT_CODE);

                    database.openTransaction();
                    List<Object> listObject2 =  database.getListData(true, SPBSLine.TABLE_NAME,
                            new String [] {SPBSLine.XML_BLOCK},
                            SPBSLine.XML_YEAR + "=?" + " and " +
                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                    SPBSLine.XML_CROP + "=?",
                            new String [] {year, companyCode, estate, spuNumber, spuDate, "04"},
                            SPBSLine.XML_BLOCK, null, SPBSLine.XML_BLOCK, null);

                    database.closeTransaction();

                    if(listObject2.size() > 0) {
                        double qtyEstimasi = 0;
                        for (int iBJR = 0; iBJR < listObject2.size(); iBJR++) {
                            SPBSLine spuLine = (SPBSLine) listObject2.get(iBJR);
                            String block = spuLine.getBlock();
                            double qtyPODQtyItem = getQtyItem(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POD_QTY_CODE);
                            double qtyGoodQtyItem = getQtyItem(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.GOOD_QTY_CODE);
                            double qtyGoodWeightItem = getQtyItem(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.GOOD_WEIGHT_CODE);
                            double qtyBadQtyItem = getQtyItem(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.BAD_QTY_CODE);
                            double qtyBadWeightItem = getQtyItem(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.BAD_WEIGHT_CODE);
                            double qtyPoorQtyItem = getQtyItem(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POOR_QTY_CODE);
                            double qtyPoorWeightItem = getQtyItem(year, companyCode, estate, spuNumber, spuDate, block, BPNQuantity.POOR_WEIGHT_CODE);

                            database.openTransaction();
                            List<Object> BJRValue = database.getListData(false, BJR.TABLE_NAME, null,
                                    BJR.XML_COMPANY_CODE + "=?" + " and " +
                                            BJR.XML_ESTATE + "=?" + " and " +
                                            BJR.XML_BLOCK + "=?" + " ",
                                    new String[]{companyCode, estate, block},
                                    null, null, BJR.XMl_EFF_DATE + " DESC", "1");

                            double qtyBJR = 0;
                            database.closeTransaction();
                            if (BJRValue.size() > 0) {
                                for (int a = 0; a < BJRValue.size(); a++) {
                                    BJR bjr = (BJR) BJRValue.get(a);

                                    qtyBJR = bjr.getBjr();
                                    qtyEstimasi = qtyPODQtyItem * qtyBJR;
                                    TotalqtyEstimasi += qtyEstimasi;
                                }
                            } else {
                                qtyEstimasi = qtyPODQty;
                                TotalqtyEstimasi += qtyEstimasi;
                            }
                        }
                    }
                    listTemp.add(new SPUReportNumberPOD(year, companyCode, estate, "04", spuNumber, nik, driver, licensePlate, qtyGoodQty, qtyGoodWeight, qtyBadQty, qtyBadWeight, qtyPoorQty, qtyPoorWeight,qtyPODQty,TotalqtyEstimasi));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<SPUReportNumberPOD> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            adapter = new AdapterSPUReportNumberPOD(SPUPODReportNumberActivity.this, listTemp, R.layout.item_spu_report_number_pod);
            lsvSpuReportNumber.setAdapter(adapter);

            double totGoodQty = 0;
            double totGoodWeight = 0;
            double totBadQty = 0;
            double totBadWeight = 0;
            double totPoorQty = 0;
            double totPoorWeight = 0;

            double totPODQty = 0;
            double totEstimasiQty = 0;

            if(listTemp != null && listTemp.size() > 0){
                for(int i = 0; i < listTemp.size(); i++){
                    SPUReportNumberPOD spuReport = (SPUReportNumberPOD) listTemp.get(i);

                    if(spuReport != null){
                        totGoodQty += (double) spuReport.getQtyGoodQty();
                        totGoodWeight += (double) spuReport.getQtyGoodWeight();

                        totBadQty += (double) spuReport.getQtyBadQty();
                        totBadWeight += (double) spuReport.getQtyBadWeight();

                        totPoorQty += (double) spuReport.getQtyPoorQty();
                        totPoorWeight += (double) spuReport.getQtyPoorWeight();
                        totPODQty += (double) spuReport.getQtyPODQty();
                        totEstimasiQty += (double) spuReport.getQtyEstimasiQty();
                    }
                }
            }


            txtSpuReportNumberTotPODQty.setText(String.valueOf(Utils.round(totPODQty, 2)));
            txtSpuReportNumberTotGoodWeight.setText(String.valueOf(Utils.round(totGoodWeight, 2)));

            txtSpuReportNumberTotEstimasiQty.setText(String.valueOf(Utils.round(totEstimasiQty, 2)));
            txtSpuReportNumberTotPoorWeight.setText(String.valueOf(Utils.round(totPoorWeight, 2)));
        }
    }

    private double getQty(String year, String companyCode, String estate, String spbsNumber, String spbsDate, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, SPBSLine.TABLE_NAME, null,
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?",
                new String [] {year, companyCode, estate, spbsNumber, spbsDate, achievementCode, "04"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                SPBSLine SPBSLine = (SPBSLine) lstQty.get(i);

                qty = qty + SPBSLine.getQuantityAngkut();
            }
        }

        return qty;
    }

    private double getQtyItem(String year, String companyCode, String estate, String spbsNumber, String spbsDate, String block, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, SPBSLine.TABLE_NAME, null,
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                        SPBSLine.XML_BLOCK + "=?" + " and " +
                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?",
                new String [] {year, companyCode, estate, spbsNumber, spbsDate, block, achievementCode, "04"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                SPBSLine SPBSLine = (SPBSLine) lstQty.get(i);

                qty = qty + SPBSLine.getQuantityAngkut();
            }
        }

        return qty;
    }
}
