package com.simp.hms.activity.master;

import java.util.ArrayList;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.adapter.AdapterVehicle;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.enums.RunningAccountType;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.routines.Constanta;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class MasterVehicleActivity extends BaseActivity implements OnItemClickListener, OnClickListener, TextWatcher{
	private Toolbar tbrMain;
	private ListView lsvMasterVehicle;
	private EditText edtMasterVehicleSearch;
	
	private List<RunningAccount> listVehicle;
	private AdapterVehicle adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	boolean isSearch = false;
	
	String companyCode;
	String estate;
	String lifnr;
	String ownerShipFlag;
	int type;
	String crop;
	String spbsactivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_master_vehicle);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		lsvMasterVehicle = (ListView) findViewById(R.id.lsvMasterVehicle);
		edtMasterVehicleSearch = (EditText) findViewById(R.id.edtMasterVehicleSearch);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.master_vehicle));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		edtMasterVehicleSearch.addTextChangedListener(this);
		lsvMasterVehicle.setOnItemClickListener(this);
		
		isSearch = getIntent().getExtras().getBoolean(Constanta.SEARCH, false);
		type = getIntent().getExtras().getInt(Constanta.TYPE, RunningAccountType.GENERAL.getId());
		companyCode = getIntent().getExtras().getString(RunningAccount.XML_COMPANY_CODE, "");
		estate = getIntent().getExtras().getString(RunningAccount.XML_ESTATE, "");
		lifnr = getIntent().getExtras().getString(RunningAccount.XML_LIFNR, "");
		ownerShipFlag = getIntent().getExtras().getString(RunningAccount.XML_OWNERSHIPFLAG, "");
		crop = getIntent().getExtras().getString(BPNHeader.XML_CROP, "");
		spbsactivity = getIntent().getExtras().getString("SPBSActivity", "");
	}

	@Override
	protected void onStart() {
		super.onStart();   
		   
		getDataAsync = new GetDataAsyncTask();
		getDataAsync.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		RunningAccount vehicle = (RunningAccount) adapter.getItem(pos);
		
		if(isSearch){
			if(crop.equalsIgnoreCase("01")) {
				if(spbsactivity.equalsIgnoreCase("SPBSActivity")) {
					setResult(RESULT_OK, new Intent(MasterVehicleActivity.this, SPBSActivity.class)
							.putExtra(RunningAccount.XML_RUNNING_ACCOUNT, vehicle.getRunningAccount())
							.putExtra(RunningAccount.XML_LICENSE_PLATE, vehicle.getLicensePlate())
							.putExtra(RunningAccount.XML_ESTATE, vehicle.getEstate())
							.putExtra(RunningAccount.XML_LIFNR, vehicle.getLifnr())
							.putExtra(RunningAccount.XML_LIFNR, vehicle.getLifnr()));
					finish();
				}else if(spbsactivity.equalsIgnoreCase("SPBSDatecsActivity")){
					setResult(RESULT_OK, new Intent(MasterVehicleActivity.this, SPBSDatecsActivity.class)
							.putExtra(RunningAccount.XML_RUNNING_ACCOUNT, vehicle.getRunningAccount())
							.putExtra(RunningAccount.XML_LICENSE_PLATE, vehicle.getLicensePlate())
							.putExtra(RunningAccount.XML_ESTATE, vehicle.getEstate())
							.putExtra(RunningAccount.XML_LIFNR, vehicle.getLifnr())
							.putExtra(RunningAccount.XML_LIFNR, vehicle.getLifnr()));
					finish();
				}
			}else if(crop.equalsIgnoreCase("04")) {
				setResult(RESULT_OK, new Intent(MasterVehicleActivity.this, SPUActivity.class)
						.putExtra(RunningAccount.XML_RUNNING_ACCOUNT, vehicle.getRunningAccount())
						.putExtra(RunningAccount.XML_LICENSE_PLATE, vehicle.getLicensePlate())
						.putExtra(RunningAccount.XML_ESTATE, vehicle.getEstate())
						.putExtra(RunningAccount.XML_LIFNR, vehicle.getLifnr())
						.putExtra(RunningAccount.XML_LIFNR, vehicle.getLifnr()));
				finish();
			}
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnActionBarLeft:
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	@Override
	public void afterTextChanged(Editable filter) {
		adapter.getFilter().filter(filter);
		adapter.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	private class GetDataAsyncTask extends AsyncTask<Void, List<RunningAccount>, List<RunningAccount>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(MasterVehicleActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override
		protected List<RunningAccount> doInBackground(Void... voids) {
			DatabaseHandler database = new DatabaseHandler(MasterVehicleActivity.this);
			List<Object> listObject = new ArrayList<>();

			database.openTransaction();

			if(type == RunningAccountType.GENERAL.getId()) {
				listObject = database.getListData(false, RunningAccount.TABLE_NAME, null,
						RunningAccount.XML_COMPANY_CODE + "=?" + " and " +
								RunningAccount.XML_ESTATE + "=?",
						new String[]{companyCode, estate},
						null, null, RunningAccount.XML_RUNNING_ACCOUNT, null);
			}else if(type == RunningAccountType.TRUCK_LOGO.getId()){
				listObject = database.getListData(false, RunningAccount.TABLE_NAME, null,
						RunningAccount.XML_COMPANY_CODE + "=?" + " and " +
								RunningAccount.XML_ESTATE + "=?" + " and " +
						RunningAccount.XML_LIFNR + " is not null " + " and " +
						RunningAccount.XML_LIFNR + " != \"\"",
						new String[]{companyCode, estate},
						null, null, RunningAccount.XML_RUNNING_ACCOUNT, null);
			}else if(type ==RunningAccountType.CH_GL.getId()){
				listObject = database.getListData(false, RunningAccount.TABLE_NAME, null,
						RunningAccount.XML_COMPANY_CODE + "=?" + " and " +
								RunningAccount.XML_ESTATE + "=?" + " and " +
						RunningAccount.XML_OWNERSHIPFLAG + "=?",
						new String[]{companyCode, estate, ownerShipFlag},
						null, null, RunningAccount.XML_RUNNING_ACCOUNT, null);
			}
			database.closeTransaction();
			
			List<RunningAccount> listTemp = new ArrayList<RunningAccount>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					RunningAccount vehicle = (RunningAccount) listObject.get(i);
					
					listTemp.add(vehicle);
				}
			}
			
			return listTemp;
		}

		@Override
		protected void onPostExecute(List<RunningAccount> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			if(listTemp.size() > 0){
				listVehicle = listTemp;
				adapter = new AdapterVehicle(MasterVehicleActivity.this, listVehicle, R.layout.item_vehicle);
			}else{
				adapter = null;
			}
			
			lsvMasterVehicle.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {  
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}
	
}
