package com.simp.hms.activity.spu;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterSPUReportNumber;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSReportNumber;
import com.simp.hms.model.SPUReportNumber;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SPUReportNumberActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener, DialogDateListener {
    private Toolbar tbrMain;
    private TextView txtSpuReportNumberSpuDate;
    private Button btnSpuReportNumberSearch;
    private ListView lsvSpuReportNumber;

    private TextView txtSpuReportNumberTotGoodQty;
    private TextView txtSpuReportNumberTotGoodWeight;
    private TextView txtSpuReportNumberTotBadQty;
    private TextView txtSpuReportNumberTotBadWeight;
    private TextView txtSpuReportNumberTotPoorQty;
    private TextView txtSpuReportNumberTotPoorWeight;

    private List<SPUReportNumber> lstSPUReport;
    private AdapterSPUReportNumber adapter;

    private SPUReportNumberActivity.GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(SPUReportNumberActivity.this);

    String spuDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_spu_report_number);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);

        txtSpuReportNumberSpuDate = (TextView) findViewById(R.id.txtSpuReportNumberSpuDate);
        btnSpuReportNumberSearch = (Button) findViewById(R.id.btnSpuReportNumberSearch);
        lsvSpuReportNumber = (ListView) findViewById(R.id.lsvSpuReportNumber);

        txtSpuReportNumberTotGoodQty = (TextView) findViewById(R.id.txtSpuReportNumberTotGoodQty);
        txtSpuReportNumberTotGoodWeight = (TextView) findViewById(R.id.txtSpuReportNumberTotGoodWeight);
        txtSpuReportNumberTotBadQty = (TextView) findViewById(R.id.txtSpuReportNumberTotBadQty);
        txtSpuReportNumberTotBadWeight = (TextView) findViewById(R.id.txtSpuReportNumberTotBadWeight);
        txtSpuReportNumberTotPoorQty = (TextView) findViewById(R.id.txtSpuReportNumberTotPoorQty);
        txtSpuReportNumberTotPoorWeight = (TextView) findViewById(R.id.txtSpuReportNumberTotPoorWeight);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarright = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.spu));
        btnActionBarright.setVisibility(View.INVISIBLE);
        txtSpuReportNumberSpuDate.setOnClickListener(this);
        btnSpuReportNumberSearch.setOnClickListener(this);
        lsvSpuReportNumber.setOnItemClickListener(this);

        spuDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        txtSpuReportNumberSpuDate.setText(new DateLocal(spuDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

        adapter = new AdapterSPUReportNumber(SPUReportNumberActivity.this, new ArrayList<SPUReportNumber>(), R.layout.item_spu_report_number);
        lsvSpuReportNumber.setAdapter(adapter);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        SPUReportNumber spuReport = (SPUReportNumber) adapter.getItem(pos);

        Bundle bundle = new Bundle();
        bundle.putParcelable(SPUReportNumber.TABLE_NAME, spuReport);

        startActivity(new Intent(SPUReportNumberActivity.this, SPUReportBlockActivity.class)
                .putExtras(bundle)
                .putExtra(SPBSHeader.XML_SPBS_DATE, spuDate));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSpuReportNumberSpuDate:
                new DialogDate(SPUReportNumberActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtSpbsReportNumberSpbsDate).show();
                break;
            case R.id.btnSpuReportNumberSearch:
                getDataAsync = new SPUReportNumberActivity.GetDataAsyncTask();
                getDataAsync.execute();
                break;
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        spuDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

        txtSpuReportNumberSpuDate.setText(new DateLocal(spuDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<SPUReportNumber>, List<SPUReportNumber>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(SPUReportNumberActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<SPUReportNumber> doInBackground(Void... voids) {
            String companyCode = "";
            String estate = "";
            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                nik = userLogin.getNik();
            }

            database.openTransaction();
            List<Object> listObject =  database.getListData(true, SPBSHeader.TABLE_NAME, null,
                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                            SPBSHeader.XML_ESTATE + "=?" + " and " +
                            SPBSHeader.XML_SPBS_DATE + "=?" + " and " +
                            SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
                            SPBSHeader.XML_CROP + "=?",
                    new String [] {companyCode, estate, spuDate, nik, "04"},
                    null, null, SPBSHeader.XML_SPBS_NUMBER, null);
            database.closeTransaction();

            List<SPUReportNumber> listTemp = new ArrayList<SPUReportNumber>();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    SPBSHeader spuHeader = (SPBSHeader) listObject.get(i);

                    String year = spuHeader.getYear();
                    String spuNumber = spuHeader.getSpbsNumber();
                    String driver = spuHeader.getDriver();
                    String licensePlate = spuHeader.getLicensePlate();

                    companyCode = spuHeader.getCompanyCode();
                    estate = spuHeader.getEstate();
                    spuDate = spuHeader.getSpbsDate();

                    double qtyGoodQty = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.GOOD_QTY_CODE);
                    double qtyGoodWeight = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.GOOD_WEIGHT_CODE);
                    double qtyBadQty = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.BAD_QTY_CODE);
                    double qtyBadWeight = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.BAD_WEIGHT_CODE);
                    double qtyPoorQty = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.POOR_QTY_CODE);
                    double qtyPoorWeight = getQty(year, companyCode, estate, spuNumber, spuDate, BPNQuantity.POOR_WEIGHT_CODE);

                    listTemp.add(new SPUReportNumber(year, companyCode, estate, "04", spuNumber, nik, driver, licensePlate, qtyGoodQty, qtyGoodWeight, qtyBadQty, qtyBadWeight, qtyPoorQty, qtyPoorWeight));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<SPUReportNumber> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            adapter = new AdapterSPUReportNumber(SPUReportNumberActivity.this, listTemp, R.layout.item_spu_report_number);
            lsvSpuReportNumber.setAdapter(adapter);

            double totGoodQty = 0;
            double totGoodWeight = 0;
            double totBadQty = 0;
            double totBadWeight = 0;
            double totPoorQty = 0;
            double totPoorWeight = 0;

            if(listTemp != null && listTemp.size() > 0){
                for(int i = 0; i < listTemp.size(); i++){
                    SPUReportNumber spuReport = (SPUReportNumber) listTemp.get(i);

                    if(spuReport != null){
                        totGoodQty += (double) spuReport.getQtyGoodQty();
                        totGoodWeight += (double) spuReport.getQtyGoodWeight();

                        totBadQty += (double) spuReport.getQtyBadQty();
                        totBadWeight += (double) spuReport.getQtyBadWeight();

                        totPoorQty += (double) spuReport.getQtyPoorQty();
                        totPoorWeight += (double) spuReport.getQtyPoorWeight();
                    }
                }
            }


            txtSpuReportNumberTotGoodQty.setText(String.valueOf(Utils.round(totGoodQty, 2)));
            txtSpuReportNumberTotGoodWeight.setText(String.valueOf(Utils.round(totGoodWeight, 2)));

            txtSpuReportNumberTotBadQty.setText(String.valueOf(Utils.round(totBadQty, 2)));
            txtSpuReportNumberTotBadWeight.setText(String.valueOf(Utils.round(totBadWeight, 2)));

            txtSpuReportNumberTotPoorQty.setText(String.valueOf(Utils.round(totPoorQty, 2)));
            txtSpuReportNumberTotPoorWeight.setText(String.valueOf(Utils.round(totPoorWeight, 2)));
        }
    }

    private double getQty(String year, String companyCode, String estate, String spbsNumber, String spbsDate, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, SPBSLine.TABLE_NAME, null,
                SPBSLine.XML_YEAR + "=?" + " and " +
                        SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                        SPBSLine.XML_ESTATE + "=?" + " and " +
                        SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                        SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                        SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        SPBSLine.XML_CROP + "=?",
                new String [] {year, companyCode, estate, spbsNumber, spbsDate, achievementCode, "04"},
                null, null, null, null);
        database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                SPBSLine SPBSLine = (SPBSLine) lstQty.get(i);

                qty = qty + SPBSLine.getQuantityAngkut();
            }
        }

        return qty;
    }
}
