package com.simp.hms.activity.bpn;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNReportHarvester;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMReportHarvester;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BPNReportHarvester;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.ForemanActive;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Utils;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class BPNReportHarvesterActivity extends BaseActivity implements OnItemClickListener, OnClickListener, DialogDateListener{
	private Toolbar tbrMain;
	private TextView txtBpnReportHarvesterBpnDate;
	private Button btnBpnReportHarvesterSearch;
	private ListView lsvBpnReportHarvester;
	
	private TextView txtBpnReportHarvesterTotJanjang;
	private TextView txtBpnReportHarvesterTotLooseFruit;
	private TextView txtBpnReportHarvesterTotMentah;
	private TextView txtBpnReportHarvesterTotBusuk;
	private TextView txtBpnReportHarvesterTotTangkaiPanjang;
	
	private List<BPNReportHarvester> lstBPNReport;
	private AdapterBPNReportHarvester adapter;
	
	private GetDataAsyncTask getDataAsync;
	private DialogProgress dialogProgress;
	
	DatabaseHandler database = new DatabaseHandler(BPNReportHarvesterActivity.this);
	
	String bpnDate = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_bpn_report_harvester);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtBpnReportHarvesterBpnDate = (TextView) findViewById(R.id.txtBpnReportHarvesterBpnDate);
		btnBpnReportHarvesterSearch = (Button) findViewById(R.id.btnBpnReportHarvesterSearch);
		lsvBpnReportHarvester = (ListView) findViewById(R.id.lsvBpnReportHarvester);
		
		txtBpnReportHarvesterTotJanjang = (TextView) findViewById(R.id.txtBpnReportHarvesterTotJanjang);
		txtBpnReportHarvesterTotLooseFruit = (TextView) findViewById(R.id.txtBpnReportHarvesterTotLooseFruit);
		txtBpnReportHarvesterTotMentah = (TextView) findViewById(R.id.txtBpnReportHarvesterTotMentah);
		txtBpnReportHarvesterTotBusuk = (TextView) findViewById(R.id.txtBpnReportHarvesterTotBusuk);
		txtBpnReportHarvesterTotTangkaiPanjang = (TextView) findViewById(R.id.txtBpnReportHarvesterTotTangkaiPanjang);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
		btnActionBarRight.setVisibility(View.INVISIBLE);
		txtBpnReportHarvesterBpnDate.setOnClickListener(this);
		btnBpnReportHarvesterSearch.setOnClickListener(this);
		lsvBpnReportHarvester.setOnItemClickListener(this);
		
		bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		txtBpnReportHarvesterBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	
		adapter = new AdapterBPNReportHarvester(BPNReportHarvesterActivity.this, new ArrayList<BPNReportHarvester>(), R.layout.item_bpn_report_harvester);
		lsvBpnReportHarvester.setAdapter(adapter);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		BPNReportHarvester bpnReport = (BPNReportHarvester) adapter.getItem(pos);

		Bundle bundle = new Bundle();
		bundle.putParcelable(BPNReportHarvester.TABLE_NAME, bpnReport);
		
		startActivity(new Intent(BPNReportHarvesterActivity.this, BPNReportBlockActivity.class)
				.putExtras(bundle)
				.putExtra(BPNHeader.XML_BPN_DATE, bpnDate));
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.txtBpnReportHarvesterBpnDate:
			new DialogDate(BPNReportHarvesterActivity.this, getResources().getString(R.string.tanggal), 
					new Date(), R.id.txtBpnReportHarvesterBpnDate).show();
			break;
		case R.id.btnBpnReportHarvesterSearch:
			getDataAsync = new GetDataAsyncTask();
			getDataAsync.execute();
			break;   
		case R.id.btnActionBarLeft:   
			
			break;
		case R.id.btnActionBarRight:

			break;
		default:
			break;
		}
	}

	private class GetDataAsyncTask extends AsyncTask<Void, List<BPNReportHarvester>, List<BPNReportHarvester>>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(!isFinishing()){
				dialogProgress = new DialogProgress(BPNReportHarvesterActivity.this, getResources().getString(R.string.loading));
				dialogProgress.show();
			}
		}

		@Override 
		protected List<BPNReportHarvester> doInBackground(Void... voids) {
			
			String companyCode = "";
			String estate = "";
			String division = "";
			String gang = "";
			String nik = "";
			
			database.openTransaction();
			UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(userLogin != null){
				companyCode = userLogin.getCompanyCode();
				estate = userLogin.getEstate();
				division = userLogin.getDivision();
				gang = userLogin.getGang();
				nik = userLogin.getNik();
			}
			
//			database.openTransaction();
//			ForemanActive foremanActive = (ForemanActive) database.getDataFirst(false, ForemanActive.TABLE_NAME, null, null, null, null, null, null, null);
//			database.closeTransaction();
//			
//			if(foremanActive != null){
//				gang = foremanActive.getGang();
//			}
			
//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME, 
//					new String [] {BPNHeader.XML_NIK_HARVESTER, BPNHeader.XML_HARVESTER}, 
//					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//					BPNHeader.XML_ESTATE + "=?" + " and " +
//					BPNHeader.XML_DIVISION + "=?" + " and " +
//					BPNHeader.XML_GANG + "=?" + " and " +
//					BPNHeader.XML_NIK_CLERK + "=?" + " and " +   
//					BPNHeader.XML_BPN_DATE + "=?" + " and " +
//					BPNHeader.XML_CROP + "=?", 
//					new String [] {companyCode, estate, division, gang, nik, bpnDate, "01"},
//					BPNHeader.XML_NIK_HARVESTER + ", " + BPNHeader.XML_HARVESTER, null, BPNHeader.XML_HARVESTER, null);
//			database.closeTransaction();
			
			database.openTransaction();
			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME, 
					new String [] {BPNHeader.XML_NIK_HARVESTER, BPNHeader.XML_HARVESTER}, 
					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
					BPNHeader.XML_ESTATE + "=?" + " and " +
					BPNHeader.XML_DIVISION + "=?" + " and " +
					BPNHeader.XML_NIK_CLERK + "=?" + " and " +   
					BPNHeader.XML_BPN_DATE + "=?" + " and " +
					BPNHeader.XML_CROP + "=?", 
					new String [] {companyCode, estate, division, nik, bpnDate, "01"},
					BPNHeader.XML_NIK_HARVESTER + ", " + BPNHeader.XML_HARVESTER, null, BPNHeader.XML_HARVESTER, null);
			database.closeTransaction();
			
//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME, 
//					new String [] {BPNHeader.XML_NIK_HARVESTER, BPNHeader.XML_HARVESTER}, 
//					null, null,
//					BPNHeader.XML_NIK_HARVESTER + ", " + BPNHeader.XML_HARVESTER, null, BPNHeader.XML_HARVESTER, null);
//			database.closeTransaction();
			
			List<BPNReportHarvester> listTemp = new ArrayList<BPNReportHarvester>();
			
			if(listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BPNHeader bpnHeader = (BPNHeader) listObject.get(i);
					
					String nikHarvester = bpnHeader.getNikHarvester();
					String harvester = bpnHeader.getHarvester();

					double qtyJanjang = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.JANJANG_CODE);
					double qtyLooseFruit = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.LOOSE_FRUIT_CODE);
					
					double qtyMentah = getQly(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.JANJANG_CODE, BPNQuality.MENTAH_CODE);
					double qtyBusuk = getQly(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.JANJANG_CODE, BPNQuality.BUSUK_CODE);
					double qtyTangkaiPanjang = getQly(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.JANJANG_CODE, BPNQuality.TANGKAI_PANJANG_CODE);
					
					listTemp.add(new BPNReportHarvester(companyCode, estate, division, gang, nikHarvester, harvester, qtyJanjang, qtyLooseFruit, 
							qtyMentah, qtyBusuk, qtyTangkaiPanjang));
				}
			}
			
			return listTemp;
		}
		
		@Override
		protected void onPostExecute(List<BPNReportHarvester> listTemp) {
			super.onPostExecute(listTemp);
			
			if(dialogProgress != null && dialogProgress.isShowing()){
				dialogProgress.dismiss();
				dialogProgress = null;
			}
			
			adapter = new AdapterBPNReportHarvester(BPNReportHarvesterActivity.this, listTemp, R.layout.item_bpn_report_harvester);
			lsvBpnReportHarvester.setAdapter(adapter);
			
			int totJanjang = 0;
			double totLooseFruit = 0;
			int totMentah = 0;
			int totBusuk = 0;
			int totTangkaiPanjang = 0;
			
			
			if(listTemp != null && listTemp.size() > 0){
				for(int i = 0; i < listTemp.size(); i++){
					BPNReportHarvester bpnReport = listTemp.get(i);
					
					if(bpnReport != null){
						totJanjang += (int) bpnReport.getQtyJanjang();
						totLooseFruit += bpnReport.getQtyLooseFruit();
						totMentah += (int)  bpnReport.getQtyMentah();
						totBusuk += (int)  bpnReport.getQtyBusuk();
						totTangkaiPanjang += (int) bpnReport.getQtyTangkaiPanjang();
					}
				}
			}
			
			txtBpnReportHarvesterTotJanjang.setText(String.valueOf(totJanjang));
			txtBpnReportHarvesterTotLooseFruit.setText(String.valueOf(Utils.round(totLooseFruit, 2)));
			txtBpnReportHarvesterTotMentah.setText(String.valueOf(totMentah));
			txtBpnReportHarvesterTotBusuk.setText(String.valueOf(totBusuk));
			txtBpnReportHarvesterTotTangkaiPanjang.setText(String.valueOf(totTangkaiPanjang));
			
//			if(listTemp.size() > 0){
//				lstBPNReport = listTemp;
//				adapter = new AdapterBPNReportHarvester(BPNReportHarvesterActivity.this, lstBPNReport, R.layout.item_bpn_report_harvester);
//			}else{
//				adapter = null;
//			}
//			
//			lsvBpnReportHarvester.setAdapter(adapter);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
		
		if(dialogProgress != null && dialogProgress.isShowing()){
			dialogProgress.dismiss();
			dialogProgress = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
			getDataAsync.cancel(true);
		}
	}

	@Override
	public void onDateOK(Date date, int id) {
		bpnDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		
		txtBpnReportHarvesterBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
	}

	private double getQty(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String achievementCode){
		double qty = 0;
		
		database.openTransaction();
		List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null, 
				BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
				BPNQuantity.XML_ESTATE + "=?" + " and " +
				BPNQuantity.XML_DIVISION + "=?" + " and " +
				BPNQuantity.XML_BPN_DATE + "=?" + " and " +
				BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
				BPNQuantity.XML_ACHIEVEMENT_CODE + "=?" + " and " +
				BPNQuantity.XML_CROP + "=?", 
				new String [] {companyCode, estate, division, bpnDate, nikHarvester, achievementCode, "01"}, 
				null, null, null, null);
		database.closeTransaction();
		   
//		database.openTransaction();
//		List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null, 
//				null, null, null, null, null, null);
//		database.closeTransaction();
		
		if(lstQty.size() > 0){
			for(int i = 0; i < lstQty.size(); i++){
				BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);
				
				qty = qty + bpnQuantity.getQuantity();
			}
		}
		
		return qty;
	}
	
	private double getQly(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String achievementCode, String qualityCode){
		double qly = 0;
		
		database.openTransaction();
		List<Object> lstQly = database.getListData(false, BPNQuality.TABLE_NAME, null, 
				BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
				BPNQuality.XML_ESTATE + "=?" + " and " +
				BPNQuality.XML_DIVISION + "=?" + " and " +
				BPNQuality.XML_BPN_DATE + "=?" + " and " +
				BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
				BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
				BPNQuality.XML_QUALITY_CODE + "=?" + " and " +
				BPNQuality.XML_CROP + "=?",
				new String [] {companyCode, estate, division, bpnDate, nikHarvester, achievementCode, qualityCode, "01"},
				null, null, null, null);
		database.closeTransaction();
		
//		database.openTransaction();
//		List<Object> lstQly = database.getListData(false, BPNQuality.TABLE_NAME, null, 
//				null, null, null, null, null, null);
//		database.closeTransaction();
		
		if(lstQly.size() > 0){
			for(int i = 0; i < lstQly.size(); i++){
				BPNQuality bpnQuality = (BPNQuality) lstQly.get(i);
				
				qly = qly + bpnQuality.getQuantity();
			}
		}
		
		return qly;
	}
}
