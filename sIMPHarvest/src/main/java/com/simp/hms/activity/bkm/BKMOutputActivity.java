package com.simp.hms.activity.bkm;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simp.hms.R;
import com.simp.hms.R.id;
import com.simp.hms.R.layout;
import com.simp.hms.R.string;
import com.simp.hms.R.style;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.activity.other.BlockPlanningActivity;
import com.simp.hms.adapter.view.ViewBKMOutputItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.listener.DialogConfirmListener;
import com.simp.hms.listener.DialogNotificationListener;
import com.simp.hms.model.AbsentType;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.BLKPLT;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Constanta;
import com.simp.hms.routines.Utils;
import com.simp.hms.widget.EditTextCustom;

import android.app.ActionBar;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
 
public class BKMOutputActivity extends BaseActivity implements OnClickListener, DialogNotificationListener, TextWatcher, DialogConfirmListener, OnFocusChangeListener {
	private Toolbar tbrMain;
	private TextView txtBkmOutputDate;
	private TextView txtBkmOutputGang;
	private TextView txtBkmOutputBlock;
	private Button btnBkmOutputSearch;
	private EditTextCustom edtBkmOutputTotal;
	private TextView txtBkmOutputRemain;
	private LinearLayout lytBkmOutputHarvester;
	      
	private HashMap<String, BKMOutput> mapBKMOutput = new HashMap<String, BKMOutput>();
	
	private final int MST_FOREMAN = 101;
	private final int MST_CLERK = 102;
	private final int MST_HARVESTER = 103;
	private final int MST_GANG = 104;
	private final int MST_OUT = 105;
	private final int MST_BLOCK = 106;

	String imei = "";
	String companyCode = "";
	String estate = "";
	String bkmDate = "";
	String division = "";
	String gang = "";
	String block = "";
	double total = 0;
	double remain = 0;
	int status = 0;
	String nikForeman = "";
	String foreman = "";
	
	Handler repeatUpdateHandler = new Handler();
	boolean mAutoIncrement = false;
	boolean mAutoDecrement = false;

	DatabaseHandler database = new DatabaseHandler(BKMOutputActivity.this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		animOnStart();

		setContentView(R.layout.activity_bkm_output);

		tbrMain = (Toolbar) findViewById(R.id.tbrMain);
		txtBkmOutputDate = (TextView) findViewById(R.id.txtBkmOutputDate);
		txtBkmOutputGang = (TextView) findViewById(R.id.txtBkmOutputGang);
		txtBkmOutputBlock = (TextView) findViewById(R.id.txtBkmOutputBlock);
		btnBkmOutputSearch = (Button) findViewById(R.id.btn_bkm_output_search);
		edtBkmOutputTotal = (EditTextCustom) findViewById(R.id.edtBkmOutputTotal);
		txtBkmOutputRemain = (TextView) findViewById(R.id.txtBkmOutputRemain);
		lytBkmOutputHarvester = (LinearLayout) findViewById(R.id.lytBkmOutputHarvester);

		ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
		TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
		TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

		btnActionBarLeft.setOnClickListener(this);
		txtActionBarTitle.setText(getResources().getString(R.string.bkm_output));
		btnActionBarRight.setOnClickListener(this);
		btnActionBarRight.setText(getResources().getString(R.string.save));
		
		txtBkmOutputBlock.setOnClickListener(this);
		btnBkmOutputSearch.setOnClickListener(this);
		edtBkmOutputTotal.addTextChangedListener(this);
		edtBkmOutputTotal.setOnFocusChangeListener(this);
		
		database.openTransaction();
		UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
		database.closeTransaction();
		
		if(userLogin != null){
			imei = new DeviceHandler(BKMOutputActivity.this).getImei();
			companyCode = userLogin.getCompanyCode();
			estate = userLogin.getEstate();
			division = userLogin.getDivision();
			gang = userLogin.getGang();
			nikForeman = userLogin.getNik();
			foreman = userLogin.getName();
		}
		
		BKMOutput bkmOutput = null;
		
		if(getIntent().getExtras() != null){
			bkmOutput = (BKMOutput) getIntent().getParcelableExtra(BKMOutput.TABLE_NAME);
		}
		
		if(bkmOutput != null){
			imei = TextUtils.isEmpty(bkmOutput.getImei()) ? new DeviceHandler(BKMOutputActivity.this).getImei(): bkmOutput.getImei() ;
			companyCode = bkmOutput.getCompanyCode();
			estate = bkmOutput.getEstate();
			bkmDate = bkmOutput.getBkmDate();
			division = bkmOutput.getDivision();
			gang = bkmOutput.getGang();
			block = bkmOutput.getBlock();
			status = bkmOutput.getStatus();
		}else{  
			database.openTransaction();
			BKMHeader bkmHeader =  (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null, 
					BKMHeader.XML_COMPANY_CODE + "=?" + " and " + 
					BKMHeader.XML_ESTATE + "=?" + " and " +
					BKMHeader.XML_DIVISION + "=?" + " and " + 
					BKMHeader.XML_NIK_FOREMAN + "=?" + " and " +
					BKMHeader.XML_BKM_DATE + "=?", 
					new String [] {companyCode, estate, division, nikForeman, new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT)}, 
					null, null, null, null);
			database.closeTransaction();
			
			if(bkmHeader != null){
				imei = bkmHeader.getImei();
				companyCode = bkmHeader.getCompanyCode();
				estate = bkmHeader.getEstate();
				bkmDate = bkmHeader.getBkmDate();
				division = bkmHeader.getDivision();
				gang = bkmHeader.getGang();
				nikForeman = bkmHeader.getNikForeman();
				foreman = bkmHeader.getForeman();
				status = bkmHeader.getStatus();    
			}
			
		}
		
		initInputView();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		getHarvesters();
	}

	private void initInputView(){
		total = 0;
		remain = 0;
		
		txtBkmOutputDate.setText(new DateLocal(bkmDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		txtBkmOutputGang.setText(gang);
		txtBkmOutputBlock.setText(block);
		
		edtBkmOutputTotal.setText(total > 0 ? String.valueOf(total) : "");
		edtBkmOutputTotal.setImeOptions(EditorInfo.IME_ACTION_NEXT);
	}
	
	private void getHarvesters(){
		List<Object> listObject;
		
		lytBkmOutputHarvester.removeAllViews();
		mapBKMOutput = new HashMap<String, BKMOutput>();
		
		total = 0;
		remain = 0;
		
//		if(TextUtils.isEmpty(block)){
//			database.openTransaction();
//			BKMOutput bkmOutput = (BKMOutput) database.getDataFirst(false, BKMOutput.TABLE_NAME, null, 
//					BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
//					BKMOutput.XML_ESTATE + "=?" + " and " +
//					BKMOutput.XML_BKM_DATE + "=?" + " and " +
//					BKMOutput.XML_DIVISION + "=?" + " and " +
//					BKMOutput.XML_GANG + "=?", 
//					new String [] {companyCode, estate, bkmDate, division, gang},
//					null, null, null, null);
//			database.closeTransaction();
//			
//			if(bkmOutput != null){
//				block = bkmOutput.getBlock();
//				txtBkmOutputBlock.setText(block);
//			}
//		}
		
//		database.openTransaction();
//		listObject = database.getListData(false, BKMOutput.TABLE_NAME, null, 
//				BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
//				BKMOutput.XML_ESTATE + "=?" + " and " +
//				BKMOutput.XML_BKM_DATE + "=?" + " and " +
//				BKMOutput.XML_DIVISION + "=?" + " and " +
//				BKMOutput.XML_GANG + "=?" + " and " +
//				BKMOutput.XML_BLOCK + "=?", 
//				new String [] {companyCode, estate, bkmDate, division, gang, block}, 
//				null, null, BKMOutput.XML_NIK, null); 
//		database.closeTransaction();
//		
//		if(listObject.size() > 0){
//			total = 0;
//			remain = 0;
//			for(int i = 0; i < listObject.size(); i++){
//				BKMOutput bkmOutputTemp = (BKMOutput) listObject.get(i);
//				
//				total = total + bkmOutputTemp.getOutput();
//				
//				addChild(bkmOutputTemp);
//			}
//		}else{
		
//			String absentType = "KJ";
			
			database.openTransaction();
			List<Object> lstAbsent = database.getListData(false, AbsentType.TABLE_NAME, null, 
					AbsentType.XML_HKRLHI + " > ? ", 
					new String [] {"0"}, 
					null, null, null, null);
			database.closeTransaction();
			    
			database.openTransaction();
			listObject = database.getListData(false, BKMLine.TABLE_NAME, null, 
					BKMLine.XML_COMPANY_CODE + "=?" + " and " +
					BKMLine.XML_ESTATE + "=?" + " and " +
					BKMLine.XML_BKM_DATE + "=?" + " and " +
					BKMLine.XML_DIVISION + "=?" + " and " +
					BKMLine.XML_GANG + "=?", 
					new String [] {companyCode, estate, bkmDate, division, gang}, 
					null, null, BKMLine.XML_NIK, null);
			database.closeTransaction();   
			
			if(listObject != null && listObject.size() > 0){
				for(int i = 0; i < listObject.size(); i++){
					BKMLine bkmLine = (BKMLine) listObject.get(i);
					
					boolean valid = false;
					
					if(lstAbsent != null && lstAbsent.size() > 0){
						for(int j = 0; j < lstAbsent.size(); j++){
							AbsentType absentType = (AbsentType) lstAbsent.get(j);
							
							if(bkmLine.getAbsentType().equalsIgnoreCase(absentType.getAbsentType())){
								valid = true;
								break;
							}
						}
					}
					
					if(valid){
						
						database.openTransaction();
						Object objOutput = database.getDataFirst(false, BKMOutput.TABLE_NAME, null, 
								BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
								BKMOutput.XML_ESTATE + "=?" + " and " +
								BKMOutput.XML_BKM_DATE + "=?" + " and " +
								BKMOutput.XML_DIVISION + "=?" + " and " +
								BKMOutput.XML_GANG + "=?" + " and " +
								BKMOutput.XML_BLOCK + "=?" + " and " +
								BKMOutput.XML_NIK + "=?", 
								new String [] {companyCode, estate, bkmDate, division, gang, block, bkmLine.getNik()}, 
								null, null, BKMOutput.XML_NIK, null); 
						database.closeTransaction();
						
						BKMOutput bkmOutputTemp = null;
						
						if(objOutput == null){
							bkmOutputTemp = new BKMOutput(0, imei, companyCode, estate, bkmDate, division, gang, block, bkmLine.getNik(), bkmLine.getName(), 
									0.0, "HA", 0, 0, "", 0, "");
						}else{
							bkmOutputTemp = (BKMOutput) objOutput;
						}
					
						if(bkmOutputTemp != null){
							total = total + bkmOutputTemp.getOutput();
							addChild(bkmOutputTemp);
						}
					}
				}
			}
			
//			total = 0;
//			remain = 0;
//		}

		edtBkmOutputTotal.setText( total > 0 ? String.valueOf(Utils.round(total, 2)) : "");
	}    

	@Override
	public void onClick(View view) {  
		switch (view.getId()) {
		case R.id.txtBkmOutputBlock:
//			startActivityForResult(new Intent(BKMOutputActivity.this, MasterBlockHdrcActivity.class)
//			.putExtra(Constanta.SEARCH, true)
//			.putExtra(BlockHdrc.XML_COMPANY_CODE, companyCode)
//			.putExtra(BlockHdrc.XML_ESTATE, estate)
//			.putExtra(BlockHdrc.XML_DIVISION, division)
//			, MST_BLOCK);
			startActivityForResult(new Intent(BKMOutputActivity.this, BlockPlanningActivity.class)
					.putExtra("readOnly", true)
					.putExtra("croptype", ""), MST_BLOCK);
			break;
		case R.id.btn_bkm_output_search:

			break;
		case R.id.btnActionBarRight:
			if(!TextUtils.isEmpty(block)){
				double harvested = Double.MAX_VALUE;
				
				database.openTransaction();
				BLKPLT blkplt = (BLKPLT) database.getDataFirst(false, BLKPLT.TABLE_NAME, null, 
						BLKPLT.XML_BLOCK + "=?", 
						new String [] {block}, 
						null, null, null, null);
				database.closeTransaction();
				
				if(blkplt != null){
					harvested = blkplt.getHarvested() > 0 ? blkplt.getHarvested() : Double.MAX_VALUE;
				}
				
				if(total <= harvested){
					total = new Converter(edtBkmOutputTotal.getText().toString().trim()).StrToDouble();
					remain = new Converter(txtBkmOutputRemain.getText().toString().trim()).StrToDouble();
					
					if(total > 0 && remain == 0){  
						if(status == 1){
							new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
									getResources().getString(R.string.data_already_export), false).show();
						}else{
							database.openTransaction();
							List<Object> listObject = database.getListData(false, BKMOutput.TABLE_NAME, null, 
									BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
									BKMOutput.XML_ESTATE + "=?" + " and " +
									BKMOutput.XML_BKM_DATE + "=?" + " and " +
									BKMOutput.XML_DIVISION + "=?" + " and " +
									BKMOutput.XML_GANG + "=?" + " and " +
									BKMOutput.XML_BLOCK + "=?", 
									new String [] {companyCode, estate, bkmDate, division, gang, block}, 
									null, null, null, null);
							database.closeTransaction();
							
							if(listObject.size() > 0){
								new DialogConfirm(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
										getResources().getString(R.string.already_exists_replaced), 
										null, R.id.btnActionBarRight).show();
							}else{
								try{
									long todayDate = new Date().getTime();
									
									database.openTransaction();
									for(Map.Entry<String, BKMOutput> entry: mapBKMOutput.entrySet()){
										BKMOutput bkmOutput = entry.getValue();
										
										if(bkmOutput.getOutput() > 0){
											bkmOutput.setCreatedDate(todayDate);
											bkmOutput.setCreatedBy(foreman);
											bkmOutput.setModifiedDate(todayDate);
											bkmOutput.setModifiedBy(foreman);
											
											database.setData(bkmOutput);
										}
									}
									
									database.commitTransaction();
	
									new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
											getResources().getString(R.string.save_successed), false).show();
								}catch(SQLiteException e){
									e.printStackTrace();
									database.closeTransaction();
									new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
											e.getMessage(), false).show();
								}finally{
									database.closeTransaction();
								}
							}
						}
					}else{
						new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
								getResources().getString(R.string.bkm_total_remain_invalid), false).show();
					}
				}else{
					new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
							String.format(getResources().getString(R.string.bkm_output_total_invalid), String.valueOf(harvested)), false).show();
				}
			}else{
				new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.data_empty), false).show();
			}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode == RESULT_OK){
			if(requestCode == MST_BLOCK){
//				block = data.getExtras().getString(BlockHdrc.XML_BLOCK);
				block = data.getExtras().getString(BlockPlanning.XML_BLOCK);
				
				txtBkmOutputBlock.setText(block);

				initInputView();
				getHarvesters();
			}
		}
	}

	@Override
	public void onOK(boolean is_finish) {
		
	}

	@Override
	public void afterTextChanged(Editable editable) {
		total = Math.abs(new Converter(editable.toString()).StrToDouble());
		
		remain = total - getOutput();
		displayRemain(remain);
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
	
	private void addChild(final BKMOutput bkmOutput){
		TextView txtBkmOutputItemNumber;
		TextView txtBkmOutputItemName;
		TextView txtBkmOutputItemAbsentType;
		TextView txtBkmOutputItemMandays;
		Button btnBKMOutputItemOutputMin;
		final EditTextCustom edtBkmOutputItemOutput;
		Button btnBKMOutputItemOutputPlus;
		
		final ViewBKMOutputItem viewBKMOutputItem;
		final View child = getLayoutInflater().inflate(R.layout.item_bkm_output, null);
		
		txtBkmOutputItemNumber = (TextView) child.findViewById(R.id.txtBkmOutputItemNumber);
        txtBkmOutputItemName = (TextView) child.findViewById(R.id.txtBkmOutputItemName3);
        txtBkmOutputItemAbsentType = (TextView) child.findViewById(R.id.txtBkmOutputItemAbsentType3);
        txtBkmOutputItemMandays = (TextView) child.findViewById(R.id.txtBkmOutputItemMandays3);
        btnBKMOutputItemOutputMin = (Button) child.findViewById(R.id.btnBKMOutputItemOutputMin3);
        edtBkmOutputItemOutput = (EditTextCustom) child.findViewById(R.id.edtBkmOutputItemOutput3);
        btnBKMOutputItemOutputPlus = (Button) child.findViewById(R.id.btnBKMOutputItemOutputPlus3);
        
        viewBKMOutputItem = new ViewBKMOutputItem(txtBkmOutputItemName, txtBkmOutputItemAbsentType, 
        		txtBkmOutputItemMandays, btnBKMOutputItemOutputMin, edtBkmOutputItemOutput, btnBKMOutputItemOutputPlus, true);
	
        viewBKMOutputItem.getTxtBkmOutputItemName().setText(bkmOutput.getName());
        viewBKMOutputItem.getTxtBkmOutputItemAbsentType().setText("");
        
        viewBKMOutputItem.getEdtBKMOutputItemOutput().setText(bkmOutput.getOutput() > 0? String.valueOf(Utils.round(bkmOutput.getOutput(), 2)) : "");

        txtBkmOutputItemName.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				edtBkmOutputItemOutput.requestFocus();
			}
		});
        
        viewBKMOutputItem.getBtnBKMOutputItemOutputMin().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				decrement(viewBKMOutputItem.getEdtBKMOutputItemOutput(), bkmOutput);
			}
		});
        
        viewBKMOutputItem.getBtnBKMOutputItemOutputMin().setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View view) {
				mAutoDecrement = true;
	            repeatUpdateHandler.post( new RptUpdater(viewBKMOutputItem.getEdtBKMOutputItemOutput(), bkmOutput) );
	            
				return false;
			}
		});
        
        viewBKMOutputItem.getBtnBKMOutputItemOutputMin().setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				if( (event.getAction() == MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL) && mAutoDecrement ){
					mAutoDecrement = false;
	            }
				return false;
			}
		});
        
        viewBKMOutputItem.getBtnBKMOutputItemOutputPlus().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				increment(viewBKMOutputItem.getEdtBKMOutputItemOutput(), bkmOutput);
			}
		});
        
        viewBKMOutputItem.getBtnBKMOutputItemOutputPlus().setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View view) {
				mAutoIncrement = true;
	            repeatUpdateHandler.post( new RptUpdater(viewBKMOutputItem.getEdtBKMOutputItemOutput(), bkmOutput) );
	            
				return false;
			}
		});
        
        viewBKMOutputItem.getBtnBKMOutputItemOutputPlus().setOnTouchListener(new OnTouchListener() {
			   
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				if( (event.getAction() == MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL) && mAutoIncrement ){
					mAutoIncrement = false;
	            }
				return false;
			}
		});
        
        
        viewBKMOutputItem.getEdtBKMOutputItemOutput().setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View arg0, boolean arg1) {
				viewBKMOutputItem.getEdtBKMOutputItemOutput().setSelection(viewBKMOutputItem.getEdtBKMOutputItemOutput().getText().length());
				
			}
		});
        
        viewBKMOutputItem.getEdtBKMOutputItemOutput().addTextChangedListener(new AbsoluteEditTextWatcher( viewBKMOutputItem.getEdtBKMOutputItemOutput(), bkmOutput));
        
//        viewBKMOutputItem.getEdtBKMOutputItemOutput().addTextChangedListener(new TextWatcher() {
//			
//			@Override
//			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
//			
//			@Override
//			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}
//			
//			@Override
//			public void afterTextChanged(Editable editable) {
//				double mValue = 0.0;
//				
//				if(new Converter(editable.toString()).StrToDouble() < 0){
//					mValue = 0.0;
//				}else{
//					mValue = new Converter(editable.toString()).StrToDouble();
//				}
//					
//				bkmOutput.setOutput(mValue);
//				mapBKMOutput.put(bkmOutput.getNik(), bkmOutput);
//				
//			    remain = total - getOutput();
//			    txtBkmOutputRemain.setText(new DecimalFormat("##.##").format(remain));
//			}
//		});
        
        mapBKMOutput.put(bkmOutput.getNik(), bkmOutput);
        lytBkmOutputHarvester.addView(child);
        
        txtBkmOutputItemNumber.setText(String.valueOf(lytBkmOutputHarvester.getChildCount()));
	}
	
	class RptUpdater implements Runnable {
		private EditText edt;
		private int pos;
		private BKMOutput bkmAbsent;
		
		public RptUpdater(EditText edt, BKMOutput bkmAbsent){
			this.edt = edt;
			this.bkmAbsent = bkmAbsent;
		}
		
	    public void run() {
	        if( mAutoIncrement ){
	            increment(edt, bkmAbsent);
	            repeatUpdateHandler.postDelayed( new RptUpdater(edt, bkmAbsent), 200 );
	        } else if( mAutoDecrement ){
	        	decrement(edt, bkmAbsent);
	            repeatUpdateHandler.postDelayed( new RptUpdater(edt, bkmAbsent), 200 );
	        }
	    }
	}
	
	public void increment(EditText edt, BKMOutput bkmOutput){
		double mValue;
		String value = edt.getText().toString().trim();
		
		mValue = new Converter(new DecimalFormat("##.##").format(new Converter(value).StrToDouble() + 0.01)).StrToDouble(); 
		
	    edt.setText(String.valueOf(mValue));
	    bkmOutput.setOutput(mValue);
	    mapBKMOutput.put(bkmOutput.getNik(), bkmOutput);
	    
	    remain = total - getOutput();
	    displayRemain(remain);
	}
	
	public void decrement(EditText edt, BKMOutput bkmOutput){
		double mValue;
		String value = edt.getText().toString().trim();
		
		if(new Converter(value).StrToDouble() > 0){
			mValue = new Converter(new DecimalFormat("##.##").format(new Converter(value).StrToDouble() - 0.01)).StrToDouble(); 
			
		    edt.setText(String.valueOf(mValue));
		    bkmOutput.setOutput(mValue);
		    mapBKMOutput.put(bkmOutput.getNik(), bkmOutput);
		    
		    remain = total - getOutput();
		    displayRemain(remain);
		}
	}
	
	private double getOutput(){
		remain = 0;
		
		for(Map.Entry<String, BKMOutput> entry: mapBKMOutput.entrySet()){
			BKMOutput bkmOutput = entry.getValue();
			
			remain = Utils.round(remain + bkmOutput.getOutput(), 2);
			Log.d("tag", "remain:" + remain);
		}

		return remain;
	}

	@Override
	public void onConfirmOK(Object object, int id) {
		switch (id) {
		case R.id.btnActionBarRight:
			try{
				database.openTransaction();
				database.deleteData(BKMOutput.TABLE_NAME, 
					BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
					BKMOutput.XML_ESTATE + "=?" + " and " +
					BKMOutput.XML_BKM_DATE + "=?" + " and " +
					BKMOutput.XML_DIVISION + "=?" + " and " +
					BKMOutput.XML_GANG + "=?" + " and " +
					BKMOutput.XML_BLOCK + "=?", 
					new String [] {companyCode, estate, bkmDate, division, gang, block});
			
				long todayDate = new Date().getTime();
				
				for(Map.Entry<String, BKMOutput> entry: mapBKMOutput.entrySet()){
					BKMOutput bkmOutput = entry.getValue();
					
					if(bkmOutput.getOutput() > 0){
						bkmOutput.setImei(imei);
						bkmOutput.setCreatedDate(todayDate);
						bkmOutput.setCreatedBy(foreman);
						bkmOutput.setModifiedDate(todayDate);
						bkmOutput.setModifiedBy(foreman); 
						database.setData(bkmOutput);
					}
					
//					BKMOutput bkmOutputCheck =  (BKMOutput) database.getDataFirst(false, BKMOutput.TABLE_NAME, null, 
//							BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
//							BKMOutput.XML_ESTATE + "=?" + " and " +
//							BKMOutput.XML_BKM_DATE + "=?" + " and " +
//							BKMOutput.XML_DIVISION + "=?" + " and " +
//							BKMOutput.XML_GANG + "=?" + " and " +
//							BKMOutput.XML_BLOCK + "=?" + " and " +
//							BKMOutput.XML_NIK + "=?", 
//							new String [] {companyCode, estate, bkmDate, division, gang, block, bkmOutput.getNik()},
//							null, null, null, null);
//					
//					if(bkmOutputCheck != null){
//						if(bkmOutput.getOutput() > 0){
//							if(bkmOutput.getOutput() != bkmOutputCheck.getOutput()){
//								bkmOutput.setCreatedDate(bkmOutputCheck.getCreatedDate());
//								bkmOutput.setCreatedBy(bkmOutputCheck.getCreatedBy());
//								bkmOutput.setModifiedDate(todayDate);
//								bkmOutput.setModifiedBy(foreman); 
//								
//								database.updateData(bkmOutput, 
//										BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
//										BKMOutput.XML_ESTATE + "=?" + " and " +
//										BKMOutput.XML_BKM_DATE + "=?" + " and " +
//										BKMOutput.XML_DIVISION + "=?" + " and " +
//										BKMOutput.XML_GANG + "=?" + " and " +
//										BKMOutput.XML_BLOCK + "=?" + " and " +
//										BKMOutput.XML_NIK + "=?", 
//										new String [] {companyCode, estate, bkmDate, division, gang, block, bkmOutput.getNik()});
//							}
//						}else{
//							database.deleteData(BKMOutput.TABLE_NAME, 
//									BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
//											BKMOutput.XML_ESTATE + "=?" + " and " +
//											BKMOutput.XML_BKM_DATE + "=?" + " and " +
//											BKMOutput.XML_DIVISION + "=?" + " and " +
//											BKMOutput.XML_GANG + "=?" + " and " +
//											BKMOutput.XML_BLOCK + "=?" + " and " +
//											BKMOutput.XML_NIK + "=?", 
//											new String [] {companyCode, estate, bkmDate, division, gang, block, bkmOutput.getNik()});
//						}
//					}else{
//						if(bkmOutput.getOutput() > 0){
//							bkmOutput.setCreatedDate(todayDate);
//							bkmOutput.setCreatedBy(foreman);
//							bkmOutput.setModifiedDate(todayDate);
//							bkmOutput.setModifiedBy(foreman); 
//							database.setData(bkmOutput);
//						}
//					}
				}
				
				database.commitTransaction();

				new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
						getResources().getString(R.string.save_successed), false).show();
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();
				new DialogNotification(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
						e.getMessage(), false).show();
			}finally{
				database.closeTransaction();
			}
			
			break;
		default:
			finish();
			animOnFinish();
			break;
		}
	}

	@Override
	public void onFocusChange(View view, boolean arg1) {
		double value = Math.abs(new Converter(edtBkmOutputTotal.getText().toString().trim()).StrToDouble());
		
		edtBkmOutputTotal.setText(value > 0 ? String.valueOf(Utils.round(value,2)) : "");
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		
		new DialogConfirm(BKMOutputActivity.this, getResources().getString(R.string.informasi), 
				getResources().getString(R.string.exit), null, 1).show();
	}
	
	private class AbsoluteEditTextWatcher implements TextWatcher{
		private EditText edt;
		private BKMOutput bkmOutput;
		
		String before = "";
		String after = "";
		String on = "";
		
		public AbsoluteEditTextWatcher(EditText edt, BKMOutput bkmOutput){
			this.edt = edt;
			this.bkmOutput = bkmOutput;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,int after) {
			before = s.toString();
		}
			
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {		
			on = s.toString();
		}

		@Override
		public void afterTextChanged(Editable s) {
			edt.removeTextChangedListener(this);
			after = s.toString().trim();
			double value;
			
			if(!after.equals(".")){
				value = new Converter(after).StrToDouble();
				
				if(checkDot(after) > 1){
					edt.setText(before);
					value = new Converter(before).StrToDouble();
				}else{
					edt.setText(after);
					value = new Converter(after).StrToDouble();
				}
				
			}else{
				edt.setText(before);
				value = new Converter(before).StrToDouble();
			}
			
			
			bkmOutput.setOutput(value);
			mapBKMOutput.put(bkmOutput.getNik(), bkmOutput);
			
			remain = total - getOutput();
			displayRemain(remain);
			
			edt.addTextChangedListener(this);
		}
	}
	
	private int checkDot(String value){
		int found = 0;
		
		for(int i = 0; i < value.length(); i++){
			char a = value.charAt(i);
			
			if(a == '.'){
				found++;
			}
		}
		
		return found;
	}
	
	private void displayRemain(double remain){
		String str = String.valueOf(remain);
		
		remain = new Converter(str).StrToDouble();
		
	    txtBkmOutputRemain.setText(new DecimalFormat("##.##").format(remain));
	}
}
