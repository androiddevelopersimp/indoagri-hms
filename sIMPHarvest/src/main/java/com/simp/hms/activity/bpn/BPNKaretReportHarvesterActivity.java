package com.simp.hms.activity.bpn;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.simp.hms.R;
import com.simp.hms.activity.BaseActivity;
import com.simp.hms.adapter.AdapterBPNCocoaReportHarvester;
import com.simp.hms.adapter.AdapterBPNKaretReportHarvester;
import com.simp.hms.adapter.AdapterBPNReportHarvester;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDate;
import com.simp.hms.dialog.DialogProgress;
import com.simp.hms.listener.DialogDateListener;
import com.simp.hms.model.BPNCocoaReportHarvester;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNKaretReportHarvester;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BPNReportHarvester;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.UserLogin;
import com.simp.hms.routines.Utils;

import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BPNKaretReportHarvesterActivity extends BaseActivity implements OnItemClickListener, OnClickListener, DialogDateListener {
    private Toolbar tbrMain;
    private TextView txtBpnReportHarvesterBpnDate;
    private Button btnBpnReportHarvesterSearch;
    private ListView lsvBpnReportHarvester;

    private TextView txtBpnReportHarvesterLatexWet;
    private TextView txtBpnReportHarvesterLatexDRC;
    private TextView txtBpnReportHarvesterLatexDry;
    private TextView txtBpnReportHarvesterLumpWet;
    private TextView txtBpnReportHarvesterLumpDRC;
    private TextView txtBpnReportHarvesterLumpDry;
    private TextView txtBpnReportHarvesterSlabWet;
    private TextView txtBpnReportHarvesterSlabDRC;
    private TextView txtBpnReportHarvesterSlabDry;
    private TextView txtBpnReportHarvesterTreelaceWet;
    private TextView txtBpnReportHarvesterTreelaceDRC;
    private TextView txtBpnReportHarvesterTreelaceDry;


    private List<BPNReportHarvester> lstBPNReport;
    private AdapterBPNKaretReportHarvester adapter;

    private BPNKaretReportHarvesterActivity.GetDataAsyncTask getDataAsync;
    private DialogProgress dialogProgress;

    DatabaseHandler database = new DatabaseHandler(BPNKaretReportHarvesterActivity.this);

    String bpnDate = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        animOnStart();

        setContentView(R.layout.activity_bpn_karet_report_harvester);

        tbrMain = (Toolbar) findViewById(R.id.tbrMain);
        txtBpnReportHarvesterBpnDate = (TextView) findViewById(R.id.txtBpnReportHarvesterBpnDate);
        btnBpnReportHarvesterSearch = (Button) findViewById(R.id.btnBpnReportHarvesterSearch);
        lsvBpnReportHarvester = (ListView) findViewById(R.id.lsvBpnReportHarvester);

        txtBpnReportHarvesterLatexWet = (TextView) findViewById(R.id.txtBpnReportHarvesterLatexWet);
        txtBpnReportHarvesterLatexDRC = (TextView) findViewById(R.id.txtBpnReportHarvesterLatexDRC);
        txtBpnReportHarvesterLatexDry = (TextView) findViewById(R.id.txtBpnReportHarvesterLatexDry);
        txtBpnReportHarvesterLumpWet = (TextView) findViewById(R.id.txtBpnReportHarvesterLumpWet);
        txtBpnReportHarvesterLumpDRC = (TextView) findViewById(R.id.txtBpnReportHarvesterLumpDRC);
        txtBpnReportHarvesterLumpDry = (TextView) findViewById(R.id.txtBpnReportHarvesterLumpDry);
        txtBpnReportHarvesterSlabWet = (TextView) findViewById(R.id.txtBpnReportHarvesterSlabWet);
        txtBpnReportHarvesterSlabDRC = (TextView) findViewById(R.id.txtBpnReportHarvesterSlabDRC);
        txtBpnReportHarvesterSlabDry = (TextView) findViewById(R.id.txtBpnReportHarvesterSlabDry);
        txtBpnReportHarvesterTreelaceWet = (TextView) findViewById(R.id.txtBpnReportHarvesterTreelaceWet);
        txtBpnReportHarvesterTreelaceDRC = (TextView) findViewById(R.id.txtBpnReportHarvesterTreelaceDRC);
        txtBpnReportHarvesterTreelaceDry = (TextView) findViewById(R.id.txtBpnReportHarvesterTreelaceDry);

        ImageButton btnActionBarLeft = (ImageButton) tbrMain.findViewById(R.id.btnActionBarLeft);
        TextView txtActionBarTitle = (TextView) tbrMain.findViewById(R.id.txtActionBarTitle);
        TextView btnActionBarRight = (TextView) tbrMain.findViewById(R.id.btnActionBarRight);

        btnActionBarLeft.setOnClickListener(this);
        txtActionBarTitle.setText(getResources().getString(R.string.buku_panen));
        btnActionBarRight.setVisibility(View.INVISIBLE);
        txtBpnReportHarvesterBpnDate.setOnClickListener(this);
        btnBpnReportHarvesterSearch.setOnClickListener(this);
        lsvBpnReportHarvester.setOnItemClickListener(this);

        bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        txtBpnReportHarvesterBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));

        adapter = new AdapterBPNKaretReportHarvester(BPNKaretReportHarvesterActivity.this, new ArrayList<BPNKaretReportHarvester>(), R.layout.item_bpn_cocoa_report_harvester);
        lsvBpnReportHarvester.setAdapter(adapter);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

        BPNKaretReportHarvester bpnReport = (BPNKaretReportHarvester) adapter.getItem(pos);

        Bundle bundle = new Bundle();
        bundle.putParcelable(BPNKaretReportHarvester.TABLE_NAME, bpnReport);

        startActivity(new Intent(BPNKaretReportHarvesterActivity.this, BPNKaretReportBlockActivity.class)
                .putExtras(bundle)
                .putExtra(BPNHeader.XML_BPN_DATE, bpnDate));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtBpnReportHarvesterBpnDate:
                new DialogDate(BPNKaretReportHarvesterActivity.this, getResources().getString(R.string.tanggal),
                        new Date(), R.id.txtBpnReportHarvesterBpnDate).show();
                break;
            case R.id.btnBpnReportHarvesterSearch:
                getDataAsync = new BPNKaretReportHarvesterActivity.GetDataAsyncTask();
                getDataAsync.execute();
                break;
            case R.id.btnActionBarLeft:

                break;
            case R.id.btnActionBarRight:

                break;
            default:
                break;
        }
    }

    private class GetDataAsyncTask extends AsyncTask<Void, List<BPNKaretReportHarvester>, List<BPNKaretReportHarvester>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!isFinishing()){
                dialogProgress = new DialogProgress(BPNKaretReportHarvesterActivity.this, getResources().getString(R.string.loading));
                dialogProgress.show();
            }
        }

        @Override
        protected List<BPNKaretReportHarvester> doInBackground(Void... voids) {

            String companyCode = "";
            String estate = "";
            String division = "";
            String gang = "";
            String nik = "";

            database.openTransaction();
            UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
            database.closeTransaction();

            if(userLogin != null){
                companyCode = userLogin.getCompanyCode();
                estate = userLogin.getEstate();
                division = userLogin.getDivision();
                gang = userLogin.getGang();
                nik = userLogin.getNik();
            }

//			database.openTransaction();
//			ForemanActive foremanActive = (ForemanActive) database.getDataFirst(false, ForemanActive.TABLE_NAME, null, null, null, null, null, null, null);
//			database.closeTransaction();
//
//			if(foremanActive != null){
//				gang = foremanActive.getGang();
//			}

//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
//					new String [] {BPNHeader.XML_NIK_HARVESTER, BPNHeader.XML_HARVESTER},
//					BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//					BPNHeader.XML_ESTATE + "=?" + " and " +
//					BPNHeader.XML_DIVISION + "=?" + " and " +
//					BPNHeader.XML_GANG + "=?" + " and " +
//					BPNHeader.XML_NIK_CLERK + "=?" + " and " +
//					BPNHeader.XML_BPN_DATE + "=?" + " and " +
//					BPNHeader.XML_CROP + "=?",
//					new String [] {companyCode, estate, division, gang, nik, bpnDate, "01"},
//					BPNHeader.XML_NIK_HARVESTER + ", " + BPNHeader.XML_HARVESTER, null, BPNHeader.XML_HARVESTER, null);
//			database.closeTransaction();

            database.openTransaction();
            List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
                    new String [] {BPNHeader.XML_NIK_HARVESTER, BPNHeader.XML_HARVESTER},
                    BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                            BPNHeader.XML_ESTATE + "=?" + " and " +
                            BPNHeader.XML_DIVISION + "=?" + " and " +
                            BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                            BPNHeader.XML_BPN_DATE + "=?" + " and " +
                            BPNHeader.XML_CROP + "=?",
                    new String [] {companyCode, estate, division, nik, bpnDate, "02"},
                    BPNHeader.XML_NIK_HARVESTER + ", " + BPNHeader.XML_HARVESTER, null, BPNHeader.XML_HARVESTER, null);
            database.closeTransaction();

//			database.openTransaction();
//			List<Object> listObject =  database.getListData(true, BPNHeader.TABLE_NAME,
//					new String [] {BPNHeader.XML_NIK_HARVESTER, BPNHeader.XML_HARVESTER},
//					null, null,
//					BPNHeader.XML_NIK_HARVESTER + ", " + BPNHeader.XML_HARVESTER, null, BPNHeader.XML_HARVESTER, null);
//			database.closeTransaction();

            List<BPNKaretReportHarvester> listTemp = new ArrayList<BPNKaretReportHarvester>();

            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    BPNHeader bpnHeader = (BPNHeader) listObject.get(i);

                    String nikHarvester = bpnHeader.getNikHarvester();
                    String harvester = bpnHeader.getHarvester();

                    double qtyLatexWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.LATEX_WET_CODE);
                    double qtyLatexDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.LATEX_DRC_CODE);
                    double qtyLatexDry = (double) ((qtyLatexWet * qtyLatexDRC) / 100);

                    double qtyLumpWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.LUMP_WET_CODE);
                    double qtyLumpDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.LUMP_DRC_CODE);
                    double qtyLumpDry = (double) ((qtyLumpWet * qtyLumpDRC) / 100);

                    double qtySlabWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.SLAB_WET_CODE);
                    double qtySlabDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.SLAB_DRC_CODE);
                    double qtySlabDry = (double) ((qtySlabWet * qtySlabDRC) /100);

                    double qtyTreelaceWet = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.TREELACE_WET_CODE);
                    double qtyTreelaceDRC = getQty(companyCode, estate, division, gang, bpnDate, nikHarvester, BPNQuantity.TREELACE_DRC_CODE);
                    double qtyTreelaceDry = (double) ((qtyTreelaceWet * qtyTreelaceDRC) / 100);

                    listTemp.add(new BPNKaretReportHarvester(companyCode, estate, division, gang, nikHarvester, harvester,
                            qtyLatexWet, qtyLatexDRC, qtyLatexDry,
                            qtyLumpWet, qtyLumpDRC, qtyLumpDry,
                            qtySlabWet, qtySlabDRC, qtySlabDry,
                            qtyTreelaceWet, qtyTreelaceDRC, qtyTreelaceDry));
                }
            }

            return listTemp;
        }

        @Override
        protected void onPostExecute(List<BPNKaretReportHarvester> listTemp) {
            super.onPostExecute(listTemp);

            if(dialogProgress != null && dialogProgress.isShowing()){
                dialogProgress.dismiss();
                dialogProgress = null;
            }

            adapter = new AdapterBPNKaretReportHarvester(BPNKaretReportHarvesterActivity.this, listTemp, R.layout.item_bpn_karet_report_harvester);
            lsvBpnReportHarvester.setAdapter(adapter);

            double totLatexWet = 0;
            double totLatexDRC = 0;
            double totLatexDry = 0;
            double totLumpWet = 0;
            double totLumpDRC = 0;
            double totLumpDry = 0;
            double totSlabWet = 0;
            double totSlabDRC = 0;
            double totSlabDry = 0;
            double totTreelaceWet = 0;
            double totTreelaceDRC = 0;
            double totTreelaceDry = 0;


            if(listTemp != null && listTemp.size() > 0){
                for(int i = 0; i < listTemp.size(); i++){
                    BPNKaretReportHarvester bpnReport = listTemp.get(i);

                    if(bpnReport != null){
                        totLatexWet += (double) bpnReport.getQtyLatexWet();
                        totLatexDRC += (double) bpnReport.getQtyLatexDRC();
                        totLatexDry += (double) bpnReport.getQtyLatexDry();

                        totLumpWet += (double) bpnReport.getQtyLumpWet();
                        totLumpDRC += (double) bpnReport.getQtyLumpDRC();
                        totLumpDry += (double) bpnReport.getQtyLumpDry();

                        totSlabWet += (double) bpnReport.getQtySlabWet();
                        totSlabDRC += (double) bpnReport.getQtySlabDRC();
                        totSlabDry += (double) bpnReport.getQtySlabDry();

                        totTreelaceWet += (double) bpnReport.getQtyTreelaceWet();
                        totTreelaceDRC += (double) bpnReport.getQtyTreelaceDRC();
                        totTreelaceDry += (double) bpnReport.getQtyTreelaceDry();
                    }
                }
            }


            txtBpnReportHarvesterLatexWet.setText(String.valueOf(Utils.round(totLatexWet, 2)));
            txtBpnReportHarvesterLatexDRC.setText(String.valueOf(Utils.round(totLatexDRC, 2)));
            txtBpnReportHarvesterLatexDry.setText(String.valueOf(Utils.round(totLatexDry, 2)));
            txtBpnReportHarvesterLumpWet.setText(String.valueOf(Utils.round(totLumpWet, 2)));
            txtBpnReportHarvesterLumpDRC.setText(String.valueOf(Utils.round(totLumpDRC, 2)));
            txtBpnReportHarvesterLumpDry.setText(String.valueOf(Utils.round(totLumpDry, 2)));
            txtBpnReportHarvesterSlabWet.setText(String.valueOf(Utils.round(totSlabWet, 2)));
            txtBpnReportHarvesterSlabDRC.setText(String.valueOf(Utils.round(totSlabDRC, 2)));
            txtBpnReportHarvesterSlabDry.setText(String.valueOf(Utils.round(totSlabDry, 2)));
            txtBpnReportHarvesterTreelaceWet.setText(String.valueOf(Utils.round(totTreelaceWet, 2)));
            txtBpnReportHarvesterTreelaceDRC.setText(String.valueOf(Utils.round(totTreelaceDRC, 2)));
            txtBpnReportHarvesterTreelaceDry.setText(String.valueOf(Utils.round(totTreelaceDry, 2)));

//			if(listTemp.size() > 0){
//				lstBPNReport = listTemp;
//				adapter = new AdapterBPNReportHarvester(BPNReportHarvesterActivity.this, lstBPNReport, R.layout.item_bpn_report_harvester);
//			}else{
//				adapter = null;
//			}
//
//			lsvBpnReportHarvester.setAdapter(adapter);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }

        if(dialogProgress != null && dialogProgress.isShowing()){
            dialogProgress.dismiss();
            dialogProgress = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(getDataAsync != null && getDataAsync.getStatus() != AsyncTask.Status.FINISHED){
            getDataAsync.cancel(true);
        }
    }

    @Override
    public void onDateOK(Date date, int id) {
        bpnDate = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);

        txtBpnReportHarvesterBpnDate.setText(new DateLocal(bpnDate, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
    }


    private double getQty(String companyCode, String estate, String division, String gang, String bpnDate, String nikHarvester, String achievementCode){
        double qty = 0;

        database.openTransaction();
        List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                        BPNQuantity.XML_ESTATE + "=?" + " and " +
                        BPNQuantity.XML_DIVISION + "=?" + " and " +
                        BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                        BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                        BPNQuantity.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                        BPNQuantity.XML_CROP + "=?",
                new String [] {companyCode, estate, division, bpnDate, nikHarvester, achievementCode, "02"},
                null, null, null, null);
        database.closeTransaction();

//		database.openTransaction();
//		List<Object> lstQty = database.getListData(false, BPNQuantity.TABLE_NAME, null,
//				null, null, null, null, null, null);
//		database.closeTransaction();

        if(lstQty.size() > 0){
            for(int i = 0; i < lstQty.size(); i++){
                BPNQuantity bpnQuantity = (BPNQuantity) lstQty.get(i);

                qty = qty + bpnQuantity.getQuantity();
            }
        }

        return qty;
    }

}
