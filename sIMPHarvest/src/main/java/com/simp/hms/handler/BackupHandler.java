package com.simp.hms.handler;

import java.util.Calendar;
import java.util.TimeZone;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.simp.hms.receiver.BackupReceiver;
import com.simp.hms.service.BackupService;
import com.simp.hms.service.GPSTriggerService;

public class BackupHandler {

	private Context context;
	private PendingIntent pendingIntent;
	private AlarmManager alarmManager;
	
	private static final long REPEAT_TIME = 1000 * 30;
	
	public BackupHandler(Context context){
		this.context = context;
	}
	
	public void startBackup() {    
		try{
			Calendar updateTime = Calendar.getInstance();
			updateTime.setTimeZone(TimeZone.getDefault());   
        
			Intent intent = new Intent(context, BackupService.class);        
		  
			pendingIntent = PendingIntent.getService(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);  
			alarmManager.setRepeating(AlarmManager.RTC, updateTime.getTimeInMillis(), REPEAT_TIME, pendingIntent);
        
			Toast.makeText(context, "Backup Service: Start", Toast.LENGTH_SHORT).show();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void stopBackup(){
		try{
			Intent intent = new Intent(context, BackupService.class);        
				
			context.stopService(intent);
				
			pendingIntent = PendingIntent.getService(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);  
			alarmManager.cancel(pendingIntent);
				
			Toast.makeText(context, "Backup Service: Stop", Toast.LENGTH_SHORT).show();
   		}catch (Exception e){
   			e.printStackTrace();
   		}
	}
}
