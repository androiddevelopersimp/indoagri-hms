package com.simp.hms.handler;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkHandler {
	private Context context;
	
	public NetworkHandler(Context context){
		this.context = context;
	}
	
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		  
		if (ni == null) {
			return false;
		} else
			return true;
		}
}
