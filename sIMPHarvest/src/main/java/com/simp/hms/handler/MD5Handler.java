package com.simp.hms.handler;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.util.Base64;
import android.util.Log;

public class MD5Handler {
	private String deviceId;
	private String todayDate;
	
	public static int NO_OPTIONS = 0;
	
	public MD5Handler(String deviceId, String todayDate){
		this.deviceId = deviceId;
		this.todayDate = todayDate;
	}
    
    private String computeMD5Hash(){
    	String result = "";
    	String password = deviceId + todayDate;
    	
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();
      
            StringBuffer MD5Hash = new StringBuffer();
            
            for (int i = 0; i < messageDigest.length; i++){
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                MD5Hash.append(h);
            }
                  
            result = MD5Hash.toString();
             
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } 
        
        return result;
    }
    
    public List<String> generateToken(){
    	String abc = "abcdefghijklmnopqrstuvwxyz";
    	String md5 = computeMD5Hash();
    	int l = 5;
    	List<String> lstToken = new ArrayList<String>();
    	
    	if(md5.length() % l != 0){
    		int sisaAwal = md5.length() % l;
    		int lAwal = md5.length() - sisaAwal;
    		int lAkhir = ((lAwal / l) + 1) * l;
    		int sisaAkhir = lAkhir - md5.length();
    		
    		for(int i = 0; i < sisaAkhir; i++){
    			md5 += abc.charAt(i);;
    		}
    	}
    	
    	int hasil = md5.length() / l;
    	
    	for(int i = 0; i < hasil; i++){
    		int start = i * l;
    		int end = (i+1) * l;
    		String token = md5.substring(start, end);
    		Log.d("tag", token);
    		lstToken.add(token);
    	}
    	
    	return lstToken;
    }
}
