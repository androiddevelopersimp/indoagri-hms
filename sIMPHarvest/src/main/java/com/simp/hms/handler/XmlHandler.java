package com.simp.hms.handler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import com.simp.hms.R;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.model.AbsentType;
import com.simp.hms.model.AncakPanenHeader;
import com.simp.hms.model.AncakPanenQuality;
import com.simp.hms.model.BLKRISET;
import com.simp.hms.model.BLKSUGC;
import com.simp.hms.model.ConfigApp;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.model.BJR;
import com.simp.hms.model.BKMHeader;
import com.simp.hms.model.BKMLine;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.BLKPLT;
import com.simp.hms.model.BLKSBC;
import com.simp.hms.model.BLKSBCDetail;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.DayOff;
import com.simp.hms.model.Employee;
import com.simp.hms.model.GroupHead;
import com.simp.hms.model.MessageStatus;
import com.simp.hms.model.Penalty;
import com.simp.hms.model.ResponseSendSPBS;
import com.simp.hms.model.SKB;
import com.simp.hms.model.SPBSDestination;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSRunningNumber;
import com.simp.hms.model.SPTA;
import com.simp.hms.model.SPTARunningNumber;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.TaksasiLine;
import com.simp.hms.model.UserApp;
import com.simp.hms.model.UserLogin;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.model.Vendor;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;

public class XmlHandler {
    private Context context;
    private DatabaseHandler database;

    public XmlHandler(Context context) {
        this.context = context;
        this.database = new DatabaseHandler(context);
    }

    public MessageStatus parsingXML(InputStream inputStream) {
        MessageStatus msgStatus = null;
        String tableName = "";
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;

        try {
            SKB skb = null;
            BlockHdrc blockHdrc = null;
            BLKSBC blksbc = null;
            BLKSBCDetail blksbcDetail = null;
            BJR bjr = null;
            BLKPLT blkplt = null;
            Employee employee = null;
            UserApp userApp = null;
            AbsentType absentType = null;
            DayOff dayOff = null;
            DivisionAssistant assDiv = null;
            RunningAccount runAccount = null;
            SPBSRunningNumber spbsRunNo = null;
            Penalty penalty = null;
            SPBSDestination spbsDestination = null;
            SPTARunningNumber sptaRunNo = null;
            Vendor vendor = null;
            BLKSUGC blksugc = null;
            GroupHead groupHead = null;
            BLKRISET blkriset = null;
            ConfigApp configApp = null;

            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(inputStream, null);

            int eventType = parser.getEventType();
            String textValue = null;

            database.openTransaction();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equalsIgnoreCase(BJR.XML_DOCUMENT)) {
                            tableName = BJR.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(BLKPLT.XML_DOCUMENT)) {
                            tableName = BLKPLT.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(BLKSBC.XML_DOCUMENT)) {
                            tableName = BLKSBC.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_DOCUMENT)) {
                            tableName = BLKSBCDetail.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_DOCUMENT)) {
                            tableName = BlockHdrc.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(Employee.XML_DOCUMENT)) {
                            tableName = Employee.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(SKB.XML_DOCUMENT)) {
                            tableName = SKB.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(UserApp.XML_DOCUMENT)) {
                            tableName = UserApp.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(AbsentType.XML_DOCUMENT)) {
                            tableName = AbsentType.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(DayOff.XML_DOCUMENT)) {
                            tableName = DayOff.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_DOCUMENT)) {
                            tableName = DivisionAssistant.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(RunningAccount.XML_DOCUMENT)) {
                            tableName = RunningAccount.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_DOCUMENT)) {
                            tableName = SPBSRunningNumber.TABLE_NAME;
//						database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(Penalty.XML_DOCUMENT)) {
                            tableName = Penalty.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(SPBSDestination.XML_DOCUMENT)) {
                            tableName = SPBSDestination.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_DOCUMENT)) {
                            tableName = SPTARunningNumber.TABLE_NAME;
                        } else if (tagName.equalsIgnoreCase(Vendor.XML_DOCUMENT)) {
                            tableName = Vendor.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_DOCUMENT)) {
                            tableName = BLKSUGC.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(GroupHead.XML_DOCUMENT)) {
                            tableName = GroupHead.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(BLKRISET.XML_DOCUMENT)) {
                            tableName = BLKRISET.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        } else if (tagName.equalsIgnoreCase(ConfigApp.XML_DOCUMENT)) {
                            tableName = ConfigApp.TABLE_NAME;
                            database.deleteData(tableName, null, null);
                        }

                        if (tableName.equalsIgnoreCase(SKB.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SKB.XML_ITEM)) {
                                skb = new SKB();
                            }
                        } else if (tableName.equalsIgnoreCase(BlockHdrc.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BlockHdrc.XML_ITEM)) {
                                blockHdrc = new BlockHdrc();
                            }
                        } else if (tableName.equalsIgnoreCase(BLKSBC.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKSBC.XML_ITEM)) {
                                blksbc = new BLKSBC();
                            }
                        } else if (tableName.equalsIgnoreCase(BLKSBCDetail.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_ITEM)) {
                                blksbcDetail = new BLKSBCDetail();
                            }
                        } else if (tableName.equalsIgnoreCase(BJR.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BJR.XML_ITEM)) {
                                bjr = new BJR();
                            }
                        } else if (tableName.equalsIgnoreCase(BLKPLT.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKPLT.XML_ITEM)) {
                                blkplt = new BLKPLT();
                            }
                        } else if (tableName.equalsIgnoreCase(Employee.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(Employee.XML_ITEM)) {
                                employee = new Employee();
                            }
                        } else if (tableName.equalsIgnoreCase(UserApp.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(UserApp.XML_ITEM)) {
                                userApp = new UserApp();
                            }
                        } else if (tableName.equalsIgnoreCase(AbsentType.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(AbsentType.XML_ITEM)) {
                                absentType = new AbsentType();
                            }
                        } else if (tableName.equalsIgnoreCase(DayOff.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(DayOff.XML_ITEM)) {
                                dayOff = new DayOff();
                            }
                        } else if (tableName.equalsIgnoreCase(DivisionAssistant.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(DivisionAssistant.XML_ITEM)) {
                                assDiv = new DivisionAssistant();
                            }
                        } else if (tableName.equalsIgnoreCase(RunningAccount.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(RunningAccount.XML_ITEM)) {
                                runAccount = new RunningAccount();
                            }
                        } else if (tableName.equalsIgnoreCase(SPBSRunningNumber.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ITEM)) {
                                spbsRunNo = new SPBSRunningNumber();
                            }
                        } else if (tableName.equalsIgnoreCase(Penalty.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(Penalty.XML_ITEM)) {
                                penalty = new Penalty();
                            }
                        } else if (tableName.equalsIgnoreCase(SPBSDestination.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPBSDestination.XML_ITEM)) {
                                spbsDestination = new SPBSDestination();
                            }
                        } else if (tableName.equalsIgnoreCase(SPTARunningNumber.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_ITEM)) {
                                sptaRunNo = new SPTARunningNumber();
                            }
                        } else if (tableName.equalsIgnoreCase(Vendor.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(Vendor.XML_ITEM)) {
                                vendor = new Vendor();
                            }
                        } else if (tableName.equalsIgnoreCase(BLKSUGC.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKSUGC.XML_ITEM)) {
                                blksugc = new BLKSUGC();
                            }
                        } else if (tableName.equalsIgnoreCase(GroupHead.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(GroupHead.XML_ITEM)) {
                                groupHead = new GroupHead();
                            }
                        } else if (tableName.equalsIgnoreCase(BLKRISET.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKRISET.XML_ITEM)) {
                                blkriset = new BLKRISET();
                            }
                        } else if (tableName.equalsIgnoreCase(ConfigApp.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(ConfigApp.XML_ITEM)) {
                                configApp = new ConfigApp();
                            }
                        }

                        break;
                    case XmlPullParser.TEXT:
                        textValue = parser.getText().trim();
                        break;
                    case XmlPullParser.END_TAG:
                        if (tableName.equalsIgnoreCase(SKB.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SKB.XML_ITEM)) {
                                database.setData(skb);
                            } else if (tagName.equalsIgnoreCase(SKB.XML_COMPANY_CODE)) {
                                skb.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(SKB.XML_ESTATE)) {
                                skb.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(SKB.XML_BLOCK)) {
                                skb.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(SKB.XML_BARIS_SKB)) {
                                skb.setBarisSkb(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(SKB.XML_VALID_FROM)) {
                                skb.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(SKB.XML_VALID_TO)) {
                                skb.setValidTo(textValue);
                            } else if (tagName.equalsIgnoreCase(SKB.XML_BARIS_BLOCK)) {
                                skb.setBarisBlok(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(SKB.XML_JUMLAH_POKOK)) {
                                skb.setJumlahPokok(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(SKB.XML_POKOK_MATI)) {
                                skb.setPokokMati(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(SKB.XML_TANGGAL_TANAM)) {
                                skb.setTanggalTanam(textValue);
                            } else if (tagName.equalsIgnoreCase(SKB.XML_LINE_SKB)) {
                                skb.setLineSkb(new Converter(textValue).StrToInt());
                            }
                        } else if (tableName.equalsIgnoreCase(BlockHdrc.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BlockHdrc.XML_ITEM)) {
                                database.setData(blockHdrc);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_COMPANY_CODE)) {
                                blockHdrc.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_ESTATE)) {
                                blockHdrc.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_BLOCK)) {
                                blockHdrc.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_VALID_FROM)) {
                                blockHdrc.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_VALID_TO)) {
                                blockHdrc.setValidTo(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_DIVISION)) {
                                blockHdrc.setDivisi(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_TYPE)) {
                                blockHdrc.setType(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_STATUS)) {
                                blockHdrc.setStatus(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_PROJECT_DEFINITION)) {
                                blockHdrc.setProjectDefinition(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_OWNER)) {
                                blockHdrc.setOwner(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_TGL_TANAM)) {
                                blockHdrc.setTglTanam(textValue);
                            } else if (tagName.equalsIgnoreCase(BlockHdrc.XML_MANDT)) {
                                blockHdrc.setMandt(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(BLKSBC.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKSBC.XML_ITEM)) {
                                database.setData(blksbc);
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_COMPANY_CODE)) {
                                blksbc.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_ESTATE)) {
                                blksbc.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_BLOCK)) {
                                blksbc.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_VALID_FROM)) {
                                blksbc.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_BASIS_NORMAL)) {
                                blksbc.setBasisNormal(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_BASIS_FRIDAY)) {
                                blksbc.setBasisFriday(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_BASIS_HOLIDAY)) {
                                blksbc.setBasisHoliday(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_PREMI_NORMAL)) {
                                blksbc.setPremiNormal(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_PREMI_FRIDAY)) {
                                blksbc.setPremiFriday(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_PREMI_HOLIDAY)) {
                                blksbc.setPremiHoliday(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBC.XML_PRIORITY)) {
                                blksbc.setPriority(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(BLKSBCDetail.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_ITEM)) {
                                database.setData(blksbcDetail);
                            } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_COMPANY_CODE)) {
                                blksbcDetail.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_ESTATE)) {
                                blksbcDetail.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_BLOCK)) {
                                blksbcDetail.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_VALID_FROM)) {
                                blksbcDetail.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_MIN_VAL)) {
                                blksbcDetail.setMinimumValue(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_MAX_VAL)) {
                                blksbcDetail.setMaximumValue(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSBCDetail.XML_OVER_BASIC_RATE)) {
                                blksbcDetail.setOverBasicRate(new Converter(textValue).StrToDouble());
                            }
                        } else if (tableName.equalsIgnoreCase(BJR.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BJR.XML_ITEM)) {
                                database.setData(bjr);
                            } else if (tagName.equalsIgnoreCase(BJR.XML_COMPANY_CODE)) {
                                bjr.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BJR.XML_ESTATE)) {
                                bjr.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BJR.XML_BLOCK)) {
                                bjr.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BJR.XMl_EFF_DATE)) {
                                bjr.setEffDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BJR.XML_BJR)) {
                                bjr.setBjr(new Converter(textValue).StrToDouble());
                            }
                        } else if (tableName.equalsIgnoreCase(BLKPLT.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKPLT.XML_ITEM)) {
                                database.setData(blkplt);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_COMPANY_CODE)) {
                                blkplt.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_ESTATE)) {
                                blkplt.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_BLOCK)) {
                                blkplt.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_VALID_FROM)) {
                                blkplt.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_VALID_TO)) {
                                blkplt.setValidTo(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_CROP_TYPE)) {
                                blkplt.setCropType(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_PREVIOUS_CROP)) {
                                blkplt.setPreviousCrop(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_FINISH_DATE)) {
                                blkplt.setFinishDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_REFERENCE)) {
                                blkplt.setReference(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_JARAK_TANAM)) {
                                blkplt.setJarakTanam(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_HARVESTING_DATE)) {
                                blkplt.setHarvestingDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_HARVESTED)) {
                                blkplt.setHarvested(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_PLAN_DATE)) {
                                blkplt.setPlanDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_TOPOGRAPHY)) {
                                blkplt.setTopography(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_SOIL_TYPE)) {
                                blkplt.setSoilType(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_SOIL_CATEGORY)) {
                                blkplt.setSoilCategory(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKPLT.XML_PROD_TREES)) {
                                blkplt.setProdTrees(new Converter(textValue).StrToDouble());
                            }
                        } else if (tableName.equalsIgnoreCase(Employee.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(Employee.XML_ITEM)) {
                                database.setData(employee);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_COMPANY_CODE)) {
                                employee.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_ESTATE)) {
                                employee.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_FISCAL_YEAR)) {
                                employee.setFiscalYear(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(Employee.XML_FISCAL_PERIOD)) {
                                employee.setFiscalPeriod(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(Employee.XML_NIK)) {
                                employee.setNik(textValue);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_NAME)) {
                                employee.setName(textValue.toUpperCase());
                            } else if (tagName.equalsIgnoreCase(Employee.XML_TERM_DATE)) {
                                employee.setTermDate(textValue);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_DIVISION)) {
                                employee.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_ROLE_ID)) {
                                employee.setRoleId(textValue.toUpperCase());
                            } else if (tagName.equalsIgnoreCase(Employee.XML_JOB_POS)) {
                                employee.setJobPos(textValue.toUpperCase());
                            } else if (tagName.equalsIgnoreCase(Employee.XML_EMP_TYPE)) {
                                employee.setEmpType(textValue.toUpperCase());
                            } else if (tagName.equalsIgnoreCase(Employee.XML_GANG)) {
                                employee.setGang(textValue.toUpperCase());
                            } else if (tagName.equalsIgnoreCase(Employee.XML_COST_CENTER)) {
                                employee.setCostCenter(textValue);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_VALID_FROM)) {
                                employee.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(Employee.XML_HARVESTER_CODE)) {
                                employee.setHarvesterCode(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(UserApp.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(UserApp.XML_ITEM)) {
                                database.setData(userApp);
                            } else if (tagName.equalsIgnoreCase(UserApp.XML_NIK)) {
                                userApp.setNik(textValue);
                            } else if (tagName.equalsIgnoreCase(UserApp.XML_USERNAME)) {
                                userApp.setUsername(textValue);
                            } else if (tagName.equalsIgnoreCase(UserApp.XML_PASSWORD)) {
                                String enc = textValue;
                                String dec = "";

                                try {
                                    dec = new CryptoHandler().decrypt(enc, CryptoHandler.KEY);
                                } catch (KeyException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (InvalidAlgorithmParameterException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IllegalBlockSizeException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (BadPaddingException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (GeneralSecurityException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

//							if(TextUtils.isEmpty(dec)){
//								dec = "Indoagri2015";
//							}

                                userApp.setPassword(dec);
                            } else if (tagName.equalsIgnoreCase(UserApp.XML_VALID_TO)) {
                                String validTo = textValue + "235959";
                                userApp.setValidTo(new DateLocal(validTo, DateLocal.FORMAT_ID).getDate().getTime());
                            } else if (tagName.equalsIgnoreCase(UserApp.XML_CREATED_DATE)) {
                                String createdDate = textValue + "000000";
                                userApp.setCreatedDate(new DateLocal(createdDate, DateLocal.FORMAT_ID).getDate().getTime());
                            } else if (tagName.equalsIgnoreCase(UserApp.XML_CREATED_BY)) {
                                userApp.setCreatedBy(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(AbsentType.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(AbsentType.XML_ITEM)) {
                                database.setData(absentType);
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_COMPANY_CODE)) {
                                absentType.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_ABSENT_TYPE)) {
                                absentType.setAbsentType(textValue);
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_DESCRIPTION)) {
                                absentType.setDescription(textValue);
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_HKRLLO)) {
                                absentType.setHkrllo(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_HKRLHI)) {
                                absentType.setHkrlhi(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_HKPYLO)) {
                                absentType.setHkpylo(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_HKPYHI)) {
                                absentType.setHkpyhi(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_HKVLLO)) {
                                absentType.setHkvllo(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_HKVLHI)) {
                                absentType.setHkvlhi(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_HKMEIN)) {
                                absentType.setHkmein(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(AbsentType.XML_AGROUP)) {
                                absentType.setAgroup(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(DayOff.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(DayOff.XML_ITEM)) {
                                database.setData(dayOff);
                            } else if (tagName.equalsIgnoreCase(DayOff.XML_ESTATE)) {
                                dayOff.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(DayOff.XML_DATE)) {
                                dayOff.setDate(textValue);
                            } else if (tagName.equalsIgnoreCase(DayOff.XML_DAY_OFF_TYPE)) {
                                dayOff.setDayOffType(textValue);
                            } else if (tagName.endsWith(DayOff.XML_DESCRIPTION)) {
                                dayOff.setDayOffType(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(DivisionAssistant.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(DivisionAssistant.XML_ITEM)) {
                                database.setData(assDiv);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_ESTATE)) {
                                assDiv.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_DIVISION)) {
                                assDiv.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_SPRAS)) {
                                assDiv.setSpras(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_DESCRIPTION)) {
                                assDiv.setDescription(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT)) {
                                assDiv.setAssistant(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_DISTANCE_TO_MILL)) {
                                assDiv.setDistanceToMill(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_UOM)) {
                                assDiv.setUom(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_ASSISTANT_NAME)) {
                                assDiv.setAssistantName(textValue);
                            } else if (tagName.equalsIgnoreCase(DivisionAssistant.XML_LIFNR)) {
                                assDiv.setLifnr(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(RunningAccount.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(RunningAccount.XML_ITEM)) {
                                database.setData(runAccount);
                            } else if (tagName.equalsIgnoreCase(RunningAccount.XML_COMPANY_CODE)) {
                                runAccount.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(RunningAccount.XML_ESTATE)) {
                                runAccount.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(RunningAccount.XML_RUNNING_ACCOUNT)) {
                                runAccount.setRunningAccount(textValue);
                            } else if (tagName.equalsIgnoreCase(RunningAccount.XML_LICENSE_PLATE)) {
                                runAccount.setLicensePlate(textValue);
                            } else if (tagName.equalsIgnoreCase(RunningAccount.XML_LIFNR)) {
                                runAccount.setLifnr(textValue);
                            } else if (tagName.equalsIgnoreCase(RunningAccount.XML_OWNERSHIPFLAG)) {
                                runAccount.setOwnerShipFlag(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(SPBSRunningNumber.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ITEM)) {
                                SPBSRunningNumber temp = (SPBSRunningNumber) database.getDataFirst(false, SPBSRunningNumber.TABLE_NAME, null,
                                        SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                                                SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                                                SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                                                SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                                SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                                        new String[]{spbsRunNo.getEstate(), spbsRunNo.getYear(),
                                                spbsRunNo.getMonth(), spbsRunNo.getImei(), String.valueOf(spbsRunNo.getDeviceAlias())},
                                        null, null, null, null);

                                if (temp != null) {
                                    int lastId = temp.getId();
                                    int newId = new Converter(spbsRunNo.getRunningNumber()).StrToInt();

                                    if (lastId < newId) {
                                        database.updateData(spbsRunNo,
                                                SPBSRunningNumber.XML_ESTATE + "=?" + " and " +
                                                        SPBSRunningNumber.XML_YEAR + "=?" + " and " +
                                                        SPBSRunningNumber.XML_MONTH + "=?" + " and " +
                                                        SPBSRunningNumber.XML_IMEI + "=?" + " and " +
                                                        SPBSRunningNumber.XML_DEVICE_ALIAS + "=?",
                                                new String[]{spbsRunNo.getEstate(), spbsRunNo.getYear(),
                                                        spbsRunNo.getMonth(), spbsRunNo.getImei(), String.valueOf(spbsRunNo.getDeviceAlias())});
                                    }
                                } else {
                                    database.setData(spbsRunNo);
                                }
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ID)) {
                                spbsRunNo.setId(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_ESTATE)) {
                                spbsRunNo.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_DIVISION)) {
                                spbsRunNo.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_YEAR)) {
                                spbsRunNo.setYear(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_MONTH)) {
                                spbsRunNo.setMonth(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_IMEI)) {
                                spbsRunNo.setImei(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_RUNNING_NUMBER)) {
                                spbsRunNo.setRunningNumber(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSRunningNumber.XML_DEVICE_ALIAS)) {
                                spbsRunNo.setDeviceAlias(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(Penalty.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(Penalty.XML_ITEM)) {
                                database.setData(penalty);
                            } else if (tagName.equalsIgnoreCase(Penalty.XML_PENALTY_CODE)) {
                                penalty.setPenaltyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(Penalty.XML_PENALTY_DESC)) {
                                penalty.setPenaltyDesc(textValue);
                            } else if (tagName.equalsIgnoreCase(Penalty.XML_UOM)) {
                                penalty.setUom(textValue);
                            } else if (tagName.equalsIgnoreCase(Penalty.XMl_CROP_TYPE)) {
                                penalty.setCropType(textValue);
                            } else if (tagName.equalsIgnoreCase(Penalty.XML_IS_LOADING)) {
                                penalty.setIsLoading(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(Penalty.XML_IS_ANCAK)) {
                                penalty.setIsAncak(new Converter(textValue).StrToInt());
                            }
                        } else if (tableName.equalsIgnoreCase(SPBSDestination.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPBSDestination.XML_ITEM)) {
                                database.setData(spbsDestination);
                            } else if (tagName.equalsIgnoreCase(SPBSDestination.XML_MANDT)) {
                                spbsDestination.setMandt(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSDestination.XML_ESTATE)) {
                                spbsDestination.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSDestination.XML_DEST_TYPE)) {
                                spbsDestination.setDestType(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSDestination.XML_DEST_ID)) {
                                spbsDestination.setDestId(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSDestination.XML_DEST_DESC)) {
                                spbsDestination.setDestDesc(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSDestination.XML_ACTIVE)) {
                                spbsDestination.setActive(new Converter(textValue).StrToInt());
                            }
                        } else if (tableName.equalsIgnoreCase(SPTARunningNumber.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_ITEM)) {
                                SPTARunningNumber temp = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
                                        SPTARunningNumber.XML_ESTATE + "=?" + " and " +
                                                SPTARunningNumber.XML_YEAR + "=?" + " and " +
                                                SPTARunningNumber.XML_MONTH + "=?" + " and " +
                                                SPTARunningNumber.XML_IMEI + "=?" + " and " +
                                                SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
                                        new String[]{sptaRunNo.getEstate(), sptaRunNo.getYear(),
                                                sptaRunNo.getMonth(), sptaRunNo.getImei(), String.valueOf(sptaRunNo.getDeviceAlias())},
                                        null, null, null, null);

                                if (temp != null) {
                                    int lastId = temp.getId();
                                    int newId = new Converter(sptaRunNo.getRunningNumber()).StrToInt();

                                    if (lastId < newId) {
                                        database.updateData(sptaRunNo,
                                                SPTARunningNumber.XML_ESTATE + "=?" + " and " +
                                                        SPTARunningNumber.XML_YEAR + "=?" + " and " +
                                                        SPTARunningNumber.XML_MONTH + "=?" + " and " +
                                                        SPTARunningNumber.XML_IMEI + "=?" + " and " +
                                                        SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
                                                new String[]{sptaRunNo.getEstate(), sptaRunNo.getYear(),
                                                        sptaRunNo.getMonth(), sptaRunNo.getImei(), String.valueOf(sptaRunNo.getDeviceAlias())});
                                    }
                                } else {
                                    database.setData(sptaRunNo);
                                }
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_ID)) {
                                sptaRunNo.setId(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_ESTATE)) {
                                sptaRunNo.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_DIVISION)) {
                                sptaRunNo.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_YEAR)) {
                                sptaRunNo.setYear(textValue);
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_MONTH)) {
                                sptaRunNo.setMonth(textValue);
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_IMEI)) {
                                sptaRunNo.setImei(textValue);
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_RUNNING_NUMBER)) {
                                sptaRunNo.setRunningNumber(textValue);
                            } else if (tagName.equalsIgnoreCase(SPTARunningNumber.XML_DEVICE_ALIAS)) {
                                sptaRunNo.setDeviceAlias(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(Vendor.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(Vendor.XML_ITEM)) {
                                database.setData(vendor);
                            } else if (tagName.equalsIgnoreCase(Vendor.XML_ESTATE)) {
                                vendor.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(Vendor.XML_LIFNR)) {
                                vendor.setLifnr(textValue);
                            } else if (tagName.equalsIgnoreCase(Vendor.XML_NAME)) {
                                vendor.setName(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(BLKSUGC.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKSUGC.XML_ITEM)) {
                                database.setData(blksugc);
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_COMPANY_CODE)) {
                                blksugc.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_ESTATE)) {
                                blksugc.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_BLOCK)) {
                                blksugc.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_VALID_FROM)) {
                                blksugc.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_VALID_TO)) {
                                blksugc.setValidTo(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_PHASE)) {
                                blksugc.setPhase(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_DISTANCE)) {
                                blksugc.setDistance(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BLKSUGC.XML_SUB_DIVISION)) {
                                blksugc.setSubDivision(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(GroupHead.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(GroupHead.XML_ITEM)) {
                                database.setData(groupHead);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_COMPANY_CODE)) {
                                groupHead.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_ESTATE)) {
                                groupHead.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_LIFNR)) {
                                groupHead.setLifnr(textValue);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_INITIAL)) {
                                groupHead.setInitial(textValue);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_VALID_FROM)) {
                                groupHead.setValidFrom(textValue);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_VALID_TO)) {
                                groupHead.setValidTo(textValue);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_NAME)) {
                                groupHead.setName(textValue);
                            } else if (tagName.equalsIgnoreCase(GroupHead.XML_STATUS)) {
                                groupHead.setStatus(new Converter(textValue).StrToInt());
                            }
                        } else if (tableName.equalsIgnoreCase(BLKRISET.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BLKRISET.XML_ITEM)) {
                                database.setData(blkriset);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_COMPANY_CODE)) {
                                blkriset.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_ESTATE)) {
                                blkriset.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_BLOCK)) {
                                blkriset.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_STATUS)) {
                                blkriset.setStatus(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_START_DATE)) {
                                blkriset.setStartDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_END_DATE)) {
                                blkriset.setEndDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_DESCRIPTION)) {
                                blkriset.setDescription(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_REF_DOC)) {
                                blkriset.setRefDoc(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_REF_DOC_DATE)) {
                                blkriset.setRefDocDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_MANDT)) {
                                blkriset.setMandt(textValue);
                            }
						/*else if(tagName.equalsIgnoreCase(BLKRISET.XML_CREATED_BY)){
							blkriset.setCreatedBy(textValue);
						}*/
                            else if (tagName.equalsIgnoreCase(BLKRISET.XML_CREATED_DATE)) {
                                blkriset.setCreatedDate(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_MODIFIED_BY)) {
                                blkriset.setModifiedBy(textValue);
                            } else if (tagName.equalsIgnoreCase(BLKRISET.XML_MODIFIED_DATE)) {
                                blkriset.setModifiedDate(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(ConfigApp.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(ConfigApp.XML_ITEM)) {
                                database.setData(configApp);
                            } else if (tagName.equalsIgnoreCase(ConfigApp.XML_COMPANY_CODE)) {
                                configApp.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(ConfigApp.XML_COMPANY_NAME)) {
                                configApp.setCompanyName(textValue);
                            } else if (tagName.equalsIgnoreCase(ConfigApp.XML_ESTATE)) {
                                configApp.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(ConfigApp.XML_ESTATE_NAME)) {
                                configApp.setEstateName(textValue);
                            }
                        }
                    default:
                        break;
                }
                eventType = parser.next();
            }

            if (tableName.equals(BJR.TABLE_NAME)) {
                msgStatus = new MessageStatus(BJR.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(BLKPLT.TABLE_NAME)) {
                msgStatus = new MessageStatus(BLKPLT.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(BLKSBC.TABLE_NAME)) {
                msgStatus = new MessageStatus(BLKSBC.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(BLKSBCDetail.TABLE_NAME)) {
                msgStatus = new MessageStatus(BLKSBCDetail.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(BlockHdrc.TABLE_NAME)) {
                msgStatus = new MessageStatus(BlockHdrc.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(Employee.TABLE_NAME)) {
                msgStatus = new MessageStatus(Employee.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(SKB.TABLE_NAME)) {
                msgStatus = new MessageStatus(SKB.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(UserApp.TABLE_NAME)) {
                msgStatus = new MessageStatus(UserApp.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(AbsentType.TABLE_NAME)) {
                msgStatus = new MessageStatus(AbsentType.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(DayOff.TABLE_NAME)) {
                msgStatus = new MessageStatus(DayOff.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(DivisionAssistant.TABLE_NAME)) {
                msgStatus = new MessageStatus(DivisionAssistant.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(RunningAccount.TABLE_NAME)) {
                msgStatus = new MessageStatus(RunningAccount.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(SPBSRunningNumber.TABLE_NAME)) {
                msgStatus = new MessageStatus(SPBSRunningNumber.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(Penalty.TABLE_NAME)) {
                msgStatus = new MessageStatus(Penalty.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(SPBSDestination.TABLE_NAME)) {
                msgStatus = new MessageStatus(SPBSDestination.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(SPTARunningNumber.TABLE_NAME)) {
                msgStatus = new MessageStatus(SPTARunningNumber.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(Vendor.TABLE_NAME)) {
                msgStatus = new MessageStatus(Vendor.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(BLKSUGC.TABLE_NAME)) {
                msgStatus = new MessageStatus(BLKSUGC.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(GroupHead.TABLE_NAME)) {
                msgStatus = new MessageStatus(GroupHead.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(BLKRISET.TABLE_NAME)) {
                msgStatus = new MessageStatus(BLKRISET.ALIAS, true, context.getResources().getString(R.string.import_successed));
            } else if (tableName.equals(ConfigApp.TABLE_NAME)) {
                msgStatus = new MessageStatus(ConfigApp.ALIAS, true, context.getResources().getString(R.string.import_successed));
            }

            database.commitTransaction();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus = new MessageStatus(tableName, false, e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus = new MessageStatus(tableName, false, e.getMessage());
        } catch (SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus = new MessageStatus(tableName, false, e.getMessage());
        } finally {
            database.closeTransaction();
        }

        return msgStatus;
    }

    public MessageStatus createXML(int btnId, String status, boolean isUsbOtgSave, String date) {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        String companyCode = "";
        String estate = "";
        String division = "";
        String nik = "";
        MessageStatus msgStatus = null;

        int rowCount = 0;

        database.openTransaction();
        UserLogin userLogin = (UserLogin) database.getDataFirst(false, UserLogin.TABLE_NAME, null, null, null, null, null, null, null);
        database.closeTransaction();

        if (userLogin != null) {
            companyCode = userLogin.getCompanyCode();
            estate = userLogin.getEstate();
            division = userLogin.getDivision();
            nik = userLogin.getNik();
        }


        switch (btnId) {
            case R.id.cbxExportBPN:
                try {
                    List<Object> listObjectBPNHeader;

                    database.openTransaction();
                    if (status != null) {
                        listObjectBPNHeader = database.getListData(false, BPNHeader.TABLE_NAME, null,
                                BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        BPNHeader.XML_ESTATE + "=?" + " and " +
                                        BPNHeader.XML_DIVISION + "=?" + " and " +
                                        BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                        BPNHeader.XML_STATUS + "=?" + " and " +
                                        BPNHeader.XML_BPN_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, status, date},
                                null, null, BPNHeader.XML_BPN_DATE, null);
                    } else {
                        listObjectBPNHeader = database.getListData(false, BPNHeader.TABLE_NAME, null,
                                BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        BPNHeader.XML_ESTATE + "=?" + " and " +
                                        BPNHeader.XML_DIVISION + "=?" + " and " +
                                        BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                        BPNHeader.XML_BPN_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, date},
                                null, null, BPNHeader.XML_BPN_DATE, null);
                    }

                    List<String> listObjectBPNHeaderStr = new ArrayList<String>();
                    for (int aa = 0; aa < listObjectBPNHeader.size(); aa++) {
                        BPNHeader bpnHeaderStr = (BPNHeader) listObjectBPNHeader.get(aa);
                        listObjectBPNHeaderStr.add(bpnHeaderStr.getBpnId());
                    }

                    List<Object> listObjectBPNQuantityBackDate = database.getListData(true, BPNQuantity.TABLE_NAME, new String[]{BPNQuantity.XML_BPN_ID, BPNQuantity.XML_BPN_DATE},
                            BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                    BPNQuantity.XML_DIVISION + "=?",
                            new String[]{companyCode, estate, date, division},
                            null, null, null, null);
                    //20200908 - kamal : request dari tim fo untuk ganti date XML_MODIFIED_DATE_STR menjadi XML_BPN_DATE


                    //List<String> listObjectBPNQuantityBackDateFilter = new ArrayList<String>();
                    ArrayList<BPNQuantityBackDateObj> listObjectBPNQuantityBackDateFilter = new ArrayList<>();
                    if (listObjectBPNHeaderStr.size() > 0 && listObjectBPNQuantityBackDate.size() > 0) {
                        for (int xx = 0; xx < listObjectBPNQuantityBackDate.size(); xx++) {
                            BPNQuantity bpnQuantityBackDate = (BPNQuantity) listObjectBPNQuantityBackDate.get(xx);
                            if (!listObjectBPNHeaderStr.contains(bpnQuantityBackDate.getBpnId())) {
                                BPNQuantityBackDateObj bpnQuantityBackDateArray = new BPNQuantityBackDateObj(bpnQuantityBackDate.getBpnId(), bpnQuantityBackDate.getBpnDate());
                                listObjectBPNQuantityBackDateFilter.add(bpnQuantityBackDateArray);
                            }
                        }
                    } else if (listObjectBPNHeaderStr.size() == 0 && listObjectBPNQuantityBackDate.size() > 0) {
                        for (int xx = 0; xx < listObjectBPNQuantityBackDate.size(); xx++) {
                            BPNQuantity bpnQuantityBackDate = (BPNQuantity) listObjectBPNQuantityBackDate.get(xx);

                            BPNQuantityBackDateObj bpnQuantityBackDateArray = new BPNQuantityBackDateObj(bpnQuantityBackDate.getBpnId(), bpnQuantityBackDate.getBpnDate());
                            listObjectBPNQuantityBackDateFilter.add(bpnQuantityBackDateArray);
                        }
                    }

                    if (listObjectBPNHeader.size() > 0 || listObjectBPNQuantityBackDateFilter.size() > 0) {
                        xmlSerializer.setOutput(writer);
                        xmlSerializer.startDocument("UTF-8", true);
                        xmlSerializer.startTag("", BPNHeader.XML_FILE);

                        if (listObjectBPNHeader.size() > 0) {
                            //rowCount = rowCount + listObjectBPNHeader.size();

                            for (int i = 0; i < listObjectBPNHeader.size(); i++) {
                                rowCount = rowCount + 1;
                                BPNHeader bpnHeader = (BPNHeader) listObjectBPNHeader.get(i);
                                bpnHeader.setStatus(1);

                                String id = bpnHeader.getBpnId();
                                String imei = bpnHeader.getImei();
                                String bpnDate = bpnHeader.getBpnDate();
                                String gang = bpnHeader.getGang();
                                String location = bpnHeader.getLocation();
                                String tph = bpnHeader.getTph();
                                String nikHarvester = bpnHeader.getNikHarvester();

//							database.updateData(bpnHeader,
//								BPNHeader.XML_BPN_ID + "=?" + " and " +
//								BPNHeader.XML_IMEI + "=?" + " and " +
//								BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//								BPNHeader.XML_ESTATE + "=?" + " and " +
//								BPNHeader.XML_BPN_DATE + "=?" + " and " +
//								BPNHeader.XML_DIVISION + "=?" + " and " +
//								BPNHeader.XML_GANG + "=?" + " and " +
//								BPNHeader.XML_LOCATION + "=?" + " and " +
//								BPNHeader.XML_TPH + "=?" + " and " +
//								BPNHeader.XML_NIK_HARVESTER + "=?",
//								new String [] {id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});

                                xmlSerializer.startTag("", BPNHeader.XML_DOCUMENT);
                                xmlSerializer.startTag("", BPNHeader.XML_BPN_ID);
                                xmlSerializer.text(bpnHeader.getBpnId());
                                xmlSerializer.endTag("", BPNHeader.XML_BPN_ID);

                                xmlSerializer.startTag("", BPNHeader.XML_IMEI);
                                xmlSerializer.text(bpnHeader.getImei());
                                xmlSerializer.endTag("", BPNHeader.XML_IMEI);

                                xmlSerializer.startTag("", BPNHeader.XML_COMPANY_CODE);
                                xmlSerializer.text(bpnHeader.getCompanyCode());
                                xmlSerializer.endTag("", BPNHeader.XML_COMPANY_CODE);

                                xmlSerializer.startTag("", BPNHeader.XML_ESTATE);
                                xmlSerializer.text(bpnHeader.getEstate());
                                xmlSerializer.endTag("", BPNHeader.XML_ESTATE);

                                xmlSerializer.startTag("", BPNHeader.XML_HARVEST_DATE);
                                xmlSerializer.text(bpnHeader.getBpnDate());
                                xmlSerializer.endTag("", BPNHeader.XML_HARVEST_DATE);

                                xmlSerializer.startTag("", BPNHeader.XML_DIVISION);
                                xmlSerializer.text(bpnHeader.getDivision());
                                xmlSerializer.endTag("", BPNHeader.XML_DIVISION);

                                xmlSerializer.startTag("", BPNHeader.XML_GANG);
                                xmlSerializer.text(bpnHeader.getGang());
                                xmlSerializer.endTag("", BPNHeader.XML_GANG);

                                xmlSerializer.startTag("", BPNHeader.XML_LOCATION);
                                xmlSerializer.text(bpnHeader.getLocation());
                                xmlSerializer.endTag("", BPNHeader.XML_LOCATION);

                                xmlSerializer.startTag("", BPNHeader.XML_TPH);
                                xmlSerializer.text(bpnHeader.getTph());
                                xmlSerializer.endTag("", BPNHeader.XML_TPH);

                                xmlSerializer.startTag("", BPNHeader.XML_NIK_HARVESTER);
                                xmlSerializer.text(bpnHeader.getNikHarvester());
                                xmlSerializer.endTag("", BPNHeader.XML_NIK_HARVESTER);

                                xmlSerializer.startTag("", BPNHeader.XML_HARVESTER);
                                xmlSerializer.text(bpnHeader.getHarvester());
                                xmlSerializer.endTag("", BPNHeader.XML_HARVESTER);

                                xmlSerializer.startTag("", BPNHeader.XML_NIK_FOREMAN);
                                xmlSerializer.text(bpnHeader.getNikForeman());
                                xmlSerializer.endTag("", BPNHeader.XML_NIK_FOREMAN);

                                xmlSerializer.startTag("", BPNHeader.XML_FOREMAN);
                                xmlSerializer.text(bpnHeader.getForeman());
                                xmlSerializer.endTag("", BPNHeader.XML_FOREMAN);

                                xmlSerializer.startTag("", BPNHeader.XML_NIK_CLERK);
                                xmlSerializer.text(bpnHeader.getNikClerk());
                                xmlSerializer.endTag("", BPNHeader.XML_NIK_CLERK);

                                xmlSerializer.startTag("", BPNHeader.XML_CLERK);
                                xmlSerializer.text(bpnHeader.getClerk());
                                xmlSerializer.endTag("", BPNHeader.XML_CLERK);

//								xmlSerializer.startTag("", BPNHeader.XML_USE_GERDANG);
//								xmlSerializer.text(String.valueOf(bpnHeader.isUseGerdang()));
//								xmlSerializer.endTag("", BPNHeader.XML_USE_GERDANG);

                                xmlSerializer.startTag("", BPNHeader.XML_CROP);
                                xmlSerializer.text(bpnHeader.getCrop());
                                xmlSerializer.endTag("", BPNHeader.XML_CROP);

                                xmlSerializer.startTag("", BPNHeader.XML_GPS_KOORDINAT);
                                xmlSerializer.text(bpnHeader.getGpsKoordinat());
                                xmlSerializer.endTag("", BPNHeader.XML_GPS_KOORDINAT);

                                xmlSerializer.startTag("", BPNHeader.XML_PHOTO);
                                xmlSerializer.text(bpnHeader.getPhoto());
                                xmlSerializer.endTag("", BPNHeader.XML_PHOTO);

                                xmlSerializer.startTag("", BPNHeader.XML_SPBS_NUMBER);
                                xmlSerializer.text(bpnHeader.getSpbsNumber());
                                xmlSerializer.endTag("", BPNHeader.XML_SPBS_NUMBER);

                                xmlSerializer.startTag("", BPNHeader.XML_CREATED_DATE);
                                if (bpnHeader.getCreatedDate() > 0) {
                                    xmlSerializer.text(new DateLocal(new Date(bpnHeader.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                } else {
                                    xmlSerializer.text("");
                                }
                                xmlSerializer.endTag("", BPNHeader.XML_CREATED_DATE);

                                xmlSerializer.startTag("", BPNHeader.XML_CREATED_BY);
                                xmlSerializer.text(bpnHeader.getCreatedBy());
                                xmlSerializer.endTag("", BPNHeader.XML_CREATED_BY);

                                xmlSerializer.startTag("", BPNHeader.XML_MODIFIED_DATE);
                                if (bpnHeader.getModifiedDate() > 0) {
                                    xmlSerializer.text(new DateLocal(new Date(bpnHeader.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                } else {
                                    xmlSerializer.text("");
                                }
                                xmlSerializer.endTag("", BPNHeader.XML_MODIFIED_DATE);

                                xmlSerializer.startTag("", BPNHeader.XML_MODIFIED_BY);
                                xmlSerializer.text(bpnHeader.getModifiedBy());
                                xmlSerializer.endTag("", BPNHeader.XML_MODIFIED_BY);

                                List<Object> listObjectQuantity;

                                if (status != null) {
                                    listObjectQuantity = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                                            BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                    BPNQuantity.XML_IMEI + "=?" + " and " +
                                                    BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                    BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                    BPNQuantity.XML_GANG + "=?" + " and " +
                                                    BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                    BPNQuantity.XML_TPH + "=?" + " and " +
                                                    BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                                                    BPNQuantity.XML_STATUS + "=?",
                                            new String[]{id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester, status},
                                            null, null, BPNQuantity.XML_ACHIEVEMENT_CODE, null);
                                } else {
                                    listObjectQuantity = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                                            BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                    BPNQuantity.XML_IMEI + "=?" + " and " +
                                                    BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                    BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                    BPNQuantity.XML_GANG + "=?" + " and " +
                                                    BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                    BPNQuantity.XML_TPH + "=?" + " and " +
                                                    BPNQuantity.XML_NIK_HARVESTER + "=?",
                                            new String[]{id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester},
                                            null, null, BPNQuantity.XML_ACHIEVEMENT_CODE, null);
                                }

                                if (listObjectQuantity.size() > 0) {

                                    for (int j = 0; j < listObjectQuantity.size(); j++) {

                                        BPNQuantity bpnQuantity = (BPNQuantity) listObjectQuantity.get(j);
                                        bpnQuantity.setStatus(1);

                                        String crop = bpnQuantity.getCrop();
                                        String achievementCode = bpnQuantity.getAchievementCode();

//										database.updateData(bpnQuantity,
//												BPNQuantity.XML_BPN_ID + "=?" + " and " +
//												BPNQuantity.XML_IMEI + "=?" + " and " +
//												BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
//												BPNQuantity.XML_ESTATE + "=?" + " and " +
//												BPNQuantity.XML_BPN_DATE + "=?" + " and " +
//												BPNQuantity.XML_DIVISION + "=?" + " and " +
//												BPNQuantity.XML_GANG + "=?" + " and " +
//												BPNQuantity.XML_LOCATION + "=?" + " and " +
//												BPNQuantity.XML_TPH + "=?" + " and " +
//												BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
//												BPNQuantity.XML_CROP + "=?" + " and " +
//												BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
//												new String [] {id, imei, companyCode, estate, bpnDate, division, gang, location,
//												tph, nikHarvester, crop, achievementCode});

                                        if ((achievementCode.equalsIgnoreCase(BPNQuantity.LOOSE_FRUIT_CODE) && bpnQuantity.getQuantity() > 0) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.JANJANG_CODE)) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.LATEX_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.LATEX_DRC_CODE)) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.LUMP_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.LUMP_DRC_CODE)) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.SLAB_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.SLAB_DRC_CODE)) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.TREELACE_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.TREELACE_DRC_CODE)) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.GOOD_WEIGHT_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.GOOD_QTY_CODE)) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.BAD_WEIGHT_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.BAD_QTY_CODE)) ||
                                                (achievementCode.equalsIgnoreCase(BPNQuantity.POOR_WEIGHT_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.POOR_QTY_CODE) || (achievementCode.equalsIgnoreCase(BPNQuantity.POD_QTY_CODE)))) {

                                            rowCount = rowCount + 1;

                                            xmlSerializer.startTag("", BPNQuantity.XML_DOCUMENT);

                                            xmlSerializer.startTag("", BPNQuantity.XML_BPN_ID);
                                            xmlSerializer.text(bpnQuantity.getBpnId());
                                            xmlSerializer.endTag("", BPNQuantity.XML_BPN_ID);


                                            xmlSerializer.startTag("", BPNQuantity.XML_IMEI);
                                            xmlSerializer.text(bpnQuantity.getImei());
                                            xmlSerializer.endTag("", BPNQuantity.XML_IMEI);

                                            xmlSerializer.startTag("", BPNQuantity.XML_COMPANY_CODE);
                                            xmlSerializer.text(bpnQuantity.getCompanyCode());
                                            xmlSerializer.endTag("", BPNQuantity.XML_COMPANY_CODE);

                                            xmlSerializer.startTag("", BPNQuantity.XML_ESTATE);
                                            xmlSerializer.text(bpnQuantity.getEstate());
                                            xmlSerializer.endTag("", BPNQuantity.XML_ESTATE);

                                            xmlSerializer.startTag("", BPNQuantity.XML_HARVEST_DATE);
                                            xmlSerializer.text(bpnQuantity.getBpnDate());
                                            xmlSerializer.endTag("", BPNQuantity.XML_HARVEST_DATE);

                                            xmlSerializer.startTag("", BPNQuantity.XML_DIVISION);
                                            xmlSerializer.text(bpnQuantity.getDivision());
                                            xmlSerializer.endTag("", BPNQuantity.XML_DIVISION);

                                            xmlSerializer.startTag("", BPNQuantity.XML_GANG);
                                            xmlSerializer.text(bpnQuantity.getGang());
                                            xmlSerializer.endTag("", BPNQuantity.XML_GANG);

                                            xmlSerializer.startTag("", BPNQuantity.XML_LOCATION);
                                            xmlSerializer.text(bpnQuantity.getLocation());
                                            xmlSerializer.endTag("", BPNQuantity.XML_LOCATION);

                                            xmlSerializer.startTag("", BPNQuantity.XML_TPH);
                                            xmlSerializer.text(bpnQuantity.getTph());
                                            xmlSerializer.endTag("", BPNQuantity.XML_TPH);

                                            xmlSerializer.startTag("", BPNQuantity.XML_NIK_HARVESTER);
                                            xmlSerializer.text(bpnQuantity.getNikHarvester());
                                            xmlSerializer.endTag("", BPNQuantity.XML_NIK_HARVESTER);

                                            xmlSerializer.startTag("", BPNQuantity.XML_CROP);
                                            xmlSerializer.text(bpnQuantity.getCrop());
                                            xmlSerializer.endTag("", BPNQuantity.XML_CROP);

                                            xmlSerializer.startTag("", BPNQuantity.XML_ACHIEVEMENT_CODE);
                                            xmlSerializer.text(bpnQuantity.getAchievementCode());
                                            xmlSerializer.endTag("", BPNQuantity.XML_ACHIEVEMENT_CODE);

                                            xmlSerializer.startTag("", BPNQuantity.XML_QUANTITY);
                                            xmlSerializer.text(String.valueOf(bpnQuantity.getQuantity()));
                                            xmlSerializer.endTag("", BPNQuantity.XML_QUANTITY);

                                            xmlSerializer.startTag("", BPNQuantity.XML_QUANTITY_REMAINING);
                                            xmlSerializer.text(String.valueOf(bpnQuantity.getQuantityRemaining()));
                                            xmlSerializer.endTag("", BPNQuantity.XML_QUANTITY_REMAINING);

                                            xmlSerializer.startTag("", BPNQuantity.XML_CREATED_DATE);
                                            if (bpnQuantity.getCreatedDate() > 0) {
                                                xmlSerializer.text(new DateLocal(new Date(bpnQuantity.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                            } else {
                                                xmlSerializer.text("");
                                            }
                                            xmlSerializer.endTag("", BPNQuantity.XML_CREATED_DATE);

                                            xmlSerializer.startTag("", BPNQuantity.XML_CREATED_BY);
                                            xmlSerializer.text(bpnQuantity.getCreatedBy());
                                            xmlSerializer.endTag("", BPNQuantity.XML_CREATED_BY);

                                            xmlSerializer.startTag("", BPNQuantity.XML_MODIFIED_DATE);
                                            if (bpnQuantity.getModifiedDate() > 0) {
                                                xmlSerializer.text(new DateLocal(new Date(bpnQuantity.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                            } else {
                                                xmlSerializer.text("");
                                            }
                                            xmlSerializer.endTag("", BPNQuantity.XML_MODIFIED_DATE);

                                            xmlSerializer.startTag("", BPNQuantity.XML_MODIFIED_BY);
                                            xmlSerializer.text(bpnQuantity.getModifiedBy());
                                            xmlSerializer.endTag("", BPNQuantity.XML_MODIFIED_BY);

                                            xmlSerializer.endTag("", BPNQuantity.XML_DOCUMENT);
                                        }
                                    }

                                    //quality
                                    List<Object> listObjectQuality;

                                    if (status != null) {
                                        listObjectQuality = database.getListData(false, BPNQuality.TABLE_NAME, null,
                                                BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                        BPNQuality.XML_IMEI + "=?" + " and " +
                                                        BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                        BPNQuality.XML_ESTATE + "=?" + " and " +
                                                        BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                        BPNQuality.XML_DIVISION + "=?" + " and " +
                                                        BPNQuality.XML_GANG + "=?" + " and " +
                                                        BPNQuality.XML_LOCATION + "=?" + " and " +
                                                        BPNQuality.XML_TPH + "=?" + " and " +
                                                        BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
                                                        BPNQuality.XML_STATUS + "=?",
                                                new String[]{id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester, status},
                                                null, null, BPNQuality.XML_QUALITY_CODE, null);
                                    } else {
                                        listObjectQuality = database.getListData(false, BPNQuality.TABLE_NAME, null,
                                                BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                        BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                        BPNQuality.XML_ESTATE + "=?" + " and " +
                                                        BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                        BPNQuality.XML_DIVISION + "=?" + " and " +
                                                        BPNQuality.XML_GANG + "=?" + " and " +
                                                        BPNQuality.XML_LOCATION + "=?" + " and " +
                                                        BPNQuality.XML_TPH + "=?" + " and " +
                                                        BPNQuality.XML_NIK_HARVESTER + "=?",
                                                new String[]{id, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester},
                                                null, null, BPNQuality.XML_QUALITY_CODE, null);
                                    }

                                    if (listObjectQuality.size() > 0) {
                                        for (int j = 0; j < listObjectQuality.size(); j++) {

                                            BPNQuality bpnQuality = (BPNQuality) listObjectQuality.get(j);
                                            bpnQuality.setStatus(1);

                                            String achievementCode = bpnQuality.getAchievementCode();
                                            String qualityCode = bpnQuality.getQualityCode();

//											database.updateData(bpnQuality,
//													BPNQuality.XML_BPN_ID + "=?" + " and " +
//													BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
//													BPNQuality.XML_ESTATE + "=?" + " and " +
//													BPNQuality.XML_BPN_DATE + "=?" + " and " +
//													BPNQuality.XML_DIVISION + "=?" + " and " +
//													BPNQuality.XML_GANG + "=?" + " and " +
//													BPNQuality.XML_LOCATION + "=?" + " and " +
//													BPNQuality.XML_TPH + "=?" + " and " +
//													BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
//													BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
//													BPNQuality.XML_QUALITY_CODE + "=?",
//													new String [] {id, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester,
//													achievementCode, qualityCode});

                                            if (bpnQuality.getQuantity() > 0) {
                                                rowCount = rowCount + 1;

                                                xmlSerializer.startTag("", BPNQuality.XML_DOCUMENT);

                                                xmlSerializer.startTag("", BPNQuality.XML_BPN_ID);
                                                xmlSerializer.text(bpnQuality.getBpnId());
                                                xmlSerializer.endTag("", BPNQuality.XML_BPN_ID);

                                                xmlSerializer.startTag("", BPNQuality.XML_IMEI);
                                                xmlSerializer.text(bpnQuality.getImei());
                                                xmlSerializer.endTag("", BPNQuality.XML_IMEI);

                                                xmlSerializer.startTag("", BPNQuality.XML_COMPANY_CODE);
                                                xmlSerializer.text(bpnQuality.getCompanyCode());
                                                xmlSerializer.endTag("", BPNQuality.XML_COMPANY_CODE);

                                                xmlSerializer.startTag("", BPNQuality.XML_ESTATE);
                                                xmlSerializer.text(bpnQuality.getEstate());
                                                xmlSerializer.endTag("", BPNQuality.XML_ESTATE);

                                                xmlSerializer.startTag("", BPNQuality.XML_HARVEST_DATE);
                                                xmlSerializer.text(bpnQuality.getBpnDate());
                                                xmlSerializer.endTag("", BPNQuality.XML_HARVEST_DATE);

                                                xmlSerializer.startTag("", BPNQuality.XML_DIVISION);
                                                xmlSerializer.text(bpnQuality.getDivision());
                                                xmlSerializer.endTag("", BPNQuality.XML_DIVISION);

                                                xmlSerializer.startTag("", BPNQuality.XML_GANG);
                                                xmlSerializer.text(bpnQuality.getGang());
                                                xmlSerializer.endTag("", BPNQuality.XML_GANG);

                                                xmlSerializer.startTag("", BPNQuality.XML_LOCATION);
                                                xmlSerializer.text(bpnQuality.getLocation());
                                                xmlSerializer.endTag("", BPNQuality.XML_LOCATION);

                                                xmlSerializer.startTag("", BPNQuality.XML_TPH);
                                                xmlSerializer.text(bpnQuality.getTph());
                                                xmlSerializer.endTag("", BPNQuality.XML_TPH);

                                                xmlSerializer.startTag("", BPNQuality.XML_NIK_HARVESTER);
                                                xmlSerializer.text(bpnQuality.getNikHarvester());
                                                xmlSerializer.endTag("", BPNQuality.XML_NIK_HARVESTER);

                                                xmlSerializer.startTag("", BPNQuality.XML_CROP);
                                                xmlSerializer.text(bpnQuality.getCrop());
                                                xmlSerializer.endTag("", BPNQuality.XML_CROP);

                                                xmlSerializer.startTag("", BPNQuality.XML_ACHIEVEMENT_CODE);
                                                xmlSerializer.text(bpnQuality.getAchievementCode());
                                                xmlSerializer.endTag("", BPNQuality.XML_ACHIEVEMENT_CODE);

                                                xmlSerializer.startTag("", BPNQuality.XML_QUALITY_CODE);
                                                xmlSerializer.text(bpnQuality.getQualityCode());
                                                xmlSerializer.endTag("", BPNQuality.XML_QUALITY_CODE);

                                                xmlSerializer.startTag("", BPNQuality.XML_QUANTITY);
                                                xmlSerializer.text(String.valueOf(bpnQuality.getQuantity()));
                                                xmlSerializer.endTag("", BPNQuality.XML_QUANTITY);

                                                xmlSerializer.startTag("", BPNQuality.XML_CREATED_DATE);
                                                if (bpnQuality.getCreatedDate() > 0) {
                                                    xmlSerializer.text(new DateLocal(new Date(bpnQuality.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                                } else {
                                                    xmlSerializer.text("");
                                                }
                                                xmlSerializer.endTag("", BPNQuality.XML_CREATED_DATE);

                                                xmlSerializer.startTag("", BPNQuality.XML_CREATED_BY);
                                                xmlSerializer.text(bpnQuality.getCreatedBy());
                                                xmlSerializer.endTag("", BPNQuality.XML_CREATED_BY);

                                                xmlSerializer.startTag("", BPNQuality.XML_MODIFIED_DATE);
                                                if (bpnQuality.getModifiedDate() > 0) {
                                                    xmlSerializer.text(new DateLocal(new Date(bpnQuality.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                                } else {
                                                    xmlSerializer.text("");
                                                }
                                                xmlSerializer.endTag("", BPNQuality.XML_MODIFIED_DATE);

                                                xmlSerializer.startTag("", BPNQuality.XML_MODIFIED_BY);
                                                xmlSerializer.text(bpnQuality.getModifiedBy());
                                                xmlSerializer.endTag("", BPNQuality.XML_MODIFIED_BY);

                                                xmlSerializer.endTag("", BPNQuality.XML_DOCUMENT);
                                            }
                                        }
                                    }
                                }
                                xmlSerializer.endTag("", BPNHeader.XML_DOCUMENT);
                            }
                        }

                        //Fernando -- Trigger Export BPN Get Modified Date From BPN Quantity Remaining, Start
                        if (listObjectBPNQuantityBackDateFilter.size() > 0) {
                            List<String> listObjectBPNHeaderBackDateStr = new ArrayList<String>();
                            for (BPNQuantityBackDateObj bpnQuantityBackDate : listObjectBPNQuantityBackDateFilter) {
                                if (!listObjectBPNHeaderBackDateStr.contains(bpnQuantityBackDate.getBpnDate()))
                                    listObjectBPNHeaderBackDateStr.add(bpnQuantityBackDate.getBpnDate());
                            }

                            for (int a = 0; a < listObjectBPNHeaderBackDateStr.size(); a++) {
                                String bpndatebackdatestr = listObjectBPNHeaderBackDateStr.get(a);

                                List<Object> listObjectBPNHeaderBackDate;
                                if (status != null) {
                                    listObjectBPNHeaderBackDate = database.getListData(false, BPNHeader.TABLE_NAME, null,
                                            //BPNHeader.XML_BPN_ID + "=?" + " and " +
                                            BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNHeader.XML_ESTATE + "=?" + " and " +
                                                    BPNHeader.XML_DIVISION + "=?" + " and " +
                                                    BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                    BPNHeader.XML_STATUS + "=?" + " and " +
                                                    BPNHeader.XML_BPN_DATE + "=?",// + " and " +
                                            //BPNHeader.XML_BPN_ID + "=?",
                                            new String[]{companyCode, estate, division, nik, status, bpndatebackdatestr},
                                            null, null, BPNHeader.XML_BPN_DATE, null);
                                } else {
                                    listObjectBPNHeaderBackDate = database.getListData(false, BPNHeader.TABLE_NAME, null,
                                            //BPNHeader.XML_BPN_ID + "=?" + " and " +
                                            BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNHeader.XML_ESTATE + "=?" + " and " +
                                                    BPNHeader.XML_DIVISION + "=?" + " and " +
                                                    BPNHeader.XML_NIK_CLERK + "=?" + " and " +
                                                    BPNHeader.XML_BPN_DATE + "=?", //+ " and " +
                                            //BPNHeader.XML_BPN_ID + "=?",
                                            new String[]{companyCode, estate, division, nik, bpndatebackdatestr},
                                            null, null, BPNHeader.XML_BPN_DATE, null);
                                }

                                if (listObjectBPNHeaderBackDate.size() > 0) {
									/*xmlSerializer.setOutput(writer);
									xmlSerializer.startDocument("UTF-8", true);
									xmlSerializer.startTag("", BPNHeader.XML_FILE);*/

                                    for (int j = 0; j < listObjectBPNHeaderBackDate.size(); j++) {
                                        rowCount = rowCount + 1;

                                        BPNHeader bpnHeader = (BPNHeader) listObjectBPNHeaderBackDate.get(j);
                                        bpnHeader.setStatus(1);

                                        String id = bpnHeader.getBpnId();
                                        String imei = bpnHeader.getImei();
                                        String bpnDate = bpnHeader.getBpnDate();
                                        String gang = bpnHeader.getGang();
                                        String location = bpnHeader.getLocation();
                                        String tph = bpnHeader.getTph();
                                        String nikHarvester = bpnHeader.getNikHarvester();

                                        xmlSerializer.startTag("", BPNHeader.XML_DOCUMENT);
                                        xmlSerializer.startTag("", BPNHeader.XML_BPN_ID);
                                        xmlSerializer.text(bpnHeader.getBpnId());
                                        xmlSerializer.endTag("", BPNHeader.XML_BPN_ID);

                                        xmlSerializer.startTag("", BPNHeader.XML_IMEI);
                                        xmlSerializer.text(bpnHeader.getImei());
                                        xmlSerializer.endTag("", BPNHeader.XML_IMEI);

                                        xmlSerializer.startTag("", BPNHeader.XML_COMPANY_CODE);
                                        xmlSerializer.text(bpnHeader.getCompanyCode());
                                        xmlSerializer.endTag("", BPNHeader.XML_COMPANY_CODE);

                                        xmlSerializer.startTag("", BPNHeader.XML_ESTATE);
                                        xmlSerializer.text(bpnHeader.getEstate());
                                        xmlSerializer.endTag("", BPNHeader.XML_ESTATE);

                                        xmlSerializer.startTag("", BPNHeader.XML_HARVEST_DATE);
                                        xmlSerializer.text(bpnHeader.getBpnDate());
                                        xmlSerializer.endTag("", BPNHeader.XML_HARVEST_DATE);

                                        xmlSerializer.startTag("", BPNHeader.XML_DIVISION);
                                        xmlSerializer.text(bpnHeader.getDivision());
                                        xmlSerializer.endTag("", BPNHeader.XML_DIVISION);

                                        xmlSerializer.startTag("", BPNHeader.XML_GANG);
                                        xmlSerializer.text(bpnHeader.getGang());
                                        xmlSerializer.endTag("", BPNHeader.XML_GANG);

                                        xmlSerializer.startTag("", BPNHeader.XML_LOCATION);
                                        xmlSerializer.text(bpnHeader.getLocation());
                                        xmlSerializer.endTag("", BPNHeader.XML_LOCATION);

                                        xmlSerializer.startTag("", BPNHeader.XML_TPH);
                                        xmlSerializer.text(bpnHeader.getTph());
                                        xmlSerializer.endTag("", BPNHeader.XML_TPH);

                                        xmlSerializer.startTag("", BPNHeader.XML_NIK_HARVESTER);
                                        xmlSerializer.text(bpnHeader.getNikHarvester());
                                        xmlSerializer.endTag("", BPNHeader.XML_NIK_HARVESTER);

                                        xmlSerializer.startTag("", BPNHeader.XML_HARVESTER);
                                        xmlSerializer.text(bpnHeader.getHarvester());
                                        xmlSerializer.endTag("", BPNHeader.XML_HARVESTER);

                                        xmlSerializer.startTag("", BPNHeader.XML_NIK_FOREMAN);
                                        xmlSerializer.text(bpnHeader.getNikForeman());
                                        xmlSerializer.endTag("", BPNHeader.XML_NIK_FOREMAN);

                                        xmlSerializer.startTag("", BPNHeader.XML_FOREMAN);
                                        xmlSerializer.text(bpnHeader.getForeman());
                                        xmlSerializer.endTag("", BPNHeader.XML_FOREMAN);

                                        xmlSerializer.startTag("", BPNHeader.XML_NIK_CLERK);
                                        xmlSerializer.text(bpnHeader.getNikClerk());
                                        xmlSerializer.endTag("", BPNHeader.XML_NIK_CLERK);

                                        xmlSerializer.startTag("", BPNHeader.XML_CLERK);
                                        xmlSerializer.text(bpnHeader.getClerk());
                                        xmlSerializer.endTag("", BPNHeader.XML_CLERK);

                                        //								xmlSerializer.startTag("", BPNHeader.XML_USE_GERDANG);
                                        //								xmlSerializer.text(String.valueOf(bpnHeader.isUseGerdang()));
                                        //								xmlSerializer.endTag("", BPNHeader.XML_USE_GERDANG);

                                        xmlSerializer.startTag("", BPNHeader.XML_CROP);
                                        xmlSerializer.text(bpnHeader.getCrop());
                                        xmlSerializer.endTag("", BPNHeader.XML_CROP);

                                        xmlSerializer.startTag("", BPNHeader.XML_GPS_KOORDINAT);
                                        xmlSerializer.text(bpnHeader.getGpsKoordinat());
                                        xmlSerializer.endTag("", BPNHeader.XML_GPS_KOORDINAT);

                                        xmlSerializer.startTag("", BPNHeader.XML_PHOTO);
                                        xmlSerializer.text(bpnHeader.getPhoto());
                                        xmlSerializer.endTag("", BPNHeader.XML_PHOTO);

                                        xmlSerializer.startTag("", BPNHeader.XML_SPBS_NUMBER);
                                        xmlSerializer.text(bpnHeader.getSpbsNumber());
                                        xmlSerializer.endTag("", BPNHeader.XML_SPBS_NUMBER);

                                        xmlSerializer.startTag("", BPNHeader.XML_CREATED_DATE);
                                        if (bpnHeader.getCreatedDate() > 0) {
                                            xmlSerializer.text(new DateLocal(new Date(bpnHeader.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                        } else {
                                            xmlSerializer.text("");
                                        }
                                        xmlSerializer.endTag("", BPNHeader.XML_CREATED_DATE);

                                        xmlSerializer.startTag("", BPNHeader.XML_CREATED_BY);
                                        xmlSerializer.text(bpnHeader.getCreatedBy());
                                        xmlSerializer.endTag("", BPNHeader.XML_CREATED_BY);

                                        xmlSerializer.startTag("", BPNHeader.XML_MODIFIED_DATE);
                                        if (bpnHeader.getModifiedDate() > 0) {
                                            xmlSerializer.text(new DateLocal(new Date(bpnHeader.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                        } else {
                                            xmlSerializer.text("");
                                        }
                                        xmlSerializer.endTag("", BPNHeader.XML_MODIFIED_DATE);

                                        xmlSerializer.startTag("", BPNHeader.XML_MODIFIED_BY);
                                        xmlSerializer.text(bpnHeader.getModifiedBy());
                                        xmlSerializer.endTag("", BPNHeader.XML_MODIFIED_BY);


                                        List<Object> listObjectQuantity;

                                        if (status != null) {
                                            listObjectQuantity = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                                                    BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                            BPNQuantity.XML_IMEI + "=?" + " and " +
                                                            BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                            BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                            BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                            BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                            BPNQuantity.XML_GANG + "=?" + " and " +
                                                            BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                            BPNQuantity.XML_TPH + "=?" + " and " +
                                                            BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                                                            BPNQuantity.XML_STATUS + "=?",
                                                    new String[]{id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester, status},
                                                    null, null, BPNQuantity.XML_ACHIEVEMENT_CODE, null);
                                        } else {
                                            listObjectQuantity = database.getListData(false, BPNQuantity.TABLE_NAME, null,
                                                    BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                            BPNQuantity.XML_IMEI + "=?" + " and " +
                                                            BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                            BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                            BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                            BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                            BPNQuantity.XML_GANG + "=?" + " and " +
                                                            BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                            BPNQuantity.XML_TPH + "=?" + " and " +
                                                            BPNQuantity.XML_NIK_HARVESTER + "=?",
                                                    new String[]{id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester},
                                                    null, null, BPNQuantity.XML_ACHIEVEMENT_CODE, null);
                                        }

                                        if (listObjectQuantity.size() > 0) {

                                            for (int k = 0; k < listObjectQuantity.size(); k++) {

                                                BPNQuantity bpnQuantity = (BPNQuantity) listObjectQuantity.get(k);
                                                bpnQuantity.setStatus(1);

                                                String crop = bpnQuantity.getCrop();
                                                String achievementCode = bpnQuantity.getAchievementCode();

                                                if ((achievementCode.equalsIgnoreCase(BPNQuantity.LOOSE_FRUIT_CODE) && bpnQuantity.getQuantity() > 0) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.JANJANG_CODE)) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.LATEX_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.LATEX_DRC_CODE)) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.LUMP_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.LUMP_DRC_CODE)) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.SLAB_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.SLAB_DRC_CODE)) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.TREELACE_WET_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.TREELACE_DRC_CODE)) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.GOOD_WEIGHT_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.GOOD_QTY_CODE)) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.BAD_WEIGHT_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.BAD_QTY_CODE)) ||
                                                        (achievementCode.equalsIgnoreCase(BPNQuantity.POOR_WEIGHT_CODE)) || (achievementCode.equalsIgnoreCase(BPNQuantity.POOR_QTY_CODE))) {

                                                    rowCount = rowCount + 1;

                                                    xmlSerializer.startTag("", BPNQuantity.XML_DOCUMENT);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_BPN_ID);
                                                    xmlSerializer.text(bpnQuantity.getBpnId());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_BPN_ID);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_IMEI);
                                                    xmlSerializer.text(bpnQuantity.getImei());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_IMEI);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_COMPANY_CODE);
                                                    xmlSerializer.text(bpnQuantity.getCompanyCode());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_COMPANY_CODE);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_ESTATE);
                                                    xmlSerializer.text(bpnQuantity.getEstate());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_ESTATE);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_HARVEST_DATE);
                                                    xmlSerializer.text(bpnQuantity.getBpnDate());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_HARVEST_DATE);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_DIVISION);
                                                    xmlSerializer.text(bpnQuantity.getDivision());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_DIVISION);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_GANG);
                                                    xmlSerializer.text(bpnQuantity.getGang());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_GANG);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_LOCATION);
                                                    xmlSerializer.text(bpnQuantity.getLocation());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_LOCATION);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_TPH);
                                                    xmlSerializer.text(bpnQuantity.getTph());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_TPH);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_NIK_HARVESTER);
                                                    xmlSerializer.text(bpnQuantity.getNikHarvester());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_NIK_HARVESTER);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_CROP);
                                                    xmlSerializer.text(bpnQuantity.getCrop());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_CROP);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_ACHIEVEMENT_CODE);
                                                    xmlSerializer.text(bpnQuantity.getAchievementCode());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_ACHIEVEMENT_CODE);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_QUANTITY);
                                                    xmlSerializer.text(String.valueOf(bpnQuantity.getQuantity()));
                                                    xmlSerializer.endTag("", BPNQuantity.XML_QUANTITY);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_QUANTITY_REMAINING);
                                                    xmlSerializer.text(String.valueOf(bpnQuantity.getQuantityRemaining()));
                                                    xmlSerializer.endTag("", BPNQuantity.XML_QUANTITY_REMAINING);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_CREATED_DATE);
                                                    if (bpnQuantity.getCreatedDate() > 0) {
                                                        xmlSerializer.text(new DateLocal(new Date(bpnQuantity.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                                    } else {
                                                        xmlSerializer.text("");
                                                    }
                                                    xmlSerializer.endTag("", BPNQuantity.XML_CREATED_DATE);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_CREATED_BY);
                                                    xmlSerializer.text(bpnQuantity.getCreatedBy());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_CREATED_BY);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_MODIFIED_DATE);
                                                    if (bpnQuantity.getModifiedDate() > 0) {
                                                        xmlSerializer.text(new DateLocal(new Date(bpnQuantity.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                                    } else {
                                                        xmlSerializer.text("");
                                                    }
                                                    xmlSerializer.endTag("", BPNQuantity.XML_MODIFIED_DATE);

                                                    xmlSerializer.startTag("", BPNQuantity.XML_MODIFIED_BY);
                                                    xmlSerializer.text(bpnQuantity.getModifiedBy());
                                                    xmlSerializer.endTag("", BPNQuantity.XML_MODIFIED_BY);

                                                    xmlSerializer.endTag("", BPNQuantity.XML_DOCUMENT);
                                                }

                                            }

                                            //quality
                                            List<Object> listObjectQuality;

                                            if (status != null) {
                                                listObjectQuality = database.getListData(false, BPNQuality.TABLE_NAME, null,
                                                        BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                                BPNQuality.XML_IMEI + "=?" + " and " +
                                                                BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                                BPNQuality.XML_ESTATE + "=?" + " and " +
                                                                BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                                BPNQuality.XML_DIVISION + "=?" + " and " +
                                                                BPNQuality.XML_GANG + "=?" + " and " +
                                                                BPNQuality.XML_LOCATION + "=?" + " and " +
                                                                BPNQuality.XML_TPH + "=?" + " and " +
                                                                BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
                                                                BPNQuality.XML_STATUS + "=?",
                                                        new String[]{id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester, status},
                                                        null, null, BPNQuality.XML_QUALITY_CODE, null);
                                            } else {
                                                listObjectQuality = database.getListData(false, BPNQuality.TABLE_NAME, null,
                                                        BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                                BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                                BPNQuality.XML_ESTATE + "=?" + " and " +
                                                                BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                                BPNQuality.XML_DIVISION + "=?" + " and " +
                                                                BPNQuality.XML_GANG + "=?" + " and " +
                                                                BPNQuality.XML_LOCATION + "=?" + " and " +
                                                                BPNQuality.XML_TPH + "=?" + " and " +
                                                                BPNQuality.XML_NIK_HARVESTER + "=?",
                                                        new String[]{id, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester},
                                                        null, null, BPNQuality.XML_QUALITY_CODE, null);
                                            }

                                            if (listObjectQuality.size() > 0) {

                                                for (int l = 0; l < listObjectQuality.size(); l++) {

                                                    BPNQuality bpnQuality = (BPNQuality) listObjectQuality.get(l);
                                                    bpnQuality.setStatus(1);

                                                    String achievementCode = bpnQuality.getAchievementCode();
                                                    String qualityCode = bpnQuality.getQualityCode();

                                                    if (bpnQuality.getQuantity() > 0) {

                                                        rowCount = rowCount + 1;

                                                        xmlSerializer.startTag("", BPNQuality.XML_DOCUMENT);

                                                        xmlSerializer.startTag("", BPNQuality.XML_BPN_ID);
                                                        xmlSerializer.text(bpnQuality.getBpnId());
                                                        xmlSerializer.endTag("", BPNQuality.XML_BPN_ID);

                                                        xmlSerializer.startTag("", BPNQuality.XML_IMEI);
                                                        xmlSerializer.text(bpnQuality.getImei());
                                                        xmlSerializer.endTag("", BPNQuality.XML_IMEI);

                                                        xmlSerializer.startTag("", BPNQuality.XML_COMPANY_CODE);
                                                        xmlSerializer.text(bpnQuality.getCompanyCode());
                                                        xmlSerializer.endTag("", BPNQuality.XML_COMPANY_CODE);

                                                        xmlSerializer.startTag("", BPNQuality.XML_ESTATE);
                                                        xmlSerializer.text(bpnQuality.getEstate());
                                                        xmlSerializer.endTag("", BPNQuality.XML_ESTATE);

                                                        xmlSerializer.startTag("", BPNQuality.XML_HARVEST_DATE);
                                                        xmlSerializer.text(bpnQuality.getBpnDate());
                                                        xmlSerializer.endTag("", BPNQuality.XML_HARVEST_DATE);

                                                        xmlSerializer.startTag("", BPNQuality.XML_DIVISION);
                                                        xmlSerializer.text(bpnQuality.getDivision());
                                                        xmlSerializer.endTag("", BPNQuality.XML_DIVISION);

                                                        xmlSerializer.startTag("", BPNQuality.XML_GANG);
                                                        xmlSerializer.text(bpnQuality.getGang());
                                                        xmlSerializer.endTag("", BPNQuality.XML_GANG);

                                                        xmlSerializer.startTag("", BPNQuality.XML_LOCATION);
                                                        xmlSerializer.text(bpnQuality.getLocation());
                                                        xmlSerializer.endTag("", BPNQuality.XML_LOCATION);

                                                        xmlSerializer.startTag("", BPNQuality.XML_TPH);
                                                        xmlSerializer.text(bpnQuality.getTph());
                                                        xmlSerializer.endTag("", BPNQuality.XML_TPH);

                                                        xmlSerializer.startTag("", BPNQuality.XML_NIK_HARVESTER);
                                                        xmlSerializer.text(bpnQuality.getNikHarvester());
                                                        xmlSerializer.endTag("", BPNQuality.XML_NIK_HARVESTER);

                                                        xmlSerializer.startTag("", BPNQuality.XML_CROP);
                                                        xmlSerializer.text(bpnQuality.getCrop());
                                                        xmlSerializer.endTag("", BPNQuality.XML_CROP);

                                                        xmlSerializer.startTag("", BPNQuality.XML_ACHIEVEMENT_CODE);
                                                        xmlSerializer.text(bpnQuality.getAchievementCode());
                                                        xmlSerializer.endTag("", BPNQuality.XML_ACHIEVEMENT_CODE);

                                                        xmlSerializer.startTag("", BPNQuality.XML_QUALITY_CODE);
                                                        xmlSerializer.text(bpnQuality.getQualityCode());
                                                        xmlSerializer.endTag("", BPNQuality.XML_QUALITY_CODE);

                                                        xmlSerializer.startTag("", BPNQuality.XML_QUANTITY);
                                                        xmlSerializer.text(String.valueOf(bpnQuality.getQuantity()));
                                                        xmlSerializer.endTag("", BPNQuality.XML_QUANTITY);

                                                        xmlSerializer.startTag("", BPNQuality.XML_CREATED_DATE);
                                                        if (bpnQuality.getCreatedDate() > 0) {
                                                            xmlSerializer.text(new DateLocal(new Date(bpnQuality.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                                        } else {
                                                            xmlSerializer.text("");
                                                        }
                                                        xmlSerializer.endTag("", BPNQuality.XML_CREATED_DATE);

                                                        xmlSerializer.startTag("", BPNQuality.XML_CREATED_BY);
                                                        xmlSerializer.text(bpnQuality.getCreatedBy());
                                                        xmlSerializer.endTag("", BPNQuality.XML_CREATED_BY);

                                                        xmlSerializer.startTag("", BPNQuality.XML_MODIFIED_DATE);
                                                        if (bpnQuality.getModifiedDate() > 0) {
                                                            xmlSerializer.text(new DateLocal(new Date(bpnQuality.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                                        } else {
                                                            xmlSerializer.text("");
                                                        }
                                                        xmlSerializer.endTag("", BPNQuality.XML_MODIFIED_DATE);

                                                        xmlSerializer.startTag("", BPNQuality.XML_MODIFIED_BY);
                                                        xmlSerializer.text(bpnQuality.getModifiedBy());
                                                        xmlSerializer.endTag("", BPNQuality.XML_MODIFIED_BY);

                                                        xmlSerializer.endTag("", BPNQuality.XML_DOCUMENT);
                                                    }
                                                }
                                            }

                                        }
                                        xmlSerializer.endTag("", BPNHeader.XML_DOCUMENT);
                                    }
                                }
                            }
                        }
                        //Fernando -- Trigger Export BPN Get Modified Date From BPN Quantity Remaining, End


                        xmlSerializer.endTag("", BPNHeader.XML_FILE);
                        xmlSerializer.endDocument();

                        String xml = writer.toString();
                        //String fileName = "M2NBPN" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
                        //modified by adit 20161226
                        String fileName = "M2NBPN" + "_" + estate + "_" + division + "_" + date.replace("-", "") + "_" + "010101" + "_" + rowCount + ".xml";

                        msgStatus = saveXml(xml, fileName, "BPN", isUsbOtgSave);

                        if (msgStatus != null && msgStatus.isStatus()) {
                            database.commitTransaction();
                        }

//						if(isUsbOtgSave){
//							File[] files = context.getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
//
//							if(files.length > 1){
//								folder = new File(files[files.length-1], FolderHandler.REPORT + "/" + FolderHandler.NEW).getAbsolutePath();
//							}
//						}else{
//							FolderHandler folderHandler = new FolderHandler(context);
//
//							if(folderHandler.init()){
//								folder = folderHandler.getFileReportNew();
//							}
//						}
//
//						if(TextUtils.isEmpty(folder)){
//							if(new FileXMLHandler(context).createFile(folder, fileName, xml)){
//								database.commitTransaction();
//
//								msgStatus = new MessageStatus("BPN", true, context.getResources().getString(R.string.export_successed));
//							}else{
//								msgStatus = new MessageStatus("BPN", false, context.getResources().getString(R.string.export_failed));
//							}
//						}else{
//							msgStatus = new MessageStatus("BPN", false, context.getResources().getString(R.string.error_sd_card));
//						}
                    } else {
                        msgStatus = new MessageStatus("BPN", false, context.getResources().getString(R.string.data_empty));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BPN", false, e.getMessage());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BPN", false, e.getMessage());
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BPN", false, e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BPN", false, e.getMessage());
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BPN", false, e.getMessage());
                } finally {
                    database.closeTransaction();
                }
                break;
            case R.id.cbxExportBKM:
                try {
                    List<Object> listObjectBKMHeader;

                    database.openTransaction();
                    if (status != null) {
                        listObjectBKMHeader = database.getListData(false, BKMHeader.TABLE_NAME, null,
                                BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        BKMHeader.XML_ESTATE + "=?" + " and " +
                                        BKMHeader.XML_DIVISION + "=?" + " and " +
                                        BKMHeader.XML_NIK_FOREMAN + "=?" + " and " +
                                        BKMHeader.XML_STATUS + "=?" + " and " +
                                        BKMHeader.XML_BKM_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, status, date},
                                null, null, BKMHeader.XML_BKM_DATE, null);
                    } else {
                        listObjectBKMHeader = database.getListData(false, BKMHeader.TABLE_NAME, null,
                                BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        BKMHeader.XML_ESTATE + "=?" + " and " +
                                        BKMHeader.XML_DIVISION + "=?" + " and " +
                                        BKMHeader.XML_NIK_FOREMAN + "=?" + " and " +
                                        BKMHeader.XML_BKM_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, date},
                                null, null, BKMHeader.XML_BKM_DATE, null);
                    }

                    if (listObjectBKMHeader.size() > 0) {
                        xmlSerializer.setOutput(writer);
                        xmlSerializer.startDocument("UTF-8", true);
                        xmlSerializer.startTag("", BKMHeader.XML_FILE);

                        rowCount = rowCount + listObjectBKMHeader.size();

                        for (int i = 0; i < listObjectBKMHeader.size(); i++) {
                            BKMHeader bkmHeader = (BKMHeader) listObjectBKMHeader.get(i);
                            bkmHeader.setStatus(1);

                            String imei = bkmHeader.getImei();
                            String bkmDate = bkmHeader.getBkmDate();
                            String gang = bkmHeader.getGang();

//							database.updateData(bkmHeader,
//									BKMHeader.XML_IMEI + "=?" + " and " +
//									BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
//									BKMHeader.XML_BKM_DATE + "=?" + " and " +
//									BKMHeader.XML_ESTATE + "=?" + " and " +
//									BKMHeader.XML_DIVISION + "=?" + " and " +
//									BKMHeader.XML_GANG + "=?",
//									new String [] {imei, companyCode, bkmDate, estate, division, gang});

                            xmlSerializer.startTag("", BKMHeader.XML_DOCUMENT);
                            xmlSerializer.startTag("", BKMHeader.XML_IMEI);
                            xmlSerializer.text(bkmHeader.getImei());
                            xmlSerializer.endTag("", BKMHeader.XML_IMEI);

                            xmlSerializer.startTag("", BKMHeader.XML_COMPANY_CODE);
                            xmlSerializer.text(bkmHeader.getCompanyCode());
                            xmlSerializer.endTag("", BKMHeader.XML_COMPANY_CODE);

                            xmlSerializer.startTag("", BKMHeader.XML_ESTATE);
                            xmlSerializer.text(bkmHeader.getEstate());
                            xmlSerializer.endTag("", BKMHeader.XML_ESTATE);

                            xmlSerializer.startTag("", BKMHeader.XML_BKM_DATE);
                            xmlSerializer.text(bkmHeader.getBkmDate());
                            xmlSerializer.endTag("", BKMHeader.XML_BKM_DATE);

                            xmlSerializer.startTag("", BKMHeader.XML_DIVISION);
                            xmlSerializer.text(bkmHeader.getDivision());
                            xmlSerializer.endTag("", BKMHeader.XML_DIVISION);

                            xmlSerializer.startTag("", BKMHeader.XML_GANG);
                            xmlSerializer.text(bkmHeader.getGang());
                            xmlSerializer.endTag("", BKMHeader.XML_GANG);

                            xmlSerializer.startTag("", BKMHeader.XML_NIK_FOREMAN);
                            xmlSerializer.text(bkmHeader.getNikForeman());
                            xmlSerializer.endTag("", BKMHeader.XML_NIK_FOREMAN);

                            xmlSerializer.startTag("", BKMHeader.XML_FOREMAN);
                            xmlSerializer.text(bkmHeader.getForeman());
                            xmlSerializer.endTag("", BKMHeader.XML_FOREMAN);

                            xmlSerializer.startTag("", BKMHeader.XML_NIK_CLERK);
                            xmlSerializer.text(bkmHeader.getNikClerk());
                            xmlSerializer.endTag("", BKMHeader.XML_NIK_CLERK);

                            xmlSerializer.startTag("", BKMHeader.XML_CLERK);
                            xmlSerializer.text(bkmHeader.getClerk());
                            xmlSerializer.endTag("", BKMHeader.XML_CLERK);

                            xmlSerializer.startTag("", BKMHeader.XML_GPS_KOORDINAT);
                            xmlSerializer.text(bkmHeader.getGpsKoordinat());
                            xmlSerializer.endTag("", BKMHeader.XML_GPS_KOORDINAT);

                            xmlSerializer.startTag("", BKMHeader.XML_CREATED_DATE);
                            if (bkmHeader.getCreatedDate() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(bkmHeader.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", BKMHeader.XML_CREATED_DATE);

                            xmlSerializer.startTag("", BKMHeader.XML_CREATED_BY);
                            xmlSerializer.text(bkmHeader.getCreatedBy());
                            xmlSerializer.endTag("", BKMHeader.XML_CREATED_BY);

                            xmlSerializer.startTag("", BKMHeader.XML_MODIFIED_DATE);
                            if (bkmHeader.getModifiedDate() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(bkmHeader.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", BKMHeader.XML_MODIFIED_DATE);

                            xmlSerializer.startTag("", BKMHeader.XML_MODIFIED_BY);
                            xmlSerializer.text(bkmHeader.getModifiedBy());
                            xmlSerializer.endTag("", BKMHeader.XML_MODIFIED_BY);

                            //line
                            List<Object> listObjectLine;

                            if (status != null) {
                                listObjectLine = database.getListData(false, BKMLine.TABLE_NAME, null,
                                        BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMLine.XML_ESTATE + "=?" + " and " +
                                                BKMLine.XML_BKM_DATE + "=?" + " and " +
                                                BKMLine.XML_DIVISION + "=?" + " and " +
                                                BKMLine.XML_GANG + "=?" + " and " +
                                                BKMLine.XML_STATUS + "=?",
                                        new String[]{companyCode, estate, bkmDate, division, gang, status},
                                        null, null, BKMLine.XML_NAME, null);
                            } else {
                                listObjectLine = database.getListData(false, BKMLine.TABLE_NAME, null,
                                        BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMLine.XML_ESTATE + "=?" + " and " +
                                                BKMLine.XML_BKM_DATE + "=?" + " and " +
                                                BKMLine.XML_DIVISION + "=?" + " and " +
                                                BKMLine.XML_GANG + "=?",
                                        new String[]{companyCode, estate, bkmDate, division, gang},
                                        null, null, BKMLine.XML_NAME, null);
                            }

                            if (listObjectLine.size() > 0) {

                                rowCount = rowCount + listObjectLine.size();

                                for (int j = 0; j < listObjectLine.size(); j++) {
                                    BKMLine bkmLine = (BKMLine) listObjectLine.get(j);
                                    bkmLine.setStatus(1);

                                    String nikHarvester = bkmLine.getNik();

//										database.updateData(bkmLine,
//											BKMLine.XML_IMEI + "=?" + " and " +
//											BKMLine.XML_COMPANY_CODE + "=?" + " and " +
//											BKMLine.XML_ESTATE + "=?" + " and " +
//											BKMLine.XML_BKM_DATE + "=?" + " and " +
//											BKMLine.XML_DIVISION + "=?" + " and " +
//											BKMLine.XML_GANG + "=?" + " and " +
//											BKMLine.XML_NIK + "=?",
//											new String [] {imei, companyCode, estate, bkmDate, division, gang, nikHarvester});

                                    xmlSerializer.startTag("", BKMLine.XML_DOCUMENT);

                                    xmlSerializer.startTag("", BKMLine.XML_IMEI);
                                    xmlSerializer.text(bkmLine.getImei());
                                    xmlSerializer.endTag("", BKMLine.XML_IMEI);

                                    xmlSerializer.startTag("", BKMLine.XML_COMPANY_CODE);
                                    xmlSerializer.text(bkmLine.getCompanyCode());
                                    xmlSerializer.endTag("", BKMLine.XML_COMPANY_CODE);

                                    xmlSerializer.startTag("", BKMLine.XML_ESTATE);
                                    xmlSerializer.text(bkmLine.getEstate());
                                    xmlSerializer.endTag("", BKMLine.XML_ESTATE);

                                    xmlSerializer.startTag("", BKMLine.XML_BKM_DATE);
                                    xmlSerializer.text(bkmLine.getBkmDate());
                                    xmlSerializer.endTag("", BKMLine.XML_BKM_DATE);

                                    xmlSerializer.startTag("", BKMLine.XML_DIVISION);
                                    xmlSerializer.text(bkmLine.getDivision());
                                    xmlSerializer.endTag("", BKMLine.XML_DIVISION);

                                    xmlSerializer.startTag("", BKMLine.XML_GANG_CODE);
                                    xmlSerializer.text(bkmLine.getGang());
                                    xmlSerializer.endTag("", BKMLine.XML_GANG_CODE);

                                    xmlSerializer.startTag("", BKMLine.XML_NIK);
                                    xmlSerializer.text(bkmLine.getNik());
                                    xmlSerializer.endTag("", BKMLine.XML_NIK);

                                    xmlSerializer.startTag("", BKMLine.XML_NAME);
                                    xmlSerializer.text(bkmLine.getName());
                                    xmlSerializer.endTag("", BKMLine.XML_NAME);

                                    xmlSerializer.startTag("", BKMLine.XML_ABSENT_TYPE);
                                    xmlSerializer.text(bkmLine.getAbsentType());
                                    xmlSerializer.endTag("", BKMLine.XML_ABSENT_TYPE);

                                    xmlSerializer.startTag("", BKMLine.XML_MANDAYS);
                                    xmlSerializer.text(String.valueOf(bkmLine.getMandays()));
                                    xmlSerializer.endTag("", BKMLine.XML_MANDAYS);

                                    xmlSerializer.startTag("", BKMLine.XML_UOM);
                                    xmlSerializer.text(bkmLine.getUom());
                                    xmlSerializer.endTag("", BKMLine.XML_UOM);

                                    xmlSerializer.startTag("", BKMLine.XML_USE_GERDANG);
                                    xmlSerializer.text(String.valueOf(bkmLine.getUseGerdang()));
                                    xmlSerializer.endTag("", BKMLine.XML_USE_GERDANG);

                                    xmlSerializer.startTag("", BKMLine.XML_CREATED_DATE);
                                    if (bkmLine.getCreatedDate() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(bkmLine.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }
                                    xmlSerializer.endTag("", BKMLine.XML_CREATED_DATE);

                                    xmlSerializer.startTag("", BKMLine.XML_CREATED_BY);
                                    xmlSerializer.text(bkmLine.getCreatedBy());
                                    xmlSerializer.endTag("", BKMLine.XML_CREATED_BY);

                                    xmlSerializer.startTag("", BKMLine.XML_MODIFIED_DATE);
                                    if (bkmLine.getModifiedDate() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(bkmLine.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }
                                    xmlSerializer.endTag("", BKMLine.XML_MODIFIED_DATE);

                                    xmlSerializer.startTag("", BKMLine.XML_MODIFIED_BY);
                                    xmlSerializer.text(bkmLine.getModifiedBy());
                                    xmlSerializer.endTag("", BKMLine.XML_MODIFIED_BY);

                                    xmlSerializer.endTag("", BKMLine.XML_DOCUMENT);
                                }

                                //output
                                List<Object> listObjectOutput;

                                if (status != null) {
                                    listObjectOutput = database.getListData(false, BKMOutput.TABLE_NAME, null,
                                            BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMOutput.XML_ESTATE + "=?" + " and " +
                                                    BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                                    BKMOutput.XML_DIVISION + "=?" + " and " +
                                                    BKMOutput.XML_GANG + "=?" + " and " +
                                                    BKMOutput.XML_STATUS + "=?",
                                            new String[]{companyCode, estate, bkmDate, division, gang, status},
                                            null, null, BKMOutput.XML_NAME, null);
                                } else {
                                    listObjectOutput = database.getListData(false, BKMOutput.TABLE_NAME, null,
                                            BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMOutput.XML_ESTATE + "=?" + " and " +
                                                    BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                                    BKMOutput.XML_DIVISION + "=?" + " and " +
                                                    BKMOutput.XML_GANG + "=?",
                                            new String[]{companyCode, estate, bkmDate, division, gang},
                                            null, null, BKMOutput.XML_NAME, null);
                                }

                                if (listObjectOutput.size() > 0) {

                                    rowCount = rowCount + listObjectOutput.size();

                                    for (int j = 0; j < listObjectOutput.size(); j++) {
                                        BKMOutput bkmOutput = (BKMOutput) listObjectOutput.get(j);
                                        bkmOutput.setStatus(1);

                                        String block = bkmOutput.getBlock();
                                        String nikHarvester = bkmOutput.getNik();

//											database.updateData(bkmOutput,
//													BKMOutput.XML_IMEI + "=?" + " and " +
//													BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
//													BKMOutput.XML_ESTATE + "=?" + " and " +
//													BKMOutput.XML_BKM_DATE + "=?" + " and " +
//													BKMOutput.XML_DIVISION + "=?" + " and " +
//													BKMOutput.XML_GANG + "=?" + " and " +
//													BKMOutput.XML_BLOCK + "=?" + " and " +
//													BKMOutput.XML_NIK + "=?",
//													new String [] {companyCode, estate, bkmDate, division, gang, block, nikHarvester});

                                        xmlSerializer.startTag("", BKMOutput.XML_DOCUMENT);

                                        xmlSerializer.startTag("", BKMOutput.XML_IMEI);
                                        xmlSerializer.text(bkmOutput.getImei());
                                        xmlSerializer.endTag("", BKMOutput.XML_IMEI);

                                        xmlSerializer.startTag("", BKMOutput.XML_COMPANY_CODE);
                                        xmlSerializer.text(bkmOutput.getCompanyCode());
                                        xmlSerializer.endTag("", BKMOutput.XML_COMPANY_CODE);

                                        xmlSerializer.startTag("", BKMOutput.XML_ESTATE);
                                        xmlSerializer.text(bkmOutput.getEstate());
                                        xmlSerializer.endTag("", BKMOutput.XML_ESTATE);

                                        xmlSerializer.startTag("", BKMOutput.XML_BKM_DATE);
                                        xmlSerializer.text(bkmOutput.getBkmDate());
                                        xmlSerializer.endTag("", BKMOutput.XML_BKM_DATE);

                                        xmlSerializer.startTag("", BKMOutput.XML_DIVISION);
                                        xmlSerializer.text(bkmOutput.getDivision());
                                        xmlSerializer.endTag("", BKMOutput.XML_DIVISION);

                                        xmlSerializer.startTag("", BKMOutput.XML_GANG_CODE);
                                        xmlSerializer.text(bkmOutput.getGang());
                                        xmlSerializer.endTag("", BKMOutput.XML_GANG_CODE);

                                        xmlSerializer.startTag("", BKMOutput.XML_BLOCK);
                                        xmlSerializer.text(bkmOutput.getBlock());
                                        xmlSerializer.endTag("", BKMOutput.XML_BLOCK);

                                        xmlSerializer.startTag("", BKMOutput.XML_NIK);
                                        xmlSerializer.text(bkmOutput.getNik());
                                        xmlSerializer.endTag("", BKMOutput.XML_NIK);

                                        xmlSerializer.startTag("", BKMOutput.XML_NAME);
                                        xmlSerializer.text(bkmOutput.getName());
                                        xmlSerializer.endTag("", BKMOutput.XML_NAME);

                                        xmlSerializer.startTag("", BKMOutput.XML_OUTPUT);
                                        xmlSerializer.text(String.valueOf(bkmOutput.getOutput()));
                                        xmlSerializer.endTag("", BKMOutput.XML_OUTPUT);

                                        xmlSerializer.startTag("", BKMOutput.XML_UOM);
                                        xmlSerializer.text(bkmOutput.getUom());
                                        xmlSerializer.endTag("", BKMOutput.XML_UOM);

                                        xmlSerializer.startTag("", BKMOutput.XML_CREATED_DATE);
                                        if (bkmOutput.getCreatedDate() > 0) {
                                            xmlSerializer.text(new DateLocal(new Date(bkmOutput.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                        } else {
                                            xmlSerializer.text("");
                                        }
                                        xmlSerializer.endTag("", BKMOutput.XML_CREATED_DATE);

                                        xmlSerializer.startTag("", BKMOutput.XML_CREATED_BY);
                                        xmlSerializer.text(bkmOutput.getCreatedBy());
                                        xmlSerializer.endTag("", BKMOutput.XML_CREATED_BY);

                                        xmlSerializer.startTag("", BKMOutput.XML_MODIFIED_DATE);
                                        if (bkmOutput.getModifiedDate() > 0) {
                                            xmlSerializer.text(new DateLocal(new Date(bkmOutput.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                        } else {
                                            xmlSerializer.text("");
                                        }
                                        xmlSerializer.endTag("", BKMOutput.XML_MODIFIED_DATE);

                                        xmlSerializer.startTag("", BKMOutput.XML_MODIFIED_BY);
                                        xmlSerializer.text(bkmOutput.getModifiedBy());
                                        xmlSerializer.endTag("", BKMOutput.XML_MODIFIED_BY);

                                        xmlSerializer.endTag("", BKMOutput.XML_DOCUMENT);
                                    }
                                }
                            }
                            xmlSerializer.endTag("", BKMHeader.XML_DOCUMENT);
                        }

                        xmlSerializer.endTag("", BKMHeader.XML_FILE);
                        xmlSerializer.endDocument();

                        String xml = writer.toString();
                        //String fileName = "M2NBKM" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
                        //modified by adit
                        String fileName = "M2NBKM" + "_" + estate + "_" + division + "_" + date.replace("-", "") + "_" + "010101" + "_" + rowCount + ".xml";

                        msgStatus = saveXml(xml, fileName, "BKM", isUsbOtgSave);

                        if (msgStatus != null && msgStatus.isStatus()) {
                            database.commitTransaction();
                        }

//						FolderHandler folderHandler = new FolderHandler(context);
//
//						if(folderHandler.init()){
//							String fileName = "M2NBKM" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
//							String folder = folderHandler.getFileReportNew();
//
//							if(new FileXMLHandler(context).createFile(folder, fileName, xml)){
//								database.commitTransaction();
//
//								msgStatus = new MessageStatus("BKM", true, context.getResources().getString(R.string.export_successed));
//							}else{
//								msgStatus = new MessageStatus("BKM", false, context.getResources().getString(R.string.export_failed));
//							}
//						}else{
//							msgStatus = new MessageStatus("BKM", false, context.getResources().getString(R.string.error_sd_card));
//						}
                    } else {
                        msgStatus = new MessageStatus("BKM", false, context.getResources().getString(R.string.data_empty));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BKM", false, e.getMessage());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BKM", false, e.getMessage());
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BKM", false, e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BKM", false, e.getMessage());
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("BKM", false, e.getMessage());
                } finally {
                    database.closeTransaction();
                }
                break;
            case R.id.cbxExportSPBS:
                try {
                    List<Object> listSpbsHeader;

                    database.openTransaction();
                    if (status != null) {
                        listSpbsHeader = database.getListData(false, SPBSHeader.TABLE_NAME, null,
                                SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        SPBSHeader.XML_ESTATE + "=?" + " and " +
                                        SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
                                        SPBSHeader.XML_STATUS + "=?" + " and " +
                                        SPBSHeader.XML_SPBS_DATE + "=?",
                                new String[]{companyCode, estate, nik, status, date},
                                null, null, null, null);
                    } else {
                        listSpbsHeader = database.getListData(false, SPBSHeader.TABLE_NAME, null,
                                SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        SPBSHeader.XML_ESTATE + "=?" + " and " +
                                        SPBSHeader.XML_NIK_CLERK + "=?" + " and " +
                                        SPBSHeader.XML_SPBS_DATE + "=?",
                                new String[]{companyCode, estate, nik, date},
                                null, null, null, null);
                    }

                    if (listSpbsHeader.size() > 0) {
                        xmlSerializer.setOutput(writer);
                        xmlSerializer.startDocument("UTF-8", true);
                        xmlSerializer.startTag("", SPBSHeader.XML_FILE);

                        rowCount = rowCount + listSpbsHeader.size();

                        for (int i = 0; i < listSpbsHeader.size(); i++) {
                            SPBSHeader spbsHeader = (SPBSHeader) listSpbsHeader.get(i);
                            spbsHeader.setStatus(1);

                            String imei = spbsHeader.getImei();
                            String year = spbsHeader.getYear();
                            String spbsNumber = spbsHeader.getSpbsNumber();
                            String spbsDate = spbsHeader.getSpbsDate();
                            String crop = spbsHeader.getCrop();

//							database.updateData(spbsHeader,
//								SPBSHeader.XML_IMEI + "=?" + " and " +
//								SPBSHeader.XML_YEAR + "=?" + " and " +
//								SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
//								SPBSHeader.XML_ESTATE + "=?" + " and " +
//								SPBSHeader.XML_CROP + "=?" + " and " +
//								SPBSHeader.XML_SPBS_NUMBER + "=?",
//								new String [] {imei, year, companyCode, estate, crop, spbsNumber});

                            xmlSerializer.startTag("", SPBSHeader.XML_DOCUMENT);
                            xmlSerializer.startTag("", SPBSHeader.XML_IMEI);
                            xmlSerializer.text(spbsHeader.getImei());
                            xmlSerializer.endTag("", SPBSHeader.XML_IMEI);

                            xmlSerializer.startTag("", SPBSHeader.XML_YEAR);
                            xmlSerializer.text(spbsHeader.getYear());
                            xmlSerializer.endTag("", SPBSHeader.XML_YEAR);

                            xmlSerializer.startTag("", SPBSHeader.XML_COMPANY_CODE);
                            xmlSerializer.text(spbsHeader.getCompanyCode());
                            xmlSerializer.endTag("", SPBSHeader.XML_COMPANY_CODE);

                            xmlSerializer.startTag("", SPBSHeader.XML_ESTATE);
                            xmlSerializer.text(spbsHeader.getEstate());
                            xmlSerializer.endTag("", SPBSHeader.XML_ESTATE);

                            xmlSerializer.startTag("", SPBSHeader.XML_CROP);
                            xmlSerializer.text(spbsHeader.getCrop());
                            xmlSerializer.endTag("", SPBSHeader.XML_CROP);

                            xmlSerializer.startTag("", SPBSHeader.XML_SPBS_NUMBER);
                            xmlSerializer.text(spbsHeader.getSpbsNumber());
                            xmlSerializer.endTag("", SPBSHeader.XML_SPBS_NUMBER);

                            xmlSerializer.startTag("", SPBSHeader.XML_SPBS_DATE);
                            xmlSerializer.text(spbsHeader.getSpbsDate());
                            xmlSerializer.endTag("", SPBSHeader.XML_SPBS_DATE);

                            if (!TextUtils.isEmpty(spbsHeader.getDestId())) {
                                xmlSerializer.startTag("", SPBSHeader.XML_DEST_ID);
                                xmlSerializer.text(spbsHeader.getDestId());
                                xmlSerializer.endTag("", SPBSHeader.XML_DEST_ID);

                                xmlSerializer.startTag("", SPBSHeader.XML_DEST_DESC);
                                xmlSerializer.text(spbsHeader.getDestDesc());
                                xmlSerializer.endTag("", SPBSHeader.XML_DEST_DESC);

                                xmlSerializer.startTag("", SPBSHeader.XML_DEST_TYPE);
                                xmlSerializer.text(spbsHeader.getDestType());
                                xmlSerializer.endTag("", SPBSHeader.XML_DEST_TYPE);
                            }

                            xmlSerializer.startTag("", SPBSHeader.XML_DIVISION);
                            xmlSerializer.text(spbsHeader.getDivision());
                            xmlSerializer.endTag("", SPBSHeader.XML_DIVISION);

                            xmlSerializer.startTag("", SPBSHeader.XML_NIK_ASSISTANT);
                            xmlSerializer.text(spbsHeader.getNikAssistant());
                            xmlSerializer.endTag("", SPBSHeader.XML_NIK_ASSISTANT);

                            xmlSerializer.startTag("", SPBSHeader.XML_ASSISTANT);
                            xmlSerializer.text(spbsHeader.getAssistant());
                            xmlSerializer.endTag("", SPBSHeader.XML_ASSISTANT);

                            xmlSerializer.startTag("", SPBSHeader.XML_NIK_CLERK);
                            xmlSerializer.text(spbsHeader.getNikClerk());
                            xmlSerializer.endTag("", SPBSHeader.XML_NIK_CLERK);

                            xmlSerializer.startTag("", SPBSHeader.XML_CLERK);
                            xmlSerializer.text(spbsHeader.getClerk());
                            xmlSerializer.endTag("", SPBSHeader.XML_CLERK);

                            xmlSerializer.startTag("", SPBSHeader.XML_NIK_DRIVER);
                            xmlSerializer.text(spbsHeader.getNikDriver());
                            xmlSerializer.endTag("", SPBSHeader.XML_NIK_DRIVER);

                            xmlSerializer.startTag("", SPBSHeader.XML_DRIVER);
                            xmlSerializer.text(spbsHeader.getDriver());
                            xmlSerializer.endTag("", SPBSHeader.XML_DRIVER);

                            xmlSerializer.startTag("", SPBSHeader.XML_NIK_KERNET);
                            xmlSerializer.text(spbsHeader.getNikKernet());
                            xmlSerializer.endTag("", SPBSHeader.XML_NIK_KERNET);

                            xmlSerializer.startTag("", SPBSHeader.XML_KERNET);
                            xmlSerializer.text(spbsHeader.getKernet());
                            xmlSerializer.endTag("", SPBSHeader.XML_KERNET);

                            xmlSerializer.startTag("", SPBSHeader.XML_LICENSE_PLATE);
                            xmlSerializer.text(spbsHeader.getLicensePlate());
                            xmlSerializer.endTag("", SPBSHeader.XML_LICENSE_PLATE);

                            xmlSerializer.startTag("", SPBSHeader.XML_RUNNING_ACCOUNT);
                            xmlSerializer.text(spbsHeader.getRunningAccount());
                            xmlSerializer.endTag("", SPBSHeader.XML_RUNNING_ACCOUNT);

                            xmlSerializer.startTag("", SPBSHeader.XML_GPS_KOORDINAT);
                            xmlSerializer.text(spbsHeader.getGpsKoordinat());
                            xmlSerializer.endTag("", SPBSHeader.XML_GPS_KOORDINAT);

                            xmlSerializer.startTag("", SPBSHeader.XML_CREATED_DATE);
                            if (spbsHeader.getCreatedDate() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(spbsHeader.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", SPBSHeader.XML_CREATED_DATE);

                            xmlSerializer.startTag("", SPBSHeader.XML_CREATED_BY);
                            xmlSerializer.text(spbsHeader.getCreatedBy());
                            xmlSerializer.endTag("", SPBSHeader.XML_CREATED_BY);

                            xmlSerializer.startTag("", SPBSHeader.XML_MODIFIED_DATE);
                            if (spbsHeader.getModifiedDate() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(spbsHeader.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", SPBSHeader.XML_MODIFIED_DATE);

                            xmlSerializer.startTag("", SPBSHeader.XML_MODIFIED_BY);
                            xmlSerializer.text(spbsHeader.getModifiedBy());
                            xmlSerializer.endTag("", SPBSHeader.XML_MODIFIED_BY);

                            xmlSerializer.startTag("", SPBSHeader.XML_LIFNR);
                            xmlSerializer.text(spbsHeader.getLifnr());
                            xmlSerializer.endTag("", SPBSHeader.XML_LIFNR);

                            //line
                            List<Object> listSpbsLine;

                            if (status != null) {
                                listSpbsLine = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                        SPBSLine.XML_IMEI + "=?" + " and " +
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_IS_SAVE + "=?" + " and " +
                                                SPBSLine.XML_STATUS + "=?",
                                        new String[]{imei, year, companyCode, estate, crop, spbsNumber, spbsDate, "1", status},
                                        null, null, SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_TPH, null);
                            } else {
                                listSpbsLine = database.getListData(false, SPBSLine.TABLE_NAME, null,
                                        SPBSLine.XML_IMEI + "=?" + " and " +
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_IS_SAVE + "=?",
                                        new String[]{imei, year, companyCode, estate, crop, spbsNumber, spbsDate, "1"},
                                        null, null, SPBSLine.XML_BLOCK + ", " + SPBSLine.XML_TPH, null);
                            }

                            if (listSpbsLine.size() > 0) {

                                rowCount = rowCount + listSpbsLine.size();

                                for (int j = 0; j < listSpbsLine.size(); j++) {
                                    SPBSLine spbsLine = (SPBSLine) listSpbsLine.get(j);
                                    spbsLine.setStatus(1);

                                    String block = spbsLine.getBlock();
                                    String tph = spbsLine.getTph();
                                    String bpnDate = spbsLine.getBpnDate();
                                    String achievementCode = spbsLine.getAchievementCode();

//										database.updateData(spbsLine,
//												SPBSLine.XML_IMEI + "=?" + " and " +
//												SPBSLine.XML_YEAR + "=?" + " and " +
//												SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
//												SPBSLine.XML_ESTATE + "=?" + " and " +
//												SPBSLine.XML_CROP + "=?" + " and " +
//												SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
//												SPBSLine.XML_SPBS_DATE + "=?" + " and " +
//												SPBSLine.XML_BLOCK + "=?" + " and " +
//												SPBSLine.XML_TPH + "=?" + " and " +
//												SPBSLine.XML_BPN_DATE + "=?" + " and " +
//												SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
//												SPBSLine.XML_IS_SAVE + "=?",
//												new String [] {imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
//												tph, bpnDate, achievementCode, "1"});

                                    xmlSerializer.startTag("", SPBSLine.XML_DOCUMENT);

                                    xmlSerializer.startTag("", SPBSLine.XML_ID);
                                    xmlSerializer.text(String.valueOf(spbsLine.getId()));
                                    xmlSerializer.endTag("", SPBSLine.XML_ID);

                                    xmlSerializer.startTag("", SPBSLine.XML_IMEI);
                                    xmlSerializer.text(spbsLine.getImei());
                                    xmlSerializer.endTag("", SPBSLine.XML_IMEI);

                                    xmlSerializer.startTag("", SPBSLine.XML_YEAR);
                                    xmlSerializer.text(spbsLine.getYear());
                                    xmlSerializer.endTag("", SPBSLine.XML_YEAR);

                                    xmlSerializer.startTag("", SPBSLine.XML_COMPANY_CODE);
                                    xmlSerializer.text(spbsLine.getCompanyCode());
                                    xmlSerializer.endTag("", SPBSLine.XML_COMPANY_CODE);

                                    xmlSerializer.startTag("", SPBSLine.XML_ESTATE);
                                    xmlSerializer.text(spbsLine.getEstate());
                                    xmlSerializer.endTag("", SPBSLine.XML_ESTATE);

                                    xmlSerializer.startTag("", SPBSLine.XML_CROP);
                                    xmlSerializer.text(spbsLine.getCrop());
                                    xmlSerializer.endTag("", SPBSLine.XML_CROP);

                                    xmlSerializer.startTag("", SPBSLine.XML_SPBS_NUMBER);
                                    xmlSerializer.text(spbsLine.getSpbsNumber());
                                    xmlSerializer.endTag("", SPBSLine.XML_SPBS_NUMBER);

                                    xmlSerializer.startTag("", SPBSLine.XML_SPBS_DATE);
                                    xmlSerializer.text(spbsLine.getSpbsDate());
                                    xmlSerializer.endTag("", SPBSLine.XML_SPBS_DATE);

                                    xmlSerializer.startTag("", SPBSLine.XML_BLOCK);
                                    xmlSerializer.text(spbsLine.getBlock());
                                    xmlSerializer.endTag("", SPBSLine.XML_BLOCK);

                                    xmlSerializer.startTag("", SPBSLine.XML_TPH);
                                    xmlSerializer.text(spbsLine.getTph());
                                    xmlSerializer.endTag("", SPBSLine.XML_TPH);

                                    xmlSerializer.startTag("", SPBSLine.XML_BPN_DATE);
                                    xmlSerializer.text(spbsLine.getBpnDate());
                                    xmlSerializer.endTag("", SPBSLine.XML_BPN_DATE);

                                    xmlSerializer.startTag("", SPBSLine.XML_BPN_ID);
                                    xmlSerializer.text(spbsLine.getBpnId());
                                    xmlSerializer.endTag("", SPBSLine.XML_BPN_ID);

                                    if (!TextUtils.isEmpty(spbsLine.getSpbsRef())) {
                                        xmlSerializer.startTag("", SPBSLine.XML_SPBS_PREV);
                                        xmlSerializer.text(spbsLine.getSpbsRef());
                                        xmlSerializer.endTag("", SPBSLine.XML_SPBS_PREV);
                                    }

                                    if (!TextUtils.isEmpty(spbsLine.getSpbsNext())) {
                                        xmlSerializer.startTag("", SPBSLine.XML_SPBS_NEXT);
                                        xmlSerializer.text(spbsLine.getSpbsNext());
                                        xmlSerializer.endTag("", SPBSLine.XML_SPBS_NEXT);
                                    }

                                    xmlSerializer.startTag("", SPBSLine.XML_ACHIEVEMENT_CODE);
                                    xmlSerializer.text(spbsLine.getAchievementCode());
                                    xmlSerializer.endTag("", SPBSLine.XML_ACHIEVEMENT_CODE);

                                    xmlSerializer.startTag("", SPBSLine.XML_QUANTITY);
                                    xmlSerializer.text(String.valueOf(spbsLine.getQuantity()));
                                    xmlSerializer.endTag("", SPBSLine.XML_QUANTITY);

                                    xmlSerializer.startTag("", SPBSLine.XML_QUANTITY_ANGKUT);
                                    xmlSerializer.text(String.valueOf(spbsLine.getQuantityAngkut()));
                                    xmlSerializer.endTag("", SPBSLine.XML_QUANTITY_ANGKUT);

                                    xmlSerializer.startTag("", SPBSLine.XML_QUANTITY_REMAINING);
                                    xmlSerializer.text(String.valueOf(spbsLine.getQuantityRemaining()));
                                    xmlSerializer.endTag("", SPBSLine.XML_QUANTITY_REMAINING);

                                    xmlSerializer.startTag("", SPBSLine.XML_UOM);
                                    xmlSerializer.text(spbsLine.getUom());
                                    xmlSerializer.endTag("", SPBSLine.XML_UOM);

                                    xmlSerializer.startTag("", SPBSLine.XML_TOTAL_HARVESTER);
                                    xmlSerializer.text(String.valueOf(spbsLine.getTotalHarvester()));
                                    xmlSerializer.endTag("", SPBSLine.XML_TOTAL_HARVESTER);

                                    xmlSerializer.startTag("", SPBSLine.XML_GPS_KOORDINAT);
                                    xmlSerializer.text(spbsLine.getGpsKoordinat());
                                    xmlSerializer.endTag("", SPBSLine.XML_GPS_KOORDINAT);

                                    xmlSerializer.startTag("", SPBSLine.XML_CREATED_DATE);
                                    if (spbsLine.getCreatedDate() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(spbsLine.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }
                                    xmlSerializer.endTag("", SPBSLine.XML_CREATED_DATE);

                                    xmlSerializer.startTag("", SPBSLine.XML_CREATED_BY);
                                    xmlSerializer.text(spbsLine.getCreatedBy());
                                    xmlSerializer.endTag("", SPBSLine.XML_CREATED_BY);

                                    xmlSerializer.startTag("", SPBSLine.XML_MODIFIED_DATE);
                                    if (spbsLine.getModifiedDate() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(spbsLine.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }
                                    xmlSerializer.endTag("", SPBSLine.XML_MODIFIED_DATE);

                                    xmlSerializer.startTag("", SPBSLine.XML_MODIFIED_BY);
                                    xmlSerializer.text(spbsLine.getModifiedBy());
                                    xmlSerializer.endTag("", SPBSLine.XML_MODIFIED_BY);

                                    BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                            BPNQuantity.XML_BPN_ID + "=?",
                                            new String[]{spbsLine.getBpnId()},
                                            null, null, null, null);

                                    xmlSerializer.startTag("", SPBSLine.XML_NIK_HARVESTER);
                                    xmlSerializer.text(bpnQuantity == null ? "" : bpnQuantity.getNikHarvester());
                                    xmlSerializer.endTag("", SPBSLine.XML_NIK_HARVESTER);

                                    xmlSerializer.endTag("", SPBSLine.XML_DOCUMENT);
                                }
                            }
                            xmlSerializer.endTag("", SPBSHeader.XML_DOCUMENT);
                        }

                        xmlSerializer.endTag("", SPBSHeader.XML_FILE);
                        xmlSerializer.endDocument();

                        String xml = writer.toString();
                        //String fileName = "M2NSPBS" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
                        //modified by adit 20161226
                        String fileName = "M2NSPBS" + "_" + estate + "_" + division + "_" + date.replace("-", "") + "_" + "010101" + "_" + rowCount + ".xml";

                        msgStatus = saveXml(xml, fileName, "SPBS", isUsbOtgSave);

                        if (msgStatus != null && msgStatus.isStatus()) {
                            database.commitTransaction();
                        }

//						FolderHandler folderHandler = new FolderHandler(context);
//
//						if(folderHandler.init()){
//							String fileName = "M2NSPBS" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
//							String folder = folderHandler.getFileReportNew();
//
//							if(new FileXMLHandler(context).createFile(folder, fileName, xml)){
//								database.commitTransaction();
//
//								msgStatus = new MessageStatus("SPBS", true, context.getResources().getString(R.string.export_successed));
//							}else{
//								msgStatus = new MessageStatus("SPBS", false, context.getResources().getString(R.string.export_failed));
//							}
//						}else{
//							msgStatus = new MessageStatus("SPBS", false, context.getResources().getString(R.string.error_sd_card));
//						}
                    } else {
                        msgStatus = new MessageStatus("SPBS", false, context.getResources().getString(R.string.data_empty));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("SPBS", false, e.getMessage());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("SPBS", false, e.getMessage());
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("SPBS", false, e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("SPBS", false, e.getMessage());
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("SPBS", false, e.getMessage());
                } finally {
                    database.closeTransaction();
                }
                break;
            case R.id.cbxExportTaksasi:
                try {
                    List<Object> listTaksasiHeader;

                    database.openTransaction();
                    if (status != null) {
                        listTaksasiHeader = database.getListData(false, TaksasiHeader.TABLE_NAME, null,
                                TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                        TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                        TaksasiHeader.XML_NIK_FOREMAN + "=?" + " and " +
                                        TaksasiHeader.XML_STATUS + "=?" + " and " +
                                        TaksasiHeader.XML_TAKSASI_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, status, date},
                                null, null, null, null);
                    } else {
                        listTaksasiHeader = database.getListData(false, TaksasiHeader.TABLE_NAME, null,
                                TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                        TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                        TaksasiHeader.XML_NIK_FOREMAN + "=?" + " and " +
                                        TaksasiHeader.XML_TAKSASI_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, date},
                                null, null, null, null);
                    }

                    if (listTaksasiHeader.size() > 0) {
                        xmlSerializer.setOutput(writer);
                        xmlSerializer.startDocument("UTF-8", true);
                        xmlSerializer.startTag("", TaksasiHeader.XML_FILE);

                        rowCount = rowCount + listTaksasiHeader.size();

                        for (int i = 0; i < listTaksasiHeader.size(); i++) {
                            TaksasiHeader taksasiHeader = (TaksasiHeader) listTaksasiHeader.get(i);
                            taksasiHeader.setStatus(1);

                            String imei = taksasiHeader.getImei();
                            String taksasiDate = taksasiHeader.getTaksasiDate();
                            String block = taksasiHeader.getBlock();
                            String crop = taksasiHeader.getCrop();

//							database.updateData(taksasiHeader,
//								TaksasiHeader.XML_IMEI + "=?" + " and " +
//								TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
//								TaksasiHeader.XML_ESTATE + "=?" + " and " +
//								TaksasiHeader.XML_DIVISION + "=?" + " and " +
//								TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
//								TaksasiHeader.XML_BLOCK + "=?" + " and " +
//								TaksasiHeader.XML_CROP + "=?",
//								new String [] {imei, companyCode, estate, division, taksasiDate, block, crop});

                            xmlSerializer.startTag("", TaksasiHeader.XML_DOCUMENT);
                            xmlSerializer.startTag("", TaksasiHeader.XML_IMEI);
                            xmlSerializer.text(taksasiHeader.getImei());
                            xmlSerializer.endTag("", TaksasiHeader.XML_IMEI);

                            xmlSerializer.startTag("", TaksasiHeader.XML_COMPANY_CODE);
                            xmlSerializer.text(taksasiHeader.getCompanyCode());
                            xmlSerializer.endTag("", TaksasiHeader.XML_COMPANY_CODE);

                            xmlSerializer.startTag("", TaksasiHeader.XML_ESTATE);
                            xmlSerializer.text(taksasiHeader.getEstate());
                            xmlSerializer.endTag("", TaksasiHeader.XML_ESTATE);

                            xmlSerializer.startTag("", TaksasiHeader.XML_DIVISION);
                            xmlSerializer.text(taksasiHeader.getDivision());
                            xmlSerializer.endTag("", TaksasiHeader.XML_DIVISION);

                            xmlSerializer.startTag("", TaksasiHeader.XML_TAKSASI_DATE);
                            xmlSerializer.text(taksasiHeader.getTaksasiDate());
                            xmlSerializer.endTag("", TaksasiHeader.XML_TAKSASI_DATE);

                            xmlSerializer.startTag("", TaksasiHeader.XML_BLOCK);
                            xmlSerializer.text(taksasiHeader.getBlock());
                            xmlSerializer.endTag("", TaksasiHeader.XML_BLOCK);

                            xmlSerializer.startTag("", TaksasiHeader.XML_CROP);
                            xmlSerializer.text(taksasiHeader.getCrop());
                            xmlSerializer.endTag("", TaksasiHeader.XML_CROP);

                            xmlSerializer.startTag("", TaksasiHeader.XML_NIK_FOREMAN);
                            xmlSerializer.text(taksasiHeader.getNikForeman());
                            xmlSerializer.endTag("", TaksasiHeader.XML_NIK_FOREMAN);

                            xmlSerializer.startTag("", TaksasiHeader.XML_FOREMAN);
                            xmlSerializer.text(taksasiHeader.getForeman());
                            xmlSerializer.endTag("", TaksasiHeader.XML_FOREMAN);

                            xmlSerializer.startTag("", TaksasiHeader.XML_PROD_TREES);
                            xmlSerializer.text(String.valueOf(taksasiHeader.getProdTrees()));
                            xmlSerializer.endTag("", TaksasiHeader.XML_PROD_TREES);

                            xmlSerializer.startTag("", TaksasiHeader.XML_BJR);
                            xmlSerializer.text(String.valueOf(taksasiHeader.getBjr()));
                            xmlSerializer.endTag("", TaksasiHeader.XML_BJR);

                            xmlSerializer.startTag("", TaksasiHeader.XML_GPS_KOORDINAT);
                            xmlSerializer.text(taksasiHeader.getGpsKoordinat());
                            xmlSerializer.endTag("", TaksasiHeader.XML_GPS_KOORDINAT);

                            xmlSerializer.startTag("", TaksasiHeader.XML_CREATED_DATE);
                            if (taksasiHeader.getCreatedDate() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(taksasiHeader.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", TaksasiHeader.XML_CREATED_DATE);

                            xmlSerializer.startTag("", TaksasiHeader.XML_CREATED_BY);
                            xmlSerializer.text(taksasiHeader.getCreatedBy());
                            xmlSerializer.endTag("", TaksasiHeader.XML_CREATED_BY);

                            xmlSerializer.startTag("", TaksasiHeader.XML_MODIFIED_DATE);
                            if (taksasiHeader.getModifiedDate() > 0) {
                                xmlSerializer.text(new DateLocal(new Date(taksasiHeader.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                            } else {
                                xmlSerializer.text("");
                            }
                            xmlSerializer.endTag("", TaksasiHeader.XML_MODIFIED_DATE);

                            xmlSerializer.startTag("", TaksasiHeader.XML_MODIFIED_BY);
                            xmlSerializer.text(taksasiHeader.getModifiedBy());
                            xmlSerializer.endTag("", TaksasiHeader.XML_MODIFIED_BY);

                            //line
                            List<Object> listTaksasiLine;

                            if (status != null) {
                                listTaksasiLine = database.getListData(false, TaksasiLine.TABLE_NAME, null,
                                        TaksasiLine.XML_IMEI + "=?" + " and " +
                                                TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_CROP + "=?" + " and " +
                                                TaksasiLine.XML_IS_SAVE + "=?" + " and " +
                                                TaksasiLine.XML_STATUS + "=?",
                                        new String[]{imei, companyCode, estate, division, taksasiDate, block, crop, "1", status},
                                        null, null,
                                        TaksasiLine.XML_BLOCK + ", " + TaksasiLine.XML_BARIS_SKB + ", " + TaksasiLine.XML_BARIS_BLOCK + ", " + TaksasiLine.XML_LINE_SKB,
                                        null);
                            } else {
                                listTaksasiLine = database.getListData(false, TaksasiLine.TABLE_NAME, null,
                                        TaksasiLine.XML_IMEI + "=?" + " and " +
                                                TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_CROP + "=?" + " and " +
                                                TaksasiLine.XML_IS_SAVE + "=?",
                                        new String[]{imei, companyCode, estate, division, taksasiDate, block, crop, "1"},
                                        null, null,
                                        TaksasiLine.XML_BLOCK + ", " + TaksasiLine.XML_BARIS_SKB + ", " + TaksasiLine.XML_BARIS_BLOCK + ", " + TaksasiLine.XML_LINE_SKB,
                                        null);
                            }

                            if (listTaksasiLine.size() > 0) {

                                rowCount = rowCount + listTaksasiLine.size();

                                for (int j = 0; j < listTaksasiLine.size(); j++) {
                                    TaksasiLine taksasiLine = (TaksasiLine) listTaksasiLine.get(j);
                                    taksasiLine.setStatus(1);

                                    int barisSkb = taksasiLine.getBarisSkb();
                                    int barisBlock = taksasiLine.getBarisBlock();
                                    int lineSkb = taksasiLine.getLineSkb();

//										database.updateData(taksasiLine,
//												TaksasiLine.XML_IMEI + "=?" + " and " +
//												TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
//												TaksasiLine.XML_ESTATE + "=?" + " and " +
//												TaksasiLine.XML_DIVISION + "=?" + " and " +
//												TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
//												TaksasiLine.XML_BLOCK + "=?" + " and " +
//												TaksasiLine.XML_CROP + "=?" + " and " +
//												TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
//												TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
//												TaksasiLine.XML_LINE_SKB + "=?" + " and " +
//												TaksasiLine.XML_IS_SAVE + "=?",
//												new String [] {imei, companyCode, estate, division, taksasiDate, block, crop,
//												String.valueOf(barisSkb), String.valueOf(barisBlock), String.valueOf(lineSkb), "1"});

                                    xmlSerializer.startTag("", TaksasiLine.XML_DOCUMENT);

                                    xmlSerializer.startTag("", TaksasiLine.XML_IMEI);
                                    xmlSerializer.text(taksasiLine.getImei());
                                    xmlSerializer.endTag("", TaksasiLine.XML_IMEI);

                                    xmlSerializer.startTag("", TaksasiLine.XML_COMPANY_CODE);
                                    xmlSerializer.text(taksasiLine.getCompanyCode());
                                    xmlSerializer.endTag("", TaksasiLine.XML_COMPANY_CODE);

                                    xmlSerializer.startTag("", TaksasiLine.XML_ESTATE);
                                    xmlSerializer.text(taksasiLine.getEstate());
                                    xmlSerializer.endTag("", TaksasiLine.XML_ESTATE);

                                    xmlSerializer.startTag("", TaksasiLine.XML_DIVISION);
                                    xmlSerializer.text(taksasiLine.getDivision());
                                    xmlSerializer.endTag("", TaksasiLine.XML_DIVISION);

                                    xmlSerializer.startTag("", TaksasiLine.XML_TAKSASI_DATE);
                                    xmlSerializer.text(taksasiLine.getTaksasiDate());
                                    xmlSerializer.endTag("", TaksasiLine.XML_TAKSASI_DATE);

                                    xmlSerializer.startTag("", TaksasiLine.XML_BLOCK);
                                    xmlSerializer.text(taksasiLine.getBlock());
                                    xmlSerializer.endTag("", TaksasiLine.XML_BLOCK);

                                    xmlSerializer.startTag("", TaksasiLine.XML_CROP);
                                    xmlSerializer.text(taksasiLine.getCrop());
                                    xmlSerializer.endTag("", TaksasiLine.XML_CROP);

                                    xmlSerializer.startTag("", TaksasiLine.XML_BARIS_SKB);
                                    xmlSerializer.text(String.valueOf(taksasiLine.getBarisSkb()));
                                    xmlSerializer.endTag("", TaksasiLine.XML_BARIS_SKB);

                                    xmlSerializer.startTag("", TaksasiLine.XML_BARIS_BLOCK);
                                    xmlSerializer.text(String.valueOf(taksasiLine.getBarisBlock()));
                                    xmlSerializer.endTag("", TaksasiLine.XML_BARIS_BLOCK);

                                    xmlSerializer.startTag("", TaksasiLine.XML_LINE_SKB);
                                    xmlSerializer.text(String.valueOf(taksasiLine.getLineSkb()));
                                    xmlSerializer.endTag("", TaksasiLine.XML_LINE_SKB);

                                    xmlSerializer.startTag("", TaksasiLine.XML_QTY_POKOK);
                                    xmlSerializer.text(String.valueOf(taksasiLine.getQtyPokok()));
                                    xmlSerializer.endTag("", TaksasiLine.XML_QTY_POKOK);

                                    xmlSerializer.startTag("", TaksasiLine.XML_QTY_JANJANG);
                                    xmlSerializer.text(String.valueOf(taksasiLine.getQtyJanjang()));
                                    xmlSerializer.endTag("", TaksasiLine.XML_QTY_JANJANG);

                                    xmlSerializer.startTag("", TaksasiLine.XML_GPS_KOORDINAT);
                                    xmlSerializer.text(taksasiLine.getGpsKoordinat());
                                    xmlSerializer.endTag("", TaksasiLine.XML_GPS_KOORDINAT);

                                    xmlSerializer.startTag("", TaksasiLine.XML_CREATED_DATE);
                                    if (taksasiLine.getCreatedDate() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(taksasiLine.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }
                                    xmlSerializer.endTag("", TaksasiLine.XML_CREATED_DATE);

                                    xmlSerializer.startTag("", TaksasiLine.XML_CREATED_BY);
                                    xmlSerializer.text(taksasiLine.getCreatedBy());
                                    xmlSerializer.endTag("", TaksasiLine.XML_CREATED_BY);

                                    xmlSerializer.startTag("", TaksasiLine.XML_MODIFIED_DATE);
                                    if (taksasiLine.getModifiedDate() > 0) {
                                        xmlSerializer.text(new DateLocal(new Date(taksasiLine.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                    } else {
                                        xmlSerializer.text("");
                                    }
                                    xmlSerializer.endTag("", TaksasiLine.XML_MODIFIED_DATE);

                                    xmlSerializer.startTag("", TaksasiLine.XML_MODIFIED_BY);
                                    xmlSerializer.text(taksasiLine.getModifiedBy());
                                    xmlSerializer.endTag("", TaksasiLine.XML_MODIFIED_BY);

                                    xmlSerializer.endTag("", TaksasiLine.XML_DOCUMENT);
                                }
                            }
                            xmlSerializer.endTag("", TaksasiHeader.XML_DOCUMENT);
                        }

                        xmlSerializer.endTag("", TaksasiHeader.XML_FILE);
                        xmlSerializer.endDocument();

                        String xml = writer.toString();
                        //String fileName = "M2NTAKSASI" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
                        //modified by adit 20161226
                        String fileName = "M2NTAKSASI" + "_" + estate + "_" + division + "_" + date.replace("-", "") + "_" + "010101" + "_" + rowCount + ".xml";

                        msgStatus = saveXml(xml, fileName, "TAKSASI", isUsbOtgSave);

                        if (msgStatus != null && msgStatus.isStatus()) {
                            database.commitTransaction();
                        }

//						FolderHandler folderHandler = new FolderHandler(context);
//
//						if(folderHandler.init()){
//							String fileName = "M2NTAKSASI" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
//							String folder = folderHandler.getFileReportNew();
//
//							if(new FileXMLHandler(context).createFile(folder, fileName, xml)){
//								database.commitTransaction();
//
//								msgStatus = new MessageStatus("TAKSASI", true, context.getResources().getString(R.string.export_successed));
//							}else{
//								msgStatus = new MessageStatus("TAKSASI", false, context.getResources().getString(R.string.export_failed));
//							}
//						}else{
//							msgStatus = new MessageStatus("TAKSASI", false, context.getResources().getString(R.string.error_sd_card));
//						}
                    } else {
                        msgStatus = new MessageStatus("TAKSASI", false, context.getResources().getString(R.string.data_empty));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("TAKSASI", false, e.getMessage());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("TAKSASI", false, e.getMessage());
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("TAKSASI", false, e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("TAKSASI", false, e.getMessage());
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("TAKSASI", false, e.getMessage());
                } finally {
                    database.closeTransaction();
                }
                break;
            case R.id.cbxExportAncakPanen:
                try {
                    List<Object> listObjectAncakPanenHeader;

                    database.openTransaction();
                    if (status != null) {
                        listObjectAncakPanenHeader = database.getListData(false, AncakPanenHeader.TABLE_NAME, null,
                                AncakPanenHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        AncakPanenHeader.XML_ESTATE + "=?" + " and " +
                                        AncakPanenHeader.XML_DIVISION + "=?" + " and " +
                                        AncakPanenHeader.XML_NIK_FOREMAN + "=?" + " and " +
                                        AncakPanenHeader.XML_STATUS + "=?" + " and " +
                                        AncakPanenHeader.XML_ANCAK_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, status, date},
                                null, null, AncakPanenHeader.XML_ANCAK_DATE, null);
                    } else {
                        listObjectAncakPanenHeader = database.getListData(false, AncakPanenHeader.TABLE_NAME, null,
                                AncakPanenHeader.XML_COMPANY_CODE + "=?" + " and " +
                                        AncakPanenHeader.XML_ESTATE + "=?" + " and " +
                                        AncakPanenHeader.XML_DIVISION + "=?" + " and " +
                                        AncakPanenHeader.XML_NIK_FOREMAN + "=?" + " and " +
                                        AncakPanenHeader.XML_ANCAK_DATE + "=?",
                                new String[]{companyCode, estate, division, nik, date},
                                null, null, AncakPanenHeader.XML_ANCAK_DATE, null);
                    }

                    if (listObjectAncakPanenHeader.size() > 0) {
                        xmlSerializer.setOutput(writer);
                        xmlSerializer.startDocument("UTF-8", true);
                        xmlSerializer.startTag("", BPNHeader.XML_FILE);

                        rowCount = rowCount + listObjectAncakPanenHeader.size();

                        for (int i = 0; i < listObjectAncakPanenHeader.size(); i++) {
                            AncakPanenHeader ancakPanenHeader = (AncakPanenHeader) listObjectAncakPanenHeader.get(i);
                            ancakPanenHeader.setStatus(1);

                            String id = ancakPanenHeader.getAncakPanenId();
                            String imei = ancakPanenHeader.getImei();
                            String ancakDate = ancakPanenHeader.getAncakDate();
                            String gang = ancakPanenHeader.getGang();
                            String location = ancakPanenHeader.getLocation();
                            String tph = ancakPanenHeader.getTph();
                            String nikHarvester = ancakPanenHeader.getNikHarvester();

//							database.updateData(ancakPanenHeader,
//								AncakPanenHeader.XML_BPN_ID + "=?" + " and " +
//								AncakPanenHeader.XML_IMEI + "=?" + " and " +
//								AncakPanenHeader.XML_COMPANY_CODE + "=?" + " and " +
//								AncakPanenHeader.XML_ESTATE + "=?" + " and " +
//								AncakPanenHeader.XML_BPN_DATE + "=?" + " and " +
//								AncakPanenHeader.XML_DIVISION + "=?" + " and " +
//								AncakPanenHeader.XML_GANG + "=?" + " and " +
//								AncakPanenHeader.XML_LOCATION + "=?" + " and " +
//								AncakPanenHeader.XML_TPH + "=?" + " and " +
//								AncakPanenHeader.XML_NIK_HARVESTER + "=?",
//								new String [] {id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_DOCUMENT);
//							xmlSerializer.startTag("", AncakPanenHeader.XML_ANCAK_PANEN_ID);
//							xmlSerializer.text(ancakPanenHeader.getAncakPanenId());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_ANCAK_PANEN_ID);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_IMEI);
//							xmlSerializer.text(ancakPanenHeader.getImei());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_IMEI);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_COMPANY_CODE);
//							xmlSerializer.text(ancakPanenHeader.getCompanyCode());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_COMPANY_CODE);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_ESTATE);
//							xmlSerializer.text(ancakPanenHeader.getEstate());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_ESTATE);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_ANCAK_DATE);
//							xmlSerializer.text(ancakPanenHeader.getAncakDate());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_ANCAK_DATE);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_DIVISION);
//							xmlSerializer.text(ancakPanenHeader.getDivision());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_DIVISION);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_GANG);
//							xmlSerializer.text(ancakPanenHeader.getGang());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_GANG);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_LOCATION);
//							xmlSerializer.text(ancakPanenHeader.getLocation());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_LOCATION);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_TPH);
//							xmlSerializer.text(ancakPanenHeader.getTph());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_TPH);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_NIK_HARVESTER);
//							xmlSerializer.text(ancakPanenHeader.getNikHarvester());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_NIK_HARVESTER);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_HARVESTER);
//							xmlSerializer.text(ancakPanenHeader.getHarvester());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_HARVESTER);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_NIK_FOREMAN);
//							xmlSerializer.text(ancakPanenHeader.getNikForeman());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_NIK_FOREMAN);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_FOREMAN);
//							xmlSerializer.text(ancakPanenHeader.getForeman());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_FOREMAN);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_NIK_CLERK);
//							xmlSerializer.text(ancakPanenHeader.getNikClerk());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_NIK_CLERK);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_CLERK);
//							xmlSerializer.text(ancakPanenHeader.getClerk());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_CLERK);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_CROP);
//							xmlSerializer.text(ancakPanenHeader.getCrop());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_CROP);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_GPS_KOORDINAT);
//							xmlSerializer.text(ancakPanenHeader.getGpsKoordinat());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_GPS_KOORDINAT);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_CREATED_DATE);
//							if(ancakPanenHeader.getCreatedDate() > 0){
//								xmlSerializer.text(new DateLocal(new Date(ancakPanenHeader.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
//							}else{
//								xmlSerializer.text("");
//							}
//							xmlSerializer.endTag("", AncakPanenHeader.XML_CREATED_DATE);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_CREATED_BY);
//							xmlSerializer.text(ancakPanenHeader.getCreatedBy());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_CREATED_BY);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_MODIFIED_DATE);
//							if(ancakPanenHeader.getModifiedDate() > 0){
//								xmlSerializer.text(new DateLocal(new Date(ancakPanenHeader.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
//							}else{
//								xmlSerializer.text("");
//							}
//							xmlSerializer.endTag("", AncakPanenHeader.XML_MODIFIED_DATE);
//
//							xmlSerializer.startTag("", AncakPanenHeader.XML_MODIFIED_BY);
//							xmlSerializer.text(ancakPanenHeader.getModifiedBy());
//							xmlSerializer.endTag("", AncakPanenHeader.XML_MODIFIED_BY);

                            //quality
                            List<Object> listObjectQuality;

                            if (status != null) {
                                listObjectQuality = database.getListData(false, AncakPanenQuality.TABLE_NAME, null,
                                        AncakPanenQuality.XML_ANCAK_PANEN_ID + "=?" + " and " +
                                                AncakPanenQuality.XML_IMEI + "=?" + " and " +
                                                AncakPanenQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                AncakPanenQuality.XML_ESTATE + "=?" + " and " +
                                                AncakPanenQuality.XML_ANCAK_DATE + "=?" + " and " +
                                                AncakPanenQuality.XML_DIVISION + "=?" + " and " +
                                                AncakPanenQuality.XML_GANG + "=?" + " and " +
                                                AncakPanenQuality.XML_LOCATION + "=?" + " and " +
                                                AncakPanenQuality.XML_TPH + "=?" + " and " +
                                                AncakPanenQuality.XML_NIK_HARVESTER + "=?" + " and " +
                                                AncakPanenQuality.XML_STATUS + "=?",
                                        new String[]{id, imei, companyCode, estate, ancakDate, division, gang, location, tph, nikHarvester, status},
                                        null, null, AncakPanenQuality.XML_QUALITY_CODE, null);
                            } else {
                                listObjectQuality = database.getListData(false, AncakPanenQuality.TABLE_NAME, null,
                                        AncakPanenQuality.XML_ANCAK_PANEN_ID + "=?" + " and " +
                                                AncakPanenQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                AncakPanenQuality.XML_ESTATE + "=?" + " and " +
                                                AncakPanenQuality.XML_ANCAK_DATE + "=?" + " and " +
                                                AncakPanenQuality.XML_DIVISION + "=?" + " and " +
                                                AncakPanenQuality.XML_GANG + "=?" + " and " +
                                                AncakPanenQuality.XML_LOCATION + "=?" + " and " +
                                                AncakPanenQuality.XML_TPH + "=?" + " and " +
                                                AncakPanenQuality.XML_NIK_HARVESTER + "=?",
                                        new String[]{id, companyCode, estate, ancakDate, division, gang, location, tph, nikHarvester},
                                        null, null, BPNQuality.XML_QUALITY_CODE, null);
                            }

                            if (listObjectQuality.size() > 0) {

                                rowCount = rowCount + listObjectQuality.size();

                                for (int j = 0; j < listObjectQuality.size(); j++) {
                                    AncakPanenQuality ancakPanenQuality = (AncakPanenQuality) listObjectQuality.get(j);
                                    ancakPanenQuality.setStatus(1);

                                    String achievementCode = ancakPanenQuality.getAchievementCode();
                                    String qualityCode = ancakPanenQuality.getQualityCode();

//											database.updateData(ancakPanenQuality,
//													AncakPanenQuality.XML_ANCAK_PANEN_ID + "=?" + " and " +
//													AncakPanenQuality.XML_COMPANY_CODE + "=?" + " and " +
//													AncakPanenQuality.XML_ESTATE + "=?" + " and " +
//													AncakPanenQuality.XML_ANCAK_DATE + "=?" + " and " +
//													AncakPanenQuality.XML_DIVISION + "=?" + " and " +
//													AncakPanenQuality.XML_GANG + "=?" + " and " +
//													AncakPanenQuality.XML_LOCATION + "=?" + " and " +
//													AncakPanenQuality.XML_TPH + "=?" + " and " +
//													AncakPanenQuality.XML_NIK_HARVESTER + "=?" + " and " +
//													AncakPanenQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
//													AncakPanenQuality.XML_QUALITY_CODE + "=?",
//													new String [] {id, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester,
//													achievementCode, qualityCode});

                                    if (ancakPanenQuality.getQuantity() > 0) {

                                        xmlSerializer.startTag("", BPNQuality.XML_DOCUMENT);

                                        //xmlSerializer.startTag("", AncakPanenQuality.XML_ANCAK_PANEN_ID);
                                        xmlSerializer.startTag("", "ID");
                                        xmlSerializer.text(ancakPanenQuality.getAncakPanenId());
                                        //xmlSerializer.endTag("", AncakPanenQuality.XML_ANCAK_PANEN_ID);
                                        xmlSerializer.endTag("", "ID");

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_IMEI);
                                        xmlSerializer.text(ancakPanenQuality.getImei());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_IMEI);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_COMPANY_CODE);
                                        xmlSerializer.text(ancakPanenQuality.getCompanyCode());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_COMPANY_CODE);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_ESTATE);
                                        xmlSerializer.text(ancakPanenQuality.getEstate());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_ESTATE);

                                        //xmlSerializer.startTag("", AncakPanenQuality.XML_ANCAK_DATE);
                                        xmlSerializer.startTag("", "HARVEST_DATE");
                                        xmlSerializer.text(ancakPanenQuality.getAncakDate());
                                        //xmlSerializer.endTag("", AncakPanenQuality.XML_ANCAK_DATE);
                                        xmlSerializer.endTag("", "HARVEST_DATE");

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_DIVISION);
                                        xmlSerializer.text(ancakPanenQuality.getDivision());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_DIVISION);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_GANG);
                                        xmlSerializer.text(ancakPanenQuality.getGang());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_GANG);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_LOCATION);
                                        xmlSerializer.text(ancakPanenQuality.getLocation());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_LOCATION);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_TPH);
                                        xmlSerializer.text(ancakPanenQuality.getTph());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_TPH);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_NIK_HARVESTER);
                                        xmlSerializer.text(ancakPanenQuality.getNikHarvester());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_NIK_HARVESTER);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_CROP);
                                        xmlSerializer.text(ancakPanenQuality.getCrop());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_CROP);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_ACHIEVEMENT_CODE);
                                        xmlSerializer.text(ancakPanenQuality.getAchievementCode());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_ACHIEVEMENT_CODE);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_QUALITY_CODE);
                                        xmlSerializer.text(ancakPanenQuality.getQualityCode());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_QUALITY_CODE);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_QUANTITY);
                                        xmlSerializer.text(String.valueOf(ancakPanenQuality.getQuantity()));
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_QUANTITY);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_CREATED_DATE);
                                        if (ancakPanenQuality.getCreatedDate() > 0) {
                                            xmlSerializer.text(new DateLocal(new Date(ancakPanenQuality.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                        } else {
                                            xmlSerializer.text("");
                                        }
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_CREATED_DATE);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_CREATED_BY);
                                        xmlSerializer.text(ancakPanenQuality.getCreatedBy());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_CREATED_BY);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_MODIFIED_DATE);
                                        if (ancakPanenQuality.getModifiedDate() > 0) {
                                            xmlSerializer.text(new DateLocal(new Date(ancakPanenQuality.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
                                        } else {
                                            xmlSerializer.text("");
                                        }
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_MODIFIED_DATE);

                                        xmlSerializer.startTag("", AncakPanenQuality.XML_MODIFIED_BY);
                                        xmlSerializer.text(ancakPanenQuality.getModifiedBy());
                                        xmlSerializer.endTag("", AncakPanenQuality.XML_MODIFIED_BY);

                                        xmlSerializer.endTag("", BPNQuality.XML_DOCUMENT);
                                    }
                                }
                            }
//							xmlSerializer.endTag("", AncakPanenHeader.XML_DOCUMENT);
                        }

                        xmlSerializer.endTag("", BPNHeader.XML_FILE);
                        xmlSerializer.endDocument();

                        String xml = writer.toString();
                        //String fileName = "M2NANCAK" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
                        //modified by adit 20161226
                        String fileName = "M2NANCAK" + "_" + estate + "_" + division + "_" + date.replace("-", "") + "_" + "010101" + "_" + rowCount + ".xml";

                        msgStatus = saveXml(xml, fileName, "Ancak Panen", isUsbOtgSave);

                        if (msgStatus != null && msgStatus.isStatus()) {
                            database.commitTransaction();
                        }

//						if(isUsbOtgSave){
//							File[] files = context.getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
//
//							if(files.length > 1){
//								folder = new File(files[files.length-1], FolderHandler.REPORT + "/" + FolderHandler.NEW).getAbsolutePath();
//							}
//						}else{
//							FolderHandler folderHandler = new FolderHandler(context);
//
//							if(folderHandler.init()){
//								folder = folderHandler.getFileReportNew();
//							}
//						}
//
//						if(TextUtils.isEmpty(folder)){
//							if(new FileXMLHandler(context).createFile(folder, fileName, xml)){
//								database.commitTransaction();
//
//								msgStatus = new MessageStatus("BPN", true, context.getResources().getString(R.string.export_successed));
//							}else{
//								msgStatus = new MessageStatus("BPN", false, context.getResources().getString(R.string.export_failed));
//							}
//						}else{
//							msgStatus = new MessageStatus("BPN", false, context.getResources().getString(R.string.error_sd_card));
//						}
                    } else {
                        msgStatus = new MessageStatus("Ancak Panen", false, context.getResources().getString(R.string.data_empty));
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("Ancak Panen", false, e.getMessage());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("Ancak Panen", false, e.getMessage());
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("Ancak Panen", false, e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("Ancak Panen", false, e.getMessage());
                } catch (SQLiteException e) {
                    e.printStackTrace();
                    database.closeTransaction();

                    msgStatus = new MessageStatus("Ancak Panen", false, e.getMessage());
                } finally {
                    database.closeTransaction();
                }
                break;
//			case R.id.cbxExportSpta:
//				try{
//					List<Object> listObjectSpta;
//
//					database.openTransaction();
//					if(status != null){
//						listObjectSpta = database.getListData(false, SPTA.TABLE_NAME, null,
//										SPTA.XML_COMPANY_CODE + "=?" + " and " +
//										SPTA.XML_ESTATE + "=?" + " and " +
//										SPTA.XML_SPTA_DATE + "=?" + " and " +
//										SPTA.XML_STATUS + "=?" + " and " +
//										SPTA.XML_CREATED_BY + "=?",
//								new String [] {companyCode, estate, date, status, nik},
//								null, null, SPTA.XML_SPTA_DATE, null);
//					}else{
//						listObjectSpta = database.getListData(false, SPTA.TABLE_NAME, null,
//										SPTA.XML_COMPANY_CODE + "=?" + " and " +
//										SPTA.XML_ESTATE + "=?" + " and " +
//										SPTA.XML_SPTA_DATE + "=?" + " and " +
//										SPTA.XML_CREATED_BY+ "=?",
//								new String [] {companyCode, estate, date, nik},
//								null, null, SPTA.XML_SPTA_DATE, null);
//					}
//
//					if(listObjectSpta.size() > 0){
//						xmlSerializer.setOutput(writer);
//						xmlSerializer.startDocument("UTF-8", true);
//						xmlSerializer.startTag("", SPTA.XML_FILE);
//
//						rowCount = rowCount + listObjectSpta.size();
//
//						for(int i = 0; i < listObjectSpta.size(); i++){
//							SPTA spta = (SPTA) listObjectSpta.get(i);
//							spta.setStatus(1);
//
//							xmlSerializer.startTag("", SPTA.XML_DOCUMENT);
//
//								xmlSerializer.startTag("", SPTA.XML_ZYEAR);
//								xmlSerializer.text(spta.getZyear());
//								xmlSerializer.endTag("", SPTA.XML_ZYEAR);
//
//								xmlSerializer.startTag("", SPTA.XML_SPTA_NUM);
//								xmlSerializer.text(spta.getSptaNum());
//								xmlSerializer.endTag("", SPTA.XML_SPTA_NUM);
//
//								xmlSerializer.startTag("", SPTA.XML_COMPANY_CODE);
//								xmlSerializer.text(spta.getCompanyCode());
//								xmlSerializer.endTag("", SPTA.XML_COMPANY_CODE);
//
//								xmlSerializer.startTag("", SPTA.XML_ESTATE);
//								xmlSerializer.text(spta.getEstate());
//								xmlSerializer.endTag("", SPTA.XML_ESTATE);
//
//								xmlSerializer.startTag("", SPTA.XML_DIVISI);
//								xmlSerializer.text(spta.getDivisi());
//								xmlSerializer.endTag("", SPTA.XML_DIVISI);
//
//								xmlSerializer.startTag("", SPTA.XML_SPTA_DATE);
//								xmlSerializer.text(spta.getSptaDate());
//								xmlSerializer.endTag("", SPTA.XML_SPTA_DATE);
//
//								xmlSerializer.startTag("", SPTA.XML_SUB_DIV);
//								xmlSerializer.text(spta.getSubDiv());
//								xmlSerializer.endTag("", SPTA.XML_SUB_DIV);
//
//								xmlSerializer.startTag("", SPTA.XML_PETAK_ID);
//								xmlSerializer.text(spta.getPetakId());
//								xmlSerializer.endTag("", SPTA.XML_PETAK_ID);
//
//								xmlSerializer.startTag("", SPTA.XML_VENDOR_ID);
//								xmlSerializer.text(spta.getVendorId());
//								xmlSerializer.endTag("", SPTA.XML_VENDOR_ID);
//
//								xmlSerializer.startTag("", SPTA.XML_NOPOL);
//								xmlSerializer.text(spta.getNopol());
//								xmlSerializer.endTag("", SPTA.XML_NOPOL);
//
//								xmlSerializer.startTag("", SPTA.XML_LOGO);
//								xmlSerializer.text(spta.getLogo());
//								xmlSerializer.endTag("", SPTA.XML_LOGO);
//
//								xmlSerializer.startTag("", SPTA.XML_JARAK);
//								xmlSerializer.text(String.valueOf(spta.getJarak()));
//								xmlSerializer.endTag("", SPTA.XML_JARAK);
//
//								xmlSerializer.startTag("", SPTA.XML_RUN_ACC_1);
//								xmlSerializer.text(spta.getRunAcc1());
//								xmlSerializer.endTag("", SPTA.XML_RUN_ACC_1);
//
//								xmlSerializer.startTag("", SPTA.XML_EMPL_ID_1);
//								xmlSerializer.text(spta.getEmplId1());
//								xmlSerializer.endTag("", SPTA.XML_EMPL_ID_1);
//
//								xmlSerializer.startTag("", SPTA.XML_RUN_ACC_2);
//								xmlSerializer.text(spta.getRunAcc2());
//								xmlSerializer.endTag("", SPTA.XML_RUN_ACC_2);
//
//								xmlSerializer.startTag("", SPTA.XML_EMPL_ID_2);
//								xmlSerializer.text(spta.getEmplId2());
//								xmlSerializer.endTag("", SPTA.XML_EMPL_ID_2);
//
//								xmlSerializer.startTag("", SPTA.XML_BURN_DATE);
//								xmlSerializer.text(spta.getBurnDate());
//								xmlSerializer.endTag("", SPTA.XML_BURN_DATE);
//
//								xmlSerializer.startTag("", SPTA.XML_BURN_HOUR);
//								xmlSerializer.text(spta.getBurnHour());
//								xmlSerializer.endTag("", SPTA.XML_BURN_HOUR);
//
//								xmlSerializer.startTag("", SPTA.XML_CHOPPED_DATE);
//								xmlSerializer.text(spta.getChoppedDate());
//								xmlSerializer.endTag("", SPTA.XML_CHOPPED_DATE);
//
//								xmlSerializer.startTag("", SPTA.XML_CHOPPED_HOUR);
//								xmlSerializer.text(spta.getChoppedHour());
//								xmlSerializer.endTag("", SPTA.XML_CHOPPED_HOUR);
//
//								xmlSerializer.startTag("", SPTA.XML_LOAD_DATE);
//								xmlSerializer.text(spta.getLoadDate());
//								xmlSerializer.endTag("", SPTA.XML_LOAD_DATE);
//
//								xmlSerializer.startTag("", SPTA.XML_LOAD_HOUR);
//								xmlSerializer.text(spta.getLoadHour());
//								xmlSerializer.endTag("", SPTA.XML_LOAD_HOUR);
//
//								xmlSerializer.startTag("", SPTA.XML_QUALITY);
//								xmlSerializer.text(spta.getQuality());
//								xmlSerializer.endTag("", SPTA.XML_QUALITY);
//
//								xmlSerializer.startTag("", SPTA.XML_CANE_TYPE);
//								xmlSerializer.text(spta.getCaneType());
//								xmlSerializer.endTag("", SPTA.XML_CANE_TYPE);
//
//								xmlSerializer.startTag("", SPTA.XML_INSENTIVE_GULMA);
//								xmlSerializer.text(String.valueOf(spta.getInsentiveGulma()));
//								xmlSerializer.endTag("", SPTA.XML_INSENTIVE_GULMA);
//
//								xmlSerializer.startTag("", SPTA.XML_INSENTIVE_LANGSIR);
//								xmlSerializer.text(String.valueOf(spta.getInsentiveLangsir()));
//								xmlSerializer.endTag("", SPTA.XML_INSENTIVE_LANGSIR);
//
//								xmlSerializer.startTag("", SPTA.XML_INSENTIVE_ROBOH);
//								xmlSerializer.text(String.valueOf(spta.getInsentiveRoboh()));
//								xmlSerializer.endTag("", SPTA.XML_INSENTIVE_ROBOH);
//
//								xmlSerializer.startTag("", SPTA.XML_COST_TEBANG);
//								xmlSerializer.text(String.valueOf(spta.getCostTebang()));
//								xmlSerializer.endTag("", SPTA.XML_COST_TEBANG);
//
//								xmlSerializer.startTag("", SPTA.XML_COST_MUAT);
//								xmlSerializer.text(String.valueOf(spta.getCostMuat()));
//								xmlSerializer.endTag("", SPTA.XML_COST_MUAT);
//
//								xmlSerializer.startTag("", SPTA.XML_COST_ANGKUT);
//								xmlSerializer.text(String.valueOf(spta.getCostAngkut()));
//								xmlSerializer.endTag("", SPTA.XML_COST_ANGKUT);
//
//								xmlSerializer.startTag("", SPTA.XML_PENALTY_TRASH);
//								xmlSerializer.text(String.valueOf(spta.getPenaltyTrash()));
//								xmlSerializer.endTag("", SPTA.XML_PENALTY_TRASH);
//
//								xmlSerializer.startTag("", SPTA.XML_CREATED_DATE);
//								if(spta.getCreatedDate() > 0){
//									xmlSerializer.text(new DateLocal(new Date(spta.getCreatedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
//								}else{
//									xmlSerializer.text("");
//								}
//								xmlSerializer.endTag("", SPTA.XML_CREATED_DATE);
//
//								xmlSerializer.startTag("", SPTA.XML_CREATED_BY);
//								xmlSerializer.text(spta.getCreatedBy());
//								xmlSerializer.endTag("", SPTA.XML_CREATED_BY);
//
//								xmlSerializer.startTag("", SPTA.XML_MODIFIED_DATE);
//								if(spta.getModifiedDate() > 0){
//									xmlSerializer.text(new DateLocal(new Date(spta.getModifiedDate())).getDateString(DateLocal.FORMAT_DATE_TIME_INPUT));
//								}else{
//									xmlSerializer.text("");
//								}
//								xmlSerializer.endTag("", SPTA.XML_MODIFIED_DATE);
//
//								xmlSerializer.startTag("", SPTA.XML_MODIFIED_BY);
//								xmlSerializer.text(spta.getModifiedBy());
//								xmlSerializer.endTag("", SPTA.XML_MODIFIED_BY);
//
//							xmlSerializer.endTag("", SPTA.XML_DOCUMENT);
//						}
//
//						xmlSerializer.endTag("", SPTA.XML_FILE);
//						xmlSerializer.endDocument();
//
//						String xml = writer.toString();
//						String fileName = "M2NSPTA" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
//
//						msgStatus = saveXml(xml, fileName, "SPTA", isUsbOtgSave);
//
//						if(msgStatus != null && msgStatus.isStatus()){
//							database.commitTransaction();
//						}
//					}else{
//						msgStatus = new MessageStatus("SPTA", false, context.getResources().getString(R.string.data_empty));
//					}
//				}catch(NullPointerException e){
//					e.printStackTrace();
//					database.closeTransaction();
//
//					msgStatus = new MessageStatus("SPTA", false, e.getMessage());
//				}catch(IllegalArgumentException e){
//					e.printStackTrace();
//					database.closeTransaction();
//
//					msgStatus = new MessageStatus("SPTA", false, e.getMessage());
//				}catch(IllegalStateException e){
//					e.printStackTrace();
//					database.closeTransaction();
//
//					msgStatus = new MessageStatus("SPTA", false, e.getMessage());
//				}catch(IOException e){
//					e.printStackTrace();
//					database.closeTransaction();
//
//					msgStatus = new MessageStatus("SPTA", false, e.getMessage());
//				}catch(SQLiteException e){
//					e.printStackTrace();
//					database.closeTransaction();
//
//					msgStatus = new MessageStatus("SPTA", false, e.getMessage());
//				}finally{
//					database.closeTransaction();
//				}
//
//				break;
            default:
                break;
        }

        return msgStatus;
    }

    public MessageStatus importXML(InputStream inputStream) {
        String tableName = "";
        XmlPullParserFactory factory = null;
        XmlPullParser parser = null;
        boolean success = false;
        MessageStatus msgStatus = null;

        try {
            database.openTransaction();

            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();

            parser.setInput(inputStream, null);

            int eventType = parser.getEventType();
            String textValue = null;

            long todayDate = new Date().getTime();

            BKMHeader bkmHeader = null;
            BKMLine bkmLine = null;
            BKMOutput bkmOutput = null;

            BPNHeader bpnHeader = null;
            BPNQuantity bpnQuantity = null;
            BPNQuality bpnQuality = null;

            SPBSHeader spbsHeader = null;
            SPBSLine spbsLine = null;

            TaksasiHeader taksasiHeader = null;
            TaksasiLine taksasiLine = null;

            String fileType = "";

            int status = 1;
            int isSave = 1;
            long createdDate = todayDate;
            String createdBy = "";
            long modifiedDate = 0;
            String modifiedBy = "";

            msgStatus = new MessageStatus();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (TextUtils.isEmpty(fileType)) {
                            if (tagName.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)) {
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)) {
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)) {
                                fileType = SPBSHeader.XML_DOCUMENT_RESTORE;
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)) {
                                fileType = TaksasiHeader.XML_DOCUMENT_RESTORE;
                            }
                        }

                        if (fileType.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)) {
                                tableName = BKMHeader.TABLE_NAME;
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BKM");
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_DOCUMENT_RESTORE)) {
                                tableName = BKMLine.TABLE_NAME;
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BKM");
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_DOCUMENT_RESTORE)) {
                                tableName = BKMOutput.TABLE_NAME;
                                fileType = BKMHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BKM");
                            }

                            if (tableName.equalsIgnoreCase(BKMHeader.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(BKMHeader.XML_ITEM_RESTORE)) {
                                    bkmHeader = new BKMHeader();
                                }
                            } else if (tableName.equalsIgnoreCase(BKMLine.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(BKMLine.XML_ITEM_RESTORE)) {
                                    bkmLine = new BKMLine();
                                }
                            } else if (tableName.equalsIgnoreCase(BKMOutput.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(BKMOutput.XML_ITEM_RESTORE)) {
                                    bkmOutput = new BKMOutput();
                                }
                            }
                        } else if (fileType.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)) {
                                tableName = BPNHeader.TABLE_NAME;
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BPN");
                            } else if (tagName.equalsIgnoreCase(BPNQuantity.XML_DOCUMENT_RESTORE)) {
                                tableName = BPNQuantity.TABLE_NAME;
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BPN");
                            } else if (tagName.equalsIgnoreCase(BPNQuality.XML_DOCUMENT_RESTORE)) {
                                tableName = BPNQuality.TABLE_NAME;
                                fileType = BPNHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("BPN");
                            }

                            if (tableName.equalsIgnoreCase(BPNHeader.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(BPNHeader.XML_ITEM_RESTORE)) {
                                    bpnHeader = new BPNHeader();
                                }
                            } else if (tableName.equalsIgnoreCase(BPNQuantity.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(BPNQuantity.XML_ITEM_RESTORE)) {
                                    bpnQuantity = new BPNQuantity();
                                }
                            } else if (tableName.equalsIgnoreCase(BPNQuality.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(BPNQuality.XML_ITEM_RESTORE)) {
                                    bpnQuality = new BPNQuality();
                                }
                            }
                        } else if (fileType.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)) {
                                tableName = SPBSHeader.TABLE_NAME;
                                fileType = SPBSHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("SPBS");
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_DOCUMENT_RESTORE)) {
                                tableName = SPBSLine.TABLE_NAME;
                                fileType = SPBSHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("SPBS");
                            }

                            if (tableName.equalsIgnoreCase(SPBSHeader.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(SPBSHeader.XML_ITEM_RESTORE)) {
                                    spbsHeader = new SPBSHeader();
                                }
                            } else if (tableName.equalsIgnoreCase(SPBSLine.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(SPBSLine.XML_ITEM_RESTORE)) {
                                    spbsLine = new SPBSLine();
                                }
                            }
                        } else if (fileType.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)) {
                                tableName = TaksasiHeader.TABLE_NAME;
                                fileType = TaksasiHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("TAKSASI");
                            } else if (tagName.equalsIgnoreCase(TaksasiLine.XML_DOCUMENT_RESTORE)) {
                                tableName = TaksasiLine.TABLE_NAME;
                                fileType = TaksasiHeader.XML_DOCUMENT_RESTORE;
                                msgStatus.setMenu("TAKSASI");
                            }

                            if (tableName.equalsIgnoreCase(TaksasiHeader.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(TaksasiHeader.XML_ITEM_RESTORE)) {
                                    taksasiHeader = new TaksasiHeader();
                                }
                            } else if (tableName.equalsIgnoreCase(TaksasiLine.TABLE_NAME)) {
                                if (tagName.equalsIgnoreCase(TaksasiLine.XML_ITEM_RESTORE)) {
                                    taksasiLine = new TaksasiLine();
                                }
                            }
                        }
                        break;
                    case XmlPullParser.TEXT:
                        textValue = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (fileType.equalsIgnoreCase(BPNHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(BPNHeader.XML_ITEM_RESTORE)) {
                                tableName = BPNHeader.TABLE_NAME;
                            }
                        } else if (fileType.equalsIgnoreCase(SPBSHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(SPBSHeader.XML_ITEM_RESTORE)) {
                                tableName = SPBSHeader.TABLE_NAME;
                            }
                        } else if (fileType.equalsIgnoreCase(TaksasiHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(TaksasiHeader.XML_ITEM_RESTORE)) {
                                tableName = TaksasiHeader.TABLE_NAME;
                            }
                        } else if (fileType.equalsIgnoreCase(BKMHeader.XML_DOCUMENT_RESTORE)) {
                            if (tagName.equalsIgnoreCase(BKMHeader.XML_ITEM_RESTORE)) {
                                tableName = BKMHeader.TABLE_NAME;
                            }
                        }

                        if (tableName.equalsIgnoreCase(BKMHeader.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BKMHeader.XML_ITEM_RESTORE)) {
                                bkmHeader.setStatus(status);

                                BKMHeader bkm = (BKMHeader) database.getDataFirst(false, BKMHeader.TABLE_NAME, null,
                                        BKMHeader.XML_IMEI + "=?" + " and " +
                                                BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMHeader.XML_ESTATE + "=?" + " and " +
                                                BKMHeader.XML_BKM_DATE + "=?" + " and " +
                                                BKMHeader.XML_DIVISION + "=?" + " and " +
                                                BKMHeader.XML_GANG + "=?",
                                        new String[]{bkmHeader.getImei(), bkmHeader.getCompanyCode(), bkmHeader.getEstate(),
                                                bkmHeader.getBkmDate(), bkmHeader.getDivision(), bkmHeader.getGang()},
                                        null, null, null, null);

                                if (bkm != null) {
                                    database.updateData(bkmHeader,
                                            BKMHeader.XML_IMEI + "=?" + " and " +
                                                    BKMHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMHeader.XML_ESTATE + "=?" + " and " +
                                                    BKMHeader.XML_BKM_DATE + "=?" + " and " +
                                                    BKMHeader.XML_DIVISION + "=?" + " and " +
                                                    BKMHeader.XML_GANG + "=?",
                                            new String[]{bkmHeader.getImei(), bkmHeader.getCompanyCode(), bkmHeader.getEstate(),
                                                    bkmHeader.getBkmDate(), bkmHeader.getDivision(), bkmHeader.getGang()});
                                } else {
                                    database.setData(bkmHeader);
                                }
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_BKM_DATE_RESTORE)) {
                                bkmHeader.setBkmDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_IMEI_RESTORE)) {
                                bkmHeader.setImei(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_ESTATE_RESTORE)) {
                                bkmHeader.setEstate(textValue);
                                bkmHeader.setCompanyCode(textValue.substring(0, 2) + "00");
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_DIVISION_RESTORE)) {
                                bkmHeader.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_GANG_RESTORE)) {
                                bkmHeader.setGang(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_NIK_FOREMAN_RESTORE)) {
                                bkmHeader.setNikForeman(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_NIK_CLERK_RESTORE)) {
                                bkmHeader.setNikClerk(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_CREATED_DATE_RESTORE)) {
                                bkmHeader.setCreatedDate(todayDate);
                            } else if (tagName.equalsIgnoreCase(BKMHeader.XML_CREATED_BY_RESTORE)) {
                                bkmHeader.setCreatedBy(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(BKMLine.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BKMLine.XML_ITEM_RESTORE)) {
                                bkmLine.setImei(bkmHeader.getImei());
                                bkmLine.setStatus(status);

                                BKMLine bkm = (BKMLine) database.getDataFirst(false, BKMLine.TABLE_NAME, null,
                                        BKMLine.XML_IMEI + "=?" + " and " +
                                                BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMLine.XML_ESTATE + "=?" + " and " +
                                                BKMLine.XML_BKM_DATE + "=?" + " and " +
                                                BKMLine.XML_DIVISION + "=?" + " and " +
                                                BKMLine.XML_GANG + "=?" + " and " +
                                                BKMLine.XML_NIK + "=?",
                                        new String[]{bkmLine.getImei(), bkmLine.getCompanyCode(), bkmLine.getEstate(),
                                                bkmLine.getBkmDate(), bkmLine.getDivision(), bkmLine.getGang(), bkmLine.getNik()},
                                        null, null, null, null);

                                if (bkm != null) {
                                    database.updateData(bkm, BKMLine.XML_IMEI + "=?" + " and " +
                                                    BKMLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMLine.XML_ESTATE + "=?" + " and " +
                                                    BKMLine.XML_BKM_DATE + "=?" + " and " +
                                                    BKMLine.XML_DIVISION + "=?" + " and " +
                                                    BKMLine.XML_GANG + "=?" + " and " +
                                                    BKMLine.XML_NIK + "=?",
                                            new String[]{bkmLine.getImei(), bkmLine.getCompanyCode(), bkmLine.getEstate(),
                                                    bkmLine.getBkmDate(), bkmLine.getDivision(), bkmLine.getGang(), bkmLine.getNik()});
                                } else {
                                    database.setData(bkmLine);
                                }
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_BKM_DATE_RESTORE)) {
                                bkmLine.setBkmDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_ESTATE_RESTORE)) {
                                bkmLine.setEstate(textValue);
                                bkmLine.setCompanyCode(textValue.substring(0, 2) + "00");
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_DIVISION_RESTORE)) {
                                bkmLine.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_GANG_RESTORE)) {
                                bkmLine.setGang(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_NIK_RESTORE)) {
                                bkmLine.setNik(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_NAME_RESTORE)) {
                                bkmLine.setName(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_ABSENT_TYPE_RESTORE)) {
                                bkmLine.setAbsentType(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_MANDAYS_RESTORE)) {
                                bkmLine.setMandays(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_UOM_RESTORE)) {
                                bkmLine.setUom(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_CREATED_DATE_RESTORE)) {
                                bkmLine.setCreatedDate(todayDate);
                            } else if (tagName.equalsIgnoreCase(BKMLine.XML_CREATED_BY_RESTORE)) {
                                bkmLine.setCreatedBy(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(BKMOutput.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BKMOutput.XML_ITEM_RESTORE)) {
                                bkmOutput.setImei(bkmHeader.getImei());
                                bkmOutput.setStatus(status);
                                bkmOutput.setCreatedDate(bkmHeader.getCreatedDate());
                                bkmOutput.setCreatedBy(bkmHeader.getCreatedBy());

                                BKMOutput bkm = (BKMOutput) database.getDataFirst(false, BKMOutput.TABLE_NAME, null,
                                        BKMOutput.XML_IMEI + "=?" + " and " +
                                                BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                                BKMOutput.XML_ESTATE + "=?" + " and " +
                                                BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                                BKMOutput.XML_DIVISION + "=?" + " and " +
                                                BKMOutput.XML_GANG + "=?" + " and " +
                                                BKMOutput.XML_NIK + "=?" + " and " +
                                                BKMOutput.XML_BLOCK + "=?",
                                        new String[]{bkmOutput.getImei(), bkmOutput.getCompanyCode(), bkmOutput.getEstate(),
                                                bkmLine.getBkmDate(), bkmOutput.getDivision(), bkmOutput.getGang(), bkmOutput.getNik(), bkmOutput.getBlock()},
                                        null, null, null, null);

                                if (bkm != null) {
                                    database.updateData(bkmOutput,
                                            BKMOutput.XML_IMEI + "=?" + " and " +
                                                    BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
                                                    BKMOutput.XML_ESTATE + "=?" + " and " +
                                                    BKMOutput.XML_BKM_DATE + "=?" + " and " +
                                                    BKMOutput.XML_DIVISION + "=?" + " and " +
                                                    BKMOutput.XML_GANG + "=?" + " and " +
                                                    BKMOutput.XML_NIK + "=?" + " and " +
                                                    BKMOutput.XML_BLOCK + "=?",
                                            new String[]{bkmOutput.getImei(), bkmOutput.getCompanyCode(), bkmOutput.getEstate(),
                                                    bkmLine.getBkmDate(), bkmOutput.getDivision(), bkmOutput.getGang(), bkmOutput.getNik(), bkmOutput.getBlock()});
                                } else {
                                    database.setData(bkmOutput);
                                }
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_BKM_DATE_RESTORE)) {
                                bkmOutput.setBkmDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_ESTATE_RESTORE)) {
                                bkmOutput.setEstate(textValue);
                                bkmOutput.setCompanyCode(textValue.substring(0, 2) + "00");
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_DIVISION_RESTORE)) {
                                bkmOutput.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_GANG_RESTORE)) {
                                bkmOutput.setGang(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_BLOCK_RESTORE)) {
                                bkmOutput.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_NIK_RESTORE)) {
                                bkmOutput.setNik(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_NAME_RESTORE)) {
                                bkmOutput.setName(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_OUTPUT_RESTORE)) {
                                bkmOutput.setOutput(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_UOM_RESTORE)) {
                                bkmOutput.setUom(textValue);
                            } else if (tagName.equalsIgnoreCase(BKMOutput.XML_DOCUMENT_RESTORE)) {
                                tableName = BKMHeader.TABLE_NAME;
                            }
                        } else if (tableName.equalsIgnoreCase(BPNHeader.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BPNHeader.XML_ITEM_RESTORE)) {
                                bpnHeader.setStatus(status);

                                BPNHeader bpn = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
                                        BPNHeader.XML_BPN_ID + "=? " + " and " +
                                                BPNHeader.XML_IMEI + "=? " + " and " +
                                                BPNHeader.XML_COMPANY_CODE + "=? " + " and " +
                                                BPNHeader.XML_ESTATE + "=? " + " and " +
                                                BPNHeader.XML_BPN_DATE + "=? " + " and " +
                                                BPNHeader.XML_DIVISION + "=? " + " and " +
                                                BPNHeader.XML_GANG + "=? " + " and " +
                                                BPNHeader.XML_LOCATION + "=? " + " and " +
                                                BPNHeader.XML_TPH + "=? " + " and " +
                                                BPNHeader.XML_NIK_HARVESTER + "=?",
                                        new String[]{bpnHeader.getBpnId(), bpnHeader.getImei(), bpnHeader.getCompanyCode(), bpnHeader.getEstate(), bpnHeader.getBpnDate(),
                                                bpnHeader.getDivision(), bpnHeader.getGang(), bpnHeader.getLocation(), bpnHeader.getTph(), bpnHeader.getNikHarvester()},
                                        null, null, null, null);

                                if (bpn != null) {
                                    database.updateData(bpnHeader,
                                            BPNHeader.XML_BPN_ID + "=? " + " and " +
                                                    BPNHeader.XML_IMEI + "=? " + " and " +
                                                    BPNHeader.XML_COMPANY_CODE + "=? " + " and " +
                                                    BPNHeader.XML_ESTATE + "=? " + " and " +
                                                    BPNHeader.XML_BPN_DATE + "=? " + " and " +
                                                    BPNHeader.XML_DIVISION + "=? " + " and " +
                                                    BPNHeader.XML_GANG + "=? " + " and " +
                                                    BPNHeader.XML_LOCATION + "=? " + " and " +
                                                    BPNHeader.XML_TPH + "=? " + " and " +
                                                    BPNHeader.XML_NIK_HARVESTER + "=?",
                                            new String[]{bpnHeader.getBpnId(), bpnHeader.getImei(), bpnHeader.getCompanyCode(), bpnHeader.getEstate(), bpnHeader.getBpnDate(),
                                                    bpnHeader.getDivision(), bpnHeader.getGang(), bpnHeader.getLocation(), bpnHeader.getTph(), bpnHeader.getNikHarvester()});
                                } else {
                                    database.setData(bpnHeader);
                                }
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_ID_RESTORE)) {
                                bpnHeader.setBpnId(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_COMPANY_CODE_RESTORE)) {
                                bpnHeader.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_ESTATE_RESTORE)) {
                                bpnHeader.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_BPN_DATE_RESTORE)) {
                                bpnHeader.setBpnDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_DIVISION_RESTORE)) {
                                bpnHeader.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_GANG_RESTORE)) {
                                bpnHeader.setGang(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_FOREMAN_RESTORE)) {
                                bpnHeader.setForeman(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_NIK_FOREMAN_RESTORE)) {
                                bpnHeader.setNikForeman(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_CLERK_RESTORE)) {
                                bpnHeader.setClerk(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_NIK_CLERK_RESTORE)) {
                                bpnHeader.setNikClerk(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_HARVERSTER_RESTORE)) {
                                bpnHeader.setHarvester(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_NIK_HARVESTER_RESTORE)) {
                                bpnHeader.setNikHarvester(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_LOCATION_RESTORE)) {
                                bpnHeader.setLocation(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_TPH_RESTORE)) {
                                bpnHeader.setTph(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_CROP_RESTORE)) {
                                bpnHeader.setCrop(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_USE_GERDANG_RESTORE)) {
                                bpnHeader.setUseGerdang(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_GPS_KOORDINAT_RESTORE)) {
                                bpnHeader.setGpsKoordinat(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_IMEI_RESTORE)) {
                                bpnHeader.setImei(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_CREATED_DATE_RESTORE)) {
                                bpnHeader.setCreatedDate(todayDate);
                            } else if (tagName.equalsIgnoreCase(BPNHeader.XML_CREATED_BY_RESTORE)) {
                                bpnHeader.setCreatedBy(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(BPNQuantity.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BPNQuantity.XML_ITEM_RESTORE)) {
                                bpnQuantity.setBpnId(bpnHeader.getBpnId());
                                bpnQuantity.setImei(bpnHeader.getImei());
                                bpnQuantity.setCompanyCode(bpnHeader.getCompanyCode());
                                bpnQuantity.setEstate(bpnHeader.getEstate());
                                bpnQuantity.setBpnDate(bpnHeader.getBpnDate());
                                bpnQuantity.setDivision(bpnHeader.getDivision());
                                bpnQuantity.setGang(bpnHeader.getGang());
                                bpnQuantity.setLocation(bpnHeader.getLocation());
                                bpnQuantity.setTph(bpnHeader.getTph());
                                bpnQuantity.setNikHarvester(bpnHeader.getNikHarvester());
                                bpnQuantity.setCrop(bpnHeader.getCrop());
                                bpnQuantity.setStatus(status);
                                bpnQuantity.setCreatedDate(bpnHeader.getCreatedDate());
                                bpnQuantity.setCreatedBy(bpnHeader.getCreatedBy());

                                BPNQuantity bpn = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                                        BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                BPNQuantity.XML_IMEI + "=?" + " and " +
                                                BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                BPNQuantity.XML_GANG + "=?" + " and " +
                                                BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                BPNQuantity.XML_TPH + "=?" + " and " +
                                                BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                                                BPNQuantity.XML_CROP + "=?" + " and " +
                                                BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                        new String[]{bpnQuantity.getBpnId(), bpnQuantity.getImei(), bpnQuantity.getCompanyCode(), bpnQuantity.getEstate(),
                                                bpnQuantity.getBpnDate(), bpnQuantity.getDivision(), bpnQuantity.getGang(), bpnQuantity.getLocation(),
                                                bpnQuantity.getTph(), bpnQuantity.getNikHarvester(), bpnQuantity.getCrop(), bpnQuantity.getAchievementCode()},
                                        null, null, null, null);

                                if (bpn != null) {
                                    database.updateData(bpnQuantity,
                                            BPNQuantity.XML_BPN_ID + "=?" + " and " +
                                                    BPNQuantity.XML_IMEI + "=?" + " and " +
                                                    BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNQuantity.XML_ESTATE + "=?" + " and " +
                                                    BPNQuantity.XML_BPN_DATE + "=?" + " and " +
                                                    BPNQuantity.XML_DIVISION + "=?" + " and " +
                                                    BPNQuantity.XML_GANG + "=?" + " and " +
                                                    BPNQuantity.XML_LOCATION + "=?" + " and " +
                                                    BPNQuantity.XML_TPH + "=?" + " and " +
                                                    BPNQuantity.XML_NIK_HARVESTER + "=?" + " and " +
                                                    BPNQuantity.XML_CROP + "=?" + " and " +
                                                    BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
                                            new String[]{bpnQuantity.getBpnId(), bpnQuantity.getImei(), bpnQuantity.getCompanyCode(), bpnQuantity.getEstate(),
                                                    bpnQuantity.getBpnDate(), bpnQuantity.getDivision(), bpnQuantity.getGang(), bpnQuantity.getLocation(),
                                                    bpnQuantity.getTph(), bpnQuantity.getNikHarvester(), bpnQuantity.getCrop(), bpnQuantity.getAchievementCode()});
                                } else {
                                    database.setData(bpnQuantity);
                                }
                            } else if (tagName.equalsIgnoreCase(BPNQuantity.XML_ACHIEVEMENT_CODE_RESTORE)) {
                                bpnQuantity.setAchievementCode(textValue);
                            } else if (tagName.equalsIgnoreCase(bpnQuantity.XML_QUANTITY_RESTORE)) {
                                bpnQuantity.setQuantity(new Converter(textValue).StrToDouble());
                            }
                        } else if (tableName.equalsIgnoreCase(BPNQuality.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(BPNQuality.XML_ITEM_RESTORE)) {
                                bpnQuality.setBpnId(bpnHeader.getBpnId());
                                bpnQuality.setImei(bpnHeader.getImei());
                                bpnQuality.setCompanyCode(bpnHeader.getCompanyCode());
                                bpnQuality.setEstate(bpnHeader.getEstate());
                                bpnQuality.setBpnDate(bpnHeader.getBpnDate());
                                bpnQuality.setDivision(bpnHeader.getDivision());
                                bpnQuality.setGang(bpnHeader.getGang());
                                bpnQuality.setLocation(bpnHeader.getLocation());
                                bpnQuality.setTph(bpnHeader.getTph());
                                bpnQuality.setNikHarvester(bpnHeader.getNikHarvester());
                                bpnQuality.setCrop(bpnHeader.getCrop());
                                bpnQuality.setAchievementCode("01");
                                bpnQuality.setStatus(status);
                                bpnQuality.setCreatedDate(bpnHeader.getCreatedDate());
                                bpnQuality.setCreatedBy(bpnHeader.getCreatedBy());

                                BPNQuality bpn = (BPNQuality) database.getDataFirst(false, BPNQuality.TABLE_NAME, null,
                                        BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                BPNQuality.XML_IMEI + "=?" + " and " +
                                                BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                BPNQuality.XML_ESTATE + "=?" + " and " +
                                                BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                BPNQuality.XML_DIVISION + "=?" + " and " +
                                                BPNQuality.XML_GANG + "=?" + " and " +
                                                BPNQuality.XML_LOCATION + "=?" + " and " +
                                                BPNQuality.XML_TPH + "=?" + " and " +
                                                BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
                                                BPNQuality.XML_CROP + "=?" + " and " +
                                                BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                BPNQuality.XML_QUALITY_CODE + "=?",
                                        new String[]{bpnQuality.getBpnId(), bpnQuality.getImei(), bpnQuality.getCompanyCode(), bpnQuality.getEstate(),
                                                bpnQuality.getBpnDate(), bpnQuality.getDivision(), bpnQuality.getGang(), bpnQuality.getLocation(),
                                                bpnQuality.getTph(), bpnQuality.getNikHarvester(), bpnQuality.getCrop(), bpnQuality.getAchievementCode(),
                                                bpnQuality.getQualityCode()},
                                        null, null, null, null);

                                if (bpn != null) {
                                    database.updateData(bpnQuality,
                                            BPNQuality.XML_BPN_ID + "=?" + " and " +
                                                    BPNQuality.XML_IMEI + "=?" + " and " +
                                                    BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
                                                    BPNQuality.XML_ESTATE + "=?" + " and " +
                                                    BPNQuality.XML_BPN_DATE + "=?" + " and " +
                                                    BPNQuality.XML_DIVISION + "=?" + " and " +
                                                    BPNQuality.XML_GANG + "=?" + " and " +
                                                    BPNQuality.XML_LOCATION + "=?" + " and " +
                                                    BPNQuality.XML_TPH + "=?" + " and " +
                                                    BPNQuality.XML_NIK_HARVESTER + "=?" + " and " +
                                                    BPNQuality.XML_CROP + "=?" + " and " +
                                                    BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
                                                    BPNQuality.XML_QUALITY_CODE + "=?",
                                            new String[]{bpnQuality.getBpnId(), bpnQuality.getImei(), bpnQuality.getCompanyCode(), bpnQuality.getEstate(),
                                                    bpnQuality.getBpnDate(), bpnQuality.getDivision(), bpnQuality.getGang(), bpnQuality.getLocation(),
                                                    bpnQuality.getTph(), bpnQuality.getNikHarvester(), bpnQuality.getCrop(), bpnQuality.getAchievementCode(),
                                                    bpnQuality.getQualityCode()});
                                } else {
                                    database.setData(bpnQuality);
                                }
                            } else if (tagName.equalsIgnoreCase(BPNQuality.XML_QUALITY_CODE_RESTORE)) {
                                bpnQuality.setQualityCode(textValue);
                            } else if (tagName.equalsIgnoreCase(BPNQuality.XML_QUANTITY_RESTORE)) {
                                bpnQuality.setQuantity(new Converter(textValue).StrToDouble());
                            }
                        } else if (tableName.equalsIgnoreCase(SPBSHeader.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPBSHeader.XML_ITEM_RESTORE)) {
                                spbsHeader.setStatus(status);
                                spbsHeader.setIsSave(isSave);

                                SPBSHeader spbs = (SPBSHeader) database.getDataFirst(false, SPBSHeader.TABLE_NAME, null,
                                        SPBSHeader.XML_IMEI + "=?" + " and " +
                                                SPBSHeader.XML_YEAR + "=?" + " and " +
                                                SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                SPBSHeader.XML_CROP + "=?" + " and " +
                                                SPBSHeader.XML_SPBS_NUMBER + "=?",
                                        new String[]{spbsHeader.getImei(), spbsHeader.getYear(), spbsHeader.getCompanyCode(), spbsHeader.getEstate(),
                                                spbsHeader.getCrop(), spbsHeader.getSpbsNumber()},
                                        null, null, null, null);

                                if (spbs != null) {
                                    database.updateData(spbsHeader,
                                            SPBSHeader.XML_IMEI + "=?" + " and " +
                                                    SPBSHeader.XML_YEAR + "=?" + " and " +
                                                    SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSHeader.XML_ESTATE + "=?" + " and " +
                                                    SPBSHeader.XML_CROP + "=?" + " and " +
                                                    SPBSHeader.XML_SPBS_NUMBER + "=?",
                                            new String[]{spbsHeader.getImei(), spbsHeader.getYear(), spbsHeader.getCompanyCode(), spbsHeader.getEstate(),
                                                    spbsHeader.getCrop(), spbsHeader.getSpbsNumber()});
                                } else {
                                    database.setData(spbsHeader);
                                }
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_IMEI_RESTORE)) {
                                spbsHeader.setImei(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_YEAR_RESTORE)) {
                                spbsHeader.setYear(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_COMPANY_CODE_RESTORE)) {
                                spbsHeader.setCompanyCode(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_ESTATE_RESTORE)) {
                                spbsHeader.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_CROP_RESTORE)) {
                                spbsHeader.setCrop(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_SPBS_NUMBER_RESTORE)) {
                                spbsHeader.setSpbsNumber(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_SPBS_DATE_RESTORE)) {
                                spbsHeader.setSpbsDate(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_DIVISION_RESTORE)) {
                                spbsHeader.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_NIK_ASSISTANT_RESTORE)) {
                                spbsHeader.setNikAssistant(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_NIK_CLERK_RESTORE)) {
                                spbsHeader.setNikClerk(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_NIK_DRIVER_RESTORE)) {
                                spbsHeader.setNikDriver(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_DRIVER_RESTORE)) {
                                spbsHeader.setDriver(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_RUNNING_ACCOUNT_RESTORE)) {
                                spbsHeader.setRunningAccount(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_LICENSE_PLATE_RESTORE)) {
                                spbsHeader.setLicensePlate(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_CREATED_DATE_RESTORE)) {
                                spbsHeader.setCreatedDate(todayDate);
                            } else if (tagName.equalsIgnoreCase(SPBSHeader.XML_CREATED_BY_RESTORE)) {
                                spbsHeader.setCreatedBy(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(SPBSLine.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(SPBSLine.XML_ITEM_RESTORE)) {
                                spbsLine.setImei(spbsHeader.getImei());
                                spbsLine.setYear(spbsHeader.getYear());
                                spbsLine.setCompanyCode(spbsHeader.getCompanyCode());
                                spbsLine.setEstate(spbsHeader.getEstate());
                                spbsLine.setCrop(spbsHeader.getCrop());
                                spbsLine.setSpbsNumber(spbsHeader.getSpbsNumber());
                                spbsLine.setSpbsDate(spbsHeader.getSpbsDate());
                                spbsLine.setStatus(status);
                                spbsLine.setIsSave(isSave);
                                spbsLine.setCreatedDate(spbsHeader.getCreatedDate());
                                spbsLine.setCreatedBy(spbsHeader.getCreatedBy());

                                SPBSLine spbs = (SPBSLine) database.getDataFirst(false, SPBSLine.TABLE_NAME, null,
                                        SPBSLine.XML_IMEI + "=?" + " and " +
                                                SPBSLine.XML_YEAR + "=?" + " and " +
                                                SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                SPBSLine.XML_ESTATE + "=?" + " and " +
                                                SPBSLine.XML_CROP + "=?" + " and " +
                                                SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                SPBSLine.XML_BLOCK + "=?" + " and " +
                                                SPBSLine.XML_TPH + "=?" + " and " +
                                                SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                        new String[]{spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(), spbsLine.getEstate(),
                                                spbsLine.getCrop(), spbsLine.getSpbsNumber(), spbsLine.getSpbsDate(), spbsLine.getBlock(), spbsLine.getTph(),
                                                spbsLine.getBpnDate(), spbsLine.getAchievementCode()},
                                        null, null, null, null);

                                if (spbs != null) {
                                    database.updateData(spbsLine,
                                            SPBSLine.XML_IMEI + "=?" + " and " +
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?" + " and " +
                                                    SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
                                            new String[]{spbsLine.getImei(), spbsLine.getYear(), spbsLine.getCompanyCode(), spbsLine.getEstate(),
                                                    spbsLine.getCrop(), spbsLine.getSpbsNumber(), spbsLine.getSpbsDate(), spbsLine.getBlock(), spbsLine.getTph(),
                                                    spbsLine.getBpnDate(), spbsLine.getAchievementCode()});
                                } else {
                                    database.setData(spbsLine);
                                }
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_BLOCK_RESTORE)) {
                                spbsLine.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_TPH_RESTORE)) {
                                spbsLine.setTph(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_GPS_KOORDINAT_RESTORE)) {
                                spbsLine.setGpsKoordinat(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_ACHIEVEMENT_CODE_RESTORE)) {
                                spbsLine.setAchievementCode(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_BPN_DATE_RESTORE)) {
                                spbsLine.setBpnDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_TOTAL_HARVESTER_RESTORE)) {
                                spbsLine.setTotalHarvester(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_QUANTITY_RESTORE)) {
                                spbsLine.setQuantity(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_UOM_RESTORE)) {
                                spbsLine.setUom(textValue);
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_CREATED_DATE_RESTORE)) {
                                spbsLine.setCreatedDate(todayDate);
                            } else if (tagName.equalsIgnoreCase(SPBSLine.XML_CREATED_BY_RESTORE)) {
                                spbsLine.setCreatedBy(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(TaksasiHeader.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(TaksasiHeader.XML_ITEM_RESTORE)) {
                                taksasiHeader.setStatus(status);
                                taksasiHeader.setIsSave(isSave);

                                TaksasiHeader taksasi = (TaksasiHeader) database.getDataFirst(false, TaksasiHeader.TABLE_NAME, null,
                                        TaksasiHeader.XML_IMEI + "=?" + " and " +
                                                TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                                TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                                TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
                                                TaksasiHeader.XML_BLOCK + "=?" + " and " +
                                                TaksasiHeader.XML_CROP + "=?",
                                        new String[]{taksasiHeader.getImei(), taksasiHeader.getCompanyCode(), taksasiHeader.getEstate(), taksasiHeader.getDivision(),
                                                taksasiHeader.getTaksasiDate(), taksasiHeader.getBlock(), taksasiHeader.getCrop()},
                                        null, null, null, null);

                                if (taksasi != null) {
                                    database.updateData(taksasiHeader,
                                            TaksasiHeader.XML_IMEI + "=?" + " and " +
                                                    TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
                                                    TaksasiHeader.XML_ESTATE + "=?" + " and " +
                                                    TaksasiHeader.XML_DIVISION + "=?" + " and " +
                                                    TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
                                                    TaksasiHeader.XML_BLOCK + "=?" + " and " +
                                                    TaksasiHeader.XML_CROP + "=?",
                                            new String[]{taksasiHeader.getImei(), taksasiHeader.getCompanyCode(), taksasiHeader.getEstate(), taksasiHeader.getDivision(),
                                                    taksasiHeader.getTaksasiDate(), taksasiHeader.getBlock(), taksasiHeader.getCrop()});
                                } else {
                                    database.setData(taksasiHeader);
                                }
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_IMEI_RESTORE)) {
                                taksasiHeader.setImei(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_ESTATE_RESTORE)) {
                                taksasiHeader.setCompanyCode(textValue.substring(0, 2) + "00");
                                taksasiHeader.setEstate(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_DIVISION_RESTORE)) {
                                taksasiHeader.setDivision(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_TAKSASI_DATE_RESTORE)) {
                                taksasiHeader.setTaksasiDate(new DateLocal(textValue, DateLocal.FORMAT_RESTORE).getDateString(DateLocal.FORMAT_INPUT));
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_BLOCK_RESTORE)) {
                                taksasiHeader.setBlock(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_CROP_RESTORE)) {
                                taksasiHeader.setCrop(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_NIK_FOREMAN_RESTORE)) {
                                taksasiHeader.setNikForeman(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_FOREMAN_RESTORE)) {
                                taksasiHeader.setForeman(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_BJR_RESTORE)) {
                                taksasiHeader.setBjr(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_PROD_TREES_RESTORE)) {
                                taksasiHeader.setProdTrees(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_GPS_KOORDINAT_RESTORE)) {
                                taksasiHeader.setGpsKoordinat(textValue);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_CREATED_DATE_RESTORE)) {
                                taksasiHeader.setCreatedDate(todayDate);
                            } else if (tagName.equalsIgnoreCase(TaksasiHeader.XML_CREATED_BY_RESTORE)) {
                                taksasiHeader.setCreatedBy(textValue);
                            }
                        } else if (tableName.equalsIgnoreCase(TaksasiLine.TABLE_NAME)) {
                            if (tagName.equalsIgnoreCase(TaksasiLine.XML_ITEM_RESTORE)) {
                                taksasiLine.setImei(taksasiHeader.getImei());
                                taksasiLine.setCompanyCode(taksasiHeader.getEstate().substring(0, 2) + "00");
                                taksasiLine.setEstate(taksasiHeader.getEstate());
                                taksasiLine.setDivision(taksasiHeader.getDivision());
                                taksasiLine.setTaksasiDate(taksasiHeader.getTaksasiDate());
                                taksasiLine.setBlock(taksasiHeader.getBlock());
                                taksasiLine.setCrop(taksasiHeader.getCrop());
                                taksasiLine.setStatus(status);
                                taksasiLine.setIsSave(isSave);
                                taksasiLine.setCreatedDate(taksasiHeader.getCreatedDate());
                                taksasiLine.setCreatedBy(taksasiHeader.getCreatedBy());

                                TaksasiLine taksasi = (TaksasiLine) database.getDataFirst(false, TaksasiLine.TABLE_NAME, null,
                                        TaksasiLine.XML_IMEI + "=?" + " and " +
                                                TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_CROP + "=?" + " and " +
                                                TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                                TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                                TaksasiLine.XML_LINE_SKB + "=?",
                                        new String[]{taksasiLine.getImei(), taksasiLine.getCompanyCode(), taksasiLine.getEstate(), taksasiLine.getDivision(),
                                                taksasiLine.getTaksasiDate(), taksasiLine.getBlock(), taksasiLine.getCrop(), String.valueOf(taksasiLine.getBarisSkb()), String.valueOf(taksasiLine.getBarisBlock()),
                                                String.valueOf(taksasiLine.getLineSkb())},
                                        null, null, null, null);

                                if (taksasi != null) {
                                    database.updateData(taksasiLine,
                                            TaksasiLine.XML_IMEI + "=?" + " and " +
                                                    TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    TaksasiLine.XML_ESTATE + "=?" + " and " +
                                                    TaksasiLine.XML_DIVISION + "=?" + " and " +
                                                    TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                                    TaksasiLine.XML_BLOCK + "=?" + " and " +
                                                    TaksasiLine.XML_CROP + "=?" + " and " +
                                                    TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                                    TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                                    TaksasiLine.XML_LINE_SKB + "=?",
                                            new String[]{taksasiLine.getImei(), taksasiLine.getCompanyCode(), taksasiLine.getEstate(), taksasiLine.getDivision(),
                                                    taksasiLine.getTaksasiDate(), taksasiLine.getBlock(), taksasiLine.getCrop(), String.valueOf(taksasiLine.getBarisSkb()), String.valueOf(taksasiLine.getBarisBlock()),
                                                    String.valueOf(taksasiLine.getLineSkb())});
                                } else {
                                    database.setData(taksasiLine);
                                }
                            } else if (tagName.equalsIgnoreCase(TaksasiLine.XML_BARIS_BLOCK_RESTORE)) {
                                taksasiLine.setBarisBlock(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(TaksasiLine.XML_BARIS_SKB_RESTORE)) {
                                taksasiLine.setBarisSkb(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(TaksasiLine.XML_LINE_SKB_RESTORE)) {
                                taksasiLine.setLineSkb(new Converter(textValue).StrToInt());
                            } else if (tagName.equalsIgnoreCase(TaksasiLine.XML_QTY_POKOK_RESTORE)) {
                                taksasiLine.setQtyPokok(new Converter(textValue).StrToDouble());
                            } else if (tagName.equalsIgnoreCase(TaksasiLine.XML_QTY_JANJANG_RESTORE)) {
                                taksasiLine.setQtyJanjang(new Converter(textValue).StrToInt());
                            }
                        }
                    default:
                        break;
                }
                eventType = parser.next();
            }

            database.commitTransaction();
            success = true;
            msgStatus.setMessage(context.getResources().getString(R.string.import_successed));
            msgStatus.setStatus(success);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus.setMessage(e.getMessage());
            msgStatus.setStatus(success);
        } catch (IOException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus.setMessage(e.getMessage());
            msgStatus.setStatus(success);
        } catch (SQLiteException e) {
            e.printStackTrace();
            database.closeTransaction();
            msgStatus.setMessage(e.getMessage());
            msgStatus.setStatus(success);
        } finally {
            database.closeTransaction();
        }

        return msgStatus;
    }

    private MessageStatus saveXml(String xml, String fileName, String type, boolean isUsbOtgSave) {
//		String xml = writer.toString();
        String folder = null;
//		String fileName = "M2NBPN" + "_" + estate + "_" + division + "_" + new DateLocal(new Date()).getDateString(DateLocal.FORMAT_FILE) + "_" + rowCount + ".xml";
        MessageStatus msgStatus;

        if (isUsbOtgSave) {
//			UsbDevice usbDevice;

            UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            HashMap<String, UsbDevice> mapUsb = manager.getDeviceList();

            String[] usbKey = mapUsb.keySet().toArray(new String[mapUsb.keySet().size()]);

            if (usbKey.length > 0) {
//				folder = new File("/storage/UsbDriveA", "Android/data/com.simp.hms/files/Documents/" + FolderHandler.REPORT + "/" + FolderHandler.NEW).getAbsolutePath();
                folder = new File("/storage/UsbDriveA", FolderHandler.ROOT + "/" + FolderHandler.REPORT + "/" + FolderHandler.NEW).getAbsolutePath();

                File folderFile = new File(folder);

                if (!folderFile.exists()) {
                    folderFile.mkdirs();
                }

            }
//
//			File[] files = context.getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
//
//			if(files.length > 1){
//				folder = new File(files[files.length-1], FolderHandler.REPORT + "/" + FolderHandler.NEW).getAbsolutePath();
//
//				File folderFile = new File(folder);
//
//				if(!folderFile.exists()){
//					folderFile.mkdirs();
//				}
//			}

        } else {
            FolderHandler folderHandler = new FolderHandler(context);

            if (folderHandler.init()) {
                folder = folderHandler.getFileReportNew();
            }
        }

        if (!TextUtils.isEmpty(folder)) {
            if (new FileXMLHandler(context).createFile(folder, fileName, xml)) {
                msgStatus = new MessageStatus(type, true, context.getResources().getString(R.string.export_successed));
            } else {
                msgStatus = new MessageStatus(type, false, context.getResources().getString(R.string.export_failed));
            }
        } else {
            msgStatus = new MessageStatus(type, false, context.getResources().getString(R.string.error_sd_card));
        }

        return msgStatus;
    }

    public String createSPBSHeader(SPBSHeader spbsHeader) {
        String retval = null;
        XmlSerializer xmlSerializer = null;
        StringWriter writer = null;
        try {

            xmlSerializer = Xml.newSerializer();
            writer = new StringWriter();

            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", SPBSHeader.XML_FILE);

            xmlSerializer.startTag("", SPBSHeader.XML_DOCUMENT);

            xmlSerializer.startTag("", SPBSHeader.XML_IMEI);
            xmlSerializer.text(spbsHeader.getImei());
            xmlSerializer.endTag("", SPBSHeader.XML_IMEI);

            xmlSerializer.startTag("", SPBSHeader.XML_YEAR);
            xmlSerializer.text(spbsHeader.getYear());
            xmlSerializer.endTag("", SPBSHeader.XML_YEAR);

            xmlSerializer.startTag("", SPBSHeader.XML_COMPANY_CODE);
            xmlSerializer.text(spbsHeader.getCompanyCode());
            xmlSerializer.endTag("", SPBSHeader.XML_COMPANY_CODE);

            xmlSerializer.startTag("", SPBSHeader.XML_ESTATE);
            xmlSerializer.text(spbsHeader.getEstate());
            xmlSerializer.endTag("", SPBSHeader.XML_ESTATE);

            xmlSerializer.startTag("", SPBSHeader.XML_CROP);
            xmlSerializer.text(spbsHeader.getCrop());
            xmlSerializer.endTag("", SPBSHeader.XML_CROP);

            xmlSerializer.startTag("", SPBSHeader.XML_SPBS_NUMBER);
            xmlSerializer.text(spbsHeader.getSpbsNumber());
            xmlSerializer.endTag("", SPBSHeader.XML_SPBS_NUMBER);

            xmlSerializer.startTag("", SPBSHeader.XML_SPBS_DATE);
            xmlSerializer.text(spbsHeader.getSpbsDate());
            xmlSerializer.endTag("", SPBSHeader.XML_SPBS_DATE);

            xmlSerializer.startTag("", SPBSHeader.XML_DEST_ID);
            xmlSerializer.text(spbsHeader.getDestId());
            xmlSerializer.endTag("", SPBSHeader.XML_DEST_ID);

            xmlSerializer.startTag("", SPBSHeader.XML_DEST_DESC);
            xmlSerializer.text(spbsHeader.getDestDesc());
            xmlSerializer.endTag("", SPBSHeader.XML_DEST_DESC);

            xmlSerializer.startTag("", SPBSHeader.XML_DEST_TYPE);
            xmlSerializer.text(spbsHeader.getDestType());
            xmlSerializer.endTag("", SPBSHeader.XML_DEST_TYPE);

            xmlSerializer.startTag("", SPBSHeader.XML_DIVISION);
            xmlSerializer.text(spbsHeader.getDivision());
            xmlSerializer.endTag("", SPBSHeader.XML_DIVISION);

            xmlSerializer.startTag("", SPBSHeader.XML_NIK_ASSISTANT);
            xmlSerializer.text(spbsHeader.getNikAssistant());
            xmlSerializer.endTag("", SPBSHeader.XML_NIK_ASSISTANT);

            xmlSerializer.startTag("", SPBSHeader.XML_ASSISTANT);
            xmlSerializer.text(spbsHeader.getAssistant());
            xmlSerializer.endTag("", SPBSHeader.XML_ASSISTANT);

            xmlSerializer.startTag("", SPBSHeader.XML_NIK_CLERK);
            xmlSerializer.text(spbsHeader.getNikClerk());
            xmlSerializer.endTag("", SPBSHeader.XML_NIK_CLERK);

            xmlSerializer.startTag("", SPBSHeader.XML_CLERK);
            xmlSerializer.text(spbsHeader.getClerk());
            xmlSerializer.endTag("", SPBSHeader.XML_CLERK);

            xmlSerializer.startTag("", SPBSHeader.XML_NIK_DRIVER);
            xmlSerializer.text(spbsHeader.getNikDriver());
            xmlSerializer.endTag("", SPBSHeader.XML_NIK_DRIVER);

            xmlSerializer.startTag("", SPBSHeader.XML_DRIVER);
            xmlSerializer.text(spbsHeader.getDriver());
            xmlSerializer.endTag("", SPBSHeader.XML_DRIVER);

            xmlSerializer.startTag("", SPBSHeader.XML_NIK_KERNET);
            xmlSerializer.text(spbsHeader.getNikKernet());
            xmlSerializer.endTag("", SPBSHeader.XML_NIK_KERNET);

            xmlSerializer.startTag("", SPBSHeader.XML_KERNET);
            xmlSerializer.text(spbsHeader.getKernet());
            xmlSerializer.endTag("", SPBSHeader.XML_KERNET);

            xmlSerializer.startTag("", SPBSHeader.XML_LICENSE_PLATE);
            xmlSerializer.text(spbsHeader.getLicensePlate());
            xmlSerializer.endTag("", SPBSHeader.XML_LICENSE_PLATE);

            xmlSerializer.startTag("", SPBSHeader.XML_RUNNING_ACCOUNT);
            xmlSerializer.text(spbsHeader.getRunningAccount());
            xmlSerializer.endTag("", SPBSHeader.XML_RUNNING_ACCOUNT);

            xmlSerializer.startTag("", SPBSHeader.XML_GPS_KOORDINAT);
            xmlSerializer.text(spbsHeader.getGpsKoordinat());
            xmlSerializer.endTag("", SPBSHeader.XML_GPS_KOORDINAT);

            xmlSerializer.startTag("", SPBSHeader.XML_CREATED_DATE);
            xmlSerializer.text(String.valueOf(spbsHeader.getCreatedDate()));
//            xmlSerializer.text("");
            xmlSerializer.endTag("", SPBSHeader.XML_CREATED_DATE);

            xmlSerializer.startTag("", SPBSHeader.XML_CREATED_BY);
            xmlSerializer.text(spbsHeader.getCreatedBy());
            xmlSerializer.endTag("", SPBSHeader.XML_CREATED_BY);

            xmlSerializer.startTag("", SPBSHeader.XML_MODIFIED_DATE);
            xmlSerializer.text(String.valueOf(spbsHeader.getModifiedDate()));
//            xmlSerializer.text("");
            xmlSerializer.endTag("", SPBSHeader.XML_MODIFIED_DATE);

            xmlSerializer.startTag("", SPBSHeader.XML_MODIFIED_BY);
            xmlSerializer.text(spbsHeader.getModifiedBy());
            xmlSerializer.endTag("", SPBSHeader.XML_MODIFIED_BY);

            xmlSerializer.startTag("", SPBSHeader.XML_LIFNR);
            xmlSerializer.text(spbsHeader.getLifnr());
            xmlSerializer.endTag("", SPBSHeader.XML_LIFNR);

            for (SPBSLine spbsLine : spbsHeader.getSpbsLines()) {
                xmlSerializer.startTag("", SPBSLine.XML_DOCUMENT);

                xmlSerializer.startTag("", SPBSLine.XML_ID);
                xmlSerializer.text(String.valueOf(spbsLine.getId()));
                xmlSerializer.endTag("", SPBSLine.XML_ID);

                xmlSerializer.startTag("", SPBSLine.XML_IMEI);
                xmlSerializer.text(spbsLine.getImei());
                xmlSerializer.endTag("", SPBSLine.XML_IMEI);

                xmlSerializer.startTag("", SPBSLine.XML_YEAR);
                xmlSerializer.text(spbsLine.getYear());
                xmlSerializer.endTag("", SPBSLine.XML_YEAR);

                xmlSerializer.startTag("", SPBSLine.XML_COMPANY_CODE);
                xmlSerializer.text(spbsLine.getCompanyCode());
                xmlSerializer.endTag("", SPBSLine.XML_COMPANY_CODE);

                xmlSerializer.startTag("", SPBSLine.XML_ESTATE);
                xmlSerializer.text(spbsLine.getEstate());
                xmlSerializer.endTag("", SPBSLine.XML_ESTATE);

                xmlSerializer.startTag("", SPBSLine.XML_CROP);
                xmlSerializer.text(spbsLine.getCrop());
                xmlSerializer.endTag("", SPBSLine.XML_CROP);

                xmlSerializer.startTag("", SPBSLine.XML_SPBS_NUMBER);
                xmlSerializer.text(spbsLine.getSpbsNumber());
                xmlSerializer.endTag("", SPBSLine.XML_SPBS_NUMBER);

                xmlSerializer.startTag("", SPBSLine.XML_SPBS_DATE);
                xmlSerializer.text(spbsLine.getSpbsDate());
                xmlSerializer.endTag("", SPBSLine.XML_SPBS_DATE);

                xmlSerializer.startTag("", SPBSLine.XML_BLOCK);
                xmlSerializer.text(spbsLine.getBlock());
                xmlSerializer.endTag("", SPBSLine.XML_BLOCK);

                xmlSerializer.startTag("", SPBSLine.XML_TPH);
                xmlSerializer.text(spbsLine.getTph());
                xmlSerializer.endTag("", SPBSLine.XML_TPH);

                xmlSerializer.startTag("", SPBSLine.XML_BPN_DATE);
                xmlSerializer.text(spbsLine.getBpnDate());
                xmlSerializer.endTag("", SPBSLine.XML_BPN_DATE);

                xmlSerializer.startTag("", SPBSLine.XML_BPN_ID);
                xmlSerializer.text(spbsLine.getBpnId());
                xmlSerializer.endTag("", SPBSLine.XML_BPN_ID);

                xmlSerializer.startTag("", SPBSLine.XML_ACHIEVEMENT_CODE);
                xmlSerializer.text(spbsLine.getAchievementCode());
                xmlSerializer.endTag("", SPBSLine.XML_ACHIEVEMENT_CODE);

                xmlSerializer.startTag("", SPBSLine.XML_QUANTITY);
                xmlSerializer.text(String.valueOf(spbsLine.getQuantity()));
                xmlSerializer.endTag("", SPBSLine.XML_QUANTITY);

                xmlSerializer.startTag("", SPBSLine.XML_QUANTITY_ANGKUT);
                xmlSerializer.text(String.valueOf(spbsLine.getQuantityAngkut()));
                xmlSerializer.endTag("", SPBSLine.XML_QUANTITY_ANGKUT);

                xmlSerializer.startTag("", SPBSLine.XML_QUANTITY_REMAINING);
                xmlSerializer.text(String.valueOf(spbsLine.getQuantityRemaining()));
                xmlSerializer.endTag("", SPBSLine.XML_QUANTITY_REMAINING);

                xmlSerializer.startTag("", SPBSLine.XML_UOM);
                xmlSerializer.text(spbsLine.getUom());
                xmlSerializer.endTag("", SPBSLine.XML_UOM);

                xmlSerializer.startTag("", SPBSLine.XML_TOTAL_HARVESTER);
                xmlSerializer.text(String.valueOf(spbsLine.getTotalHarvester()));
                xmlSerializer.endTag("", SPBSLine.XML_TOTAL_HARVESTER);

                xmlSerializer.startTag("", SPBSLine.XML_GPS_KOORDINAT);
                xmlSerializer.text(spbsLine.getGpsKoordinat());
                xmlSerializer.endTag("", SPBSLine.XML_GPS_KOORDINAT);

                xmlSerializer.startTag("", SPBSLine.XML_CREATED_DATE);
                xmlSerializer.text(String.valueOf(spbsLine.getCreatedDate()));
                xmlSerializer.endTag("", SPBSLine.XML_CREATED_DATE);

                xmlSerializer.startTag("", SPBSLine.XML_CREATED_BY);
                xmlSerializer.text(spbsLine.getCreatedBy());
                xmlSerializer.endTag("", SPBSLine.XML_CREATED_BY);

                xmlSerializer.startTag("", SPBSLine.XML_MODIFIED_DATE);
                xmlSerializer.text(String.valueOf(spbsLine.getModifiedDate()));
                xmlSerializer.endTag("", SPBSLine.XML_MODIFIED_DATE);

                xmlSerializer.startTag("", SPBSLine.XML_MODIFIED_BY);
                xmlSerializer.text(spbsLine.getModifiedBy());
                xmlSerializer.endTag("", SPBSLine.XML_MODIFIED_BY);

                database.openTransaction();
                BPNQuantity bpnQuantity = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
                        BPNQuantity.XML_BPN_ID + "=?",
                        new String[]{spbsLine.getBpnId()},
                        null, null, null, null);
                database.closeTransaction();

                xmlSerializer.startTag("", SPBSLine.XML_NIK_HARVESTER);
                xmlSerializer.text(bpnQuantity == null ? "" : bpnQuantity.getNikHarvester());
                xmlSerializer.endTag("", SPBSLine.XML_NIK_HARVESTER);

                xmlSerializer.startTag("", SPBSLine.XML_BJR);
                xmlSerializer.text(String.valueOf(spbsLine.getBjr()));
                xmlSerializer.endTag("", SPBSLine.XML_BJR);

                xmlSerializer.endTag("", SPBSLine.XML_DOCUMENT);
            }

            xmlSerializer.endTag("", SPBSHeader.XML_DOCUMENT);

            xmlSerializer.endTag("", SPBSHeader.XML_FILE);

            xmlSerializer.endDocument();
            retval = writer.toString();
        } catch (Exception e) {
            Log.e("Ini Error", e.getMessage());
            e.printStackTrace();
        }
        return retval;
    }

    public ResponseSendSPBS parseResponseSPBS(InputStream inputStream) {
        ResponseSendSPBS responseSPBS = null;
        XmlPullParserFactory xmlPullParserFactory = null;
        XmlPullParser parser = null;
        try {
            xmlPullParserFactory = XmlPullParserFactory.newInstance();
            xmlPullParserFactory.setNamespaceAware(true);
            parser = xmlPullParserFactory.newPullParser();

            parser.setInput(inputStream, null);

            int eventType = parser.getEventType();
            String textValue = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equals(ResponseSendSPBS.XML.HEADER)) {
                            responseSPBS = new ResponseSendSPBS();
                        }
                        break;
                    case XmlPullParser.TEXT:
                        textValue = parser.getText().trim();
                        break;
                    case XmlPullParser.END_TAG:
                        if (tagName.equals(ResponseSendSPBS.XML.SPBS_NO)) {
                            responseSPBS.setSpbsNo(textValue);
                        } else if (tagName.equals(ResponseSendSPBS.XML.SPBS_STATUS)) {
                            responseSPBS.setSpbsStatus(textValue);
                        } else if (tagName.equals(ResponseSendSPBS.XML.HEADER)) {

                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseSPBS;
    }

}

