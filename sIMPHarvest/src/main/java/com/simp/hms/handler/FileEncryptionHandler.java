package com.simp.hms.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;

import com.simp.hms.database.DatabaseHelper;
import com.simp.hms.model.DateLocal;

public class FileEncryptionHandler {
	private Context context;
	
	private final String KEY = "IndoAgri2015Anka";
	
	File fileDatabase;
	File fileEncrypt;
	File fileDecrypt;
	
	public static final int STORAGE_INTERNAL = 1;
	public static final int STORAGE_EXTERNAL = 2;
	
	public FileEncryptionHandler(Context context){
		this.context = context;
		
		FolderHandler folderHandler = new FolderHandler(context);
		
		if(folderHandler.init()){
			fileDatabase = new File(context.getDatabasePath(DatabaseHelper.dbName).getAbsolutePath());
			//fileDatabase = new File(context.getDatabasePath(DatabaseHelper.dbName).getAbsolutePath());
			//fileDatabase = new File(folderHandler.getFileDatabaseExport(), DatabaseHelper.dbName);
		}
	}
	
	
	public boolean encrypt(int type) {
		try {
			boolean folderFound = false;
			File folder = null;
			
			switch (type) {
			case STORAGE_INTERNAL:
				FolderHandler folderHandler = new FolderHandler(context);
				
				if(folderHandler.init()){
					folder = new File(folderHandler.getFileDatabaseExport());
				}
				
				break;   
			case STORAGE_EXTERNAL:
	            File[] files = context.getExternalFilesDirs(Environment.DIRECTORY_DOCUMENTS);
	            
	            folder = new File(files[files.length-1], FolderHandler.DATABASE);
				break;
			default:
				break;
			}  

            if(folder != null && folder.exists()){
            	folderFound = true;
            }else{
            	folderFound = folder.mkdirs();
            }
                  
            if(folderFound){
                fileEncrypt = new File(folder.getAbsolutePath(), DatabaseHelper.dbName);
			  
			    // Here you read the cleartext.
			    FileInputStream fis = new FileInputStream(fileDatabase);
			    // This stream write the encrypted text. This stream will be wrapped by another stream.
			    FileOutputStream fos = new FileOutputStream(fileEncrypt);
			    
	            fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
	            // or fis.getChannel().transferTo(0, fis.getChannel().size(), fos.getChannel());
	            fis.close();
	            fos.close();
			    
			    /*
			    // Length is 16 byte
			    // Careful when taking user input!!! http://stackoverflow.com/a/3452620/1188357
			    SecretKeySpec sks = new SecretKeySpec(KEY.getBytes(), "AES");
			    // Create cipher
			    Cipher cipher = Cipher.getInstance("AES");
			    cipher.init(Cipher.ENCRYPT_MODE, sks);
			    // Wrap the output stream
			    CipherOutputStream cos = new CipherOutputStream(fos, cipher);
			    // Write bytes
			    int b;
			    byte[] d = new byte[8];
			    while((b = fis.read(d)) != -1) {
			        cos.write(d, 0, b);
			    }
			    // Flush and close streams.
			    cos.flush();
			    cos.close();
			    fis.close();
			    */
			    
			    MediaScannerConnection.scanFile(context, new String[] { fileEncrypt.getAbsolutePath() }, null, null);
			    
			    return true;
            }
		}catch(IOException e){
			e.printStackTrace();
		}
		
		/*
		catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		return false;
	}
	
	public boolean decrypt(String filePath) {
		try {
			FolderHandler folderHandler = new FolderHandler(context);
			
			if(folderHandler.init()){
				fileEncrypt = new File(filePath);  
				fileDecrypt = new File(folderHandler.getFileDatabaseImport(), DatabaseHelper.dbName);
				
            	FileInputStream fis = new FileInputStream(fileEncrypt);
	            FileOutputStream fos = new FileOutputStream(fileDecrypt);
	            fos.getChannel().transferFrom(fis.getChannel(), 0, fis.getChannel().size());
	            // or fis.getChannel().transferTo(0, fis.getChannel().size(), fos.getChannel());
	            fis.close();
	            fos.close();
				
				/*
			    FileInputStream fis = new FileInputStream(fileEncrypt);
				
			    FileOutputStream fos = new FileOutputStream(fileDecrypt);
			    SecretKeySpec sks = new SecretKeySpec(KEY.getBytes(), "AES");
			    Cipher cipher = Cipher.getInstance("AES");
			    cipher.init(Cipher.DECRYPT_MODE, sks);
			    CipherInputStream cis = new CipherInputStream(fis, cipher);
			    int b;   
			    byte[] d = new byte[8];
			    while((b = cis.read(d)) != -1) {
			        fos.write(d, 0, b);
			    }
			    fos.flush();
			    fos.close(); 
			    cis.close();
			    */	
			    
			    MediaScannerConnection.scanFile(context, new String[] { fileDecrypt.getAbsolutePath() }, null, null);
			    
			    return true;
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		
		return false;
	}
}
