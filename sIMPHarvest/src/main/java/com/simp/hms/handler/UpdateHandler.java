package com.simp.hms.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.simp.hms.routines.Constanta;


public class UpdateHandler {
	
	public String checkVersion(){
		int resCode = -1;
		InputStream is = null;
		
		try{
			URL url = new URL(Constanta.SERVER + "versi.json");
			HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
			
			httpConn.setAllowUserInteraction(false);
	        httpConn.setInstanceFollowRedirects(true);   
	        httpConn.setRequestMethod("GET");
	        httpConn.connect();
	        
	        resCode = httpConn.getResponseCode();
	        
	        if(resCode == HttpURLConnection.HTTP_OK){
	        	is = httpConn.getInputStream();
	        	
	        	return convertStreamToString(is);
	        }
	        
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	private String convertStreamToString(InputStream is) {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	            sb.append(line).append('\n');
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
	}
}
