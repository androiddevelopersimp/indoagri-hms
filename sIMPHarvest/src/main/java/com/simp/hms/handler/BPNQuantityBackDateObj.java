package com.simp.hms.handler;

class BPNQuantityBackDateObj {
    // REMEMBER: each attribute is a column
    //
    private final String bpnId;
    private final String bpnDate;

    public BPNQuantityBackDateObj(String bpnId, String bpnDate) {
        this.bpnId = bpnId;
        this.bpnDate = bpnDate;
    }

    public String getBpnId() {
        return bpnId;
    }

    public String getBpnDate() {
        return bpnDate;
    }

}
