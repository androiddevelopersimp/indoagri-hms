package com.simp.hms.handler;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class DeviceHandler {
    private Context context;
    private TelephonyManager tlpMgr;

    public DeviceHandler(Context context) {
        this.context = context;

        tlpMgr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
    }

    public String getImei() {
//		return "359143060294357";
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            return Secure.getString(context.getContentResolver(),
                    Secure.ANDROID_ID);
        } else {
            return tlpMgr.getDeviceId();
        }
    }

    public String getDeviceId() {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    public String getAppVersion() {
        PackageInfo vInfo;
        String vAppName;
        int vAppCode;

        try {
            vInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            vAppName = vInfo.versionName;
            vAppCode = vInfo.versionCode;

        } catch (NameNotFoundException e) {
            e.printStackTrace();
            vAppName = "1";
            vAppCode = 1;
        }

        return vAppName;
    }

    public String getCodeVersion() {
        PackageInfo vInfo;
        int vAppCode;

        try {
            vInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            vAppCode = vInfo.versionCode;

        } catch (NameNotFoundException e) {
            e.printStackTrace();
            vAppCode = 1;
        }

        return String.valueOf(vAppCode);
    }
}
