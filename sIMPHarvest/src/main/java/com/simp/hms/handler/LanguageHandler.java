package com.simp.hms.handler;

import java.util.Locale;

import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;

public class LanguageHandler {

	private Context context;
	
	public LanguageHandler(Context context){
		this.context = context;
	}
	
	public void updateLanguage(String codeCountry){
		Configuration config = new Configuration();
		
		if(!TextUtils.isEmpty(codeCountry)){
			config.locale = new Locale(codeCountry);
		}else{
			config.locale = Locale.getDefault();
		}
		
		context.getResources().updateConfiguration(config, null);
	}
}
