package com.simp.hms.handler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.model.FileXML;

import android.content.Context;
import android.media.MediaScannerConnection;

public class FileXMLHandler {
	private Context context;
	
	public FileXMLHandler(Context context){
		this.context = context;
	}
	
	public List<FileXML> getFiles(String path_new, String path_backup, final String type){
		File file_path = new File(path_new);
		List<FileXML> lst_file = new ArrayList<FileXML>();
		
		if(file_path.exists()){
			File files[] = file_path.listFiles(new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String name) {
					return name.toLowerCase(Locale.getDefault()).endsWith(type);
				}
			});
			
			for(int i = 0; i < files.length; i++){
				lst_file.add(new FileXML(files[i].getPath(), path_backup));
			}
		}
		
		return lst_file;
		
	}

	public boolean createFile(String folder, String filename, String content){
		try{
			File file = new File(folder, filename);


			//add by adit 20161229
			if(file.exists())
				file.delete();

			if(!file.exists()){
				file.createNewFile();
			}


			  
            BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
            out.write(content);   
            out.newLine();
            out.flush();
            out.close();
            
            MediaScannerConnection.scanFile(context, new String[] { file.getAbsolutePath() }, null, null);
            
            return true;
		}catch(IOException e){
			e.printStackTrace();
			
			return false;
		}
	}
}
