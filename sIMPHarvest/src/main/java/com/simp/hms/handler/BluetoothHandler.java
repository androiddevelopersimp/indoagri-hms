package com.simp.hms.handler;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

import java.lang.reflect.Method;

import static android.content.ContentValues.TAG;

public class BluetoothHandler {

    public boolean pairDevice(BluetoothDevice btDevice) {
        try {
            Class class1 = Class.forName("android.bluetooth.BluetoothDevice");
            Method createBondMethod = class1.getMethod("createBond");
            Boolean returnValue = (Boolean) createBondMethod.invoke(btDevice);
            return returnValue.booleanValue();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        }
    }

    public boolean unpairDevice(BluetoothDevice device) {
        try {
            Method m = device.getClass().getMethod("removeBond", (Class[]) null);
            Boolean retVal = (Boolean) m.invoke(device, (Object[]) null);
            return retVal.booleanValue();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        }
    }
}
