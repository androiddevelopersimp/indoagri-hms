package com.simp.hms.handler;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Anka.Wirawan on 8/3/2016.
 */
public class AssetsHandler {
    private Context context;
    private String fileName;

    public  AssetsHandler(Context context, String fileName){
        this.context = context;
        this.fileName = fileName;
    }

    public String getJsonFromAsset(){
        AssetManager assetManager = context.getAssets();
        InputStream istr = null;
        String json = "";

        try {
            istr = assetManager.open(fileName);

            int length = istr.available();
            byte[] data = new byte[length];
            istr.read(data);
            json = new String(data);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    public Bitmap getImageFromAsset()
    {
        AssetManager assetManager = context.getAssets();
        InputStream is = null;

        try {
            is = assetManager.open(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bitmap bitmap = BitmapFactory.decodeStream(is);

        return bitmap;
    }
}
