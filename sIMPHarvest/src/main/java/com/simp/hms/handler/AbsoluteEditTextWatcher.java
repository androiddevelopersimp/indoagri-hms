package com.simp.hms.handler;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

public class AbsoluteEditTextWatcher implements TextWatcher{
	private EditText edt;
	private int type;	// 0 = int, 1 = double
	String before;
	String after;
	
	
	public AbsoluteEditTextWatcher(EditText edt, int type){
		this.edt = edt;
		this.type = type;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
		before = s.toString();
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {		

	}

	@Override
	public void afterTextChanged(Editable s) {
		String value;
		
		after = s.toString().trim();
		value = after;
		
		if(!TextUtils.isEmpty(value)){
			edt.removeTextChangedListener(this);
			
			value = value.replace("-", "");
			
			if(checkDot(value) > 1){
				value = before;
			}
			
			if(decimalPlaces(value, 2) > 2){
				value = value.substring(0, value.length() - 1);
			}
			
			edt.setText(value);
			edt.addTextChangedListener(this);
		}
	}
	
	private int checkDot(String value){
		int countDot = 0;
		
		for(int i = 0; i < value.length(); i++){
			char c = value.charAt(i);
			
			if(c == '.'){
				countDot++;
			}
		}
		
		return countDot;
	}
	
	private int decimalPlaces(String value, int digit){
		if(value.indexOf(".") > 0){
			String temp = value.substring(value.indexOf(".") + 1, value.length());
			
			return temp.length();
		}else{
			return 0;
		}
	}
}
