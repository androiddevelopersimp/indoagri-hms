package com.simp.hms.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

public class EditTextCustom extends EditText{

	public EditTextCustom(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}
	
	public EditTextCustom(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public EditTextCustom(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onSelectionChanged(int selStart, int selEnd) {
		super.onSelectionChanged(selStart, selEnd);
		   
		Log.d("tag", selStart + ":" + selEnd);   
		String text = this.getText().toString();
		
		if(selStart > text.length()){   
			selStart = text.length();
		}
		
		if(selEnd > text.length()){
			selEnd = text.length();
		}
		
		try{
			if(text.length() > 0){
				this.setSelection(selStart, selEnd);
			}
		}catch(IndexOutOfBoundsException e){
			e.printStackTrace();
		}
	}
}
