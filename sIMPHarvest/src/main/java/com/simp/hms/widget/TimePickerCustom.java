package com.simp.hms.widget;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.simp.hms.R;

import java.util.Calendar;
import java.util.Locale;

public class TimePickerCustom extends LinearLayout{

	private View myPickerView;

	private ImageButton hour_plus;
	private TextView hour_display;
	private ImageButton hour_minus;

	private ImageButton minute_plus;
	private TextView minute_display;
	private ImageButton minute_minus;

	private ImageButton second_plus;
	private TextView second_display;
	private ImageButton second_minus;

	private Calendar cal;

	public TimePickerCustom(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context mContext) {
		LayoutInflater inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		myPickerView = inflator.inflate(R.layout.widget_timepicker, null);
		this.addView(myPickerView);

		initializeReference();
	}

	private void initializeReference() {

		minute_plus = (ImageButton) myPickerView.findViewById(R.id.btn_minute_plus);
		minute_plus.setOnClickListener(minute_plus_listener);
		minute_display = (TextView) myPickerView.findViewById(R.id.txt_minute_display);
		minute_minus = (ImageButton) myPickerView.findViewById(R.id.btn_minute_minus);
		minute_minus.setOnClickListener(minute_minus_listener);

		hour_plus = (ImageButton) myPickerView.findViewById(R.id.btn_hour_plus);
		hour_plus.setOnClickListener(hour_plus_listener);
		hour_display = (TextView) myPickerView.findViewById(R.id.txt_hour_display);
		hour_minus = (ImageButton) myPickerView.findViewById(R.id.btn_hour_minus);
		hour_minus.setOnClickListener(hour_minus_listener);

		second_plus = (ImageButton) myPickerView.findViewById(R.id.btn_second_plus);
		second_plus.setOnClickListener(second_plus_listener);
		second_display = (TextView) myPickerView.findViewById(R.id.txt_second_display);
		second_minus = (ImageButton) myPickerView.findViewById(R.id.btn_second_minus);
		second_minus.setOnClickListener(second_minus_listener);

		initData();

	}

	private void initData() {
		cal = Calendar.getInstance(Locale.getDefault());

		hour_display.setText(String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
		minute_display.setText(String.valueOf(cal.get(Calendar.MINUTE)));
		second_display.setText(String.valueOf(cal.get(Calendar.SECOND)));
	}

	OnClickListener hour_plus_listener = new OnClickListener() {

		public void onClick(View v) {

			try {
				cal.add(Calendar.HOUR_OF_DAY, 1);

				setDisplay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				sendToListener();
			} catch (Exception e) {
				Log.e("", e.toString());
			}
		}
	};

	OnClickListener hour_minus_listener = new OnClickListener() {

		public void onClick(View v) {
			try {
				cal.add(Calendar.HOUR_OF_DAY, -1);

				setDisplay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				sendToListener();
			} catch (Exception e) {
				Log.e("", e.toString());
			}
		}
	};

	OnClickListener minute_plus_listener = new OnClickListener() {

		public void onClick(View v) {

			try {
				cal.add(Calendar.MINUTE, 1);

				setDisplay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				sendToListener();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	};

	OnClickListener minute_minus_listener = new OnClickListener() {

		public void onClick(View v) {

			try {
				cal.add(Calendar.MINUTE, -1);

				setDisplay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				sendToListener();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	OnClickListener second_plus_listener = new OnClickListener() {

		public void onClick(View v) {

			try {
				cal.add(Calendar.SECOND, +1);

				setDisplay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				sendToListener();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	OnClickListener second_minus_listener = new OnClickListener() {

		public void onClick(View v) {

			try {
				cal.add(Calendar.YEAR, -1);

				setDisplay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				sendToListener();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	private void sendToListener() {

		if (mTimeWatcher != null) {
			mTimeWatcher.onTimeChanged(cal);
		}

	}

	public void setTimeChangedListener(TimeWatcher listener) {
		this.mTimeWatcher = listener;
	}

	TimeWatcher mTimeWatcher = null;

	public interface TimeWatcher {
		void onTimeChanged(Calendar c);
	}

	private void initDisplay() {
		hour_display.setText(String.valueOf(cal.get(Calendar.HOUR_OF_DAY)));
		minute_display.setText(String.valueOf(cal.get(Calendar.MINUTE)));
		second_display.setText(String.valueOf(cal.get(Calendar.SECOND)));

	}
	
	public void setDisplay(int vDate, int vMonth, int vYear){
		
		Log.d("date", String.valueOf(vDate));
		Log.d("month", String.valueOf(vMonth));
		Log.d("year", String.valueOf(vYear));
		
		hour_display.setText(String.valueOf(vDate));
		minute_display.setText(String.valueOf(vMonth));
		second_display.setText(String.valueOf(vYear));
	}
	
	public String getTime(){
		return hour_display.getText().toString() + ":" +
				minute_display.getText().toString() + ":" +
				second_display.getText().toString();
	}
}
