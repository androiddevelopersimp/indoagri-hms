package com.simp.hms.service;

import java.io.IOException;

import com.simp.hms.handler.FileEncryptionHandler;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;

public class BackupService extends Service{
	
	BackupTask backupTask;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		   
		backupTask = new BackupTask();
		backupTask.execute();
		
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if(backupTask != null && backupTask.getStatus() != AsyncTask.Status.FINISHED){
			backupTask.cancel(true);
			backupTask = null;
		}
	}



	private class BackupTask extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {

			try {
				copyAppDbToDownloadFolder();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return null;
		}
	}
	
	private void copyAppDbToDownloadFolder() throws IOException {
	    	boolean mExternalStorageAvailable = false;
	    	boolean mExternalStorageWriteable = false;
	    	String state = Environment.getExternalStorageState();
	    	
	    	if (Environment.MEDIA_MOUNTED.equals(state)) {
	    	    // We can read and write the media
	    	    mExternalStorageAvailable = mExternalStorageWriteable = true;
	    	} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
	    	    // We can only read the media
	    	    mExternalStorageAvailable = true;
	    	    mExternalStorageWriteable = false;
	    	} else {
	    	    // Something else is wrong. It may be one of many other states, but all we need
	    	    //  to know is we can neither read nor write
	    	    mExternalStorageAvailable = mExternalStorageWriteable = false;
	    	}
//	    	
	    	if(mExternalStorageAvailable && mExternalStorageWriteable){
	        	new FileEncryptionHandler(getApplicationContext()).encrypt(FileEncryptionHandler.STORAGE_EXTERNAL);
	    	}
	}
}
