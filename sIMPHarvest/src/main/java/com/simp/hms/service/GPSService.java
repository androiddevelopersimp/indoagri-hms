package com.simp.hms.service;

import java.util.Date;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.simp.hms.R;

public class GPSService extends Service implements LocationListener {
    private Context context;

    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;

    Location location;
    double latitude;
    double longitude;

    protected LocationManager loc_mgr;

    public GPSService(Context context) {
        this.context = context;
        location = null;
        getLocation();
        checkGPS();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
    }

    public void checkGPS() {
        if (!loc_mgr.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    public Location getLocation() {
        try {
            loc_mgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            loc_mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

            isGPSEnabled = loc_mgr.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = loc_mgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (isGPSEnabled) {
                canGetLocation = true;

                if (loc_mgr != null) {
                    Location newLocation = loc_mgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    if (newLocation != null) {
                        Date dateCurr = new Date();

                        long timeDiff = dateCurr.getTime() - newLocation.getTime();

//                		Log.d("tag", "cur:" + new DateLocal(new Date(date_curr.getTime())).getDateString(DateLocal.FORMAT_FILE));
//                		Log.d("tag", "new:" + new DateLocal(new Date(new_location.getTime())).getDateString(DateLocal.FORMAT_FILE));
//                		Log.d("tag", "X:" + time_diff);

                        if (timeDiff <= (5 * 60 * 1000) && newLocation.getAccuracy() < 100) {
                            doWorkWithNewLocation(newLocation);
                        }
                    }
                }
            } else {
                canGetLocation = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    public boolean canGetLocation() {
        return canGetLocation;
    }

    public void stopUsingGPS() {
        if (loc_mgr != null) {
            loc_mgr.removeUpdates(GPSService.this);
        }
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        doWorkWithNewLocation(location);

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    private void doWorkWithNewLocation(Location new_location) {
        if (isBetterLocation(location, new_location)) {
            location = new_location;

            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }
    }


    private boolean isBetterLocation(Location old_location, Location new_location) {
        if (old_location == null) {
            return true;
        }

        boolean is_newer = new_location.getTime() > old_location.getTime();

        boolean is_more_accurate = new_location.getAccuracy() < old_location.getAccuracy();

        if (is_more_accurate && is_newer) {
            return true;
        } else if (is_more_accurate && !is_newer) {
            long time_diff = new_location.getTime() - old_location.getTime();

            if (time_diff > 1000) {
                return true;
            }
        }

        return false;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater lay = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = lay.inflate(R.layout.dialog_notification, null);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txt_notification_title = dialoglayout.findViewById(R.id.txt_notification_title);
        TextView txt_notification_message = dialoglayout.findViewById(R.id.txt_notification_message);
        Button btn_notification_ok = dialoglayout.findViewById(R.id.btn_notification_ok);

        txt_notification_title.setText("GPS");
        txt_notification_message.setText("GPS Must enable !");
        btn_notification_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                alertDialog.dismiss();
            }
        });

//        builder.setMessage("GPS must Enable !")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
//                        context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                    }
//                });
//        final AlertDialog alert = builder.create();
//        alert.show();
        alertDialog.show();
    }

}
