package com.simp.hms.service;

import java.util.Date;
import java.util.List;

import com.simp.hms.activity.ancakpanen.AncakPanenActivity;
import com.simp.hms.activity.bkm.BKMAbsentActivity;
import com.simp.hms.activity.bpn.BPNActivity;
import com.simp.hms.activity.bpn.BPNCocoaActivity;
import com.simp.hms.activity.bpn.BPNCocoaPODActivity;
import com.simp.hms.activity.bpn.BPNKaretActivity;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSBlockActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spbs.SPBSUniversalActivity;
import com.simp.hms.activity.spbs.SPBSWoosimActivity;
import com.simp.hms.activity.spta.SPTAActivity;
import com.simp.hms.activity.spu.SPUActivity;
import com.simp.hms.activity.spu.SPUPODActivity;
import com.simp.hms.activity.taksasi.TaksasiActivity;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class GPSTriggerService extends Service implements LocationListener {
	 
    private Location location = null;
    protected LocationManager locMgr;  
    
    public static BPNActivity bpnActivity;
	public static BPNKaretActivity bpnKaretActivity;
	public static BPNCocoaActivity bpnCocoaActivity;
    public static BKMAbsentActivity bkmAbsentActivity;
    public static SPBSActivity spbsActivity;
    public static SPBSUniversalActivity spbsUniversalActivity;
    public static SPBSWoosimActivity spbsWoosimActivity;
    public static SPBSDatecsActivity spbsDatecsActivity;
    public static SPBSBlockActivity spbsBlockActivity;
    public static TaksasiActivity taksasiActivity;
    public static AncakPanenActivity ancakPanenActivity;
	public static SPTAActivity sptaActivity;
	public static SPUActivity spuActivity;
	public static SPUPODActivity spupodActivity;
	public static BPNCocoaPODActivity bpnCocoaPODActivity;
    private ActivityManager activityMgr;
  
    public void initGPS() {    	
        try {              	
            locMgr = (LocationManager) getSystemService(LOCATION_SERVICE); 

            if (locMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				try {
					locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

					if (locMgr != null) {
						Location new_location = locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);

						if (new_location != null) {
							Date date_curr = new Date();

							long time_diff = date_curr.getTime() - new_location.getTime();

							if (time_diff <= (5 * 60 * 1000) && new_location.getAccuracy() < 100) {
								doWorkWithNewLocation(new_location);
							}
						}
					}
				}catch (SecurityException e){

				}
            }
            
        } catch (Exception e) {
            e.printStackTrace(); 
        }
    }
    

	public void stopGps(){
		if(locMgr != null){
			try {
				locMgr.removeUpdates(GPSTriggerService.this);
			}catch (SecurityException e){

			}
		}
	}
    
    @Override
    public void onLocationChanged(Location new_location) { 
    	doWorkWithNewLocation(new_location);
    }  
 
    @Override
    public void onProviderDisabled(String provider) {
    	Log.d("TAG", provider + " disbaled");
    }
 
    @Override
    public void onProviderEnabled(String provider) {
    	Log.d("TAG", provider + " enabled");
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
 
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
    
	@Override
	public void onCreate() {
		super.onCreate();
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		activityMgr = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		initGPS();
		
//		Toast.makeText(getApplicationContext(), "jalan", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		activityMgr = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		initGPS();
		return Service.START_STICKY;
	}


	@Override
	public void onDestroy() {
		stopGps();
		super.onDestroy();
	}
	
	private void doWorkWithNewLocation(Location newLocation){
		if(isBetterLocation(location, newLocation)){
			location = newLocation;
		}
		  
		if(activityMgr != null){
			List<ActivityManager.RunningTaskInfo> taskInfo = activityMgr.getRunningTasks(1);
			
			if(taskInfo.get(0).topActivity.getClassName().toString().equals(BPNActivity.class.getCanonicalName())){
				if(bpnActivity != null){
					bpnActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(BPNKaretActivity.class.getCanonicalName())){
				if(bpnKaretActivity != null){
					bpnKaretActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(BPNCocoaActivity.class.getCanonicalName())){
				if(bpnCocoaActivity != null){
					bpnCocoaActivity.updateGpsKoordinat(location);
				}
			}
			else if(taskInfo.get(0).topActivity.getClassName().toString().equals(BKMAbsentActivity.class.getCanonicalName())){
				if(bkmAbsentActivity != null){
					bkmAbsentActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPBSActivity.class.getCanonicalName())){
				if(spbsActivity != null){
					spbsActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPBSActivity.class.getCanonicalName())){
				if(spbsUniversalActivity != null){
					spbsUniversalActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPBSWoosimActivity.class.getCanonicalName())){
				if(spbsWoosimActivity != null){
					spbsWoosimActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPBSDatecsActivity.class.getCanonicalName())){
				if(spbsDatecsActivity != null){
					spbsDatecsActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(TaksasiActivity.class.getCanonicalName())){
				if(taksasiActivity != null){
					taksasiActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(AncakPanenActivity.class.getCanonicalName())){
				if(ancakPanenActivity != null){
					ancakPanenActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPTAActivity.class.getCanonicalName())){
				if(sptaActivity != null){
					sptaActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPUActivity.class.getCanonicalName())){
				if(spuActivity != null){
					spuActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPUActivity.class.getCanonicalName())){
				if(spupodActivity != null){
					spupodActivity.updateGpsKoordinat(location);
				}
			}else if(taskInfo.get(0).topActivity.getClassName().toString().equals(SPUActivity.class.getCanonicalName())){
				if(bpnCocoaPODActivity != null){
					bpnCocoaPODActivity.updateGpsKoordinat(location);
				}
			}
		}
	}
	
	
	private boolean isBetterLocation(Location old_location, Location new_location){
		if(old_location == null){
			return true;
		}
		
		boolean is_newer = new_location.getTime() > old_location.getTime();
		
		boolean is_more_accurate = new_location.getAccuracy() < old_location.getAccuracy();
		
		if(is_more_accurate && is_newer){
			return true;
		}else if(is_more_accurate && !is_newer){
			long time_diff = new_location.getTime() - old_location.getTime();
			
			if(time_diff > 1000){
				return true;
			}
		}
		
		return false;
	}
  
}