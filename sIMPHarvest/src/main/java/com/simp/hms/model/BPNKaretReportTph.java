package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Fernando.Siagian on 3/11/2017.
 */

public class BPNKaretReportTph implements Parcelable {
    private String id;
    private String companyCode;
    private String estate;
    private String division;
    private String tph;
    private long createdDate;

    private double qtyLatexWet;
    private double qtyLatexDRC;
    private double qtyLatexDry;
    private double qtyLumpWet;
    private double qtyLumpDRC;
    private double qtyLumpDry;
    private double qtySlabWet;
    private double qtySlabDRC;
    private double qtySlabDry;
    private double qtyTreelaceWet;
    private double qtyTreelaceDRC;
    private double qtyTreelaceDry;

    public BPNKaretReportTph(String id, String companyCode, String estate,
                             String division, String tph, long createdDate,
                             double qtyLatexWet, double qtyLatexDRC, double qtyLatexDry,
                             double qtyLumpWet, double qtyLumpDRC, double qtyLumpDry,
                             double qtySlabWet, double qtySlabDRC, double qtySlabDry,
                             double qtyTreelaceWet, double qtyTreelaceDRC, double qtyTreelaceDry) {
        super();
        this.id = id;
        this.companyCode = companyCode;
        this.estate = estate;
        this.division = division;
        this.tph = tph;
        this.createdDate = createdDate;
        this.qtyLatexWet = qtyLatexWet;
        this.qtyLatexDRC = qtyLatexDRC;
        this.qtyLatexDry = qtyLatexDry;
        this.qtyLumpWet = qtyLumpWet;
        this.qtyLumpDRC = qtyLumpDRC;
        this.qtyLumpDry = qtyLumpDry;
        this.qtySlabWet = qtySlabWet;
        this.qtySlabDRC = qtySlabDRC;
        this.qtySlabDry = qtySlabDry;
        this.qtyTreelaceWet = qtyTreelaceWet;
        this.qtyTreelaceDRC = qtyTreelaceDRC;
        this.qtyTreelaceDry = qtyTreelaceDry;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getTph() {
        return tph;
    }

    public void setTph(String tph) {
        this.tph = tph;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public double getQtyLatexWet() {
        return qtyLatexWet;
    }

    public void setQtyLatexWet(double qtyLatexWet) {
        this.qtyLatexWet = qtyLatexWet;
    }

    public double getQtyLatexDRC() {
        return qtyLatexDRC;
    }

    public void setQtyLatexDRC(double qtyLatexDRC) {
        this.qtyLatexDRC = qtyLatexDRC;
    }

    public double getQtyLatexDry() {
        return qtyLatexDry;
    }

    public void setQtyLatexDry(double qtyLatexDry) {
        this.qtyLatexDry = qtyLatexDry;
    }

    public double getQtyLumpWet() {
        return qtyLumpWet;
    }

    public void setQtyLumpWet(double qtyLumpWet) {
        this.qtyLumpWet = qtyLumpWet;
    }

    public double getQtyLumpDRC() {
        return qtyLumpDRC;
    }

    public void setQtyLumpDRC(double qtyLumpDRC) {
        this.qtyLumpDRC = qtyLumpDRC;
    }

    public double getQtyLumpDry () {
        return qtyLumpDry;
    }

    public void setQtyLumpDry(double qtyLumpDry) {
        this.qtyLumpDry = qtyLumpDry;
    }

    public double getQtySlabWet() {
        return qtySlabWet;
    }

    public void setQtySlabWet(double qtySlabWet) {
        this.qtySlabWet = qtySlabWet;
    }

    public double getQtySlabDRC() {
        return qtySlabDRC;
    }

    public void setQtySlabDRC(double qtySlabDRC) {
        this.qtySlabDRC = qtySlabDRC;
    }

    public double getQtySlabDry() {
        return qtySlabDry;
    }

    public void setQtySlabDry(double qtySlabDry) {
        this.qtySlabDry = qtySlabDry;
    }

    public double getQtyTreelaceWet() {
        return qtyTreelaceWet;
    }

    public void setQtyTreelaceWet(double qtyTreelaceWet) {
        this.qtyTreelaceWet = qtyTreelaceWet;
    }

    public double getQtyTreelaceDRC() {
        return qtyTreelaceDRC;
    }

    public void setQtyTreelaceDRC(double qtyTreelaceDRC) {
        this.qtyTreelaceDRC = qtyTreelaceDRC;
    }

    public double getQtyTreelaceDry() {
        return qtyTreelaceDry;
    }

    public void setQtyTreelaceDry(double qtyTreelaceDry) {
        this.qtyTreelaceDry = qtyTreelaceDry;
    }

    public static final Parcelable.Creator<BPNKaretReportTph> CREATOR = new Creator<BPNKaretReportTph>() {

        @Override
        public BPNKaretReportTph createFromParcel(Parcel parcel) {
            BPNKaretReportTph bpnHeader = new BPNKaretReportTph(parcel.readString(), parcel.readString(),
                    parcel.readString(), parcel.readString(), parcel.readString(), parcel.readLong(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble());

            return bpnHeader;
        }

        @Override
        public BPNKaretReportTph[] newArray(int size) {
            return new BPNKaretReportTph[size];
        }
    };

    @Override
        public int describeContents() {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int flags) {
            parcel.writeString(id);
            parcel.writeString(companyCode);
            parcel.writeString(estate);
            parcel.writeString(division);
            parcel.writeString(tph);
            parcel.writeLong(createdDate);
            parcel.writeDouble(qtyLatexWet);
            parcel.writeDouble(qtyLatexDRC);
            parcel.writeDouble(qtyLatexDry);
            parcel.writeDouble(qtyLumpWet);
            parcel.writeDouble(qtyLumpDRC);
            parcel.writeDouble(qtyLumpDry);
            parcel.writeDouble(qtySlabWet);
            parcel.writeDouble(qtySlabDRC);
            parcel.writeDouble(qtySlabDry);
            parcel.writeDouble(qtyTreelaceWet);
            parcel.writeDouble(qtyTreelaceDRC);
            parcel.writeDouble(qtyTreelaceDry);

    }
}
