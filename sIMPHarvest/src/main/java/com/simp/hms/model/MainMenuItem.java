package com.simp.hms.model;

import android.content.Intent;

public class MainMenuItem {
	private int icon;
	private String label;
	private Intent intent;
	
	public MainMenuItem(int icon, String label, Intent intent) {
		this.icon = icon;
		this.label = label;
		this.intent = intent;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Intent getIntent() {
		return intent;
	}

	public void setIntent(Intent intent) {
		this.intent = intent;
	}
}
