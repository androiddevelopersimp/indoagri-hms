package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Fernando.Siagian on 30/10/2017.
 */

public class BPNKaretReportBlock implements Parcelable {
    private String companyCode;
    private String estate;
    private String division;
    private String gang;
    private String block;

    private double qtyLatexWet;
    private double qtyLatexDRC;
    private double qtyLatexDry;
    private double qtyLumpWet;
    private double qtyLumpDRC;
    private double qtyLumpDry;
    private double qtySlabWet;
    private double qtySlabDRC;
    private double qtySlabDry;
    private double qtyTreelaceWet;
    private double qtyTreelaceDRC;
    private double qtyTreelaceDry;

    public static final String TABLE_NAME = "BPN_REPORT_HARVESTER";

    public BPNKaretReportBlock(String companyCode, String estate, String division, String gang, String block,
                                   double qtyLatexWet, double qtyLatexDRC, double qtyLatexDry,
                                   double qtyLumpWet, double qtyLumpDRC, double qtyLumpDry,
                                   double qtySlabWet, double qtySlabDRC, double qtySlabDry,
                                   double qtyTreelaceWet, double qtyTreelaceDRC, double qtyTreelaceDry) {

        super();
        this.companyCode = companyCode;
        this.estate = estate;
        this.division = division;
        this.gang = gang;
        this.block = block;

        this.qtyLatexWet = qtyLatexWet;
        this.qtyLatexDRC = qtyLatexDRC;
        this.qtyLatexDry = qtyLatexDry;
        this.qtyLumpWet = qtyLumpWet;
        this.qtyLumpDRC = qtyLumpDRC;
        this.qtyLumpDry = qtyLumpDry;
        this.qtySlabWet = qtySlabWet;
        this.qtySlabDRC = qtySlabDRC;
        this.qtySlabDry = qtySlabDry;
        this.qtyTreelaceWet = qtyTreelaceWet;
        this.qtyTreelaceDRC = qtyTreelaceDRC;
        this.qtyTreelaceDry = qtyTreelaceDry;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getGang() {
        return gang;
    }

    public void setGang(String gang) {
        this.gang = gang;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public double getQtyLatexWet() {
        return qtyLatexWet;
    }

    public void setQtyLatexWet(double qtyLatexWet) {
        this.qtyLatexWet = qtyLatexWet;
    }

    public double getQtyLatexDRC() {
        return qtyLatexDRC;
    }

    public void setQtyLatexDRC(double qtyLatexDRC) {
        this.qtyLatexDRC = qtyLatexDRC;
    }

    public double getQtyLatexDry() {
        return qtyLatexDry;
    }

    public void setQtyLatexDry(double qtyLatexDry) {
        this.qtyLatexDry = qtyLatexDry;
    }

    public double getQtyLumpWet() {
        return qtyLumpWet;
    }

    public void setQtyLumpWet(double qtyLumpWet) {
        this.qtyLumpWet = qtyLumpWet;
    }

    public double getQtyLumpDRC() {
        return qtyLumpDRC;
    }

    public void setQtyLumpDRC(double qtyLumpDRC) {
        this.qtyLumpDRC = qtyLumpDRC;
    }

    public double getQtyLumpDry () {
        return qtyLumpDry;
    }

    public void setQtyLumpDry(double qtyLumpDry) {
        this.qtyLumpDry = qtyLumpDry;
    }

    public double getQtySlabWet() {
        return qtySlabWet;
    }

    public void setQtySlabWet(double qtySlabWet) {
        this.qtySlabWet = qtySlabWet;
    }

    public double getQtySlabDRC() {
        return qtySlabDRC;
    }

    public void setQtySlabDRC(double qtySlabDRC) {
        this.qtySlabDRC = qtySlabDRC;
    }

    public double getQtySlabDry() {
        return qtySlabDry;
    }

    public void setQtySlabDry(double qtySlabDry) {
        this.qtySlabDry = qtySlabDry;
    }

    public double getQtyTreelaceWet() {
        return qtyTreelaceWet;
    }

    public void setQtyTreelaceWet(double qtyTreelaceWet) {
        this.qtyTreelaceWet = qtyTreelaceWet;
    }

    public double getQtyTreelaceDRC() {
        return qtyTreelaceDRC;
    }

    public void setQtyTreelaceDRC(double qtyTreelaceDRC) {
        this.qtyTreelaceDRC = qtyTreelaceDRC;
    }

    public double getQtyTreelaceDry() {
        return qtyTreelaceDry;
    }

    public void setQtyTreelaceDry(double qtyTreelaceDry) {
        this.qtyTreelaceDry = qtyTreelaceDry;
    }

    public static final Parcelable.Creator<BPNKaretReportBlock> CREATOR = new Creator<BPNKaretReportBlock>() {

        @Override
        public BPNKaretReportBlock createFromParcel(Parcel parcel) {
            BPNKaretReportBlock bpnHeader = new BPNKaretReportBlock(parcel.readString(), parcel.readString(),
                    parcel.readString(), parcel.readString(), parcel.readString(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(), parcel.readDouble());

            return bpnHeader;
        }

        @Override
        public BPNKaretReportBlock[] newArray(int size) {
            return new BPNKaretReportBlock[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(companyCode);
        parcel.writeString(estate);
        parcel.writeString(division);
        parcel.writeString(gang);
        parcel.writeString(block);

        parcel.writeDouble(qtyLatexWet);
        parcel.writeDouble(qtyLatexDRC);
        parcel.writeDouble(qtyLatexDry);
        parcel.writeDouble(qtyLumpWet);
        parcel.writeDouble(qtyLumpDRC);
        parcel.writeDouble(qtyLumpDry);
        parcel.writeDouble(qtySlabWet);
        parcel.writeDouble(qtySlabDRC);
        parcel.writeDouble(qtySlabDry);
        parcel.writeDouble(qtyTreelaceWet);
        parcel.writeDouble(qtyTreelaceDRC);
        parcel.writeDouble(qtyTreelaceDry);

    }
}
