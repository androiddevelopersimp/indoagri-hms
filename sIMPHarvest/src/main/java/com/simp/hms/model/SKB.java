package com.simp.hms.model;

public class SKB {
	private int rowId;
	private String companyCode;				//p
	private String estate;					//p
	private String block;					//p
	private int barisSkb;				//p
	private String validFrom;				//p
	private String validTo;
	private int barisBlok;				//p
	private double jumlahPokok;
	private double pokokMati;
	private String tanggalTanam;
	private int lineSkb;					//p

	
	public final static String TABLE_NAME = "SKB";
	public static final String ALIAS = "SKB";
	public static final String XML_DOCUMENT = "IT_SKB";
	public final static String XML_ITEM = "ITEM";
	public final static String XML_ID = "ID";
	public final static String XML_COMPANY_CODE = "BUKRS";
	public final static String XML_ESTATE = "ESTATE";
	public final static String XML_BLOCK = "BLOCK";
	public final static String XML_BARIS_SKB = "BARIS_SKB";
	public final static String XML_VALID_FROM = "VALID_FROM";
	public final static String XML_VALID_TO = "VALID_TO";
	public final static String XML_BARIS_BLOCK = "BARIS_BLOCK";
	public final static String XML_JUMLAH_POKOK = "POKOK";
	public final static String XML_POKOK_MATI = "MATI";
	public final static String XML_TANGGAL_TANAM = "TGL_TANAM";
	public final static String XML_LINE_SKB = "LINE_SKB";
	
	public SKB(){}

	public SKB(int rowId, String company_code, String estate, String block, int baris_skb, String valid_from, String valid_to,
			   int baris_blok, double jumlah_pokok, double pokok_mati, String tanggal_tanam, int line_skb) {
		
		this.rowId = rowId;
		this.companyCode = company_code;
		this.estate = estate;
		this.block = block;
		this.barisSkb = baris_skb;
		this.validFrom = valid_from;
		this.validTo = valid_to;
		this.barisBlok = baris_blok;
		this.jumlahPokok = jumlah_pokok;
		this.pokokMati = pokok_mati;
		this.tanggalTanam = tanggal_tanam;
		this.lineSkb = line_skb;
	}

	public int getId() {
		return rowId;
	}

	public void setId(int id) {
		this.rowId = id;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String company_code) {
		this.companyCode = company_code;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public int getBarisSkb() {
		return barisSkb;
	}

	public void setBarisSkb(int baris_skb) {
		this.barisSkb = baris_skb;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String valid_from) {
		this.validFrom = valid_from;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String valid_to) {
		this.validTo = valid_to;
	}

	public int getBarisBlok() {
		return barisBlok;
	}

	public void setBarisBlok(int baris_blok) {
		this.barisBlok = baris_blok;
	}

	public double getJumlahPokok() {
		return jumlahPokok;
	}

	public void setJumlahPokok(double jumlah_pokok) {
		this.jumlahPokok = jumlah_pokok;
	}

	public double getPokokMati() {
		return pokokMati;
	}

	public void setPokokMati(double pokok_mati) {
		this.pokokMati = pokok_mati;
	}

	public String getTanggalTanam() {
		return tanggalTanam;
	}

	public void setTanggalTanam(String tanggal_tanam) {
		this.tanggalTanam = tanggal_tanam;
	}

	public int getLineSkb() {
		return lineSkb;
	}

	public void setLineSkb(int line_skb) {
		this.lineSkb = line_skb;
	}

//	
//	public ContentValues getContentValues(){
//		ContentValues values = new ContentValues();
//		
//		values.put(XML_ID, getId());
//		values.put(XML_COMPANY_CODE, getCompanyCode());
//		values.put(XML_ESTATE, getEstate());
//		values.put(XML_BLOCK, getBlock());
//		values.put(XML_BARIS_SKB, getBarisSkb());
//		values.put(XML_VALID_FROM, getValidFrom());
//		values.put(XML_VALID_TO, getValidTo());
//		values.put(XML_BARIS_BLOK, getBarisBlok());
//		values.put(XML_JUMLAH_POKOK, getJumlahPokok());
//		values.put(XML_POKOK_MATI, getPokokMati());
//		values.put(XML_TANGGAL_TANAM, getTanggalTanam());
//		values.put(XML_LINE_SKB, getLineSkb());
//		values.put(XML_MANDT, getMandt());
//		
//		return values;
//	}
}
