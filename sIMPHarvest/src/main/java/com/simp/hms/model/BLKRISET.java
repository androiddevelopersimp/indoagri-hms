package com.simp.hms.model;

/**
 * Created by Fernando.Siagian on 24/08/2018.
 */

public class BLKRISET {
    private int rowId;
    //private int id;
    private String companyCode;
    private String estate;
    private String block;
    private String status;
    private String startDate;
    private long startDateLong;
    private String endDate;
    private long endDateLong;
    private String description;
    private String refDoc;
    private String refDocDate;
    private String mandt;
    private String createdDate;
    //private String createdBy;
    private String modifiedDate;
    private String modifiedBy;

    public static final String TABLE_NAME = "BLKRISET";
    public static final String ALIAS = "BLKRISET";
    public static final String XML_DOCUMENT = "IT_BLKRISET";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_ID = "ID";
    public static final String XML_COMPANY_CODE = "COMPANY_CODE";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_BLOCK = "BLOCK";
    public static final String XML_STATUS = "STATUS";
    public static final String XML_START_DATE = "START_DATE";
    public static final String XML_START_DATE_LONG = "START_DATE_LONG";
    public static final String XML_END_DATE = "END_DATE";
    public static final String XML_END_DATE_LONG = "END_DATE_LONG";
    public static final String XML_DESCRIPTION = "DESCRIPTION";
    public static final String XML_REF_DOC = "REFDOC";
    public static final String XML_REF_DOC_DATE = "REFDOC_DATE";
    public static final String XML_MANDT = "MANDT";
    public static final String XML_CREATED_DATE = "CREATEDDATE";
    //public static final String XML_CREATED_BY = "CREATEDBY";
    public static final String XML_MODIFIED_DATE = "LASTUPDATEDDATE";
    public static final String XML_MODIFIED_BY = "LASTUPDATEDBY";

    public BLKRISET() {
    }

    public BLKRISET(int rowId, int id, String companyCode, String estate, String block, String status,
                    String startDate, long startDateLong, String endDate, long endDateLong, String description, String refDoc,
                    String refDocDate, String mandt, String createdDate, String modifiedDate, String modifiedBy){
        this.rowId = rowId;
        //this.id = id;
        this.companyCode = companyCode;
        this.estate = estate;
        this.status = status;
        this.startDate = startDate;
        this.startDateLong = startDateLong;
        this.endDate = endDate;
        this.endDateLong = endDateLong;
        this.description = description;
        this.refDoc = refDoc;
        this.refDocDate = refDocDate;
        this.mandt = mandt;
        this.createdDate = createdDate;
        //this.createdBy = createdBy;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int RowId) {
        this.rowId = RowId;
    }

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String company_code) {
        this.companyCode = company_code;
    }

    public String getEstate() { return estate; }

    public void setEstate(String estate) { this.estate = estate; }

    public String getBlock() { return block; }

    public void setBlock (String block) { this.block = block; }

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public String getStartDate() { return  startDate; }

    public void setStartDate(String startDate) { this.startDate = startDate; }

    public long getStartDateLong() { return  startDateLong; }

    public void setStartDateLong(long startDateLong) { this.startDateLong = startDateLong; }

    public String getEndDate() { return  endDate; }

    public void setEndDate(String endDate) { this.endDate = endDate; }

    public long getEndDateLong() { return  endDateLong; }

    public void setEndDateLong(long endDateLong) { this.endDateLong = endDateLong; }

    public String getDescription() { return  description; }

    public void setDescription(String description) { this.description = description; }

    public String getRefDoc() { return  refDoc; }

    public void setRefDoc(String refDoc) { this.refDoc = refDoc ;}

    public String getRefDocDate() {return  refDocDate; }

    public void setRefDocDate(String refDocDate) { this.refDocDate = refDocDate; }

    public String getMandt() { return mandt; }

    public void setMandt(String mandt) { this.mandt = mandt; }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /*public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }*/

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
