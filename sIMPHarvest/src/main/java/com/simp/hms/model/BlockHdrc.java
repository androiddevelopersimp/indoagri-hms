package com.simp.hms.model;

public class BlockHdrc {
	private long rowId;
	private String company_code;		//p
	private String estate;				//p
	private String block;				//p
	private String valid_from;			//p
	private String valid_to;
	private String divisi;				//p
	private String type;
	private String status;
	private String project_definition;
	private String owner;
	private String tgl_tanam;
	private String mandt;				//p
	
	public final static String TABLE_NAME = "BLOCK_HDRC";
	public static final String ALIAS = "BLOCK";
	public static final String XML_DOCUMENT = "IT_HDR";
	public final static String XML_ITEM = "ITEM";
	public final static String XML_ID = "ID";
	public final static String XML_COMPANY_CODE = "COMPANY";
	public final static String XML_ESTATE = "ESTATE";
	public final static String XML_BLOCK = "BLOCK";
	public final static String XML_VALID_FROM = "VALIDFROM";
	public final static String XML_VALID_TO = "VALIDTO";
	public final static String XML_DIVISION = "DIVISI";
	public final static String XML_TYPE = "TYPE";
	public final static String XML_STATUS = "STATUS";
	public final static String XML_PROJECT_DEFINITION = "PROJECT";
	public final static String XML_OWNER = "OWNER";
	public final static String XML_TGL_TANAM = "TGL_TANAM";
	public final static String XML_MANDT = "MANDT";
	
	public BlockHdrc(){}

	public BlockHdrc(long rowId, String company_code, String estate, String block, String valid_from, String valid_to, String divisi, String type,
			String status, String project_definition, String owner, String tgl_tanam, String mandt) {
		super();
		this.rowId = rowId;
		this.company_code = company_code;
		this.estate = estate;
		this.block = block;
		this.valid_from = valid_from;
		this.valid_to = valid_to;
		this.divisi = divisi;
		this.type = type;
		this.status = status;
		this.project_definition = project_definition;
		this.owner = owner;
		this.tgl_tanam = tgl_tanam;
		this.mandt = mandt;
	}



	public long getId() {
		return rowId;
	}

	public void setId(long id) {
		this.rowId = id;
	}

	public String getCompanyCode() {
		return company_code;
	}

	public void setCompanyCode(String company_code) {
		this.company_code = company_code;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getValidFrom() {
		return valid_from;
	}

	public void setValidFrom(String valid_from) {
		this.valid_from = valid_from;
	}

	public String getValidTo() {
		return valid_to;
	}

	public void setValidTo(String valid_to) {
		this.valid_to = valid_to;
	}

	public String getDivisi() {
		return divisi;
	}

	public void setDivisi(String divisi) {
		this.divisi = divisi;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProjectDefinition() {
		return project_definition;
	}

	public void setProjectDefinition(String project_definition) {
		this.project_definition = project_definition;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getTglTanam() {
		return tgl_tanam;
	}

	public void setTglTanam(String tgl_tanam) {
		this.tgl_tanam = tgl_tanam;
	}

	public String getMandt() {
		return mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}
}
