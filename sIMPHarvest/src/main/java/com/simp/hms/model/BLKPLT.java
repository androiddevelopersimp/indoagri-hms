package com.simp.hms.model;

public class BLKPLT {
	private int rowIid;
	private String companyCode; // p
	private String estate; 		// p
	private String block; 		// p
	private String validFrom; 	// p
	private String validTo;
	private String cropType; 	// p
	private String previousCrop;
	private String finishDate;
	private String reference;
	private String jarakTanam;
	private String harvestingDate;
	private double harvested;
	private String planDate;
	private String topography;
	private String soilType;
	private String soilCategory;
	private double prodTrees;

	public static final String TABLE_NAME = "BLKPLT";
	public static final String ALIAS = "BLOCK PLANTING";
	public static final String XML_DOCUMENT = "IT_PLT";
	public static final String XML_ITEM = "ITEM";
	public static final String XML_ID = "ID";
	public static final String XML_COMPANY_CODE = "COMPANY";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_BLOCK = "BLOCK";
	public static final String XML_VALID_FROM = "VALIDFROM";
	public static final String XML_VALID_TO = "VALIDTO";
	public static final String XML_CROP_TYPE = "CROP_TYPE";
	public static final String XML_PREVIOUS_CROP = "PREVIOUS_CROP";
	public static final String XML_FINISH_DATE = "FINISHDATE";
	public static final String XML_REFERENCE = "REFERENCE";
	public static final String XML_JARAK_TANAM = "JARAK_TANAM";
	public static final String XML_HARVESTING_DATE = "HARVESTING_DATE";
	public static final String XML_HARVESTED = "HARVESTED";
	public static final String XML_PLAN_DATE = "PLAN_DATE";
	public static final String XML_TOPOGRAPHY = "TOPOGRAPHY";
	public static final String XML_SOIL_TYPE = "SOIL_TYPE";
	public static final String XML_SOIL_CATEGORY = "SOIL_CATEGORY";
	public static final String XML_PROD_TREES = "PROD_TREES";

	public BLKPLT() {
	}

	public BLKPLT(int rowId, String company_code, String estate, String block,
			String valid_from, String valid_to, String crop_type,
			String previous_crop, String finish_date, String reference,
			String jarak_tanam, String harvesting_date, double harvested,
			String plan_date, String topography, String soil_type,
			String soil_category, double prod_trees) {
		this.rowIid = rowId;
		this.companyCode = company_code;
		this.estate = estate;
		this.block = block;
		this.validFrom = valid_from;
		this.validTo = valid_to;
		this.cropType = crop_type;
		this.previousCrop = previous_crop;
		this.finishDate = finish_date;
		this.reference = reference;
		this.jarakTanam = jarak_tanam;
		this.harvestingDate = harvesting_date;
		this.harvested = harvested;
		this.planDate = plan_date;
		this.topography = topography;
		this.soilType = soil_type;
		this.soilCategory = soil_category;
		this.prodTrees = prod_trees;
	}

	public int getId() {
		return rowIid;
	}

	public void setId(int id) {
		this.rowIid = id;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String company_code) {
		this.companyCode = company_code;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String valid_from) {
		this.validFrom = valid_from;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String valid_to) {
		this.validTo = valid_to;
	}

	public String getCropType() {
		return cropType;
	}

	public void setCropType(String crop_type) {
		this.cropType = crop_type;
	}

	public String getPreviousCrop() {
		return previousCrop;
	}

	public void setPreviousCrop(String previous_crop) {
		this.previousCrop = previous_crop;
	}

	public String getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(String finish_date) {
		this.finishDate = finish_date;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getJarakTanam() {
		return jarakTanam;
	}

	public void setJarakTanam(String jarak_tanam) {
		this.jarakTanam = jarak_tanam;
	}

	public String getHarvestingDate() {
		return harvestingDate;
	}

	public void setHarvestingDate(String harvesting_date) {
		this.harvestingDate = harvesting_date;
	}

	public double getHarvested() {
		return harvested;
	}

	public void setHarvested(double harvested) {
		this.harvested = harvested;
	}

	public String getPlanDate() {
		return planDate;
	}

	public void setPlanDate(String Plan_date) {
		this.planDate = Plan_date;
	}

	public String getTopography() {
		return topography;
	}

	public void setTopography(String topography) {
		this.topography = topography;
	}

	public String getSoilType() {
		return soilType;
	}

	public void setSoilType(String soil) {
		this.soilType = soil;
	}

	public String getSoilCategory() {
		return soilCategory;
	}

	public void setSoilCategory(String soil_category) {
		this.soilCategory = soil_category;
	}

	public double getProdTrees() {
		return prodTrees;
	}

	public void setProdTrees(double prod_trees) {
		this.prodTrees = prod_trees;
	}
}
