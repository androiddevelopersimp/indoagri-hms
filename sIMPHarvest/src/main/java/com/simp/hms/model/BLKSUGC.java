package com.simp.hms.model;

/**
 * Created by Anka.Wirawan on 8/4/2016.
 */
public class BLKSUGC {
    public String companyCode;
    public String estate;
    public String block;
    public String validFrom;
    public String validTo;
    public String phase;
    public double distance;
    public String subDivision;

    public static final String TABLE_NAME = "BLKSUGC";
    public static final String ALIAS = "BLKSUGC";
    public static final String XML_DOCUMENT = "IT_BLKSUGC";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_COMPANY_CODE = "COMPANY_CODE";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_BLOCK = "BLOCK";
    public static final String XML_VALID_FROM = "VALID_FROM";
    public static final String XML_VALID_TO = "VALID_TO";
    public static final String XML_PHASE = "PHASE";
    public static final String XML_DISTANCE = "DISTANCE";
    public static final String XML_SUB_DIVISION = "SUB_DIVISION";

    public BLKSUGC() {
    }

    public BLKSUGC(String companyCode, String estate, String block, String validFrom, String validTo,
                   String phase, double distance, String subDivision) {
        this.companyCode = companyCode;
        this.estate = estate;
        this.block = block;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.phase = phase;
        this.distance = distance;
        this.subDivision = subDivision;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getSubDivision() {
        return subDivision;
    }

    public void setSubDivision(String subDivision) {
        this.subDivision = subDivision;
    }
}
