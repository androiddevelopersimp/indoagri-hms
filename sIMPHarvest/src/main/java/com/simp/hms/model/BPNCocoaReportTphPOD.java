package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BPNCocoaReportTphPOD implements Parcelable{
    private String id;
    private String companyCode;
    private String estate;
    private String division;
    private String tph;
    private long createdDate;

    private double qtyGoodQty;
    private double qtyGoodWeight;
    private double qtyBadQty;
    private double qtyBadWeight;
    private double qtyPoorQty;
    private double qtyPoorWeight;
    private double qtyPODQty;
    private double qtyEstimasiQty;

    public BPNCocoaReportTphPOD(String id, String companyCode, String estate,
                                String division, String tph, long createdDate,
                                double qtyGoodQty, double qtyGoodWeight,
                                double qtyBadQty, double qtyBadWeight,
                                double qtyPoorQty, double qtyPoorWeight,
                                double qtyPODQty, double qtyEstimasiQty) {
        super();
        this.id = id;
        this.companyCode = companyCode;
        this.estate = estate;
        this.division = division;
        this.tph = tph;
        this.createdDate = createdDate;

        this.qtyGoodQty = qtyGoodQty;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadQty = qtyBadQty;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorQty = qtyPoorQty;
        this.qtyPoorWeight = qtyPoorWeight;
        this.qtyPODQty = qtyPODQty;
        this.qtyEstimasiQty = qtyEstimasiQty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getTph() {
        return tph;
    }

    public void setTph(String tph) {
        this.tph = tph;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public double getQtyGoodQty() {
        return qtyGoodQty;
    }

    public void setQtyGoodQty(double qtyGoodQty) {
        this.qtyGoodQty = qtyGoodQty;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadQty() {
        return qtyBadQty;
    }

    public void setQtyBadQty(double qtyBadQty) {
        this.qtyBadQty = qtyBadQty;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorQty() {
        return qtyPoorQty;
    }

    public void setQtyPoorQty(double qtyPoorQty) {
        this.qtyPoorQty = qtyPoorQty;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public double getQtyPODQty() {
        return qtyPODQty;
    }

    public void setQtyPODQty(double qtyPODQty) {
        this.qtyPODQty = qtyPODQty;
    }

    public double getQtyEstimasiQty() {
        return qtyEstimasiQty;
    }

    public void setQtyEstimasiQty(double qtyEstimasiQty) {
        this.qtyEstimasiQty = qtyEstimasiQty;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.companyCode);
        dest.writeString(this.estate);
        dest.writeString(this.division);
        dest.writeString(this.tph);
        dest.writeLong(this.createdDate);
        dest.writeDouble(this.qtyGoodQty);
        dest.writeDouble(this.qtyGoodWeight);
        dest.writeDouble(this.qtyBadQty);
        dest.writeDouble(this.qtyBadWeight);
        dest.writeDouble(this.qtyPoorQty);
        dest.writeDouble(this.qtyPoorWeight);
        dest.writeDouble(this.qtyPODQty);
        dest.writeDouble(this.qtyEstimasiQty);
    }

    protected BPNCocoaReportTphPOD(Parcel in) {
        this.id = in.readString();
        this.companyCode = in.readString();
        this.estate = in.readString();
        this.division = in.readString();
        this.tph = in.readString();
        this.createdDate = in.readLong();
        this.qtyGoodQty = in.readDouble();
        this.qtyGoodWeight = in.readDouble();
        this.qtyBadQty = in.readDouble();
        this.qtyBadWeight = in.readDouble();
        this.qtyPoorQty = in.readDouble();
        this.qtyPoorWeight = in.readDouble();
        this.qtyPODQty = in.readDouble();
        this.qtyEstimasiQty = in.readDouble();
    }

    public static final Creator<BPNCocoaReportTphPOD> CREATOR = new Creator<BPNCocoaReportTphPOD>() {
        @Override
        public BPNCocoaReportTphPOD createFromParcel(Parcel source) {
            return new BPNCocoaReportTphPOD(source);
        }

        @Override
        public BPNCocoaReportTphPOD[] newArray(int size) {
            return new BPNCocoaReportTphPOD[size];
        }
    };
}
