package com.simp.hms.model;

import java.io.Serializable;

public class SPBSWB implements Serializable {
    public static class XML {
        public static final String TABLE_NAME = "SPBS_WB";
        public final static String FILE = "SPBS_WB";
        public final static String DOCUMENT = "ITEM";
        public static final String WB_NUMBER = "WB_NUMBER";
        public static final String SPBS_NUMBER = "SPBS_NUMBER";
        public static final String DRIVER = "DRIVER";
        public static final String LICENSE_PLATE = "LICENSE_PLATE";
        public static final String NETTO = "NETTO";
        public static final String NETWEIGHT = "NETWEIGHT";
        public static final String WB_DATE = "WB_DATE";
        public static final String TRANSACTION_TYPE = "TRANSACTION_TYPE";
        public static final String TIME_IN = "TIME_IN";
        public static final String TIME_OUT = "TIME_OUT";
        public static final String DEDUCTION = "DEDUCTION";
    }

    private String wbNumber;
    private String spbsNumber;
    private String driver;
    private String licensePlate;
    private String netto;
    private String wbDate;
    private String transactionType;
    private String timeIn;
    private String timeOut;
    private String deduction;

    public SPBSWB() {

    }

    public SPBSWB(String wbNumber, String spbsNumber, String driver,
                  String licensePlate, String netto, String wbDate,
                  String transactionType, String timeIn, String timeOut, String deduction) {
        this.wbNumber = wbNumber;
        this.spbsNumber = spbsNumber;
        this.driver = driver;
        this.licensePlate = licensePlate;
        this.netto = netto;
        this.wbDate = wbDate;
        this.transactionType = transactionType;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.deduction = deduction;
    }

    public String getWbNumber() {
        return wbNumber;
    }

    public void setWbNumber(String wbNumber) {
        this.wbNumber = wbNumber;
    }

    public String getSpbsNumber() {
        return spbsNumber;
    }

    public void setSpbsNumber(String spbsNumber) {
        this.spbsNumber = spbsNumber;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getNetto() {
        return netto;
    }

    public void setNetto(String netto) {
        this.netto = netto;
    }

    public String getWbDate() {
        return wbDate;
    }

    public void setWDate(String wb_date) {
        this.wbDate = wb_date;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getDeduction() {
        return deduction;
    }

    public void setDeduction(String deduction) {
        this.deduction = deduction;
    }
}
