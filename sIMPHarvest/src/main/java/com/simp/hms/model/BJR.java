package com.simp.hms.model;

public class BJR {
	private int rowId;
	private String companyCode; 	// p
	private String estate; 			// p
	private String effDDate; 		// p
	private String block; 			// p
	private double bjr;

	public static final String TABLE_NAME = "BJR";
	public static final String ALIAS = "BJR";
	public static final String XML_DOCUMENT = "IT_BJR";
	public static final String XML_ITEM = "ITEM";
	public static final String XML_ID = "ID";
	public static final String XML_COMPANY_CODE = "COMPANY";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XMl_EFF_DATE = "EFF_DATE";
	public static final String XML_BLOCK = "BLOCK";
	public static final String XML_BJR = "BJR";

	public BJR() {
	}

	public BJR(int rowId, String companyCode, String estate, String effDate,
			String block, double bjr) {
		this.rowId = rowId;
		this.companyCode = companyCode;
		this.estate = estate;
		this.effDDate = effDate;
		this.block = block;
		this.bjr = bjr;
	}

	public int getId() {
		return rowId;
	}

	public void setId(int id) {
		this.rowId = id;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getEffDate() {
		return effDDate;
	}

	public void setEffDate(String effDate) {
		this.effDDate = effDate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public double getBjr() {
		return bjr;
	}

	public void setBjr(double bjr) {
		this.bjr = bjr;
	}
}
