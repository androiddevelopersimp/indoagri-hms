package com.simp.hms.model;

public class SPULineSummaryPODS {
    int id = 1;
    String tph = "";
    String bpnId = "";
    String bpnDate = "";
    String spbsRef = "";
    String spbsNext = "";
    double qtyPODS = 0;
    double qtyGoodWeight = 0;
    double qtyBadWeight = 0;
    double qtyPoorWeight = 0;
    String harvester = "";
    String gpsKoordinat = "0.0:0.0";
    int isSave = 0;

    public SPULineSummaryPODS(int id, String tph, String bpnId, String bpnDate, String spbsRef, String spbsNext,double qtyPODS,
                              double qtyGoodWeight, double qtyBadWeight, double qtyPoorWeight,
                              String gpsKoordinat, int isSave, String harvester) {
        super();
        this.id = id;
        this.tph = tph;
        this.bpnId = bpnId;
        this.bpnDate = bpnDate;
        this.spbsRef = spbsRef;
        this.spbsNext = spbsNext;
        this.qtyPODS = qtyPODS;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorWeight = qtyPoorWeight;
        this.gpsKoordinat = gpsKoordinat;
        this.isSave = isSave;
        this.harvester = harvester;
    }

    public double getQtyPODS() {
        return qtyPODS;
    }

    public void setQtyPODS(double qtyPODS) {
        this.qtyPODS = qtyPODS;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTph() {
        return tph;
    }

    public void setTph(String tph) {
        this.tph = tph;
    }

    public String getBpnId() {
        return bpnId;
    }

    public void setBpnId(String bpnId) {
        this.bpnId = bpnId;
    }

    public String getBpnDate() {
        return bpnDate;
    }

    public void setBpnDate(String bpnDate) {
        this.bpnDate = bpnDate;
    }

    public String getSpbsRef() {
        return spbsRef;
    }

    public void setSpbsRef(String spbsRef) {
        this.spbsRef = spbsRef;
    }

    public String getSpbsNext() {
        return spbsNext;
    }

    public void setSpbsNext(String spbsNext) {
        this.spbsNext = spbsNext;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public String getGpsKoordinat() {
        return gpsKoordinat;
    }

    public void setGpsKoordinat(String gpsKoordinat) {
        this.gpsKoordinat = gpsKoordinat;
    }

    public int getIsSave() {
        return isSave;
    }

    public void setIsSave(int isSave) {
        this.isSave = isSave;
    }

    public String getHarvester() {
        return harvester;
    }

    public void setHarvester(String harvester) {
        this.harvester = harvester;
    }
}
