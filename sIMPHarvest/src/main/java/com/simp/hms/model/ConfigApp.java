package com.simp.hms.model;

public class ConfigApp {
    private String companyCode;		//p
    private String estate;		//p
    private String companyName;
    private String estateName;

    public final static String TABLE_NAME = "CONFIG_APP";
    public static final String ALIAS= "CONFIG_APP";
    public static final String XML_DOCUMENT = "IT_CONFIGAPP";
    public static final String XML_ITEM = "ITEM";
    public static final String XML_COMPANY_CODE = "COMPANYCODE";
    public static final String XML_COMPANY_NAME = "COMPANYNAME";
    public static final String XML_ESTATE = "ESTATE";
    public static final String XML_ESTATE_NAME = "ESTATENAME";


    public ConfigApp(){}

    public ConfigApp(String companyCode, String companyName,
                     String estate, String estateName) {
        super();
        this.companyCode = companyCode;
        this.companyName = companyName;
        this.estate = estate;
        this.estateName = estateName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getEstateName() {
        return estateName;
    }

    public void setEstateName(String estateName) {
        this.estateName = estateName;
    }
}
