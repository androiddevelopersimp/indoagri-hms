package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BPNCocoaReportBlockPOD implements Parcelable {
    private String companyCode;
    private String estate;
    private String division;
    private String gang;
    private String block;

    private double qtyGoodQty;

    private double qtyGoodWeight;
    private double qtyBadQty;
    private double qtyBadWeight;
    private double qtyPoorQty;
    private double qtyPoorWeight;
    private double qtyPODQty;
    private double qtyEstimasiQty;

    public static final String TABLE_NAME = "BPN_REPORT_HARVESTER";

    public BPNCocoaReportBlockPOD(String companyCode, String estate, String division, String gang, String block,
                                  double qtyGoodQty, double qtyGoodWeight,
                                  double qtyBadQty, double qtyBadWeight,
                                  double qtyPoorQty, double qtyPoorWeight,
                                  double qtyPODQty, double qtyEstimasiQty) {
        super();
        this.companyCode = companyCode;
        this.estate = estate;
        this.division = division;
        this.gang = gang;
        this.block = block;

        this.qtyGoodQty = qtyGoodQty;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadQty = qtyBadQty;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorQty = qtyPoorQty;
        this.qtyPoorWeight = qtyPoorWeight;
        this.qtyPODQty = qtyPODQty;
        this.qtyEstimasiQty = qtyEstimasiQty;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getGang() {
        return gang;
    }

    public void setGang(String gang) {
        this.gang = gang;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public double getQtyGoodQty() {
        return qtyGoodQty;
    }

    public void setQtyGoodQty(double qtyGoodQty) {
        this.qtyGoodQty = qtyGoodQty;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadQty() {
        return qtyBadQty;
    }

    public void setQtyBadQty(double qtyBadQty) {
        this.qtyBadQty = qtyBadQty;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorQty() {
        return qtyPoorQty;
    }

    public void setQtyPoorQty(double qtyPoorQty) {
        this.qtyPoorQty = qtyPoorQty;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public double getQtyPODQty() {
        return qtyPODQty;
    }

    public void setQtyPODQty(double qtyPODQty) {
        this.qtyPODQty = qtyPODQty;
    }

    public double getQtyEstimasiQty() {
        return qtyEstimasiQty;
    }

    public void setQtyEstimasiQty(double qtyEstimasiQty) {
        this.qtyEstimasiQty = qtyEstimasiQty;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.companyCode);
        dest.writeString(this.estate);
        dest.writeString(this.division);
        dest.writeString(this.gang);
        dest.writeString(this.block);
        dest.writeDouble(this.qtyGoodQty);
        dest.writeDouble(this.qtyGoodWeight);
        dest.writeDouble(this.qtyBadQty);
        dest.writeDouble(this.qtyBadWeight);
        dest.writeDouble(this.qtyPoorQty);
        dest.writeDouble(this.qtyPoorWeight);
        dest.writeDouble(this.qtyPODQty);
        dest.writeDouble(this.qtyEstimasiQty);
    }

    protected BPNCocoaReportBlockPOD(Parcel in) {
        this.companyCode = in.readString();
        this.estate = in.readString();
        this.division = in.readString();
        this.gang = in.readString();
        this.block = in.readString();
        this.qtyGoodQty = in.readDouble();
        this.qtyGoodWeight = in.readDouble();
        this.qtyBadQty = in.readDouble();
        this.qtyBadWeight = in.readDouble();
        this.qtyPoorQty = in.readDouble();
        this.qtyPoorWeight = in.readDouble();
        this.qtyPODQty = in.readDouble();
        this.qtyEstimasiQty = in.readDouble();
    }

    public static final Creator<BPNCocoaReportBlockPOD> CREATOR = new Creator<BPNCocoaReportBlockPOD>() {
        @Override
        public BPNCocoaReportBlockPOD createFromParcel(Parcel source) {
            return new BPNCocoaReportBlockPOD(source);
        }

        @Override
        public BPNCocoaReportBlockPOD[] newArray(int size) {
            return new BPNCocoaReportBlockPOD[size];
        }
    };
}
