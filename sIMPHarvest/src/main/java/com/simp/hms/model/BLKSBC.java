package com.simp.hms.model;

public class BLKSBC {
	private int rowId;
	private String companyCode;			//p
	private String estate;				//p
	private String block;				//p
	private String validFrom;			//p
	private double basisNormal;
	private double basisFriday;
	private double basisHoliday;
	private double premiNormal;
	private double premiFriday;
	private double premiHoliday;
	private String priority;
	
	public static final String TABLE_NAME = "BLKSBC";
	public static final String ALIAS = "BLOCK BASIC";
	public static final String XML_DOCUMENT = "IT_BLK";
	public static final String XML_ID = "ID";
	public static final String XML_ITEM = "ITEM";
	public static final String XML_COMPANY_CODE = "COMPANY";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_BLOCK = "BLOCK";
	public static final String XML_VALID_FROM = "VALIDFROM";
	public static final String XML_BASIS_NORMAL = "BASIS_NORMAL";
	public static final String XML_BASIS_FRIDAY = "BASIS_FRIDAY";
	public static final String XML_BASIS_HOLIDAY = "BASIS_HOLIDAY";
	public static final String XML_PREMI_NORMAL = "PREMI_NORMAL";
	public static final String XML_PREMI_FRIDAY = "PREMI_FRIDAY";
	public static final String XML_PREMI_HOLIDAY = "PREMI_HOLIDAY";
	public static final String XML_PRIORITY = "PRIORITY";
	
	public BLKSBC(){}

	public BLKSBC(int rowId, String company_code, String estate, String block, String valid_from, double basis_normal,
			double basis_friday, double basis_holiday, double premi_normal, double premi_friday, double premi_holiday, String priority) {
		this.rowId = rowId;
		this.companyCode = company_code;
		this.estate = estate;
		this.block = block;
		this.validFrom = valid_from;
		this.basisNormal = basis_normal;
		this.basisFriday = basis_friday;
		this.basisHoliday = basis_holiday;
		this.premiNormal = premi_normal;
		this.premiFriday = premi_friday;
		this.premiHoliday = premi_holiday;
		this.priority = priority;
	}

	public int getId() {
		return rowId;
	}

	public void setId(int id) {
		this.rowId = id;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String company_code) {
		this.companyCode = company_code;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String valid_from) {
		this.validFrom = valid_from;
	}

	public double getBasisNormal() {
		return basisNormal;
	}

	public void setBasisNormal(double basis_normal) {
		this.basisNormal = basis_normal;
	}

	public double getBasisFriday() {
		return basisFriday;
	}

	public void setBasisFriday(double basis_friday) {
		this.basisFriday = basis_friday;
	}

	public double getBasisHoliday() {
		return basisHoliday;
	}

	public void setBasisHoliday(double basis_holiday) {
		this.basisHoliday = basis_holiday;
	}

	public double getPremiNormal() {
		return premiNormal;
	}

	public void setPremiNormal(double premi_normal) {
		this.premiNormal = premi_normal;
	}

	public double getPremiFriday() {
		return premiFriday;
	}

	public void setPremiFriday(double premi_friday) {
		this.premiFriday = premi_friday;
	}

	public double getPremiHoliday() {
		return premiHoliday;
	}

	public void setPremiHoliday(double premi_holiday) {
		this.premiHoliday = premi_holiday;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
}
