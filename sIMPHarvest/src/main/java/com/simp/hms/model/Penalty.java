package com.simp.hms.model;

public class Penalty {
	private String penaltyCode;
	private String penaltyDesc;
	private String uom;
	private String cropType;
	private int isLoading;
	private int isAncak;
	
	public static final String TABLE_NAME = "PENALTY";
	public static final String ALIAS = "PENALTY";
	public static final String XML_DOCUMENT = "IT_PENALTY";
	public static final String XML_ITEM = "ITEM";
	public static final String XML_PENALTY_CODE = "PENALTY_CODE";
	public static final String XML_PENALTY_DESC = "PENALTY_DESC";
	public static final String XML_UOM = "UOM";
	public static final String XMl_CROP_TYPE = "CROP_TYPE";
	public static final String XML_IS_LOADING = "ISLOADING";
	public static final String XML_IS_ANCAK = "ISANCAK";
	
	public Penalty(){}
		
	public Penalty(String penaltyCode, String penaltyDesc, String uom, String cropType, int isLoading, int isAncak) {
		super();
		this.penaltyCode = penaltyCode;
		this.penaltyDesc = penaltyDesc;
		this.uom = uom;
		this.cropType = cropType;
		this.isLoading = isLoading;
		this.isAncak = isAncak;
	}

	public String getPenaltyCode() {
		return penaltyCode;
	}

	public void setPenaltyCode(String penaltyCode) {
		this.penaltyCode = penaltyCode;
	}

	public String getPenaltyDesc() {
		return penaltyDesc;
	}

	public void setPenaltyDesc(String penaltyDesc) {
		this.penaltyDesc = penaltyDesc;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getCropType() {
		return cropType;
	}

	public void setCropType(String cropType) {
		this.cropType = cropType;
	}

	public int getIsLoading() {
		return isLoading;
	}

	public void setIsLoading(int isLoading) {
		this.isLoading = isLoading;
	}

	public int getIsAncak() {
		return isAncak;
	}

	public void setIsAncak(int isAncak) {
		this.isAncak = isAncak;
	}
}
