package com.simp.hms.model;

public class UserLogin extends Employee{

	public final static String TABLE_NAME = "USER_LOGIN";

	public UserLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserLogin(int rowId, String company_code, String estate, int fiscal_year, int fiscal_period, String nik, String name,
			String termination_date, String divisi, String role_id, String job_pos, String gang_code, String cost_center,
			String emp_type, String valid_from, String harvester_code) {
		
		super(rowId, company_code, estate, fiscal_year, fiscal_period, nik, name,
				termination_date, divisi, role_id, job_pos, gang_code, cost_center,
				emp_type, valid_from, harvester_code);
	}
	
	
}
