package com.simp.hms.model;

public class BPNQuantity {
	private long rowId = 0;
	private String bpnId = "";					// p
	private String imei = ""; 				// p
	private String companyCode = ""; 		// p
	private String estate = ""; 			// p
	private String bpnDate = ""; 			// p
	private String division = ""; 			// p
	private String gang = ""; 				// p
	private String location = ""; 			// p
	private String tph = ""; 				// p
	private String nikHarvester = ""; 		// p
	private String crop = ""; 				// p
	private String achievementCode = ""; 	// p
	private double quantity = 0;
	private double quantityremaining = 0;
	private int status = 0;
	private long createdDate = 0;
	private String createdBy = "";
	private long modifiedDate = 0;
	private String modifiedBy = "";
	private String modifiedDateStr = "";

	public final static String TABLE_NAME = "BPN_QUANTITY";
	public final static String XML_FILE = "IT_BPN";
	public final static String XML_DOCUMENT = "BPN_QUANTITY";
	public final static String XML_BPN_ID = "ID";
	public final static String XML_IMEI = "IMEI";
	public final static String XML_COMPANY_CODE = "COMPANY_CODE";
	public final static String XML_ESTATE = "ESTATE";
	public final static String XML_BPN_DATE = "BPN_DATE";
	public final static String XML_HARVEST_DATE = "HARVEST_DATE";
	public final static String XML_DIVISION = "DIVISION";
	public final static String XML_GANG = "GANG";
	public final static String XML_LOCATION = "LOCATION";
	public final static String XML_TPH = "TPH";
	public final static String XML_NIK_HARVESTER = "NIK_HARVESTER";
	public final static String XML_CROP = "CROP";
	public final static String XML_ACHIEVEMENT_CODE = "ACHIEVEMENT_CODE";
	public final static String XML_QUANTITY = "QUANTITY";
	public final static String XML_QUANTITY_REMAINING = "QUANTITY_REMAINING";
	public final static String XML_STATUS = "STATUS";
	public final static String XML_CREATED_DATE = "CREATED_DATE";
	public final static String XML_CREATED_BY = "CREATED_BY";
	public final static String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public final static String XML_MODIFIED_BY = "MODIFIED_BY";
	public final static String XML_MODIFIED_DATE_STR = "MODIFIED_DATE_STR";

	public final static String XML_DOCUMENT_RESTORE = "BPN_ACHIEVEMENT";
	public final static String XML_ITEM_RESTORE = "BPN_ACHIEVEMENT";
	public final static String XML_ACHIEVEMENT_CODE_RESTORE = "ACHIEVEMENTCODE";
	public final static String XML_QUANTITY_RESTORE = "QTY";

	public final static String JANJANG_CODE = "01";
	public final static String LOOSE_FRUIT_CODE = "03";


	//Karet
	public final static String LATEX_WET_CODE = "07";
	public final static String LATEX_DRC_CODE = "08";
	public final static String LUMP_WET_CODE  = "10";
	public final static String LUMP_DRC_CODE = "11";
	public final static String SLAB_WET_CODE = "13";
	public final static String SLAB_DRC_CODE = "14";
	public final static String TREELACE_WET_CODE = "36";
	public final static String TREELACE_DRC_CODE = "37";


	//Cocoa
	public final static  String POD_QTY_CODE = "93";
	public final static  String GOOD_WEIGHT_CODE = "76";
	public final static  String POOR_WEIGHT_CODE = "77";
	public final static  String BAD_WEIGHT_CODE = "78";

	public final static  String GOOD_QTY_CODE = "89";
	public final static  String POOR_QTY_CODE = "90";
	public final static  String BAD_QTY_CODE = "91";

	public BPNQuantity() {
	}

	public BPNQuantity(long rowId, String bpnId, String imei, String companyCode,
					   String estate, String bpnDate, String division, String gang,
					   String location, String tph, String nikHarvester, String crop,
					   String achievementCode, double quantity, double quantityremaining, int status,
					   long createdDate, String createdBy, long modifiedDate,
					   String modifiedBy, String modifiedDateStr) {
		super();
		this.rowId = rowId;
		this.bpnId = bpnId;
		this.imei = imei;
		this.companyCode = companyCode;
		this.estate = estate;
		this.bpnDate = bpnDate;
		this.division = division;
		this.gang = gang;
		this.location = location;
		this.tph = tph;
		this.nikHarvester = nikHarvester;
		this.crop = crop;
		this.achievementCode = achievementCode;
		this.quantity = quantity;
		this.quantityremaining = quantityremaining;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDateStr = modifiedDateStr;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public String getBpnId() {
		return bpnId;
	}

	public void setBpnId(String BpnId) {
		this.bpnId = BpnId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBpnDate() {
		return bpnDate;
	}

	public void setBpnDate(String bpnDate) {
		this.bpnDate = bpnDate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTph() {
		return tph;
	}

	public void setTph(String tph) {
		this.tph = tph;
	}

	public String getNikHarvester() {
		return nikHarvester;
	}

	public void setNikHarvester(String nikHarvester) {
		this.nikHarvester = nikHarvester;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getAchievementCode() {
		return achievementCode;
	}

	public void setAchievementCode(String achievementCode) {
		this.achievementCode = achievementCode;
	}

	public double getQuantity() { return quantity; }

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getQuantityRemaining() {
		return quantityremaining;
	}

	public void setQuantityRemaining(double quantityremaining) { this.quantityremaining = quantityremaining; }

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDateStr() {
		return modifiedDateStr;
	}

	public void setModifiedDateStr(String modifiedDateStr) {
		this.modifiedDateStr = modifiedDateStr;
	}
}
