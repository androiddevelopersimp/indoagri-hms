package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BPNHeader implements Parcelable {
	private long rowId = 0;
	private String bpnId = ""; 			// p
	private String imei = ""; 			// p
	private String companyCode = ""; 	// p
	private String estate = ""; 		// p
	private String bpnDate = ""; 		// p
	private String division = ""; 		// p
	private String gang = ""; 			// p
	private String location = ""; 		// p
	private String tph = ""; 			// p
	private String nikHarvester = ""; 	// p
	private String harvester = "";
	private String nikForeman = "";
	private String foreman = "";
	private String nikClerk = "";
	private String clerk = "";
	private int useGerdang = 0;
	private String crop = "";
	private String gpsKoordinat = "0.0:0.0";
	private String photo = "";
	private int status = 0;
	private String spbsNumber = "";
	private long createdDate = 0;
	private String createdBy = "";
	private long modifiedDate = 0;
	private String modifiedBy = "";

	public final static String TABLE_NAME = "BPN_HEADER";
	public final static String XML_FILE = "IT_BPN";
	public final static String XML_DOCUMENT = "BPN_HEADER";
	public final static String XML_BPN_ID = "BPN_ID";
	public final static String XML_IMEI = "IMEI";
	public final static String XML_COMPANY_CODE = "COMPANY_CODE";
	public final static String XML_ESTATE = "ESTATE";
	public final static String XML_BPN_DATE = "BPN_DATE";
	public final static String XML_HARVEST_DATE = "HARVEST_DATE";
	public final static String XML_DIVISION = "DIVISION";
	public final static String XML_GANG = "GANG";
	public final static String XML_LOCATION = "LOCATION";
	public final static String XML_TPH = "TPH";
	public final static String XML_NIK_HARVESTER = "NIK_HARVESTER";
	public final static String XML_HARVESTER = "HARVESTER";
	public final static String XML_NIK_FOREMAN = "NIK_FOREMAN";
	public final static String XML_FOREMAN = "FOREMAN";
	public final static String XML_NIK_CLERK = "NIK_CLERK";
	public final static String XML_CLERK = "CLERK";
	public final static String XML_USE_GERDANG = "USE_GERDANG";
	public final static String XML_CROP = "CROP";
	public final static String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
	public final static String XML_PHOTO = "PHOTO";
	public final static String XML_STATUS = "STATUS";
	public final static String XML_SPBS_NUMBER = "SPBS_NUMBER";
	public final static String XML_CREATED_DATE = "CREATED_DATE";
	public final static String XML_CREATED_BY = "CREATED_BY";
	public final static String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public final static String XML_MODIFIED_BY = "MODIFIED_BY";

	public final static String XML_DOCUMENT_RESTORE = "BUKU_PANEN";
	public final static String XML_ITEM_RESTORE = "BPHEADER";
	public final static String XML_ID_RESTORE = "ID";
	public final static String XML_COMPANY_CODE_RESTORE = "COMPANY";
	public final static String XML_ESTATE_RESTORE = "ESTATE";
	public final static String XML_BPN_DATE_RESTORE = "HARVEST_DATE";
	public final static String XML_DIVISION_RESTORE = "DIVISION";
	public final static String XML_GANG_RESTORE = "GANG";
	public final static String XML_FOREMAN_RESTORE = "FOREMAN";
	public final static String XML_NIK_FOREMAN_RESTORE = "NIK_FOREMAN";
	public final static String XML_CLERK_RESTORE = "CLERK";
	public final static String XML_NIK_CLERK_RESTORE = "NIK_CLERK";
	public final static String XML_HARVERSTER_RESTORE = "HARVESTER";
	public final static String XML_NIK_HARVESTER_RESTORE = "NIK_HARVESTER";
	public final static String XML_LOCATION_RESTORE = "LOCATION";
	public final static String XML_TPH_RESTORE = "TPH";
	public final static String XML_CROP_RESTORE = "CROP";
	public final static String XML_USE_GERDANG_RESTORE = "USE_GERDANG";
	public final static String XML_GPS_KOORDINAT_RESTORE = "GPS_KOORDINAT";
	public final static String XML_PHOTO_RESTORE = "PHOTO";
	public final static String XML_IMEI_RESTORE = "IMEI";
	public final static String XML_CREATED_DATE_RESTORE = "CREATED_DATE";
	public final static String XML_CREATED_BY_RESTORE = "CREATED_BY";

	public BPNHeader() {
	}

	public BPNHeader(long rowId, String bpnId, String imei, String companyCode,
			String estate, String bpnDate, String division, String gang,
			String location, String tph, String nikHarvester, String harvester,
			String nikForeman, String foreman, String nikClerk, String clerk,
			int useGerdang, String crop, String gpsKoordinat, String photo, int status,
			String spbsNumber, long createdDate, String createdBy,
			long modifiedDate, String modifiedBy) {
		super();
		this.rowId = rowId;
		this.bpnId = bpnId;
		this.imei = imei;
		this.companyCode = companyCode;
		this.estate = estate;
		this.bpnDate = bpnDate;
		this.division = division;
		this.gang = gang;
		this.location = location;
		this.tph = tph;
		this.nikHarvester = nikHarvester;
		this.harvester = harvester;
		this.nikForeman = nikForeman;
		this.foreman = foreman;
		this.nikClerk = nikClerk;
		this.clerk = clerk;
		this.useGerdang = useGerdang;
		this.crop = crop;
		this.gpsKoordinat = gpsKoordinat;
		this.photo = photo;
		this.status = status;
		this.spbsNumber = spbsNumber;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public String getBpnId() {
		return bpnId;
	}

	public void setBpnId(String bpnId) {
		this.bpnId = bpnId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBpnDate() {
		return bpnDate;
	}

	public void setBpnDate(String bpnDate) {
		this.bpnDate = bpnDate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTph() {
		return tph;
	}

	public void setTph(String tph) {
		this.tph = tph;
	}

	public String getNikHarvester() {
		return nikHarvester;
	}

	public void setNikHarvester(String nikHarvester) {
		this.nikHarvester = nikHarvester;
	}

	public String getHarvester() {
		return harvester;
	}

	public void setHarvester(String harvester) {
		this.harvester = harvester;
	}

	public String getNikForeman() {
		return nikForeman;
	}

	public void setNikForeman(String nikForeman) {
		this.nikForeman = nikForeman;
	}

	public String getForeman() {
		return foreman;
	}

	public void setForeman(String foreman) {
		this.foreman = foreman;
	}

	public String getNikClerk() {
		return nikClerk;
	}

	public void setNikClerk(String nikClerk) {
		this.nikClerk = nikClerk;
	}

	public String getClerk() {
		return clerk;
	}

	public void setClerk(String clerk) {
		this.clerk = clerk;
	}

	public int isUseGerdang() {
		return useGerdang;
	}

	public void setUseGerdang(int useGerdang) {
		this.useGerdang = useGerdang;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getGpsKoordinat() {
		return gpsKoordinat;
	}

	public void setGpsKoordinat(String gpsKoordinat) {
		this.gpsKoordinat = gpsKoordinat;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getSpbsNumber() {
		return spbsNumber;
	}

	public void setSpbsNumber(String spbsNumber) {
		this.spbsNumber = spbsNumber;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public static final Parcelable.Creator<BPNHeader> CREATOR = new Creator<BPNHeader>() {

		@Override
		public BPNHeader createFromParcel(Parcel parcel) {
			BPNHeader bpnHeader = new BPNHeader(parcel.readLong(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readInt(), 
					parcel.readString(), parcel.readString(), 
					parcel.readString(), parcel.readInt(), 
					parcel.readString(), parcel.readLong(),
					parcel.readString(), parcel.readLong(), 
					parcel.readString());

			return bpnHeader;
		}

		@Override
		public BPNHeader[] newArray(int size) {
			return new BPNHeader[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeLong(rowId);
		parcel.writeString(bpnId);
		parcel.writeString(imei);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(bpnDate);
		parcel.writeString(division);
		parcel.writeString(gang);
		parcel.writeString(location);
		parcel.writeString(tph);
		parcel.writeString(nikHarvester);
		parcel.writeString(harvester);
		parcel.writeString(nikForeman);
		parcel.writeString(foreman);
		parcel.writeString(nikClerk);
		parcel.writeString(clerk);
		parcel.writeInt(useGerdang);
		parcel.writeString(crop);
		parcel.writeString(gpsKoordinat);
		parcel.writeString(photo);
		parcel.writeInt(status);
		parcel.writeString(spbsNumber);
		parcel.writeLong(createdDate);
		parcel.writeString(createdBy);
		parcel.writeLong(modifiedDate);
		parcel.writeString(modifiedBy);
	}
}
