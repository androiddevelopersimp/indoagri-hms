package com.simp.hms.model;

public class SPUPrint {
    private String block;
    private String bpnDate;
    private double qtyGoodWeight;
    private double qtyBadWeight;
    private double qtyPoorWeight;
    private double qtyTotalWeight;

    public SPUPrint(String block, String bpnDate,
                    double qtyGoodWeight, double qtyBadWeight, double qtyPoorWeight, double qtyTotalWeight) {
        super();
        this.block = block;
        this.bpnDate = bpnDate;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorWeight = qtyPoorWeight;
        this.qtyTotalWeight = qtyTotalWeight;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getBpnDate() {
        return bpnDate;
    }

    public void setBpnDate(String bpnDate) {
        this.bpnDate = bpnDate;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public double getQtyTotalWeight() {
        return qtyTotalWeight;
    }

    public void setQtyTotalWeight(double qtyTotalWeight) {
        this.qtyTotalWeight = qtyTotalWeight;
    }
}
