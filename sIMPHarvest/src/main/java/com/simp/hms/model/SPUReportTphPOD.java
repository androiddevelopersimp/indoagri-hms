package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SPUReportTphPOD implements Parcelable {
    private String year;
    private String companyCode;
    private String estate;
    private String crop;
    private String spuNumber;
    private String spuDate;
    private String tph;
    private double qtyGoodQty;
    private double qtyGoodWeight;
    private double qtyBadQty;
    private double qtyBadWeight;
    private double qtyPoorQty;
    private double qtyPoorWeight;
    private double qtyPODQty;
    private double qtyEstimasiQty;

    public static final String TABLE_NAME = "SPU_REPORT_TPH";

    public SPUReportTphPOD(String year, String companyCode, String estate, String crop,
                           String spuNumber, String spuDate, String tph,
                           double qtyGoodQty, double qtyGoodWeight,
                           double qtyBadQty, double qtyBadWeight,
                           double qtyPoorQty, double qtyPoorWeight,
                           double qtyPODQty, double qtyEstimasiQty) {
        super();
        this.year = year;
        this.companyCode = companyCode;
        this.estate = estate;
        this.crop = crop;
        this.spuNumber = spuNumber;
        this.spuDate = spuDate;
        this.tph = tph;
        this.qtyGoodQty = qtyGoodQty;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadQty = qtyBadQty;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorQty = qtyPoorQty;
        this.qtyPoorWeight = qtyPoorWeight;
        this.qtyPODQty = qtyPODQty;
        this.qtyEstimasiQty = qtyEstimasiQty;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public String getSpuNumber() {
        return spuNumber;
    }

    public void setSpuNumber(String spuNumber) {
        this.spuNumber = spuNumber;
    }

    public String getSpuDate() {
        return spuDate;
    }

    public void setSpuDate(String spuDate) {
        this.spuDate = spuDate;
    }

    public String getTph() {
        return tph;
    }

    public void setTph(String tph) {
        this.tph = tph;
    }

    public double getQtyGoodQty() {
        return qtyGoodQty;
    }

    public void setQtyGoodQty(double qtyGoodQty) {
        this.qtyGoodQty = qtyGoodQty;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadQty() {
        return qtyBadQty;
    }

    public void setQtyBadQty(double qtyBadQty) {
        this.qtyBadQty = qtyBadQty;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorQty() {
        return qtyPoorQty;
    }

    public void setQtyPoorQty(double qtyPoorQty) {
        this.qtyPoorQty = qtyPoorQty;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public double getQtyPODQty() {
        return qtyPODQty;
    }

    public void setQtyPODQty(double qtyPODQty) {
        this.qtyPODQty = qtyPODQty;
    }

    public double getQtyEstimasiQty() {
        return qtyEstimasiQty;
    }

    public void setQtyEstimasiQty(double qtyEstimasiQty) {
        this.qtyEstimasiQty = qtyEstimasiQty;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.year);
        dest.writeString(this.companyCode);
        dest.writeString(this.estate);
        dest.writeString(this.crop);
        dest.writeString(this.spuNumber);
        dest.writeString(this.spuDate);
        dest.writeString(this.tph);
        dest.writeDouble(this.qtyGoodQty);
        dest.writeDouble(this.qtyGoodWeight);
        dest.writeDouble(this.qtyBadQty);
        dest.writeDouble(this.qtyBadWeight);
        dest.writeDouble(this.qtyPoorQty);
        dest.writeDouble(this.qtyPoorWeight);
        dest.writeDouble(this.qtyPODQty);
        dest.writeDouble(this.qtyEstimasiQty);
    }

    protected SPUReportTphPOD(Parcel in) {
        this.year = in.readString();
        this.companyCode = in.readString();
        this.estate = in.readString();
        this.crop = in.readString();
        this.spuNumber = in.readString();
        this.spuDate = in.readString();
        this.tph = in.readString();
        this.qtyGoodQty = in.readDouble();
        this.qtyGoodWeight = in.readDouble();
        this.qtyBadQty = in.readDouble();
        this.qtyBadWeight = in.readDouble();
        this.qtyPoorQty = in.readDouble();
        this.qtyPoorWeight = in.readDouble();
        this.qtyPODQty = in.readDouble();
        this.qtyEstimasiQty = in.readDouble();
    }

    public static final Creator<SPUReportTphPOD> CREATOR = new Creator<SPUReportTphPOD>() {
        @Override
        public SPUReportTphPOD createFromParcel(Parcel source) {
            return new SPUReportTphPOD(source);
        }

        @Override
        public SPUReportTphPOD[] newArray(int size) {
            return new SPUReportTphPOD[size];
        }
    };
}
