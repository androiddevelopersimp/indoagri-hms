package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BPNCocoaReportTph implements Parcelable{
    private String id;
    private String companyCode;
    private String estate;
    private String division;
    private String tph;
    private long createdDate;

    private double qtyGoodQty;
    private double qtyGoodWeight;
    private double qtyBadQty;
    private double qtyBadWeight;
    private double qtyPoorQty;
    private double qtyPoorWeight;

    public BPNCocoaReportTph(String id, String companyCode, String estate,
                             String division, String tph, long createdDate,
                             double qtyGoodQty, double qtyGoodWeight,
                             double qtyBadQty, double qtyBadWeight,
                             double qtyPoorQty, double qtyPoorWeight) {
        super();
        this.id = id;
        this.companyCode = companyCode;
        this.estate = estate;
        this.division = division;
        this.tph = tph;
        this.createdDate = createdDate;

        this.qtyGoodQty = qtyGoodQty;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadQty = qtyBadQty;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorQty = qtyPoorQty;
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getTph() {
        return tph;
    }

    public void setTph(String tph) {
        this.tph = tph;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public double getQtyGoodQty() {
        return qtyGoodQty;
    }

    public void setQtyGoodQty(double qtyGoodQty) {
        this.qtyGoodQty = qtyGoodQty;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadQty() {
        return qtyBadQty;
    }

    public void setQtyBadQty(double qtyBadQty) {
        this.qtyBadQty = qtyBadQty;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyGoodWeight = qtyBadWeight;
    }

    public double getQtyPoorQty() {
        return qtyPoorQty;
    }

    public void setQtyPoorQty(double qtyPoorQty) {
        this.qtyPoorQty = qtyPoorQty;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public static final Parcelable.Creator<BPNCocoaReportTph> CREATOR = new Parcelable.Creator<BPNCocoaReportTph>() {

        @Override
        public BPNCocoaReportTph createFromParcel(Parcel parcel) {
            BPNCocoaReportTph bpnHeader = new BPNCocoaReportTph(parcel.readString(), parcel.readString(),
                    parcel.readString(), parcel.readString(), parcel.readString(), parcel.readLong(),
                    parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble());

            return bpnHeader;
        }

        @Override
        public BPNCocoaReportTph[] newArray(int size) {
            return new BPNCocoaReportTph[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(companyCode);
        parcel.writeString(estate);
        parcel.writeString(division);
        parcel.writeString(tph);
        parcel.writeLong(createdDate);

        parcel.writeDouble(qtyGoodQty);
        parcel.writeDouble(qtyGoodWeight);
        parcel.writeDouble(qtyBadQty);
        parcel.writeDouble(qtyBadWeight);
        parcel.writeDouble(qtyPoorQty);
        parcel.writeDouble(qtyPoorWeight);
    }

}
