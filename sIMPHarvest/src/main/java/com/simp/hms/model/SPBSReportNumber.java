package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SPBSReportNumber implements Parcelable {
	private String year;
	private String companyCode;
	private String estate;
	private String crop;
	private String spbsNumber;
	private String spbsDate;
	private String driver;
	private String licensePlate;
	private double qtyJanjangAngkut;
	private double qtyLooseFruitAngkut;
	private  String desttype;
	
	public static final String TABLE_NAME = "SPBS_REPORT_NUMBER";

	public SPBSReportNumber(String year, String companyCode, String estate,
			String crop, String spbsNumber, String spbsDate, String driver,
			String licensePlate, double qtyJanjangAngkut, double qtyLooseFruitAngkut, String desttype) {
		super();
		this.year = year;
		this.companyCode = companyCode;
		this.estate = estate;
		this.crop = crop;
		this.spbsNumber = spbsNumber;
		this.spbsDate = spbsDate;
		this.driver = driver;
		this.licensePlate = licensePlate;
		this.qtyJanjangAngkut = qtyJanjangAngkut;
		this.qtyLooseFruitAngkut = qtyLooseFruitAngkut;
		this.desttype = desttype;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getSpbsNumber() {
		return spbsNumber;
	}

	public void setSpbsNumber(String spbsNumber) {
		this.spbsNumber = spbsNumber;
	}

	public String getSpbsDate() {
		return spbsDate;
	}

	public void setSpbsDate(String spbsDate) {
		this.spbsDate = spbsDate;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public double getQtyJanjangAngkut() {
		return qtyJanjangAngkut;
	}

	public void setQtyJanjangAngkut(double qtyJanjangAngkut) {
		this.qtyJanjangAngkut = qtyJanjangAngkut;
	}
	
	public double getQtyLooseFruitAngkut() {
		return qtyLooseFruitAngkut;
	}

	public void setQtyLooseFruitAngkut(double qtyLooseFruitAngkut) {
		this.qtyLooseFruitAngkut = qtyLooseFruitAngkut;
	}


	public String getDesttype() {
		return desttype;
	}

	public void setDesttype(String desttype) {
		this.desttype = desttype;
	}

	public static final Parcelable.Creator<SPBSReportNumber> CREATOR = new Creator<SPBSReportNumber>() {

		@Override
		public SPBSReportNumber createFromParcel(Parcel parcel) {
			SPBSReportNumber bpnReport = new SPBSReportNumber(
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readDouble(), parcel.readDouble(),
					parcel.readString());

			return bpnReport;
		}

		@Override
		public SPBSReportNumber[] newArray(int size) {
			return new SPBSReportNumber[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(year);
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(crop);
		parcel.writeString(spbsNumber);
		parcel.writeString(spbsDate);
		parcel.writeString(driver);
		parcel.writeString(licensePlate);
		parcel.writeDouble(qtyJanjangAngkut);
		parcel.writeDouble(qtyLooseFruitAngkut);
		parcel.writeString(desttype);
	}

}
