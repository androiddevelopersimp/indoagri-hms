package com.simp.hms.model;

public class SPUSummary {
    private String spuNumber;
    private String spuDate;
    private String block;
    private double qtyGoodWeight;
    private double qtyBadWeight;
    private double qtyPoorWeight;
    private double qtyTotalWeight;

    public SPUSummary(String spuNumber, String spuDate, String block,
                      double qtyGoodWeight, double qtyBadWeight, double qtyPoorWeight, double qtyTotalWeight) {
        super();
        this.spuNumber = spuNumber;
        this.spuDate = spuDate;
        this.block = block;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorWeight = qtyPoorWeight;
        this.qtyTotalWeight = qtyTotalWeight;
    }

    public String getSpuNumber() {
        return spuNumber;
    }

    public void setSpuNumber(String spuNumber) {
        this.spuNumber = spuNumber;
    }

    public String getSpuDate() {
        return spuDate;
    }

    public void setSpuDate(String spuDate) {
        this.spuDate = spuDate;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public double getQtyTotalWeight() {
        return qtyTotalWeight;
    }

    public void setQtyTotalWeight(double qtyTotalWeight) {
        this.qtyTotalWeight = qtyTotalWeight;
    }
}
