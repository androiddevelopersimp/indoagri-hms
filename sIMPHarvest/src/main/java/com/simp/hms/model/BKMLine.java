package com.simp.hms.model;

public class BKMLine {
	private long rowId = 0;
	private String imei = "";		//p
	private String companyCode = "";	//p
	private String estate = "";		//p
	private String bkmDate = "";		//p
	private String division = "";	//p
	private String gang = "";		//p
	private String nik = "";			//p
	private String name = "";
	private String absentType = "";
	private double mandays = 0;
	private String uom = "";
	private int useGerdang = 0;
	private int status = 0;
	private long createdDate = 0;
	private String createdBy = "";
	private long modifiedDate = 0;
	private String modifiedBy = "";
	
	public static final String TABLE_NAME = "BKM_LINE";
	public static final String XML_FILE = "IT_BKM";
	public static final String XML_DOCUMENT = "BKM_LINE";
	public static final String XML_IMEI = "IMEI";
	public static final String XML_COMPANY_CODE = "COMPANY_CODE";
	public static final String XML_ESTATE = "ESTATE";
	public static final String XML_BKM_DATE = "BKM_DATE";
	public static final String XML_DIVISION = "DIVISION";
	public static final String XML_GANG = "GANG";
	public static final String XML_GANG_CODE = "GANG_CODE";
	public static final String XML_NIK = "NIK";
	public static final String XML_NAME = "NAME";
	public static final String XML_ABSENT_TYPE = "ABSENT_TYPE";
	public static final String XML_MANDAYS = "MANDAYS";
	public static final String XML_UOM = "UOM";
	public static final String XML_USE_GERDANG = "USE_GERDANG";
	public static final String XML_STATUS = "STATUS";
	public static final String XML_CREATED_DATE = "CREATED_DATE";
	public static final String XML_CREATED_BY = "CREATED_BY";
	public static final String XML_MODIFIED_DATE = "MODIFIED_DATE";
	public static final String XML_MODIFIED_BY = "MODIFIED_BY";
	
	public static final String XML_DOCUMENT_RESTORE = "ABSENT_ITEM";
	public static final String XML_ITEM_RESTORE = "ITEM";
	public static final String XML_BKM_DATE_RESTORE = "BKMDATE";
	public static final String XML_ESTATE_RESTORE = "ESTATE";
	public static final String XML_DIVISION_RESTORE = "DIVISION";
	public static final String XML_GANG_RESTORE = "GANG";
	public static final String XML_NIK_RESTORE = "NIK";
	public static final String XML_NAME_RESTORE = "NAME";
	public static final String XML_ABSENT_TYPE_RESTORE = "ABSENTTYPE";
	public static final String XML_MANDAYS_RESTORE = "MANDAYS";
	public static final String XML_UOM_RESTORE = "UOM";
	public static final String XML_CREATED_DATE_RESTORE = "CREATEDDATE";
	public static final String XML_CREATED_BY_RESTORE = "CREATEDBY";
	
	
	public BKMLine(){}
	
	public BKMLine(long rowId, String imei, String companyCode, String estate,
			String bkmDate, String division, String gang, String nik,
			String name, String absentType, double mandays, String uom, int useGerdang,
			int status, long createdDate, String createdBy, long modifiedDate,
			String modifiedBy) {
		super();
		this.rowId = rowId;
		this.imei = imei;
		this.companyCode = companyCode;
		this.estate = estate;
		this.bkmDate = bkmDate;
		this.division = division;
		this.gang = gang;
		this.nik = nik;
		this.name = name;
		this.absentType = absentType;
		this.mandays = mandays;
		this.uom = uom;
		this.useGerdang = useGerdang;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}

	public long getRowId() {
		return rowId;
	}

	public void setRowId(long rowId) {
		this.rowId = rowId;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getBkmDate() {
		return bkmDate;
	}

	public void setBkmDate(String bkmDate) {
		this.bkmDate = bkmDate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbsentType() {
		return absentType;
	}

	public void setAbsentType(String absentType) {
		this.absentType = absentType;
	}

	public double getMandays() {
		return mandays;
	}

	public void setMandays(double mandays) {
		this.mandays = mandays;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public int getUseGerdang() {
		return useGerdang;
	}

	public void setUseGerdang(int useGerdang) {
		this.useGerdang = useGerdang;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
}
