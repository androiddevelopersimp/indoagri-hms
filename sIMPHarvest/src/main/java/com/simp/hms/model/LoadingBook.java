package com.simp.hms.model;

public class LoadingBook {
	private String trx_id;				//p
	private String divison;				//p
	private String gang;				//p
	private String foreman;
	private String nik_foreman;
	private String clerk;
	private String harvester;
	private String location;
	private String tph;
	private String qty_jjg;
	
	public LoadingBook(){}

	public LoadingBook(String trx_id, String divison, String gang, String foreman, String nik_foreman, String clerk, String harvester,
					   String location, String tph, String qty_jjg) {
		this.trx_id = trx_id;
		this.divison = divison;
		this.gang = gang;
		this.foreman = foreman;
		this.nik_foreman = nik_foreman;
		this.clerk = clerk;
		this.harvester = harvester;
		this.location = location;
		this.tph = tph;
		this.qty_jjg = qty_jjg;
	}

	public String getTrxId() {
		return trx_id;
	}

	public void setTrxId(String trx_id) {
		this.trx_id = trx_id;
	}

	public String getDivison() {
		return divison;
	}

	public void setDivison(String divison) {
		this.divison = divison;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getForeman() {
		return foreman;
	}

	public void setForeman(String foreman) {
		this.foreman = foreman;
	}

	public String getNikForeman() {
		return nik_foreman;
	}

	public void setNikForeman(String nik_foreman) {
		this.nik_foreman = nik_foreman;
	}

	public String getClerk() {
		return clerk;
	}

	public void setClerk(String clerk) {
		this.clerk = clerk;
	}

	public String getHarvester() {
		return harvester;
	}

	public void setHarvester(String harvester) {
		this.harvester = harvester;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTph() {
		return tph;
	}

	public void setTph(String tph) {
		this.tph = tph;
	}

	public String getQtyJjg() {
		return qty_jjg;
	}

	public void setQtyJjg(String qty_jjg) {
		this.qty_jjg = qty_jjg;
	}
}
