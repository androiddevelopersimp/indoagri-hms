package com.simp.hms.model;

public class CropSensusBook {
	private String trx_id;
	private String tgl_taksasi;
	private String block;
	private String jml_pohon;
	private String estimasi_jenjang;
	private String bjr;
	private String gps_koordinat;
	
	public final static String TABLE_TAKSASI = "TAKSASI";
	public final static String XML_TRX_ID = "TRX_ID";
	public final static String XML_TGL_TAKSASI = "TGL_TAKSASI";
	public final static String XML_BLOCK = "BLOCK";
	public final static String XML_JML_POHON = "JML_POHON";
	public final static String XML_ESTIMASI_JENJANG = "ESTIMASI_JENJANG";
	public final static String XML_BJR = "BJR";
	public final static String XML_GPS_KOORDINAT = "GPS_KOORDINAT";
	
	public CropSensusBook(){}
	
	public CropSensusBook(String trx_id, String tgl_taksasi, String block, String jml_pohon, String estimasi_jenjang, String bjr, String gps_koordinat) {
		this.trx_id = trx_id;
		this.tgl_taksasi = tgl_taksasi;
		this.block = block;
		this.jml_pohon = jml_pohon;
		this.estimasi_jenjang = estimasi_jenjang;
		this.bjr = bjr;
		this.gps_koordinat = gps_koordinat;
	}

	public String getTrxId() {
		return trx_id;
	}

	public void setTrxId(String trx_id) {
		this.trx_id = trx_id;
	}

	public String getTglTaksasi() {
		return tgl_taksasi;
	}

	public void setTglTaksasi(String tgl_taksasi) {
		this.tgl_taksasi = tgl_taksasi;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getJmlPohon() {
		return jml_pohon;
	}

	public void setJmlPohon(String jml_pohon) {
		this.jml_pohon = jml_pohon;
	}

	public String getEstimasiJenjang() {
		return estimasi_jenjang;
	}

	public void setEstimasiJenjang(String estimasi_jenjang) {
		this.estimasi_jenjang = estimasi_jenjang;
	}

	public String getBjr() {
		return bjr;
	}

	public void setBjr(String bjr) {
		this.bjr = bjr;
	}

	public String getGpsKoordinat() {
		return gps_koordinat;
	}

	public void setGpsKoordinat(String gps_koordinat) {
		this.gps_koordinat = gps_koordinat;
	}
}
