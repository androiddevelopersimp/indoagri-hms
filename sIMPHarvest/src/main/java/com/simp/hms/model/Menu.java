package com.simp.hms.model;

/**
 * Created by Anka.Wirawan on 8/3/2016.
 */
public class Menu implements Comparable<Menu>{
    private int id;
    private String icon;
    private String caption;
    private String activity;

    public final static String TAG_MAIN_MENU = "menu";
    public final static String TAG_ID = "id";
    public final static String TAG_ICON = "icon";
    public final static String TAG_CAPTION = "caption";
    public final static String TAG_ACTIVITY = "activity";

    public Menu(){}

    public Menu(int id, String icon, String caption, String activity) {
        this.caption = caption;
        this.icon = icon;
        this.id = id;
        this.activity = activity;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public int compareTo(Menu menu) {
        return this.id < menu.getId()? -1 : 1;
    }
}
