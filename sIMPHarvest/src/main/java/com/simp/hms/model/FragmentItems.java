package com.simp.hms.model;

import android.support.v4.app.Fragment;

public class FragmentItems {
	private String title;
	private Fragment fragment;
	
	public FragmentItems(){}
	
	public FragmentItems(String name, Fragment fragment){
		this.title = name;
		this.fragment = fragment;
	}

	public String getName() {
		return title;
	}

	public void setName(String name) {
		this.title = name;
	}

	public Fragment getFragment() {
		return fragment;
	}

	public void setFragment(Fragment fragment) {
		this.fragment = fragment;
	}
}
