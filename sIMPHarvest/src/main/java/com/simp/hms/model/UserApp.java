package com.simp.hms.model;

public class UserApp {
	private int rowId;
	private String nik;
	private String username; // p
	private String password; // p
	private long validTo;
	private long createdDate;
	private String createdBy;

	public static final String TABLE_NAME = "USERAPP";
	public static final String ALIAS = "USER APPS";
	public static final String XML_DOCUMENT = "IT_USRAPPL";
	public static final String XML_ITEM = "ITEM";
	public static final String XML_NIK = "NIK";
	public static final String XML_USERNAME = "USERNAME";
	public static final String XML_PASSWORD = "PASSWORD";
	public static final String XML_VALID_TO = "VALID_TO";
	public static final String XML_CREATED_DATE = "CREATEDDATE";
	public static final String XML_CREATED_BY = "CREATEDBY";

	public static final String KEY_ENCRYPT = "HMS_INDOAGRI_2015";

	public UserApp() {
	}

	public UserApp(int rowId, String nik, String username, String password,
			long validTo, long createdDate, String createdBy) {
		super();
		this.rowId = rowId;
		this.nik = nik;
		this.username = username;
		this.password = password;
		this.validTo = validTo;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
	}



	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getValidTo() {
		return validTo;
	}

	public void setValidTo(long validTo) {
		this.validTo = validTo;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
