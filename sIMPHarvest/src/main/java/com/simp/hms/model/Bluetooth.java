package com.simp.hms.model;

public class Bluetooth {
	String name = "";
	String address = "";
	boolean isPaired = false;
	boolean isSelected = false;
	
	public final static String TABLE_NAME = "BLUETOOTH";
	public final static String XML_NAME = "NAME";
	public final static String XML_ADDRESS = "ADDRESS";
	public final static String XML_IS_PAIRED = "IS_PAIRED";
	public final static String XML_IS_SELECTED = "IS_SELECTED";
	
	public Bluetooth(){}
	
	public Bluetooth(String name, String address, boolean isPaired, boolean isSelected) {
		super();
		this.name = name;
		this.address = address;
		this.isPaired = isPaired;
		this.isSelected = isSelected;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public boolean isPaired() {
		return isPaired;
	}

	public void setPaired(boolean isPaired) {
		this.isPaired = isPaired;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
}
