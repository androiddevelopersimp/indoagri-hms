package com.simp.hms.model;

public class FileXML {
	private String file_new;
	private String path_backup;
	
	public FileXML(){}
	
	public FileXML(String file_new, String path_backup){
		this.file_new = file_new;
		this.path_backup = path_backup;
	}

	public String getFileNew() {
		return file_new;
	}

	public void setFileNew(String path_new) {
		this.file_new = path_new;
	}

	public String getPathBackup() {
		return path_backup;
	}

	public void setPath_backup(String path_backup) {
		this.path_backup = path_backup;
	}
}
