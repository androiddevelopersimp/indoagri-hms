package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BPNReportBlock implements Parcelable {
	private String companyCode;
	private String estate;
	private String division;
	private String gang;
	private String block;
	private double qtyJanjang;
	private double qtyLooseFruit;
	private double qtyMentah;
	private double qtyBusuk;
	private double qtyTangkaiPanjang;

	public static final String TABLE_NAME = "BPN_REPORT_BLOCK";

	public BPNReportBlock(String companyCode, String estate, String division, String gang,
			String block, double qtyJanjang, double qtyLooseFruit,
			double qtyMentah, double qtyBusuk, double qtyTangkaiPanjang) {
		super();
		this.companyCode = companyCode;
		this.estate = estate;
		this.division = division;
		this.gang = gang;
		this.block = block;
		this.qtyJanjang = qtyJanjang;
		this.qtyLooseFruit = qtyLooseFruit;
		this.qtyMentah = qtyMentah;
		this.qtyBusuk = qtyBusuk;
		this.qtyTangkaiPanjang = qtyTangkaiPanjang;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getEstate() {
		return estate;
	}

	public void setEstate(String estate) {
		this.estate = estate;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public double getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(double qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}

	public double getQtyLooseFruit() {
		return qtyLooseFruit;
	}

	public void setQtyLooseFruit(double qtyLooseFruit) {
		this.qtyLooseFruit = qtyLooseFruit;
	}

	public double getQtyMentah() {
		return qtyMentah;
	}

	public void setQtyMentah(double qtyMentah) {
		this.qtyMentah = qtyMentah;
	}

	public double getQtyBusuk() {
		return qtyBusuk;
	}

	public void setQtyBusuk(double qtyBusuk) {
		this.qtyBusuk = qtyBusuk;
	}

	public double getQtyTangkaiPanjang() {
		return qtyTangkaiPanjang;
	}

	public void setQtyTangkaiPanjang(double qtyTangkaiPanjang) {
		this.qtyTangkaiPanjang = qtyTangkaiPanjang;
	}

	public static final Parcelable.Creator<BPNReportBlock> CREATOR = new Creator<BPNReportBlock>() {

		@Override
		public BPNReportBlock createFromParcel(Parcel parcel) {
			BPNReportBlock bpnHeader = new BPNReportBlock(parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readString(),
					parcel.readString(), parcel.readDouble(),
					parcel.readDouble(), parcel.readDouble(),
					parcel.readDouble(), parcel.readDouble());

			return bpnHeader;
		}

		@Override
		public BPNReportBlock[] newArray(int size) {
			return new BPNReportBlock[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeString(companyCode);
		parcel.writeString(estate);
		parcel.writeString(division);
		parcel.writeString(gang);
		parcel.writeString(block);
		parcel.writeDouble(qtyJanjang);
		parcel.writeDouble(qtyLooseFruit);
		parcel.writeDouble(qtyMentah);
		parcel.writeDouble(qtyBusuk);
		parcel.writeDouble(qtyTangkaiPanjang);
	}

}
