package com.simp.hms.model;

public class SPBSLineSummary {
	int id = 1;
	String tph = "";
	String bpnId = "";  
	String bpnDate = "";
	String spbsRef = "";
	String spbsNext = "";
	double qtyJanjang = 0;
	double qtyJanjangAngkut = 0;
	double qtyLooseFruit = 0;
	double qtyLooseFruitAngkut = 0;
	String gpsKoordinat = "0.0:0.0";
	int isSave = 0;
	double qtyJanjangPrev = 0;
	String tphOriginal = "";

	public SPBSLineSummary(int id, String tph, String bpnId, String bpnDate, String spbsRef, String spbsNext,
			double qtyJanjang, double qtyJanjangAngkut, double qtyLooseFruit, double qtyLooseFruitAngkut, 
			String gpsKoordinat, int isSave, String tphOriginal) {
		super();
		this.id = id;
		this.tph = tph;
		this.bpnId = bpnId;
		this.bpnDate = bpnDate;
		this.spbsRef = spbsRef;
		this.spbsNext = spbsNext;
		this.qtyJanjang = qtyJanjang;
		this.qtyJanjangAngkut = qtyJanjangAngkut;
		this.qtyLooseFruit = qtyLooseFruit;
		this.qtyLooseFruitAngkut = qtyLooseFruitAngkut;
		this.gpsKoordinat = gpsKoordinat;
		this.isSave = isSave;
		this.tphOriginal = tphOriginal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTph() {
		return tph;
	}

	public void setTph(String tph) {
		this.tph = tph;
	}

	public String getBpnId() {
		return bpnId;
	}

	public void setBpnId(String bpnId) {
		this.bpnId = bpnId;
	}

	public String getBpnDate() {
		return bpnDate;
	}

	public void setBpnDate(String bpnDate) {
		this.bpnDate = bpnDate;
	}

	public String getSpbsRef() {
		return spbsRef;
	}

	public void setSpbsRef(String spbsRef) {
		this.spbsRef = spbsRef;
	}

	public String getSpbsNext() {
		return spbsNext;
	}

	public void setSpbsNext(String spbsNext) {
		this.spbsNext = spbsNext;
	}

	public double getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(double qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}

	public double getQtyJanjangAngkut() {
		return qtyJanjangAngkut;
	}

	public void setQtyJanjangAngkut(double qtyJanjangAngkut) {
		this.qtyJanjangAngkut = qtyJanjangAngkut;
	}

	public double getQtyLooseFruit() {
		return qtyLooseFruit;
	}

	public void setQtyLooseFruit(double qtyLooseFruit) {
		this.qtyLooseFruit = qtyLooseFruit;
	}
	
	public double getQtyLooseFruitAngkut() {
		return qtyLooseFruitAngkut;
	}

	public void setQtyLooseFruitAngkut(double qtyLooseFruitAngkut) {
		this.qtyLooseFruitAngkut = qtyLooseFruitAngkut;
	}

	public String getGpsKoordinat() {
		return gpsKoordinat;
	}

	public void setGpsKoordinat(String gpsKoordinat) {
		this.gpsKoordinat = gpsKoordinat;
	}

	public int getIsSave() {
		return isSave;
	}

	public void setIsSave(int isSave) {
		this.isSave = isSave;
	}

	public String getTphOriginal() {
		return tphOriginal;
	}

	public void setTphOriginal(String tphOriginal) {
		this.tphOriginal = tphOriginal;
	}

}
