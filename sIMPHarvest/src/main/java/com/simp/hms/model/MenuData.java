package com.simp.hms.model;

import java.util.List;

/**
 * Created by Anka.Wirawan on 8/3/2016.
 */
public class MenuData {
    private String status;
    private Data data;
    private String message;

    public MenuData(){}

    public MenuData(String status, Data data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public List<Menu> getMenus(){
        return getData().getMenus();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private class Data{
        List<Menu> menus;

        public List<Menu> getMenus() {
            return menus;
        }
    }
}
