package com.simp.hms.model;

public class SPUSummaryPODS {
    private String spuNumber;
    private String spuDate;
    private String block;
    private double qtyPODS;
    private double qtyGoodWeight;
    private double qtyBadWeight;
    private double qtyPoorWeight;
    private double qtyTotalWeight;

    public SPUSummaryPODS(String spuNumber, String spuDate, String block, double qtyPODS,
                          double qtyGoodWeight, double qtyBadWeight, double qtyPoorWeight, double qtyTotalWeight) {
        super();
        this.spuNumber = spuNumber;
        this.spuDate = spuDate;
        this.block = block;
        this.qtyPODS = qtyPODS;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorWeight = qtyPoorWeight;
        this.qtyTotalWeight = qtyTotalWeight;
    }

    public double getQtyPODS() {
        return qtyPODS;
    }

    public void setQtyPODS(double qtyPODS) {
        this.qtyPODS = qtyPODS;
    }

    public String getSpuNumber() {
        return spuNumber;
    }

    public void setSpuNumber(String spuNumber) {
        this.spuNumber = spuNumber;
    }

    public String getSpuDate() {
        return spuDate;
    }

    public void setSpuDate(String spuDate) {
        this.spuDate = spuDate;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public double getQtyTotalWeight() {
        return qtyTotalWeight;
    }

    public void setQtyTotalWeight(double qtyTotalWeight) {
        this.qtyTotalWeight = qtyTotalWeight;
    }
}
