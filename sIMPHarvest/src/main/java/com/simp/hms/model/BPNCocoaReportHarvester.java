package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BPNCocoaReportHarvester implements Parcelable {
    private String companyCode;
    private String estate;
    private String division;
    private String gang;
    private String nik;
    private String name;

    private double qtyGoodQty;
    private double qtyGoodWeight;
    private double qtyBadQty;
    private double qtyBadWeight;
    private double qtyPoorQty;
    private double qtyPoorWeight;

    public static final String TABLE_NAME = "BPN_REPORT_HARVESTER";

    public BPNCocoaReportHarvester(String companyCode, String estate, String division, String gang, String nik, String name,
                                   double qtyGoodQty, double qtyGoodWeight,
                                   double qtyBadQty, double qtyBadWeight,
                                   double qtyPoorQty, double qtyPoorWeight) {
        super();
        this.companyCode = companyCode;
        this.estate = estate;
        this.division = division;
        this.gang = gang;
        this.nik = nik;
        this.name = name;

        this.qtyGoodQty = qtyGoodQty;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadQty = qtyBadQty;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorQty = qtyPoorQty;
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getGang() {
        return gang;
    }

    public void setGang(String gang) {
        this.gang = gang;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQtyGoodQty() {
        return qtyGoodQty;
    }

    public void setQtyGoodQty(double qtyGoodQty) {
        this.qtyGoodQty = qtyGoodQty;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadQty() {
        return qtyBadQty;
    }

    public void setQtyBadQty(double qtyBadQty) {
        this.qtyBadQty = qtyBadQty;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyGoodWeight = qtyBadWeight;
    }

    public double getQtyPoorQty() {
        return qtyPoorQty;
    }

    public void setQtyPoorQty(double qtyPoorQty) {
        this.qtyPoorQty = qtyPoorQty;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public static final Parcelable.Creator<BPNCocoaReportHarvester> CREATOR = new Creator<BPNCocoaReportHarvester>() {

        @Override
        public BPNCocoaReportHarvester createFromParcel(Parcel parcel) {
            BPNCocoaReportHarvester bpnReport  = new BPNCocoaReportHarvester(parcel.readString(), parcel.readString(), parcel.readString(),
                    parcel.readString(), parcel.readString(), parcel.readString(),
                    parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble());

            return bpnReport;
        }

        @Override
        public BPNCocoaReportHarvester[] newArray(int size) {
            return new BPNCocoaReportHarvester[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(companyCode);
        parcel.writeString(estate);
        parcel.writeString(division);
        parcel.writeString(gang);
        parcel.writeString(nik);
        parcel.writeString(name);

        parcel.writeDouble(qtyGoodQty);
        parcel.writeDouble(qtyGoodWeight);
        parcel.writeDouble(qtyBadQty);
        parcel.writeDouble(qtyBadWeight);
        parcel.writeDouble(qtyPoorQty);
        parcel.writeDouble(qtyPoorWeight);
    }
}
