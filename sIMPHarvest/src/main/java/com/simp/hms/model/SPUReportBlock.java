package com.simp.hms.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SPUReportBlock implements Parcelable {
    private String year;
    private String companyCode;
    private String estate;
    private String crop;
    private String spuNumber;
    private String spuDate;
    private String block;
    private double qtyGoodQty;
    private double qtyGoodWeight;
    private double qtyBadQty;
    private double qtyBadWeight;
    private double qtyPoorQty;
    private double qtyPoorWeight;

    public static final String TABLE_NAME = "SPU_REPORT_BLOCK";

    public SPUReportBlock(String year, String companyCode, String estate,
                          String crop, String spuNumber, String spuDate, String block,
                          double qtyGoodQty, double qtyGoodWeight,
                          double qtyBadQty, double qtyBadWeight,
                          double qtyPoorQty, double qtyPoorWeight) {

        super();
        this.year = year;
        this.companyCode = companyCode;
        this.estate = estate;
        this.crop = crop;
        this.spuNumber = spuNumber;
        this.spuDate = spuDate;
        this.block = block;
        this.qtyGoodQty = qtyGoodQty;
        this.qtyGoodWeight = qtyGoodWeight;
        this.qtyBadQty = qtyBadQty;
        this.qtyBadWeight = qtyBadWeight;
        this.qtyPoorQty = qtyPoorQty;
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public String getSpuNumber() {
        return spuNumber;
    }

    public void setSpuNumber(String spuNumber) {
        this.spuNumber = spuNumber;
    }

    public String getSpuDate() {
        return spuDate;
    }

    public void setSpuDate(String spuDate) {
        this.spuDate = spuDate;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public double getQtyGoodQty() {
        return qtyGoodQty;
    }

    public void setQtyGoodQty(double qtyGoodQty) {
        this.qtyGoodQty = qtyGoodQty;
    }

    public double getQtyGoodWeight() {
        return qtyGoodWeight;
    }

    public void setQtyGoodWeight(double qtyGoodWeight) {
        this.qtyGoodWeight = qtyGoodWeight;
    }

    public double getQtyBadQty() {
        return qtyBadQty;
    }

    public void setQtyBadQty(double qtyBadQty) {
        this.qtyBadQty = qtyBadQty;
    }

    public double getQtyBadWeight() {
        return qtyBadWeight;
    }

    public void setQtyBadWeight(double qtyBadWeight) {
        this.qtyBadWeight = qtyBadWeight;
    }

    public double getQtyPoorQty() {
        return qtyPoorQty;
    }

    public void setQtyPoorQty(double qtyPoorQty) {
        this.qtyPoorQty = qtyPoorQty;
    }

    public double getQtyPoorWeight() {
        return qtyPoorWeight;
    }

    public void setQtyPoorWeight(double qtyPoorWeight) {
        this.qtyPoorWeight = qtyPoorWeight;
    }

    public static final Parcelable.Creator<SPUReportBlock> CREATOR = new Creator<SPUReportBlock>() {

        @Override
        public SPUReportBlock createFromParcel(Parcel parcel) {
            SPUReportBlock spuReport = new SPUReportBlock(
                    parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(),
                    parcel.readString(), parcel.readString(), parcel.readString(),
                    parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble(),
                    parcel.readDouble(), parcel.readDouble());

            return spuReport;
        }

        @Override
        public SPUReportBlock[] newArray(int size) {
            return new SPUReportBlock[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(year);
        parcel.writeString(companyCode);
        parcel.writeString(estate);
        parcel.writeString(crop);
        parcel.writeString(spuNumber);
        parcel.writeString(spuDate);
        parcel.writeString(block);
        parcel.writeDouble(qtyGoodQty);
        parcel.writeDouble(qtyGoodWeight);
        parcel.writeDouble(qtyBadQty);
        parcel.writeDouble(qtyBadWeight);
        parcel.writeDouble(qtyPoorQty);
        parcel.writeDouble(qtyPoorWeight);
    }
}
