package com.simp.hms.model;

public class MessageStatus {
	private String menu = "";
	private boolean status = false;	
	private String message = "";
	
	public MessageStatus(){}
	
	public MessageStatus(String menu, boolean status, String message) {
		super();
		this.menu = menu;
		this.status = status;
		this.message = message;
	}
	
	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
