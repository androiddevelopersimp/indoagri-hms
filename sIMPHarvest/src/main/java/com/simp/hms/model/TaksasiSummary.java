package com.simp.hms.model;

public class TaksasiSummary {
	private int barisSkb;
	private int qtyJanjang;
	private int qtyPokok;
	private double taksasiKg;

	public TaksasiSummary(int barisSkb, int qtyJanjang, int qtyPokok, double taksasiKg) {
		super();
		this.barisSkb = barisSkb;
		this.qtyJanjang = qtyJanjang;
		this.qtyPokok = qtyPokok;
		this.taksasiKg = taksasiKg;
	}

	public int getBarisSkb() {
		return barisSkb;
	}

	public void setBarisSkb(int barisSkb) {
		this.barisSkb = barisSkb;
	}	
	
	public int getQtyJanjang() {
		return qtyJanjang;
	}

	public void setQtyJanjang(int qtyJanjang) {
		this.qtyJanjang = qtyJanjang;
	}
	
	public int getQtyPokok() {
		return qtyPokok;
	}

	public void setQtyPokok(int qtyPokok) {
		this.qtyPokok = qtyPokok;
	}

	public double getTaksasiKg() {
		return taksasiKg;
	}

	public void setTaksasiKg(double taksasiKg) {
		this.taksasiKg = taksasiKg;
	}

}
