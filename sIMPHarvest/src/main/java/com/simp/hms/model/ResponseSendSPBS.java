package com.simp.hms.model;

public class ResponseSendSPBS {

    public static class XML {
        public static String HEADER = "RESPONSE_SPBS";
        public static String SPBS_NO = "NOSPBS";
        public static String SPBS_STATUS = "STATUSSPBS";
    }

    private String spbsNo;
    private String spbsStatus;

    public String getSpbsNo() {
        return spbsNo;
    }

    public void setSpbsNo(String spbsNo) {
        this.spbsNo = spbsNo;
    }

    public String getSpbsStatus() {
        return spbsStatus;
    }

    public void setSpbsStatus(String spbsStatus) {
        this.spbsStatus = spbsStatus;
    }
}
