package com.simp.hms.model;

public class BKMReportBlock {
	private String block;
	private double output;

	public static final String TABLE_NAME = "BKM_REPORT_BLOCK";

	public BKMReportBlock(String block, double output) {
		super();
		this.block = block;
		this.output = output;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public double getOutput() {
		return output;
	}

	public void setOutput(double output) {
		this.output = output;
	}

}
