package com.simp.hms.model;

public class Gang {
	private String code;

	public static final String XML_GANG_CODE = "GANG_CODE";
	
	public Gang(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
