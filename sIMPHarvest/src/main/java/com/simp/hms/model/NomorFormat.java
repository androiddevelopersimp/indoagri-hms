package com.simp.hms.model;

import android.text.TextUtils;

public class NomorFormat {
	private String companyCode;
	private String estate;
	private String division;
	private String year;
	private String month;
	private String alias;
	private String runningNumber;
	
	public NomorFormat(String companyCode, String estate, String division,
			String year, String month, String alias, String runningNumber) {
		super();
		this.companyCode = companyCode;
		this.estate = estate;
		this.division = division;
		this.year = year;
		this.month = month;
		this.alias = alias;
		this.runningNumber = runningNumber;
	}
	
	public String getSPBSFormat(){
		year = !TextUtils.isEmpty(year) ? year.substring(2, year.length()) : "";
		runningNumber = !TextUtils.isEmpty(runningNumber) ? new Converter(String.valueOf(runningNumber)).StrToDigit(3) : "";
		alias = !TextUtils.isEmpty(alias) ? new Converter(String.valueOf(alias)).StrToDigit(2) : "";
		division = !TextUtils.isEmpty(division) ?  new Converter(String.valueOf(division)).StrToDigit(2) : "";
		
		return estate + month + division + year + alias + runningNumber;
	}
	
}
