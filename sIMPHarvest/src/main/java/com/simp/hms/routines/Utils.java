package com.simp.hms.routines;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.simp.hms.R;

import java.lang.reflect.Field;

public class Utils {
	public static double round(double value, int precision){
		int scale = (int) Math.pow(10, precision);
		
		return (double) Math.round(value * scale)/scale;
	}

	public static Class<?> convertStringToClass(String className){
		Class<?> c = null;

		if(className != null) {
			try {
				c = Class.forName(className);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		return c;
	}

	public static int getId(String resourceName, Class<?> c) {
		try {
			Field idField = c.getDeclaredField(resourceName);
			return idField.getInt(idField);
		} catch (Exception e) {
			throw new RuntimeException("No resource ID found for: " + resourceName + " / " + c, e);
		}
	}

	public static DisplayMetrics getScreenSize(Context context){
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

		return displayMetrics;
	}

	public static float convertDpToPixel(Context context, float dp) {
		Resources resources = context.getResources();

		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());

		return px;
	}

	public static int getColumnCount(Activity activity){
		int columnCount = 1;

		DisplayMetrics displayMetrics = Utils.getScreenSize(activity);

		int widthScreen = displayMetrics.widthPixels;
		float widthMin = activity.getResources().getDimension(R.dimen.min_main_menu_width);

		if(widthScreen > widthMin) {
			int widthPx = (int) Utils.convertDpToPixel(activity, widthMin);
			columnCount = widthScreen / widthPx;
		}

		return columnCount;
	}
}
