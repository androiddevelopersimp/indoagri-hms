package com.simp.hms.enums;

public enum QrCodeQuality {
	L(1249),
	M(970),
	Q(702),
	H(557);
	
	private int id;
	
	private QrCodeQuality(int id){
		this.id = id;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}
	
	public int getId(){
		return id;
	}
}
