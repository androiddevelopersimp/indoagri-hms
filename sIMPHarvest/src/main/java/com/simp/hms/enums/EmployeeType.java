package com.simp.hms.enums;

public enum EmployeeType {
	FOREMAN(1),
	CLERK(2),
	HARVESTER(3),
	HARVESTER_ABSENT(4),
	DRIVER(5),
	KERNET(6),
	OPERATOR(7);
	
	private int id;
	
	private EmployeeType(int id){
		this.id = id;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}
	
	public int getId(){
		return id;
	}
}
