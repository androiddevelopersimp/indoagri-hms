package com.simp.hms.enums;

/**
 * Created by Anka.Wirawan on 8/8/2016.
 */
public enum RunningAccountType {
    GENERAL(1),
    TRUCK_LOGO(2),
    CH_GL(3);

    private int id;

    private RunningAccountType(int id){
        this.id = id;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

    public int getId(){
        return id;
    }
}