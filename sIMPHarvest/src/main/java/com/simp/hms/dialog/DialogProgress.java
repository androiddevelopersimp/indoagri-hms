package com.simp.hms.dialog;

import com.simp.hms.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DialogProgress extends Dialog{
	
	private ProgressBar pgb_progress_bar;
	private TextView txt_progress_message;
	private String message;
	
	public DialogProgress(Context context){
		super(context);
	}   

	public DialogProgress(Context context, String message){
		super(context);
		this.message = message;
	} 
	
	public void setTitle(String message){
		this.message = message; 
	}
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		setContentView(R.layout.dialog_progress);  
		
		pgb_progress_bar = (ProgressBar)findViewById(R.id.pgb_progress_bar);
		txt_progress_message = (TextView)findViewById(R.id.txt_progress_message);
		
		txt_progress_message.setText(message);	
	}
	
	@Override
	public void cancel() {
		//do nothing
	}

	@Override
	public void onBackPressed() {
		//do nothing
	}

	@Override
	public void onContentChanged() {
		super.onContentChanged();
	}
}
