package com.simp.hms.dialog;

import com.simp.hms.R;
import com.simp.hms.listener.DialogConfirmFragmentListener;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DialogConfirmFragment extends DialogFragment implements android.view.View.OnClickListener {		
	private TextView txt_confirm_title;
	private TextView txt_confirm_message;
	private Button btn_confirm_cancel;
	private Button btn_confirm_ok;
	
//	private Activity activity;
	private String title;
	private String message;
	private int id;
	
	private DialogConfirmFragmentListener callback;

	public DialogConfirmFragment(){}

	public static DialogConfirmFragment newInstance(String title, String message, int id){
		DialogConfirmFragment fragment = new DialogConfirmFragment();
		Bundle bundle = new Bundle();

		bundle.putString("title", title);
		bundle.putString("message", message);
		bundle.putInt("id", id);

		fragment.setArguments(bundle);

		return fragment;
	}
	
//	public DialogConfirmFragment(String title, String message, int id){
//		this.title = title;
//		this.message = message;
//		this.id = id;
//	}

	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog =  super.onCreateDialog(savedInstanceState);
	
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);	
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.dialog_confirm, null);
		
		txt_confirm_title = (TextView) view.findViewById(R.id.txt_confirm_title);
		txt_confirm_message = (TextView) view.findViewById(R.id.txt_confirm_message);
		btn_confirm_cancel = (Button) view.findViewById(R.id.btn_confirm_cancel);
		btn_confirm_ok = (Button) view.findViewById(R.id.btn_confirm_ok);

		Bundle bundle = getArguments();

		if(bundle != null){
			title = bundle.getString("title", "");
			message = bundle.getString("message", "");
			id = bundle.getInt("id", 0);
		}

		txt_confirm_title.setText(title);
		txt_confirm_message.setText(message);
		btn_confirm_cancel.setOnClickListener(this);
		btn_confirm_ok.setOnClickListener(this);
		
		return view;
	}


	@Override
	public void onClick(View view) {	
		switch (view.getId()) {
		case R.id.btn_confirm_cancel:
			dismiss();
			break;
		case R.id.btn_confirm_ok:
			try{
				callback = (DialogConfirmFragmentListener) getTargetFragment();
			}catch(ClassCastException e){
				e.printStackTrace();
				throw e;
			}
			
			if(callback != null){
				callback.onConfirmFragmentOK(id);
				dismiss();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		dismiss();
	}
}