package com.simp.hms.dialog;

import java.util.List;

import com.simp.hms.R;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.listener.DialogSpbsDetailFragmentListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.Penalty;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DialogSpbsDetailFragment extends DialogFragment implements android.view.View.OnClickListener {
	private TextView txtNotificationSpbsDetailTitle;
	private TextView txtNotificationSpbsDetailHarvester;
	private TextView txtNotificationSpbsDetailQtyJanjang;
	private TextView txtNotificationSpbsDetailQtyLooseFruit;
	private LinearLayout lnrNotificationSpbsDetailPenalty;
	private Button btnNotificationSpbsDetailOk;
	
//	private Activity activity;
	private String title;
	private boolean isFinish;
	private String bpnId;
	
	private DialogSpbsDetailFragmentListener callback;

	public DialogSpbsDetailFragment(){}

	public static DialogSpbsDetailFragment newInstance(String title, String bpnId, boolean finish){
		DialogSpbsDetailFragment fragment = new DialogSpbsDetailFragment();
		Bundle bundle = new Bundle();

		bundle.putString("title", title);
		bundle.putString("bpnId", bpnId);
		bundle.putBoolean("finish", finish);

		fragment.setArguments(bundle);

		return fragment;
	}
	
//	public DialogSpbsDetailFragment(String title, String bpnId, boolean isFinish){
//		this.title = title;
//		this.bpnId = bpnId;
//		this.isFinish = isFinish;
//	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog =  super.onCreateDialog(savedInstanceState);
	
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);	
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.dialog_spbs_detail, null);
		
		txtNotificationSpbsDetailTitle = (TextView) view.findViewById(R.id.txtNotificationSpbsDetailTitle);
		txtNotificationSpbsDetailHarvester = (TextView) view.findViewById(R.id.txtNotificationSpbsDetailHarvester);
		txtNotificationSpbsDetailQtyJanjang = (TextView) view.findViewById(R.id.txtNotificationSpbsDetailQtyJanjang);
		txtNotificationSpbsDetailQtyLooseFruit = (TextView) view.findViewById(R.id.txtNotificationSpbsDetailQtyLooseFruit);
		lnrNotificationSpbsDetailPenalty = (LinearLayout) view.findViewById(R.id.lnrNotificationSpbsDetailPenalty);
		btnNotificationSpbsDetailOk = (Button) view.findViewById(R.id.btnNotificationSpbsDetailOk);

		Bundle bundle = getArguments();

		if(bundle != null){
			title = bundle.getString("title", "");
			bpnId = bundle.getString("bpnId", "");
			isFinish = bundle.getBoolean("finish", false);
		}

		txtNotificationSpbsDetailTitle.setText(title);
		btnNotificationSpbsDetailOk.setOnClickListener(this);
		
		new DetailTask().execute();
		
		return view;
	}


	@Override
	public void onClick(View view) {	
		switch (view.getId()) {
		case R.id.btnNotificationSpbsDetailOk:
			try{
				callback = (DialogSpbsDetailFragmentListener) getTargetFragment();
			}catch(ClassCastException e){
				e.printStackTrace();
				throw e;
			}
			
			if(callback != null){
				callback.onSpbsDetailFragmentOK(isFinish);
				dismiss();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		dismiss();
	}
	
	public class DetailTask extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			DatabaseHandler database = new DatabaseHandler(getActivity());
			
			database.openTransaction();
			BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null, 
					BPNHeader.XML_BPN_ID + "=?", 
					new String [] {bpnId}, 
					null, null, null, null);
			database.closeTransaction();
			
			txtNotificationSpbsDetailHarvester.setText( bpnHeader != null ? bpnHeader.getHarvester() : "");
			
			database.openTransaction();
			BPNQuantity bpnQuantityJjg = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null, 
					BPNQuantity.XML_BPN_ID + "=?" + " and " +
					BPNQuantity.XML_ACHIEVEMENT_CODE + "=?", 
					new String [] {bpnId, BPNQuantity.JANJANG_CODE}, 
					null, null, null, null);
			database.closeTransaction();
			
			txtNotificationSpbsDetailQtyJanjang.setText( bpnQuantityJjg != null? String.valueOf((int)bpnQuantityJjg.getQuantity()) : "0");
			
			database.openTransaction();
			BPNQuantity bpnQuantityLf = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null, 
					BPNQuantity.XML_BPN_ID + "=?" + " and " +
					BPNQuantity.XML_ACHIEVEMENT_CODE + "=?", 
					new String [] {bpnId, BPNQuantity.LOOSE_FRUIT_CODE}, 
					null, null, null, null);
			database.closeTransaction();
			
			txtNotificationSpbsDetailQtyLooseFruit.setText( bpnQuantityLf != null? String.valueOf(bpnQuantityLf.getQuantity()) : "0");
			
			database.openTransaction();
			List<Object> lstObject = database.getListData(false, Penalty.TABLE_NAME, null, null, null, null, null, null, null);
			database.closeTransaction();
			
			if(lstObject != null && lstObject.size() > 0){
				for(Object object : lstObject){
					Penalty penalty = (Penalty) object;
					
					database.openTransaction();
					BPNQuality bpnQuality = (BPNQuality) database.getDataFirst(false, BPNQuality.TABLE_NAME, null, 
							BPNQuality.XML_BPN_ID + "=?" + " and " +
							BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
							BPNQuality.XML_QUALITY_CODE + "=?", 
							new String [] {bpnId, BPNQuantity.JANJANG_CODE, penalty.getPenaltyCode()}, 
							null, null, null, null);
					database.closeTransaction();
					
					addChild(penalty, bpnQuality);
				}
			}
		}
		
		private void addChild(Penalty penalty, BPNQuality bpnQuality){
			TextView txtPenaltyItemName;
			TextView txtPenaltyItemCount;
			TextView txtPenaltyItemUom;
			
			final View child = getActivity().getLayoutInflater().inflate(R.layout.item_penalty_spbs, null);
			
			txtPenaltyItemName = (TextView) child.findViewById(R.id.txtPenaltyItemName);
			txtPenaltyItemCount = (TextView) child.findViewById(R.id.txtPenaltyItemCount);
			txtPenaltyItemUom = (TextView) child.findViewById(R.id.txtPenaltyItemUom);
			
			txtPenaltyItemName.setText(penalty.getPenaltyDesc());
			txtPenaltyItemCount.setText(bpnQuality != null ? String.valueOf((int) bpnQuality.getQuantity()) : "0");
			txtPenaltyItemUom.setText(penalty.getUom());
			
			lnrNotificationSpbsDetailPenalty.addView(child);
		}
	}
}