package com.simp.hms.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.listener.DialogDateFragmentListener;
import com.simp.hms.listener.DialogTimeFragmentListener;
import com.simp.hms.model.DateLocal;
import com.simp.hms.widget.DatePickerCustom;
import com.simp.hms.widget.DatePickerCustom.DateWatcher;
import com.simp.hms.widget.TimePickerCustom;

import java.util.Calendar;

public class DialogTimeFragment extends DialogFragment implements View.OnClickListener, TimePickerCustom.TimeWatcher {
	private TextView txt_time_title;
	private TimePickerCustom dtp_time_waktu;
	private Button btn_time_cancel;
	private Button btn_time_ok;

	private String title;
	private int hour;
	private int minute;
	private int second;
	private int id;

	private DialogTimeFragmentListener callback;

	public DialogTimeFragment(){}

	public static DialogTimeFragment newInstance(String title, int hour, int minute, int second, int id){
		DialogTimeFragment fragment = new DialogTimeFragment();
		Bundle bundle = new Bundle();

		bundle.putString("title", title);
		bundle.putInt("hour", hour);
		bundle.putInt("minute", minute);
		bundle.putInt("second", second);
		bundle.putInt("id", id);

		fragment.setArguments(bundle);

		return fragment;
	}

//	public DialogTimeFragment(Activity activity, String title, int hour, int minute, int second, int id){
//		this.activity = activity;
//		this.title = title;
//		this.hour = hour;
//		this.minute = minute;
//		this.second = second;
//		this.id = id;
//	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog =  super.onCreateDialog(savedInstanceState);
	
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);	
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.dialog_time, null);
		
		txt_time_title = (TextView) view.findViewById(R.id.txt_time_title);
		dtp_time_waktu = (TimePickerCustom) view.findViewById(R.id.dtp_time_waktu);
		btn_time_cancel = (Button) view.findViewById(R.id.btn_time_cancel);
		btn_time_ok = (Button) view.findViewById(R.id.btn_time_ok);

		Bundle bundle = getArguments();

		if(bundle != null){
			title = bundle.getString("title", "");
			hour = bundle.getInt("hour", 0);
			minute = bundle.getInt("minute", 0);
			second = bundle.getInt("second", 0);
			id = bundle.getInt("id", 0);
		}

		txt_time_title.setText(title);
		
		dtp_time_waktu.setTimeChangedListener(this);
		btn_time_cancel.setOnClickListener(this);
		btn_time_ok.setOnClickListener(this);
		
		return view;
	}


	@Override
	public void onClick(View view) {	
		switch (view.getId()) {
		case R.id.btn_time_cancel:
			dismiss();
			break;
		case R.id.btn_time_ok:
			try{
				callback = (DialogTimeFragmentListener) getTargetFragment();
			}catch(ClassCastException e){
				e.printStackTrace();
				throw e;
			}
			
			if(callback != null){
				callback.onTimeFragmentOK(hour, minute, second, id);
				dismiss();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		dismiss();
	}

	@Override
	public void onTimeChanged(Calendar c) {
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);
		second = c.get(Calendar.SECOND);
	}
}