package com.simp.hms.dialog;

import com.simp.hms.R;
import com.simp.hms.listener.DialogNotificationFragmentListener;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DialogNotificationFragment extends DialogFragment implements android.view.View.OnClickListener {
	private TextView txt_notification_title;
	private TextView txt_notification_message;
	private Button btn_notification_ok;

	private String title;
	private String message;
	private boolean is_finish;
	
	private DialogNotificationFragmentListener callback;

	public DialogNotificationFragment(){}

	public static DialogNotificationFragment newInstance(String title, String message, boolean finish){
		DialogNotificationFragment fragment = new DialogNotificationFragment();
		Bundle bundle = new Bundle();

		bundle.putString("title", title);
		bundle.putString("message", message);
		bundle.putBoolean("finish", finish);

		fragment.setArguments(bundle);

		return fragment;
	}

//	public DialogNotificationFragment(String title, String message, boolean is_finish){
//		this.title = title;
//		this.message = message;
//		this.is_finish = is_finish;
//	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog =  super.onCreateDialog(savedInstanceState);
	
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);	
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.dialog_notification, null);
		
		txt_notification_title = (TextView) view.findViewById(R.id.txt_notification_title);
		txt_notification_message = (TextView) view.findViewById(R.id.txt_notification_message);
		btn_notification_ok = (Button) view.findViewById(R.id.btn_notification_ok);

		Bundle bundle = getArguments();

		if(bundle != null){
			title = bundle.getString("title", "");
			message = bundle.getString("message", "");
			is_finish = bundle.getBoolean("finish", false);
		}

		txt_notification_title.setText(title);
		txt_notification_message.setText(message);
		btn_notification_ok.setOnClickListener(this);
		
		return view;
	}


	@Override
	public void onClick(View view) {	
		switch (view.getId()) {
		case R.id.btn_notification_ok:
			try{
				callback = (DialogNotificationFragmentListener) getTargetFragment();
			}catch(ClassCastException e){
				e.printStackTrace();
				throw e;
			}
			
			if(callback != null){
				callback.onFragmentOK(is_finish);
				dismiss();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		dismiss();
	}
}