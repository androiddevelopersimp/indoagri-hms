package com.simp.hms.dialog;

import java.util.Calendar;
import java.util.Date;

import com.simp.hms.R;
import com.simp.hms.listener.DialogDateFragmentListener;
import com.simp.hms.model.DateLocal;
import com.simp.hms.widget.DatePickerCustom;
import com.simp.hms.widget.DatePickerCustom.DateWatcher;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DialogDateFragment extends DialogFragment implements android.view.View.OnClickListener, DateWatcher {
	private TextView txt_date_title;
	private DatePickerCustom dtp_date_tanggal;
	private Button btn_date_cancel;
	private Button btn_date_ok;

	private String title;
	private Date date;
	private int id;
	
	private DialogDateFragmentListener callback;

	public DialogDateFragment(){}

	public static DialogDateFragment newInstance(String title, int id){
		DialogDateFragment fragment = new DialogDateFragment();
		Bundle bundle = new Bundle();

		bundle.putString("title", title);
		bundle.putInt("id", id);

		fragment.setArguments(bundle);

		return fragment;
	}
	
//	public DialogDateFragment(Activity activity, String title, Date date, int id){
//		this.activity = activity;
//		this.title = title;
//		this.date = date;
//		this.id = id;
//	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog =  super.onCreateDialog(savedInstanceState);
	
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);	
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

		date = new Date();
		
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.dialog_date, null);
		
		txt_date_title = (TextView) view.findViewById(R.id.txt_date_title);
		dtp_date_tanggal = (DatePickerCustom) view.findViewById(R.id.dtp_date_tanggal);
		btn_date_cancel = (Button) view.findViewById(R.id.btn_date_cancel);
		btn_date_ok = (Button) view.findViewById(R.id.btn_date_ok);


		Bundle bundle = getArguments();

		if(bundle != null){
			title = bundle.getString("title", "");
			id = bundle.getInt("id", id);
		}

		txt_date_title.setText(title);
		
		dtp_date_tanggal.setDateChangedListener(this);
		btn_date_cancel.setOnClickListener(this);	
		btn_date_ok.setOnClickListener(this);
		
		return view;
	}


	@Override
	public void onClick(View view) {	
		switch (view.getId()) {
		case R.id.btn_date_cancel:
			dismiss();
			break;
		case R.id.btn_date_ok:
			try{
				callback = (DialogDateFragmentListener) getTargetFragment();
			}catch(ClassCastException e){
				e.printStackTrace();
				throw e;
			}
			
			if(callback != null){
				callback.onDateFragmentOK(date, id);
				dismiss();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		dismiss();
	}



	@Override
	public void onDateChanged(Calendar c) {
		String tanggal;
		DateLocal date_local;
		
//		tanggal = String.valueOf(c.get(Calendar.DAY_OF_MONTH)) + "-" + 
//				 String.valueOf(c.get(Calendar.MONTH) + 1) + "-" +
//				 String.valueOf(c.get(Calendar.YEAR));
		tanggal = String.valueOf(c.get(Calendar.YEAR)) + "-" + String.valueOf(c.get(Calendar.MONTH) + 1) + "-" + String.valueOf(c.get(Calendar.DAY_OF_MONTH));
		
		date_local = new DateLocal(tanggal, DateLocal.FORMAT_INPUT);
		
		Log.d("tag", tanggal);
		
		date = date_local.getDate();	
	}
}