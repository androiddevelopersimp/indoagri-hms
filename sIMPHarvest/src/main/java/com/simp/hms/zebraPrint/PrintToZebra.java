package com.simp.hms.zebraPrint;

import android.app.Activity;
import android.content.Context;

import com.simp.hms.database.DatabaseHandler;

/**
 * Created by Dwi.Fajar on 3/12/2018.
 */

public class PrintToZebra {

    Context context;
    DatabaseHandler database;
    Activity activity;

    public PrintToZebra(Context context, Activity activity){
        this.context = context;
        database = new DatabaseHandler(context);
        this.activity = activity;
    }
}
