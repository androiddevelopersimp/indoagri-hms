package com.simp.hms.fragment;

import com.simp.hms.R;
import com.simp.hms.activity.master.MasterBlksugcActivity;
import com.simp.hms.activity.master.MasterEmployeeActivity;
import com.simp.hms.activity.master.MasterVehicleActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogDateFragment;
import com.simp.hms.dialog.DialogTimeFragment;
import com.simp.hms.enums.EmployeeType;
import com.simp.hms.enums.RunningAccountType;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.listener.DialogDateFragmentListener;
import com.simp.hms.listener.DialogTimeFragmentListener;
import com.simp.hms.model.BLKSUGC;
import com.simp.hms.model.BlockHdrc;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.DeviceAlias;
import com.simp.hms.model.Employee;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.model.SPTA;
import com.simp.hms.model.SPTARunningNumber;
import com.simp.hms.model.Vendor;
import com.simp.hms.routines.Constanta;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SPTA1Fragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener,
		DialogDateFragmentListener, DialogTimeFragmentListener {

	private TextView txtSptaNum;
	private TextView txtSptaDate;
	private TextView txtSptaPetak;
	private RadioGroup rgpSptaQuality;
	private RadioButton rbnSptaQualityPremium;
	private RadioButton rbnSptaQualityKonvensional;
	private RadioGroup rgpSptaCane;
	private RadioButton rbnSptaCaneBc;
	private RadioButton rbnSptaCaneCc;
	private RadioButton rbnSptaCaneLc;
	private TextView txtSptaBurnDate;
	private TextView txtSptaBurnHour;
	private TextView txtSptaChoppedDate;
	private TextView txtSptaChoppedHour;
	private TextView txtSptaLoadDate;
	private TextView txtSptaLoadHour;
	private TextView txtSptaTruckLogo;
	private TextView txtSptaVendor;
	private TextView txtSptaChNo;
	private TextView txtSptaChOperator;
	private TextView txtSptaGlNo;
	private TextView txtSptaGlOperator;

	private String zyear = "";
	private String imei = "";
	private String sptaNum = "";
	private String companyCode = "";
	private String estate = "";
	private String divisi = "";
	private String sptaDate = "";
	private String subDiv = "";
	private String petakId = "";
	private String phase = "";
	private String vendorId = "";
	private String nopol = "";
	private String logo = "";
	private double jarak = 0;
	private String runAcc1 = "";
	private String emplId1 = "";
	private String runAcc2 = "";
	private String emplId2 = "";
	private String choppedDate = "";
	private String choppedHour = "";
	private String burnDate = "";
	private String burnHour = "";
	private String loadDate = "";
	private String loadHour = "";
	private String quality = "";
	private String caneType = "";

	private String month;

	private GetDataAsyncTask getDatTask;
	private SPTARunningNumber sptaNumberMax;

	final int MST_BKLSUGC = 990;
	final int MST_TRUCK_LOGO = 991;
	final int MST_CH_NO = 992;
	final int MST_CH_OPT = 993;
	final int MST_GL_NO = 994;
	final int MST_GL_OPT = 995;

	public SPTA1Fragment(){}

	public static SPTA1Fragment newInstance(String sptaNumber, String companyCode, String estate, String divisi){
		SPTA1Fragment fragment = new SPTA1Fragment();
		Bundle bundle = new Bundle();

		bundle.putString(SPTA.XML_SPTA_NUM, sptaNumber);
		bundle.putString(SPTA.XML_COMPANY_CODE, companyCode);
		bundle.putString(SPTA.XML_ESTATE, estate);
		bundle.putString(SPTA.XML_DIVISI, divisi);

		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_spta1, container, false);

		txtSptaNum = (TextView) view.findViewById(R.id.txtSptaNum);
		txtSptaDate = (TextView) view.findViewById(R.id.txtSptaDate);
		txtSptaPetak = (TextView) view.findViewById(R.id.txtSptaPetak);
		txtSptaPetak.setOnClickListener(this);
		rgpSptaQuality = (RadioGroup) view.findViewById(R.id.rgpSptaQuality);
		rgpSptaQuality.setOnCheckedChangeListener(this);
		rbnSptaQualityPremium = (RadioButton) view.findViewById(R.id.rbnSptaQualityPremium);
		rbnSptaQualityKonvensional = (RadioButton) view.findViewById(R.id.rbnSptaQualityKonvensional);
		rgpSptaCane = (RadioGroup) view.findViewById(R.id.rgpSptaCane);
		rgpSptaCane.setOnCheckedChangeListener(this);
		rbnSptaCaneBc = (RadioButton) view.findViewById(R.id.rbnSptaCaneBc);
		rbnSptaCaneCc = (RadioButton) view.findViewById(R.id.rbnSptaCaneCc);
		rbnSptaCaneLc = (RadioButton) view.findViewById(R.id.rbnSptaCaneLc);
		txtSptaBurnDate  = (TextView) view.findViewById(R.id.txtSptaBurnDate);
		txtSptaBurnDate.setOnClickListener(this);
		txtSptaBurnHour  = (TextView) view.findViewById(R.id.txtSptaBurnHour);
		txtSptaBurnHour.setOnClickListener(this);
		txtSptaChoppedDate = (TextView) view.findViewById(R.id.txtSptaChoppedDate);
		txtSptaChoppedDate.setOnClickListener(this);
		txtSptaChoppedHour = (TextView) view.findViewById(R.id.txtSptaChoppedHour);
		txtSptaChoppedHour.setOnClickListener(this);
		txtSptaLoadDate = (TextView) view.findViewById(R.id.txtSptaLoadDate);
		txtSptaLoadDate.setOnClickListener(this);
		txtSptaLoadHour = (TextView) view.findViewById(R.id.txtSptaLoadHour);
		txtSptaLoadHour.setOnClickListener(this);
		txtSptaTruckLogo = (TextView) view.findViewById(R.id.txtSptaTruckLogo);
		txtSptaTruckLogo.setOnClickListener(this);
		txtSptaVendor = (TextView) view.findViewById(R.id.txtSptaVendor);
		txtSptaChNo = (TextView) view.findViewById(R.id.txtSptaChNo);
		txtSptaChNo.setOnClickListener(this);
		txtSptaChOperator = (TextView) view.findViewById(R.id.txtSptaChOperator);
		txtSptaChOperator.setOnClickListener(this);
		txtSptaGlNo = (TextView) view.findViewById(R.id.txtSptaGlNo);
		txtSptaGlNo.setOnClickListener(this);
		txtSptaGlOperator = (TextView) view.findViewById(R.id.txtSptaGlOperator);
		txtSptaGlOperator.setOnClickListener(this);

		Bundle bundle = getArguments();

		if(bundle != null){
			sptaNum = bundle.getString(SPTA.XML_SPTA_NUM, "");
			companyCode = bundle.getString(SPTA.XML_COMPANY_CODE, "");
			estate = bundle.getString(SPTA.XML_ESTATE, "");
			divisi = bundle.getString(SPTA.XML_DIVISI, "");
		}

		loadData();

		return view;
	}

	private void loadData(){
		getDatTask = new GetDataAsyncTask();
		getDatTask.execute();
	}

	public void cancelTask(){
		if(getDatTask != null && getDatTask.getStatus() != AsyncTask.Status.FINISHED){
			getDatTask.cancel(true);
			getDatTask = null;
		}
	}

	@Override
	public void onClick(View v) {
		DialogDateFragment dialogDateFragment = new DialogDateFragment();
		DialogTimeFragment dialogTimeFragment = new DialogTimeFragment();
		Bundle bundle = new Bundle();

		Calendar cal = Calendar.getInstance(Locale.getDefault());

		switch (v.getId()){
			case R.id.txtSptaPetak:
				startActivityForResult(new Intent(getActivity(), MasterBlksugcActivity.class)
						.putExtra(BLKSUGC.XML_COMPANY_CODE, companyCode)
						.putExtra(BLKSUGC.XML_ESTATE, estate)
						.putExtra(Constanta.SEARCH, true), MST_BKLSUGC);
				break;
			case R.id.txtSptaBurnDate:
				bundle.putString("title", getResources().getString(R.string.tanggal));
				bundle.putInt("id", R.id.txtSptaBurnDate);

				dialogDateFragment.setArguments(bundle);

				dialogDateFragment.setTargetFragment(SPTA1Fragment.this, 1);
				dialogDateFragment.show(getFragmentManager(), "dialog");
				break;
			case R.id.txtSptaBurnHour:
				bundle.putString("title", getResources().getString(R.string.waktu));
				bundle.putInt("hour", cal.get(Calendar.HOUR_OF_DAY));
				bundle.putInt("minute", cal.get(Calendar.MINUTE));
				bundle.putInt("second", cal.get(Calendar.SECOND));
				bundle.putInt("id", R.id.txtSptaBurnHour);

				dialogTimeFragment.setArguments(bundle);

				dialogTimeFragment.setTargetFragment(SPTA1Fragment.this, 1);
				dialogTimeFragment.show(getFragmentManager(), "dialog");
				break;
			case R.id.txtSptaChoppedDate:
				bundle.putString("title", getResources().getString(R.string.tanggal));
				bundle.putInt("id", R.id.txtSptaChoppedDate);

				dialogDateFragment.setArguments(bundle);

				dialogDateFragment.setTargetFragment(SPTA1Fragment.this, 1);
				dialogDateFragment.show(getFragmentManager(), "dialog");
				break;
			case R.id.txtSptaChoppedHour:
				bundle.putString("title", getResources().getString(R.string.waktu));
				bundle.putInt("hour", cal.get(Calendar.HOUR_OF_DAY));
				bundle.putInt("minute", cal.get(Calendar.MINUTE));
				bundle.putInt("second", cal.get(Calendar.SECOND));
				bundle.putInt("id", R.id.txtSptaChoppedHour);

				dialogTimeFragment.setArguments(bundle);

				dialogTimeFragment.setTargetFragment(SPTA1Fragment.this, 1);
				dialogTimeFragment.show(getFragmentManager(), "dialog");
				break;
			case R.id.txtSptaLoadDate:
				bundle.putString("title", getResources().getString(R.string.tanggal));
				bundle.putInt("id", R.id.txtSptaLoadDate);

				dialogDateFragment.setArguments(bundle);


				dialogDateFragment.setTargetFragment(SPTA1Fragment.this, 1);
				dialogDateFragment.show(getFragmentManager(), "dialog");
				break;
			case R.id.txtSptaLoadHour:
				bundle.putString("title", getResources().getString(R.string.waktu));
				bundle.putInt("hour", cal.get(Calendar.HOUR_OF_DAY));
				bundle.putInt("minute", cal.get(Calendar.MINUTE));
				bundle.putInt("second", cal.get(Calendar.SECOND));
				bundle.putInt("id", R.id.txtSptaLoadHour);

				dialogTimeFragment.setArguments(bundle);

				dialogTimeFragment.setTargetFragment(SPTA1Fragment.this, 1);
				dialogTimeFragment.show(getFragmentManager(), "dialog");
				break;
			case R.id.txtSptaTruckLogo:
				startActivityForResult(new Intent(getActivity(), MasterVehicleActivity.class)
						.putExtra(RunningAccount.XML_COMPANY_CODE, companyCode)
						.putExtra(RunningAccount.XML_ESTATE, estate)
						.putExtra(Constanta.SEARCH, true)
						.putExtra(Constanta.TYPE, RunningAccountType.TRUCK_LOGO.getId()), MST_TRUCK_LOGO);
				break;
			case R.id.txtSptaChNo:
				startActivityForResult(new Intent(getActivity(), MasterVehicleActivity.class)
						.putExtra(RunningAccount.XML_COMPANY_CODE, companyCode)
						.putExtra(RunningAccount.XML_ESTATE, estate)
						.putExtra(RunningAccount.XML_OWNERSHIPFLAG, "I")
						.putExtra(Constanta.SEARCH, true)
						.putExtra(Constanta.TYPE, RunningAccountType.CH_GL.getId()), MST_CH_NO);
				break;
			case R.id.txtSptaChOperator:
				startActivityForResult(new Intent(getActivity(), MasterEmployeeActivity.class)
								.putExtra(Employee.XML_COMPANY_CODE, companyCode)
								.putExtra(Employee.XML_ESTATE, estate)
								.putExtra(Employee.XML_DIVISION, divisi)
								.putExtra(Constanta.SEARCH, true)
								.putExtra(Constanta.TYPE, EmployeeType.OPERATOR.getId()), MST_CH_OPT);
				break;
			case R.id.txtSptaGlNo:
				startActivityForResult(new Intent(getActivity(), MasterVehicleActivity.class)
						.putExtra(RunningAccount.XML_COMPANY_CODE, companyCode)
						.putExtra(RunningAccount.XML_ESTATE, estate)
						.putExtra(RunningAccount.XML_OWNERSHIPFLAG, "I")
						.putExtra(Constanta.SEARCH, true)
						.putExtra(Constanta.TYPE, RunningAccountType.CH_GL.getId()), MST_GL_NO);
				break;
			case R.id.txtSptaGlOperator:
				startActivityForResult(new Intent(getActivity(), MasterEmployeeActivity.class)
								.putExtra(Employee.XML_COMPANY_CODE, companyCode)
								.putExtra(Employee.XML_ESTATE, estate)
								.putExtra(Employee.XML_DIVISION, divisi)
								.putExtra(Constanta.SEARCH, true)
								.putExtra(Constanta.TYPE, EmployeeType.OPERATOR.getId()), MST_GL_OPT);
				break;
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId){
			case R.id.rbnSptaQualityPremium:
				quality = getResources().getString(R.string.spta_quality_premium);
				break;
			case R.id.rbnSptaQualityKonvensional:
				quality = getResources().getString(R.string.spta_quality_konvensional);
				break;
			case R.id.rbnSptaCaneBc:
				caneType = getResources().getString(R.string.spta_cane_bc);
				break;
			case R.id.rbnSptaCaneCc:
				caneType = getResources().getString(R.string.spta_cane_cc);
				break;
			case R.id.rbnSptaCaneLc:
				caneType = getResources().getString(R.string.spta_cane_lc);
				break;
		}
	}

	@Override
	public void onDateFragmentOK(Date date, int id) {
		switch (id){
			case R.id.txtSptaBurnDate:
				burnDate = new DateLocal(date).getDateString(DateLocal.FORMAT_DATE_ONLY);
				txtSptaBurnDate.setText(new DateLocal(date).getDateString(DateLocal.FORMAT_VIEW));
				break;
			case R.id.txtSptaChoppedDate:
				choppedDate = new DateLocal(date).getDateString(DateLocal.FORMAT_DATE_ONLY);
				txtSptaChoppedDate.setText(new DateLocal(date).getDateString(DateLocal.FORMAT_VIEW));
				break;
			case R.id.txtSptaLoadDate:
				loadDate = new DateLocal(date).getDateString(DateLocal.FORMAT_DATE_ONLY);
				txtSptaLoadDate.setText(new DateLocal(date).getDateString(DateLocal.FORMAT_VIEW));
		}
	}

	@Override
	public void onTimeFragmentOK(int hour, int minute, int second, int id) {
		switch (id) {
			case R.id.txtSptaBurnHour:
				burnHour = hour + ":" + minute;
				txtSptaBurnHour.setText(burnHour);
				break;
			case R.id.txtSptaChoppedHour:
				choppedHour = hour + ":" + minute;
				txtSptaChoppedHour.setText(choppedHour);
				break;
			case R.id.txtSptaLoadHour:
				loadHour = hour + ":" + minute;
				txtSptaLoadHour.setText(loadHour);
				break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(resultCode == getActivity().RESULT_OK){
			if(requestCode == MST_BKLSUGC){
				petakId = data.getExtras().getString(BLKSUGC.XML_BLOCK, "");
				phase = data.getExtras().getString(BLKSUGC.XML_PHASE, "");
				subDiv = data.getExtras().getString(BLKSUGC.XML_SUB_DIVISION, "");
				jarak = data.getExtras().getDouble(BLKSUGC.XML_DISTANCE, 0);

				String hektar = GetHektar(subDiv);
				hektar = TextUtils.isEmpty(hektar) ? "" : " - " +hektar;

				txtSptaPetak.setText(petakId + " - " + phase + " - " + subDiv + hektar);
			}else if(requestCode == MST_TRUCK_LOGO){
				logo = data.getExtras().getString(RunningAccount.XML_RUNNING_ACCOUNT, "");
				nopol = data.getExtras().getString(RunningAccount.XML_LICENSE_PLATE, "");
				String estate = data.getExtras().getString(RunningAccount.XML_ESTATE, "");
				String lifnr = data.getExtras().getString(RunningAccount.XML_LIFNR, "");

				txtSptaTruckLogo.setText(logo + " - " + nopol);

				Vendor vendor = getVendor(estate, lifnr);

				if(vendor != null){
					vendorId = vendor.getLifnr();
					txtSptaVendor.setText(vendorId + " - " + vendor.getName());
				}
			}else if(requestCode == MST_CH_NO) {
				runAcc1 = data.getExtras().getString(RunningAccount.XML_RUNNING_ACCOUNT, "");

				txtSptaChNo.setText(runAcc1);
			}else if(requestCode == MST_CH_OPT){
				emplId1 = data.getExtras().getString(Employee.XML_NIK, "");
				String name = data.getExtras().getString(Employee.XML_NAME, "");

				txtSptaChOperator.setText(emplId1 + " - " + name);
			}else if(requestCode == MST_GL_NO){
				runAcc2 = data.getExtras().getString(RunningAccount.XML_RUNNING_ACCOUNT, "");

				txtSptaGlNo.setText(runAcc2);
			}else if(requestCode == MST_GL_OPT){
				emplId2 = data.getExtras().getString(Employee.XML_NIK, "");
				String name = data.getExtras().getString(Employee.XML_NAME, "");

				txtSptaGlOperator.setText(emplId2 + " - " + name);
			}
		}
	}

	private class GetDataAsyncTask extends AsyncTask<Void, SPTA, SPTA>{

		@Override
		protected SPTA doInBackground(Void... params) {
			DatabaseHandler database = new DatabaseHandler(getActivity());

			database.openTransaction();
			SPTA spta = (SPTA) database.getDataFirst(false, SPTA.TABLE_NAME, null,
					SPTA.XML_SPTA_NUM + " = ? " + " and " +
							SPTA.XML_COMPANY_CODE + " = ? " + " and " +
							SPTA.XML_ESTATE + " = ? ",
					new String[]{sptaNum, companyCode, estate},
					null, null, null, null);
			database.closeTransaction();

			return spta;
		}

		@Override
		protected void onPostExecute(SPTA spta) {
			super.onPostExecute(spta);

			if(spta != null){
				zyear = spta.getZyear();
				imei = spta.getImei();
				sptaNum = spta.getSptaNum();
				companyCode = spta.getCompanyCode();
				estate = spta.getEstate();
				divisi = spta.getDivisi();
				sptaDate = spta.getSptaDate();
				subDiv = spta.getSubDiv();
				petakId = spta.getPetakId();
				phase = "";
				vendorId = spta.getVendorId();
				nopol = spta.getNopol();
				logo = spta.getLogo();
				jarak = 0;
				runAcc1 = spta.getRunAcc1();
				emplId1 = spta.getEmplId1();
				runAcc2 = spta.getRunAcc2();
				emplId2 = spta.getEmplId2();
				choppedDate = spta.getChoppedDate();
				choppedHour = spta.getChoppedHour();
				burnDate = spta.getBurnDate();
				burnHour = spta.getBurnHour();
				loadDate = spta.getLoadDate();
				loadHour = spta.getLoadHour();
				quality = spta.getQuality();
				caneType = spta.getCaneType();
			}else{
				imei = new DeviceHandler(getActivity()).getImei();
				zyear = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_YEAR_ONLY);
				month = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_MONTH_ONLY);
				sptaNumberMax = getAlias(estate, divisi, zyear, month, imei);

				if(sptaNumberMax != null) {
					int no = new Converter(sptaNumberMax.getRunningNumber()).StrToInt() + 1;
					sptaNum = estate + divisi + month + zyear.substring(2, 3) + sptaNumberMax.getDeviceAlias() +
							new Converter(String.valueOf(no)).StrToDigit(4);
				}

				sptaDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_DATE_ONLY);
				txtSptaDate.setText(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_VIEW));

				quality = getResources().getString(R.string.spta_quality_premium);
				caneType = getResources().getString(R.string.spta_cane_bc);
			}

			txtSptaNum.setText(sptaNum);
			displayDate(txtSptaDate, sptaDate);
			displayPetak();
			displayQuality();
			displayCaneType();
			displayDate(txtSptaBurnDate, burnDate);
			txtSptaBurnHour.setText(burnHour);
			displayDate(txtSptaChoppedDate, choppedDate);
			txtSptaChoppedHour.setText(choppedHour);
			displayDate(txtSptaLoadDate, loadDate);
			txtSptaLoadHour.setText(loadHour);
			txtSptaTruckLogo.setText(logo + " - " + nopol);
			txtSptaVendor.setText(vendorId);
			txtSptaChNo.setText(runAcc1);
			displayChOperator();
			txtSptaGlNo.setText(runAcc2);
			displayGlOperator();
		}
	}

	private String GetHektar(String subDiv){
		String block = "";
		DatabaseHandler database = new DatabaseHandler(getActivity());

		try {
			database.openTransaction();

			BlockHdrc blockHdrc = (BlockHdrc) database.getDataFirst(false, BlockHdrc.TABLE_NAME, null,
					BlockHdrc.XML_COMPANY_CODE + " = ? " + " and " +
					BlockHdrc.XML_ESTATE + " = ? " + " and " +
					BlockHdrc.XML_BLOCK + " = ?",
					new String[] {companyCode, estate, subDiv},
					null, null, null, null);

			if(blockHdrc != null){
				block = blockHdrc.getBlock();
			}
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}

		return block;
	}

	private SPTARunningNumber getAlias(String estate, String division, String year, String month, String imei){
		DatabaseHandler database = new DatabaseHandler(getActivity());
		SPTARunningNumber sptaNumberMax = null;

		database.openTransaction();
		DeviceAlias deviceAlias = (DeviceAlias) database.getDataFirst(false, DeviceAlias.TABLE_NAME,
				null, null, null, null, null, null, null);
		database.closeTransaction();

		if(deviceAlias == null){
			database.openTransaction();
			sptaNumberMax = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
					SPTARunningNumber.XML_ESTATE + "=?" + " and " +
							SPTARunningNumber.XML_YEAR + "=?" + " and " +
							SPTARunningNumber.XML_MONTH + "=?" + " and " +
							SPTARunningNumber.XML_IMEI + "=?",
					new String [] {estate, year, month, imei},
					null, null, SPTARunningNumber.XML_ID + " desc", null);
			database.closeTransaction();

			if(sptaNumberMax == null){
				database.openTransaction();
				SPTARunningNumber tempNumber = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
						SPTARunningNumber.XML_IMEI + "=?",
						new String [] {imei},
						null, null, SPTARunningNumber.XML_ID + " desc", null);
				database.closeTransaction();

				if(tempNumber != null){
					String alias = tempNumber.getDeviceAlias();
					int id = tempNumber.getId() + 1;
					sptaNumberMax = new SPTARunningNumber(0, id, estate, division, year, month, imei, "0", alias);
				}
			}
		}else{
			database.openTransaction();
			sptaNumberMax = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
					SPTARunningNumber.XML_ESTATE + "=?" + " and " +
							SPTARunningNumber.XML_YEAR + "=?" + " and " +
							SPTARunningNumber.XML_MONTH + "=?" + " and " +
							SPTARunningNumber.XML_IMEI + "=?" + " and " +
							SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
					new String [] {estate, year, month, imei, deviceAlias.getDeviceAlias()},
					null, null, SPTARunningNumber.XML_ID + " desc", null);
			database.closeTransaction();

			if(sptaNumberMax == null){
				database.openTransaction();
				SPTARunningNumber tempNumber = (SPTARunningNumber) database.getDataFirst(false, SPTARunningNumber.TABLE_NAME, null,
						SPTARunningNumber.XML_IMEI + "=?" + " and " +
								SPTARunningNumber.XML_DEVICE_ALIAS + "=?",
						new String [] {imei, deviceAlias.getDeviceAlias()},
						null, null, SPTARunningNumber.XML_ID + " desc", null);
				database.closeTransaction();

				if(tempNumber != null){
					String alias = tempNumber.getDeviceAlias();
					int id = tempNumber.getId() + 1;
					sptaNumberMax = new SPTARunningNumber(0, id, estate, division, year, month, imei, "0", alias);
				}
			}
		}

		return sptaNumberMax;
	}

	public SPTA getData(){
		SPTA spta = new SPTA();

		spta.setZyear(zyear);
		spta.setImei(imei);
		spta.setSptaNum(sptaNum);
		spta.setSptaDate(sptaDate);
		spta.setCompanyCode(companyCode);
		spta.setEstate(estate);
		spta.setDivisi(divisi);
		spta.setSubDiv(subDiv);
		spta.setPetakId(petakId);
		spta.setVendorId(vendorId);
		spta.setNopol(nopol);
		spta.setLogo(logo);
		spta.setJarak(jarak);
		spta.setRunAcc1(runAcc1);
		spta.setEmplId1(emplId1);
		spta.setRunAcc2(runAcc2);
		spta.setEmplId2(emplId2);
		spta.setChoppedDate(choppedDate);
		spta.setChoppedHour(choppedHour);
		spta.setBurnDate(burnDate);
		spta.setBurnHour(burnHour);
		spta.setLoadDate(loadDate);
		spta.setLoadHour(loadHour);
		spta.setQuality(quality);
		spta.setCaneType(caneType);

		return spta;
	}

	private void displayPetak(){
		BLKSUGC blksugc = getBLKSUGC(petakId, companyCode, estate);
		String hektar = GetHektar(subDiv);
		hektar = TextUtils.isEmpty(hektar) ? "" : " - " +hektar;

		if(blksugc != null) txtSptaPetak.setText(petakId + " - " + blksugc.getPhase() + " - " + subDiv + hektar);
	}

	private void displayQuality(){
		if(!TextUtils.isEmpty(quality)){
			if(quality.equalsIgnoreCase(getResources().getString(R.string.spta_quality_premium))){
				rbnSptaQualityPremium.setChecked(true);
			}else{
				rbnSptaQualityKonvensional.setChecked(true);
			}
		}
	}

	private void displayCaneType(){
		if(!TextUtils.isEmpty(caneType)){
			if(caneType.equalsIgnoreCase(getResources().getString(R.string.spta_cane_bc))){
				rbnSptaCaneBc.setChecked(true);
			}else if(caneType.equalsIgnoreCase(getResources().getString(R.string.spta_cane_cc))){
				rbnSptaCaneCc.setChecked(true);
			}else{
				rbnSptaCaneLc.setChecked(true);
			}
		}
	}

	private void displayChOperator(){
		Employee employee = getEmployee(emplId1);

		if(employee != null) txtSptaChOperator.setText(emplId1 + " - " + employee.getName());
	}

	private void displayGlOperator(){
		Employee employee = getEmployee(emplId2);

		if(employee != null) txtSptaGlOperator.setText(emplId2 + " - " + employee.getName());
	}

	private void displayDate(TextView txt, String date){
		if(!TextUtils.isEmpty(date)){
			txt.setText(new DateLocal(date, DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		}
	}

	public SPTARunningNumber getSptaNumberMax(){
		return sptaNumberMax;
	}

	public void updateSPTANum(String sptaNum){
		txtSptaNum.setText(sptaNum);
	}

	private BLKSUGC getBLKSUGC(String petakId, String companyCode, String estate){
		BLKSUGC blksugc = null;
		DatabaseHandler database = new DatabaseHandler(getActivity());

		try{
			database.openTransaction();

			blksugc = (BLKSUGC) database.getDataFirst(false, BLKSUGC.TABLE_NAME, null,
					BLKSUGC.XML_BLOCK + " = ? " + " and " +
					BLKSUGC.XML_COMPANY_CODE + " = ? " + "  and " +
					BLKSUGC.XML_ESTATE + " = ? ",
					new String[] {petakId, companyCode, estate},
					null, null, null, null);
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}

		return blksugc;
	}

	private Employee getEmployee(String nik){
		Employee employee = null;
		DatabaseHandler database  = new DatabaseHandler(getActivity());

		try{
			database.openTransaction();
			employee = (Employee) database.getDataFirst(false, Employee.TABLE_NAME, null,
					Employee.XML_NIK + " = ?",
					new String[] {nik},
					null, null, null, null);

		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}

		return employee;
	}

	private Vendor getVendor(String estate, String lifnr){
		DatabaseHandler database = new DatabaseHandler(getActivity());
		Vendor vendor = null;

		try{
			database.openTransaction();
			vendor = (Vendor) database.getDataFirst(false, Vendor.TABLE_NAME, null,
					Vendor.XML_ESTATE + " = ? " + " and " +
							Vendor.XML_LIFNR + " = ?",
					new String[] {estate, lifnr},
					null, null, null, null);
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}

		return vendor;
	}
}
