package com.simp.hms.fragment;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simp.hms.R;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirmFragment;
import com.simp.hms.dialog.DialogDateFragment;
import com.simp.hms.dialog.DialogNotificationFragment;
import com.simp.hms.dialog.DialogSpbsDetailFragment;
import com.simp.hms.listener.DialogConfirmFragmentListener;
import com.simp.hms.listener.DialogDateFragmentListener;
import com.simp.hms.listener.DialogNotificationFragmentListener;
import com.simp.hms.listener.DialogSpbsDetailFragmentListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.Penalty;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSLineSummary;
import com.simp.hms.routines.Utils;
import com.simp.hms.service.GPSService;
import com.simp.hms.widget.EditTextCustom;
import com.simp.hms.adapter.view.ViewSpbsBlockItem;

import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SPBSBlockFragment extends Fragment implements OnClickListener, DialogConfirmFragmentListener, 
			DialogDateFragmentListener, DialogNotificationFragmentListener, DialogSpbsDetailFragmentListener{
	
	private LinearLayout lnrSpbsBlock;
	private Button btnSpbsBlockAddTph;
	
	String imei = "";
	String year = "";
	String companyCode = "";
	String estate = "";
	String spbsNumber = "";
	String spbsDate = "";
	String block = "";
	String crop = "";
	String clerk = "";
	String gpsKoordinat = "0.0:0.0";
	int status = 0;
	String bpnDate = "";
	long todayDate = 0;
	double latitude = 0;
	double longitude = 0;
	
	DatabaseHandler database;
	HashMap<String, SPBSLineSummary> mapSpbsLineSummary = new HashMap<String, SPBSLineSummary>();
	GPSService gps;
	
	String selectedId = "";
	View selectedChild = null;
	
	final String TPH_TANGKAPAN = "888";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view  = inflater.inflate(R.layout.fragment_spbs_block, container, false);
		
		lnrSpbsBlock = (LinearLayout) view.findViewById(R.id.lnrSpbsBlock);
		btnSpbsBlockAddTph = (Button) view.findViewById(R.id.btnSpbsBlockAddTph);
		
		try{
			Bundle bundle = getArguments();
			if(bundle != null){
				imei = bundle.getString(SPBSLine.XML_IMEI);
				year = bundle.getString(SPBSLine.XML_YEAR);
				companyCode = bundle.getString(SPBSLine.XML_COMPANY_CODE);
				estate = bundle.getString(SPBSLine.XML_ESTATE);
				spbsNumber = bundle.getString(SPBSLine.XML_SPBS_NUMBER);
				spbsDate = bundle.getString(SPBSLine.XML_SPBS_DATE);
				block = bundle.getString(SPBSLine.XML_BLOCK);
				crop = bundle.getString(SPBSLine.XML_CROP);
				clerk = bundle.getString(SPBSHeader.XML_CLERK);
				status = bundle.getInt(SPBSLine.XML_STATUS);
			}
		}catch(NullPointerException e){
			e.printStackTrace();
		}
		
		bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
		todayDate = new Date().getTime();
		
		gps = new GPSService(getActivity());
		database = new DatabaseHandler(getActivity().getApplicationContext());
		
		btnSpbsBlockAddTph.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		
		getTPH();
	}

	private String getGpsKoordinat(){
		String gpsKoordinat = "0.0:0.0";
		
		if(gps != null){
			Location location = gps.getLocation();
			
			if(location != null){
				latitude = location.getLatitude();
				longitude = location.getLongitude();
			}
			
			gpsKoordinat = latitude + ":" + longitude;
		}
		
		return gpsKoordinat;
	}
	
	private void getTPH(){
		mapSpbsLineSummary = new HashMap<String, SPBSLineSummary>();
		lnrSpbsBlock.removeAllViews();
		
		database.openTransaction();
		List<SPBSLineSummary> listSpbsLineSummary = database.getListTPH(imei, year, companyCode, estate, spbsNumber, spbsDate, block, crop);
		database.closeTransaction();

		if(listSpbsLineSummary.size() > 0){
			for(int i = 0; i < listSpbsLineSummary.size(); i++){
				SPBSLineSummary spbsLineSummary = (SPBSLineSummary) listSpbsLineSummary.get(i);
				
				addChild(spbsLineSummary);
			}
		}
	}
	
	private void addChild(final SPBSLineSummary spbsLineSummary){
		Button btnSpbsBlockItemDelete;
		TextView txtSpbsBlockItemTph;
		TextView txtSpbsBlockItemBpnDate;
		EditTextCustom edtSpbsBlockItemQtyJanjang;
		EditTextCustom edtSpbsBlockItemQtyJanjangAngkut;
		EditTextCustom edtSpbsBlockItemQtyLooseFruit;
		EditTextCustom edtSpbsBlockItemQtyLooseFruitAngkut;
		Button btnSpbsBlockItemWarning;
		
		final View child = getActivity().getLayoutInflater().inflate(R.layout.item_spbs_block, null);		
		
		child.setTag(spbsLineSummary);
		
		btnSpbsBlockItemDelete = (Button) child.findViewById(R.id.btnSpbsBlockItemDelete);
		txtSpbsBlockItemTph = (TextView) child.findViewById(R.id.txtSpbsBlockItemTph);
		txtSpbsBlockItemBpnDate = (TextView) child.findViewById(R.id.txtSpbsBlockItemBpnDate);
		edtSpbsBlockItemQtyJanjang = (EditTextCustom) child.findViewById(R.id.edtSpbsBlockItemQtyJanjang);
		edtSpbsBlockItemQtyJanjangAngkut = (EditTextCustom) child.findViewById(R.id.edtSpbsBlockItemQtyJanjangAngkut);
		edtSpbsBlockItemQtyLooseFruit = (EditTextCustom) child.findViewById(R.id.edtSpbsBlockItemQtyLooseFruit);
		edtSpbsBlockItemQtyLooseFruitAngkut = (EditTextCustom) child.findViewById(R.id.edtSpbsBlockItemQtyLooseFruitAngkut);
		btnSpbsBlockItemWarning = (Button) child.findViewById(R.id.btnSpbsBlockItemWarning);
		  
		final ViewSpbsBlockItem view = new ViewSpbsBlockItem(btnSpbsBlockItemDelete, txtSpbsBlockItemTph, txtSpbsBlockItemBpnDate, 
				edtSpbsBlockItemQtyJanjang, edtSpbsBlockItemQtyJanjangAngkut, edtSpbsBlockItemQtyLooseFruit, edtSpbsBlockItemQtyLooseFruitAngkut, btnSpbsBlockItemWarning);
		
		view.getTxtSpbsBlockItemTph().setText(spbsLineSummary.getTph());
		view.getTxtSpbsBlockItemBpnDate().setText(spbsLineSummary.getBpnDate());
		view.getTxtSpbsBlockItemTph().setImeOptions(EditorInfo.IME_ACTION_NEXT);
		view.getEdtSpbsBlockItemQtyJanjang().setImeOptions(EditorInfo.IME_ACTION_NEXT);
		view.getEdtSpbsBlockItemQtyLooseFruit().setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		view.getEdtSpbsBlockItemQtyJanjang().setText(spbsLineSummary.getQtyJanjang() > 0 ? String.valueOf((int) spbsLineSummary.getQtyJanjang()) : "");
		view.getEdtSpbsBlockItemQtyJanjangAngkut().setText(spbsLineSummary.getQtyJanjangAngkut() > 0 ? String.valueOf((int) spbsLineSummary.getQtyJanjangAngkut()) : "");
		view.getEdtSpbsBlockItemQtyLooseFruit().setText(spbsLineSummary.getQtyLooseFruit() > 0 ? String.valueOf(Utils.round(spbsLineSummary.getQtyLooseFruit(), 2)) : "");
		view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setText(spbsLineSummary.getQtyLooseFruitAngkut() > 0 ? String.valueOf(Utils.round(spbsLineSummary.getQtyLooseFruitAngkut(), 2)) : "");

		if(!TextUtils.isEmpty(spbsLineSummary.getSpbsNext())){
			btnSpbsBlockItemDelete.setEnabled(false);
			edtSpbsBlockItemQtyJanjangAngkut.setEnabled(false);
			edtSpbsBlockItemQtyLooseFruitAngkut.setEnabled(false);
		}

		view.getBtnSpbsBlockItemDelete().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(status == 0){
					selectedId = String.valueOf(spbsLineSummary.getId());
					selectedChild = child;
					
					view.getTxtSpbsBlockItemTph().clearFocus();
					view.getEdtSpbsBlockItemQtyJanjang().clearFocus();
					view.getEdtSpbsBlockItemQtyJanjangAngkut().clearFocus();
					view.getEdtSpbsBlockItemQtyLooseFruit().clearFocus();
					view.getEdtSpbsBlockItemQtyLooseFruitAngkut().clearFocus();
					
					DialogConfirmFragment dialogConfirmFragment = new DialogConfirmFragment();

					Bundle bundle = new Bundle();

					bundle.putString("title", getResources().getString(R.string.informasi));
					bundle.putString("message", getResources().getString(R.string.data_delete));
					bundle.putInt("id", R.id.btnSpbsBlockItemDelete);

					dialogConfirmFragment.setArguments(bundle);

					dialogConfirmFragment.setTargetFragment(SPBSBlockFragment.this, 1);
					dialogConfirmFragment.show(getFragmentManager(), "dialog");
				}else{
					DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

					Bundle bundle = new Bundle();

					bundle.putString("title", getResources().getString(R.string.informasi));
					bundle.putString("message", getResources().getString(R.string.data_already_export));
					bundle.putBoolean("finish", false);

					dialogNotification.setArguments(bundle);

					dialogNotification.setTargetFragment(SPBSBlockFragment.this, 1);
					dialogNotification.show(getFragmentManager(), "dialog");
				}
			}
		});
		
		view.getTxtSpbsBlockItemTph().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String bpnId = spbsLineSummary.getBpnId();

				DialogSpbsDetailFragment dialogSpbsDetailFragment = new DialogSpbsDetailFragment();

				Bundle bundle = new Bundle();

				bundle.putString("title", getResources().getString(R.string.informasi));
				bundle.putString("bpnId", bpnId);
				bundle.putBoolean("finish", false);

				dialogSpbsDetailFragment.setArguments(bundle);

				dialogSpbsDetailFragment.setTargetFragment(SPBSBlockFragment.this, 1);
				dialogSpbsDetailFragment.show(getFragmentManager(), "dialog");
				
			}
		});
		
		view.getTxtSpbsBlockItemBpnDate().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				if(status == 0){
					selectedId = String.valueOf(spbsLineSummary.getId());
					selectedChild = child;
					
					DialogDateFragment dialogDateFragment = new DialogDateFragment();

					Bundle bundle = new Bundle();

					bundle.putString("title", getResources().getString(R.string.tanggal));
					bundle.putInt("id", R.id.txtSpbsBlockItemBpnDate);

					dialogDateFragment.setArguments(bundle);

					dialogDateFragment.setTargetFragment(SPBSBlockFragment.this, 1);
					dialogDateFragment.show(getFragmentManager(), "dialog");
				}else{
					DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

					Bundle bundle = new Bundle();

					bundle.putString("title", getResources().getString(R.string.informasi));
					bundle.putString("message", getResources().getString(R.string.data_already_export));
					bundle.putBoolean("finish", false);

					dialogNotification.setArguments(bundle);

					dialogNotification.setTargetFragment(SPBSBlockFragment.this, 1);
					dialogNotification.show(getFragmentManager(), "dialog");
				}
			}
		});
		
		view.getEdtSpbsBlockItemQtyJanjangAngkut().addTextChangedListener(new TextWatcher() {
			String before = "";
			String after = "";
			
			@Override
			public void onTextChanged(CharSequence charSeq, int arg1, int arg2, int arg3) {}
			
			@Override
			public void beforeTextChanged(CharSequence charSeq, int arg1, int arg2, int arg3) {
				before = charSeq.toString();
			}
			
			@Override
			public void afterTextChanged(Editable editebale) {
				view.getEdtSpbsBlockItemQtyJanjangAngkut().removeTextChangedListener(this);
				
				after = editebale.toString();
				
				if(status == 0){
					String tph = spbsLineSummary.getTphOriginal();  ; //view.getTxtSpbsBlockItemTph().getText().toString().trim();
					if(TextUtils.isEmpty(tph))
						tph = spbsLineSummary.getTph();

					double qtyJanjang = (double) Math.abs(new Converter(view.getEdtSpbsBlockItemQtyJanjang().getText().toString().trim()).StrToDouble());
					double qtyJanjangAngkut = (double) Math.abs(new Converter(editebale.toString().trim()).StrToDouble());
					double qtyLooseFruit = Math.abs(new Converter(view.getEdtSpbsBlockItemQtyLooseFruit().getText().toString().trim()).StrToDouble());
					double qtyLooseFruitAngkut = Math.abs(new Converter(view.getEdtSpbsBlockItemQtyLooseFruitAngkut().getText().toString().trim()).StrToDouble());
					
					if(!TextUtils.isEmpty(tph)){
//						boolean isQtyJanjangValid = isTphQtyValid(block, tph, crop, spbsLineSummary.getBpnDate(), BPNQuantity.JANJANG_CODE, qtyJanjang);
//						boolean isQtyLooseFruitValid = isTphQtyValid(block, tph, crop, spbsLineSummary.getBpnDate(), BPNQuantity.LOOSE_FRUIT_CODE, qtyLooseFruit);
						
						boolean isQtyJanjangValid = qtyJanjangAngkut <= qtyJanjang;
						boolean isQtyLooseFruitValid = qtyLooseFruitAngkut <= qtyLooseFruitAngkut;
						
						if(tph.equalsIgnoreCase(TPH_TANGKAPAN)){
							isQtyJanjangValid = true;
							isQtyLooseFruitValid = true;
						}
						
						if(isQtyJanjangValid && isQtyLooseFruitValid){
							try{
								long todayDate = new Date().getTime();
								//spbsLineSummary.setQtyJanjang(qtyJanjang);
								
								database.openTransaction();
								database.deleteData(SPBSLine.TABLE_NAME, 
										SPBSLine.XML_ID + "=?" + " and " +
										SPBSLine.XML_YEAR + "=?" + " and " +
										SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
										SPBSLine.XML_ESTATE + "=?" + " and " +
										SPBSLine.XML_CROP + "=?" + " and " +
										SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
										SPBSLine.XML_BLOCK + "=?" + " and " + 
										SPBSLine.XML_TPH + "=?" + " and " +
										SPBSLine.XML_BPN_DATE + "=?", 
										new String [] {String.valueOf(spbsLineSummary.getId()), year, companyCode, estate, crop, spbsNumber, block, tph, spbsLineSummary.getBpnDate()});

								double qtyJanjangRemaining = (double) 0;
								double qtyLooseFruitRemaining = (double)0;
								if(spbsLineSummary.getTph().equalsIgnoreCase("L")) {
									qtyJanjangRemaining = (double) (qtyJanjang - qtyJanjangAngkut);
									qtyLooseFruitRemaining = (double) (qtyLooseFruit - qtyLooseFruitAngkut);
								}

								database.setData(new SPBSLine(0, spbsLineSummary.getId(), imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
										tph, spbsLineSummary.getBpnDate(), BPNQuantity.JANJANG_CODE, qtyJanjang, qtyJanjangAngkut, 0, "EA", spbsLineSummary.getGpsKoordinat(),
										spbsLineSummary.getIsSave(), 0, spbsLineSummary.getBpnId(), spbsLineSummary.getSpbsRef(),  spbsLineSummary.getSpbsNext(), todayDate, clerk, todayDate, clerk, qtyJanjangRemaining));
								database.setData(new SPBSLine(0, spbsLineSummary.getId(), imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block, 
										tph, spbsLineSummary.getBpnDate(), BPNQuantity.LOOSE_FRUIT_CODE, qtyLooseFruit, qtyLooseFruitAngkut, 0, "Kg", spbsLineSummary.getGpsKoordinat(), 
										spbsLineSummary.getIsSave(), 0, spbsLineSummary.getBpnId(), spbsLineSummary.getSpbsRef(), spbsLineSummary.getSpbsNext(),todayDate, clerk, todayDate, clerk, qtyLooseFruitRemaining));
								
								database.commitTransaction();
							}catch(SQLiteException e){
								e.printStackTrace();
								database.closeTransaction();
							}finally{
								database.closeTransaction();
							}
							
							view.getBtnSpbsBlockItemWarning().setVisibility(View.INVISIBLE);
						}else{
							view.getBtnSpbsBlockItemWarning().setVisibility(View.VISIBLE);
						}
					}
				}else{   
					if(!before.equals(after)){
						view.getEdtSpbsBlockItemQtyJanjangAngkut().setText(before);
						view.getEdtSpbsBlockItemQtyJanjangAngkut().setSelection(before.length());
						
						DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

						Bundle bundle = new Bundle();

						bundle.putString("title",  getResources().getString(R.string.informasi));
						bundle.putString("message", getResources().getString(R.string.data_already_export));
						bundle.putBoolean("finish", false);

						dialogNotification.setArguments(bundle);

						dialogNotification.setTargetFragment(SPBSBlockFragment.this, 1);
						dialogNotification.show(getFragmentManager(), "dialog");
					}
				}
				
				view.getEdtSpbsBlockItemQtyJanjangAngkut().addTextChangedListener(this);
			}
		});
		
		view.getEdtSpbsBlockItemQtyJanjangAngkut().setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean arg1) {
				String tph = view.getTxtSpbsBlockItemTph().getText().toString();
				
				if(!tph.equalsIgnoreCase(TPH_TANGKAPAN)){
					String value = view.getEdtSpbsBlockItemQtyJanjangAngkut().getText().toString().trim();
					int qtyJanjang = (int) Math.abs(new Converter(view.getEdtSpbsBlockItemQtyJanjang().getText().toString().trim()).StrToDouble());
					int qtyJanjangAngkut = (int) Math.abs(new Converter(value).StrToDouble());
					
					qtyJanjangAngkut = qtyJanjangAngkut <= qtyJanjang ? qtyJanjangAngkut : qtyJanjang;
					
					if(qtyJanjangAngkut > 0){
						view.getEdtSpbsBlockItemQtyJanjangAngkut().setText(String.valueOf(qtyJanjangAngkut));
					}else{
						view.getEdtSpbsBlockItemQtyJanjangAngkut().setText("");
					}
					
					view.getEdtSpbsBlockItemQtyJanjangAngkut().setSelection(view.getEdtSpbsBlockItemQtyJanjangAngkut().getText().length());
				}
			}
		});
		
		view.getEdtSpbsBlockItemQtyLooseFruitAngkut().addTextChangedListener(new TextWatcher() {
			String before = "";
			String after = "";
			
			@Override
			public void onTextChanged(CharSequence charSeq, int arg1, int arg2, int arg3) {}
			
			@Override
			public void beforeTextChanged(CharSequence charSeq, int arg1, int arg2, int arg3) {
				before = charSeq.toString();
			}
			
			@Override
			public void afterTextChanged(Editable editebale) {
				view.getEdtSpbsBlockItemQtyLooseFruitAngkut().removeTextChangedListener(this);
				
				
				String value;
				
				after = editebale.toString().trim();
				value = after;
				
				if(!TextUtils.isEmpty(value)){				
					value = value.replace("-", "");
					
					if(checkDot(value) > 1){
						value = before;
					}
					
					if(decimalPlaces(value, 2) > 2){
						value = value.substring(0, value.length() - 1);
					}
					
					view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setText(value);
				}
				
				if(status == 0) {
					String tph = view.getTxtSpbsBlockItemTph().getText().toString().trim();
					int qtyJanjang = (int) Math.abs(new Converter(view.getEdtSpbsBlockItemQtyJanjang().getText().toString().trim()).StrToDouble());
					int qtyJanjangAngkut = (int) Math.abs(new Converter(view.getEdtSpbsBlockItemQtyJanjangAngkut().getText().toString().trim()).StrToDouble());
					double qtyLooseFruit = Math.abs(new Converter(view.getEdtSpbsBlockItemQtyLooseFruit().getText().toString().trim()).StrToDouble());
					double qtyLooseFruitAngkut = Math.abs(new Converter(value).StrToDouble());
					
					if(!TextUtils.isEmpty(tph)){
//						boolean isQtyJanjangValid = isTphQtyValid(block, tph, crop, spbsLineSummary.getBpnDate(), BPNQuantity.JANJANG_CODE, qtyJanjang);
//						boolean isQtyLooseFruitValid = isTphQtyValid(block, tph, crop, spbsLineSummary.getBpnDate(), BPNQuantity.LOOSE_FRUIT_CODE, qtyLooseFruit);
						
						boolean isQtyJanjangValid = qtyJanjangAngkut <= qtyJanjang;
						boolean isQtyLooseFruitValid = qtyLooseFruitAngkut == qtyLooseFruit || qtyLooseFruitAngkut == 0;
						
						if(tph.equalsIgnoreCase(TPH_TANGKAPAN)){
							isQtyJanjangValid = true;
							isQtyLooseFruitValid = true;
						}
						
						if(isQtyJanjangValid && isQtyLooseFruitValid){
							try{
								long todayDate = new Date().getTime();
								spbsLineSummary.setQtyLooseFruit(qtyLooseFruit);
								
								database.openTransaction();
								database.deleteData(SPBSLine.TABLE_NAME,
										SPBSLine.XML_ID + "=?" + " and " +
										SPBSLine.XML_YEAR + "=?" + " and " +
										SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
										SPBSLine.XML_ESTATE + "=?" + " and " +
										SPBSLine.XML_CROP + "=?" + " and " +
										SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
										SPBSLine.XML_BLOCK + "=?" + " and " + 
										SPBSLine.XML_TPH + "=?" + " and " +
										SPBSLine.XML_BPN_DATE + "=?", 
										new String [] {String.valueOf(spbsLineSummary.getId()), year, companyCode, estate, crop, spbsNumber, block, tph, spbsLineSummary.getBpnDate()});

								if(!spbsLineSummary.getTph().equalsIgnoreCase("L")) {
									database.setData(new SPBSLine(0, spbsLineSummary.getId(), imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
											tph, spbsLineSummary.getBpnDate(), BPNQuantity.JANJANG_CODE, qtyJanjang, qtyJanjangAngkut, 0, "EA", spbsLineSummary.getGpsKoordinat(),
											spbsLineSummary.getIsSave(), 0, spbsLineSummary.getBpnId(), spbsLineSummary.getSpbsRef(), spbsLineSummary.getSpbsNext(), todayDate, clerk, todayDate, clerk, 0));
									database.setData(new SPBSLine(0, spbsLineSummary.getId(), imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block,
											tph, spbsLineSummary.getBpnDate(), BPNQuantity.LOOSE_FRUIT_CODE, qtyLooseFruit, qtyLooseFruitAngkut, 0, "Kg", spbsLineSummary.getGpsKoordinat(),
											spbsLineSummary.getIsSave(), 0, spbsLineSummary.getBpnId(), spbsLineSummary.getSpbsRef(), spbsLineSummary.getSpbsNext(), todayDate, clerk, todayDate, clerk, 0));

								}


								database.commitTransaction();
							}catch(SQLiteException e){
								e.printStackTrace();
								database.closeTransaction();
							}finally{
								database.closeTransaction();  
							}
							
							view.getBtnSpbsBlockItemWarning().setVisibility(View.INVISIBLE);
						}else{
							view.getBtnSpbsBlockItemWarning().setVisibility(View.VISIBLE);
						}
					}
				}else{
					if(!before.equals(value)){  
						view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setText(before);
						view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setSelection(before.length());
						
						DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

						Bundle bundle = new Bundle();

						bundle.putString("title",  getResources().getString(R.string.informasi));
						bundle.putString("message", getResources().getString(R.string.data_already_export));
						bundle.putBoolean("finish", false);

						dialogNotification.setArguments(bundle);

						dialogNotification.setTargetFragment(SPBSBlockFragment.this, 1);
						dialogNotification.show(getFragmentManager(), "dialog");
					}
				}
				
				view.getEdtSpbsBlockItemQtyLooseFruitAngkut().addTextChangedListener(this);
			}
		});  
		
		view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean arg1) {
				String tph = view.getTxtSpbsBlockItemTph().getText().toString();
				
				if(!tph.equalsIgnoreCase(TPH_TANGKAPAN)){
					String value = view.getEdtSpbsBlockItemQtyLooseFruitAngkut().getText().toString().trim();
					double qtyLooseFruitAngkut = Math.abs(new Converter(value).StrToDouble());
					
					if(qtyLooseFruitAngkut > 0){
						view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setText(String.valueOf(qtyLooseFruitAngkut));
					}else{
						view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setText("");
					}
					
					view.getEdtSpbsBlockItemQtyLooseFruitAngkut().setSelection(view.getEdtSpbsBlockItemQtyLooseFruitAngkut().getText().length());
				}
			}
		});
		
		view.getBtnSpbsBlockItemWarning().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

				Bundle bundle = new Bundle();

				bundle.putString("title",  getResources().getString(R.string.informasi));
				bundle.putString("message", getResources().getString(R.string.Invalid_data));
				bundle.putBoolean("finish", false);

				dialogNotification.setArguments(bundle);

				dialogNotification.setTargetFragment(SPBSBlockFragment.this, 1);
				dialogNotification.show(getFragmentManager(), "dialog");
			}
		});

		mapSpbsLineSummary.put(String.valueOf(spbsLineSummary.getId()), spbsLineSummary);
		lnrSpbsBlock.addView(child);
		
		int index = lnrSpbsBlock.indexOfChild(child);
		child.setTag(index);
		
		view.getTxtSpbsBlockItemTph().requestFocus();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnSpbsBlockAddTph:
			int newId = 0;
			boolean found = false;
			
			for(Map.Entry<String, SPBSLineSummary> entry: mapSpbsLineSummary.entrySet()){
				SPBSLineSummary spbsLine = entry.getValue();
				
				if(spbsLine.getTphOriginal().equalsIgnoreCase(TPH_TANGKAPAN) ||
						spbsLine.getTph().equalsIgnoreCase(TPH_TANGKAPAN)){
					found = true;
					break;
				}
			}
			
			if(!found){
				if(mapSpbsLineSummary.size() > 0){
					for(Map.Entry<String, SPBSLineSummary> entry: mapSpbsLineSummary.entrySet()){
						SPBSLineSummary spbsLine = entry.getValue();
						
						if(newId < spbsLine.getId()){
							newId = spbsLine.getId();
						}
					}
				}
				
				newId = newId + 1;
	
				addChild(new SPBSLineSummary(newId, TPH_TANGKAPAN, "", new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT), "", "", 0, 0, 0, 0, getGpsKoordinat(), 0, ""));
			}else{
				DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

				Bundle bundle = new Bundle();

				bundle.putString("title",  getResources().getString(R.string.informasi));
				bundle.putString("message", getResources().getString(R.string.spbs_add_tph_error));
				bundle.putBoolean("finish", false);

				dialogNotification.setArguments(bundle);

				dialogNotification.setTargetFragment(SPBSBlockFragment.this, 1);
				dialogNotification.show(getFragmentManager(), "dialog");
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onConfirmFragmentOK(int id) {
		switch (id) {
		case R.id.btnSpbsBlockItemDelete:
			
			if(!TextUtils.isEmpty(selectedId) && selectedChild != null){
				SPBSLineSummary spbsLineSummary = mapSpbsLineSummary.get(selectedId);  

				if(spbsLineSummary !=null) {
					try {
						database.openTransaction();

						String bpnId = spbsLineSummary.getBpnId();
						String bpnDate = spbsLineSummary.getBpnDate();
						String tph = spbsLineSummary.getTph();
						String spbsRef = spbsLineSummary.getSpbsRef();
						String spbsNext = spbsLineSummary.getSpbsNext();

						boolean del = false;

						if (TextUtils.isEmpty(spbsNext)) {
							if (!TextUtils.isEmpty(spbsRef)) {
								String spbsNumber = spbsRef.split("_")[0];
								String lineId = spbsRef.split("_")[1];

								List<Object> listObjectRef = database.getListData(false, SPBSLine.TABLE_NAME, null,
										SPBSLine.XML_ID + " = ? " + " and " +
												SPBSLine.XML_SPBS_NUMBER + " = ? ",
										new String[]{lineId, spbsNumber},
										null, null, null, null);

								if (listObjectRef != null && listObjectRef.size() > 0) {
									for (int i = 0; i < listObjectRef.size(); i++) {
										SPBSLine spbsLineRef = (SPBSLine) listObjectRef.get(i);

										if (!TextUtils.isEmpty(spbsLineRef.getSpbsNext())) {
											if (spbsLineRef.getAchievementCode().equalsIgnoreCase("01"))
												spbsLineRef.setQuantityRemaining((double) (spbsLineRef.getQuantityRemaining() + spbsLineSummary.getQtyJanjangAngkut()));
											else if (spbsLineRef.getAchievementCode().equalsIgnoreCase("03"))
												spbsLineRef.setQuantityRemaining((double) (spbsLineRef.getQuantityRemaining() + spbsLineSummary.getQtyLooseFruitAngkut()));

											spbsLineRef.setSpbsNext("");
										}

										database.updateData(spbsLineRef,
												SPBSLine.XML_ID + " = ? " + " and " +
														SPBSLine.XML_SPBS_NUMBER + " = ?" + " and " +
														SPBSLine.XML_ACHIEVEMENT_CODE + " =?",
												new String[]{lineId, spbsNumber, spbsLineRef.getAchievementCode()});

										del = true;
									}
								}
							} else {
								if (!TextUtils.isEmpty(bpnId)) {
								/*BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
									BPNHeader.XML_BPN_ID + "=?", 
									new String [] {bpnId}, 
									null, null, null, null);
								
								if(bpnHeader != null){
									bpnHeader.setSpbsNumber("");
									
									database.updateData(bpnHeader, 
											BPNHeader.XML_BPN_ID + "=?", 
											new String [] {bpnId});

									del = true;
								}*/

									//Fernando, New Load SPBS - Delete Item
									double qtyAngkutRemaining = 0;
									BPNQuantity bpnQuantityAngkut = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
											BPNQuantity.XML_BPN_ID + "=?" + " and " +
													BPNQuantity.XML_BPN_DATE + "=?" + " and " +
													BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
													BPNQuantity.XML_ESTATE + "=?" + " and " +
													BPNQuantity.XML_LOCATION + "=?" + " and " +
													BPNQuantity.XML_TPH + "=?" + " and " +
													BPNQuantity.XML_CROP + "=?" + " and " +
													BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
											new String[]{bpnId, bpnDate, companyCode, estate, block, tph, crop, "01"},
											null, null, null, null);

									if (bpnQuantityAngkut != null) {
										List<Object> listObjectSPBSCurrentAngkut = database.getListData(false, SPBSLine.TABLE_NAME, null,
												SPBSLine.XML_BPN_ID + " = ? " + " and " +
														SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
														SPBSLine.XML_ESTATE + "=?" + " and " +
														SPBSLine.XML_TPH + "=?" + " and " +
														SPBSLine.XML_CROP + "=?" + " and " +
														SPBSLine.XML_SPBS_REF + "=?" + " and " +
														SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
												new String[]{bpnId, companyCode, estate, tph, crop, "", "01"},
												null, null, null, null);

										double qtyCurrentAngkut = 0;
										if (listObjectSPBSCurrentAngkut.size() > 0) {
											for (int i = 0; i < listObjectSPBSCurrentAngkut.size(); i++) {
												SPBSLine spbsLineCurrentAngkut = (SPBSLine) listObjectSPBSCurrentAngkut.get(i);
												qtyCurrentAngkut = (double) (qtyCurrentAngkut + spbsLineCurrentAngkut.getQuantityAngkut());
											}
										}

										double penalty = getPenalty(bpnId, companyCode, estate, bpnDate, bpnQuantityAngkut.getDivision(), bpnQuantityAngkut.getGang(), block, BPNQuantity.JANJANG_CODE);
										qtyAngkutRemaining = (double) ((bpnQuantityAngkut.getQuantity() - penalty) - qtyCurrentAngkut) + spbsLineSummary.getQtyJanjangAngkut();
										bpnQuantityAngkut.setQuantityRemaining(qtyAngkutRemaining);
										bpnQuantityAngkut.setModifiedDate(new Date().getTime());
										bpnQuantityAngkut.setModifiedBy(clerk);
										bpnQuantityAngkut.setModifiedDateStr(new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT));

										database.updateData(bpnQuantityAngkut,
												BPNQuantity.XML_BPN_ID + "=?" + " and " +
														BPNQuantity.XML_BPN_DATE + "=?" + " and " +
														BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
														BPNQuantity.XML_ESTATE + "=?" + " and " +
														BPNQuantity.XML_LOCATION + "=?" + " and " +
														BPNQuantity.XML_TPH + "=?" + " and " +
														BPNQuantity.XML_CROP + "=?" + " and " +
														BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
												new String[]{bpnId, bpnDate, companyCode, estate, block, tph, crop, "01"});

									}

									double qtyLooseFruitRemaining = 0;
									BPNQuantity bpnQuantityLooseFruit = (BPNQuantity) database.getDataFirst(false, BPNQuantity.TABLE_NAME, null,
											BPNQuantity.XML_BPN_ID + "=?" + " and " +
													BPNQuantity.XML_BPN_DATE + "=?" + " and " +
													BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
													BPNQuantity.XML_ESTATE + "=?" + " and " +
													BPNQuantity.XML_LOCATION + "=?" + " and " +
													BPNQuantity.XML_TPH + "=?" + " and " +
													BPNQuantity.XML_CROP + "=?" + " and " +
													BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
											new String[]{bpnId, bpnDate, companyCode, estate, block, tph, crop, "03"},
											null, null, null, null);

									if (bpnQuantityLooseFruit != null) {
										List<Object> listObjectSPBSCurrentAngkut = database.getListData(false, SPBSLine.TABLE_NAME, null,
												SPBSLine.XML_BPN_ID + " = ? " + " and " +
														SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
														SPBSLine.XML_ESTATE + "=?" + " and " +
														SPBSLine.XML_TPH + "=?" + " and " +
														SPBSLine.XML_CROP + "=?" + " and " +
														SPBSLine.XML_SPBS_REF + "=?" + " and " +
														SPBSLine.XML_ACHIEVEMENT_CODE + "=?",
												new String[]{bpnId, companyCode, estate, tph, crop, "", "03"},
												null, null, null, null);

										double qtyCurrentAngkut = 0;
										if (listObjectSPBSCurrentAngkut.size() > 0) {
											for (int i = 0; i < listObjectSPBSCurrentAngkut.size(); i++) {
												SPBSLine spbsLineCurrentAngkut = (SPBSLine) listObjectSPBSCurrentAngkut.get(i);
												qtyCurrentAngkut = (double) (qtyCurrentAngkut + spbsLineCurrentAngkut.getQuantityAngkut());
											}
										}

										qtyLooseFruitRemaining = (double) (bpnQuantityLooseFruit.getQuantity() - qtyCurrentAngkut) + spbsLineSummary.getQtyLooseFruitAngkut();

										database.updateData(bpnQuantityLooseFruit,
												BPNQuantity.XML_BPN_ID + "=?" + " and " +
														BPNQuantity.XML_BPN_DATE + "=?" + " and " +
														BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
														BPNQuantity.XML_ESTATE + "=?" + " and " +
														BPNQuantity.XML_LOCATION + "=?" + " and " +
														BPNQuantity.XML_TPH + "=?" + " and " +
														BPNQuantity.XML_CROP + "=?" + " and " +
														BPNQuantity.XML_ACHIEVEMENT_CODE + "=?",
												new String[]{bpnId, bpnDate, companyCode, estate, block, tph, crop, "03"});
									}

									del = true;
								}else{
									del = true;
								}
							}

							if (del) {
								tph = spbsLineSummary.getTphOriginal();
								if (TextUtils.isEmpty(tph))
									tph = spbsLineSummary.getTph();

								database.deleteData(SPBSLine.TABLE_NAME,
										SPBSLine.XML_ID + "=?" + " and " +
												SPBSLine.XML_YEAR + "=?" + " and " +
												SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
												SPBSLine.XML_ESTATE + "=?" + " and " +
												SPBSLine.XML_CROP + "=?" + " and " +
												SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
												SPBSLine.XML_SPBS_DATE + "=?" + " and " +
												SPBSLine.XML_BLOCK + "=?" + " and " +
												SPBSLine.XML_TPH + "=?" + " and " +
												SPBSLine.XML_BPN_ID + "=?" + " and " +
												SPBSLine.XML_BPN_DATE + "=?",
										new String[]{String.valueOf(spbsLineSummary.getId()), year, companyCode, estate, crop, spbsNumber, spbsDate, block, tph, bpnId, spbsLineSummary.getBpnDate()});

								database.commitTransaction();

							}

							lnrSpbsBlock.removeView(selectedChild);
							mapSpbsLineSummary.remove(selectedId);
						}

					} catch (SQLiteException e) {
						e.printStackTrace();
						database.closeTransaction();
					} finally {
						database.closeTransaction();
					}
				}
			}
			   
			break;

		default:
			break;
		}
	}

	private double getPenalty(String bpnId, String companyCode, String estate, String bpnDate, String division, String gang, String block, String achievementCode){
		double qty = 0;

		List<Object> lstPenalty = database.getListData(false, Penalty.TABLE_NAME, null,
				Penalty.XML_IS_LOADING + "=?",
				new String [] {"0"},
				null, null, null, null);

		if(lstPenalty != null && lstPenalty.size() > 0){
			for(Object obj : lstPenalty){
				Penalty penalty = (Penalty) obj;

				if(penalty != null){

					BPNQuality bpnQuality =  (BPNQuality) database.getDataFirst(false, BPNQuality.TABLE_NAME, null,
							BPNQuality.XML_BPN_ID + "=?" + " and " +
									BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
									BPNQuality.XML_ESTATE + "=?" + " and " +
									BPNQuality.XML_BPN_DATE + "=?" + " and " +
									BPNQuality.XML_DIVISION + "=?" + " and " +
									BPNQuality.XML_GANG + "=?" + " and " +
									BPNQuality.XML_LOCATION + "=?" + " and " +
									BPNQuality.XML_ACHIEVEMENT_CODE + "=?" + " and " +
									BPNQuality.XML_QUALITY_CODE + "=?",
							new String [] {bpnId, companyCode, estate, bpnDate, division, gang, block, achievementCode, penalty.getPenaltyCode()},
							null, null, null, null);

					if(bpnQuality != null)
						qty = qty + bpnQuality.getQuantity();
				}
			}
		}

		return qty;
	}

	@Override
	public void onDateFragmentOK(Date date, int id) {
		SPBSLineSummary spbsLineSummary = mapSpbsLineSummary.get(selectedId);
		
		String tph = spbsLineSummary.getTphOriginal();
		if(TextUtils.isEmpty(tph))
			tph = spbsLineSummary.getTph();

		double qtyJanjang = spbsLineSummary.getQtyJanjang();
		double qtyJanjangAngkut = spbsLineSummary.getQtyJanjangAngkut();
		double qtyLooseFruit = spbsLineSummary.getQtyLooseFruit();
		double qtyLooseFruitAngkut = spbsLineSummary.getQtyLooseFruitAngkut();
		String gpsKoordinat = spbsLineSummary.getGpsKoordinat();
		String bpnDateCurrent = spbsLineSummary.getBpnDate();
		String bpnDateNew = new DateLocal(date).getDateString(DateLocal.FORMAT_INPUT);
		int isSave = spbsLineSummary.getIsSave();
		
		if(!TextUtils.isEmpty(tph) && !tph.equalsIgnoreCase("0")){
			try{
				database.openTransaction();
//				database.deleteData(SPBSLine.TABLE_NAME, 
//						SPBSLine.XML_ID + "=?" + " and " +
//						SPBSLine.XML_IMEI + "=?" + " and " +
//						SPBSLine.XML_YEAR + "=?" + " and " +
//						SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
//						SPBSLine.XML_ESTATE + "=?" + " and " +
//						SPBSLine.XML_CROP + "=?" + " and " +
//						SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
//						SPBSLine.XML_SPBS_DATE + "=?" + " and " +
//						SPBSLine.XML_BLOCK + "=?" + " and " + 
//						SPBSLine.XML_TPH + "=?" + " and " +
//						SPBSLine.XML_BPN_DATE + "=?", 
//						new String [] {String.valueOf(spbsLineSummary.getId()), imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block, tph, bpnDateCurrent});

				database.deleteData(SPBSLine.TABLE_NAME, 
						SPBSLine.XML_ID + "=?" + " and " +
						SPBSLine.XML_YEAR + "=?" + " and " +
						SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
						SPBSLine.XML_ESTATE + "=?" + " and " +
						SPBSLine.XML_CROP + "=?" + " and " +
						SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
						SPBSLine.XML_SPBS_DATE + "=?" + " and " +
						SPBSLine.XML_BLOCK + "=?" + " and " + 
						SPBSLine.XML_TPH + "=?" + " and " +
						SPBSLine.XML_BPN_DATE + "=?", 
						new String [] {String.valueOf(spbsLineSummary.getId()), year, companyCode, estate, crop, spbsNumber, spbsDate, block, tph, bpnDateCurrent});

				
				database.setData(new SPBSLine(0, spbsLineSummary.getId(), imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block, 
						tph, bpnDateNew, BPNQuantity.JANJANG_CODE, qtyJanjang, qtyJanjangAngkut, 0, "EA", gpsKoordinat, isSave, 0, "", "", "", todayDate, clerk, 0, "", 0));
				database.setData(new SPBSLine(0, spbsLineSummary.getId(), imei, year, companyCode, estate, crop, spbsNumber, spbsDate, block, 
						tph, bpnDateNew, BPNQuantity.LOOSE_FRUIT_CODE, qtyLooseFruit, qtyLooseFruitAngkut, 0, "Kg", gpsKoordinat, isSave, 0, "", "", "", todayDate, clerk, 0, "", 0));
				
				database.commitTransaction();
				
				spbsLineSummary.setBpnDate(bpnDateNew);
				mapSpbsLineSummary.put(selectedId, spbsLineSummary);
				
				TextView txt = (TextView) selectedChild.findViewById(R.id.txtSpbsBlockItemBpnDate);
				txt.setText(bpnDateNew);
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();

				DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

				Bundle bundle = new Bundle();

				bundle.putString("title",  getResources().getString(R.string.informasi));
				bundle.putString("message", e.getMessage());
				bundle.putBoolean("finish", false);

				dialogNotification.setArguments(bundle);

				dialogNotification.setTargetFragment(SPBSBlockFragment.this, 1);
				dialogNotification.show(getFragmentManager(), "dialog");
			}finally{
				database.closeTransaction();
			}
		}
	}

	@Override
	public void onFragmentOK(boolean is_finish) {
		
	}
	
	private boolean isTphQtyValid(String block, String tph, String crop, String bpnDate, String achivementCode, double qty){
		double qtyBpn = 0;
		double qtySpbs = 0;
		
		database.openTransaction();
		List<Object> listBpn = database.getListData(false, BPNQuantity.TABLE_NAME, 
				new String [] {BPNQuantity.XML_TPH, "SUM(" + BPNQuantity.XML_QUANTITY + ") as " + BPNQuantity.XML_QUANTITY}, 
				BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
				BPNQuantity.XML_ESTATE + "=?" + " and " +
				BPNQuantity.XML_BPN_DATE + "=?" + " and " +
				BPNQuantity.XML_LOCATION + "=?" + " and " +
				BPNQuantity.XML_TPH + "=?" + " and " +
				BPNQuantity.XML_CROP + "=?" + " and " +
				BPNQuantity.XML_ACHIEVEMENT_CODE + "=?", 
				new String [] {companyCode, estate, bpnDate, block, tph, crop, achivementCode}, 
				BPNQuantity.XML_TPH, null, null, null);
		
		if(listBpn.size() > 0){
			for(int i = 0; i < listBpn.size(); i++){
				BPNQuantity bpnQuantity = (BPNQuantity) listBpn.get(i);
				
				qtyBpn = qtyBpn + bpnQuantity.getQuantity();
			}
		}
		
//		List<Object> listSpbs = database.getListData(false, SPBSLine.TABLE_NAME, 
//				new String [] {SPBSLine.XML_TPH, "SUM(" + SPBSLine.XML_QUANTITY + ") as " + SPBSLine.XML_QUANTITY}, 
//				SPBSLine.XML_IMEI + "=?" + " and " +
//				SPBSLine.XML_YEAR + "=?" + " and " +
//				SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
//				SPBSLine.XML_ESTATE + "=?" + " and " +
//				SPBSLine.XML_BPN_DATE + "=?" + " and " +
//				SPBSLine.XML_BLOCK + "=?" + " and " +
//				SPBSLine.XML_TPH + "=?" + " and " +
//				SPBSLine.XML_CROP + "=?" + " and " +
//				SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
//				SPBSLine.XML_SPBS_NUMBER + "!=?", 
//				new String [] {imei, year, companyCode, estate, bpnDate, block, tph, crop, achivementCode, spbsNumber}, 
//				SPBSLine.XML_TPH, null, null, null);
		
		List<Object> listSpbs = database.getListData(false, SPBSLine.TABLE_NAME, 
				new String [] {SPBSLine.XML_TPH, "SUM(" + SPBSLine.XML_QUANTITY + ") as " + SPBSLine.XML_QUANTITY}, 
				SPBSLine.XML_YEAR + "=?" + " and " +
				SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
				SPBSLine.XML_ESTATE + "=?" + " and " +
				SPBSLine.XML_BPN_DATE + "=?" + " and " +
				SPBSLine.XML_BLOCK + "=?" + " and " +
				SPBSLine.XML_TPH + "=?" + " and " +
				SPBSLine.XML_CROP + "=?" + " and " +
				SPBSLine.XML_ACHIEVEMENT_CODE + "=?" + " and " +
				SPBSLine.XML_SPBS_NUMBER + "!=?", 
				new String [] {year, companyCode, estate, bpnDate, block, tph, crop, achivementCode, spbsNumber}, 
				SPBSLine.XML_TPH, null, null, null);
		
		database.closeTransaction();
		
		if(listSpbs.size() > 0){
			for(int i = 0; i < listSpbs.size(); i++){
				SPBSLine spbsLine = (SPBSLine) listSpbs.get(i);
				
				qtySpbs = qtySpbs + spbsLine.getQuantity();
			}
		}
		
		Log.d("tag", ":" + qtyBpn);
		Log.d("tag", ":" + qtySpbs);
		
		if(qtyBpn > 0){
			if(qty <= Math.abs((qtyBpn - qtySpbs))){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	private int checkDot(String value){
		int countDot = 0;
		
		for(int i = 0; i < value.length(); i++){
			char c = value.charAt(i);
			
			if(c == '.'){
				countDot++;
			}
		}
		
		return countDot;
	}
	
	private int decimalPlaces(String value, int digit){
		if(value.indexOf(".") > 0){
			String temp = value.substring(value.indexOf(".") + 1, value.length());
			
			return temp.length();
		}else{
			return 0;
		}
	}

	@Override
	public void onSpbsDetailFragmentOK(boolean is_finish) {
		// TODO Auto-generated method stub
		
	}
}
