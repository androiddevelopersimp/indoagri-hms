package com.simp.hms.fragment;

import com.simp.hms.R;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.handler.DeviceHandler;
import com.simp.hms.model.Converter;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPTA;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.Date;

public class SPTA2Fragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

	private CheckBox cbxSptaIncentiveGulma;
	private CheckBox cbxSptaIncentiveLangsir;
	private CheckBox cbxSptaIncentiveRoboh;
	private CheckBox cbxSptaCostTebang;
	private CheckBox cbxSptaCostMuat;
	private CheckBox cbxSptaCostAngkut;
	private CheckBox cbxSptaPenaltyTrash;

	private String sptaNum = "";
	private String companyCode = "";
	private String estate = "";
	private String divisi = "";
	private int insentiveGulma = 0;
	private int insentiveLangsir = 0;
	private int insentiveRoboh = 0;
	private String caneType = "";
	private int costTebang = 0;
	private int costMuat = 0;
	private int costAngkut = 0;
	private int penaltyTrash = 0;

	private GetDataAsyncTask getDatTask;

	public SPTA2Fragment(){}

	public static SPTA2Fragment newInstance(String sptaNumber, String companyCode, String estate, String divisi){
		SPTA2Fragment fragment = new SPTA2Fragment();
		Bundle bundle = new Bundle();

		bundle.putString(SPTA.XML_SPTA_NUM, sptaNumber);
		bundle.putString(SPTA.XML_COMPANY_CODE, companyCode);
		bundle.putString(SPTA.XML_ESTATE, estate);
		bundle.putString(SPTA.XML_DIVISI, divisi);

		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_spta2, container, false);

		cbxSptaIncentiveGulma = (CheckBox) view.findViewById(R.id.cbxSptaIncentiveGulma);
		cbxSptaIncentiveGulma.setOnCheckedChangeListener(this);
		cbxSptaIncentiveLangsir = (CheckBox) view.findViewById(R.id.cbxSptaIncentiveLangsir);
		cbxSptaIncentiveLangsir.setOnCheckedChangeListener(this);
		cbxSptaIncentiveRoboh = (CheckBox) view.findViewById(R.id.cbxSptaIncentiveRoboh);
		cbxSptaIncentiveRoboh.setOnCheckedChangeListener(this);
		cbxSptaCostTebang = (CheckBox) view.findViewById(R.id.cbxSptaCostTebang);
		cbxSptaCostTebang.setOnCheckedChangeListener(this);
		cbxSptaCostMuat = (CheckBox) view.findViewById(R.id.cbxSptaCostMuat);
		cbxSptaCostMuat.setOnCheckedChangeListener(this);
		cbxSptaCostAngkut = (CheckBox) view.findViewById(R.id.cbxSptaCostAngkut);
		cbxSptaCostAngkut.setOnCheckedChangeListener(this);
		cbxSptaPenaltyTrash = (CheckBox) view.findViewById(R.id.cbxSptaPenaltyTrash);
		cbxSptaPenaltyTrash.setOnCheckedChangeListener(this);

		Bundle bundle = getArguments();

		if(bundle != null){
			sptaNum = bundle.getString(SPTA.XML_SPTA_NUM, "");
			companyCode = bundle.getString(SPTA.XML_COMPANY_CODE, "");
			estate = bundle.getString(SPTA.XML_ESTATE, "");
			divisi = bundle.getString(SPTA.XML_DIVISI, "");
		}

		loadData();

		return view;
	}

	private void loadData(){
		getDatTask = new GetDataAsyncTask();
		getDatTask.execute();
	}

	public void cancelTask(){
		if(getDatTask != null && getDatTask.getStatus() != AsyncTask.Status.FINISHED){
			getDatTask.cancel(true);
			getDatTask = null;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()){
			case R.id.cbxSptaIncentiveGulma:
				insentiveGulma = isChecked ? 1 : 0;
				break;
			case R.id.cbxSptaIncentiveLangsir:
				insentiveLangsir = isChecked ? 1 : 0;
				break;
			case R.id.cbxSptaIncentiveRoboh:
				insentiveRoboh = isChecked ? 1 : 0;
				break;
			case R.id.cbxSptaCostTebang:
				costTebang = isChecked ? 1 : 0;
				break;
			case R.id.cbxSptaCostMuat:
				costMuat = isChecked ? 1 : 0;
				break;
			case R.id.cbxSptaCostAngkut:
				costAngkut = isChecked ? 1 : 0;
				break;
			case R.id.cbxSptaPenaltyTrash:
				penaltyTrash = isChecked ? 1 : 0;
				break;
		}
	}

	private class GetDataAsyncTask extends AsyncTask<Void, SPTA, SPTA>{

		@Override
		protected SPTA doInBackground(Void... params) {
			DatabaseHandler database = new DatabaseHandler(getActivity());

			database.openTransaction();
			SPTA spta = (SPTA) database.getDataFirst(false, SPTA.TABLE_NAME, null,
					SPTA.XML_SPTA_NUM + " = ? " + " and " +
							SPTA.XML_COMPANY_CODE + " = ? " + " and " +
							SPTA.XML_ESTATE + " = ? ",
					new String[]{sptaNum, companyCode, estate},
					null, null, null, null);
			database.closeTransaction();

			return spta;
		}

		@Override
		protected void onPostExecute(SPTA spta) {
			super.onPostExecute(spta);

			if(spta != null){
				insentiveGulma = spta.getInsentiveGulma();
				insentiveLangsir = spta.getInsentiveLangsir();
				insentiveRoboh = spta.getInsentiveRoboh();

				costTebang = spta.getCostTebang();
				costMuat = spta.getCostMuat();
				costAngkut = spta.getCostAngkut();

				penaltyTrash = spta.getPenaltyTrash();
			}else{
				insentiveGulma = 0;
				insentiveLangsir = 0;
				insentiveRoboh = 0;

				costTebang = 0;
				costMuat = 0;
				costAngkut = 0;

				penaltyTrash = 0;
			}

			cbxSptaIncentiveGulma.setChecked(insentiveGulma == 1 ? true : false);
			cbxSptaIncentiveLangsir.setChecked(insentiveLangsir == 1 ? true : false);
			cbxSptaIncentiveRoboh.setChecked(insentiveRoboh == 1 ? true : false);
			cbxSptaCostTebang.setChecked(costTebang == 1 ? true : false);
			cbxSptaCostMuat.setChecked(costMuat == 1 ? true : false);
			cbxSptaCostAngkut.setChecked(costAngkut == 1 ? true : false);
			cbxSptaPenaltyTrash.setChecked(penaltyTrash == 1 ? true : false);

		}
	}

	public SPTA getData(){

		SPTA spta = new SPTA();

		spta.setInsentiveGulma(insentiveGulma);
		spta.setInsentiveLangsir(insentiveLangsir);
		spta.setInsentiveRoboh(insentiveRoboh);
		spta.setCostTebang(costTebang);
		spta.setCostMuat(costMuat);
		spta.setCostAngkut(costAngkut);
		spta.setPenaltyTrash(penaltyTrash);

		return  spta;
	}
}
