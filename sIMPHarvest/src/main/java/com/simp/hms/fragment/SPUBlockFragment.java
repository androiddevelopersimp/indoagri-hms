package com.simp.hms.fragment;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSpbsBlockItem;
import com.simp.hms.adapter.view.ViewSpuBlockItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirmFragment;
import com.simp.hms.dialog.DialogNotificationFragment;
import com.simp.hms.listener.DialogConfirmFragmentListener;
import com.simp.hms.listener.DialogDateFragmentListener;
import com.simp.hms.listener.DialogNotificationFragmentListener;
import com.simp.hms.listener.DialogSpbsDetailFragmentListener;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSLineSummary;
import com.simp.hms.model.SPULineSummary;
import com.simp.hms.service.GPSService;
import com.simp.hms.widget.EditTextCustom;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class SPUBlockFragment extends Fragment implements View.OnClickListener, DialogConfirmFragmentListener,
        DialogDateFragmentListener, DialogNotificationFragmentListener, DialogSpbsDetailFragmentListener {

    private LinearLayout lnrSpuBlock;
    //private Button btnSpbsBlockAddTph;

    String imei = "";
    String year = "";
    String companyCode = "";
    String estate = "";
    String spbsNumber = "";
    String spbsDate = "";
    String block = "";
    String crop = "";
    String clerk = "";
    String gpsKoordinat = "0.0:0.0";
    int status = 0;
    String bpnDate = "";
    long todayDate = 0;
    double latitude = 0;
    double longitude = 0;

    DatabaseHandler database;
    HashMap<String, SPULineSummary> mapSpuLineSummary = new HashMap<String, SPULineSummary>();
    GPSService gps;

    String selectedId = "";
    View selectedChild = null;

    final String TPH_TANGKAPAN = "888";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view  = inflater.inflate(R.layout.fragment_spu_block, container, false);

        lnrSpuBlock = (LinearLayout) view.findViewById(R.id.lnrSpuBlock);

        try{
            Bundle bundle = getArguments();
            if(bundle != null){
                imei = bundle.getString(SPBSLine.XML_IMEI);
                year = bundle.getString(SPBSLine.XML_YEAR);
                companyCode = bundle.getString(SPBSLine.XML_COMPANY_CODE);
                estate = bundle.getString(SPBSLine.XML_ESTATE);
                spbsNumber = bundle.getString(SPBSLine.XML_SPBS_NUMBER);
                spbsDate = bundle.getString(SPBSLine.XML_SPBS_DATE);
                block = bundle.getString(SPBSLine.XML_BLOCK);
                crop = bundle.getString(SPBSLine.XML_CROP);
                clerk = bundle.getString(SPBSHeader.XML_CLERK);
                status = bundle.getInt(SPBSLine.XML_STATUS);
            }
        }catch(NullPointerException e){
            e.printStackTrace();
        }

        bpnDate = new DateLocal(new Date()).getDateString(DateLocal.FORMAT_INPUT);
        todayDate = new Date().getTime();

        gps = new GPSService(getActivity());
        database = new DatabaseHandler(getActivity().getApplicationContext());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        getTPH();
    }

    private String getGpsKoordinat(){
        String gpsKoordinat = "0.0:0.0";

        if(gps != null){
            Location location = gps.getLocation();

            if(location != null){
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

            gpsKoordinat = latitude + ":" + longitude;
        }

        return gpsKoordinat;
    }

    private void getTPH(){
        mapSpuLineSummary = new HashMap<String, SPULineSummary>();
        lnrSpuBlock.removeAllViews();

        database.openTransaction();
        List<SPULineSummary> listSpuLineSummary = database.getListTPHSPU(imei, year, companyCode, estate, spbsNumber, spbsDate, block, crop);
        database.closeTransaction();

        if(listSpuLineSummary.size() > 0){
            for(int i = 0; i < listSpuLineSummary.size(); i++){
                SPULineSummary spuLineSummary = (SPULineSummary) listSpuLineSummary.get(i);

                addChild(spuLineSummary);
            }
        }
    }

    private void addChild(final SPULineSummary spuLineSummary){
        Button btnSpuBlockItemDelete;
        TextView txtSpuBlockItemTph;
        TextView txtSpuBlockItemBpnDate;
        EditTextCustom edtSpuBlockItemQtyGoodWeight;
        EditTextCustom edtSpuBlockItemQtyBadWeight;
        EditTextCustom edtSpbsBlockItemQtyPoorWeight;
        TextView txtSpuBlockItemHarvester;
        Button btnSpuBlockItemWarning;

        final View child = getActivity().getLayoutInflater().inflate(R.layout.item_spu_block, null);

        child.setTag(spuLineSummary);

        btnSpuBlockItemDelete = (Button) child.findViewById(R.id.btnSpuBlockItemDelete);
        txtSpuBlockItemTph = (TextView) child.findViewById(R.id.txtSpuBlockItemTph);
        txtSpuBlockItemBpnDate = (TextView) child.findViewById(R.id.txtSpuBlockItemBpnDate);
        edtSpuBlockItemQtyGoodWeight = (EditTextCustom) child.findViewById(R.id.edtSpuBlockItemQtyGoodWeight);
        edtSpuBlockItemQtyBadWeight = (EditTextCustom) child.findViewById(R.id.edtSpuBlockItemQtyBadWeight);
        edtSpbsBlockItemQtyPoorWeight = (EditTextCustom) child.findViewById(R.id.edtSpbsBlockItemQtyPoorWeight);
        txtSpuBlockItemHarvester  = (TextView) child.findViewById(R.id.txtSpuBlockItemHarvester);
        btnSpuBlockItemWarning = (Button) child.findViewById(R.id.btnSpuBlockItemWarning);

        final ViewSpuBlockItem view = new ViewSpuBlockItem(btnSpuBlockItemDelete, txtSpuBlockItemTph, txtSpuBlockItemBpnDate,
                edtSpuBlockItemQtyGoodWeight, edtSpuBlockItemQtyBadWeight, edtSpbsBlockItemQtyPoorWeight, btnSpuBlockItemWarning, txtSpuBlockItemHarvester);

        view.getTxtSpuBlockItemTph().setText(spuLineSummary.getTph());
        view.getTxtSpuBlockItemBpnDate().setText(spuLineSummary.getBpnDate());
        view.getTxtSpuBlockItemTph().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        /*view.getEdtSpbsBlockItemQtyJanjang().setImeOptions(EditorInfo.IME_ACTION_NEXT);
        view.getEdtSpbsBlockItemQtyLooseFruit().setImeOptions(EditorInfo.IME_ACTION_DONE);*/

        view.getEdtSpuBlockItemQtyGoodWeight().setText(spuLineSummary.getQtyGoodWeight() > 0 ? String.valueOf((int) spuLineSummary.getQtyGoodWeight()) : "");
        view.getEdtSpuBlockItemQtyBadWeight().setText(spuLineSummary.getQtyBadWeight() > 0 ? String.valueOf((int) spuLineSummary.getQtyBadWeight()) : "");
        view.getEdtSpuBlockItemQtyPoorWeight().setText(spuLineSummary.getQtyPoorWeight() > 0 ? String.valueOf((int) spuLineSummary.getQtyPoorWeight()) : "");
        view.getTxtSpuBlockItemHarvester().setText(spuLineSummary.getHarvester());

        view.getBtnSpuBlockItemDelete().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status==0){
                    selectedId = String.valueOf(spuLineSummary.getId());
                    selectedChild = child;

                    view.getTxtSpuBlockItemTph().clearFocus();
                    view.getEdtSpuBlockItemQtyGoodWeight().clearFocus();
                    view.getEdtSpuBlockItemQtyBadWeight().clearFocus();
                    view.getEdtSpuBlockItemQtyPoorWeight().clearFocus();

                    DialogConfirmFragment dialogConfirmFragment = new DialogConfirmFragment();

                    Bundle bundle = new Bundle();

                    bundle.putString("title", getResources().getString(R.string.informasi));
                    bundle.putString("message", getResources().getString(R.string.data_delete));
                    bundle.putInt("id", R.id.btnSpuBlockItemDelete);

                    dialogConfirmFragment.setArguments(bundle);

                    dialogConfirmFragment.setTargetFragment(SPUBlockFragment.this, 1);
                    dialogConfirmFragment.show(getFragmentManager(), "dialog");
                }else{
                    DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

                    Bundle bundle = new Bundle();

                    bundle.putString("title", getResources().getString(R.string.informasi));
                    bundle.putString("message", getResources().getString(R.string.data_already_export));
                    bundle.putBoolean("finish", false);

                    dialogNotification.setArguments(bundle);

                    dialogNotification.setTargetFragment(SPUBlockFragment.this, 1);
                    dialogNotification.show(getFragmentManager(), "dialog");
                }
            }
        });

        mapSpuLineSummary.put(String.valueOf(spuLineSummary.getId()), spuLineSummary);
        lnrSpuBlock.addView(child);

        int index = lnrSpuBlock.indexOfChild(child);
        child.setTag(index);

        view.getTxtSpuBlockItemTph().requestFocus();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConfirmFragmentOK(int id) {
        switch (id) {
            case R.id.btnSpuBlockItemDelete:
                if(!TextUtils.isEmpty(selectedId) && selectedChild != null) {
                    SPULineSummary spuLineSummary = mapSpuLineSummary.get(selectedId);

                    if(spuLineSummary !=null) {
                        try {

                            database.openTransaction();

                            String bpnId = spuLineSummary.getBpnId();
                            String bpnDate = spuLineSummary.getBpnDate();
                            String tph = spuLineSummary.getTph();

                            boolean del = false;

                            if (!TextUtils.isEmpty(bpnId)) {
                                BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null,
									BPNHeader.XML_BPN_ID + "=?",
									new String [] {bpnId},
									null, null, null, null);

								if(bpnHeader != null){
									bpnHeader.setSpbsNumber("");

									database.updateData(bpnHeader,
											BPNHeader.XML_BPN_ID + "=?",
											new String [] {bpnId});

									del = true;
								}

                                if (del) {
                                    tph = spuLineSummary.getTph();

                                    database.deleteData(SPBSLine.TABLE_NAME,
                                            SPBSLine.XML_ID + "=?" + " and " +
                                                    SPBSLine.XML_YEAR + "=?" + " and " +
                                                    SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
                                                    SPBSLine.XML_ESTATE + "=?" + " and " +
                                                    SPBSLine.XML_CROP + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_NUMBER + "=?" + " and " +
                                                    SPBSLine.XML_SPBS_DATE + "=?" + " and " +
                                                    SPBSLine.XML_BLOCK + "=?" + " and " +
                                                    SPBSLine.XML_TPH + "=?" + " and " +
                                                    SPBSLine.XML_BPN_ID + "=?" + " and " +
                                                    SPBSLine.XML_BPN_DATE + "=?",
                                            new String[]{String.valueOf(spuLineSummary.getId()), year, companyCode, estate, crop, spbsNumber, spbsDate, block, tph, bpnId, spuLineSummary.getBpnDate()});

                                    database.commitTransaction();

                                }

                                lnrSpuBlock.removeView(selectedChild);
                                mapSpuLineSummary.remove(selectedId);
                            }

                        }catch (SQLiteException e) {
                            e.printStackTrace();
                            database.closeTransaction();
                        } finally {
                            database.closeTransaction();
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onDateFragmentOK(Date date, int id) {

    }

    @Override
    public void onFragmentOK(boolean is_finish) {

    }

    @Override
    public void onSpbsDetailFragmentOK(boolean is_finish) {

    }

    private int checkDot(String value){
        int countDot = 0;

        for(int i = 0; i < value.length(); i++){
            char c = value.charAt(i);

            if(c == '.'){
                countDot++;
            }
        }

        return countDot;
    }

    private int decimalPlaces(String value, int digit){
        if(value.indexOf(".") > 0){
            String temp = value.substring(value.indexOf(".") + 1, value.length());

            return temp.length();
        }else{
            return 0;
        }
    }
}
