package com.simp.hms.fragment;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.ResultPoint;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.camera.CameraSettings;
import com.simp.hms.R;
import com.simp.hms.activity.spbskranibluetooth.SPBSWBQRCodeActivity;

import java.util.List;

public class FragmentScanQR extends Fragment {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;

    View view;
    private DecoratedBarcodeView qrView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_scan_qr, container, false);


        return view;
    }


}
