package com.simp.hms.fragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import com.simp.hms.R;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotificationFragment;
import com.simp.hms.handler.StringHandler;
import com.simp.hms.listener.DialogNotificationFragmentListener;
import com.simp.hms.model.Converter;
import com.simp.hms.model.SKB;
import com.simp.hms.model.TaksasiHeader;
import com.simp.hms.model.TaksasiLine;
import com.simp.hms.service.GPSService;
import com.simp.hms.widget.EditTextCustom;
import com.simp.hms.adapter.view.ViewTaksasiSKBItem;

import android.database.sqlite.SQLiteException;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TaksasiSkbFragment extends Fragment implements DialogNotificationFragmentListener {
    private TextView txtTaksasiSkbBarisBlock1;
    private TextView txtTaksasiSkbBarisBlock2;
    private LinearLayout lnrTaksasiSkbBarisBlock1;
    private LinearLayout lnrTaksasiSkbBarisBlock2;
    private Button btnTaksasiSkbSwitch;
    private ImageButton btnTaksasiSkbBlock1;
    private ImageButton btnTaksasiSkbBlock2;

    String imei = "";
    String companyCode = "";
    String estate = "";
    String division = "";
    String taksasiDate = "";
    String block = "";
    String crop = "";
    int barisSkb = 0;
    String foreman = "";
    String gpsKoordinat = "0.0:0.0";
    int status = 0;
    long todayDate = 0;
    double latitude = 0;
    double longitude = 0;

    DatabaseHandler database;
    HashMap<String, TaksasiLine> mapTaksasiLine1 = new HashMap<String, TaksasiLine>();
    HashMap<String, TaksasiLine> mapTaksasiLine2 = new HashMap<String, TaksasiLine>();

    GPSService gps;

    int childId = 0;
    String selectedId = "";
    View selectedChild = null;

    final double qtyJanjangMin = 0;
    final double qtyJanjangMax = Integer.MAX_VALUE;

    boolean isLeft = true;

    List<SKB> skbList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_taksasi_skb, container, false);

        txtTaksasiSkbBarisBlock1 = (TextView) view.findViewById(R.id.txtTaksasiSkbBarisBlock1);
        txtTaksasiSkbBarisBlock2 = (TextView) view.findViewById(R.id.txtTaksasiSkbBarisBlock2);
        lnrTaksasiSkbBarisBlock1 = (LinearLayout) view.findViewById(R.id.lnrTaksasiSkbBarisBlock1);
        lnrTaksasiSkbBarisBlock2 = (LinearLayout) view.findViewById(R.id.lnrTaksasiSkbBarisBlock2);
        btnTaksasiSkbSwitch = (Button) view.findViewById(R.id.btnTaksasiSkbSwitch);
        btnTaksasiSkbBlock1 = (ImageButton) view.findViewById(R.id.btnTaksasiSkbBlock1);
        btnTaksasiSkbBlock2 = (ImageButton) view.findViewById(R.id.btnTaksasiSkbBlock2);

        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                imei = bundle.getString(TaksasiLine.XML_IMEI);
                companyCode = bundle.getString(TaksasiLine.XML_COMPANY_CODE);
                estate = bundle.getString(TaksasiLine.XML_ESTATE);
                division = bundle.getString(TaksasiLine.XML_DIVISION);
                taksasiDate = bundle.getString(TaksasiLine.XML_TAKSASI_DATE);
                block = bundle.getString(TaksasiLine.XML_BLOCK);
                crop = bundle.getString(TaksasiLine.XML_CROP);
                barisSkb = bundle.getInt(TaksasiLine.XML_BARIS_SKB);
                foreman = bundle.getString(TaksasiHeader.XML_FOREMAN);
                status = bundle.getInt(TaksasiLine.XML_STATUS);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        todayDate = new Date().getTime();

        gps = new GPSService(getActivity());
        database = new DatabaseHandler(getActivity().getApplicationContext());


        btnTaksasiSkbSwitch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                switchBarisBlock();
            }
        });

        btnTaksasiSkbBlock1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int barisBlok = Integer.valueOf(txtTaksasiSkbBarisBlock1.getText().toString());
                for (SKB skb : skbList) {
                    if (skb.getBarisBlok() == barisBlok) {
                        int lastPokok = (int) skb.getJumlahPokok() - 1;
                        skb.setJumlahPokok(lastPokok);
                        skb.setLineSkb(lastPokok);
                        updateSKB(skb);
                    } else {
                        int lastPokok = (int) skb.getJumlahPokok() + 1;
                        skb.setJumlahPokok(lastPokok);
                        skb.setLineSkb(lastPokok);
                        updateSKB(skb);
                    }
                }
                getLineSkb(isLeft);
            }
        });

        btnTaksasiSkbBlock2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int barisBlok = Integer.valueOf(txtTaksasiSkbBarisBlock2.getText().toString());
                for (SKB skb : skbList) {
                    if (skb.getBarisBlok() == barisBlok) {
                        int lastPokok = (int) skb.getJumlahPokok() - 1;
                        skb.setJumlahPokok(lastPokok);
                        skb.setLineSkb(lastPokok);
                        updateSKB(skb);
                    } else {
                        int lastPokok = (int) skb.getJumlahPokok() + 1;
                        skb.setJumlahPokok(lastPokok);
                        skb.setLineSkb(lastPokok);
                        updateSKB(skb);
                    }
                }
                getLineSkb(isLeft);
            }
        });

        return view;
    }

    public void updateSKB(SKB skb) {
        database.openTransaction();
        database.updateData(skb, SKB.XML_COMPANY_CODE + "=?" + " and " +
                        SKB.XML_ESTATE + "=?" + " and " +
                        SKB.XML_BLOCK + "=?" + " and " +
                        SKB.XML_BARIS_SKB + "=?" + " and " +
                        SKB.XML_BARIS_BLOCK + "=?" + " and " +
                        SKB.XML_VALID_TO + "=? ",
                new String[]{companyCode, estate, block, String.valueOf(barisSkb), String.valueOf(skb.getBarisBlok()), taksasiDate});
        database.commitTransaction();
        database.closeTransaction();
    }

    @Override
    public void onStart() {
        super.onStart();

        getLineSkb(isLeft);
    }

    private String getGpsKoordinat() {
        String gpsKoordinat = "0.0:0.0";

        if (gps != null) {
            Location location = gps.getLocation();

            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

            gpsKoordinat = latitude + ":" + longitude;
        }

        return gpsKoordinat;
    }

    private void getLineSkb(boolean isLeft) {
        mapTaksasiLine1 = new HashMap<String, TaksasiLine>();
        mapTaksasiLine2 = new HashMap<String, TaksasiLine>();
        lnrTaksasiSkbBarisBlock1.removeAllViews();
        lnrTaksasiSkbBarisBlock2.removeAllViews();
        skbList = new ArrayList<>();
        childId = 0;

        database.openTransaction();
        final List<Object> listSkbBarisBlock = database.getListData(false, SKB.TABLE_NAME,
//                new String[]{SKB.XML_BARIS_SKB, SKB.XML_BARIS_BLOCK},
                null,
                SKB.XML_COMPANY_CODE + "=?" + " and " +
                        SKB.XML_ESTATE + "=?" + " and " +
                        SKB.XML_BLOCK + "=?" + " and " +
                        SKB.XML_BARIS_SKB + "=?" + " and " +
                        SKB.XML_VALID_TO + "=? ",
                new String[]{companyCode, estate, block, String.valueOf(barisSkb), taksasiDate},
                SKB.XML_BARIS_SKB + ", " + SKB.XML_BARIS_BLOCK, null, SKB.XML_BARIS_BLOCK, null);
        database.closeTransaction();

        if (listSkbBarisBlock.size() > 0) {

            for (int i = 0; i < listSkbBarisBlock.size(); i++) {
                SKB skbBarisBlock = (SKB) listSkbBarisBlock.get(i);
                skbList.add(skbBarisBlock);
                int barisBlock = skbBarisBlock.getBarisBlok();

                if (isLeft) {
                    if (i == 0) {
                        txtTaksasiSkbBarisBlock1.setText(String.valueOf(barisBlock));
                    } else {
                        txtTaksasiSkbBarisBlock2.setText(String.valueOf(barisBlock));
                    }
                } else {
                    if (i == 0) {
                        txtTaksasiSkbBarisBlock2.setText(String.valueOf(barisBlock));
                    } else {
                        txtTaksasiSkbBarisBlock1.setText(String.valueOf(barisBlock));
                    }
                }

                database.openTransaction();
                List<Object> listSkb = database.getListData(false, SKB.TABLE_NAME, null,
                        SKB.XML_COMPANY_CODE + "=?" + " and " +
                                SKB.XML_ESTATE + "=?" + " and " +
                                SKB.XML_BLOCK + "=?" + " and " +
                                SKB.XML_BARIS_SKB + "=?" + " and " +
                                SKB.XML_BARIS_BLOCK + "=?" + " and " +
                                SKB.XML_VALID_TO + "=? ",
                        new String[]{companyCode, estate, block, String.valueOf(barisSkb), String.valueOf(barisBlock), taksasiDate},
                        null, null, SKB.XML_LINE_SKB, null);
                database.closeTransaction();

                if (listSkb.size() > 0) {
                    for (int j = 0; j < listSkb.size(); j++) {
                        SKB skb = (SKB) listSkb.get(j);
                        for (int k = 1; k <= skb.getLineSkb(); k++) {
                            int lineSkb = k;
                            double qtyPokok = skb.getJumlahPokok();

//						database.openTransaction();
//						TaksasiLine taksasiLineQtyJanjang = (TaksasiLine) database.getDataFirst(false, TaksasiLine.TABLE_NAME, null,
//								TaksasiLine.XML_IMEI + "=?" + " and " +
//								TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
//								TaksasiLine.XML_ESTATE + "=?" + " and " +
//								TaksasiLine.XML_DIVISION + "=?" + " and " +
//								TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
//								TaksasiLine.XML_BLOCK + "=?" + " and " +
//								TaksasiLine.XML_CROP + "=?" + " and " +
//								TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
//								TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
//								TaksasiLine.XML_LINE_SKB + "=?",
//								new String [] {imei, companyCode, estate, division, taksasiDate, block, crop, barisSkb, barisBlock, String.valueOf(lineSkb)},
//								null, null, null, null);
//						database.closeTransaction();

                            database.openTransaction();
                            TaksasiLine taksasiLineQtyJanjang = (TaksasiLine) database.getDataFirst(false, TaksasiLine.TABLE_NAME, null,
                                    TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                            TaksasiLine.XML_ESTATE + "=?" + " and " +
                                            TaksasiLine.XML_DIVISION + "=?" + " and " +
                                            TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                            TaksasiLine.XML_BLOCK + "=?" + " and " +
                                            TaksasiLine.XML_CROP + "=?" + " and " +
                                            TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                            TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                            TaksasiLine.XML_LINE_SKB + "=?",
                                    new String[]{companyCode, estate, division, taksasiDate, block, crop, String.valueOf(barisSkb), String.valueOf(barisBlock), String.valueOf(lineSkb)},
                                    null, null, null, null);
                            database.closeTransaction();

                            int qtyJanjang = 0;

                            if (taksasiLineQtyJanjang != null) {
                                qtyJanjang = taksasiLineQtyJanjang.getQtyJanjang();
                            }

                            TaksasiLine taksasiLine = new TaksasiLine(0, imei, companyCode,
                                    estate, division, taksasiDate, block, crop,
                                    barisSkb, barisBlock, lineSkb, qtyPokok, qtyJanjang,
                                    getGpsKoordinat(), 0, status, todayDate,
                                    foreman, 0, "");

                            try {
                                database.openTransaction();
                                database.setData(taksasiLine);
                                database.commitTransaction();
                            } catch (SQLiteException e) {
                                database.closeTransaction();
                            } finally {
                                database.closeTransaction();
                            }

//						addChild(taksasiLine, leftRight);
                            runThread(taksasiLine, isLeft, i);
                        }
                    }

//					isLeft = (isLeft) ? false: true;
                }
            }

            btnTaksasiSkbBlock1.setVisibility(View.VISIBLE);
            btnTaksasiSkbBlock2.setVisibility(View.VISIBLE);
        }
    }


    private void runThread(final TaksasiLine taksasiLine, final boolean isLeft, final int index) {
        new Thread() {
            public void run() {
                try {
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            addChild(taksasiLine, isLeft, index);

                        }
                    });

                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void addChild(final TaksasiLine taksasiLine, final boolean isLeft, final int index) {
        TextView txtTaksasiSkbItemLineSkb;
        Button btnTaksasiSkbItemQtyJanjangMinus;
        EditTextCustom edtTaksasiSkbItemQtyJanjang;
        Button btnTaksasiSkbItemQtyJanjangPlus;

        final View child = getActivity().getLayoutInflater().inflate(R.layout.item_taksasi_skb, null);
        child.setTag(taksasiLine);

        txtTaksasiSkbItemLineSkb = (TextView) child.findViewById(R.id.txtTaksasiSkbItemLineSkb);
        btnTaksasiSkbItemQtyJanjangMinus = (Button) child.findViewById(R.id.btnTaksasiSkbItemQtyJanjangMinus);
        edtTaksasiSkbItemQtyJanjang = (EditTextCustom) child.findViewById(R.id.edtTaksasiSkbItemQtyJanjang);
        btnTaksasiSkbItemQtyJanjangPlus = (Button) child.findViewById(R.id.btnTaksasiSkbItemQtyJanjangPlus);

        final ViewTaksasiSKBItem view = new ViewTaksasiSKBItem(txtTaksasiSkbItemLineSkb, btnTaksasiSkbItemQtyJanjangMinus,
                edtTaksasiSkbItemQtyJanjang, btnTaksasiSkbItemQtyJanjangPlus);

        view.getTxtTaksasiSkbItemLineSkb().setText(String.valueOf(taksasiLine.getLineSkb()));

        if (taksasiLine.getQtyJanjang() > 0) {
            view.getEdtTaksasiSkbItemQtyJanjang().setText(String.valueOf(taksasiLine.getQtyJanjang()));
        } else {
            view.getEdtTaksasiSkbItemQtyJanjang().setText("");
        }

        view.getEdtTaksasiSkbItemQtyJanjang().addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String qtyJanjang = s.toString().trim();

                if (status == 0) {
                    taksasiLine.setQtyJanjang(new Converter(qtyJanjang).StrToInt());

                    int barisBlock = taksasiLine.getBarisBlock();
                    int lineSkb = taksasiLine.getLineSkb();

                    try {
                        database.openTransaction();

//						database.deleteData(TaksasiLine.TABLE_NAME, 
//							TaksasiLine.XML_IMEI + "=?" + " and " +
//							TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
//							TaksasiLine.XML_ESTATE + "=?" + " and " +
//							TaksasiLine.XML_DIVISION + "=?" + " and " +
//							TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
//							TaksasiLine.XML_BLOCK + "=?" + " and " +
//							TaksasiLine.XML_CROP + "=?" + " and " +
//							TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
//							TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
//							TaksasiLine.XML_LINE_SKB + "=?",
//							new String [] {imei, companyCode, estate, division, taksasiDate, block,
//								crop, barisSkb, barisBlock, String.valueOf(lineSkb)});

                        database.deleteData(TaksasiLine.TABLE_NAME,
                                TaksasiLine.XML_COMPANY_CODE + "=?" + " and " +
                                        TaksasiLine.XML_ESTATE + "=?" + " and " +
                                        TaksasiLine.XML_DIVISION + "=?" + " and " +
                                        TaksasiLine.XML_TAKSASI_DATE + "=?" + " and " +
                                        TaksasiLine.XML_BLOCK + "=?" + " and " +
                                        TaksasiLine.XML_CROP + "=?" + " and " +
                                        TaksasiLine.XML_BARIS_SKB + "=?" + " and " +
                                        TaksasiLine.XML_BARIS_BLOCK + "=?" + " and " +
                                        TaksasiLine.XML_LINE_SKB + "=?",
                                new String[]{companyCode, estate, division, taksasiDate, block,
                                        crop, String.valueOf(barisSkb), String.valueOf(barisBlock), String.valueOf(lineSkb)});

                        database.setData(taksasiLine);

                        database.commitTransaction();
                    } catch (SQLiteException e) {
                        e.printStackTrace();
                        database.closeTransaction();
                    } finally {
                        database.closeTransaction();
                    }
                } else {
                    view.getEdtTaksasiSkbItemQtyJanjang().setText(qtyJanjang);
                    view.getEdtTaksasiSkbItemQtyJanjang().setSelection(qtyJanjang.length());

                    DialogNotificationFragment dialogNotification = new DialogNotificationFragment();

                    Bundle bundle = new Bundle();

                    bundle.putString("title", getResources().getString(R.string.informasi));
                    bundle.putString("message", getResources().getString(R.string.data_already_export));
                    bundle.putBoolean("finish", false);

                    dialogNotification.setArguments(bundle);

                    dialogNotification.setTargetFragment(TaksasiSkbFragment.this, 1);
                    dialogNotification.show(getFragmentManager(), "dialog");
                }
            }
        });

        view.getBtnTaksasiSkbItemQtyJanjangMinus().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                decrement(view.getEdtTaksasiSkbItemQtyJanjang(), taksasiLine, isLeft);
            }
        });

        view.getBtnTaksasiSkbItemQtyJanjangPlus().setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                increment(view.getEdtTaksasiSkbItemQtyJanjang(), taksasiLine, isLeft);
            }
        });


        if (isLeft) {
            if (index == 0) {
                mapTaksasiLine1.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
                lnrTaksasiSkbBarisBlock1.addView(child);

                int idx = lnrTaksasiSkbBarisBlock1.indexOfChild(child);
                child.setTag(idx);
            } else {
                mapTaksasiLine2.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
                lnrTaksasiSkbBarisBlock2.addView(child);

                int idx = lnrTaksasiSkbBarisBlock2.indexOfChild(child);
                child.setTag(idx);
            }
        } else {
            if (index == 0) {
                mapTaksasiLine2.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
                lnrTaksasiSkbBarisBlock2.addView(child);

                int idx = lnrTaksasiSkbBarisBlock2.indexOfChild(child);
                child.setTag(idx);
            } else {
                mapTaksasiLine1.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
                lnrTaksasiSkbBarisBlock1.addView(child);

                int idx = lnrTaksasiSkbBarisBlock1.indexOfChild(child);
                child.setTag(idx);
            }
        }
    }

    public void increment(EditTextCustom edt, TaksasiLine taksasiLine, boolean isLeft) {
        int mValue;
        String value = edt.getText().toString().trim();

        if (new Converter(value).StrToDouble() < qtyJanjangMax) {
            mValue = new Converter(edt.getText().toString().trim()).StrToInt() + 1;

            edt.setText(String.valueOf(mValue));
            taksasiLine.setQtyJanjang(mValue);

            if (isLeft) {
                mapTaksasiLine1.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
            } else {
                mapTaksasiLine2.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
            }

//		    mapBKMLine.put(bkmLine.getNik(), bkmLine);
        }
    }

    public void decrement(EditTextCustom edt, TaksasiLine taksasiLine, boolean isLeft) {
        int mValue;
        String value = edt.getText().toString().trim();

        if (new Converter(value).StrToDouble() > qtyJanjangMin) {
            mValue = new Converter(edt.getText().toString().trim()).StrToInt() - 1;

            edt.setText(String.valueOf(mValue));
            taksasiLine.setQtyJanjang(mValue);

            if (isLeft) {
                mapTaksasiLine1.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
            } else {
                mapTaksasiLine2.put(String.valueOf(taksasiLine.getLineSkb()), taksasiLine);
            }
//		    mapBKMLine.put(bkmLine.getNik(), bkmLine);
        }
    }

    @Override
    public void onFragmentOK(boolean is_finish) {
        // TODO Auto-generated method stub

    }

    private void switchBarisBlock() {
        isLeft = (isLeft) ? false : true;

        getLineSkb(isLeft);
    }
}
