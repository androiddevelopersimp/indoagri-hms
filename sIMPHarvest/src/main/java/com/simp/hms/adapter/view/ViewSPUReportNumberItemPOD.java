package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPUReportNumberItemPOD {
    private TextView txtSpuReportNumberItemSpuNumber;
    private TextView txtSpuReportNumberItemQtyPODQty;
    private TextView txtSpuReportNumberItemQtyGoodWeight;
    private TextView txtSpuReportNumberItemQtyEstimasiQty;
    private TextView txtSpuReportNumberItemQtyPoorWeight;

    public ViewSPUReportNumberItemPOD(TextView txtSpuReportNumberItemSpuNumber,
                                      TextView txtSpuReportNumberItemQtyPODQty,
                                      TextView txtSpuReportNumberItemQtyGoodWeight,
                                      TextView txtSpuReportNumberItemQtyEstimasiQty,
                                      TextView txtSpuReportNumberItemQtyPoorWeight) {
        super();
        this.txtSpuReportNumberItemSpuNumber = txtSpuReportNumberItemSpuNumber;
        this.txtSpuReportNumberItemQtyPODQty = txtSpuReportNumberItemQtyPODQty;
        this.txtSpuReportNumberItemQtyGoodWeight = txtSpuReportNumberItemQtyGoodWeight;
        this.txtSpuReportNumberItemQtyEstimasiQty = txtSpuReportNumberItemQtyEstimasiQty;
        this.txtSpuReportNumberItemQtyPoorWeight = txtSpuReportNumberItemQtyPoorWeight;
    }

    public TextView getTxtSpuReportNumberItemSpuNumber() {
        return txtSpuReportNumberItemSpuNumber;
    }

    public void setTxtSpuReportNumberItemSpuNumber(TextView txtSpuReportNumberItemSpuNumber) {
        this.txtSpuReportNumberItemSpuNumber = txtSpuReportNumberItemSpuNumber;
    }

    public TextView getTxtSpuReportNumberItemQtyPODQty() {
        return txtSpuReportNumberItemQtyPODQty;
    }

    public void setTxtSpuReportNumberItemQtyPODQty(TextView txtSpuReportNumberItemQtyPODQty) {
        this.txtSpuReportNumberItemQtyPODQty = txtSpuReportNumberItemQtyPODQty;
    }

    public TextView getTxtSpuReportNumberItemQtyGoodWeight() {
        return txtSpuReportNumberItemQtyGoodWeight;
    }

    public void setTxtSpuReportNumberItemQtyGoodWeight(TextView txtSpuReportNumberItemQtyGoodWeight) {
        this.txtSpuReportNumberItemQtyGoodWeight = txtSpuReportNumberItemQtyGoodWeight;
    }

    public TextView getTxtSpuReportNumberItemQtyEstimasiQty() {
        return txtSpuReportNumberItemQtyEstimasiQty;
    }

    public void setTxtSpuReportNumberItemQtyEstimasiQty(TextView txtSpuReportNumberItemQtyEstimasiQty) {
        this.txtSpuReportNumberItemQtyEstimasiQty = txtSpuReportNumberItemQtyEstimasiQty;
    }

    public TextView getTxtSpuReportNumberItemQtyPoorWeight() {
        return txtSpuReportNumberItemQtyPoorWeight;
    }

    public void setTxtSpuReportNumberItemQtyPoorWeight(TextView txtSpuReportNumberItemQtyPoorWeight) {
        this.txtSpuReportNumberItemQtyPoorWeight = txtSpuReportNumberItemQtyPoorWeight;
    }
}
