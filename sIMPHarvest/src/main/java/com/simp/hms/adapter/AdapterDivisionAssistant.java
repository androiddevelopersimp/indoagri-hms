package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewDivisionAssistantItem;
import com.simp.hms.model.DivisionAssistant;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterDivisionAssistant extends BaseAdapter{
	private Context context;
	private List<DivisionAssistant> listAssistantDivision, listTemp;
	private int layout;

	public AdapterDivisionAssistant(Context context, List<DivisionAssistant> listAssistantDivision, int layout){
		this.context = context;
		this.listAssistantDivision = listAssistantDivision;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listAssistantDivision.size();
	}

	@Override
	public Object getItem(int pos) {
		return listAssistantDivision.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		DivisionAssistant item = listAssistantDivision.get(pos);
		
		ViewDivisionAssistantItem view;
		
		TextView txtDivisionAssistantItemEstate;
		TextView txtDivisionAssistantItemDivision;
		TextView txtDivisionAssistantItemSpras;
		TextView txtDivisionAssistantItemDescription;
		TextView txtDivisionAssistantItemAssistant;
		TextView txtDivisionAssistantItemDistanceToMill;
		TextView txtDivisionAssistantItemUom;
		TextView txtDivisionAssistantItemAssistantName;
		TextView txtDivisionAssistantItemLifnr;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtDivisionAssistantItemEstate = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemEstate);
            txtDivisionAssistantItemDivision = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemDivision);
            txtDivisionAssistantItemSpras = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemSpras);
            txtDivisionAssistantItemDescription = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemDescription);
            txtDivisionAssistantItemAssistant = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemAssistant);
            txtDivisionAssistantItemDistanceToMill = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemDistanceToMill);
            txtDivisionAssistantItemUom = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemUom);
            txtDivisionAssistantItemAssistantName = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemAssistantName);
			txtDivisionAssistantItemLifnr = (TextView) convertView.findViewById(R.id.txtDivisionAssistantItemLifnr);
            
            view = new ViewDivisionAssistantItem(txtDivisionAssistantItemEstate, txtDivisionAssistantItemDivision, 
            		txtDivisionAssistantItemSpras, txtDivisionAssistantItemDescription, txtDivisionAssistantItemAssistant, 
            		txtDivisionAssistantItemDistanceToMill, txtDivisionAssistantItemUom, txtDivisionAssistantItemAssistantName,
					txtDivisionAssistantItemLifnr);
            
            convertView.setTag(view);
        }else{
        	view = (ViewDivisionAssistantItem) convertView.getTag();
        	
        	txtDivisionAssistantItemEstate = view.getTxtDivisionAssistantItemEstate();
        	txtDivisionAssistantItemDivision = view.gettxtDivisionAssistantItemDivision();
        	txtDivisionAssistantItemSpras = view.gettxtDivisionAssistantItemSpras();
        	txtDivisionAssistantItemDescription = view.gettxtDivisionAssistantItemDescription();
        	txtDivisionAssistantItemAssistant = view.gettxtDivisionAssistantItemAssistant();
        	txtDivisionAssistantItemDistanceToMill = view.gettxtDivisionAssistantItemDistanceToMill();
        	txtDivisionAssistantItemUom = view.gettxtDivisionAssistantItemUom();
        	txtDivisionAssistantItemAssistantName = view.gettxtDivisionAssistantItemAssistantName();
			txtDivisionAssistantItemLifnr = view.gettxtDivisionAssistantItemLifnr();
        }

    	txtDivisionAssistantItemEstate.setText(item.getEstate());
    	txtDivisionAssistantItemDivision.setText(item.getDivision());
    	txtDivisionAssistantItemSpras.setText(item.getSpras());
    	txtDivisionAssistantItemDescription.setText(item.getDescription());
    	txtDivisionAssistantItemAssistant.setText(item.getAssistant());
    	txtDivisionAssistantItemDistanceToMill.setText(item.getDistanceToMill());
    	txtDivisionAssistantItemUom.setText(item.getUom());
    	txtDivisionAssistantItemAssistantName.setText(item.getAssistantName());
		txtDivisionAssistantItemLifnr.setText(item.getLifnr());
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listAssistantDivision = null;
				listAssistantDivision = (List<DivisionAssistant>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<DivisionAssistant> listResult = new ArrayList<DivisionAssistant>();
				DivisionAssistant assistantDivision;
				
				if(listTemp == null){
					listTemp = new ArrayList<DivisionAssistant>(listAssistantDivision);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						assistantDivision = listTemp.get(i);
						
						if(assistantDivision.getAssistantName().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(assistantDivision);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
