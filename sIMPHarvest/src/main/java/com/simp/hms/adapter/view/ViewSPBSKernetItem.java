package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.TextView;

public class ViewSPBSKernetItem {
	private Button btnSpbsKernetItemDelete;
	private TextView txtSpbsKernetItemName;

	public ViewSPBSKernetItem(Button btnSpbsKernetItemDelete,
			TextView txtSpbsKernetItemName) {
		super();
		this.btnSpbsKernetItemDelete = btnSpbsKernetItemDelete;
		this.txtSpbsKernetItemName = txtSpbsKernetItemName;
	}

	public Button getBtnSpbsKernetItemDelete() {
		return btnSpbsKernetItemDelete;
	}

	public void setBtnSpbsKernetItemDelete(Button btnSpbsKernetItemDelete) {
		this.btnSpbsKernetItemDelete = btnSpbsKernetItemDelete;
	}

	public TextView getTxtSpbsKernetItemName() {
		return txtSpbsKernetItemName;
	}

	public void setTxtSpbsKernetItemName(TextView txtSpbsKernetItemName) {
		this.txtSpbsKernetItemName = txtSpbsKernetItemName;
	}
}
