package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNHistoryItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.BPNQuality;
import com.simp.hms.model.BPNQuantity;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSLine;

import android.app.Activity;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBPNHistory extends BaseAdapter{
	private Activity activity;
	private List<BPNHeader> lstBpn, lstTemp;
	private int layout;
	
	public AdapterBPNHistory(Activity activity, List<BPNHeader> lstBpn, int layout){
		this.activity = activity;
		this.lstBpn = lstBpn;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstBpn.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstBpn.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BPNHeader harvest_book = lstBpn.get(pos);
		
		ViewBPNHistoryItem vie_harvest_book;
		TextView txt_harvest_book_history_item_date;
		TextView txt_harvest_book_history_item_clerk;
		TextView txt_harvest_book_history_item_foreman;
		TextView txt_harvest_book_history_item_block;
		TextView txt_harvest_book_history_item_harvester;
		TextView txt_harvest_book_history_item_tph;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txt_harvest_book_history_item_date = (TextView) convertView.findViewById(R.id.txt_harvest_book_history_item_date2);
            txt_harvest_book_history_item_clerk = (TextView) convertView.findViewById(R.id.txt_harvest_book_history_item_clerk2);
            txt_harvest_book_history_item_foreman = (TextView) convertView.findViewById(R.id.txt_harvest_book_history_item_foreman2);
            txt_harvest_book_history_item_block = (TextView) convertView.findViewById(R.id.txt_harvest_book_history_item_block2);
            txt_harvest_book_history_item_harvester = (TextView) convertView.findViewById(R.id.txt_harvest_book_history_item_harvester2);
            txt_harvest_book_history_item_tph = (TextView) convertView.findViewById(R.id.txt_harvest_book_history_item_tph);
            
            vie_harvest_book = new ViewBPNHistoryItem(txt_harvest_book_history_item_date, txt_harvest_book_history_item_clerk, txt_harvest_book_history_item_foreman,
            		txt_harvest_book_history_item_block, txt_harvest_book_history_item_harvester, txt_harvest_book_history_item_tph);     
            convertView.setTag(vie_harvest_book);
        }else{
        	vie_harvest_book = (ViewBPNHistoryItem) convertView.getTag();
        	
        	txt_harvest_book_history_item_date = vie_harvest_book.getTxtHarvestBookHistoryItemDate();
        	txt_harvest_book_history_item_clerk = vie_harvest_book.getTxtHarvestBookHistoryItemClerk();
        	txt_harvest_book_history_item_foreman = vie_harvest_book.getTxtHarvestBookHistoryItemForeman();
        	txt_harvest_book_history_item_block = vie_harvest_book.getTxtHarvestBookHistoryItemBlock();
        	txt_harvest_book_history_item_harvester = vie_harvest_book.getTxtHarvestBookHistoryItemHarvester();
        	txt_harvest_book_history_item_tph = vie_harvest_book.getTxtHarvestBookHistoryItemTph();
        }

        txt_harvest_book_history_item_date.setText(new DateLocal(harvest_book.getBpnDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txt_harvest_book_history_item_clerk.setText(harvest_book.getClerk());
        txt_harvest_book_history_item_foreman.setText(harvest_book.getForeman());
        txt_harvest_book_history_item_block.setText(harvest_book.getLocation());
        txt_harvest_book_history_item_harvester.setText(harvest_book.getHarvester());
        txt_harvest_book_history_item_tph.setText(harvest_book.getTph());

        return convertView;
	}
	
	public void deleteData(int pos){
		BPNHeader item = lstBpn.get(pos);
		
		String id = item.getBpnId();
		String imei = item.getImei();
		String companyCode = item.getCompanyCode();
		String estate = item.getEstate();
		String bpnDate = item.getBpnDate();
		String division = item.getDivision();
		String gang = item.getGang();
		String location = item.getLocation();
		String tph = item.getTph();
		String nikHarvester = item.getNikHarvester();
		int status = item.getStatus();
		String spbsNumber = item.getSpbsNumber();
		
		if(status == 0){
			
			if(TextUtils.isEmpty(spbsNumber)){
				DatabaseHandler database = new DatabaseHandler(activity);
				
				try{
					database.openTransaction();
					
//					database.deleteData(BPNQuantity.TABLE_NAME, 
//							BPNQuantity.XML_BPN_ID + "=?" + " and " +
//							BPNQuantity.XML_IMEI + "=?" + " and " +
//							BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
//							BPNQuantity.XML_ESTATE + "=?" + " and " +
//							BPNQuantity.XML_BPN_DATE + "=?" + " and " +
//							BPNQuantity.XML_DIVISION + "=?" + " and " +
//							BPNQuantity.XML_GANG + "=?" + " and " +
//							BPNQuantity.XML_LOCATION + "=?" + " and " +
//							BPNQuantity.XML_TPH + "=?" + " and " +
//							BPNQuantity.XML_NIK_HARVESTER + "=?", 
//							new String [] {id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});


					//Fernando, check SPBS Line jika sudah ada, BPN tidak bisa dihapus
					List<Object> listspbsline = database.getListData(false, SPBSLine.TABLE_NAME, null,
							SPBSLine.XML_BPN_ID + "=?" + " and " +
									SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
									SPBSLine.XML_ESTATE + "=?",
							new String[]{id, companyCode, estate},
							null, null, null, null);

					if(listspbsline.size() > 0){
						new DialogNotification(activity, activity.getResources().getString(R.string.informasi),
								activity.getResources().getString(R.string.data_bpn_delete_cannot), false).show();

					}else {
						database.deleteData(BPNQuantity.TABLE_NAME,
								BPNQuantity.XML_BPN_ID + "=?" + " and " +
										BPNQuantity.XML_COMPANY_CODE + "=?" + " and " +
										BPNQuantity.XML_ESTATE + "=?" + " and " +
										BPNQuantity.XML_BPN_DATE + "=?" + " and " +
										BPNQuantity.XML_DIVISION + "=?" + " and " +
										BPNQuantity.XML_GANG + "=?" + " and " +
										BPNQuantity.XML_LOCATION + "=?" + " and " +
										BPNQuantity.XML_TPH + "=?" + " and " +
										BPNQuantity.XML_NIK_HARVESTER + "=?",
								new String[]{id, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});

//					database.deleteData(BPNQuality.TABLE_NAME, 
//							BPNQuality.XML_BPN_ID + "=?" + " and " +
//							BPNQuality.XML_IMEI + "=?" + " and " +
//							BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
//							BPNQuality.XML_ESTATE + "=?" + " and " +
//							BPNQuality.XML_BPN_DATE + "=?" + " and " +
//							BPNQuality.XML_DIVISION + "=?" + " and " +
//							BPNQuality.XML_GANG + "=?" + " and " +
//							BPNQuality.XML_LOCATION + "=?" + " and " +
//							BPNQuality.XML_TPH + "=?" + " and " +
//							BPNQuality.XML_NIK_HARVESTER + "=?", 
//							new String [] {id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});

						database.deleteData(BPNQuality.TABLE_NAME,
								BPNQuality.XML_BPN_ID + "=?" + " and " +
										BPNQuality.XML_COMPANY_CODE + "=?" + " and " +
										BPNQuality.XML_ESTATE + "=?" + " and " +
										BPNQuality.XML_BPN_DATE + "=?" + " and " +
										BPNQuality.XML_DIVISION + "=?" + " and " +
										BPNQuality.XML_GANG + "=?" + " and " +
										BPNQuality.XML_LOCATION + "=?" + " and " +
										BPNQuality.XML_TPH + "=?" + " and " +
										BPNQuality.XML_NIK_HARVESTER + "=?",
								new String[]{id, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});

//					database.deleteData(BPNHeader.TABLE_NAME, 
//							BPNHeader.XML_BPN_ID + "=?" + " and " +
//							BPNHeader.XML_IMEI + "=?" + " and " +
//							BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
//							BPNHeader.XML_ESTATE + "=?" + " and " +
//							BPNHeader.XML_BPN_DATE + "=?" + " and " +
//							BPNHeader.XML_DIVISION + "=?" + " and " +
//							BPNHeader.XML_GANG + "=?" + " and " +
//							BPNHeader.XML_LOCATION + "=?" + " and " +
//							BPNHeader.XML_TPH + "=?" + " and " +
//							BPNHeader.XML_NIK_HARVESTER + "=?", 
//							new String [] {id, imei, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});

						database.deleteData(BPNHeader.TABLE_NAME,
								BPNHeader.XML_BPN_ID + "=?" + " and " +
										BPNHeader.XML_COMPANY_CODE + "=?" + " and " +
										BPNHeader.XML_ESTATE + "=?" + " and " +
										BPNHeader.XML_BPN_DATE + "=?" + " and " +
										BPNHeader.XML_DIVISION + "=?" + " and " +
										BPNHeader.XML_GANG + "=?" + " and " +
										BPNHeader.XML_LOCATION + "=?" + " and " +
										BPNHeader.XML_TPH + "=?" + " and " +
										BPNHeader.XML_NIK_HARVESTER + "=?",
								new String[]{id, companyCode, estate, bpnDate, division, gang, location, tph, nikHarvester});

						database.commitTransaction();

						lstBpn.remove(pos);
						notifyDataSetChanged();
					}
				}catch(SQLiteException e){
					e.printStackTrace();
					database.closeTransaction();
				}finally{
					database.closeTransaction();
				}
			}else{
				new DialogNotification(activity, activity.getResources().getString(R.string.informasi), 
						activity.getResources().getString(R.string.data_delete_cannot), false).show();
			}
		}else{
			new DialogNotification(activity, activity.getResources().getString(R.string.informasi), 
					activity.getResources().getString(R.string.data_already_export), false).show();
		}
	}
	
	public void updateData(BPNHeader bpnHeader){
		int position = -1;
		
		for(int i = 0; i < lstBpn.size(); i++){
			if(bpnHeader.getBpnId().equalsIgnoreCase(lstBpn.get(i).getBpnId())){
				position = i;
				break;
			}
		}
		
		if(position > -1){
			lstBpn.set(position, bpnHeader);
			notifyDataSetChanged();
		}
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstBpn = null;
				lstBpn = (List<BPNHeader>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BPNHeader> list_result = new ArrayList<BPNHeader>();
				BPNHeader harvest_book;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BPNHeader>(lstBpn);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						harvest_book = lstTemp.get(i);
						
						if(harvest_book.getLocation().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(harvest_book);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
