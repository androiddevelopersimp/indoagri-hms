package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewDayOffItem;
import com.simp.hms.model.DayOff;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterDayOff extends BaseAdapter{
	private Context context;
	private List<DayOff> listDayOff, listTemp;
	private int layout;

	public AdapterDayOff(Context context, List<DayOff> listDayOff, int layout){
		this.context = context;
		this.listDayOff = listDayOff;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listDayOff.size();
	}

	@Override
	public Object getItem(int pos) {
		return listDayOff.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		DayOff dayOff = listDayOff.get(pos);
		
		ViewDayOffItem view;
		
		TextView txtDayOffEstate;
		TextView txtDayOffDate;
		TextView txtDayOffDayOffType;
		TextView txtDayOffDescription;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtDayOffEstate = (TextView) convertView.findViewById(R.id.txtDayOffEstate);
            txtDayOffDate = (TextView) convertView.findViewById(R.id.txtDayOffDate);
            txtDayOffDayOffType = (TextView) convertView.findViewById(R.id.txtDayOffDayOffType);
            txtDayOffDescription = (TextView) convertView.findViewById(R.id.txtDayOffDescription);
            
            view = new ViewDayOffItem(txtDayOffEstate, txtDayOffDate, txtDayOffDayOffType, txtDayOffDescription);
            
            convertView.setTag(view);
        }else{
        	view = (ViewDayOffItem) convertView.getTag();
        	
        	txtDayOffEstate = view.getTxtDayOffEstate();
        	txtDayOffDate = view.getTxtDayOffDate();
        	txtDayOffDayOffType = view.getTxtDayOffDayOffType();
        	txtDayOffDescription = view.getTxtDayOffDescription();
        }

        txtDayOffEstate.setText(dayOff.getEstate());
        txtDayOffDate.setText(dayOff.getDate());
        txtDayOffDayOffType.setText(dayOff.getDayOffType());
        txtDayOffDescription.setText(dayOff.getDescription());

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listDayOff = null;
				listDayOff = (List<DayOff>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<DayOff> listResult = new ArrayList<DayOff>();
				DayOff dayOff;
				
				if(listTemp == null){
					listTemp = new ArrayList<DayOff>(listDayOff);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						dayOff = listTemp.get(i);
						
						if(dayOff.getDayOffType().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(dayOff);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
