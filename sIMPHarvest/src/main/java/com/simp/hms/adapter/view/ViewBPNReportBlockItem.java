package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNReportBlockItem {
	private TextView txtBpnReportBlockItemBlock;
	private TextView txtBpnReportBlockItemQtyJanjang;
	private TextView txtBpnReportBlockItemQtyLooseFruit;
	private TextView txtBpnReportBlockItemQtyMentah;
	private TextView txtBpnReportBlockItemQtyBusuk;
	private TextView txtBpnReportBlockItemQtyTangkaiPanjang;

	public ViewBPNReportBlockItem(TextView txtBpnReportBlockItemBlock,
			TextView txtBpnReportBlockItemQtyJanjang,
			TextView txtBpnReportBlockItemQtyLooseFruit,
			TextView txtBpnReportBlockItemQtyMentah,
			TextView txtBpnReportBlockItemQtyBusuk,
			TextView txtBpnReportBlockItemQtyTangkaiPanjang) {
		super();
		this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
		this.txtBpnReportBlockItemQtyJanjang = txtBpnReportBlockItemQtyJanjang;
		this.txtBpnReportBlockItemQtyLooseFruit = txtBpnReportBlockItemQtyLooseFruit;
		this.txtBpnReportBlockItemQtyMentah = txtBpnReportBlockItemQtyMentah;
		this.txtBpnReportBlockItemQtyBusuk = txtBpnReportBlockItemQtyBusuk;
		this.txtBpnReportBlockItemQtyTangkaiPanjang = txtBpnReportBlockItemQtyTangkaiPanjang;
	}

	public TextView getTxtBpnReportBlockItemBlock() {
		return txtBpnReportBlockItemBlock;
	}

	public void setTxtBpnReportBlockItemBlock(
			TextView txtBpnReportBlockItemBlock) {
		this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
	}

	public TextView getTxtBpnReportBlockItemQtyJanjang() {
		return txtBpnReportBlockItemQtyJanjang;
	}

	public void setTxtBpnReportBlockItemQtyJanjang(
			TextView txtBpnReportBlockItemQtyJanjang) {
		this.txtBpnReportBlockItemQtyJanjang = txtBpnReportBlockItemQtyJanjang;
	}

	public TextView getTxtBpnReportBlockItemQtyLooseFruit() {
		return txtBpnReportBlockItemQtyLooseFruit;
	}

	public void setTxtBpnReportBlockItemQtyLooseFruit(
			TextView txtBpnReportBlockItemQtyLooseFruit) {
		this.txtBpnReportBlockItemQtyLooseFruit = txtBpnReportBlockItemQtyLooseFruit;
	}

	public TextView getTxtBpnReportBlockItemQtyMentah() {
		return txtBpnReportBlockItemQtyMentah;
	}

	public void setTxtBpnReportBlockItemQtyMentah(
			TextView txtBpnReportBlockItemQtyMentah) {
		this.txtBpnReportBlockItemQtyMentah = txtBpnReportBlockItemQtyMentah;
	}

	public TextView getTxtBpnReportBlockItemQtyBusuk() {
		return txtBpnReportBlockItemQtyBusuk;
	}

	public void setTxtBpnReportBlockItemQtyBusuk(
			TextView txtBpnReportBlockItemQtyBusuk) {
		this.txtBpnReportBlockItemQtyBusuk = txtBpnReportBlockItemQtyBusuk;
	}

	public TextView getTxtBpnReportBlockItemQtyTangkaiPanjang() {
		return txtBpnReportBlockItemQtyTangkaiPanjang;
	}

	public void setTxtBpnReportBlockItemQtyTangkaiPanjang(
			TextView txtBpnReportBlockItemQtyTangkaiPanjang) {
		this.txtBpnReportBlockItemQtyTangkaiPanjang = txtBpnReportBlockItemQtyTangkaiPanjang;
	}

}
