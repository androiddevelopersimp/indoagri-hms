package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.TextView;

public class ViewSpbsSummaryItem {
	private Button btnSpbsSummaryItemDelete;
	private TextView txtSpbsSummaryItemBlock;
	private TextView txtSpbsSummaryItemQtyJanjang;
	private TextView txtSpbsSummaryItemQtyLooseFruit;

	public ViewSpbsSummaryItem(Button btnSpbsSummaryItemDelete, TextView txtSpbsSummaryItemBlock,
			TextView txtSpbsSummaryItemQtyJanjang,
			TextView txtSpbsSummaryItemQtyLooseFruit) {
		super();
		this.btnSpbsSummaryItemDelete = btnSpbsSummaryItemDelete;
		this.txtSpbsSummaryItemBlock = txtSpbsSummaryItemBlock;
		this.txtSpbsSummaryItemQtyJanjang = txtSpbsSummaryItemQtyJanjang;
		this.txtSpbsSummaryItemQtyLooseFruit = txtSpbsSummaryItemQtyLooseFruit;
	}
	
	public Button getBtnSpbsSummaryItemDelete() {
		return btnSpbsSummaryItemDelete;
	}

	public void setBtnSpbsSummaryItemDelete(Button btnSpbsSummaryItemDelete) {
		this.btnSpbsSummaryItemDelete = btnSpbsSummaryItemDelete;
	}

	public TextView getTxtSpbsSummaryItemBlock() {
		return txtSpbsSummaryItemBlock;
	}

	public void setTxtSpbsSummaryItemBlock(TextView txtSpbsSummaryItemBlock) {
		this.txtSpbsSummaryItemBlock = txtSpbsSummaryItemBlock;
	}

	public TextView getTxtSpbsSummaryItemQtyJanjang() {
		return txtSpbsSummaryItemQtyJanjang;
	}

	public void setTxtSpbsSummaryItemQtyJanjang(
			TextView txtSpbsSummaryItemQtyJanjang) {
		this.txtSpbsSummaryItemQtyJanjang = txtSpbsSummaryItemQtyJanjang;
	}

	public TextView getTxtSpbsSummaryItemQtyLooseFruit() {
		return txtSpbsSummaryItemQtyLooseFruit;
	}

	public void setTxtSpbsSummaryItemQtyLooseFruit(
			TextView txtSpbsSummaryItemQtyLooseFruit) {
		this.txtSpbsSummaryItemQtyLooseFruit = txtSpbsSummaryItemQtyLooseFruit;
	}

}
