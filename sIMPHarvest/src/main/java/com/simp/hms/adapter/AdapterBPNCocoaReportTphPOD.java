package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNCocoaReportTphItemPOD;
import com.simp.hms.model.BPNCocoaReportTphPOD;
import com.simp.hms.model.DateLocal;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AdapterBPNCocoaReportTphPOD extends BaseAdapter{
    private Context context;
    private List<BPNCocoaReportTphPOD> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNCocoaReportTphPOD(Context context, List<BPNCocoaReportTphPOD> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNCocoaReportTphPOD item = lstSummary.get(pos);

        ViewBPNCocoaReportTphItemPOD view;
        TextView txtBpnReportTphItemTph;
        TextView txtBpnReportTphItemBpnDate;

        TextView txtBpnReportTphItemQtyPODQty;
        TextView txtBpnReportTphItemQtyGoodWeight;
        TextView txtBpnReportTphItemQtyEstimasiQty;
        TextView txtBpnReportTphItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtBpnReportTphItemTph = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemTph);
            txtBpnReportTphItemBpnDate = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemBpnDate);

            txtBpnReportTphItemQtyPODQty = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyPODQty);
            txtBpnReportTphItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyGoodWeight);
            txtBpnReportTphItemQtyEstimasiQty = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyEstimasiQty);
            txtBpnReportTphItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyPoorWeight);

            view = new ViewBPNCocoaReportTphItemPOD(txtBpnReportTphItemTph, txtBpnReportTphItemBpnDate,
                    txtBpnReportTphItemQtyPODQty, txtBpnReportTphItemQtyGoodWeight,
                    txtBpnReportTphItemQtyEstimasiQty, txtBpnReportTphItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewBPNCocoaReportTphItemPOD) convertView.getTag();

            txtBpnReportTphItemTph = view.getTxtBpnReportTphItemTph();
            txtBpnReportTphItemBpnDate = view.getTxtBpnReportTphItemBpnDate();

            txtBpnReportTphItemQtyPODQty = view.getTxtBpnReportTphItemQtyPODQty();
            txtBpnReportTphItemQtyGoodWeight = view.getTxtBpnReportTphItemQtyGoodWeight();
            txtBpnReportTphItemQtyEstimasiQty = view.getTxtBpnReportTphItemQtyEstimasiQty();
            txtBpnReportTphItemQtyPoorWeight = view.getTxtBpnReportTphItemQtyPoorWeight();
        }

        txtBpnReportTphItemTph.setText(item.getTph());
        txtBpnReportTphItemBpnDate.setText(new DateLocal(new Date(item.getCreatedDate())).getDateString(DateLocal.FORMAT_REPORT_VIEW));

        txtBpnReportTphItemQtyPODQty.setText(String.valueOf(Utils.round(item.getQtyPODQty(), 0)));
        txtBpnReportTphItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 0)));
        txtBpnReportTphItemQtyEstimasiQty.setText(String.valueOf(Utils.round(item.getQtyEstimasiQty(), 0)));
        txtBpnReportTphItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 0)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNCocoaReportTphPOD>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNCocoaReportTphPOD> lstResult = new ArrayList<BPNCocoaReportTphPOD>();
                BPNCocoaReportTphPOD item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNCocoaReportTphPOD>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getTph().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
