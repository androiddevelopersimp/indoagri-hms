package com.simp.hms.adapter.view;

import android.widget.TextView;

/**
 * Created by Fernando.Siagian on 3/11/2017.
 */

public class ViewBPNKaretReportTphItem {
    private TextView txtBpnReportTphItemTph;
    private TextView txtBpnReportTphItemBpnDate;

    private TextView txtBpnReportTphItemQtyLatexWet;
    private TextView txtBpnReportTphItemQtyLatexDRC;
    private TextView txtBpnReportTphItemQtyLatexDry;
    private TextView txtBpnReportTphItemQtyLumpWet;
    private TextView txtBpnReportTphItemQtyLumpDRC;
    private TextView txtBpnReportTphItemQtyLumpDry;
    private TextView txtBpnReportTphItemQtySlabWet;
    private TextView txtBpnReportTphItemQtySlabDRC;
    private TextView txtBpnReportTphItemQtySlabDry;
    private TextView txtBpnReportTphItemQtyTreelaceWet;
    private TextView txtBpnReportTphItemQtyTreelaceDRC;
    private TextView txtBpnReportTphItemQtyTreelaceDry;

    public ViewBPNKaretReportTphItem(
            TextView txtBpnReportTphItemTph,
            TextView txtBpnReportTphItemBpnDate,
            TextView txtBpnReportTphItemQtyLatexWet,
            TextView txtBpnReportTphItemQtyLatexDRC,
            TextView txtBpnReportTphItemQtyLatexDry,
            TextView txtBpnReportTphItemQtyLumpWet,
            TextView txtBpnReportTphItemQtyLumpDRC,
            TextView txtBpnReportTphItemQtyLumpDry,
            TextView txtBpnReportTphItemQtySlabWet,
            TextView txtBpnReportTphItemQtySlabDRC,
            TextView txtBpnReportTphItemQtySlabDry,
            TextView txtBpnReportTphItemQtyTreelaceWet,
            TextView txtBpnReportTphItemQtyTreelaceDRC,
            TextView txtBpnReportTphItemQtyTreelaceDry
    ) {
        super();
        this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
        this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
        this.txtBpnReportTphItemQtyLatexWet = txtBpnReportTphItemQtyLatexWet;
        this.txtBpnReportTphItemQtyLatexDRC = txtBpnReportTphItemQtyLatexDRC;
        this.txtBpnReportTphItemQtyLatexDry = txtBpnReportTphItemQtyLatexDry;
        this.txtBpnReportTphItemQtyLumpWet = txtBpnReportTphItemQtyLumpWet;
        this.txtBpnReportTphItemQtyLumpDRC = txtBpnReportTphItemQtyLumpDRC;
        this.txtBpnReportTphItemQtyLumpDry = txtBpnReportTphItemQtyLumpDry;
        this.txtBpnReportTphItemQtySlabWet = txtBpnReportTphItemQtySlabWet;
        this.txtBpnReportTphItemQtySlabDRC = txtBpnReportTphItemQtySlabDRC;
        this.txtBpnReportTphItemQtySlabDry = txtBpnReportTphItemQtySlabDry;
        this.txtBpnReportTphItemQtyTreelaceWet = txtBpnReportTphItemQtyTreelaceWet;
        this.txtBpnReportTphItemQtyTreelaceDRC = txtBpnReportTphItemQtyTreelaceDRC;
        this.txtBpnReportTphItemQtyTreelaceDry = txtBpnReportTphItemQtyTreelaceDry;
    }

    public TextView getTxtBpnReportTphItemTph() {
        return txtBpnReportTphItemTph;
    }

    public void setTxtBpnReportTphItemTph(TextView txtBpnReportTphItemTph) {
        this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
    }

    public TextView getTxtBpnReportTphItemBpnDate() {
        return txtBpnReportTphItemBpnDate;
    }

    public void setTxtBpnReportTphItemBpnDate(
            TextView txtBpnReportTphItemBpnDate) {
        this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
    }

    public TextView getTxtBpnReportTphItemQtyLatexWet() {
        return txtBpnReportTphItemQtyLatexWet;
    }

    public void setTxtBpnReportTphItemQtyLatexWet(
            TextView txtBpnReportTphItemQtyLatexWet) {
        this.txtBpnReportTphItemQtyLatexWet = txtBpnReportTphItemQtyLatexWet;
    }

    public TextView getTxtBpnReportTphItemQtyLatexDRC() {
        return txtBpnReportTphItemQtyLatexDRC;
    }

    public void setTxtBpnReportTphItemQtyLatexDRC(
            TextView txtBpnReportTphItemQtyLatexDRC) {
        this.txtBpnReportTphItemQtyLatexDRC = txtBpnReportTphItemQtyLatexDRC;
    }

    public TextView getTxtBpnReportTphItemQtyLatexDry() {
        return txtBpnReportTphItemQtyLatexDry;
    }

    public void setTxtBpnReportTphItemQtyLatexDry(
            TextView txtBpnReportTphItemQtyLatexDry) {
        this.txtBpnReportTphItemQtyLatexDry = txtBpnReportTphItemQtyLatexDry;
    }

    public TextView getTxtBpnReportTphItemQtyLumpWet() {
        return txtBpnReportTphItemQtyLumpWet;
    }

    public void setTxtBpnReportTphItemQtyLumpWet(
            TextView txtBpnReportTphItemQtyLumpWet) {
        this.txtBpnReportTphItemQtyLumpWet = txtBpnReportTphItemQtyLumpWet;
    }

    public TextView getTxtBpnReportTphItemQtyLumpDRC() {
        return txtBpnReportTphItemQtyLumpDRC;
    }

    public void setTxtBpnReportTphItemQtyLumpDRC(
            TextView txtBpnReportTphItemQtyLumpDRC) {
        this.txtBpnReportTphItemQtyLumpDRC = txtBpnReportTphItemQtyLumpDRC;
    }

    public TextView getTxtBpnReportTphItemQtyLumpDry() {
        return txtBpnReportTphItemQtyLumpDry;
    }

    public void setTxtBpnReportTphItemQtyLumpDry(
            TextView txtBpnReportTphItemQtyLumpDry) {
        this.txtBpnReportTphItemQtyLumpDry = txtBpnReportTphItemQtyLumpDry;
    }

    public TextView getTxtBpnReportTphItemQtySlabWet() {
        return txtBpnReportTphItemQtySlabWet;
    }

    public void setTxtBpnReportTphItemQtySlabWet(
            TextView txtBpnReportTphItemQtySlabWet) {
        this.txtBpnReportTphItemQtySlabWet = txtBpnReportTphItemQtySlabWet;
    }

    public TextView getTxtBpnReportTphItemQtySlabDRC() {
        return txtBpnReportTphItemQtySlabDRC;
    }

    public void setTxtBpnReportTphItemQtySlabDRC(
            TextView txtBpnReportTphItemQtySlabDRC) {
        this.txtBpnReportTphItemQtySlabDRC = txtBpnReportTphItemQtySlabDRC;
    }

    public TextView getTxtBpnReportTphItemQtySlabDry() {
        return txtBpnReportTphItemQtySlabDry;
    }

    public void setTxtBpnReportTphItemQtySlabDry(
            TextView txtBpnReportTphItemQtySlabDry) {
        this.txtBpnReportTphItemQtySlabDry = txtBpnReportTphItemQtySlabDry;
    }

    public TextView getTxtBpnReportTphItemQtyTreelaceWet() {
        return txtBpnReportTphItemQtyTreelaceWet;
    }

    public void setTxtBpnReportTphItemQtyTreelaceWet(
            TextView txtBpnReportTphItemQtyTreelaceWet) {
        this.txtBpnReportTphItemQtyTreelaceWet = txtBpnReportTphItemQtyTreelaceWet;
    }

    public TextView getTxtBpnReportTphItemQtyTreelaceDRC() {
        return txtBpnReportTphItemQtyTreelaceDRC;
    }

    public void setTxtBpnReportTphItemQtyTreelaceDRC(
            TextView txtBpnReportTphItemQtyTreelaceDRC) {
        this.txtBpnReportTphItemQtyTreelaceDRC = txtBpnReportTphItemQtyTreelaceDRC;
    }

    public TextView getTxtBpnReportTphItemQtyTreelaceDry() {
        return txtBpnReportTphItemQtyTreelaceDry;
    }

    public void setTxtBpnReportTphItemQtyTreelaceDry(
            TextView txtBpnReportTphItemQtyTreelaceDry) {
        this.txtBpnReportTphItemQtyTreelaceDry = txtBpnReportTphItemQtyTreelaceDry;
    }
}
