package com.simp.hms.adapter.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.TextView;

public class ViewSPUReportTphItemPOD{
    private TextView txtSpuReportTphItemTph;
    private TextView txtSpuReportTphItemQtyPODQty;
    private TextView txtSpuReportTphItemQtyGoodWeight;
    private TextView txtSpuReportTphItemQtyEstimasiQty;
    private TextView txtSpuReportTphItemQtyPoorWeight;

    public ViewSPUReportTphItemPOD(TextView txtSpuReportTphItemTph,
                                   TextView txtSpuReportTphItemQtyPODQty,
                                   TextView txtSpuReportTphItemQtyGoodWeight,
                                   TextView txtSpuReportTphItemQtyEstimasiQty,
                                   TextView txtSpuReportTphItemQtyPoorWeight) {
        super();
        this.txtSpuReportTphItemTph = txtSpuReportTphItemTph;
        this.txtSpuReportTphItemQtyPODQty = txtSpuReportTphItemQtyPODQty;
        this.txtSpuReportTphItemQtyGoodWeight = txtSpuReportTphItemQtyGoodWeight;
        this.txtSpuReportTphItemQtyEstimasiQty = txtSpuReportTphItemQtyEstimasiQty;
        this.txtSpuReportTphItemQtyPoorWeight = txtSpuReportTphItemQtyPoorWeight;
    }

    public TextView getTxtSpuReportTphItemTph() {
        return txtSpuReportTphItemTph;
    }

    public void setTxtSpuReportTphItemTph(TextView txtSpuReportTphItemTph) {
        this.txtSpuReportTphItemTph = txtSpuReportTphItemTph;
    }

    public TextView getTxtSpuReportTphItemQtyPODQty() {
        return txtSpuReportTphItemQtyPODQty;
    }

    public void setTxtSpuReportTphItemQtyPODQty(TextView txtSpuReportTphItemQtyPODQty) {
        this.txtSpuReportTphItemQtyPODQty = txtSpuReportTphItemQtyPODQty;
    }

    public TextView getTxtSpuReportTphItemQtyGoodWeight() {
        return txtSpuReportTphItemQtyGoodWeight;
    }

    public void setTxtSpuReportTphItemQtyGoodWeight(TextView txtSpuReportTphItemQtyGoodWeight) {
        this.txtSpuReportTphItemQtyGoodWeight = txtSpuReportTphItemQtyGoodWeight;
    }

    public TextView getTxtSpuReportTphItemQtyEstimasiQty() {
        return txtSpuReportTphItemQtyEstimasiQty;
    }

    public void setTxtSpuReportTphItemQtyEstimasiQty(TextView txtSpuReportTphItemQtyEstimasiQty) {
        this.txtSpuReportTphItemQtyEstimasiQty = txtSpuReportTphItemQtyEstimasiQty;
    }

    public TextView getTxtSpuReportTphItemQtyPoorWeight() {
        return txtSpuReportTphItemQtyPoorWeight;
    }

    public void setTxtSpuReportTphItemQtyPoorWeight(TextView txtSpuReportTphItemQtyPoorWeight) {
        this.txtSpuReportTphItemQtyPoorWeight = txtSpuReportTphItemQtyPoorWeight;
    }


}
