package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ViewSpuBlockItemPODS {
    private Button btnSpuBlockItemDelete;
    private TextView txtSpuBlockItemTph;
    private TextView txtSpuBlockItemBpnDate;
    private EditText edtSpuBlockItemQtyPODS;
    private EditText edtSpuBlockItemQtyGoodWeight;
    private EditText edtSpuBlockItemQtyBadWeight;
    private EditText edtSpuBlockItemQtyPoorWeight;
    private TextView txtSpuBlockItemHarvester;
    private Button btnSpuBlockItemWarning;

    public ViewSpuBlockItemPODS() {
    }

    public ViewSpuBlockItemPODS(Button btnSpuBlockItemDelete,
                                TextView txtSpuBlockItemTph, TextView txtSpuBlockItemBpnDate, EditText edtSpuBlockItemQtyPODS,
                                EditText edtSpuBlockItemQtyGoodWeight, EditText edtSpuBlockItemQtyBadWeight,
                                EditText edtSpuBlockItemQtyPoorWeight, Button btnSpuBlockItemWarning,
                                TextView txtSpuBlockItemHarvester) {
        super();
        this.btnSpuBlockItemDelete = btnSpuBlockItemDelete;
        this.txtSpuBlockItemTph = txtSpuBlockItemTph;
        this.txtSpuBlockItemBpnDate = txtSpuBlockItemBpnDate;
        this.edtSpuBlockItemQtyPODS = edtSpuBlockItemQtyPODS;
        this.edtSpuBlockItemQtyGoodWeight = edtSpuBlockItemQtyGoodWeight;
        this.edtSpuBlockItemQtyBadWeight = edtSpuBlockItemQtyBadWeight;
        this.edtSpuBlockItemQtyPoorWeight = edtSpuBlockItemQtyPoorWeight;
        this.btnSpuBlockItemWarning = btnSpuBlockItemWarning;
        this.txtSpuBlockItemHarvester = txtSpuBlockItemHarvester;
    }

    public EditText getEdtSpuBlockItemQtyPODS() {
        return edtSpuBlockItemQtyPODS;
    }

    public void setEdtSpuBlockItemQtyPODS(EditText edtSpuBlockItemQtyPODS) {
        this.edtSpuBlockItemQtyPODS = edtSpuBlockItemQtyPODS;
    }

    public Button getBtnSpuBlockItemDelete() {
        return btnSpuBlockItemDelete;
    }

    public void setBtnSpuBlockItemDelete(Button btnSpuBlockItemDelete) {
        this.btnSpuBlockItemDelete = btnSpuBlockItemDelete;
    }

    public TextView getTxtSpuBlockItemTph() {
        return txtSpuBlockItemTph;
    }

    public void setTxtSpuBlockItemTph(TextView txtSpuBlockItemTph) {
        this.txtSpuBlockItemTph = txtSpuBlockItemTph;
    }

    public TextView getTxtSpuBlockItemBpnDate() {
        return txtSpuBlockItemBpnDate;
    }

    public void setTxtSpuBlockItemBpnDate(TextView txtSpuBlockItemBpnDate) {
        this.txtSpuBlockItemBpnDate = txtSpuBlockItemBpnDate;
    }

    public EditText getEdtSpuBlockItemQtyGoodWeight() {
        return edtSpuBlockItemQtyGoodWeight;
    }

    public void setEdtSpuBlockItemQtyGoodWeight(
            EditText edtSpuBlockItemQtyGoodWeight) {
        this.edtSpuBlockItemQtyGoodWeight = edtSpuBlockItemQtyGoodWeight;
    }

    public EditText getEdtSpuBlockItemQtyBadWeight() {
        return edtSpuBlockItemQtyBadWeight;
    }

    public void setEdtSpuBlockItemQtyBadWeight(
            EditText edtSpuBlockItemQtyBadWeight) {
        this.edtSpuBlockItemQtyBadWeight = edtSpuBlockItemQtyBadWeight;
    }

    public EditText getEdtSpuBlockItemQtyPoorWeight() {
        return edtSpuBlockItemQtyPoorWeight;
    }

    public void setEdtSpuBlockItemQtyPoorWeight(
            EditText edtSpuBlockItemQtyPoorWeight) {
        this.edtSpuBlockItemQtyPoorWeight = edtSpuBlockItemQtyPoorWeight;
    }

    public TextView getTxtSpuBlockItemHarvester() {
        return txtSpuBlockItemHarvester;
    }

    public void setTxtSpuBlockItemHarvester(TextView txtSpuBlockItemHarvester) {
        this.txtSpuBlockItemHarvester = txtSpuBlockItemHarvester;
    }
}
