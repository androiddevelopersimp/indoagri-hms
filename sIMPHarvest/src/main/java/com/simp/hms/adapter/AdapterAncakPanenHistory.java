package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewAncakPanenHistoryItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.model.AncakPanenHeader;
import com.simp.hms.model.AncakPanenQuality;
import com.simp.hms.model.DateLocal;

import android.app.Activity;
import android.database.sqlite.SQLiteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterAncakPanenHistory extends BaseAdapter{
	private Activity activity;
	private List<AncakPanenHeader> lstData, lstTemp;
	private int layout;
	
	public AdapterAncakPanenHistory(Activity activity, List<AncakPanenHeader> lstData, int layout){
		this.activity = activity;
		this.lstData = lstData;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstData.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstData.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		AncakPanenHeader ancakPanenHeader = lstData.get(pos);
		
		ViewAncakPanenHistoryItem vie_ancak_panen;
		TextView txt_ancak_panen_history_item_date;
		TextView txt_ancak_panen_history_item_clerk;
		TextView txt_ancak_panen_history_item_foreman;
		TextView txt_ancak_panen_history_item_block;
		TextView txt_ancak_panen_history_item_harvester;
		TextView txt_ancak_panen_history_item_tph;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txt_ancak_panen_history_item_date = (TextView) convertView.findViewById(R.id.txt_ancak_panen_history_item_date);
            txt_ancak_panen_history_item_clerk = (TextView) convertView.findViewById(R.id.txt_ancak_panen_history_item_clerk);
            txt_ancak_panen_history_item_foreman = (TextView) convertView.findViewById(R.id.txt_ancak_panen_history_item_foreman);
            txt_ancak_panen_history_item_block = (TextView) convertView.findViewById(R.id.txt_ancak_panen_history_item_block);
            txt_ancak_panen_history_item_harvester = (TextView) convertView.findViewById(R.id.txt_ancak_panen_history_item_harvester);
            txt_ancak_panen_history_item_tph = (TextView) convertView.findViewById(R.id.txt_ancak_panen_history_item_tph);
            
            vie_ancak_panen = new ViewAncakPanenHistoryItem(txt_ancak_panen_history_item_date, txt_ancak_panen_history_item_clerk, txt_ancak_panen_history_item_foreman,
            		txt_ancak_panen_history_item_block, txt_ancak_panen_history_item_harvester, txt_ancak_panen_history_item_tph);     
            convertView.setTag(vie_ancak_panen);
        }else{
        	vie_ancak_panen = (ViewAncakPanenHistoryItem) convertView.getTag();
        	
        	txt_ancak_panen_history_item_date = vie_ancak_panen.getTxtAncakPanenHistoryItemDate();
        	txt_ancak_panen_history_item_clerk = vie_ancak_panen.getTxtAncakPanenHistoryItemClerk();
        	txt_ancak_panen_history_item_foreman = vie_ancak_panen.getTxtAncakPanenHistoryItemForeman();
        	txt_ancak_panen_history_item_block = vie_ancak_panen.getTxtAncakPanenHistoryItemBlock();
        	txt_ancak_panen_history_item_harvester = vie_ancak_panen.getTxtAncakPanenHistoryItemHarvester();
        	txt_ancak_panen_history_item_tph = vie_ancak_panen.getTxtAncakPanenHistoryItemTph();
        }

        txt_ancak_panen_history_item_date.setText(new DateLocal(ancakPanenHeader.getAncakDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txt_ancak_panen_history_item_clerk.setText(ancakPanenHeader.getClerk());
        txt_ancak_panen_history_item_foreman.setText(ancakPanenHeader.getForeman());
        txt_ancak_panen_history_item_block.setText(ancakPanenHeader.getLocation());
        txt_ancak_panen_history_item_harvester.setText(ancakPanenHeader.getHarvester());
        txt_ancak_panen_history_item_tph.setText(ancakPanenHeader.getTph());

        return convertView;
	}
	
	public void deleteData(int pos){
		AncakPanenHeader item = lstData.get(pos);
		
		String id = item.getAncakPanenId();
		String imei = item.getImei();
		String companyCode = item.getCompanyCode();
		String estate = item.getEstate();
		String ancakDate = item.getAncakDate();
		String division = item.getDivision();
		String gang = item.getGang();
		String location = item.getLocation();
		String tph = item.getTph();
		String nikHarvester = item.getNikHarvester();
		int status = item.getStatus();
		
		if(status == 0){
				DatabaseHandler database = new DatabaseHandler(activity);
				
				try{
					database.openTransaction();

					database.deleteData(AncakPanenQuality.TABLE_NAME, 
							AncakPanenQuality.XML_ANCAK_PANEN_ID + "=?" + " and " +
							AncakPanenQuality.XML_COMPANY_CODE + "=?" + " and " +
							AncakPanenQuality.XML_ESTATE + "=?" + " and " +
							AncakPanenQuality.XML_ANCAK_DATE + "=?" + " and " +
							AncakPanenQuality.XML_DIVISION + "=?" + " and " +
							AncakPanenQuality.XML_GANG + "=?" + " and " +
							AncakPanenQuality.XML_LOCATION + "=?" + " and " +
							AncakPanenQuality.XML_TPH + "=?" + " and " +
							AncakPanenQuality.XML_NIK_HARVESTER + "=?", 
							new String [] {id, companyCode, estate, ancakDate, division, gang, location, tph, nikHarvester});

					database.deleteData(AncakPanenHeader.TABLE_NAME, 
							AncakPanenHeader.XML_ANCAK_PANEN_ID + "=?" + " and " +
							AncakPanenHeader.XML_COMPANY_CODE + "=?" + " and " +
							AncakPanenHeader.XML_ESTATE + "=?" + " and " +
							AncakPanenHeader.XML_ANCAK_DATE + "=?" + " and " +
							AncakPanenHeader.XML_DIVISION + "=?" + " and " +
							AncakPanenHeader.XML_GANG + "=?" + " and " +
							AncakPanenHeader.XML_LOCATION + "=?" + " and " +
							AncakPanenHeader.XML_TPH + "=?" + " and " +
							AncakPanenHeader.XML_NIK_HARVESTER + "=?", 
							new String [] {id, companyCode, estate, ancakDate, division, gang, location, tph, nikHarvester});
					
					database.commitTransaction();
					
					lstData.remove(pos);
					notifyDataSetChanged();
				}catch(SQLiteException e){
					e.printStackTrace();
					database.closeTransaction();
				}finally{
					database.closeTransaction();
				}
		}else{
			new DialogNotification(activity, activity.getResources().getString(R.string.informasi), 
					activity.getResources().getString(R.string.data_already_export), false).show();
		}
	}
	
	public void updateData(AncakPanenHeader ancakPanenHeader){
		int position = -1;
		
		for(int i = 0; i < lstData.size(); i++){
			if(ancakPanenHeader.getAncakPanenId().equalsIgnoreCase(lstData.get(i).getAncakPanenId())){
				position = i;
				break;
			}
		}
		
		if(position > -1){
			lstData.set(position, ancakPanenHeader);
			notifyDataSetChanged();
		}
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstData = null;
				lstData = (List<AncakPanenHeader>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<AncakPanenHeader> list_result = new ArrayList<AncakPanenHeader>();
				AncakPanenHeader ancak_panen;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<AncakPanenHeader>(lstData);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						ancak_panen = lstTemp.get(i);
						
						if(ancak_panen.getLocation().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(ancak_panen);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
