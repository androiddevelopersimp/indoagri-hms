package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNCocoaReportTphItem {
    private TextView txtBpnReportTphItemTph;
    private TextView txtBpnReportTphItemBpnDate;

    private TextView txtBpnReportTphItemQtyGoodQty;
    private TextView txtBpnReportTphItemQtyGoodWeight;
    private TextView txtBpnReportTphItemQtyBadQty;
    private TextView txtBpnReportTphItemQtyBadWeight;
    private TextView txtBpnReportTphItemQtyPoorQty;
    private TextView txtBpnReportTphItemQtyPoorWeight;

    public ViewBPNCocoaReportTphItem(
            TextView txtBpnReportTphItemTph,
            TextView txtBpnReportTphItemBpnDate,
            TextView txtBpnReportTphItemQtyGoodQty,
            TextView txtBpnReportTphItemQtyGoodWeight,
            TextView txtBpnReportTphItemQtyBadQty,
            TextView txtBpnReportTphItemQtyBadWeight,
            TextView txtBpnReportTphItemQtyPoorQty,
            TextView txtBpnReportTphItemQtyPoorWeight
    ) {
        super();
        this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
        this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
        this.txtBpnReportTphItemQtyGoodQty = txtBpnReportTphItemQtyGoodQty;
        this.txtBpnReportTphItemQtyGoodWeight = txtBpnReportTphItemQtyGoodWeight;
        this.txtBpnReportTphItemQtyBadQty = txtBpnReportTphItemQtyBadQty;
        this.txtBpnReportTphItemQtyBadWeight = txtBpnReportTphItemQtyBadWeight;
        this.txtBpnReportTphItemQtyPoorQty = txtBpnReportTphItemQtyPoorQty;
        this.txtBpnReportTphItemQtyPoorWeight = txtBpnReportTphItemQtyPoorWeight;
    }

    public TextView getTxtBpnReportTphItemTph() {
        return txtBpnReportTphItemTph;
    }

    public void setTxtBpnReportTphItemTph(TextView txtBpnReportTphItemTph) {
        this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
    }

    public TextView getTxtBpnReportTphItemBpnDate() {
        return txtBpnReportTphItemBpnDate;
    }

    public void setTxtBpnReportTphItemBpnDate(
            TextView txtBpnReportTphItemBpnDate) {
        this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
    }

    public TextView getTxtBpnReportTphItemQtyGoodQty() {
        return txtBpnReportTphItemQtyGoodQty;
    }

    public void setTxtBpnReportTphItemQtyGoodQty(
            TextView txtBpnReportTphItemQtyGoodQty) {
        this.txtBpnReportTphItemQtyGoodQty = txtBpnReportTphItemQtyGoodQty;
    }

    public TextView getTxtBpnReportTphItemQtyGoodWeight() {
        return txtBpnReportTphItemQtyGoodWeight;
    }

    public void setTxtBpnReportTphItemQtyGoodWeight(
            TextView txtBpnReportTphItemQtyGoodWeight) {
        this.txtBpnReportTphItemQtyGoodWeight = txtBpnReportTphItemQtyGoodWeight;
    }

    public TextView getTxtBpnReportTphItemQtyBadQty() {
        return txtBpnReportTphItemQtyBadQty;
    }

    public void setTxtBpnReportTphItemQtyBadQty(
            TextView txtBpnReportTphItemQtyBadQty) {
        this.txtBpnReportTphItemQtyBadQty = txtBpnReportTphItemQtyBadQty;
    }

    public TextView getTxtBpnReportTphItemQtyBadWeight() {
        return txtBpnReportTphItemQtyBadWeight;
    }

    public void setTxtBpnReportTphItemQtyBadWeight(
            TextView txtBpnReportTphItemQtyBadWeight) {
        this.txtBpnReportTphItemQtyBadWeight = txtBpnReportTphItemQtyBadWeight;
    }

    public TextView getTxtBpnReportTphItemQtyPoorQty() {
        return txtBpnReportTphItemQtyPoorQty;
    }

    public void setTxtBpnReportTphItemQtyPoorQty(
            TextView txtBpnReportTphItemQtyPoorQty) {
        this.txtBpnReportTphItemQtyPoorQty = txtBpnReportTphItemQtyPoorQty;
    }

    public TextView getTxtBpnReportTphItemQtyPoorWeight() {
        return txtBpnReportTphItemQtyPoorWeight;
    }

    public void setTxtBpnReportTphItemQtyPoorWeight(
            TextView txtBpnReportTphItemQtyPoorWeight) {
        this.txtBpnReportTphItemQtyPoorWeight = txtBpnReportTphItemQtyPoorWeight;
    }

}
