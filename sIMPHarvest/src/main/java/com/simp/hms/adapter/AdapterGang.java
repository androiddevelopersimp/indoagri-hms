package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewGangItem;
import com.simp.hms.model.Gang;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterGang extends BaseAdapter{
	private Context context;
	private List<Gang> lst_gang, lst_temp;
	private int layout;
	
	private ViewGangItem vie_gang;
	private TextView txt_gang_item_code;
	
	public AdapterGang(Context context, List<Gang> lst_gang, int layout){
		this.context = context;
		this.lst_gang = lst_gang;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_gang.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_gang.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		Gang gang = lst_gang.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
        	txt_gang_item_code = (TextView) convertView.findViewById(R.id.txt_gang_item_code);
            
            vie_gang = new ViewGangItem(txt_gang_item_code);    
            convertView.setTag(vie_gang);
        }else{
        	vie_gang = (ViewGangItem) convertView.getTag();
        	
        	txt_gang_item_code = vie_gang.getTxtGangItemCode();
        }

    	txt_gang_item_code.setText(String.valueOf(gang.getCode()));

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lst_gang = null;
				lst_gang = (List<Gang>) results.values;
				notifyDataSetChanged();
			}
			
			@Override    
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<Gang> list_result = new ArrayList<Gang>();
				Gang gang;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<Gang>(lst_gang);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						gang = lst_temp.get(i);
						
						if(gang.getCode().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(gang);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
