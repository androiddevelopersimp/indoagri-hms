package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNHistoryItem {
	private TextView txt_harvest_book_history_item_date;
	private TextView txt_harvest_book_history_item_clerk;
	private TextView txt_harvest_book_history_item_foreman;
	private TextView txt_harvest_book_history_item_block;
	private TextView txt_harvest_book_history_item_harvester;
	private TextView txt_harvest_book_history_item_tph;
	
	public ViewBPNHistoryItem(TextView txt_harvest_book_history_item_date,
			TextView txt_harvest_book_history_item_clerk,
			TextView txt_harvest_book_history_item_foreman,
			TextView txt_harvest_book_history_item_block,
			TextView txt_harvest_book_history_item_harvester, TextView txt_harvest_book_history_item_tph) {

		this.txt_harvest_book_history_item_date = txt_harvest_book_history_item_date;
		this.txt_harvest_book_history_item_clerk = txt_harvest_book_history_item_clerk;
		this.txt_harvest_book_history_item_foreman = txt_harvest_book_history_item_foreman;
		this.txt_harvest_book_history_item_block = txt_harvest_book_history_item_block;
		this.txt_harvest_book_history_item_harvester = txt_harvest_book_history_item_harvester;
		this.txt_harvest_book_history_item_tph = txt_harvest_book_history_item_tph;
	}

	public TextView getTxtHarvestBookHistoryItemDate() {
		return txt_harvest_book_history_item_date;
	}

	public void setTxtHarvestBookHistoryItemDate(
			TextView txt_harvest_book_history_item_date) {
		this.txt_harvest_book_history_item_date = txt_harvest_book_history_item_date;
	}

	public TextView getTxtHarvestBookHistoryItemClerk() {
		return txt_harvest_book_history_item_clerk;
	}

	public void setTxtHarvestBookHistoryItemClerk(
			TextView txt_harvest_book_history_item_clerk) {
		this.txt_harvest_book_history_item_clerk = txt_harvest_book_history_item_clerk;
	}

	public TextView getTxtHarvestBookHistoryItemForeman() {
		return txt_harvest_book_history_item_foreman;
	}

	public void setTxtHarvestBookHistoryItemForeman(
			TextView txt_harvest_book_history_item_foreman) {
		this.txt_harvest_book_history_item_foreman = txt_harvest_book_history_item_foreman;
	}

	public TextView getTxtHarvestBookHistoryItemBlock() {
		return txt_harvest_book_history_item_block;
	}

	public void setTxtHarvestBookHistoryItemBlock(
			TextView txt_harvest_book_history_item_block) {
		this.txt_harvest_book_history_item_block = txt_harvest_book_history_item_block;
	}

	public TextView getTxtHarvestBookHistoryItemHarvester() {
		return txt_harvest_book_history_item_harvester;
	}

	public void setTxtHarvestBookHistoryItemHarvester(
			TextView txt_harvest_book_history_item_harvester) {
		this.txt_harvest_book_history_item_harvester = txt_harvest_book_history_item_harvester;
	}
	
	public TextView getTxtHarvestBookHistoryItemTph() {
		return txt_harvest_book_history_item_tph;
	}

	public void setTxtHarvestBookHistoryItemTph(
			TextView txt_harvest_book_history_item_tph) {
		this.txt_harvest_book_history_item_tph = txt_harvest_book_history_item_tph;
	}
	
	
}
