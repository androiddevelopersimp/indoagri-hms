package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSReportNumberItem;
import com.simp.hms.model.SPBSReportNumber;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSPBSReportNumber extends BaseAdapter{
	private Context context;
	private List<SPBSReportNumber> lstSummary, lstTemp;
	private int layout;
	
	public AdapterSPBSReportNumber(Context context, List<SPBSReportNumber> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		SPBSReportNumber item = lstSummary.get(pos);
		
		ViewSPBSReportNumberItem view;
		TextView txtSpbsReportNumberItemSpbsNumber;
		TextView txtSpbsReportNumberItemSpbsDate;
		TextView txtSpbsReportNumberItemDriver;
		TextView txtSpbsReportNumberItemLicensePlate;
		TextView txtSpbsReportNumberItemQtyJanjang;
		TextView txtSpbsReportNumberItemQtyLooseFruit;
		TextView txtSpbsReportNumberItemDestType;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtSpbsReportNumberItemSpbsNumber = (TextView) convertView.findViewById(R.id.txtSpbsReportNumberItemSpbsNumber);
            txtSpbsReportNumberItemSpbsDate = (TextView) convertView.findViewById(R.id.txtSpbsReportNumberItemSpbsDate);
            txtSpbsReportNumberItemDriver = (TextView) convertView.findViewById(R.id.txtSpbsReportNumberItemDriver); 
            txtSpbsReportNumberItemLicensePlate = (TextView) convertView.findViewById(R.id.txtSpbsReportNumberItemLicensePlate);
            txtSpbsReportNumberItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtSpbsReportNumberItemQtyJanjang);
            txtSpbsReportNumberItemQtyLooseFruit = (TextView) convertView.findViewById(R.id.txtSpbsReportNumberItemQtyLooseFruit);
			txtSpbsReportNumberItemDestType = (TextView)convertView.findViewById(R.id.txtSpbsReportNumberItemDestType);
            
            view = new ViewSPBSReportNumberItem(txtSpbsReportNumberItemSpbsNumber, txtSpbsReportNumberItemSpbsDate, txtSpbsReportNumberItemDriver,
            		txtSpbsReportNumberItemLicensePlate, txtSpbsReportNumberItemQtyJanjang, txtSpbsReportNumberItemQtyLooseFruit, txtSpbsReportNumberItemDestType);
             
            convertView.setTag(view);
        }else{
        	view = (ViewSPBSReportNumberItem) convertView.getTag();
        	
        	txtSpbsReportNumberItemSpbsNumber = view.getTxtSpbsReportNumberItemSpbsNumber();
			txtSpbsReportNumberItemDestType = view.getTxtSpbsReportNumberDestType();
        	txtSpbsReportNumberItemSpbsDate = view.getTxtSpbsReportNumberItemSpbsDate();
        	txtSpbsReportNumberItemDriver = view.getTxtSpbsReportNumberItemDriver();
        	txtSpbsReportNumberItemLicensePlate = view.getTxtSpbsReportNumberItemLicensePlate();
        	txtSpbsReportNumberItemQtyJanjang = view.getTxtSpbsReportNumberItemQtyJanjang();
        	txtSpbsReportNumberItemQtyLooseFruit = view.getTxtSpbsReportNumberItemQtyLooseFruit();
        }

        txtSpbsReportNumberItemSpbsNumber.setText(item.getSpbsNumber());
		txtSpbsReportNumberItemDestType.setText(item.getDesttype());
        txtSpbsReportNumberItemSpbsDate.setText(item.getSpbsDate());
        txtSpbsReportNumberItemDriver.setText(item.getDriver());
        txtSpbsReportNumberItemLicensePlate.setText(item.getLicensePlate());
        txtSpbsReportNumberItemQtyJanjang.setText(String.valueOf((int) item.getQtyJanjangAngkut()));
        txtSpbsReportNumberItemQtyLooseFruit.setText(String.valueOf(Utils.round(item.getQtyLooseFruitAngkut(), 2)));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<SPBSReportNumber>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SPBSReportNumber> lstResult = new ArrayList<SPBSReportNumber>();
				SPBSReportNumber item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<SPBSReportNumber>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getSpbsNumber().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
