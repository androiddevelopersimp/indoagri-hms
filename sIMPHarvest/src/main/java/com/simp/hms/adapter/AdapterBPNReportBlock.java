package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNReportBlockItem;
import com.simp.hms.model.BPNReportBlock;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBPNReportBlock extends BaseAdapter{
	private Context context;
	private List<BPNReportBlock> lstSummary, lstTemp;
	private int layout;
	
	public AdapterBPNReportBlock(Context context, List<BPNReportBlock> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BPNReportBlock item = lstSummary.get(pos);
		
		ViewBPNReportBlockItem view;
		TextView txtBpnReportBlockItemBlock;
		TextView txtBpnReportBlockItemQtyJanjang;
		TextView txtBpnReportBlockItemQtyLooseFruit;
		TextView txtBpnReportBlockItemQtyMentah;
		TextView txtBpnReportBlockItemQtyBusuk;
		TextView txtBpnReportBlockItemQtyTangkaiPanjang;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtBpnReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemBlock);
            txtBpnReportBlockItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyJanjang);
            txtBpnReportBlockItemQtyLooseFruit = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyLooseFruit);
            txtBpnReportBlockItemQtyMentah = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyMentah);
            txtBpnReportBlockItemQtyBusuk = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyBusuk);
            txtBpnReportBlockItemQtyTangkaiPanjang = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyTangkaiPanjang);
            
            view = new ViewBPNReportBlockItem(txtBpnReportBlockItemBlock, txtBpnReportBlockItemQtyJanjang, txtBpnReportBlockItemQtyLooseFruit,
            		txtBpnReportBlockItemQtyMentah, txtBpnReportBlockItemQtyBusuk, txtBpnReportBlockItemQtyTangkaiPanjang);
             
            convertView.setTag(view);
        }else{
        	view = (ViewBPNReportBlockItem) convertView.getTag();
        	
        	txtBpnReportBlockItemBlock = view.getTxtBpnReportBlockItemBlock();
        	txtBpnReportBlockItemQtyJanjang = view.getTxtBpnReportBlockItemQtyJanjang();
        	txtBpnReportBlockItemQtyLooseFruit = view.getTxtBpnReportBlockItemQtyLooseFruit();
        	txtBpnReportBlockItemQtyMentah = view.getTxtBpnReportBlockItemQtyMentah();
        	txtBpnReportBlockItemQtyBusuk = view.getTxtBpnReportBlockItemQtyBusuk();
        	txtBpnReportBlockItemQtyTangkaiPanjang = view.getTxtBpnReportBlockItemQtyTangkaiPanjang();
        }

        txtBpnReportBlockItemBlock.setText(item.getBlock());
        txtBpnReportBlockItemQtyJanjang.setText(String.valueOf((int) item.getQtyJanjang()));
        txtBpnReportBlockItemQtyLooseFruit.setText(String.valueOf(Utils.round(item.getQtyLooseFruit(), 2)));
        txtBpnReportBlockItemQtyMentah.setText(String.valueOf((int) item.getQtyMentah()));
        txtBpnReportBlockItemQtyBusuk.setText(String.valueOf((int) item.getQtyBusuk()));
        txtBpnReportBlockItemQtyTangkaiPanjang.setText(String.valueOf((int) item.getQtyTangkaiPanjang()));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<BPNReportBlock>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BPNReportBlock> lstResult = new ArrayList<BPNReportBlock>();
				BPNReportBlock item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BPNReportBlock>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
