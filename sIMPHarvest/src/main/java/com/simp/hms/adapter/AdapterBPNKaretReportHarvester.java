package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNKaretReportHarvesterItem;
import com.simp.hms.adapter.view.ViewBPNReportHarvesterItem;
import com.simp.hms.model.BPNKaretReportHarvester;
import com.simp.hms.model.BPNReportHarvester;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Fernando.Siagian on 27/10/2017.
 */

public class AdapterBPNKaretReportHarvester extends BaseAdapter {
    private Context context;
    private List<BPNKaretReportHarvester> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNKaretReportHarvester(Context context, List<BPNKaretReportHarvester> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }


    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNKaretReportHarvester item = lstSummary.get(pos);

        ViewBPNKaretReportHarvesterItem view;
        TextView txtBpnReportHarvesterItemHarvesterName;

        TextView txtBpnReportHarvesterItemQtyLatexWet;
        TextView txtBpnReportHarvesterItemQtyLatexDRC;
        TextView txtBpnReportHarvesterItemQtyLatexDry;
        TextView txtBpnReportHarvesterItemQtyLumpWet;
        TextView txtBpnReportHarvesterItemQtyLumpDRC;
        TextView txtBpnReportHarvesterItemQtyLumpDry;
        TextView txtBpnReportHarvesterItemQtySlabWet;
        TextView txtBpnReportHarvesterItemQtySlabDRC;
        TextView txtBpnReportHarvesterItemQtySlabDry;
        TextView txtBpnReportHarvesterItemQtyTreelaceWet;
        TextView txtBpnReportHarvesterItemQtyTreelaceDRC;
        TextView txtBpnReportHarvesterItemQtyTreelaceDry;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
            txtBpnReportHarvesterItemHarvesterName = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemHarvesterName);

            txtBpnReportHarvesterItemQtyLatexWet = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyLatexWet);
            txtBpnReportHarvesterItemQtyLatexDRC = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyLatexDRC);
            txtBpnReportHarvesterItemQtyLatexDry = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyLatexDry);
            txtBpnReportHarvesterItemQtyLumpWet = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyLumpWet);
            txtBpnReportHarvesterItemQtyLumpDRC = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyLumpDRC);
            txtBpnReportHarvesterItemQtyLumpDry = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyLumpDry);
            txtBpnReportHarvesterItemQtySlabWet = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtySlabWet);
            txtBpnReportHarvesterItemQtySlabDRC = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtySlabDRC);
            txtBpnReportHarvesterItemQtySlabDry = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtySlabDry);
            txtBpnReportHarvesterItemQtyTreelaceWet = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyTreelaceWet);
            txtBpnReportHarvesterItemQtyTreelaceDRC = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyTreelaceDRC);
            txtBpnReportHarvesterItemQtyTreelaceDry = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyTreelaceDry);

            view = new ViewBPNKaretReportHarvesterItem(txtBpnReportHarvesterItemHarvesterName,
                    txtBpnReportHarvesterItemQtyLatexWet, txtBpnReportHarvesterItemQtyLatexDRC, txtBpnReportHarvesterItemQtyLatexDry,
                    txtBpnReportHarvesterItemQtyLumpWet, txtBpnReportHarvesterItemQtyLumpDRC, txtBpnReportHarvesterItemQtyLumpDry,
                    txtBpnReportHarvesterItemQtySlabWet, txtBpnReportHarvesterItemQtySlabDRC, txtBpnReportHarvesterItemQtySlabDry,
                    txtBpnReportHarvesterItemQtyTreelaceWet, txtBpnReportHarvesterItemQtyTreelaceDRC, txtBpnReportHarvesterItemQtyTreelaceDry);

            convertView.setTag(view);
        }else{
            view = (ViewBPNKaretReportHarvesterItem) convertView.getTag();

            txtBpnReportHarvesterItemHarvesterName = view.getTxtBpnReportHarvesterItemHarvesterName();
            txtBpnReportHarvesterItemQtyLatexWet = view.getTxtBpnReportHarvesterItemQtyLatexWet();
            txtBpnReportHarvesterItemQtyLatexDRC = view.getTxtBpnReportHarvesterItemQtyLatexDRC();
            txtBpnReportHarvesterItemQtyLatexDry = view.getTxtBpnReportHarvesterItemQtyLatexDry();
            txtBpnReportHarvesterItemQtyLumpWet = view.getTxtBpnReportHarvesterItemQtyLumpWet();
            txtBpnReportHarvesterItemQtyLumpDRC = view.getTxtBpnReportHarvesterItemQtyLumpDRC();
            txtBpnReportHarvesterItemQtyLumpDry = view.getTxtBpnReportHarvesterItemQtyLumpDry();
            txtBpnReportHarvesterItemQtySlabWet = view.getTxtBpnReportHarvesterItemQtySlabWet();
            txtBpnReportHarvesterItemQtySlabDRC = view.getTxtBpnReportHarvesterItemQtySlabDRC();
            txtBpnReportHarvesterItemQtySlabDry = view.getTxtBpnReportHarvesterItemQtySlabDry();
            txtBpnReportHarvesterItemQtyTreelaceWet = view.getTxtBpnReportHarvesterItemQtyTreelaceWet();
            txtBpnReportHarvesterItemQtyTreelaceDRC = view.getTxtBpnReportHarvesterItemQtyTreelaceDRC();
            txtBpnReportHarvesterItemQtyTreelaceDry = view.getTxtBpnReportHarvesterItemQtyTreelaceDry();
        }

        txtBpnReportHarvesterItemHarvesterName.setText(item.getName());
        txtBpnReportHarvesterItemQtyLatexWet.setText(String.valueOf(Utils.round(item.getQtyLatexWet(), 2)));
        txtBpnReportHarvesterItemQtyLatexDRC.setText(String.valueOf(Utils.round(item.getQtyLatexDRC(), 2)));
        txtBpnReportHarvesterItemQtyLatexDry.setText(String.valueOf(Utils.round(item.getQtyLatexDry(), 2)));
        txtBpnReportHarvesterItemQtyLumpWet.setText(String.valueOf(Utils.round(item.getQtyLumpWet(), 2)));
        txtBpnReportHarvesterItemQtyLumpDRC.setText(String.valueOf(Utils.round(item.getQtyLumpDRC(), 2)));
        txtBpnReportHarvesterItemQtyLumpDry.setText(String.valueOf(Utils.round(item.getQtyLumpDry(), 2)));
        txtBpnReportHarvesterItemQtySlabWet.setText(String.valueOf(Utils.round(item.getQtySlabWet(), 2)));
        txtBpnReportHarvesterItemQtySlabDRC.setText(String.valueOf(Utils.round(item.getQtySlabDRC(), 2)));
        txtBpnReportHarvesterItemQtySlabDry.setText(String.valueOf(Utils.round(item.getQtySlabDry(), 2)));
        txtBpnReportHarvesterItemQtyTreelaceWet.setText(String.valueOf(Utils.round(item.getQtyTreelaceWet(), 2)));
        txtBpnReportHarvesterItemQtyTreelaceDRC.setText(String.valueOf(Utils.round(item.getQtyTreelaceDRC(), 2)));
        txtBpnReportHarvesterItemQtyTreelaceDry.setText(String.valueOf(Utils.round(item.getQtyTreelaceDry(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNKaretReportHarvester>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNKaretReportHarvester> lstResult = new ArrayList<BPNKaretReportHarvester>();
                BPNKaretReportHarvester item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNKaretReportHarvester>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
