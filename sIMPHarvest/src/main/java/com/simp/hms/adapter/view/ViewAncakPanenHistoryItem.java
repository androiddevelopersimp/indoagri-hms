package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewAncakPanenHistoryItem {
	private TextView txt_ancak_panen_history_item_date;
	private TextView txt_ancak_panen_history_item_clerk;
	private TextView txt_ancak_panen_history_item_foreman;
	private TextView txt_ancak_panen_history_item_block;
	private TextView txt_ancak_panen_history_item_harvester;
	private TextView txt_ancak_panen_history_item_tph;
	
	public ViewAncakPanenHistoryItem(TextView txt_ancak_panen_history_item_date,
			TextView txt_ancak_panen_history_item_clerk,
			TextView txt_ancak_panen_history_item_foreman,
			TextView txt_ancak_panen_history_item_block,
			TextView txt_ancak_panen_history_item_harvester, TextView txt_ancak_panen_history_item_tph) {

		this.txt_ancak_panen_history_item_date = txt_ancak_panen_history_item_date;
		this.txt_ancak_panen_history_item_clerk = txt_ancak_panen_history_item_clerk;
		this.txt_ancak_panen_history_item_foreman = txt_ancak_panen_history_item_foreman;
		this.txt_ancak_panen_history_item_block = txt_ancak_panen_history_item_block;
		this.txt_ancak_panen_history_item_harvester = txt_ancak_panen_history_item_harvester;
		this.txt_ancak_panen_history_item_tph = txt_ancak_panen_history_item_tph;
	}

	public TextView getTxtAncakPanenHistoryItemDate() {
		return txt_ancak_panen_history_item_date;
	}

	public void setTxtAncakPanenHistoryItemDate(
			TextView txt_ancak_panen_history_item_date) {
		this.txt_ancak_panen_history_item_date = txt_ancak_panen_history_item_date;
	}

	public TextView getTxtAncakPanenHistoryItemClerk() {
		return txt_ancak_panen_history_item_clerk;
	}

	public void setTxtAncakPanenHistoryItemClerk(
			TextView txt_ancak_panen_history_item_clerk) {
		this.txt_ancak_panen_history_item_clerk = txt_ancak_panen_history_item_clerk;
	}

	public TextView getTxtAncakPanenHistoryItemForeman() {
		return txt_ancak_panen_history_item_foreman;
	}

	public void setTxtAncakPanenHistoryItemForeman(
			TextView txt_ancak_panen_history_item_foreman) {
		this.txt_ancak_panen_history_item_foreman = txt_ancak_panen_history_item_foreman;
	}

	public TextView getTxtAncakPanenHistoryItemBlock() {
		return txt_ancak_panen_history_item_block;
	}

	public void setTxtAncakPanenHistoryItemBlock(
			TextView txt_ancak_panen_history_item_block) {
		this.txt_ancak_panen_history_item_block = txt_ancak_panen_history_item_block;
	}

	public TextView getTxtAncakPanenHistoryItemHarvester() {
		return txt_ancak_panen_history_item_harvester;
	}

	public void setTxtAncakPanenHistoryItemHarvester(
			TextView txt_ancak_panen_history_item_harvester) {
		this.txt_ancak_panen_history_item_harvester = txt_ancak_panen_history_item_harvester;
	}
	
	public TextView getTxtAncakPanenHistoryItemTph() {
		return txt_ancak_panen_history_item_tph;
	}

	public void setTxtAncakPanenHistoryItemTph(
			TextView txt_ancak_panen_history_item_tph) {
		this.txt_ancak_panen_history_item_tph = txt_ancak_panen_history_item_tph;
	}
	
	
}
