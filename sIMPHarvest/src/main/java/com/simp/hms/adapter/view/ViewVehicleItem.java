package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewVehicleItem {
	private TextView txtVehicleCompanyCode;
	private TextView txtVehicleEstate;
	private TextView txtVehicleRunningAccount;
	private TextView txtVehicleLicensePlate;
	private TextView txtVehicleVendor;

	public ViewVehicleItem(TextView txtVehicleCompanyCode, TextView txtVehicleEstate, TextView txtVehicleRunningAccount,
			TextView txtVehicleLicensePlate, TextView txtVehicleVendor) {
		super();
		this.txtVehicleCompanyCode = txtVehicleCompanyCode;
		this.txtVehicleEstate = txtVehicleEstate;
		this.txtVehicleRunningAccount = txtVehicleRunningAccount;
		this.txtVehicleLicensePlate = txtVehicleLicensePlate;
		this.txtVehicleVendor = txtVehicleVendor;
	}

	public TextView getTxtVehicleCompanyCode() {
		return txtVehicleCompanyCode;
	}

	public void setTxtVehicleCompanyCode(TextView txtVehicleCompanyCode) {
		this.txtVehicleCompanyCode = txtVehicleCompanyCode;
	}

	public TextView getTxtVehicleEstate() {
		return txtVehicleEstate;
	}

	public void setTxtVehicleEstate(TextView txtVehicleEstate) {
		this.txtVehicleEstate = txtVehicleEstate;
	}

	public TextView getTxtVehicleRunningAccount() {
		return txtVehicleRunningAccount;
	}

	public void setTxtVehicleRunningAccount(TextView txtVehicleRunningAccount) {
		this.txtVehicleRunningAccount = txtVehicleRunningAccount;
	}

	public TextView getTxtVehicleLicensePlate() {
		return txtVehicleLicensePlate;
	}

	public void setTxtVehicleLIcensePlate(TextView txtVehicleLicensePlate) {
		this.txtVehicleLicensePlate = txtVehicleLicensePlate;
	}

	public TextView getTxtVehicleVendor() {
		return txtVehicleVendor;
	}

	public void setTxtVehicleVendor(TextView txtVehicleVendor) {
		this.txtVehicleVendor = txtVehicleVendor;
	}
}
