package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPUReportTphItem {
    private TextView txtSpuReportTphItemTph;
    private TextView txtSpuReportTphItemQtyGoodQty;
    private TextView txtSpuReportTphItemQtyGoodWeight;
    private TextView txtSpuReportTphItemQtyBadQty;
    private TextView txtSpuReportTphItemQtyBadWeight;
    private TextView txtSpuReportTphItemQtyPoorQty;
    private TextView txtSpuReportTphItemQtyPoorWeight;

    public ViewSPUReportTphItem(TextView txtSpuReportTphItemTph,
                                  TextView txtSpuReportTphItemQtyGoodQty,
                                  TextView txtSpuReportTphItemQtyGoodWeight,
                                  TextView txtSpuReportTphItemQtyBadQty,
                                  TextView txtSpuReportTphItemQtyBadWeight,
                                  TextView txtSpuReportTphItemQtyPoorQty,
                                  TextView txtSpuReportTphItemQtyPoorWeight) {
        super();
        this.txtSpuReportTphItemTph = txtSpuReportTphItemTph;
        this.txtSpuReportTphItemQtyGoodQty = txtSpuReportTphItemQtyGoodQty;
        this.txtSpuReportTphItemQtyGoodWeight = txtSpuReportTphItemQtyGoodWeight;
        this.txtSpuReportTphItemQtyBadQty = txtSpuReportTphItemQtyBadQty;
        this.txtSpuReportTphItemQtyBadWeight = txtSpuReportTphItemQtyBadWeight;
        this.txtSpuReportTphItemQtyPoorQty = txtSpuReportTphItemQtyPoorQty;
        this.txtSpuReportTphItemQtyPoorWeight = txtSpuReportTphItemQtyPoorWeight;
    }

    public TextView getTxtSpuReportTphItemTPh() {
        return txtSpuReportTphItemTph;
    }

    public void setTxtSpuReportTphItemTPh(TextView txtSpuReportTphItemTPh) {
        this.txtSpuReportTphItemTph = txtSpuReportTphItemTPh;
    }

    public TextView getTxtSpuReportTphItemQtyGoodQty() {
        return txtSpuReportTphItemQtyGoodQty;
    }

    public void setTxtSpuReportTphItemQtyGoodQty(
            TextView txtSpuReportTphItemQtyGoodQty) {
        this.txtSpuReportTphItemQtyGoodQty = txtSpuReportTphItemQtyGoodQty;
    }

    public TextView getTxtSpuReportTphItemQtyGoodWeight() {
        return txtSpuReportTphItemQtyGoodWeight;
    }

    public void setTxtSpuReportTphItemQtyGoodWeight(
            TextView txtSpuReportTphItemQtyGoodWeight) {
        this.txtSpuReportTphItemQtyGoodWeight = txtSpuReportTphItemQtyGoodWeight;
    }

    public TextView getTxtSpuReportTphItemQtyBadQty() {
        return txtSpuReportTphItemQtyBadQty;
    }

    public void setTxtSpuReportTphItemQtyBadQty(
            TextView txtSpuReportTphItemQtyBadQty) {
        this.txtSpuReportTphItemQtyBadQty = txtSpuReportTphItemQtyBadQty;
    }

    public TextView getTxtSpuReportTphItemQtyBadWeight() {
        return txtSpuReportTphItemQtyBadWeight;
    }

    public void setTxtSpuReportTphItemQtyBadWeight(
            TextView txtSpuReportTphItemQtyBadWeight) {
        this.txtSpuReportTphItemQtyBadWeight = txtSpuReportTphItemQtyBadWeight;
    }

    public TextView getTxtSpuReportTphItemQtyPoorQty() {
        return txtSpuReportTphItemQtyPoorQty;
    }

    public void setTxtSpuReportTphItemQtyPoorQty(
            TextView txtSpuReportTphItemQtyPoorQty) {
        this.txtSpuReportTphItemQtyPoorQty = txtSpuReportTphItemQtyPoorQty;
    }

    public TextView getTxtSpuReportTphItemQtyPoorWeight() {
        return txtSpuReportTphItemQtyPoorWeight;
    }

    public void setTxtSpuReportTphItemQtyPoorWeight(
            TextView txtSpuReportTphItemQtyPoorWeight) {
        this.txtSpuReportTphItemQtyPoorWeight = txtSpuReportTphItemQtyPoorWeight;
    }
}
