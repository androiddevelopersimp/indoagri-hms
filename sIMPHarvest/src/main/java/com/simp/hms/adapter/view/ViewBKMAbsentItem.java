package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ViewBKMAbsentItem {
	private TextView txtBkmAbsentItemName;
	private TextView txtBkmAbsentItemAbsentType;
	private Button btnBKMAbsentItemMandaysMin;
	private EditText edtBkmAbsentItemMandays;
	private Button btnBKMAbsentItemMandaysPlus;
	private Button btnBkmAbsentItemDelete;
	private boolean isVisible;
	
	public ViewBKMAbsentItem(TextView txtBkmAbsentItemName,
			TextView txtBkmAbsentItemAbsentType,
			Button btnsBKMAbsentItemMandaysMin,
			EditText edtBkmAbsentItemMandays, 
			Button btnBKMAbsentItemMandaysPlus,
			Button btnBkmAbsentItemDelete, boolean isVisible) {
		this.txtBkmAbsentItemName = txtBkmAbsentItemName;
		this.txtBkmAbsentItemAbsentType = txtBkmAbsentItemAbsentType;
		this.btnBKMAbsentItemMandaysMin = btnsBKMAbsentItemMandaysMin;
		this.edtBkmAbsentItemMandays = edtBkmAbsentItemMandays;
		this.btnBKMAbsentItemMandaysPlus = btnBKMAbsentItemMandaysPlus;
		this.btnBkmAbsentItemDelete = btnBkmAbsentItemDelete;
		this.isVisible = isVisible;
	}

	public TextView getTxtBkmAbsentItemName() {
		return txtBkmAbsentItemName;
	}

	public void setTxtBkmAbsentItemName(TextView txtBkmAbsentItemName) {
		this.txtBkmAbsentItemName = txtBkmAbsentItemName;
	}

	public TextView getTxtBkmAbsentItemAbsentType() {
		return txtBkmAbsentItemAbsentType;
	}

	public void setTxtBkmAbsentItemAbsentType(TextView txtBkmAbsentItemAbsentType) {
		this.txtBkmAbsentItemAbsentType = txtBkmAbsentItemAbsentType;
	}

	
	public Button getBtnBKMAbsentItemMandaysMin() {
		return btnBKMAbsentItemMandaysMin;
	}    

	public void setBtnBKMAbsentItemMandaysMin(Button btnBKMAbsentItemMandaysMin) {
		this.btnBKMAbsentItemMandaysMin = btnBKMAbsentItemMandaysMin;
	}  

	public EditText getEdtBkmAbsentItemMandays() {
		return edtBkmAbsentItemMandays;
	}

	public void setEdtBkmAbsentItemMandays(EditText edtBkmAbsentItemMandays) {
		this.edtBkmAbsentItemMandays = edtBkmAbsentItemMandays;
	}
	
	public Button getBtnBKMAbsentItemMandaysPlus() {
		return btnBKMAbsentItemMandaysPlus;
	}

	public void setBtnBKMAbsentItemMandaysPlus(Button btnBKMAbsentItemMandaysPlus) {
		this.btnBKMAbsentItemMandaysPlus = btnBKMAbsentItemMandaysPlus;
	}

	public Button getBtnBkmAbsentItemDelete() {
		return btnBkmAbsentItemDelete;
	}

	public void setBtnBkmAbsentItemDelete(Button btnBkmAbsentItemDelete) {
		this.btnBkmAbsentItemDelete = btnBkmAbsentItemDelete;
	}
	

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
}
