package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewEmployeeItem;
import com.simp.hms.model.Employee;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterEmployee extends BaseAdapter{
	private Context context;
	private List<Employee> lstEmployee, lstTemp;
	private int layout;
	
	public AdapterEmployee(Context context, List<Employee> lstEmployee, int layout){
		this.context = context;
		this.lstEmployee = lstEmployee;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstEmployee.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstEmployee.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		Employee employee = lstEmployee.get(pos);
		
		ViewEmployeeItem view;
		TextView txt_employee_item_id;
		TextView txt_employee_item_company_code;
		TextView txt_employee_item_estate;
		TextView txt_employee_item_fiscal_year;
		TextView txt_employee_item_fiscal_period;
		TextView txt_employee_item_nik;
		TextView txt_employee_item_name;
		TextView txt_employee_item_term_date;
		TextView txt_employee_item_divisi;
		TextView txt_employee_item_role_id;
		TextView txt_employee_item_job_pos;
		TextView txt_employee_item_gang_code;
		TextView txt_employee_item_cost_center;
		TextView txt_employee_item_emp_type;
		TextView txt_employee_item_valid_from;
		TextView txt_employee_item_harvester_code;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txt_employee_item_id = (TextView) convertView.findViewById(R.id.txt_employee_item_id);
            txt_employee_item_company_code = (TextView) convertView.findViewById(R.id.txt_employee_item_company_code);
            txt_employee_item_estate = (TextView) convertView.findViewById(R.id.txt_employee_item_estate);
            txt_employee_item_fiscal_year = (TextView) convertView.findViewById(R.id.txt_employee_item_fiscal_year);
            txt_employee_item_fiscal_period = (TextView) convertView.findViewById(R.id.txt_employee_item_fiscal_period);
            txt_employee_item_nik = (TextView) convertView.findViewById(R.id.txt_employee_item_nik);
            txt_employee_item_name = (TextView) convertView.findViewById(R.id.txt_employee_item_name);
            txt_employee_item_term_date = (TextView) convertView.findViewById(R.id.txt_employee_item_term_date);
            txt_employee_item_divisi = (TextView) convertView.findViewById(R.id.txt_employee_item_divisi);
            txt_employee_item_role_id = (TextView) convertView.findViewById(R.id.txt_employee_item_role_id);
            txt_employee_item_job_pos = (TextView) convertView.findViewById(R.id.txt_employee_item_job_pos);
            txt_employee_item_gang_code = (TextView) convertView.findViewById(R.id.txt_employee_item_gang_code);
            txt_employee_item_cost_center = (TextView) convertView.findViewById(R.id.txt_employee_item_cost_center);
            txt_employee_item_emp_type = (TextView) convertView.findViewById(R.id.txt_employee_item_emp_type);
            txt_employee_item_valid_from = (TextView) convertView.findViewById(R.id.txt_employee_item_valid_from);
            txt_employee_item_harvester_code = (TextView) convertView.findViewById(R.id.txt_employee_item_harvester_code);
            
            view = new ViewEmployeeItem(txt_employee_item_id, txt_employee_item_company_code, txt_employee_item_estate, 
            		txt_employee_item_fiscal_year, txt_employee_item_fiscal_period, txt_employee_item_nik, txt_employee_item_name, 
            		txt_employee_item_term_date, txt_employee_item_divisi, txt_employee_item_role_id, txt_employee_item_job_pos, 
            		txt_employee_item_gang_code, txt_employee_item_cost_center, txt_employee_item_emp_type, txt_employee_item_valid_from,
            		txt_employee_item_harvester_code);     
            
            convertView.setTag(view);
        }else{
        	view = (ViewEmployeeItem) convertView.getTag();
        	
        	txt_employee_item_id = view.getTxtEmployeeItemId();
        	txt_employee_item_company_code = view.getTxtEmployeeItemCompanyCode();
        	txt_employee_item_estate = view.getTxtEmployeeItemEstate();
        	txt_employee_item_fiscal_year = view.getTxtEmployeeItemFiscalYear();
        	txt_employee_item_fiscal_period = view.getTxtEmployeeItemFiscalPeriod();
        	txt_employee_item_nik = view.getTxtEmployeeItemNik();
        	txt_employee_item_name = view.getTxtEmployeeItemName();
        	txt_employee_item_term_date = view.getTxtEmployeeItemTermDate();
        	txt_employee_item_divisi = view.getTxtEmployeeItemDivisi();
        	txt_employee_item_role_id = view.getTxtEmployeeItemRoleId();
        	txt_employee_item_job_pos = view.getTxtEmployeeItemJobPos();
        	txt_employee_item_gang_code = view.getTxtEmployeeItemGangCode();
        	txt_employee_item_cost_center = view.getTxtEmployeeItemCostCenter();
        	txt_employee_item_emp_type = view.getTxtEmployeeItemEmType();
        	txt_employee_item_valid_from = view.getTxtEmployeeItemValidFrom();
        	txt_employee_item_harvester_code = view.getTxtEmployeeItemHarvesterCode();
        }

        txt_employee_item_id.setText(String.valueOf(employee.getRowId()));
        txt_employee_item_company_code.setText(employee.getCompanyCode());
        txt_employee_item_estate.setText(employee.getEstate());
        txt_employee_item_fiscal_year.setText(String.valueOf(employee.getFiscalYear()));
        txt_employee_item_fiscal_period.setText(String.valueOf(employee.getFiscalPeriod()));
        txt_employee_item_nik.setText(employee.getNik());
        txt_employee_item_name.setText(employee.getName());
        txt_employee_item_term_date.setText(employee.getTermDate());
        txt_employee_item_divisi.setText(employee.getDivision());
        txt_employee_item_role_id.setText(employee.getRoleId());
        txt_employee_item_job_pos.setText(employee.getJobPos());
        txt_employee_item_gang_code.setText(employee.getGang());
        txt_employee_item_cost_center.setText(employee.getCostCenter());
        txt_employee_item_emp_type.setText(employee.getEmpType());
        txt_employee_item_valid_from.setText(employee.getValidFrom());
        txt_employee_item_harvester_code.setText(TextUtils.isEmpty(employee.getHarvesterCode()) ? "-" : employee.getHarvesterCode());

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstEmployee = null;
				lstEmployee = (List<Employee>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<Employee> list_result = new ArrayList<Employee>();
				Employee employee;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<Employee>(lstEmployee);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						employee = lstTemp.get(i);
						
						if((employee.getNik().toLowerCase(Locale.getDefault()).contains(constraint)) || 
								(employee.getName().toLowerCase(Locale.getDefault()).contains(constraint)) ||
								(employee.getHarvesterCode().toLowerCase(Locale.getDefault()).contains(constraint))){
							list_result.add(employee);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
	
	public void addData(Employee employee){
		boolean found = false;
		
		for(int i = 0; i < lstEmployee.size(); i++){
			if(employee.getNik().equalsIgnoreCase(lstEmployee.get(i).getNik())){
				found = true;
				break;
			}
		}
		
		if(!found){
			lstEmployee.add(employee);
			notifyDataSetChanged();
		}
	}
}
