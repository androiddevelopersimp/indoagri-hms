package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPUReportBlockItem {
    private TextView txtSpuReportBlockItemBlock;
    private TextView txtSpuReportBlockItemQtyGoodQty;
    private TextView txtSpuReportBlockItemQtyGoodWeight;
    private TextView txtSpuReportBlockItemQtyBadQty;
    private TextView txtSpuReportBlockItemQtyBadWeight;
    private TextView txtSpuReportBlockItemQtyPoorQty;
    private TextView txtSpuReportBlockItemQtyPoorWeight;

    public ViewSPUReportBlockItem(TextView txtSpuReportBlockItemBlock,
                                  TextView txtSpuReportBlockItemQtyGoodQty,
                                  TextView txtSpuReportBlockItemQtyGoodWeight,
                                  TextView txtSpuReportBlockItemQtyBadQty,
                                  TextView txtSpuReportBlockItemQtyBadWeight,
                                  TextView txtSpuReportBlockItemQtyPoorQty,
                                  TextView txtSpuReportBlockItemQtyPoorWeight) {
        super();
        this.txtSpuReportBlockItemBlock = txtSpuReportBlockItemBlock;
        this.txtSpuReportBlockItemQtyGoodQty = txtSpuReportBlockItemQtyGoodQty;
        this.txtSpuReportBlockItemQtyGoodWeight = txtSpuReportBlockItemQtyGoodWeight;
        this.txtSpuReportBlockItemQtyBadQty = txtSpuReportBlockItemQtyBadQty;
        this.txtSpuReportBlockItemQtyBadWeight = txtSpuReportBlockItemQtyBadWeight;
        this.txtSpuReportBlockItemQtyPoorQty = txtSpuReportBlockItemQtyPoorQty;
        this.txtSpuReportBlockItemQtyPoorWeight = txtSpuReportBlockItemQtyPoorWeight;
    }

    public TextView getTxtSpuReportBlockItemBlock() {
        return txtSpuReportBlockItemBlock;
    }

    public void setTxtSpuReportBlockItemBlock(
            TextView txtSpuReportBlockItemBlock) {
        this.txtSpuReportBlockItemBlock = txtSpuReportBlockItemBlock;
    }

    public TextView getTxtSpuReportBlockItemQtyGoodQty() {
        return txtSpuReportBlockItemQtyGoodQty;
    }

    public void setTxtSpuReportBlockItemQtyGoodQty(
            TextView txtSpuReportBlockItemQtyGoodQty) {
        this.txtSpuReportBlockItemQtyGoodQty = txtSpuReportBlockItemQtyGoodQty;
    }

    public TextView getTxtSpuReportBlockItemQtyGoodWeight() {
        return txtSpuReportBlockItemQtyGoodWeight;
    }

    public void setTxtSpuReportBlockItemQtyGoodWeight(
            TextView txtSpuReportBlockItemQtyGoodWeight) {
        this.txtSpuReportBlockItemQtyGoodWeight = txtSpuReportBlockItemQtyGoodWeight;
    }

    public TextView getTxtSpuReportBlockItemQtyBadQty() {
        return txtSpuReportBlockItemQtyBadQty;
    }

    public void setTxtSpuReportBlockItemQtyBadQty(
            TextView txtSpuReportBlockItemQtyBadQty) {
        this.txtSpuReportBlockItemQtyBadQty = txtSpuReportBlockItemQtyBadQty;
    }

    public TextView getTxtSpuReportBlockItemQtyBadWeight() {
        return txtSpuReportBlockItemQtyBadWeight;
    }

    public void setTxtSpuReportBlockItemQtyBadWeight(
            TextView txtSpuReportBlockItemQtyBadWeight) {
        this.txtSpuReportBlockItemQtyBadWeight = txtSpuReportBlockItemQtyBadWeight;
    }

    public TextView getTxtSpuReportBlockItemQtyPoorQty() {
        return txtSpuReportBlockItemQtyPoorQty;
    }

    public void setTxtSpuReportBlockItemQtyPoorQty(
            TextView txtSpuReportBlockItemQtyPoorQty) {
        this.txtSpuReportBlockItemQtyPoorQty = txtSpuReportBlockItemQtyPoorQty;
    }

    public TextView getTxtSpuReportBlockItemQtyPoorWeight() {
        return txtSpuReportBlockItemQtyPoorWeight;
    }

    public void setTxtSpuReportBlockItemQtyPoorWeight(
            TextView txtSpuReportBlockItemQtyPoorWeight) {
        this.txtSpuReportBlockItemQtyPoorWeight = txtSpuReportBlockItemQtyPoorWeight;
    }
}
