package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSKBItem {
	private TextView txt_skb_item_id;
	private TextView txt_skb_item_company_code;
	private TextView txt_skb_item_estate;
	private TextView txt_skb_item_block;
	private TextView txt_skb_item_baris_skb;
	private TextView txt_skb_item_valid_from;
	private TextView txt_skb_item_valid_to;
	private TextView txt_skb_item_baris_blok;
	private TextView txt_skb_item_jumlah_pokok;
	private TextView txt_skb_item_pokok_mati;
	private TextView txt_skb_item_tanggal_tanam;
	private TextView txt_skb_item_line_skb;
	private TextView txt_skb_item_mandt;
	
	public ViewSKBItem(TextView txt_skb_item_id, TextView txt_skb_item_company_code, TextView txt_skb_item_estate, TextView txt_skb_item_block, 
					   TextView txt_skb_item_baris_skb, TextView txt_skb_item_valid_from, TextView txt_skb_item_valid_to, TextView txt_skb_item_baris_blok,
					   TextView txt_skb_item_jumlah_pokok, TextView txt_skb_item_pokok_mati, TextView txt_skb_item_tanggal_tanam, TextView txt_skb_item_line_skb,
					   TextView txt_skb_item_mandt){
		this.txt_skb_item_id = txt_skb_item_id;
		this.txt_skb_item_company_code = txt_skb_item_company_code;
		this.txt_skb_item_estate = txt_skb_item_estate;
		this.txt_skb_item_block = txt_skb_item_block;
		this.txt_skb_item_baris_skb = txt_skb_item_baris_skb;
		this.txt_skb_item_valid_from = txt_skb_item_valid_from;
		this.txt_skb_item_valid_to = txt_skb_item_valid_to;   
		this.txt_skb_item_baris_blok = txt_skb_item_baris_blok;
		this.txt_skb_item_jumlah_pokok = txt_skb_item_jumlah_pokok;
		this.txt_skb_item_pokok_mati = txt_skb_item_pokok_mati;
		this.txt_skb_item_tanggal_tanam = txt_skb_item_tanggal_tanam;
		this.txt_skb_item_line_skb = txt_skb_item_line_skb;
		this.txt_skb_item_mandt = txt_skb_item_mandt;
	}

	public TextView getTxtSkbItemId() {
		return txt_skb_item_id;
	}

	public void setTxtSkbItemId(TextView txt_skb_item_id) {
		this.txt_skb_item_id = txt_skb_item_id;
	}

	public TextView getTxtSkbItemCompanyCode() {
		return txt_skb_item_company_code;
	}

	public void setTxtSkbItemCompanyCode(TextView txt_skb_item_company_code) {
		this.txt_skb_item_company_code = txt_skb_item_company_code;
	}

	public TextView getTxtSkbItemEstate() {
		return txt_skb_item_estate;
	}

	public void setTxtSkbItemEstate(TextView txt_skb_item_estate) {
		this.txt_skb_item_estate = txt_skb_item_estate;
	}

	public TextView getTxtSkbItemBlock() {
		return txt_skb_item_block;
	}

	public void setTxtSkbItemBlock(TextView txt_skb_item_block) {
		this.txt_skb_item_block = txt_skb_item_block;
	}

	public TextView getTxtSkbItemBarisSkb() {
		return txt_skb_item_baris_skb;
	}

	public void setTxtSkbItemBarisSkb(TextView txt_skb_item_baris_skb) {
		this.txt_skb_item_baris_skb = txt_skb_item_baris_skb;
	}

	public TextView getTxtSkbItemValidFrom() {
		return txt_skb_item_valid_from;
	}

	public void setTxtSkbItemValidFrom(TextView txt_skb_item_valid_from) {
		this.txt_skb_item_valid_from = txt_skb_item_valid_from;
	}

	public TextView getTxtSkbItemValidTo() {
		return txt_skb_item_valid_to;
	}

	public void setTxtSkbItemValidTo(TextView txt_skb_item_valid_to) {
		this.txt_skb_item_valid_to = txt_skb_item_valid_to;
	}

	public TextView getTxtSkbItemBarisBlok() {
		return txt_skb_item_baris_blok;
	}

	public void setTxtSkbItemBarisBlok(TextView txt_skb_item_baris_blok) {
		this.txt_skb_item_baris_blok = txt_skb_item_baris_blok;
	}

	public TextView getTxtSkbItemJumlahPokok() {
		return txt_skb_item_jumlah_pokok;
	}

	public void setTxtSkbItemJumlahPokok(TextView txt_skb_item_jumlah_pokok) {
		this.txt_skb_item_jumlah_pokok = txt_skb_item_jumlah_pokok;
	}

	public TextView getTxtSkbItemPokokMati() {
		return txt_skb_item_pokok_mati;
	}

	public void setTxtSkbItemPokokMati(TextView txt_skb_item_pokok_mati) {
		this.txt_skb_item_pokok_mati = txt_skb_item_pokok_mati;
	}

	public TextView getTxtSkbItemTanggalTanam() {
		return txt_skb_item_tanggal_tanam;
	}

	public void setTxtSkbItemTanggalTanam(TextView txt_skb_item_tanggal_tanam) {
		this.txt_skb_item_tanggal_tanam = txt_skb_item_tanggal_tanam;
	}

	public TextView getTxtSkbItemLineSkb() {
		return txt_skb_item_line_skb;
	}

	public void setTxtSkbItemLineSkb(TextView txt_skb_item_line_skb) {
		this.txt_skb_item_line_skb = txt_skb_item_line_skb;
	}

	public TextView getTxtSkbItemMandt() {
		return txt_skb_item_mandt;
	}

	public void setTxtSkbItemMandt(TextView txt_skb_item_mandt) {
		this.txt_skb_item_mandt = txt_skb_item_mandt;
	}
}
