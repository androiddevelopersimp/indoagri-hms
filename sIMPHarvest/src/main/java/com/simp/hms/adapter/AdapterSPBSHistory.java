package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSpbsHistoryItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.model.BPNHeader;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPBSHeader;
import com.simp.hms.model.SPBSLine;

import android.app.Activity;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSPBSHistory extends BaseAdapter{
	private Activity activity;
	private List<SPBSHeader> lstSpbs, lstTemp;
	private int layout;
	
	
	public AdapterSPBSHistory(Activity activity, List<SPBSHeader> listItems, int layout){
		this.activity = activity;
		this.lstSpbs = listItems;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSpbs.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSpbs.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		SPBSHeader item = lstSpbs.get(pos);
		
		ViewSpbsHistoryItem view;
		TextView txtSpbsHistoryItemSpbsNumber;
		TextView txtSpbsHistoryItemSpbsDate;
		TextView txtSpbsHistoryItemClerk;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtSpbsHistoryItemSpbsNumber = (TextView) convertView.findViewById(R.id.txtSpbsHistoryItemSpbsNumber);
            txtSpbsHistoryItemSpbsDate = (TextView) convertView.findViewById(R.id.txtSpbsHistoryItemSpbsDate);
            txtSpbsHistoryItemClerk = (TextView) convertView.findViewById(R.id.txtSpbsHistoryItemClerk);
            
            view = new ViewSpbsHistoryItem(txtSpbsHistoryItemSpbsNumber, txtSpbsHistoryItemSpbsDate, txtSpbsHistoryItemClerk);
            
            convertView.setTag(view);
        }else{
        	view = (ViewSpbsHistoryItem) convertView.getTag();
        	
        	txtSpbsHistoryItemSpbsNumber = view.getTxtSpbsHistoryItemSpbsNumber();
        	txtSpbsHistoryItemSpbsDate = view.getTxtSpbsHistoryItemSpbsDate();
        	txtSpbsHistoryItemClerk = view.getTxtSpbsHistoryItemClerk();
        }
        
        String remark = "";
        if(item.getCrop().equalsIgnoreCase("01"))
         remark = item.getDestType().equalsIgnoreCase("MILL")? "" : " - Langsir";

        txtSpbsHistoryItemSpbsNumber.setText(item.getSpbsNumber() + remark);
        txtSpbsHistoryItemSpbsDate.setText(new DateLocal(item.getSpbsDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtSpbsHistoryItemClerk.setText(item.getClerk());

        return convertView;
	}
	
	public void deleteData(int pos){
		SPBSHeader item = lstSpbs.get(pos);
		
		String imei = item.getImei();
		String year = item.getYear();
		String companyCode = item.getCompanyCode();
		String estate = item.getEstate();
		String crop = item.getCrop();
		String spbsNumber = item.getSpbsNumber();
		int status = item.getStatus();
		
		if(status == 0){
			DatabaseHandler database = new DatabaseHandler(activity);
			
			try{
				database.openTransaction();
				
				List<Object> lstSpbsLine = database.getListData(false, SPBSLine.TABLE_NAME, null, 
						SPBSLine.XML_IMEI + "=?" + " and " +
						SPBSLine.XML_YEAR + "=?" + " and " +
						SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
						SPBSLine.XML_ESTATE + "=?" + " and " +
						SPBSLine.XML_CROP + "=?" + " and " +
						SPBSLine.XML_SPBS_NUMBER + "=?", 
						new String [] {imei, year, companyCode, estate, crop, spbsNumber}, 
						null, null, null, null);
				
				if(lstSpbsLine.size() > 0){
					for(int i = 0; i < lstSpbsLine.size(); i++){
						SPBSLine spbsLine = (SPBSLine) lstSpbsLine.get(i);
						
						String bpnId = spbsLine.getBpnId();
						
						if(!TextUtils.isEmpty(bpnId)){
							BPNHeader bpnHeader = (BPNHeader) database.getDataFirst(false, BPNHeader.TABLE_NAME, null, 
								BPNHeader.XML_BPN_ID + "=?", 
								new String [] {bpnId}, 
								null, null, null, null);
							
							if(bpnHeader != null){
								bpnHeader.setSpbsNumber("");
								
								database.updateData(bpnHeader, 
									BPNHeader.XML_BPN_ID + "=?", 
									new String [] {bpnId});
							}
						}
					}
				}
				
				
				database.deleteData(SPBSLine.TABLE_NAME, 
						SPBSLine.XML_IMEI + "=?" + " and " +
						SPBSLine.XML_YEAR + "=?" + " and " +
						SPBSLine.XML_COMPANY_CODE + "=?" + " and " +
						SPBSLine.XML_ESTATE + "=?" + " and " +
						SPBSLine.XML_CROP + "=?" + " and " +
						SPBSLine.XML_SPBS_NUMBER + "=?", 
						new String [] {imei, year, companyCode, estate, crop, spbsNumber});
				
				database.deleteData(SPBSHeader.TABLE_NAME, 
						SPBSHeader.XML_IMEI + "=?" + " and " +
						SPBSHeader.XML_YEAR + "=?" + " and " +
						SPBSHeader.XML_COMPANY_CODE + "=?" + " and " +
						SPBSHeader.XML_ESTATE + "=?" + " and " +
						SPBSHeader.XML_CROP + "=?" + " and " +
						SPBSHeader.XML_SPBS_NUMBER + "=?", 
						new String [] {imei, year, companyCode, estate, crop, spbsNumber});
				
				database.commitTransaction();
				
				lstSpbs.remove(pos);
				notifyDataSetChanged();
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();
			}finally{
				database.closeTransaction();
			}
			
		}else{
			new DialogNotification(activity, activity.getResources().getString(R.string.informasi), 
					activity.getResources().getString(R.string.data_already_export), false).show();
		}
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSpbs = null;
				lstSpbs = (List<SPBSHeader>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SPBSHeader> listResult = new ArrayList<SPBSHeader>();
				SPBSHeader spbsHeader;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<SPBSHeader>(lstSpbs);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						spbsHeader = lstTemp.get(i);
						
						if(spbsHeader.getSpbsNumber().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(spbsHeader);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
