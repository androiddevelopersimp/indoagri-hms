package com.simp.hms.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSKrani;
import com.simp.hms.model.SPBSHeader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPBSKrani extends BaseAdapter {

    private OnClickedListener mCallback;
    private Activity activity;
    private List<SPBSHeader> listData, listTemp;
    private int layout;

    public AdapterSPBSKrani(Activity activity, List<SPBSHeader> listData, int layout) {
        this.activity = activity;
        this.listData = listData;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int pos) {
        return listData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {
        final SPBSHeader item = listData.get(pos);

        ViewSPBSKrani view;

        Button btnStatus;
        TextView txtNameSpbs;
        Button btnPrintSpbs;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            btnStatus = (Button) convertView.findViewById(R.id.btnStatus);
            txtNameSpbs = (TextView) convertView.findViewById(R.id.txtSPbsName);
            btnPrintSpbs = (Button) convertView.findViewById(R.id.btnSpbsPrint);

            view = new ViewSPBSKrani(btnStatus, txtNameSpbs, btnPrintSpbs);

            convertView.setTag(view);
        } else {
            view = (ViewSPBSKrani) convertView.getTag();

            btnStatus = view.getBtnSpbsStatus();
            txtNameSpbs = view.getTxtNameSpbs();
            btnPrintSpbs = view.getBtnSpbsPrint();

        }


        String status = "DRAFT";
        if (item.getSendStatus() == 0) {
            status = "DRAFT";
        } else {
            status = "RECEIVED " + item.getSendStatus();
        }
        btnPrintSpbs.setText("SEND");
        txtNameSpbs.setText(item.getSpbsNumber());
        btnStatus.setText(status);

        btnPrintSpbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onItemClicked(pos);
            }
        });

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listData = null;
                listData = (List<SPBSHeader>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPBSHeader> listResult = new ArrayList<SPBSHeader>();
                SPBSHeader spbsHeader;

                if (listTemp == null) {
                    listTemp = new ArrayList<SPBSHeader>(listData);
                }

                if (constraint == null || constraint.length() == 0) {
                    result.count = listTemp.size();
                    result.values = listTemp;
                } else {
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for (int i = 0; i < listTemp.size(); i++) {
                        spbsHeader = listTemp.get(i);

                        if (spbsHeader.getSpbsNumber().toLowerCase(Locale.getDefault()).contains(constraint)) {
                            listResult.add(spbsHeader);
                        }
                    }
                    result.count = listResult.size();
                    result.values = listResult;
                }

                return result;
            }
        };
        return filter;
    }

    public void addOrUpdateData(SPBSHeader spbsHeader) {
        boolean found = false;

        for (int i = 0; i < listData.size(); i++) {
            if (spbsHeader.getSpbsNumber().equalsIgnoreCase(listData.get(i).getSpbsNumber())) {
                found = true;
                updateData(i, spbsHeader);

                break;
            }
        }

        if (!found) {
            listData.add(spbsHeader);
            notifyDataSetChanged();
        }
    }

    public void updateData(int pos, SPBSHeader spbsHeader) {
        listData.set(pos, spbsHeader);

        notifyDataSetChanged();
    }

    public void clearData() {
        listData.clear();
        notifyDataSetChanged();
    }

    public void deleteData(SPBSHeader spbsHeader) {
        for (int i = 0; i < listData.size(); i++) {
            SPBSHeader spbsDriverEx = listData.get(i);
            if (spbsDriverEx.getSpbsNumber().equalsIgnoreCase(spbsHeader.getSpbsNumber())) {
                listData.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setOnItemClickListener(OnClickedListener mCallback) {
        this.mCallback = mCallback;
    }

    public interface OnClickedListener {
        void onItemClicked(int posisition);
    }

}
