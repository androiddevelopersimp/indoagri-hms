package com.simp.hms.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.simp.hms.R;
import com.simp.hms.model.SPBSWB;

import java.util.List;

public class AdapterSPBSWBRecyclerview extends RecyclerView.Adapter<AdapterSPBSWBRecyclerview.MyViewHolder> {
    private Context mContext;
    private List<SPBSWB> spbswbs;
    private AppListener appListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtWbNumber, txtSPBSNumber, txtWbDate, txtTransactionType, txtTimeIn, txtTimeOut, txtDeduction, txtNettWeigth;


        public MyViewHolder(View view) {
            super(view);
            txtWbNumber = view.findViewById(R.id.txtWbNumber);
            txtSPBSNumber = view.findViewById(R.id.txtSPBSNo);
            txtWbDate = view.findViewById(R.id.txtWbDate);
            txtTransactionType = view.findViewById(R.id.txtWBTransactionType);
            txtTimeIn = view.findViewById(R.id.txtWbTimeIn);
            txtTimeOut = view.findViewById(R.id.txtWbTimeOut);
            txtDeduction = view.findViewById(R.id.txtWbDeduction);
            txtNettWeigth = view.findViewById(R.id.txtNettWeight);
        }
    }

    public AdapterSPBSWBRecyclerview(Context mContext, List<SPBSWB> spbswbs) {
        this.mContext = mContext;
        this.spbswbs = spbswbs;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_spbswb, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final SPBSWB spbswb = spbswbs.get(position);
        holder.txtWbNumber.setText(spbswb.getWbNumber());
        holder.txtSPBSNumber.setText(spbswb.getSpbsNumber());
        holder.txtTransactionType.setText(spbswb.getTransactionType());
        holder.txtWbDate.setText(spbswb.getWbDate());
        holder.txtTimeIn.setText(spbswb.getTimeIn());
        holder.txtTimeOut.setText(spbswb.getTimeOut());
        holder.txtDeduction.setText(spbswb.getDeduction());
        holder.txtNettWeigth.setText(spbswb.getNetto());
    }


    @Override
    public int getItemCount() {
        return spbswbs.size();
    }


    public interface AppListener {
        void onAppSelected(SPBSWB spbswb);
    }


}