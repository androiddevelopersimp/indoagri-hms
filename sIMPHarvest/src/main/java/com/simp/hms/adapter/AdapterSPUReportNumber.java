package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSReportNumberItem;
import com.simp.hms.adapter.view.ViewSPUReportNumberItem;
import com.simp.hms.model.SPBSReportNumber;
import com.simp.hms.model.SPUReportNumber;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPUReportNumber extends BaseAdapter {
    private Context context;
    private List<SPUReportNumber> lstSummary, lstTemp;
    private int layout;

    public AdapterSPUReportNumber(Context context, List<SPUReportNumber> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        SPUReportNumber item = lstSummary.get(pos);

        ViewSPUReportNumberItem view;
        TextView txtSpuReportNumberItemSpuNumber;
        TextView txtSpuReportNumberItemQtyGoodQty;
        TextView txtSpuReportNumberItemQtyGoodWeight;
        TextView txtSpuReportNumberItemQtyBadQty;
        TextView txtSpuReportNumberItemQtyBadWeight;
        TextView txtSpuReportNumberItemQtyPoorQty;
        TextView txtSpuReportNumberItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtSpuReportNumberItemSpuNumber = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemSpuNumber);
            txtSpuReportNumberItemQtyGoodQty = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyGoodQty);
            txtSpuReportNumberItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyGoodWeight);
            txtSpuReportNumberItemQtyBadQty = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyBadQty);
            txtSpuReportNumberItemQtyBadWeight = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyBadWeight);
            txtSpuReportNumberItemQtyPoorQty = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyPoorQty);
            txtSpuReportNumberItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyPoorWeight);

            view = new ViewSPUReportNumberItem(txtSpuReportNumberItemSpuNumber,
                    txtSpuReportNumberItemQtyGoodQty, txtSpuReportNumberItemQtyGoodWeight,
                    txtSpuReportNumberItemQtyBadQty, txtSpuReportNumberItemQtyBadWeight,
                    txtSpuReportNumberItemQtyPoorQty, txtSpuReportNumberItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewSPUReportNumberItem) convertView.getTag();

            txtSpuReportNumberItemSpuNumber = view.getTxtSpuReportNumberItemSpuNumber();
            txtSpuReportNumberItemQtyGoodQty = view.getTxtSpuReportNumberItemQtyGoodQty();
            txtSpuReportNumberItemQtyGoodWeight = view.getTxtSpuReportNumberItemQtyGoodWeight();
            txtSpuReportNumberItemQtyBadQty = view.getTxtSpuReportNumberItemQtyBadQty();
            txtSpuReportNumberItemQtyBadWeight = view.getTxtSpuReportNumberItemQtyBadWeight();
            txtSpuReportNumberItemQtyPoorQty = view.getTxtSpuReportNumberItemQtyPoorQty();
            txtSpuReportNumberItemQtyPoorWeight = view.getTxtSpuReportNumberItemQtyPoorWeight();
        }

        txtSpuReportNumberItemSpuNumber.setText(item.getSpuNumber());
        txtSpuReportNumberItemQtyGoodQty.setText(String.valueOf(Utils.round(item.getQtyGoodQty(), 2)));
        txtSpuReportNumberItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 2)));
        txtSpuReportNumberItemQtyBadQty.setText(String.valueOf(Utils.round(item.getQtyBadQty(), 2)));
        txtSpuReportNumberItemQtyBadWeight.setText(String.valueOf(Utils.round(item.getQtyBadWeight(), 2)));
        txtSpuReportNumberItemQtyPoorQty.setText(String.valueOf(Utils.round(item.getQtyPoorQty(), 2)));
        txtSpuReportNumberItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<SPUReportNumber>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPUReportNumber> lstResult = new ArrayList<SPUReportNumber>();
                SPUReportNumber item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<SPUReportNumber>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getSpuNumber().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
