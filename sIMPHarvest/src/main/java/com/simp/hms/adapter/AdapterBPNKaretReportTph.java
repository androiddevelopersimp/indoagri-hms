package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNKaretReportTphItem;
import com.simp.hms.adapter.view.ViewBPNReportTphItem;
import com.simp.hms.model.BPNKaretReportTph;
import com.simp.hms.model.BPNReportTph;
import com.simp.hms.model.DateLocal;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Fernando.Siagian on 3/11/2017.
 */

public class AdapterBPNKaretReportTph extends BaseAdapter {
    private Context context;
    private List<BPNKaretReportTph> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNKaretReportTph(Context context, List<BPNKaretReportTph> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNKaretReportTph item = lstSummary.get(pos);

        ViewBPNKaretReportTphItem view;
        TextView txtBpnReportTphItemTph;
        TextView txtBpnReportTphItemBpnDate;

        TextView txtBpnReportTphItemQtyLatexWet;
        TextView txtBpnReportTphItemQtyLatexDRC;
        TextView txtBpnReportTphItemQtyLatexDry;
        TextView txtBpnReportTphItemQtyLumpWet;
        TextView txtBpnReportTphItemQtyLumpDRC;
        TextView txtBpnReportTphItemQtyLumpDry;
        TextView txtBpnReportTphItemQtySlabWet;
        TextView txtBpnReportTphItemQtySlabDRC;
        TextView txtBpnReportTphItemQtySlabDry;
        TextView txtBpnReportTphItemQtyTreelaceWet;
        TextView txtBpnReportTphItemQtyTreelaceDRC;
        TextView txtBpnReportTphItemQtyTreelaceDry;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtBpnReportTphItemTph = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemTph);
            txtBpnReportTphItemBpnDate = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemBpnDate);

            txtBpnReportTphItemQtyLatexWet = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyLatexWet);
            txtBpnReportTphItemQtyLatexDRC = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyLatexDRC);
            txtBpnReportTphItemQtyLatexDry = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyLatexDry);
            txtBpnReportTphItemQtyLumpWet = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyLumpWet);
            txtBpnReportTphItemQtyLumpDRC = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyLumpDRC);
            txtBpnReportTphItemQtyLumpDry = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyLumpDry);
            txtBpnReportTphItemQtySlabWet = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtySlabWet);
            txtBpnReportTphItemQtySlabDRC = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtySlabDRC);
            txtBpnReportTphItemQtySlabDry = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtySlabDry);
            txtBpnReportTphItemQtyTreelaceWet = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyTreelaceWet);
            txtBpnReportTphItemQtyTreelaceDRC = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyTreelaceDRC);
            txtBpnReportTphItemQtyTreelaceDry = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyTreelaceDry);

            view = new ViewBPNKaretReportTphItem(txtBpnReportTphItemTph, txtBpnReportTphItemBpnDate,
                    txtBpnReportTphItemQtyLatexWet, txtBpnReportTphItemQtyLatexDRC, txtBpnReportTphItemQtyLatexDry,
                    txtBpnReportTphItemQtyLumpWet, txtBpnReportTphItemQtyLumpDRC, txtBpnReportTphItemQtyLumpDry,
                    txtBpnReportTphItemQtySlabWet, txtBpnReportTphItemQtySlabDRC, txtBpnReportTphItemQtySlabDry,
                    txtBpnReportTphItemQtyTreelaceWet, txtBpnReportTphItemQtyTreelaceDRC, txtBpnReportTphItemQtyTreelaceDry);

            convertView.setTag(view);
        }else{
            view = (ViewBPNKaretReportTphItem) convertView.getTag();

            txtBpnReportTphItemTph = view.getTxtBpnReportTphItemTph();
            txtBpnReportTphItemBpnDate = view.getTxtBpnReportTphItemBpnDate();
            txtBpnReportTphItemQtyLatexWet = view.getTxtBpnReportTphItemQtyLatexWet();
            txtBpnReportTphItemQtyLatexDRC = view.getTxtBpnReportTphItemQtyLatexDRC();
            txtBpnReportTphItemQtyLatexDry = view.getTxtBpnReportTphItemQtyLatexDry();
            txtBpnReportTphItemQtyLumpWet = view.getTxtBpnReportTphItemQtyLumpWet();
            txtBpnReportTphItemQtyLumpDRC = view.getTxtBpnReportTphItemQtyLumpDRC();
            txtBpnReportTphItemQtyLumpDry = view.getTxtBpnReportTphItemQtyLumpDry();
            txtBpnReportTphItemQtySlabWet = view.getTxtBpnReportTphItemQtySlabWet();
            txtBpnReportTphItemQtySlabDRC = view.getTxtBpnReportTphItemQtySlabDRC();
            txtBpnReportTphItemQtySlabDry = view.getTxtBpnReportTphItemQtySlabDry();
            txtBpnReportTphItemQtyTreelaceWet = view.getTxtBpnReportTphItemQtyTreelaceWet();
            txtBpnReportTphItemQtyTreelaceDRC = view.getTxtBpnReportTphItemQtyTreelaceDRC();
            txtBpnReportTphItemQtyTreelaceDry = view.getTxtBpnReportTphItemQtyTreelaceDry();
        }

        txtBpnReportTphItemTph.setText(item.getTph());
        txtBpnReportTphItemBpnDate.setText(new DateLocal(new Date(item.getCreatedDate())).getDateString(DateLocal.FORMAT_REPORT_VIEW));

        txtBpnReportTphItemQtyLatexWet.setText(String.valueOf(Utils.round(item.getQtyLatexWet(), 2)));
        txtBpnReportTphItemQtyLatexDRC.setText(String.valueOf(Utils.round(item.getQtyLatexDRC(), 2)));
        txtBpnReportTphItemQtyLatexDry.setText(String.valueOf(Utils.round(item.getQtyLatexDry(), 2)));
        txtBpnReportTphItemQtyLumpWet.setText(String.valueOf(Utils.round(item.getQtyLumpWet(), 2)));
        txtBpnReportTphItemQtyLumpDRC.setText(String.valueOf(Utils.round(item.getQtyLumpDRC(), 2)));
        txtBpnReportTphItemQtyLumpDry.setText(String.valueOf(Utils.round(item.getQtyLumpDry(), 2)));
        txtBpnReportTphItemQtySlabWet.setText(String.valueOf(Utils.round(item.getQtySlabWet(), 2)));
        txtBpnReportTphItemQtySlabDRC.setText(String.valueOf(Utils.round(item.getQtySlabDRC(), 2)));
        txtBpnReportTphItemQtySlabDry.setText(String.valueOf(Utils.round(item.getQtySlabDry(), 2)));
        txtBpnReportTphItemQtyTreelaceWet.setText(String.valueOf(Utils.round(item.getQtyTreelaceWet(), 2)));
        txtBpnReportTphItemQtyTreelaceDRC.setText(String.valueOf(Utils.round(item.getQtyTreelaceDRC(), 2)));
        txtBpnReportTphItemQtyTreelaceDry.setText(String.valueOf(Utils.round(item.getQtyTreelaceDry(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNKaretReportTph>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNKaretReportTph> lstResult = new ArrayList<BPNKaretReportTph>();
                BPNKaretReportTph item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNKaretReportTph>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getTph().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
