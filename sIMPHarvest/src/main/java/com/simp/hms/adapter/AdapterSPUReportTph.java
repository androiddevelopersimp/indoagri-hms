package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSReportTphItem;
import com.simp.hms.adapter.view.ViewSPUReportBlockItem;
import com.simp.hms.adapter.view.ViewSPUReportTphItem;
import com.simp.hms.model.SPBSReportTph;
import com.simp.hms.model.SPUReportTph;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPUReportTph extends BaseAdapter {
    private Context context;
    private List<SPUReportTph> lstSummary, lstTemp;
    private int layout;

    public AdapterSPUReportTph(Context context, List<SPUReportTph> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        SPUReportTph item = lstSummary.get(pos);

        ViewSPUReportTphItem view;
        TextView txtSpuReportTphItemTPh;
        TextView txtSpuReportTphItemQtyGoodQty;
        TextView txtSpuReportTphItemQtyGoodWeight;
        TextView txtSpuReportTphItemQtyBadQty;
        TextView txtSpuReportTphItemQtyBadWeight;
        TextView txtSpuReportTphItemQtyPoorQty;
        TextView txtSpuReportTphItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtSpuReportTphItemTPh = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemTPh);
            txtSpuReportTphItemQtyGoodQty = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyGoodQty);
            txtSpuReportTphItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyGoodWeight);
            txtSpuReportTphItemQtyBadQty = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyBadQty);
            txtSpuReportTphItemQtyBadWeight = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyBadWeight);
            txtSpuReportTphItemQtyPoorQty = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyPoorQty);
            txtSpuReportTphItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyPoorWeight);

            view = new ViewSPUReportTphItem(txtSpuReportTphItemTPh,
                                            txtSpuReportTphItemQtyGoodQty, txtSpuReportTphItemQtyGoodWeight,
                                            txtSpuReportTphItemQtyBadQty, txtSpuReportTphItemQtyBadWeight,
                                            txtSpuReportTphItemQtyPoorQty, txtSpuReportTphItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewSPUReportTphItem) convertView.getTag();

            txtSpuReportTphItemTPh = view.getTxtSpuReportTphItemTPh();
            txtSpuReportTphItemQtyGoodQty = view.getTxtSpuReportTphItemQtyGoodQty ();
            txtSpuReportTphItemQtyGoodWeight = view.getTxtSpuReportTphItemQtyGoodWeight();
            txtSpuReportTphItemQtyBadQty = view.getTxtSpuReportTphItemQtyBadQty();
            txtSpuReportTphItemQtyBadWeight = view.getTxtSpuReportTphItemQtyBadWeight();
            txtSpuReportTphItemQtyPoorQty = view.getTxtSpuReportTphItemQtyPoorQty();
            txtSpuReportTphItemQtyPoorWeight = view.getTxtSpuReportTphItemQtyPoorWeight();
        }

        txtSpuReportTphItemTPh.setText(item.getTph());
        txtSpuReportTphItemQtyGoodQty.setText(String.valueOf(Utils.round(item.getQtyGoodQty(), 2)));
        txtSpuReportTphItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 2)));
        txtSpuReportTphItemQtyBadQty.setText(String.valueOf(Utils.round(item.getQtyBadQty(), 2)));
        txtSpuReportTphItemQtyBadWeight.setText(String.valueOf(Utils.round(item.getQtyBadWeight(), 2)));
        txtSpuReportTphItemQtyPoorQty.setText(String.valueOf(Utils.round(item.getQtyPoorQty(), 2)));
        txtSpuReportTphItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<SPUReportTph>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPUReportTph> lstResult = new ArrayList<SPUReportTph>();
                SPUReportTph item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<SPUReportTph>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getTph().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
