package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSReportBlockItem;
import com.simp.hms.adapter.view.ViewSPUReportBlockItem;
import com.simp.hms.model.SPBSReportBlock;
import com.simp.hms.model.SPUReportBlock;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPUReportBlock extends BaseAdapter {
    private Context context;
    private List<SPUReportBlock> lstSummary, lstTemp;
    private int layout;

    public AdapterSPUReportBlock(Context context, List<SPUReportBlock> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        SPUReportBlock item = lstSummary.get(pos);

        ViewSPUReportBlockItem view;TextView txtSPBSReportBlockItemSpbsNumber;
        TextView txtSpuReportBlockItemBlock;
        TextView txtSpuReportBlockItemQtyGoodQty;
        TextView txtSpuReportBlockItemQtyGoodWeight;
        TextView txtSpuReportBlockItemQtyBadQty;
        TextView txtSpuReportBlockItemQtyBadWeight;
        TextView txtSpuReportBlockItemQtyPoorQty;
        TextView txtSpuReportBlockItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtSpuReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemBlock);
            txtSpuReportBlockItemQtyGoodQty = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyGoodQty);
            txtSpuReportBlockItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyGoodWeight);
            txtSpuReportBlockItemQtyBadQty = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyBadQty);
            txtSpuReportBlockItemQtyBadWeight = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyBadWeight);
            txtSpuReportBlockItemQtyPoorQty = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyPoorQty);
            txtSpuReportBlockItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyPoorWeight);

            view = new ViewSPUReportBlockItem(txtSpuReportBlockItemBlock, txtSpuReportBlockItemQtyGoodQty, txtSpuReportBlockItemQtyGoodWeight,
                    txtSpuReportBlockItemQtyBadQty, txtSpuReportBlockItemQtyBadWeight, txtSpuReportBlockItemQtyPoorQty, txtSpuReportBlockItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewSPUReportBlockItem) convertView.getTag();

            txtSpuReportBlockItemBlock = view.getTxtSpuReportBlockItemBlock();
            txtSpuReportBlockItemQtyGoodQty = view.getTxtSpuReportBlockItemQtyGoodQty ();
            txtSpuReportBlockItemQtyGoodWeight = view.getTxtSpuReportBlockItemQtyGoodWeight();
            txtSpuReportBlockItemQtyBadQty = view.getTxtSpuReportBlockItemQtyBadQty();
            txtSpuReportBlockItemQtyBadWeight = view.getTxtSpuReportBlockItemQtyBadWeight();
            txtSpuReportBlockItemQtyPoorQty = view.getTxtSpuReportBlockItemQtyPoorQty();
            txtSpuReportBlockItemQtyPoorWeight = view.getTxtSpuReportBlockItemQtyPoorWeight();
        }
        txtSpuReportBlockItemBlock.setText(item.getBlock());
        txtSpuReportBlockItemQtyGoodQty.setText(String.valueOf(Utils.round(item.getQtyGoodQty(), 2)));
        txtSpuReportBlockItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 2)));
        txtSpuReportBlockItemQtyBadQty.setText(String.valueOf(Utils.round(item.getQtyBadQty(), 2)));
        txtSpuReportBlockItemQtyBadWeight.setText(String.valueOf(Utils.round(item.getQtyBadWeight(), 2)));
        txtSpuReportBlockItemQtyPoorQty.setText(String.valueOf(Utils.round(item.getQtyPoorQty(), 2)));
        txtSpuReportBlockItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<SPUReportBlock>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPUReportBlock> lstResult = new ArrayList<SPUReportBlock>();
                SPUReportBlock item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<SPUReportBlock>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
