package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewTaksasiSummaryItem {
	private TextView txtTaksasiSummaryItemBarisSkb;
	private TextView txtTaksasiSummaryItemQtyJanjang;
	private TextView txtTaksasiSummaryItemQtyPokok;
	private TextView txtTaksasiSummaryItemTaksasiKg;

	public ViewTaksasiSummaryItem(TextView txtTaksasiSummaryItemBarisSkb,
			TextView txtTaksasiSummaryItemQtyJanjang,
			TextView txtTaksasiSummaryItemQtyPokok,
			TextView txtTaksasiSummaryItemTaksasiKg) {
		super();
		this.txtTaksasiSummaryItemBarisSkb = txtTaksasiSummaryItemBarisSkb;
		this.txtTaksasiSummaryItemQtyJanjang = txtTaksasiSummaryItemQtyJanjang;
		this.txtTaksasiSummaryItemQtyPokok = txtTaksasiSummaryItemQtyPokok;
		this.txtTaksasiSummaryItemTaksasiKg = txtTaksasiSummaryItemTaksasiKg;
	}

	public TextView getTxtTaksasiSummaryItemBarisSkb() {
		return txtTaksasiSummaryItemBarisSkb;
	}

	public void setTxtTaksasiSummaryItemBarisSkb(
			TextView txtTaksasiSummaryItemBarisSkb) {
		this.txtTaksasiSummaryItemBarisSkb = txtTaksasiSummaryItemBarisSkb;
	}

	public TextView getTxtTaksasiSummaryItemQtyJanjang() {
		return txtTaksasiSummaryItemQtyJanjang;
	}

	public void setTxtTaksasiSummaryItemQtyJanjang(
			TextView txtTaksasiSummaryItemQtyJanjang) {
		this.txtTaksasiSummaryItemQtyJanjang = txtTaksasiSummaryItemQtyJanjang;
	}

	public TextView getTxtTaksasiSummaryItemQtyPokok() {
		return txtTaksasiSummaryItemQtyPokok;
	}

	public void setTxtTaksasiSummaryItemQtyPokok(
			TextView txtTaksasiSummaryItemQtyPokok) {
		this.txtTaksasiSummaryItemQtyPokok = txtTaksasiSummaryItemQtyPokok;
	}

	public TextView getTxtTaksasiSummaryItemTaksasiKg() {
		return txtTaksasiSummaryItemTaksasiKg;
	}

	public void setTxtTaksasiSummaryItemTaksasiKg(
			TextView txtTaksasiSummaryItemTaksasiKg) {
		this.txtTaksasiSummaryItemTaksasiKg = txtTaksasiSummaryItemTaksasiKg;
	}

}
