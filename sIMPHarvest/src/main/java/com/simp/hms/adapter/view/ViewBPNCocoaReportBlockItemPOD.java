package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNCocoaReportBlockItemPOD {
    private TextView txtBpnReportBlockItemBlock;

    private TextView txtBpnReportBlockItemQtyPODQty;
    private TextView txtBpnReportBlockItemQtyGoodWeight;
    private TextView txtBpnReportBlockItemQtyEstimasiQty;
    private TextView txtBpnReportBlockItemQtyPoorWeight;

    public ViewBPNCocoaReportBlockItemPOD(
            TextView txtBpnReportBlockItemBlock,
            TextView txtBpnReportBlockItemQtyPODQty,
            TextView txtBpnReportBlockItemQtyGoodWeight,
            TextView txtBpnReportBlockItemQtyEstimasiQty,
            TextView txtBpnReportBlockItemQtyPoorWeight) {
        super();
        this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
        this.txtBpnReportBlockItemQtyPODQty = txtBpnReportBlockItemQtyPODQty;
        this.txtBpnReportBlockItemQtyGoodWeight = txtBpnReportBlockItemQtyGoodWeight;
        this.txtBpnReportBlockItemQtyEstimasiQty= txtBpnReportBlockItemQtyEstimasiQty;
        this.txtBpnReportBlockItemQtyPoorWeight = txtBpnReportBlockItemQtyPoorWeight;
    }

    public TextView getTxtBpnReportBlockItemBlock() {
        return txtBpnReportBlockItemBlock;
    }

    public void setTxtBpnReportBlockItemBlock(
            TextView txtBpnReportBlockItemBlock) {
        this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
    }

    public TextView getTxtBpnReportBlockItemQtyPODQty() {
        return txtBpnReportBlockItemQtyPODQty;
    }

    public void setTxtBpnReportBlockItemQtyGoodQty(
            TextView txtBpnReportBlockItemQtyGoodQty) {
        this.txtBpnReportBlockItemQtyPODQty = txtBpnReportBlockItemQtyPODQty;
    }

    public TextView getTxtBpnReportBlockItemQtyGoodWeight() {
        return txtBpnReportBlockItemQtyGoodWeight;
    }

    public void setTxtBpnReportBlockItemQtyGoodWeight(
            TextView txtBpnReportBlockItemQtyGoodWeight) {
        this.txtBpnReportBlockItemQtyGoodWeight = txtBpnReportBlockItemQtyGoodWeight;
    }


    public TextView getTxtBpnReportBlockItemQtyEstimasiQty() {
        return txtBpnReportBlockItemQtyEstimasiQty;
    }

    public void setTxtBpnReportBlockItemQtyEstimasiQty(
            TextView txtBpnReportBlockItemQtyEstimasiQty) {
        this.txtBpnReportBlockItemQtyEstimasiQty = txtBpnReportBlockItemQtyEstimasiQty;
    }

    public TextView getTxtBpnReportBlockItemQtyPoorWeight() {
        return txtBpnReportBlockItemQtyPoorWeight;
    }

    public void setTxtBpnReportBlockItemQtyPoorWeight(
            TextView txtBpnReportBlockItemQtyPoorWeight) {
        this.txtBpnReportBlockItemQtyPoorWeight = txtBpnReportBlockItemQtyPoorWeight;
    }

}
