package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ViewSpuSummaryItem {
    private Button btnSpuSummaryItemDelete;
    private TextView txtSpuSummaryItemBlock;
    private TextView txtSpuSummaryItemQtyGoodWeight;
    private TextView txtSpuSummaryItemQtyBadWeight;
    private TextView txtSpuSummaryItemQtyPoorWeight;
    private TextView txtSpuSummaryItemQtyTotalWeight;

    public ViewSpuSummaryItem(Button btnSpuSummaryItemDelete, TextView txtSpuSummaryItemBlock,
                               TextView txtSpuSummaryItemQtyGoodWeight,
                               TextView txtSpuSummaryItemQtyBadWeight,
                               TextView txtSpuSummaryItemQtyPoorWeight,
                               TextView txtSpuSummaryItemQtyTotalWeight) {
        super();
        this.btnSpuSummaryItemDelete = btnSpuSummaryItemDelete;
        this.txtSpuSummaryItemBlock = txtSpuSummaryItemBlock;
        this.txtSpuSummaryItemQtyGoodWeight = txtSpuSummaryItemQtyGoodWeight;
        this.txtSpuSummaryItemQtyBadWeight = txtSpuSummaryItemQtyBadWeight;
        this.txtSpuSummaryItemQtyPoorWeight = txtSpuSummaryItemQtyPoorWeight;
        this.txtSpuSummaryItemQtyTotalWeight = txtSpuSummaryItemQtyTotalWeight;
    }

    public Button getBtnSpuSummaryItemDelete() {
        return btnSpuSummaryItemDelete;
    }

    public void setBtnSpuSummaryItemDelete(Button btnSpuSummaryItemDelete) {
        this.btnSpuSummaryItemDelete = btnSpuSummaryItemDelete;
    }

    public TextView getTxtSpuSummaryItemBlock() {
        return txtSpuSummaryItemBlock;
    }

    public void setTxtSpuSummaryItemBlock(TextView txtSpuSummaryItemBlock) {
        this.txtSpuSummaryItemBlock = txtSpuSummaryItemBlock;
    }

    public TextView getTxtSpuSummaryItemQtyGoodWeight() {
        return txtSpuSummaryItemQtyGoodWeight;
    }

    public void setTxtSpuSummaryItemQtyGoodWeight(
            TextView txtSpuSummaryItemQtyGoodWeight) {
        this.txtSpuSummaryItemQtyGoodWeight = txtSpuSummaryItemQtyGoodWeight;
    }

    public TextView getTxtSpuSummaryItemQtyBadWeight() {
        return txtSpuSummaryItemQtyBadWeight;
    }

    public void setTxtSpuSummaryItemQtyBadWeight(
            TextView txtSpuSummaryItemQtyBadWeight) {
        this.txtSpuSummaryItemQtyBadWeight = txtSpuSummaryItemQtyBadWeight;
    }

    public TextView getTxtSpuSummaryItemQtyPoorWeight() {
        return txtSpuSummaryItemQtyPoorWeight;
    }

    public void setTxtSpuSummaryItemQtyPoorWeight(
            TextView txtSpuSummaryItemQtyPoorWeight) {
        this.txtSpuSummaryItemQtyPoorWeight = txtSpuSummaryItemQtyPoorWeight;
    }

    public TextView getTxtSpuSummaryItemQtyTotalWeight() {
        return txtSpuSummaryItemQtyTotalWeight;
    }

    public void setTxtSpuSummaryItemQtyTotalWeight(
            TextView txtSpuSummaryItemQtyTotalWeight) {
        this.txtSpuSummaryItemQtyTotalWeight = txtSpuSummaryItemQtyTotalWeight;
    }
}
