package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPBSReportBlockItem {
	private TextView txtSpbsReportBlockItemSpbsNumber;
	private TextView txtSpbsReportBlockItemSpbsDate;
	private TextView txtSpbsReportBlockItemBlock;
	private TextView txtSpbsReportBlockItemQtyJanjang;
	private TextView txtSpbsReportBlockItemQtyLooseFruit;

	public ViewSPBSReportBlockItem(TextView txtSpbsReportBlockItemSpbsNumber,
			TextView txtSpbsReportBlockItemSpbsDate,
			TextView txtSpbsReportBlockItemBlock,
			TextView txtSpbsReportBlockItemQtyJanjang,
			TextView txtSpbsReportBlockItemQtyLooseFruit) {
		super();
		this.txtSpbsReportBlockItemSpbsNumber = txtSpbsReportBlockItemSpbsNumber;
		this.txtSpbsReportBlockItemSpbsDate = txtSpbsReportBlockItemSpbsDate;
		this.txtSpbsReportBlockItemBlock = txtSpbsReportBlockItemBlock;
		this.txtSpbsReportBlockItemQtyJanjang = txtSpbsReportBlockItemQtyJanjang;
		this.txtSpbsReportBlockItemQtyLooseFruit = txtSpbsReportBlockItemQtyLooseFruit;
	}

	public TextView getTxtSpbsReportBlockItemSpbsNumber() {
		return txtSpbsReportBlockItemSpbsNumber;
	}

	public void setTxtSpbsReportBlockItemSpbsNumber(
			TextView txtSpbsReportBlockItemSpbsNumber) {
		this.txtSpbsReportBlockItemSpbsNumber = txtSpbsReportBlockItemSpbsNumber;
	}

	public TextView getTxtSpbsReportBlockItemSpbsDate() {
		return txtSpbsReportBlockItemSpbsDate;
	}

	public void setTxtSpbsReportBlockItemSpbsDate(
			TextView txtSpbsReportBlockItemSpbsDate) {
		this.txtSpbsReportBlockItemSpbsDate = txtSpbsReportBlockItemSpbsDate;
	}

	public TextView getTxtSpbsReportBlockItemBlock() {
		return txtSpbsReportBlockItemBlock;
	}

	public void setTxtSpbsReportBlockItemBlock(
			TextView txtSpbsReportBlockItemBlock) {
		this.txtSpbsReportBlockItemBlock = txtSpbsReportBlockItemBlock;
	}

	public TextView getTxtSpbsReportBlockItemQtyJanjang() {
		return txtSpbsReportBlockItemQtyJanjang;
	}

	public void setTxtSpbsReportBlockItemQtyJanjang(
			TextView txtSpbsReportBlockItemQtyJanjang) {
		this.txtSpbsReportBlockItemQtyJanjang = txtSpbsReportBlockItemQtyJanjang;
	}

	public TextView getTxtSpbsReportBlockItemQtyLooseFruit() {
		return txtSpbsReportBlockItemQtyLooseFruit;
	}

	public void setTxtSpbsReportBlockItemQtyLooseFruit(
			TextView txtSpbsReportBlockItemQtyLooseFruit) {
		this.txtSpbsReportBlockItemQtyLooseFruit = txtSpbsReportBlockItemQtyLooseFruit;
	}

}
