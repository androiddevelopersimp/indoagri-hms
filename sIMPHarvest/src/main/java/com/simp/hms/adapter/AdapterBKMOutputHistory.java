package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBKMOutputHistoryItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.model.BKMOutput;
import com.simp.hms.model.DateLocal;

import android.app.Activity;
import android.database.sqlite.SQLiteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBKMOutputHistory extends BaseAdapter{
	private Activity activity;
	private List<BKMOutput> lstBkmOutput, lstTemp;
	private int layout;
	
	public AdapterBKMOutputHistory(Activity activity, List<BKMOutput> lstBkmOutput, int layout){
		this.activity = activity;
		this.lstBkmOutput = lstBkmOutput;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstBkmOutput.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstBkmOutput.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BKMOutput bkmOutput = lstBkmOutput.get(pos);
		
		ViewBKMOutputHistoryItem view;
		TextView txtBkmAbsentHistoryItemDate;
		TextView txtBkmAbsentHistoryItemGang;
		TextView txtBkmAbsentHistoryItemBlock;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtBkmAbsentHistoryItemDate = (TextView) convertView.findViewById(R.id.txtBkmOutputHistoryItemDate);
            txtBkmAbsentHistoryItemGang = (TextView) convertView.findViewById(R.id.txtBkmOutputHistoryItemGang);
            txtBkmAbsentHistoryItemBlock = (TextView) convertView.findViewById(R.id.txtBkmOutputHistoryItemBlock);
            
            view = new ViewBKMOutputHistoryItem(txtBkmAbsentHistoryItemDate, txtBkmAbsentHistoryItemGang, txtBkmAbsentHistoryItemBlock);
            
            convertView.setTag(view);
        }else{
        	view = (ViewBKMOutputHistoryItem) convertView.getTag();
        	  
        	txtBkmAbsentHistoryItemDate = view.getTxtBkmAbsentHistoryItemDate();
        	txtBkmAbsentHistoryItemGang = view.getTxtBkmAbsentHistoryItemGang();
        	txtBkmAbsentHistoryItemBlock = view.getTxtBkmAbsentHistoryItemBlock();
        }

        txtBkmAbsentHistoryItemDate.setText(new DateLocal(bkmOutput.getBkmDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtBkmAbsentHistoryItemGang.setText(bkmOutput.getGang());
        txtBkmAbsentHistoryItemBlock.setText(bkmOutput.getBlock());

        return convertView;
	}
	
	public void deleteData(int pos){
		BKMOutput item = lstBkmOutput.get(pos);
		
		String companyCode = item.getCompanyCode();
		String estate = item.getEstate();
		String bkmDate = item.getBkmDate();
		String division = item.getDivision();
		String gang = item.getGang();
		String block = item.getBlock();
		int status = item.getStatus();
		
		if(status == 0){
			DatabaseHandler database = new DatabaseHandler(activity);
			
			try{
				database.openTransaction();
				
				database.deleteData(BKMOutput.TABLE_NAME, 
						BKMOutput.XML_COMPANY_CODE + "=?" + " and " +
						BKMOutput.XML_ESTATE + "=?" + " and " +
						BKMOutput.XML_BKM_DATE + "=?" + " and " + 
						BKMOutput.XML_DIVISION + "=?" + " and " +
						BKMOutput.XML_GANG + "=?" + " and " +
						BKMOutput.XML_BLOCK + "=?", 
						new String [] {companyCode, estate, bkmDate, division, gang, block});
				
				database.commitTransaction();
				
				lstBkmOutput.remove(pos);
				notifyDataSetChanged();
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();
			}finally{
				database.closeTransaction();
			}
		}else{
			new DialogNotification(activity, activity.getResources().getString(R.string.informasi), 
					activity.getResources().getString(R.string.data_already_export), false).show();
		}
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstBkmOutput = null;
				lstBkmOutput = (List<BKMOutput>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BKMOutput> listResult = new ArrayList<BKMOutput>();
				BKMOutput bkmOutput;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BKMOutput>(lstBkmOutput);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						bkmOutput = lstTemp.get(i);
						
						if(bkmOutput.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(bkmOutput);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
