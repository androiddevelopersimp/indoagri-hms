package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewDivisionAssistantItem {
	private TextView txtDivisionAssistantItemEstate;
	private TextView txtDivisionAssistantItemDivision;
	private TextView txtDivisionAssistantItemSpras;
	private TextView txtDivisionAssistantItemDescription;
	private TextView txtDivisionAssistantItemAssistant;
	private TextView txtDivisionAssistantItemDistanceToMill;
	private TextView txtDivisionAssistantItemUom;
	private TextView txtDivisionAssistantItemAssistantName;
	private TextView txtDivisionAssistantItemLifnr;

	public ViewDivisionAssistantItem(TextView txtAssistantDivisionItemEstate,
			TextView txtDivisionAssistantItemDivision,
			TextView txtDivisionAssistantItemSpras,
			TextView txtDivisionAssistantItemDescription,
			TextView txtDivisionAssistantItemAssistant,
			TextView txtDivisionAssistantItemDistanceToMill,
			TextView txtDivisionAssistantItemUom,
			TextView txtDivisionAssistantItemAssistantName,
			TextView txtDivisionAssistantItemLifnr) {
		super();
		this.txtDivisionAssistantItemEstate = txtAssistantDivisionItemEstate;
		this.txtDivisionAssistantItemDivision = txtDivisionAssistantItemDivision;
		this.txtDivisionAssistantItemSpras = txtDivisionAssistantItemSpras;
		this.txtDivisionAssistantItemDescription = txtDivisionAssistantItemDescription;
		this.txtDivisionAssistantItemAssistant = txtDivisionAssistantItemAssistant;
		this.txtDivisionAssistantItemDistanceToMill = txtDivisionAssistantItemDistanceToMill;
		this.txtDivisionAssistantItemUom = txtDivisionAssistantItemUom;
		this.txtDivisionAssistantItemAssistantName = txtDivisionAssistantItemAssistantName;
		this.txtDivisionAssistantItemLifnr = txtDivisionAssistantItemLifnr;
	}

	public TextView getTxtDivisionAssistantItemEstate() {
		return txtDivisionAssistantItemEstate;
	}

	public void setTxtDivisionAssistantItemEstate(
			TextView txtAssistantDivisionItemEstate) {
		this.txtDivisionAssistantItemEstate = txtAssistantDivisionItemEstate;
	}

	public TextView gettxtDivisionAssistantItemDivision() {
		return txtDivisionAssistantItemDivision;
	}

	public void settxtDivisionAssistantItemDivision(
			TextView txtDivisionAssistantItemDivision) {
		this.txtDivisionAssistantItemDivision = txtDivisionAssistantItemDivision;
	}

	public TextView gettxtDivisionAssistantItemSpras() {
		return txtDivisionAssistantItemSpras;
	}

	public void settxtDivisionAssistantItemSpras(
			TextView txtDivisionAssistantItemSpras) {
		this.txtDivisionAssistantItemSpras = txtDivisionAssistantItemSpras;
	}

	public TextView gettxtDivisionAssistantItemDescription() {
		return txtDivisionAssistantItemDescription;
	}

	public void settxtDivisionAssistantItemDescription(
			TextView txtDivisionAssistantItemDescription) {
		this.txtDivisionAssistantItemDescription = txtDivisionAssistantItemDescription;
	}

	public TextView gettxtDivisionAssistantItemAssistant() {
		return txtDivisionAssistantItemAssistant;
	}

	public void settxtDivisionAssistantItemAssistant(
			TextView txtDivisionAssistantItemAssistant) {
		this.txtDivisionAssistantItemAssistant = txtDivisionAssistantItemAssistant;
	}

	public TextView gettxtDivisionAssistantItemDistanceToMill() {
		return txtDivisionAssistantItemDistanceToMill;
	}

	public void settxtDivisionAssistantItemDistanceToMill(
			TextView txtDivisionAssistantItemDistanceToMill) {
		this.txtDivisionAssistantItemDistanceToMill = txtDivisionAssistantItemDistanceToMill;
	}

	public TextView gettxtDivisionAssistantItemUom() {
		return txtDivisionAssistantItemUom;
	}

	public void settxtDivisionAssistantItemUom(
			TextView txtDivisionAssistantItemUom) {
		this.txtDivisionAssistantItemUom = txtDivisionAssistantItemUom;
	}

	public TextView gettxtDivisionAssistantItemAssistantName() {
		return txtDivisionAssistantItemAssistantName;
	}

	public void settxtDivisionAssistantItemAssistantName(
			TextView txtDivisionAssistantItemAssistantName) {
		this.txtDivisionAssistantItemAssistantName = txtDivisionAssistantItemAssistantName;
	}


	public TextView gettxtDivisionAssistantItemLifnr() {
		return txtDivisionAssistantItemLifnr;
	}

	public void settxtDivisionAssistantItemLifnr(
			TextView txtDivisionAssistantItemLifnr) {
		this.txtDivisionAssistantItemLifnr = txtDivisionAssistantItemLifnr;
	}
}
