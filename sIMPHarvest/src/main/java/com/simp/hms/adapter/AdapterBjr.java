package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBjrItem;
import com.simp.hms.model.BJR;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBjr extends BaseAdapter{
	private Context context;
	private List<BJR> lst_bjr, lst_temp;
	private int layout;
	
	private ViewBjrItem vie_bjr;
	private TextView txt_bjr_item_id;
	private TextView txt_bjr_item_company_code;
	private TextView txt_bjr_item_estate;
	private TextView txt_bjr_item_block;
	private TextView txt_bjr_item_eff_date;
	private TextView txt_bjr_item_bjr;
	private TextView txt_bjr_item_uom;
	private TextView txt_bjr_item_mandt;
	
	public AdapterBjr(Context context, List<BJR> lst_skb, int layout){
		this.context = context;
		this.lst_bjr = lst_skb;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_bjr.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_bjr.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BJR bjr = lst_bjr.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txt_bjr_item_id = (TextView) convertView.findViewById(R.id.txt_bjr_item_id);
            txt_bjr_item_company_code = (TextView) convertView.findViewById(R.id.txt_bjr_item_company_code);
            txt_bjr_item_estate = (TextView) convertView.findViewById(R.id.txt_bjr_item_estate);
            txt_bjr_item_block = (TextView) convertView.findViewById(R.id.txt_bjr_item_block);
            txt_bjr_item_eff_date = (TextView) convertView.findViewById(R.id.txt_bjr_item_eff_date);
            txt_bjr_item_bjr = (TextView) convertView.findViewById(R.id.txt_bjr_item_bjr);
            txt_bjr_item_uom = (TextView) convertView.findViewById(R.id.txt_bjr_item_uom);
            txt_bjr_item_mandt = (TextView) convertView.findViewById(R.id.txt_bjr_item_mandt);
            
            vie_bjr = new ViewBjrItem(txt_bjr_item_id, txt_bjr_item_company_code, txt_bjr_item_estate, txt_bjr_item_block, 
            		txt_bjr_item_eff_date, txt_bjr_item_bjr, txt_bjr_item_uom, txt_bjr_item_mandt);     
            convertView.setTag(vie_bjr);
        }else{
        	vie_bjr = (ViewBjrItem) convertView.getTag();
        	
        	txt_bjr_item_id = vie_bjr.getTxtBjrItemId();
        	txt_bjr_item_company_code = vie_bjr.getTxtBjrItemCompanyCode();
        	txt_bjr_item_estate = vie_bjr.getTxtBjrItemEstate();
        	txt_bjr_item_block = vie_bjr.getTxtBjrItemBlock();
        	txt_bjr_item_eff_date = vie_bjr.getTxtBjrItemEffDate();
        	txt_bjr_item_bjr = vie_bjr.getTxtBjrItemBjr();
        	txt_bjr_item_uom = vie_bjr.getTxtBjrItemUom();
        	txt_bjr_item_mandt = vie_bjr.getTxtBjrItemMandt();
        }

        txt_bjr_item_id.setText(String.valueOf(bjr.getId()));
        txt_bjr_item_company_code.setText(bjr.getCompanyCode());
        txt_bjr_item_estate.setText(bjr.getEstate());
        txt_bjr_item_block.setText(bjr.getBlock());
        txt_bjr_item_eff_date.setText(bjr.getEffDate());
        txt_bjr_item_bjr.setText(String.valueOf(bjr.getBjr()));


        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lst_bjr = null;
				lst_bjr = (List<BJR>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BJR> list_result = new ArrayList<BJR>();
				BJR bjr;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<BJR>(lst_bjr);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						bjr = lst_temp.get(i);
						
						if(bjr.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(bjr);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
