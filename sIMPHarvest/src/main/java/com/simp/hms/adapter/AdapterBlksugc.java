package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBlksugcItem;
import com.simp.hms.adapter.view.ViewBlockHdrcItem;
import com.simp.hms.model.BLKSUGC;
import com.simp.hms.model.BlockHdrc;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterBlksugc extends BaseAdapter{
	private Context context;
	private List<BLKSUGC> lstBlksugc, lst_temp;
	private int layout;

	private ViewBlksugcItem vie_block_hdrc;
	private TextView txt_blcksugc_block;
	private TextView txt_blksugc_phase;
	private TextView txt_blksugc_sub_division;
	private TextView txt_blksugc_hektar;

	public AdapterBlksugc(Context context, List<BLKSUGC> lstBlksugc, int layout){
		this.context = context;
		this.lstBlksugc = lstBlksugc;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstBlksugc.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstBlksugc.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BLKSUGC blksugc = lstBlksugc.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

			txt_blcksugc_block = (TextView) convertView.findViewById(R.id.txt_blcksugc_block);
			txt_blksugc_phase = (TextView) convertView.findViewById(R.id.txt_blksugc_phase);
			txt_blksugc_sub_division = (TextView) convertView.findViewById(R.id.txt_blksugc_sub_division);
			txt_blksugc_hektar = (TextView) convertView.findViewById(R.id.txt_blksugc_hektar);
            
            vie_block_hdrc = new ViewBlksugcItem(txt_blcksugc_block, txt_blksugc_phase, txt_blksugc_sub_division, txt_blksugc_hektar);
            convertView.setTag(vie_block_hdrc);
        }else{
        	vie_block_hdrc = (ViewBlksugcItem) convertView.getTag();

			txt_blcksugc_block = vie_block_hdrc.getTxt_blcksugc_block();
			txt_blksugc_phase = vie_block_hdrc.getTxt_blksugc_phase();
			txt_blksugc_sub_division = vie_block_hdrc.getTxt_blksugc_sub_division();
			txt_blksugc_hektar = vie_block_hdrc.getTxt_blksugc_hektar();
        }

		txt_blcksugc_block.setText(String.valueOf(blksugc.getBlock()));
		txt_blksugc_phase.setText(blksugc.getPhase());
		txt_blksugc_sub_division.setText(blksugc.getSubDivision());
		txt_blksugc_hektar.setText("");


        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstBlksugc = null;
				lstBlksugc = (List<BLKSUGC>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BLKSUGC> list_result = new ArrayList<BLKSUGC>();
				BLKSUGC blksugc;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<BLKSUGC>(lstBlksugc);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						blksugc = lst_temp.get(i);
						
						if(blksugc.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(blksugc);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
	
	public void addData(BLKSUGC blksugc){
		boolean found = false;
		
		for(int i = 0; i < lstBlksugc.size(); i++){
			if(blksugc.getBlock().equalsIgnoreCase(lstBlksugc.get(i).getBlock())){
				found = true;
				break;
			}
		}
		
		if(!found){
			lstBlksugc.add(blksugc);
			notifyDataSetChanged();
		}
	}
}
