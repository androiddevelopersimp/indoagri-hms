package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNCocoaReportTphItemPOD {
    private TextView txtBpnReportTphItemTph;
    private TextView txtBpnReportTphItemBpnDate;

    private TextView txtBpnReportTphItemQtyPODQty;
    private TextView txtBpnReportTphItemQtyGoodWeight;
    private TextView txtBpnReportTphItemQtyEstimasiQty;
    private TextView txtBpnReportTphItemQtyPoorWeight;

    public ViewBPNCocoaReportTphItemPOD(
            TextView txtBpnReportTphItemTph,
            TextView txtBpnReportTphItemBpnDate,
            TextView txtBpnReportTphItemQtyPODQty,
            TextView txtBpnReportTphItemQtyGoodWeight,
            TextView txtBpnReportTphItemQtyEstimasiQty,
            TextView txtBpnReportTphItemQtyPoorWeight
    ) {
        super();
        this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
        this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
        this.txtBpnReportTphItemQtyPODQty = txtBpnReportTphItemQtyPODQty;
        this.txtBpnReportTphItemQtyGoodWeight = txtBpnReportTphItemQtyGoodWeight;
        this.txtBpnReportTphItemQtyEstimasiQty = txtBpnReportTphItemQtyEstimasiQty;
        this.txtBpnReportTphItemQtyPoorWeight = txtBpnReportTphItemQtyPoorWeight;
    }

    public TextView getTxtBpnReportTphItemTph() {
        return txtBpnReportTphItemTph;
    }

    public void setTxtBpnReportTphItemTph(TextView txtBpnReportTphItemTph) {
        this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
    }

    public TextView getTxtBpnReportTphItemBpnDate() {
        return txtBpnReportTphItemBpnDate;
    }

    public void setTxtBpnReportTphItemBpnDate(
            TextView txtBpnReportTphItemBpnDate) {
        this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
    }

    public TextView getTxtBpnReportTphItemQtyPODQty() {
        return txtBpnReportTphItemQtyPODQty;
    }

    public void setTxtBpnReportTphItemQtyPODQty(
            TextView txtBpnReportTphItemQtyPODQty) {
        this.txtBpnReportTphItemQtyPODQty = txtBpnReportTphItemQtyPODQty;
    }

    public TextView getTxtBpnReportTphItemQtyGoodWeight() {
        return txtBpnReportTphItemQtyGoodWeight;
    }

    public void setTxtBpnReportTphItemQtyGoodWeight(
            TextView txtBpnReportTphItemQtyGoodWeight) {
        this.txtBpnReportTphItemQtyGoodWeight = txtBpnReportTphItemQtyGoodWeight;
    }



    public TextView getTxtBpnReportTphItemQtyEstimasiQty() {
        return txtBpnReportTphItemQtyEstimasiQty;
    }

    public void setTxtBpnReportTphItemQtyEstimasiQty(
            TextView txtBpnReportTphItemQtyEstimasiQty) {
        this.txtBpnReportTphItemQtyEstimasiQty = txtBpnReportTphItemQtyEstimasiQty;
    }

    public TextView getTxtBpnReportTphItemQtyPoorWeight() {
        return txtBpnReportTphItemQtyPoorWeight;
    }

    public void setTxtBpnReportTphItemQtyPoorWeight(
            TextView txtBpnReportTphItemQtyPoorWeight) {
        this.txtBpnReportTphItemQtyPoorWeight = txtBpnReportTphItemQtyPoorWeight;
    }

}
