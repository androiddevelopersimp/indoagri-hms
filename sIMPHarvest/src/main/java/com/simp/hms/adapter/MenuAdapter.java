package com.simp.hms.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.simp.hms.R;
import com.simp.hms.adapter.holder.MainMenuViewHolder;
import com.simp.hms.model.Menu;

import java.util.List;

/**
 * Created by Anka.Wirawan on 8/3/2016.
 */
public class MenuAdapter extends RecyclerView.Adapter<MainMenuViewHolder> {
    private Context context;
    private List<Menu> lstMenu;

    private LayoutInflater lytInflater;

    public MenuAdapter(Context context, List<Menu> lstMenu){
        this.context = context;
        this.lstMenu = lstMenu;
        this.lytInflater = LayoutInflater.from(context);
    }

    @Override
    public MainMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = lytInflater.inflate(R.layout.item_main_menu, parent, false);

        MainMenuViewHolder holder = new MainMenuViewHolder(rootView);

        return holder;
    }

    @Override
    public void onBindViewHolder(MainMenuViewHolder holder, int pos) {
        holder.bindData(context, lstMenu.get(pos));
    }

    @Override
    public int getItemCount() {
        return lstMenu.size();
    }

    public void addData(Menu menu, int pos){
        lstMenu.add(pos, menu);
        notifyDataSetChanged();
    }
}

