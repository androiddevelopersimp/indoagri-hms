package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSNumber;
import com.simp.hms.adapter.view.ViewSPBSWB;
import com.simp.hms.model.SPBSWB;

import java.util.List;

public class AdapterSPBSWB extends BaseAdapter {

    private Context context;
    private List<SPBSWB> listData, listTemp;
    private int layout;
    private OnWBClickedListener clickedListener;

    public AdapterSPBSWB(Context context, List<SPBSWB> listData, int layout) {
        this.context = context;
        this.listData = listData;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int pos) {
        return listData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        final SPBSWB item = listData.get(pos);

        ViewSPBSWB view;

        TextView txtWBNumber, txtSPBSNumber, txtNettWeight;
        LinearLayout llWB;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtWBNumber = (TextView) convertView.findViewById(R.id.txtWbNumber);
            txtSPBSNumber = (TextView) convertView.findViewById(R.id.txtSPBSNo);
            txtNettWeight = (TextView) convertView.findViewById(R.id.txtNettWeight);
            llWB = (LinearLayout) convertView.findViewById(R.id.llWB);

            view = new ViewSPBSWB(txtWBNumber, txtSPBSNumber, txtNettWeight, llWB);

            convertView.setTag(view);
        } else {
            view = (ViewSPBSWB) convertView.getTag();
            txtWBNumber = view.getWbNumber();
            txtSPBSNumber = view.getSpbsNumber();
            txtNettWeight = view.getNettWeight();
            llWB = view.getLlWB();
        }

        if (item.getWbNumber() == null) {
            txtWBNumber.setText("-");
            txtNettWeight.setText("-");
            txtSPBSNumber.setBackgroundResource(R.drawable.ic_btn_red_rounded_normal);
        } else {
            txtWBNumber.setText(item.getWbNumber());
            txtNettWeight.setText(item.getNetto());
            txtSPBSNumber.setBackgroundResource(R.drawable.ic_btn_green_rounded_normal);
            llWB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickedListener.onItemClick(item);
                }
            });
        }
        txtSPBSNumber.setText(item.getSpbsNumber() == null ? "-" : item.getSpbsNumber());

        return convertView;
    }


    public void setOnWBClickListener(OnWBClickedListener clickedListener) {
        this.clickedListener = clickedListener;
    }

    public interface OnWBClickedListener {
        void onItemClick(SPBSWB spbswb);
    }

}
