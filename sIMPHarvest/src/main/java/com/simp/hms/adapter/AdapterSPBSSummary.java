package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSpbsSummaryItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.model.SPBSLine;
import com.simp.hms.model.SPBSSummary;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSPBSSummary extends BaseAdapter{
	private Activity activity;
	private List<SPBSSummary> listSpbsSummary, listTemp;
	private int layout;

	public AdapterSPBSSummary(Activity activity, List<SPBSSummary> listSpbsSummary, int layout){
		this.activity = activity;
		this.listSpbsSummary = listSpbsSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listSpbsSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return listSpbsSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		final SPBSSummary spbsSummary = listSpbsSummary.get(pos);
		
		ViewSpbsSummaryItem view;
		
		Button btnSpbsSummaryItemDelete;
		TextView txtSpbsSummaryItemBlock;
		TextView txtSpbsSummaryItemQtyJanjang;
		TextView txtSpbsSummaryItemQtyLooseFruit;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            btnSpbsSummaryItemDelete = (Button) convertView.findViewById(R.id.btnSpbsSummaryItemDelete);
            txtSpbsSummaryItemBlock = (TextView) convertView.findViewById(R.id.txtSpbsSummaryItemBlock);
            txtSpbsSummaryItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtSpbsSummaryItemQtyJanjang);
            txtSpbsSummaryItemQtyLooseFruit = (TextView) convertView.findViewById(R.id.txtSpbsSummaryItemQtyLooseFruit);
            
            view = new ViewSpbsSummaryItem(btnSpbsSummaryItemDelete, txtSpbsSummaryItemBlock, txtSpbsSummaryItemQtyJanjang, txtSpbsSummaryItemQtyLooseFruit);
            
            convertView.setTag(view);
        }else{
        	view = (ViewSpbsSummaryItem) convertView.getTag();
        	
        	btnSpbsSummaryItemDelete = view.getBtnSpbsSummaryItemDelete();
        	txtSpbsSummaryItemBlock = view.getTxtSpbsSummaryItemBlock();
        	txtSpbsSummaryItemQtyJanjang = view.getTxtSpbsSummaryItemQtyJanjang();
        	txtSpbsSummaryItemQtyLooseFruit = view.getTxtSpbsSummaryItemQtyLooseFruit();
        }

    	txtSpbsSummaryItemBlock.setText(spbsSummary.getBlock());
    	txtSpbsSummaryItemQtyJanjang.setText(String.valueOf( (int) spbsSummary.getQtyJanjang()));
		txtSpbsSummaryItemQtyLooseFruit.setText(String.valueOf(Utils.round(spbsSummary.getQtyLooseFruit(), 2)));

    	btnSpbsSummaryItemDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new DialogConfirm(activity, activity.getResources().getString(R.string.informasi), 
						String.format(activity.getResources().getString(R.string.data_delete_2), spbsSummary.getBlock()), 
						spbsSummary, R.id.btnSpbsSummaryItemDelete).show();
			}
		});

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listSpbsSummary = null;
				listSpbsSummary = (List<SPBSSummary>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SPBSSummary> listResult = new ArrayList<SPBSSummary>();
				SPBSSummary spbsSummary;
				
				if(listTemp == null){
					listTemp = new ArrayList<SPBSSummary>(listSpbsSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						spbsSummary = listTemp.get(i);
						
						if(spbsSummary.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(spbsSummary);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
	
	public void addOrUpdateData(SPBSSummary spbsSummary){
		boolean found = false;
		
		for(int i = 0; i < listSpbsSummary.size(); i++){
			if(spbsSummary.getBlock().equalsIgnoreCase(listSpbsSummary.get(i).getBlock())){
				found = true;
				updateData(i, spbsSummary);
				
				break;
			}
		}
		
		if(!found){
			listSpbsSummary.add(spbsSummary);
			notifyDataSetChanged();
		}
	}
	
	public void updateData(int pos, SPBSSummary spbsSummary){
		listSpbsSummary.set(pos, spbsSummary);
		
		notifyDataSetChanged();
	}
	
	public void clearData(){
		listSpbsSummary.clear();
		notifyDataSetChanged();
	}
	
	public void deleteData(SPBSSummary spbsSummary){
		for(int i = 0; i < listSpbsSummary.size(); i++){
			SPBSSummary spbsSummaryEx = listSpbsSummary.get(i);
			if(spbsSummaryEx.getBlock().equalsIgnoreCase(spbsSummary.getBlock())){
				listSpbsSummary.remove(i);
				
				notifyDataSetChanged();
				break;
			}
		}
	}
}
