package com.simp.hms.adapter.holder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.activity.spbs.SPBSActivity;
import com.simp.hms.activity.spbs.SPBSDatecsActivity;
import com.simp.hms.activity.spbs.SPBSWoosimActivity;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.model.Bluetooth;
import com.simp.hms.model.Menu;
import com.simp.hms.routines.Utils;

/**
 * Created by Anka.Wirawan on 8/3/2016.
 */
public class MainMenuViewHolder extends RecyclerView.ViewHolder {

    private ImageView imgItemMainMenuIcon;
    private TextView txtItemMainMenuCaption;

    private Menu menu;

    public MainMenuViewHolder(final View itemView) {
        super(itemView);

        imgItemMainMenuIcon = (ImageView) itemView.findViewById(R.id.imgItemMainMenuIcon);
        txtItemMainMenuCaption = (TextView) itemView.findViewById(R.id.txtItemMainMenuCaption);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menu != null){
                    String activity = menu.getActivity();

                    if(activity.equalsIgnoreCase("com.simp.hms.activity.spbs.SPBSActivity")){
                        DatabaseHandler database = new DatabaseHandler(itemView.getContext());

                        database.openTransaction();
                        Bluetooth bluetooth = (Bluetooth) database.getDataFirst(false, Bluetooth.TABLE_NAME, null, null, null, null, null, null, null);
                        database.closeTransaction();

                        if(bluetooth != null) {
                            if (bluetooth.getName().equalsIgnoreCase("woosim")) {
                                activity = "com.simp.hms.activity.spbs.SPBSWoosimActivity";
                            } else if (bluetooth.getName().equalsIgnoreCase("DPP-350")) {
                                activity = "com.simp.hms.activity.spbs.SPBSDatecsActivity";
                            }
                        }
                    }

                    Class c = Utils.convertStringToClass(activity);

                    if(c != null) {
                        Intent intent = new Intent(itemView.getContext(), c);
                        itemView.getContext().startActivity(intent);
                    }
                }
            }
        });
    }

    public void bindData(Context context, Menu menu){
        this.menu = menu;

        int img = Utils.getId(menu.getIcon(), R.drawable.class);
        imgItemMainMenuIcon.setImageResource(img);
        int caption = Utils.getId(menu.getCaption(), R.string.class);
        txtItemMainMenuCaption.setText(context.getString(caption));
    }
}