package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewVehicleItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.model.RunningAccount;
import com.simp.hms.model.Vendor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterVehicle extends BaseAdapter{
	private Context context;
	private List<RunningAccount> listVehicle, listTemp;
	private int layout;

	public AdapterVehicle(Context context, List<RunningAccount> listAssistantDivision, int layout){
		this.context = context;
		this.listVehicle = listAssistantDivision;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listVehicle.size();
	}

	@Override
	public Object getItem(int pos) {
		return listVehicle.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		RunningAccount item = listVehicle.get(pos);
		
		ViewVehicleItem view;
		
		TextView txtVehicleCompanyCode;
		TextView txtVehicleEstate;
		TextView txtVehicleRunningAccount;
		TextView txtVehicleLicensePlate;
		TextView txtVehicleVendor;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtVehicleCompanyCode = (TextView) convertView.findViewById(R.id.txtVehicleCompanyCode);
            txtVehicleEstate = (TextView) convertView.findViewById(R.id.txtVehicleEstate);
            txtVehicleRunningAccount = (TextView) convertView.findViewById(R.id.txtVehicleRunningAccount);
            txtVehicleLicensePlate = (TextView) convertView.findViewById(R.id.txtVehicleLicensePlate);
			txtVehicleVendor = (TextView) convertView.findViewById(R.id.txtVehicleVendor);
            
            view = new ViewVehicleItem(txtVehicleCompanyCode, txtVehicleEstate, 
            		txtVehicleRunningAccount, txtVehicleLicensePlate, txtVehicleVendor);
            
            convertView.setTag(view);
        }else{
        	view = (ViewVehicleItem) convertView.getTag();
        	
        	txtVehicleCompanyCode = view.getTxtVehicleCompanyCode();
        	txtVehicleEstate = view.getTxtVehicleEstate();
        	txtVehicleRunningAccount = view.getTxtVehicleRunningAccount();
        	txtVehicleLicensePlate = view.getTxtVehicleLicensePlate();
			txtVehicleVendor = view.getTxtVehicleVendor();
        }

    	txtVehicleCompanyCode.setText(item.getCompanyCode());
    	txtVehicleEstate.setText(item.getEstate());
    	txtVehicleRunningAccount.setText(item.getRunningAccount());
    	txtVehicleLicensePlate.setText(item.getLicensePlate());

		String vendor = getVendor(item.getEstate(), item.getLifnr());

		txtVehicleVendor.setText(vendor);
    	
    	return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listVehicle = null;
				listVehicle = (List<RunningAccount>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<RunningAccount> listResult = new ArrayList<RunningAccount>();
				RunningAccount vehicle;
				
				if(listTemp == null){
					listTemp = new ArrayList<RunningAccount>(listVehicle);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						vehicle = listTemp.get(i);
						
						if(vehicle.getLicensePlate().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(vehicle);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}

	private String getVendor(String estate, String lifnr){
		DatabaseHandler database = new DatabaseHandler(context);
		String name = "";

		try{
			database.openTransaction();
			Vendor vendor = (Vendor) database.getDataFirst(false, Vendor.TABLE_NAME, null,
					Vendor.XML_ESTATE + " = ? " + " and " +
					Vendor.XML_LIFNR + " = ?",
					new String[] {estate, lifnr},
					null, null, null, null);

			if(vendor != null) name = vendor.getName();

		}catch (Exception e){
			e.printStackTrace();
		}finally {
			database.closeTransaction();
		}

		return name;
	}
}
