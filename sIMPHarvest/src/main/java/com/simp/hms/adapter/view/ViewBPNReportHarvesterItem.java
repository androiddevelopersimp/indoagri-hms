package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNReportHarvesterItem {
	private TextView txtBpnReportHarvesterItemHarvesterName;
	private TextView txtBpnReportHarvesterItemQtyJanjang;
	private TextView txtBpnReportHarvesterItemQtyLooseFruit;
	private TextView txtBpnReportHarvesterItemQtyMentah;
	private TextView txtBpnReportHarvesterItemQtyBusuk;
	private TextView txtBpnReportHarvesterItemQtyTangkaiPanjang;

	public ViewBPNReportHarvesterItem(
			TextView txtBpnReportHarvesterItemHarvesterName,
			TextView txtBpnReportHarvesterItemQtyJanjang,
			TextView txtBpnReportHarvesterItemQtyLooseFruit,
			TextView txtBpnReportHarvesterItemQtyMentah,
			TextView txtBpnReportHarvesterItemQtyBusuk,
			TextView txtBpnReportHarvesterItemQtyTangkaiPanjang) {
		super();
		this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
		this.txtBpnReportHarvesterItemQtyJanjang = txtBpnReportHarvesterItemQtyJanjang;
		this.txtBpnReportHarvesterItemQtyLooseFruit = txtBpnReportHarvesterItemQtyLooseFruit;
		this.txtBpnReportHarvesterItemQtyMentah = txtBpnReportHarvesterItemQtyMentah;
		this.txtBpnReportHarvesterItemQtyBusuk = txtBpnReportHarvesterItemQtyBusuk;
		this.txtBpnReportHarvesterItemQtyTangkaiPanjang = txtBpnReportHarvesterItemQtyTangkaiPanjang;
	}

	public TextView getTxtBpnReportHarvesterItemHarvesterName() {
		return txtBpnReportHarvesterItemHarvesterName;
	}

	public void setTxtBpnReportHarvesterItemHarvesterName(
			TextView txtBpnReportHarvesterItemHarvesterName) {
		this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
	}

	public TextView getTxtBpnReportHarvesterItemQtyJanjang() {
		return txtBpnReportHarvesterItemQtyJanjang;
	}

	public void setTxtBpnReportHarvesterItemQtyJanjang(
			TextView txtBpnReportHarvesterItemQtyJanjang) {
		this.txtBpnReportHarvesterItemQtyJanjang = txtBpnReportHarvesterItemQtyJanjang;
	}

	public TextView getTxtBpnReportHarvesterItemQtyLooseFruit() {
		return txtBpnReportHarvesterItemQtyLooseFruit;
	}

	public void setTxtBpnReportHarvesterItemQtyLooseFruit(
			TextView txtBpnReportHarvesterItemQtyLooseFruit) {
		this.txtBpnReportHarvesterItemQtyLooseFruit = txtBpnReportHarvesterItemQtyLooseFruit;
	}

	public TextView getTxtBpnReportHarvesterItemQtyMentah() {
		return txtBpnReportHarvesterItemQtyMentah;
	}

	public void setTxtBpnReportHarvesterItemQtyMentah(
			TextView txtBpnReportHarvesterItemQtyMentah) {
		this.txtBpnReportHarvesterItemQtyMentah = txtBpnReportHarvesterItemQtyMentah;
	}

	public TextView getTxtBpnReportHarvesterItemQtyBusuk() {
		return txtBpnReportHarvesterItemQtyBusuk;
	}

	public void setTxtBpnReportHarvesterItemQtyBusuk(
			TextView txtBpnReportHarvesterItemQtyBusuk) {
		this.txtBpnReportHarvesterItemQtyBusuk = txtBpnReportHarvesterItemQtyBusuk;
	}

	public TextView getTxtBpnReportHarvesterItemQtyTangkaiPanjang() {
		return txtBpnReportHarvesterItemQtyTangkaiPanjang;
	}

	public void setTxtBpnReportHarvesterItemQtyTangkaiPanjang(
			TextView txtBpnReportHarvesterItemQtyTangkaiPanjang) {
		this.txtBpnReportHarvesterItemQtyTangkaiPanjang = txtBpnReportHarvesterItemQtyTangkaiPanjang;
	}

}
