package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewGangItem {
	private TextView txt_gang_item_code;

	public ViewGangItem(TextView txt_gang_item_code) {
		this.txt_gang_item_code = txt_gang_item_code;
	}

	public TextView getTxtGangItemCode() {
		return txt_gang_item_code;
	}

	public void setTxtGangItemCode(TextView txt_gang_item_code) {
		this.txt_gang_item_code = txt_gang_item_code;
	}
	
	
}
