package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewDivisionItem;
import com.simp.hms.model.Division;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterDivision extends BaseAdapter{
	private Context context;
	private List<Division> lstDivision, lstTemp;
	private int layout;
	
	public AdapterDivision(Context context, List<Division> lstDivision, int layout){
		this.context = context;
		this.lstDivision = lstDivision;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstDivision.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstDivision.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		Division division = lstDivision.get(pos);
		
		ViewDivisionItem view;
		TextView txtDivisionItemDivision;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
        	txtDivisionItemDivision = (TextView) convertView.findViewById(R.id.txtDivisionItemDivision);
            
            view = new ViewDivisionItem(txtDivisionItemDivision);    
            convertView.setTag(view);
        }else{
        	view = (ViewDivisionItem) convertView.getTag();
        	
        	txtDivisionItemDivision = view.getTxtDivisionItemDivision();
        }

    	txtDivisionItemDivision.setText(String.valueOf(division.getCode()));

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstDivision = null;
				lstDivision = (List<Division>) results.values;
				notifyDataSetChanged();
			}
			
			@Override    
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<Division> list_result = new ArrayList<Division>();
				Division gang;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<Division>(lstDivision);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						gang = lstTemp.get(i);
						
						if(gang.getCode().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(gang);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
