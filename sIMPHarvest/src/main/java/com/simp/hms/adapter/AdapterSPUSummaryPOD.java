package com.simp.hms.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.TextView;


import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSpuSummaryItemPODS;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.model.SPUSummary;
import com.simp.hms.model.SPUSummaryPODS;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPUSummaryPOD extends BaseAdapter {
    private Activity activity;
    private List<SPUSummaryPODS> listSpuSummary, listTemp;
    private int layout;

    public AdapterSPUSummaryPOD(Activity activity, List<SPUSummaryPODS> listSpuSummary, int layout){
        this.activity = activity;
        this.listSpuSummary = listSpuSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return listSpuSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return listSpuSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        final SPUSummaryPODS spuSummary = listSpuSummary.get(pos);

        ViewSpuSummaryItemPODS view;

        Button btnSpuSummaryItemDelete;
        TextView txtSpuSummaryItemBlock;
        TextView txtSpuSummaryPODS;
        TextView txtSpuSummaryItemQtyGoodWeight;
        TextView txtSpuSummaryItemQtyBadWeight;
        TextView txtSpuSummaryItemQtyPoorWeight;
        TextView txtSpuSummaryItemQtyTotalWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            btnSpuSummaryItemDelete = (Button) convertView.findViewById(R.id.btnSpuSummaryItemDelete);
            txtSpuSummaryItemBlock = (TextView) convertView.findViewById(R.id.txtSpuSummaryItemBlock);
            txtSpuSummaryPODS = (TextView) convertView.findViewById(R.id.txtSpuSummaryItemQtyPODS);
            txtSpuSummaryItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtSpuSummaryItemQtyGoodWeight);
            txtSpuSummaryItemQtyBadWeight = (TextView) convertView.findViewById(R.id.txtSpuSummaryItemQtyBadWeight);
            txtSpuSummaryItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtSpuSummaryItemQtyPoorWeight);
            txtSpuSummaryItemQtyTotalWeight = (TextView) convertView.findViewById(R.id.txtSpuSummaryItemQtyTotalWeight);

            view = new ViewSpuSummaryItemPODS(btnSpuSummaryItemDelete, txtSpuSummaryItemBlock,txtSpuSummaryPODS,
                                            txtSpuSummaryItemQtyGoodWeight, txtSpuSummaryItemQtyBadWeight,txtSpuSummaryItemQtyPoorWeight, txtSpuSummaryItemQtyTotalWeight);

            convertView.setTag(view);
        }else{
            view = (ViewSpuSummaryItemPODS) convertView.getTag();
            btnSpuSummaryItemDelete = view.getBtnSpuSummaryItemDelete();
            txtSpuSummaryItemBlock = view.getTxtSpuSummaryItemBlock();
            txtSpuSummaryPODS = view.getTxtSpuSummaryItemQtyPods();
            txtSpuSummaryItemQtyGoodWeight = view.getTxtSpuSummaryItemQtyGoodWeight();
            txtSpuSummaryItemQtyBadWeight = view.getTxtSpuSummaryItemQtyBadWeight();
            txtSpuSummaryItemQtyPoorWeight = view.getTxtSpuSummaryItemQtyPoorWeight();
            txtSpuSummaryItemQtyTotalWeight = view.getTxtSpuSummaryItemQtyTotalWeight();
        }

        txtSpuSummaryItemBlock.setText(spuSummary.getBlock());
        txtSpuSummaryPODS.setText(String.valueOf( (int) spuSummary.getQtyPODS()));
        txtSpuSummaryItemQtyGoodWeight.setText(String.valueOf( (int) spuSummary.getQtyGoodWeight()));
        txtSpuSummaryItemQtyBadWeight.setText(String.valueOf( (int) spuSummary.getQtyBadWeight()));
        txtSpuSummaryItemQtyPoorWeight.setText(String.valueOf( (int) spuSummary.getQtyPoorWeight()));
        txtSpuSummaryItemQtyTotalWeight.setText(String.valueOf( (int) spuSummary.getQtyTotalWeight()));

        btnSpuSummaryItemDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DialogConfirm(activity, activity.getResources().getString(R.string.informasi),
                        String.format(activity.getResources().getString(R.string.data_delete_2), spuSummary.getBlock()),
                        spuSummary, R.id.btnSpuSummaryItemDelete).show();
            }
        });

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listSpuSummary = null;
                listSpuSummary = (List<SPUSummaryPODS>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPUSummaryPODS> listResult = new ArrayList<SPUSummaryPODS>();
                SPUSummaryPODS spuSummary;

                if(listTemp == null){
                    listTemp = new ArrayList<SPUSummaryPODS>(listSpuSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = listTemp.size();
                    result.values = listTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < listTemp.size(); i++){
                        spuSummary = listTemp.get(i);

                        if(spuSummary.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
                            listResult.add(spuSummary);
                        }
                    }
                    result.count = listResult.size();
                    result.values = listResult;
                }

                return result;
            }
        };
        return filter;
    }

    public void addOrUpdateData(SPUSummaryPODS spuSummary){
        boolean found = false;

        for(int i = 0; i < listSpuSummary.size(); i++){
            if(spuSummary.getBlock().equalsIgnoreCase(listSpuSummary.get(i).getBlock())){
                found = true;
                updateData(i, spuSummary);

                break;
            }
        }

        if(!found){
            listSpuSummary.add(spuSummary);
            notifyDataSetChanged();
        }
    }

    public void updateData(int pos, SPUSummaryPODS spuSummary){
        listSpuSummary.set(pos, spuSummary);

        notifyDataSetChanged();
    }

    public void clearData(){
        listSpuSummary.clear();
        notifyDataSetChanged();
    }

    public void deleteData(SPUSummaryPODS spuSummary){
        for(int i = 0; i < listSpuSummary.size(); i++){
            SPUSummaryPODS spuSummaryEx = listSpuSummary.get(i);
            if(spuSummaryEx.getBlock().equalsIgnoreCase(spuSummary.getBlock())){
                listSpuSummary.remove(i);

                notifyDataSetChanged();
                break;
            }
        }
    }
}
