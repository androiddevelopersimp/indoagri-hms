package com.simp.hms.adapter;

import com.simp.hms.fragment.SPTA1Fragment;
import com.simp.hms.fragment.SPTA2Fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdapterSPTAFragment extends FragmentStatePagerAdapter {

	private String sptaNumber;
	private String companyCode;
	private String estate;
	private String divisi;
	
	public AdapterSPTAFragment(FragmentManager fm) {
		super(fm);
	}
	
	public AdapterSPTAFragment(FragmentManager fm, String sptaNumber, String companyCode, String estate, String divisi){
		super(fm);
		
		this.sptaNumber = sptaNumber;
		this.companyCode = companyCode;
		this.estate = estate;
		this.divisi = divisi;
	}

	@Override
	public Fragment getItem(int position) {

		switch (position) {
			case 0:
				return SPTA1Fragment.newInstance(sptaNumber, companyCode, estate, divisi);
			case 1:
				return SPTA2Fragment.newInstance(sptaNumber, companyCode, estate, divisi);
			default:
				return null;
		}
	}

	@Override
	public int getCount() {
		return 2;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {

		switch (position) {
			case 0:
				return "SPTA 1";
			case 1:
				return "SPTA 2";
			default:
				return "";
		}
	}

}
