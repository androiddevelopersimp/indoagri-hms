package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBlksbcDetailItem {
	private TextView txt_blksbc_detail_item_id;
	private TextView txt_blksbc_detail_item_company_code;
	private TextView txt_blksbc_detail_item_estate;
	private TextView txt_blksbc_detail_item_block;
	private TextView txt_blksbc_detail_item_valid_from;
	private TextView txt_blksbc_detail_item_min_value;
	private TextView txt_blksbc_detail_item_max_value;
	private TextView txt_blksbc_detail_item_over_basic_rate;
	
	public ViewBlksbcDetailItem(TextView txt_blksbc_detail_item_id,
			TextView txt_blksbc_detail_item_company_code,
			TextView txt_blksbc_detail_item_estate,
			TextView txt_blksbc_detail_item_block,
			TextView txt_blksbc_detail_item_valid_from,
			TextView txt_blksbc_detail_item_min_value,
			TextView txt_blksbc_detail_item_max_value,
			TextView txt_blksbc_detail_item_over_basic_rate) {
		this.txt_blksbc_detail_item_id = txt_blksbc_detail_item_id;
		this.txt_blksbc_detail_item_company_code = txt_blksbc_detail_item_company_code;
		this.txt_blksbc_detail_item_estate = txt_blksbc_detail_item_estate;
		this.txt_blksbc_detail_item_block = txt_blksbc_detail_item_block;
		this.txt_blksbc_detail_item_valid_from = txt_blksbc_detail_item_valid_from;
		this.txt_blksbc_detail_item_min_value = txt_blksbc_detail_item_min_value;
		this.txt_blksbc_detail_item_max_value = txt_blksbc_detail_item_max_value;
		this.txt_blksbc_detail_item_over_basic_rate = txt_blksbc_detail_item_over_basic_rate;
	}

	public TextView getTxtBlksbcDetailItemId() {
		return txt_blksbc_detail_item_id;
	}

	public void setTxtBlksbcDetailItemId(TextView txt_blksbc_detail_item_id) {
		this.txt_blksbc_detail_item_id = txt_blksbc_detail_item_id;
	}

	public TextView getTxtBlksbcDetailItemCompanyCode() {
		return txt_blksbc_detail_item_company_code;
	}

	public void setTxtBlksbcDetailItemCompanyCode(
			TextView txt_blksbc_detail_item_company_code) {
		this.txt_blksbc_detail_item_company_code = txt_blksbc_detail_item_company_code;
	}

	public TextView getTxtBlksbcDetailItemEstate() {
		return txt_blksbc_detail_item_estate;
	}

	public void setTxtBlksbcDetailItemEstate(
			TextView txt_blksbc_detail_item_estate) {
		this.txt_blksbc_detail_item_estate = txt_blksbc_detail_item_estate;
	}

	public TextView getTxtBlksbcDetailItemBlock() {
		return txt_blksbc_detail_item_block;
	}

	public void setTxtBlksbcDetailItemBlock(
			TextView txt_blksbc_detail_item_block) {
		this.txt_blksbc_detail_item_block = txt_blksbc_detail_item_block;
	}

	public TextView getTxtBlksbcDetailItemValidFrom() {
		return txt_blksbc_detail_item_valid_from;
	}

	public void setTxtBlksbcDetailItemValidFrom(
			TextView txt_blksbc_detail_item_valid_from) {
		this.txt_blksbc_detail_item_valid_from = txt_blksbc_detail_item_valid_from;
	}

	public TextView getTxtBlksbcDetailItemMinValue() {
		return txt_blksbc_detail_item_min_value;
	}

	public void setTxtBlksbcDetailItemMinValue(
			TextView txt_blksbc_detail_item_min_value) {
		this.txt_blksbc_detail_item_min_value = txt_blksbc_detail_item_min_value;
	}

	public TextView getTxtBlksbcDetailItemMaxValue() {
		return txt_blksbc_detail_item_max_value;
	}

	public void setTxtBlksbcDetailItemMaxValue(
			TextView txt_blksbc_detail_item_max_value) {
		this.txt_blksbc_detail_item_max_value = txt_blksbc_detail_item_max_value;
	}

	public TextView getTxtBlksbcDetailItemOverBasicRate() {
		return txt_blksbc_detail_item_over_basic_rate;
	}

	public void setTxtBlksbcDetailItemOverBasicRate(
			TextView txt_blksbc_detail_item_over_basic_rate) {
		this.txt_blksbc_detail_item_over_basic_rate = txt_blksbc_detail_item_over_basic_rate;
	}
}
