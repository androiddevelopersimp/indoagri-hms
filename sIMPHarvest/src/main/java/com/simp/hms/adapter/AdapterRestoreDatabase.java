package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewAbsentTypeItem;
import com.simp.hms.adapter.view.ViewRestoreDatabase;
import com.simp.hms.model.AbsentType;
import com.simp.hms.model.RestoreDatabase;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterRestoreDatabase extends BaseAdapter{
	private Context context;
	private List<RestoreDatabase> lstRestoreDatabase, listTemp;
	private int layout;

	public AdapterRestoreDatabase(Context context, List<RestoreDatabase> lstRestoreDatabase, int layout){
		this.context = context;
		this.lstRestoreDatabase = lstRestoreDatabase;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstRestoreDatabase.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstRestoreDatabase.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		RestoreDatabase restoreDatabase = lstRestoreDatabase.get(pos);
		
		ViewRestoreDatabase view;
		
		TextView txtRestoreDatabaseItemName;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtRestoreDatabaseItemName = (TextView) convertView.findViewById(R.id.txtRestoreDatabaseItemName);

            
            view = new ViewRestoreDatabase(txtRestoreDatabaseItemName);
            
            convertView.setTag(view);
        }else{
        	view = (ViewRestoreDatabase) convertView.getTag();
        	
        	txtRestoreDatabaseItemName = view.getTxtRestoreDatabaseItemName();
        }

    	txtRestoreDatabaseItemName.setText(restoreDatabase.getFileName());

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstRestoreDatabase = null;
				lstRestoreDatabase = (List<RestoreDatabase>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<RestoreDatabase> listResult = new ArrayList<RestoreDatabase>();
				RestoreDatabase restoreDatabase;
				
				if(listTemp == null){
					listTemp = new ArrayList<RestoreDatabase>(lstRestoreDatabase);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						restoreDatabase = listTemp.get(i);
						
						if(restoreDatabase.getFileName().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(restoreDatabase);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
