package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewTaksasiHistoryItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.TaksasiHeader;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterTaksasiHistory extends BaseAdapter{
	private Activity activity;
	private List<TaksasiHeader> listItems, listTemp;
	private int layout;
	
	
	public AdapterTaksasiHistory(Activity activity, List<TaksasiHeader> listItems, int layout){
		this.activity = activity;
		this.listItems = listItems;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public Object getItem(int pos) {
		return listItems.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		TaksasiHeader item = listItems.get(pos);
		
		ViewTaksasiHistoryItem view;
		TextView txtTaksasiHistoryItemTaksasiDate;
		TextView txtTaksasiHistoryItemBlock;
		TextView txtTaksasiHistoryItemForeman;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtTaksasiHistoryItemTaksasiDate = (TextView) convertView.findViewById(R.id.txtTaksasiHistoryItemTaksasiDate);
            txtTaksasiHistoryItemBlock = (TextView) convertView.findViewById(R.id.txtTaksasiHistoryItemBlock);
            txtTaksasiHistoryItemForeman = (TextView) convertView.findViewById(R.id.txtTaksasiHistoryItemForeman);
            
            view = new ViewTaksasiHistoryItem(txtTaksasiHistoryItemTaksasiDate, txtTaksasiHistoryItemBlock, txtTaksasiHistoryItemForeman);
            
            convertView.setTag(view);
        }else{
        	view = (ViewTaksasiHistoryItem) convertView.getTag();
        	
        	txtTaksasiHistoryItemTaksasiDate = view.getTxtTaksasiHistoryItemTaksasiDate();
        	txtTaksasiHistoryItemBlock = view.getTxtTaksasiHistoryItemBlock();
        	txtTaksasiHistoryItemForeman = view.getTxtTaksasiHistoryItemForeman();
        }

        txtTaksasiHistoryItemTaksasiDate.setText(new DateLocal(item.getTaksasiDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
        txtTaksasiHistoryItemBlock.setText(item.getBlock());
        txtTaksasiHistoryItemForeman.setText(item.getForeman());

        return convertView;
	}
	
	public void deleteData(int pos){
		TaksasiHeader taksasiHeader = listItems.get(pos);
		
		String imei = taksasiHeader.getImei();
		String companyCode = taksasiHeader.getCompanyCode();
		String estate = taksasiHeader.getEstate();
		String division = taksasiHeader.getDivision();
		String taksasiDate = taksasiHeader.getTaksasiDate();
		String block = taksasiHeader.getBlock();
		String crop = taksasiHeader.getCrop();
		int status = taksasiHeader.getStatus();
		
		if(status == 0){
			DatabaseHandler database = new DatabaseHandler(activity);
			
			try{
				database.openTransaction();
				
//				database.deleteData(TaksasiHeader.TABLE_NAME, 
//						TaksasiHeader.XML_IMEI + "=?" + " and " +
//						TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
//						TaksasiHeader.XML_ESTATE + "=?" + " and " +
//						TaksasiHeader.XML_DIVISION + "=?" + " and " +
//						TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
//						TaksasiHeader.XML_BLOCK + "=?" + " and " +
//						TaksasiHeader.XML_CROP + "=?", 
//						new String [] {imei, companyCode, estate, division, taksasiDate, block, crop});
				
				database.deleteData(TaksasiHeader.TABLE_NAME, 
						TaksasiHeader.XML_COMPANY_CODE + "=?" + " and " +
						TaksasiHeader.XML_ESTATE + "=?" + " and " +
						TaksasiHeader.XML_DIVISION + "=?" + " and " +
						TaksasiHeader.XML_TAKSASI_DATE + "=?" + " and " +
						TaksasiHeader.XML_BLOCK + "=?" + " and " +
						TaksasiHeader.XML_CROP + "=?", 
						new String [] {companyCode, estate, division, taksasiDate, block, crop});
				
				database.commitTransaction();
			}catch(SQLiteException e){
				e.printStackTrace();
				database.closeTransaction();
			}finally{
				database.closeTransaction();
			}
		}else{
			new DialogNotification(activity, activity.getResources().getString(R.string.informasi), 
					activity.getResources().getString(R.string.data_already_export), false).show();
		}
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listItems = null;
				listItems = (List<TaksasiHeader>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<TaksasiHeader> listResult = new ArrayList<TaksasiHeader>();
				TaksasiHeader taksasiHeader;
				
				if(listTemp == null){
					listTemp = new ArrayList<TaksasiHeader>(listItems);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						taksasiHeader = listTemp.get(i);
						
						if(taksasiHeader.getTaksasiDate().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(taksasiHeader);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
