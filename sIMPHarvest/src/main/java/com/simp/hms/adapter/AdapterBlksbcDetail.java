package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBlksbcDetailItem;
import com.simp.hms.model.BLKSBCDetail;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBlksbcDetail extends BaseAdapter{
	private Context context;
	private List<BLKSBCDetail> lst_blksbc_detail, lst_temp;
	private int layout;
	
	private ViewBlksbcDetailItem vie_blksbc_detail;
	private TextView txt_blksbc_detail_item_id;
	private TextView txt_blksbc_detail_item_company_code;
	private TextView txt_blksbc_detail_item_estate;
	private TextView txt_blksbc_detail_item_block;
	private TextView txt_blksbc_detail_item_valid_from;
	private TextView txt_blksbc_detail_item_min_value;
	private TextView txt_blksbc_detail_item_max_value;
	private TextView txt_blksbc_detail_item_over_basic_rate;
	
	public AdapterBlksbcDetail(Context context, List<BLKSBCDetail> lst_blksbc_detail, int layout){
		this.context = context;
		this.lst_blksbc_detail = lst_blksbc_detail;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_blksbc_detail.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_blksbc_detail.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BLKSBCDetail blksbc_detail = lst_blksbc_detail.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
        	txt_blksbc_detail_item_id = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_id);
        	txt_blksbc_detail_item_company_code = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_company_code);
        	txt_blksbc_detail_item_estate = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_estate);
        	txt_blksbc_detail_item_block = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_block);
        	txt_blksbc_detail_item_valid_from = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_valid_from);
        	txt_blksbc_detail_item_min_value = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_min_value);
        	txt_blksbc_detail_item_max_value = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_max_value);
        	txt_blksbc_detail_item_over_basic_rate = (TextView) convertView.findViewById(R.id.txt_blksbc_detail_item_over_basic_rate);
            
            vie_blksbc_detail = new ViewBlksbcDetailItem(txt_blksbc_detail_item_id, txt_blksbc_detail_item_company_code, 
            		txt_blksbc_detail_item_estate, txt_blksbc_detail_item_block, txt_blksbc_detail_item_valid_from, txt_blksbc_detail_item_min_value, 
            		txt_blksbc_detail_item_max_value, txt_blksbc_detail_item_over_basic_rate);
            
            convertView.setTag(vie_blksbc_detail);
        }else{
        	vie_blksbc_detail = (ViewBlksbcDetailItem) convertView.getTag();
        	
        	txt_blksbc_detail_item_id = vie_blksbc_detail.getTxtBlksbcDetailItemId();
        	txt_blksbc_detail_item_company_code = vie_blksbc_detail.getTxtBlksbcDetailItemCompanyCode();
        	txt_blksbc_detail_item_estate = vie_blksbc_detail.getTxtBlksbcDetailItemEstate();
        	txt_blksbc_detail_item_block = vie_blksbc_detail.getTxtBlksbcDetailItemBlock();
        	txt_blksbc_detail_item_valid_from = vie_blksbc_detail.getTxtBlksbcDetailItemValidFrom();
        	txt_blksbc_detail_item_min_value = vie_blksbc_detail.getTxtBlksbcDetailItemMinValue();
        	txt_blksbc_detail_item_max_value = vie_blksbc_detail.getTxtBlksbcDetailItemMaxValue();
        	txt_blksbc_detail_item_over_basic_rate = vie_blksbc_detail.getTxtBlksbcDetailItemOverBasicRate();
        }

    	txt_blksbc_detail_item_id.setText(String.valueOf(blksbc_detail.getId()));
    	txt_blksbc_detail_item_company_code.setText(blksbc_detail.getCompanyCode());
    	txt_blksbc_detail_item_estate.setText(blksbc_detail.getEstate());
    	txt_blksbc_detail_item_block.setText(blksbc_detail.getBlock());
    	txt_blksbc_detail_item_valid_from.setText(blksbc_detail.getValidFrom());
    	txt_blksbc_detail_item_min_value.setText(String.valueOf(blksbc_detail.getMinimumValue()));
    	txt_blksbc_detail_item_max_value.setText(String.valueOf(blksbc_detail.getMaximumValue()));
    	txt_blksbc_detail_item_over_basic_rate.setText(String.valueOf(blksbc_detail.getOverBasicRate()));

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lst_blksbc_detail = null;
				lst_blksbc_detail = (List<BLKSBCDetail>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BLKSBCDetail> list_result = new ArrayList<BLKSBCDetail>();
				BLKSBCDetail blksbc_detail;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<BLKSBCDetail>(lst_blksbc_detail);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						blksbc_detail = lst_temp.get(i);
						
						if(blksbc_detail.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(blksbc_detail);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
