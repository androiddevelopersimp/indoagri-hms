package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPUReportBlockItemPOD {
    private TextView txtSpuReportBlockItemBlock;
    private TextView txtSpuReportBlockItemQtyPODQty;
    private TextView txtSpuReportBlockItemQtyGoodWeight;
    private TextView txtSpuReportBlockItemQtyEstimasiQty;
    private TextView txtSpuReportBlockItemQtyPoorWeight;

    public ViewSPUReportBlockItemPOD(TextView txtSpuReportBlockItemBlock,
                                     TextView txtSpuReportBlockItemQtyPODQty,
                                     TextView txtSpuReportBlockItemQtyGoodWeight,
                                     TextView txtSpuReportBlockItemQtyEstimasiQty,
                                     TextView txtSpuReportBlockItemQtyPoorWeight) {
        super();
        this.txtSpuReportBlockItemBlock = txtSpuReportBlockItemBlock;
        this.txtSpuReportBlockItemQtyPODQty = txtSpuReportBlockItemQtyPODQty;
        this.txtSpuReportBlockItemQtyGoodWeight = txtSpuReportBlockItemQtyGoodWeight;
        this.txtSpuReportBlockItemQtyEstimasiQty = txtSpuReportBlockItemQtyEstimasiQty;
        this.txtSpuReportBlockItemQtyPoorWeight = txtSpuReportBlockItemQtyPoorWeight;
    }

    public TextView getTxtSpuReportBlockItemBlock() {
        return txtSpuReportBlockItemBlock;
    }

    public void setTxtSpuReportBlockItemBlock(TextView txtSpuReportBlockItemBlock) {
        this.txtSpuReportBlockItemBlock = txtSpuReportBlockItemBlock;
    }

    public TextView getTxtSpuReportBlockItemQtyPODQty() {
        return txtSpuReportBlockItemQtyPODQty;
    }

    public void setTxtSpuReportBlockItemQtyPODQty(TextView txtSpuReportBlockItemQtyPODQty) {
        this.txtSpuReportBlockItemQtyPODQty = txtSpuReportBlockItemQtyPODQty;
    }

    public TextView getTxtSpuReportBlockItemQtyGoodWeight() {
        return txtSpuReportBlockItemQtyGoodWeight;
    }

    public void setTxtSpuReportBlockItemQtyGoodWeight(TextView txtSpuReportBlockItemQtyGoodWeight) {
        this.txtSpuReportBlockItemQtyGoodWeight = txtSpuReportBlockItemQtyGoodWeight;
    }

    public TextView getTxtSpuReportBlockItemQtyEstimasiQty() {
        return txtSpuReportBlockItemQtyEstimasiQty;
    }

    public void setTxtSpuReportBlockItemQtyEstimasiQty(TextView txtSpuReportBlockItemQtyEstimasiQty) {
        this.txtSpuReportBlockItemQtyEstimasiQty = txtSpuReportBlockItemQtyEstimasiQty;
    }

    public TextView getTxtSpuReportBlockItemQtyPoorWeight() {
        return txtSpuReportBlockItemQtyPoorWeight;
    }

    public void setTxtSpuReportBlockItemQtyPoorWeight(TextView txtSpuReportBlockItemQtyPoorWeight) {
        this.txtSpuReportBlockItemQtyPoorWeight = txtSpuReportBlockItemQtyPoorWeight;
    }
}
