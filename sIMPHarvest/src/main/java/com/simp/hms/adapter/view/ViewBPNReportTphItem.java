package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNReportTphItem {
	private TextView txtBpnReportTphItemTph;
	private TextView txtBpnReportTphItemBpnDate;
	private TextView txtBpnReportTphItemQtyJanjang;
	private TextView txtBpnReportTphItemQtyLooseFruit;
	private TextView txtBpnReportTphItemQtyMentah;
	private TextView txtBpnReportTphItemQtyBusuk;
	private TextView txtBpnReportTphItemQtyTangkaiPanjang;

	public ViewBPNReportTphItem(TextView txtBpnReportTphItemTph,
			TextView txtBpnReportTphItemBpnDate,
			TextView txtBpnReportTphItemQtyJanjang,
			TextView txtBpnReportTphItemQtyLooseFruit,
			TextView txtBpnReportTphItemQtyMentah,
			TextView txtBpnReportTphItemQtyBusuk,
			TextView txtBpnReportTphItemQtyTangkaiPanjang) {
		super();
		this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
		this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
		this.txtBpnReportTphItemQtyJanjang = txtBpnReportTphItemQtyJanjang;
		this.txtBpnReportTphItemQtyLooseFruit = txtBpnReportTphItemQtyLooseFruit;
		this.txtBpnReportTphItemQtyMentah = txtBpnReportTphItemQtyMentah;
		this.txtBpnReportTphItemQtyBusuk = txtBpnReportTphItemQtyBusuk;
		this.txtBpnReportTphItemQtyTangkaiPanjang = txtBpnReportTphItemQtyTangkaiPanjang;
	}

	public TextView getTxtBpnReportTphItemTph() {
		return txtBpnReportTphItemTph;
	}

	public void setTxtBpnReportTphItemTph(TextView txtBpnReportTphItemTph) {
		this.txtBpnReportTphItemTph = txtBpnReportTphItemTph;
	}

	public TextView getTxtBpnReportTphItemBpnDate() {
		return txtBpnReportTphItemBpnDate;
	}

	public void setTxtBpnReportTphItemBpnDate(
			TextView txtBpnReportTphItemBpnDate) {
		this.txtBpnReportTphItemBpnDate = txtBpnReportTphItemBpnDate;
	}

	public TextView getTxtBpnReportTphItemQtyJanjang() {
		return txtBpnReportTphItemQtyJanjang;
	}

	public void setTxtBpnReportTphItemQtyJanjang(
			TextView txtBpnReportTphItemQtyJanjang) {
		this.txtBpnReportTphItemQtyJanjang = txtBpnReportTphItemQtyJanjang;
	}

	public TextView getTxtBpnReportTphItemQtyLooseFruit() {
		return txtBpnReportTphItemQtyLooseFruit;
	}

	public void setTxtBpnReportTphItemQtyLooseFruit(
			TextView txtBpnReportTphItemQtyLooseFruit) {
		this.txtBpnReportTphItemQtyLooseFruit = txtBpnReportTphItemQtyLooseFruit;
	}

	public TextView getTxtBpnReportTphItemQtyMentah() {
		return txtBpnReportTphItemQtyMentah;
	}

	public void setTxtBpnReportTphItemQtyMentah(
			TextView txtBpnReportTphItemQtyMentah) {
		this.txtBpnReportTphItemQtyMentah = txtBpnReportTphItemQtyMentah;
	}

	public TextView getTxtBpnReportTphItemQtyBusuk() {
		return txtBpnReportTphItemQtyBusuk;
	}

	public void setTxtBpnReportTphItemQtyBusuk(
			TextView txtBpnReportTphItemQtyBusuk) {
		this.txtBpnReportTphItemQtyBusuk = txtBpnReportTphItemQtyBusuk;
	}

	public TextView getTxtBpnReportTphItemQtyTangkaiPanjang() {
		return txtBpnReportTphItemQtyTangkaiPanjang;
	}

	public void setTxtBpnReportTphItemQtyTangkaiPanjang(
			TextView txtBpnReportTphItemQtyTangkaiPanjang) {
		this.txtBpnReportTphItemQtyTangkaiPanjang = txtBpnReportTphItemQtyTangkaiPanjang;
	}

}
