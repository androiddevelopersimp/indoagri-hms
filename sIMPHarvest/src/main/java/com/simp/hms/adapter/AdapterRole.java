package com.simp.hms.adapter;

import java.util.List;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewRoleItem;
import com.simp.hms.model.Role;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterRole extends BaseAdapter{
	private Context context;
	private List<Role> lstRole;
	private int layout;
	
	ViewRoleItem vieRole;
	TextView txtItemRole;
	
	public AdapterRole(Context context, List<Role> lstRole, int layout){
		this.context = context;
		this.lstRole = lstRole;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstRole.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lstRole.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Role role = lstRole.get(position);
		
		if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
            
            txtItemRole = (TextView) convertView.findViewById(R.id.txtItemRole);
            vieRole = new ViewRoleItem(txtItemRole);
            
            convertView.setTag(vieRole);
		}else {
			vieRole = (ViewRoleItem) convertView.getTag();
			
			txtItemRole = vieRole.getTxtItemRole();
		}
		
		txtItemRole.setText(role.getName());
		
		return convertView;
	}

}
