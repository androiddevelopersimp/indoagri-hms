package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewAbsentTypeItem;
import com.simp.hms.model.AbsentType;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterAbsentType extends BaseAdapter{
	private Context context;
	private List<AbsentType> listAbsentType, listTemp;
	private int layout;

	public AdapterAbsentType(Context context, List<AbsentType> listAbsentType, int layout){
		this.context = context;
		this.listAbsentType = listAbsentType;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listAbsentType.size();
	}

	@Override
	public Object getItem(int pos) {
		return listAbsentType.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		AbsentType absentType = listAbsentType.get(pos);
		
		ViewAbsentTypeItem view;
		
		TextView txtAbsentTypeItemCompanyCode;
		TextView txtAbsentTypeItemAbsentType;
		TextView txtAbsentTypeItemDescription;
		TextView txtAbsentTypeItemHkrllo;
		TextView txtAbsentTypeItemHkrlhi;
		TextView txtAbsentTypeItemHkpylo;
		TextView txtAbsentTypeItemHkpyhi;
		TextView txtAbsentTypeItemHkvllo;
		TextView txtAbsentTypeItemHkvlhi;
		TextView txtAbsentTypeItemHkmein;
		TextView txtAbsentTypeItemAgroup;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtAbsentTypeItemCompanyCode = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemCompanyCode);
            txtAbsentTypeItemAbsentType = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemAbsentType);
            txtAbsentTypeItemDescription = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemDescription);
            txtAbsentTypeItemHkrllo = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemHkrllo);
            txtAbsentTypeItemHkrlhi = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemHkrlhi);
            txtAbsentTypeItemHkpylo = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemHkpylo);
            txtAbsentTypeItemHkpyhi = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemHkpyhi);
            txtAbsentTypeItemHkvllo = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemHkvllo);
            txtAbsentTypeItemHkvlhi = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemHkvlhi);
            txtAbsentTypeItemHkmein = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemHkmein);
            txtAbsentTypeItemAgroup = (TextView) convertView.findViewById(R.id.txtAbsentTypeItemAgroup);
            
            view = new ViewAbsentTypeItem(txtAbsentTypeItemCompanyCode, txtAbsentTypeItemAbsentType, 
            		txtAbsentTypeItemDescription, txtAbsentTypeItemHkrllo, txtAbsentTypeItemHkrlhi, 
            		txtAbsentTypeItemHkpylo, txtAbsentTypeItemHkpyhi, txtAbsentTypeItemHkvllo, 
            		txtAbsentTypeItemHkvlhi, txtAbsentTypeItemHkmein, txtAbsentTypeItemAgroup);
            
            convertView.setTag(view);
        }else{
        	view = (ViewAbsentTypeItem) convertView.getTag();
        	
        	txtAbsentTypeItemCompanyCode = view.getTxtAbsentTypeItemCompanyCode();
        	txtAbsentTypeItemAbsentType = view.getTxtAbsentTypeItemAbsentType();
        	txtAbsentTypeItemDescription = view.getTxtAbsentTypeItemDescription();
        	txtAbsentTypeItemHkrllo = view.getTxtAbsentTypeItemHkrllo();
        	txtAbsentTypeItemHkrlhi = view.getTxtAbsentTypeItemHkrlhi();
        	txtAbsentTypeItemHkpylo = view.getTxtAbsentTypeItemHkpylo();
        	txtAbsentTypeItemHkpyhi = view.getTxtAbsentTypeItemHkpyhi();
        	txtAbsentTypeItemHkvllo = view.getTxtAbsentTypeItemHkvllo();
        	txtAbsentTypeItemHkvlhi = view.getTxtAbsentTypeItemHkvlhi();
        	txtAbsentTypeItemHkmein = view.getTxtAbsentTypeItemHkmein();
        	txtAbsentTypeItemAgroup = view.getTxtAbsentTypeItemAgroup();
        }

    	txtAbsentTypeItemCompanyCode.setText(absentType.getCompanyCode());
    	txtAbsentTypeItemAbsentType.setText(absentType.getAbsentType());
    	txtAbsentTypeItemDescription.setText(absentType.getDescription());
    	txtAbsentTypeItemHkrllo.setText(String.valueOf(absentType.getHkrllo()));
    	txtAbsentTypeItemHkrlhi.setText(String.valueOf(absentType.getHkrlhi()));
    	txtAbsentTypeItemHkpylo.setText(String.valueOf(absentType.getHkpylo()));
    	txtAbsentTypeItemHkpyhi.setText(String.valueOf(absentType.getHkpyhi()));
    	txtAbsentTypeItemHkvllo.setText(String.valueOf(absentType.getHkvllo()));
    	txtAbsentTypeItemHkvlhi.setText(String.valueOf(absentType.getHkvlhi()));
    	txtAbsentTypeItemHkmein.setText(String.valueOf(absentType.getHkmein()));
    	txtAbsentTypeItemAgroup.setText(String.valueOf(absentType.getAgroup()));

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listAbsentType = null;
				listAbsentType = (List<AbsentType>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<AbsentType> listResult = new ArrayList<AbsentType>();
				AbsentType absentType;
				
				if(listTemp == null){
					listTemp = new ArrayList<AbsentType>(listAbsentType);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						absentType = listTemp.get(i);
						
						if(absentType.getAbsentType().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(absentType);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
