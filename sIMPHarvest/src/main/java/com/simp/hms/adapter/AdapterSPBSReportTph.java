package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSReportTphItem;
import com.simp.hms.model.SPBSReportTph;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSPBSReportTph extends BaseAdapter{
	private Context context;
	private List<SPBSReportTph> lstSummary, lstTemp;
	private int layout;
	
	public AdapterSPBSReportTph(Context context, List<SPBSReportTph> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		SPBSReportTph item = lstSummary.get(pos);
		
		ViewSPBSReportTphItem view;
		TextView txtSPBSReportTphItemSpbsNumber;
		TextView txtSPBSReportTphItemSpbsDate;
		TextView txtSPBSReportTphItemBlock;
		TextView txtSpbsReportTphItemTPh;
		TextView txtSpbsReportTphItemBpnDate;
		TextView txtSPBSReportTphItemQtyJanjang;
		TextView txtSPBSReportTphItemQtyLooseFruit;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtSPBSReportTphItemSpbsNumber = (TextView) convertView.findViewById(R.id.txtSpbsReportTphItemSpbsNumber);
            txtSPBSReportTphItemSpbsDate = (TextView) convertView.findViewById(R.id.txtSpbsReportTphItemSpbsDate);
            txtSPBSReportTphItemBlock = (TextView) convertView.findViewById(R.id.txtSpbsReportTphItemBlock);
            txtSpbsReportTphItemTPh = (TextView) convertView.findViewById(R.id.txtSpbsReportTphItemTPh);
            txtSpbsReportTphItemBpnDate = (TextView) convertView.findViewById(R.id.txtSpbsReportTphItemBpnDate);
            txtSPBSReportTphItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtSpbsReportTphItemQtyJanjang);
            txtSPBSReportTphItemQtyLooseFruit = (TextView) convertView.findViewById(R.id.txtSpbsReportTphItemQtyLooseFruit);
            
            view = new ViewSPBSReportTphItem(txtSPBSReportTphItemSpbsNumber, txtSPBSReportTphItemSpbsDate, txtSPBSReportTphItemBlock,
            		txtSpbsReportTphItemTPh, txtSpbsReportTphItemBpnDate, txtSPBSReportTphItemQtyJanjang, txtSPBSReportTphItemQtyLooseFruit);
             
            convertView.setTag(view);
        }else{
        	view = (ViewSPBSReportTphItem) convertView.getTag();
        	
        	txtSPBSReportTphItemSpbsNumber = view.getTxtSpbsReportTphItemSpbsNumber();
        	txtSPBSReportTphItemSpbsDate = view.getTxtSpbsReportTphItemSpbsDate();
        	txtSPBSReportTphItemBlock = view.getTxtSpbsReportTphItemBlock();
        	txtSpbsReportTphItemTPh = view .getTxtSpbsReportTphItemTPh();
        	txtSpbsReportTphItemBpnDate = view.getTxtSpbsReportTphItemBpnDate();
        	txtSPBSReportTphItemQtyJanjang = view.getTxtSpbsReportTphItemQtyJanjang();
        	txtSPBSReportTphItemQtyLooseFruit = view.getTxtSpbsReportTphItemQtyLooseFruit();
        }

        txtSPBSReportTphItemSpbsNumber.setText(item.getSpbsNumber());
        txtSPBSReportTphItemSpbsDate.setText(item.getSpbsDate());
        txtSPBSReportTphItemBlock.setText(item.getBlock());
        txtSpbsReportTphItemTPh.setText(item.getTph());
        txtSpbsReportTphItemBpnDate.setText(item.getBpnDate());
        txtSPBSReportTphItemQtyJanjang.setText(String.valueOf((int) item.getQtyJanjang()));
        txtSPBSReportTphItemQtyLooseFruit.setText(String.valueOf(Utils.round(item.getQtyLooseFruit(), 2)));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<SPBSReportTph>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SPBSReportTph> lstResult = new ArrayList<SPBSReportTph>();
				SPBSReportTph item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<SPBSReportTph>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getTph().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
