package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNReportTphItem;
import com.simp.hms.model.BPNReportTph;
import com.simp.hms.model.DateLocal;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBPNReportTph extends BaseAdapter{
	private Context context;
	private List<BPNReportTph> lstSummary, lstTemp;
	private int layout;
	
	public AdapterBPNReportTph(Context context, List<BPNReportTph> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BPNReportTph item = lstSummary.get(pos);
		
		ViewBPNReportTphItem view;
		TextView txtBpnReportTphItemTph;
		TextView txtBpnReportTphItemBpnDate;
		TextView txtBPNReportTphItemQtyJanjang;
		TextView txtBPNReportTphItemQtyLooseFruit;
		TextView txtBpnReportTphItemQtyMentah;
		TextView txtBpnReportTphItemQtyBusuk;
		TextView txtBpnReportTphItemQtyTangkaiPanjang;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtBpnReportTphItemTph = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemTph);
            txtBpnReportTphItemBpnDate = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemBpnDate);
            txtBPNReportTphItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyJanjang);
            txtBPNReportTphItemQtyLooseFruit = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyLooseFruit);
            txtBpnReportTphItemQtyMentah = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyMentah);
            txtBpnReportTphItemQtyBusuk = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyBusuk);
            txtBpnReportTphItemQtyTangkaiPanjang = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyTangkaiPanjang);
             
            view = new ViewBPNReportTphItem(txtBpnReportTphItemTph, txtBpnReportTphItemBpnDate, txtBPNReportTphItemQtyJanjang, txtBPNReportTphItemQtyLooseFruit,
            		txtBpnReportTphItemQtyMentah, txtBpnReportTphItemQtyBusuk, txtBpnReportTphItemQtyTangkaiPanjang);
             
            convertView.setTag(view);
        }else{
        	view = (ViewBPNReportTphItem) convertView.getTag();
        	
        	txtBpnReportTphItemTph = view.getTxtBpnReportTphItemTph();
        	txtBpnReportTphItemBpnDate = view.getTxtBpnReportTphItemBpnDate();
        	txtBPNReportTphItemQtyJanjang = view.getTxtBpnReportTphItemQtyJanjang();
        	txtBPNReportTphItemQtyLooseFruit = view.getTxtBpnReportTphItemQtyLooseFruit();
        	txtBpnReportTphItemQtyMentah = view.getTxtBpnReportTphItemQtyMentah();
        	txtBpnReportTphItemQtyBusuk = view.getTxtBpnReportTphItemQtyBusuk();
        	txtBpnReportTphItemQtyTangkaiPanjang = view.getTxtBpnReportTphItemQtyTangkaiPanjang();
        }

        txtBpnReportTphItemTph.setText(item.getTph());
//        txtBpnReportTphItemBpnDate.setText(new DateLocal(item.getId(), DateLocal.FORMAT_ID).getDateString(DateLocal.FORMAT_MASTER_XML));
        
        txtBpnReportTphItemBpnDate.setText(new DateLocal(new Date(item.getCreatedDate())).getDateString(DateLocal.FORMAT_REPORT_VIEW));
        
        txtBPNReportTphItemQtyJanjang.setText(String.valueOf((int) item.getQtyJanjang()));
        txtBPNReportTphItemQtyLooseFruit.setText(String.valueOf(Utils.round(item.getQtyLooseFruit(), 2)));
        txtBpnReportTphItemQtyMentah.setText(String.valueOf((int) item.getQtyMentah()));
        txtBpnReportTphItemQtyBusuk.setText(String.valueOf((int) item.getQtyBusuk()));
        txtBpnReportTphItemQtyTangkaiPanjang.setText(String.valueOf((int) item.getQtyTangkaiPanjang()));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<BPNReportTph>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BPNReportTph> lstResult = new ArrayList<BPNReportTph>();
				BPNReportTph item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BPNReportTph>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getTph().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
