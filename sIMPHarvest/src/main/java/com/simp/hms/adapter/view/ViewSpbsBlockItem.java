package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ViewSpbsBlockItem {
	private Button btnSpbsBlockItemDelete;
	private TextView txtSpbsBlockItemTph;
	private TextView txtSpbsBlockItemBpnDate;
	private EditText edtSpbsBlockItemQtyJanjang;
	private EditText edtSpbsBlockItemQtyJanjangAngkut;
	private EditText edtSpbsBlockItemQtyLooseFruit;
	private EditText edtSpbsBlockItemQtyLooseFruitAngkut;
	private Button btnSpbsBlockItemWarning;

	public ViewSpbsBlockItem() {
	}

	public ViewSpbsBlockItem(Button btnSpbsBlockItemDelete,
			TextView txtSpbsBlockItemTph, TextView txtSpbsBlockItemBpnDate,
			EditText edtSpbsBlockItemQtyJanjang, EditText edtSpbsBlockItemQtyJanjangAngkut,
			EditText edtSpbsBlockItemQtyLooseFruit, EditText edtSpbsBlockItemQtyLooseFruitAngkut,
			Button btnSpbsBlockItemWarning) {
		super();
		this.btnSpbsBlockItemDelete = btnSpbsBlockItemDelete;
		this.txtSpbsBlockItemTph = txtSpbsBlockItemTph;
		this.txtSpbsBlockItemBpnDate = txtSpbsBlockItemBpnDate;
		this.edtSpbsBlockItemQtyJanjang = edtSpbsBlockItemQtyJanjang;
		this.edtSpbsBlockItemQtyJanjangAngkut = edtSpbsBlockItemQtyJanjangAngkut;
		this.edtSpbsBlockItemQtyLooseFruit = edtSpbsBlockItemQtyLooseFruit;
		this.edtSpbsBlockItemQtyLooseFruitAngkut = edtSpbsBlockItemQtyLooseFruitAngkut;
		this.btnSpbsBlockItemWarning = btnSpbsBlockItemWarning;
	}

	public Button getBtnSpbsBlockItemDelete() {
		return btnSpbsBlockItemDelete;
	}

	public void setBtnSpbsBlockItemDelete(Button btnSpbsBlockItemDelete) {
		this.btnSpbsBlockItemDelete = btnSpbsBlockItemDelete;
	}

	public TextView getTxtSpbsBlockItemTph() {
		return txtSpbsBlockItemTph;
	}

	public void setTxtSpbsBlockItemTph(TextView txtSpbsBlockItemTph) {
		this.txtSpbsBlockItemTph = txtSpbsBlockItemTph;
	}

	public TextView getTxtSpbsBlockItemBpnDate() {
		return txtSpbsBlockItemBpnDate;
	}

	public void setTxtSpbsBlockItemBpnDate(TextView txtSpbsBlockItemBpnDate) {
		this.txtSpbsBlockItemBpnDate = txtSpbsBlockItemBpnDate;
	}

	public EditText getEdtSpbsBlockItemQtyJanjang() {
		return edtSpbsBlockItemQtyJanjang;
	}

	public void setEdtSpbsBlockItemQtyJanjang(
			EditText edtSpbsBlockItemQtyJanjang) {
		this.edtSpbsBlockItemQtyJanjang = edtSpbsBlockItemQtyJanjang;
	}

	public EditText getEdtSpbsBlockItemQtyJanjangAngkut() {
		return edtSpbsBlockItemQtyJanjangAngkut;
	}

	public void setEdtSpbsBlockItemQtyJanjangAngkut(
			EditText edtSpbsBlockItemQtyJanjangAngkut) {
		this.edtSpbsBlockItemQtyJanjangAngkut = edtSpbsBlockItemQtyJanjangAngkut;
	}

	public EditText getEdtSpbsBlockItemQtyLooseFruit() {
		return edtSpbsBlockItemQtyLooseFruit;
	}

	public void setEdtSpbsBlockItemQtyLooseFruit(
			EditText edtSpbsBlockItemQtyLooseFruit) {
		this.edtSpbsBlockItemQtyLooseFruit = edtSpbsBlockItemQtyLooseFruit;
	}
	
	public EditText getEdtSpbsBlockItemQtyLooseFruitAngkut() {
		return edtSpbsBlockItemQtyLooseFruitAngkut;
	}

	public void setEdtSpbsBlockItemQtyLooseFruitAngkut(
			EditText edtSpbsBlockItemQtyLooseFruitAngkut) {
		this.edtSpbsBlockItemQtyLooseFruitAngkut = edtSpbsBlockItemQtyLooseFruitAngkut;
	}

	public Button getBtnSpbsBlockItemWarning() {
		return btnSpbsBlockItemWarning;
	}

	public void setBtnSpbsBlockItemWarning(Button btnSpbsBlockItemWarning) {
		this.btnSpbsBlockItemWarning = btnSpbsBlockItemWarning;
	}

}
