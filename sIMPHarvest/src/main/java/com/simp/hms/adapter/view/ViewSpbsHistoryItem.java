package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSpbsHistoryItem {
	private TextView txtSpbsHistoryItemSpbsNumber;
	private TextView txtSpbsHistoryItemSpbsDate;
	private TextView txtSpbsHistoryItemClerk;

	public ViewSpbsHistoryItem(TextView txtSpbsHistoryItemSpbsNumber,
			TextView txtSpbsHistoryItemSpbsDate,
			TextView txtSpbsHistoryItemClerk) {
		super();
		this.txtSpbsHistoryItemSpbsNumber = txtSpbsHistoryItemSpbsNumber;
		this.txtSpbsHistoryItemSpbsDate = txtSpbsHistoryItemSpbsDate;
		this.txtSpbsHistoryItemClerk = txtSpbsHistoryItemClerk;
	}

	public TextView getTxtSpbsHistoryItemSpbsNumber() {
		return txtSpbsHistoryItemSpbsNumber;
	}

	public void setTxtSpbsHistoryItemSpbsNumber(
			TextView txtSpbsHistoryItemSpbsNumber) {
		this.txtSpbsHistoryItemSpbsNumber = txtSpbsHistoryItemSpbsNumber;
	}

	public TextView getTxtSpbsHistoryItemSpbsDate() {
		return txtSpbsHistoryItemSpbsDate;
	}

	public void setTxtSpbsHistoryItemSpbsDate(
			TextView txtSpbsHistoryItemSpbsDate) {
		this.txtSpbsHistoryItemSpbsDate = txtSpbsHistoryItemSpbsDate;
	}

	public TextView getTxtSpbsHistoryItemClerk() {
		return txtSpbsHistoryItemClerk;
	}

	public void setTxtSpbsHistoryItemClerk(TextView txtSpbsHistoryItemClerk) {
		this.txtSpbsHistoryItemClerk = txtSpbsHistoryItemClerk;
	}

}
