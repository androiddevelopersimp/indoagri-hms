package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNCocoaReportHarvesterItemPOD {
    private TextView txtBpnReportHarvesterItemHarvesterName;

    private TextView txtBpnReportHarvesterItemQtyPODQty;
    private TextView txtBpnReportHarvesterItemQtyGoodWeight;
    private TextView txtBpnReportHarvesterItemQtyEstimasiQty;
    private TextView txtBpnReportHarvesterItemQtyPoorWeight;

    public ViewBPNCocoaReportHarvesterItemPOD(
            TextView txtBpnReportHarvesterItemHarvesterName,
            TextView txtBpnReportHarvesterItemQtyPODQty,
            TextView txtBpnReportHarvesterItemQtyGoodWeight,
            TextView txtBpnReportHarvesterItemQtyEstimasiQty,
            TextView txtBpnReportHarvesterItemQtyPoorWeight) {
        super();
        this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
        this.txtBpnReportHarvesterItemQtyPODQty = txtBpnReportHarvesterItemQtyPODQty;
        this.txtBpnReportHarvesterItemQtyGoodWeight = txtBpnReportHarvesterItemQtyGoodWeight;
        this.txtBpnReportHarvesterItemQtyEstimasiQty = txtBpnReportHarvesterItemQtyEstimasiQty;
        this.txtBpnReportHarvesterItemQtyPoorWeight = txtBpnReportHarvesterItemQtyPoorWeight;
    }

    public TextView getTxtBpnReportHarvesterItemHarvesterName() {
        return txtBpnReportHarvesterItemHarvesterName;
    }

    public void setTxtBpnReportHarvesterItemHarvesterName(TextView txtBpnReportHarvesterItemHarvesterName) {
        this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
    }

    public TextView getTxtBpnReportHarvesterItemQtyPODQty() {
        return txtBpnReportHarvesterItemQtyPODQty;
    }

    public void setTxtBpnReportHarvesterItemQtyPODQty(TextView txtBpnReportHarvesterItemQtyPODQty) {
        this.txtBpnReportHarvesterItemQtyPODQty = txtBpnReportHarvesterItemQtyPODQty;
    }

    public TextView getTxtBpnReportHarvesterItemQtyGoodWeight() {
        return txtBpnReportHarvesterItemQtyGoodWeight;
    }

    public void setTxtBpnReportHarvesterItemQtyGoodWeight(TextView txtBpnReportHarvesterItemQtyGoodWeight) {
        this.txtBpnReportHarvesterItemQtyGoodWeight = txtBpnReportHarvesterItemQtyGoodWeight;
    }

    public TextView getTxtBpnReportHarvesterItemQtyEstimasiQty() {
        return txtBpnReportHarvesterItemQtyEstimasiQty;
    }

    public void setTxtBpnReportHarvesterItemQtyEstimasiQty(TextView txtBpnReportHarvesterItemQtyEstimasiQty) {
        this.txtBpnReportHarvesterItemQtyEstimasiQty = txtBpnReportHarvesterItemQtyEstimasiQty;
    }

    public TextView getTxtBpnReportHarvesterItemQtyPoorWeight() {
        return txtBpnReportHarvesterItemQtyPoorWeight;
    }

    public void setTxtBpnReportHarvesterItemQtyPoorWeight(TextView txtBpnReportHarvesterItemQtyPoorWeight) {
        this.txtBpnReportHarvesterItemQtyPoorWeight = txtBpnReportHarvesterItemQtyPoorWeight;
    }
}
