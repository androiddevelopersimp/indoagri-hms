package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewDayOffItem {
	private TextView txtDayOffEstate;
	private TextView txtDayOffDate;
	private TextView txtDayOffDayOffType;
	private TextView txtDayOffDescription;

	public ViewDayOffItem(TextView txtDayOffEstate, TextView txtDayOffDate,
			TextView txtDayOffDayOffType, TextView txtDayOffDescription) {
		super();
		this.txtDayOffEstate = txtDayOffEstate;
		this.txtDayOffDate = txtDayOffDate;
		this.txtDayOffDayOffType = txtDayOffDayOffType;
		this.txtDayOffDescription = txtDayOffDescription;
	}

	public TextView getTxtDayOffEstate() {
		return txtDayOffEstate;
	}

	public void setTxtDayOffEstate(TextView txtDayOffEstate) {
		this.txtDayOffEstate = txtDayOffEstate;
	}

	public TextView getTxtDayOffDate() {
		return txtDayOffDate;
	}

	public void setTxtDayOffDate(TextView txtDayOffDate) {
		this.txtDayOffDate = txtDayOffDate;
	}

	public TextView getTxtDayOffDayOffType() {
		return txtDayOffDayOffType;
	}

	public void setTxtDayOffDayOffType(TextView txtDayOffDayOffType) {
		this.txtDayOffDayOffType = txtDayOffDayOffType;
	}

	public TextView getTxtDayOffDescription() {
		return txtDayOffDescription;
	}

	public void setTxtDayOffDescription(TextView txtDayOffDescription) {
		this.txtDayOffDescription = txtDayOffDescription;
	}

}
