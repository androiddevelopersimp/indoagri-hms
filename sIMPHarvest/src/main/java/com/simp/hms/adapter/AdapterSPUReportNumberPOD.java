package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPUReportNumberItemPOD;
import com.simp.hms.model.SPUReportNumberPOD;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPUReportNumberPOD extends BaseAdapter {
    private Context context;
    private List<SPUReportNumberPOD> lstSummary, lstTemp;
    private int layout;

    public AdapterSPUReportNumberPOD(Context context, List<SPUReportNumberPOD> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        SPUReportNumberPOD item = lstSummary.get(pos);

        ViewSPUReportNumberItemPOD view;
        TextView txtSpuReportNumberItemSpuNumber;
        TextView txtSpuReportNumberItemQtyPODQty;
        TextView txtSpuReportNumberItemQtyGoodWeight;
        TextView txtSpuReportNumberItemQtyEstimasiQty;
        TextView txtSpuReportNumberItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtSpuReportNumberItemSpuNumber = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemSpuNumber);
            txtSpuReportNumberItemQtyPODQty = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyPODQty);
            txtSpuReportNumberItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyGoodWeight);
            txtSpuReportNumberItemQtyEstimasiQty = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyEstimasiQty);
            txtSpuReportNumberItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtSpuReportNumberItemQtyPoorWeight);

            view = new ViewSPUReportNumberItemPOD(txtSpuReportNumberItemSpuNumber,
                    txtSpuReportNumberItemQtyPODQty, txtSpuReportNumberItemQtyGoodWeight,
                    txtSpuReportNumberItemQtyEstimasiQty, txtSpuReportNumberItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewSPUReportNumberItemPOD) convertView.getTag();

            txtSpuReportNumberItemSpuNumber = view.getTxtSpuReportNumberItemSpuNumber();
            txtSpuReportNumberItemQtyPODQty = view.getTxtSpuReportNumberItemQtyPODQty();
            txtSpuReportNumberItemQtyGoodWeight = view.getTxtSpuReportNumberItemQtyGoodWeight();
            txtSpuReportNumberItemQtyEstimasiQty = view.getTxtSpuReportNumberItemQtyEstimasiQty();
            txtSpuReportNumberItemQtyPoorWeight = view.getTxtSpuReportNumberItemQtyPoorWeight();
        }

        txtSpuReportNumberItemSpuNumber.setText(item.getSpuNumber());
        txtSpuReportNumberItemQtyPODQty.setText(String.valueOf(Utils.round(item.getQtyPODQty(), 2)));
        txtSpuReportNumberItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 2)));
        txtSpuReportNumberItemQtyEstimasiQty.setText(String.valueOf(Utils.round(item.getQtyEstimasiQty(), 2)));
        txtSpuReportNumberItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<SPUReportNumberPOD>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPUReportNumberPOD> lstResult = new ArrayList<SPUReportNumberPOD>();
                SPUReportNumberPOD item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<SPUReportNumberPOD>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getSpuNumber().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
