package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBKMReportHarvesterItem;
import com.simp.hms.model.BKMReportHarvester;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBKMReportHarvester extends BaseAdapter{
	private Context context;
	private List<BKMReportHarvester> lstSummary, lstTemp;
	private int layout;
	
	public AdapterBKMReportHarvester(Context context, List<BKMReportHarvester> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BKMReportHarvester item = lstSummary.get(pos);
		
		ViewBKMReportHarvesterItem view;
		TextView txtBKMReportHarvesterItemHarvesterName;
		TextView txtBKMReportHarvesterItemAbsentType;
		TextView txtBKMReportHarvesterItemMandays;
		TextView txtBkmReportHarvesterItemOutput;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtBKMReportHarvesterItemHarvesterName = (TextView) convertView.findViewById(R.id.txtBkmReportHarvesterItemHarvesterName);
            txtBKMReportHarvesterItemAbsentType = (TextView) convertView.findViewById(R.id.txtBkmReportHarvesterItemAbsentType);
            txtBKMReportHarvesterItemMandays = (TextView) convertView.findViewById(R.id.txtBkmReportHarvesterItemMandays);
            txtBkmReportHarvesterItemOutput = (TextView) convertView.findViewById(R.id.txtBkmReportHarvesterItemOutput);
            
            view = new ViewBKMReportHarvesterItem(txtBKMReportHarvesterItemHarvesterName, txtBKMReportHarvesterItemAbsentType, txtBKMReportHarvesterItemMandays, txtBkmReportHarvesterItemOutput);
             
            convertView.setTag(view);
        }else{
        	view = (ViewBKMReportHarvesterItem) convertView.getTag();
        	
        	txtBKMReportHarvesterItemHarvesterName = view.getTxtBkmReportHarvesterItemHarvesterName();
        	txtBKMReportHarvesterItemAbsentType = view.getTxtBkmReportHarvesterItemAbsentType();
        	txtBKMReportHarvesterItemMandays = view.getTxtBkmReportHarvesterItemMandays();
        	txtBkmReportHarvesterItemOutput = view.getTxtBkmReportHarvesterItemOutput();
        }

        txtBKMReportHarvesterItemHarvesterName.setText(item.getName());
        txtBKMReportHarvesterItemAbsentType.setText(item.getAbsentType());
        txtBKMReportHarvesterItemMandays.setText(String.valueOf(Utils.round(item.getMandays(), 2)));
        txtBkmReportHarvesterItemOutput.setText(String.valueOf(Utils.round(item.getOutput(), 2)));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<BKMReportHarvester>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BKMReportHarvester> lstResult = new ArrayList<BKMReportHarvester>();
				BKMReportHarvester item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BKMReportHarvester>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
