package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNCocoaReportBlockItem;
import com.simp.hms.model.BPNCocoaReportBlock;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterBPNCocoaReportBlock extends BaseAdapter {
    private Context context;
    private List<BPNCocoaReportBlock> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNCocoaReportBlock(Context context, List<BPNCocoaReportBlock> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNCocoaReportBlock item = lstSummary.get(pos);

        ViewBPNCocoaReportBlockItem view;
        TextView txtBpnReportBlockItemBlock;

        TextView txtBpnReportBlockItemQtyGoodQty;
        TextView txtBpnReportBlockItemQtyGoodWeight;
        TextView txtBpnReportBlockItemQtyBadQty;
        TextView txtBpnReportBlockItemQtyBadWeight;
        TextView txtBpnReportBlockItemQtyPoorQty;
        TextView txtBpnReportBlockItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtBpnReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemBlock);

            txtBpnReportBlockItemQtyGoodQty = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyGoodQty);
            txtBpnReportBlockItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyGoodWeight);
            txtBpnReportBlockItemQtyBadQty = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyBadQty);
            txtBpnReportBlockItemQtyBadWeight = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyBadWeight);
            txtBpnReportBlockItemQtyPoorQty = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyPoorQty);
            txtBpnReportBlockItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyPoorWeight);

            view = new ViewBPNCocoaReportBlockItem(txtBpnReportBlockItemBlock,
                    txtBpnReportBlockItemQtyGoodQty, txtBpnReportBlockItemQtyGoodWeight,
                    txtBpnReportBlockItemQtyBadQty, txtBpnReportBlockItemQtyBadWeight,
                    txtBpnReportBlockItemQtyPoorQty, txtBpnReportBlockItemQtyPoorWeight);


            convertView.setTag(view);
        }else{
            view = (ViewBPNCocoaReportBlockItem) convertView.getTag();

            txtBpnReportBlockItemBlock = view.getTxtBpnReportBlockItemBlock();
            txtBpnReportBlockItemQtyGoodQty = view.getTxtBpnReportBlockItemQtyGoodQty();
            txtBpnReportBlockItemQtyGoodWeight = view.getTxtBpnReportBlockItemQtyGoodWeight();
            txtBpnReportBlockItemQtyBadQty = view.getTxtBpnReportBlockItemQtyBadQty();
            txtBpnReportBlockItemQtyBadWeight = view.getTxtBpnReportBlockItemQtyBadWeight();
            txtBpnReportBlockItemQtyPoorQty = view.getTxtBpnReportBlockItemQtyPoorQty();
            txtBpnReportBlockItemQtyPoorWeight = view.getTxtBpnReportBlockItemQtyPoorWeight();
        }

        txtBpnReportBlockItemBlock.setText(item.getBlock());
        txtBpnReportBlockItemQtyGoodQty.setText(String.valueOf(Utils.round(item.getQtyGoodQty(), 0)));
        txtBpnReportBlockItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 0)));
        txtBpnReportBlockItemQtyBadQty.setText(String.valueOf(Utils.round(item.getQtyBadQty(), 0)));
        txtBpnReportBlockItemQtyBadWeight.setText(String.valueOf(Utils.round(item.getQtyBadWeight(), 0)));
        txtBpnReportBlockItemQtyPoorQty.setText(String.valueOf(Utils.round(item.getQtyPoorQty(), 0)));
        txtBpnReportBlockItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 0)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNCocoaReportBlock>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNCocoaReportBlock> lstResult = new ArrayList<BPNCocoaReportBlock>();
                BPNCocoaReportBlock item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNCocoaReportBlock>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
