package com.simp.hms.adapter.view;

import android.widget.TextView;

/**
 * Created by Fernando.Siagian on 27/10/2017.
 */

public class ViewBPNKaretReportHarvesterItem {
    private TextView txtBpnReportHarvesterItemHarvesterName;

    private TextView txtBpnReportHarvesterItemQtyLatexWet;
    private TextView txtBpnReportHarvesterItemQtyLatexDRC;
    private TextView txtBpnReportHarvesterItemQtyLatexDry;
    private TextView txtBpnReportHarvesterItemQtyLumpWet;
    private TextView txtBpnReportHarvesterItemQtyLumpDRC;
    private TextView txtBpnReportHarvesterItemQtyLumpDry;
    private TextView txtBpnReportHarvesterItemQtySlabWet;
    private TextView txtBpnReportHarvesterItemQtySlabDRC;
    private TextView txtBpnReportHarvesterItemQtySlabDry;
    private TextView txtBpnReportHarvesterItemQtyTreelaceWet;
    private TextView txtBpnReportHarvesterItemQtyTreelaceDRC;
    private TextView txtBpnReportHarvesterItemQtyTreelaceDry;

    public ViewBPNKaretReportHarvesterItem(
            TextView txtBpnReportHarvesterItemHarvesterName,
            TextView txtBpnReportHarvesterItemQtyLatexWet,
            TextView txtBpnReportHarvesterItemQtyLatexDRC,
            TextView txtBpnReportHarvesterItemQtyLatexDry,
            TextView txtBpnReportHarvesterItemQtyLumpWet,
            TextView txtBpnReportHarvesterItemQtyLumpDRC,
            TextView txtBpnReportHarvesterItemQtyLumpDry,
            TextView txtBpnReportHarvesterItemQtySlabWet,
            TextView txtBpnReportHarvesterItemQtySlabDRC,
            TextView txtBpnReportHarvesterItemQtySlabDry,
            TextView txtBpnReportHarvesterItemQtyTreelaceWet,
            TextView txtBpnReportHarvesterItemQtyTreelaceDRC,
            TextView txtBpnReportHarvesterItemQtyTreelaceDry) {
        super();
        this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
        this.txtBpnReportHarvesterItemQtyLatexWet = txtBpnReportHarvesterItemQtyLatexWet;
        this.txtBpnReportHarvesterItemQtyLatexDRC = txtBpnReportHarvesterItemQtyLatexDRC;
        this.txtBpnReportHarvesterItemQtyLatexDry = txtBpnReportHarvesterItemQtyLatexDry;
        this.txtBpnReportHarvesterItemQtyLumpWet = txtBpnReportHarvesterItemQtyLumpWet;
        this.txtBpnReportHarvesterItemQtyLumpDRC = txtBpnReportHarvesterItemQtyLumpDRC;
        this.txtBpnReportHarvesterItemQtyLumpDry = txtBpnReportHarvesterItemQtyLumpDry;
        this.txtBpnReportHarvesterItemQtySlabWet = txtBpnReportHarvesterItemQtySlabWet;
        this.txtBpnReportHarvesterItemQtySlabDRC = txtBpnReportHarvesterItemQtySlabDRC;
        this.txtBpnReportHarvesterItemQtySlabDry = txtBpnReportHarvesterItemQtySlabDry;
        this.txtBpnReportHarvesterItemQtyTreelaceWet = txtBpnReportHarvesterItemQtyTreelaceWet;
        this.txtBpnReportHarvesterItemQtyTreelaceDRC = txtBpnReportHarvesterItemQtyTreelaceDRC;
        this.txtBpnReportHarvesterItemQtyTreelaceDry = txtBpnReportHarvesterItemQtyTreelaceDry;
    }

    public TextView getTxtBpnReportHarvesterItemHarvesterName() {
        return txtBpnReportHarvesterItemHarvesterName;
    }

    public void setTxtBpnReportHarvesterItemHarvesterName(
            TextView txtBpnReportHarvesterItemHarvesterName) {
        this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
    }

    public TextView getTxtBpnReportHarvesterItemQtyLatexWet() {
        return txtBpnReportHarvesterItemQtyLatexWet;
    }

    public void setTxtBpnReportHarvesterItemQtyLatexWet(
            TextView txtBpnReportHarvesterItemQtyLatexWet) {
        this.txtBpnReportHarvesterItemQtyLatexWet = txtBpnReportHarvesterItemQtyLatexWet;
    }

    public TextView getTxtBpnReportHarvesterItemQtyLatexDRC() {
        return txtBpnReportHarvesterItemQtyLatexDRC;
    }

    public void setTxtBpnReportHarvesterItemQtyLatexDRC(
            TextView txtBpnReportHarvesterItemQtyLatexDRC) {
        this.txtBpnReportHarvesterItemQtyLatexDRC = txtBpnReportHarvesterItemQtyLatexDRC;
    }

    public TextView getTxtBpnReportHarvesterItemQtyLatexDry() {
        return txtBpnReportHarvesterItemQtyLatexDry;
    }

    public void setTxtBpnReportHarvesterItemQtyLatexDry(
            TextView txtBpnReportHarvesterItemQtyLatexDry) {
        this.txtBpnReportHarvesterItemQtyLatexDry = txtBpnReportHarvesterItemQtyLatexDry;
    }

    public TextView getTxtBpnReportHarvesterItemQtyLumpWet() {
        return txtBpnReportHarvesterItemQtyLumpWet;
    }

    public void setTxtBpnReportHarvesterItemQtyLumpWet(
            TextView txtBpnReportHarvesterItemQtyLumpWet) {
        this.txtBpnReportHarvesterItemQtyLumpWet = txtBpnReportHarvesterItemQtyLumpWet;
    }

    public TextView getTxtBpnReportHarvesterItemQtyLumpDRC() {
        return txtBpnReportHarvesterItemQtyLumpDRC;
    }

    public void setTxtBpnReportHarvesterItemQtyLumpDRC(
            TextView txtBpnReportHarvesterItemQtyLumpDRC) {
        this.txtBpnReportHarvesterItemQtyLumpDRC = txtBpnReportHarvesterItemQtyLumpDRC;
    }

    public TextView getTxtBpnReportHarvesterItemQtyLumpDry() {
        return txtBpnReportHarvesterItemQtyLumpDry;
    }

    public void setTxtBpnReportHarvesterItemQtyLumpDry(
            TextView txtBpnReportHarvesterItemQtyLumpDry) {
        this.txtBpnReportHarvesterItemQtyLumpDry = txtBpnReportHarvesterItemQtyLumpDry;
    }

    public TextView getTxtBpnReportHarvesterItemQtySlabWet() {
        return txtBpnReportHarvesterItemQtySlabWet;
    }

    public void setTxtBpnReportHarvesterItemQtySlabWet(
            TextView txtBpnReportHarvesterItemQtySlabWet) {
        this.txtBpnReportHarvesterItemQtySlabWet = txtBpnReportHarvesterItemQtySlabWet;
    }

    public TextView getTxtBpnReportHarvesterItemQtySlabDRC() {
        return txtBpnReportHarvesterItemQtySlabDRC;
    }

    public void setTxtBpnReportHarvesterItemQtySlabDRC(
            TextView txtBpnReportHarvesterItemQtySlabDRC) {
        this.txtBpnReportHarvesterItemQtySlabDRC = txtBpnReportHarvesterItemQtySlabDRC;
    }

    public TextView getTxtBpnReportHarvesterItemQtySlabDry() {
        return txtBpnReportHarvesterItemQtySlabDry;
    }

    public void setTxtBpnReportHarvesterItemQtySlabDry(
            TextView txtBpnReportHarvesterItemQtySlabDry) {
        this.txtBpnReportHarvesterItemQtySlabDry = txtBpnReportHarvesterItemQtySlabDry;
    }

    public TextView getTxtBpnReportHarvesterItemQtyTreelaceWet() {
        return txtBpnReportHarvesterItemQtyTreelaceWet;
    }

    public void setTxtBpnReportHarvesterItemQtyTreelaceWet(
            TextView txtBpnReportHarvesterItemQtyTreelaceWet) {
        this.txtBpnReportHarvesterItemQtyTreelaceWet = txtBpnReportHarvesterItemQtyTreelaceWet;
    }

    public TextView getTxtBpnReportHarvesterItemQtyTreelaceDRC() {
        return txtBpnReportHarvesterItemQtyTreelaceDRC;
    }

    public void setTxtBpnReportHarvesterItemQtyTreelaceDRC(
            TextView txtBpnReportHarvesterItemQtyTreelaceDRC) {
        this.txtBpnReportHarvesterItemQtyTreelaceDRC = txtBpnReportHarvesterItemQtyTreelaceDRC;
    }

    public TextView getTxtBpnReportHarvesterItemQtyTreelaceDry() {
        return txtBpnReportHarvesterItemQtyTreelaceDry;
    }

    public void setTxtBpnReportHarvesterItemQtyTreelaceDry(
            TextView txtBpnReportHarvesterItemQtyTreelaceDry) {
        this.txtBpnReportHarvesterItemQtyTreelaceDry = txtBpnReportHarvesterItemQtyTreelaceDry;
    }
}
