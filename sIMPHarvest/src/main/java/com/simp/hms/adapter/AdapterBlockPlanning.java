package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBlockPlanningItem;
import com.simp.hms.adapter.view.ViewSPBSKernetItem;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.model.BlockPlanning;
import com.simp.hms.model.Employee;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBlockPlanning extends BaseAdapter {
	private Activity activity;
	private List<BlockPlanning> lstBlockPlanning, lstTemp;
	private int layout;
	private boolean isReadOnly;
	
	public AdapterBlockPlanning(Activity activity, List<BlockPlanning> lstBlockPlanning, int layout, boolean isReadOnly){
		this.activity = activity;
		this.lstBlockPlanning = lstBlockPlanning;
		this.layout = layout;
		this.isReadOnly = isReadOnly;
	}

	@Override
	public int getCount() {
		return lstBlockPlanning.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstBlockPlanning.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		final BlockPlanning item = lstBlockPlanning.get(pos);
		
		ViewBlockPlanningItem view;
		Button btnBlockPlanningItemDelete;
		TextView txtBlockPlanningItemName;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            btnBlockPlanningItemDelete = (Button) convertView.findViewById(R.id.btnBlockPlanningItemDelete);
            txtBlockPlanningItemName = (TextView) convertView.findViewById(R.id.txtBlockPlanningItemName);
            
            view = new ViewBlockPlanningItem(btnBlockPlanningItemDelete, txtBlockPlanningItemName);
             
            convertView.setTag(view);
        }else{
        	view = (ViewBlockPlanningItem) convertView.getTag();
        	
        	btnBlockPlanningItemDelete = view.getBtnBlockPlanningItemDelete();
        	txtBlockPlanningItemName = view.getTxtBlockPlanningItemName();
        }

        btnBlockPlanningItemDelete.setVisibility(isReadOnly ? View.GONE : View.VISIBLE);
        txtBlockPlanningItemName.setText(item.getBlock());
        
        btnBlockPlanningItemDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new DialogConfirm(activity, activity.getResources().getString(R.string.informasi),
						activity.getResources().getString(R.string.data_delete), item, pos).show();
			}
		});
        
        return convertView;
	}
	
	public void addData(BlockPlanning blockPlanning){
		if(!isFound(blockPlanning)){
			lstBlockPlanning.add(blockPlanning);
			notifyDataSetChanged();
		}
	}
	
	public void deleteData(int pos){
		lstBlockPlanning.remove(pos);
		notifyDataSetChanged();
	}
	
	private boolean isFound(BlockPlanning blockPlanning){
		boolean found = false;
		
		for (BlockPlanning temp : lstBlockPlanning) {
			if(temp.getBlock().equalsIgnoreCase(blockPlanning.getBlock())){
				found = true;
				break;
			}
		}
		
		return found;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstBlockPlanning = null;
				lstBlockPlanning = (List<BlockPlanning>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BlockPlanning> lstResult = new ArrayList<BlockPlanning>();
				BlockPlanning item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BlockPlanning>(lstBlockPlanning);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
