package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBKMReportBlockItem;
import com.simp.hms.model.BKMReportBlock;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBKMReportBlock extends BaseAdapter{
	private Context context;
	private List<BKMReportBlock> lstSummary, lstTemp;
	private int layout;
	
	public AdapterBKMReportBlock(Context context, List<BKMReportBlock> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BKMReportBlock item = lstSummary.get(pos);
		
		ViewBKMReportBlockItem view;
		TextView txtBkmReportBlockItemBlock;
		TextView txtBkmReportBlockItemOutput;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtBkmReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtBkmReportBlockItemBlock);
            txtBkmReportBlockItemOutput = (TextView) convertView.findViewById(R.id.txtBkmReportBlockItemOutput);
            
            view = new ViewBKMReportBlockItem(txtBkmReportBlockItemBlock, txtBkmReportBlockItemOutput);
             
            convertView.setTag(view);
        }else{
        	view = (ViewBKMReportBlockItem) convertView.getTag();
        	
        	txtBkmReportBlockItemBlock = view.getTxtBkmReportBlockItemBlock();
        	txtBkmReportBlockItemOutput = view.getTxtBkmReportBlockItemOutput();
        }

        txtBkmReportBlockItemBlock.setText(item.getBlock());
        txtBkmReportBlockItemOutput.setText(String.valueOf(Utils.round(item.getOutput(), 2)));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<BKMReportBlock>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BKMReportBlock> lstResult = new ArrayList<BKMReportBlock>();
				BKMReportBlock item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BKMReportBlock>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
