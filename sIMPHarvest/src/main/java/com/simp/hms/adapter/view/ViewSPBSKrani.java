package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.TextView;

public class ViewSPBSKrani {

    private Button btnSpbsStatus;
    private TextView txtNameSpbs;
    private Button btnSpbsPrint;

    public ViewSPBSKrani(){super();}

    public ViewSPBSKrani(Button btnSpbsStatus,
                         TextView txtNameSpbs, Button btnSpbsPrint) {
        super();
        this.btnSpbsStatus = btnSpbsStatus;
        this.txtNameSpbs = txtNameSpbs;
        this.btnSpbsPrint = btnSpbsPrint;
    }

    public Button getBtnSpbsStatus() {
        return btnSpbsStatus;
    }

    public void setBtnSpbsStatus(Button btnSpbsStatus) {
        this.btnSpbsStatus = btnSpbsStatus;
    }

    public TextView getTxtNameSpbs() {
        return txtNameSpbs;
    }

    public void setTxtNameSpbs(TextView txtNameSpbs) {
        this.txtNameSpbs = txtNameSpbs;
    }

    public Button getBtnSpbsPrint() {
        return btnSpbsPrint;
    }

    public void setBtnSpbsPrint(Button btnSpbsPrint) {
        this.btnSpbsPrint = btnSpbsPrint;
    }
}
