package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewRestoreDatabase {
	private TextView txtRestoreDatabaseItemName;

	public ViewRestoreDatabase(TextView txtRestoreDatabaseItemName) {
		super();
		this.txtRestoreDatabaseItemName = txtRestoreDatabaseItemName;
	}

	public TextView getTxtRestoreDatabaseItemName() {
		return txtRestoreDatabaseItemName;
	}

	public void setTxtRestoreDatabaseItemName(TextView txtRestoreDatabaseItemName) {
		this.txtRestoreDatabaseItemName = txtRestoreDatabaseItemName;
	}
	
	
}
