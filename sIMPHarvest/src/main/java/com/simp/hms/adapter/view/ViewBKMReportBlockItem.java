package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBKMReportBlockItem {
	private TextView txtBkmReportBlockItemBlock;
	private TextView txtBkmReportBlockItemOutput;

	public ViewBKMReportBlockItem(TextView txtBkmReportBlockItemBlock,
			TextView txtBkmReportBlockItemOutput) {
		super();
		this.txtBkmReportBlockItemBlock = txtBkmReportBlockItemBlock;
		this.txtBkmReportBlockItemOutput = txtBkmReportBlockItemOutput;
	}

	public TextView getTxtBkmReportBlockItemBlock() {
		return txtBkmReportBlockItemBlock;
	}

	public void setTxtBkmReportBlockItemBlock(
			TextView txtBkmReportBlockItemBlock) {
		this.txtBkmReportBlockItemBlock = txtBkmReportBlockItemBlock;
	}

	public TextView getTxtBkmReportBlockItemOutput() {
		return txtBkmReportBlockItemOutput;
	}

	public void setTxtBkmReportBlockItemOutput(
			TextView txtBkmReportBlockItemOutput) {
		this.txtBkmReportBlockItemOutput = txtBkmReportBlockItemOutput;
	}

}
