package com.simp.hms.adapter;

import java.util.List;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBKMOutputItem;
import com.simp.hms.listener.OutputListener;
import com.simp.hms.model.BKMOutput;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AdapterBKMOutput extends BaseAdapter{
	private Activity activity;
	private Context context;
	private List<BKMOutput> lstBkmOutput;
	private int layout;
	private OutputListener callback;
	
	private Handler repeatUpdateHandler = new Handler();
	
	private boolean mAutoIncrement = false;
	private boolean mAutoDecrement = false;
	
	public double mandays = 1;
	private BKMOutput activeBkmAbsent;
	private int activePos;
	
	
	public AdapterBKMOutput(Activity activity, Context context, List<BKMOutput> lstBkmOutput, int layout){
		this.activity = activity;
		this.context = context;
		this.lstBkmOutput = lstBkmOutput;
		this.layout = layout;
		callback = (OutputListener) activity;
	}

	@Override
	public int getCount() {
		return lstBkmOutput.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstBkmOutput.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(final int pos, View convertView, final ViewGroup parent) {
		final ViewBKMOutputItem vieBkmOutput;
		TextView txtBkmOutputItemName;
		TextView txtBkmOutputItemAbsentType;
		TextView txtBkmOutputItemMandays;
		Button btnBKMOutputItemOutputMin;
		final EditText edtBkmOutputItemOutput;
		Button btnBKMOutputItemOutputPlus;
		
		final BKMOutput bkmOutput = lstBkmOutput.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);     
        
            txtBkmOutputItemName = (TextView) convertView.findViewById(R.id.txtBkmOutputItemName3);
            txtBkmOutputItemAbsentType = (TextView) convertView.findViewById(R.id.txtBkmOutputItemAbsentType3);
            txtBkmOutputItemMandays = (TextView) convertView.findViewById(R.id.txtBkmOutputItemMandays3);
            btnBKMOutputItemOutputMin = (Button) convertView.findViewById(R.id.btnBKMOutputItemOutputMin3);
            edtBkmOutputItemOutput = (EditText) convertView.findViewById(R.id.edtBkmOutputItemOutput3);
            btnBKMOutputItemOutputPlus = (Button) convertView.findViewById(R.id.btnBKMOutputItemOutputPlus3);
            
            vieBkmOutput = new ViewBKMOutputItem(txtBkmOutputItemName, txtBkmOutputItemAbsentType, txtBkmOutputItemMandays, 
            		btnBKMOutputItemOutputMin, edtBkmOutputItemOutput, btnBKMOutputItemOutputPlus, true);     
            convertView.setTag(vieBkmOutput);
        }else{
        	vieBkmOutput = (ViewBKMOutputItem) convertView.getTag();
        	
        	txtBkmOutputItemName = vieBkmOutput.getTxtBkmOutputItemName();
        	txtBkmOutputItemAbsentType = vieBkmOutput.getTxtBkmOutputItemAbsentType();
        	txtBkmOutputItemMandays = vieBkmOutput.getTxtBkmOutputItemMandays();
        	btnBKMOutputItemOutputMin = vieBkmOutput.getBtnBKMOutputItemOutputMin();
        	edtBkmOutputItemOutput = vieBkmOutput.getEdtBKMOutputItemOutput();
        	btnBKMOutputItemOutputPlus = vieBkmOutput.getBtnBKMOutputItemOutputPlus();
        }
        
        txtBkmOutputItemName.setText("" + bkmOutput.getName());
        txtBkmOutputItemAbsentType.setText("");
        edtBkmOutputItemOutput.setText(String.valueOf(bkmOutput.getOutput()));
//        txtBkmOutputItemOutput.setImeOptions(pos == lstBkmOutput.size() - 1 ? EditorInfo.IME_ACTION_DONE : EditorInfo.IME_ACTION_NEXT);
        
        btnBKMOutputItemOutputMin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				decrement(edtBkmOutputItemOutput, pos, bkmOutput);
	        	updateData(activePos, activeBkmAbsent);
	        	callback.onOutputChange();
			}
		});
        
        btnBKMOutputItemOutputMin.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				mAutoDecrement = true;
	            repeatUpdateHandler.post( new RptUpdaterMin(edtBkmOutputItemOutput, pos, bkmOutput) );
	            
				return false;
			}
		});

        
        btnBKMOutputItemOutputMin.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if( (event.getAction() == MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL) && mAutoDecrement ){
					mAutoDecrement = false;
	            }
				
				return false;
			}
		});
        
        btnBKMOutputItemOutputPlus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				increment(edtBkmOutputItemOutput, pos, bkmOutput);
	        	updateData(activePos, activeBkmAbsent);
	        	callback.onOutputChange();
			}
		});
        
        btnBKMOutputItemOutputPlus.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				mAutoIncrement = true;
	            repeatUpdateHandler.post( new RptUpdaterPlus(edtBkmOutputItemOutput, pos, bkmOutput) );
				return false;
			}
		});
        
        btnBKMOutputItemOutputPlus.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if( (event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL) && mAutoIncrement ){
					mAutoIncrement = false;
	            }
				return false;
			}
		});
        
//        
//        txtBkmOutputItemOutput.addTextChangedListener(new TextWatcher() {
//			
//			@Override
//			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//					int arg3) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void afterTextChanged(Editable editable) {
//				BKMOutput bkmOutput = lstBkmOutput.get(pos);
//				
//				bkmOutput.setOutput(editable.toString());
//			}
//		});
        
//        txtBkmOutputItemOutput.setOnEditorActionListener(new OnEditorActionListener() {
//			
//			@Override
//			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//				final ListView lv = (ListView) parent;
//				
//				if(actionId == EditorInfo.IME_ACTION_NEXT && 
//					lv != null &&
//					pos >= lv.getLastVisiblePosition() &&
//					pos != lstBkmOutput.size() - 1){
//						lv.post(new Runnable() {
//							
//							@Override
//							public void run() {
//								lv.setSelection(pos + 1);
//								lv.smoothScrollToPosition(pos + 1);
//								
//								EditText nextField = (EditText) vieBkmOutput.getTxtBkmOutputItemOutput();
//								
//								nextField.focusSearch(View.FOCUS_DOWN);
//								
//								if(nextField != null){
//									nextField.requestFocus();
//								}
//							}
//						});
//					return true;
//				}
//				
//				return false;
//			}
//		});
        

        return convertView;
	}
	
	public void updateData(int pos, BKMOutput bkmOutput){
		lstBkmOutput.set(pos, bkmOutput);
		
		notifyDataSetChanged();
	}
	
	public void addData(BKMOutput bkmOutput){
		lstBkmOutput.add(bkmOutput);
		
		notifyDataSetChanged();
	}
	
	public void increment(TextView v, int pos, BKMOutput bkmOutput){
		double temp = 0;
		
//		if(new Converter(bkmOutput.getOutput()).StrToDouble() < 1){
			temp = bkmOutput.getOutput() + 0.01;
			bkmOutput.setOutput(temp);
			
			activePos = pos;
			activeBkmAbsent = bkmOutput;
			
			v.setText(String.valueOf(temp));
//		}
	}
	
	public void decrement(TextView v, int pos, BKMOutput bkmOutput){
		double temp = 0;
		
		if(bkmOutput.getOutput() > 0){
			temp = bkmOutput.getOutput() - 0.01;
			bkmOutput.setOutput(temp);
		
			activePos = pos;   
			activeBkmAbsent = bkmOutput;
			
			v.setText(String.valueOf(temp));
		}
	}
	
	private class RptUpdaterMin implements Runnable {
		private TextView v;
		private int pos;
		private BKMOutput bkmOutput;
		
		public RptUpdaterMin(TextView v, int pos, BKMOutput bkmOutput){
			this.v = v;
			this.pos = pos;
			this.bkmOutput = bkmOutput;
		}
		
	    public void run() {
	        if(mAutoDecrement){
	        	decrement(v, pos, bkmOutput);
	            repeatUpdateHandler.postDelayed( new RptUpdaterMin(v, pos, bkmOutput), 100);
	        }else{
	        	v.setText(String.valueOf(bkmOutput.getOutput()));
	        	
	        	updateData(pos, bkmOutput);
	        	callback.onOutputChange();
	        }
	    }
	}
	
	private class RptUpdaterPlus implements Runnable {
		private TextView v;
		private int pos;
		private BKMOutput bkmOutput;
		
		public RptUpdaterPlus(TextView v, int pos, BKMOutput bkmOutput){
			this.v = v;
			this.pos = pos;
			this.bkmOutput = bkmOutput;
		}
		
	    public void run() {
	        if(mAutoIncrement){
	            increment(v, pos, bkmOutput);
	            repeatUpdateHandler.postDelayed( new RptUpdaterPlus(v, pos, bkmOutput), 100);
	        }else{
	        	v.setText(String.valueOf(bkmOutput.getOutput()));
	        	
	        	updateData(pos, bkmOutput);
	        	callback.onOutputChange();
	        }
	    }
	}
	
	
}