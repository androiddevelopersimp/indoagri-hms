package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewDivisionAssistantItem;
import com.simp.hms.adapter.view.ViewSPBSDestinationItem;
import com.simp.hms.model.DivisionAssistant;
import com.simp.hms.model.SPBSDestination;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSPBSDestination extends BaseAdapter{
	private Context context;
	private List<SPBSDestination> listData, listTemp;
	private int layout;

	public AdapterSPBSDestination(Context context, List<SPBSDestination> listData, int layout){
		this.context = context;
		this.listData = listData;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int pos) {
		return listData.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		SPBSDestination item = listData.get(pos);
		
		ViewSPBSDestinationItem view;
		
		TextView txtSpbsDestinationItemType;
		TextView txtSpbsDestinationItemDesc;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtSpbsDestinationItemType = (TextView) convertView.findViewById(R.id.txtSpbsDestinationItemType);
            txtSpbsDestinationItemDesc = (TextView) convertView.findViewById(R.id.txtSpbsDestinationItemDesc);
          
            view = new ViewSPBSDestinationItem(txtSpbsDestinationItemType, txtSpbsDestinationItemDesc);
            
            convertView.setTag(view);
        }else{
        	view = (ViewSPBSDestinationItem) convertView.getTag();
        	
        	txtSpbsDestinationItemType = view.getTxtSpbsDestinationItemType();
        	txtSpbsDestinationItemDesc = view.getTxtSpbsDestinationItemDesc();
        }

        txtSpbsDestinationItemType.setText(item.getDestType());
        txtSpbsDestinationItemDesc.setText(item.getDestDesc());

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listData = null;
				listData = (List<SPBSDestination>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SPBSDestination> listResult = new ArrayList<SPBSDestination>();
				SPBSDestination spbsDestination;
				
				if(listTemp == null){
					listTemp = new ArrayList<SPBSDestination>(listData);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						spbsDestination = listTemp.get(i);
						
						if(spbsDestination.getDestDesc().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(spbsDestination);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
