package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNCocoaReportHarvesterItem {
    private TextView txtBpnReportHarvesterItemHarvesterName;

    private TextView txtBpnReportHarvesterItemQtyGoodQty;
    private TextView txtBpnReportHarvesterItemQtyGoodWeight;
    private TextView txtBpnReportHarvesterItemQtyBadQty;
    private TextView txtBpnReportHarvesterItemQtyBadWeight;
    private TextView txtBpnReportHarvesterItemQtyPoorQty;
    private TextView txtBpnReportHarvesterItemQtyPoorWeight;

    public ViewBPNCocoaReportHarvesterItem(
            TextView txtBpnReportHarvesterItemHarvesterName,
            TextView txtBpnReportHarvesterItemQtyGoodQty,
            TextView txtBpnReportHarvesterItemQtyGoodWeight,
            TextView txtBpnReportHarvesterItemQtyBadQty,
            TextView txtBpnReportHarvesterItemQtyBadWeight,
            TextView txtBpnReportHarvesterItemQtyPoorQty,
            TextView txtBpnReportHarvesterItemQtyPoorWeight) {
        super();
        this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
        this.txtBpnReportHarvesterItemQtyGoodQty = txtBpnReportHarvesterItemQtyGoodQty;
        this.txtBpnReportHarvesterItemQtyGoodWeight = txtBpnReportHarvesterItemQtyGoodWeight;
        this.txtBpnReportHarvesterItemQtyBadQty = txtBpnReportHarvesterItemQtyBadQty;
        this.txtBpnReportHarvesterItemQtyBadWeight = txtBpnReportHarvesterItemQtyBadWeight;
        this.txtBpnReportHarvesterItemQtyPoorQty = txtBpnReportHarvesterItemQtyPoorQty;
        this.txtBpnReportHarvesterItemQtyPoorWeight = txtBpnReportHarvesterItemQtyPoorWeight;
    }

    public TextView getTxtBpnReportHarvesterItemHarvesterName() {
        return txtBpnReportHarvesterItemHarvesterName;
    }

    public void setTxtBpnReportHarvesterItemHarvesterName(
            TextView txtBpnReportHarvesterItemHarvesterName) {
        this.txtBpnReportHarvesterItemHarvesterName = txtBpnReportHarvesterItemHarvesterName;
    }

    public TextView getTxtBpnReportHarvesterItemQtyGoodQty() {
        return txtBpnReportHarvesterItemQtyGoodQty;
    }

    public void setTxtBpnReportHarvesterItemQtyGoodQty(
            TextView txtBpnReportHarvesterItemQtyGoodQty) {
        this.txtBpnReportHarvesterItemQtyGoodQty = txtBpnReportHarvesterItemQtyGoodQty;
    }

    public TextView getTxtBpnReportHarvesterItemQtyGoodWeight() {
        return txtBpnReportHarvesterItemQtyGoodWeight;
    }

    public void setTxtBpnReportHarvesterItemQtyGoodWeight(
            TextView txtBpnReportHarvesterItemQtyGoodWeight) {
        this.txtBpnReportHarvesterItemQtyGoodWeight = txtBpnReportHarvesterItemQtyGoodWeight;
    }

    public TextView getTxtBpnReportHarvesterItemQtyBadQty() {
        return txtBpnReportHarvesterItemQtyBadQty;
    }

    public void setTxtBpnReportHarvesterItemQtyBadQty(
            TextView txtBpnReportHarvesterItemQtyBadQty) {
        this.txtBpnReportHarvesterItemQtyBadQty = txtBpnReportHarvesterItemQtyBadQty;
    }

    public TextView getTxtBpnReportHarvesterItemQtyBadWeight() {
        return txtBpnReportHarvesterItemQtyBadWeight;
    }

    public void setTxtBpnReportHarvesterItemQtyBadWeight(
            TextView txtBpnReportHarvesterItemQtyBadWeight) {
        this.txtBpnReportHarvesterItemQtyBadWeight = txtBpnReportHarvesterItemQtyBadWeight;
    }

    public TextView getTxtBpnReportHarvesterItemQtyPoorQty() {
        return txtBpnReportHarvesterItemQtyPoorQty;
    }

    public void setTxtBpnReportHarvesterItemQtyPoorQty(
            TextView txtBpnReportHarvesterItemQtyPoorQty) {
        this.txtBpnReportHarvesterItemQtyPoorQty = txtBpnReportHarvesterItemQtyPoorQty;
    }

    public TextView getTxtBpnReportHarvesterItemQtyPoorWeight() {
        return txtBpnReportHarvesterItemQtyPoorWeight;
    }

    public void setTxtBpnReportHarvesterItemQtyPoorWeight(
            TextView txtBpnReportHarvesterItemQtyPoorWeight) {
        this.txtBpnReportHarvesterItemQtyPoorWeight = txtBpnReportHarvesterItemQtyPoorWeight;
    }
}
