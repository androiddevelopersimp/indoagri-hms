package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewAbsentTypeItem {
	private TextView txtAbsentTypeItemCompanyCode;
	private TextView txtAbsentTypeItemAbsentType;
	private TextView txtAbsentTypeItemDescription;
	private TextView txtAbsentTypeItemHkrllo;
	private TextView txtAbsentTypeItemHkrlhi;
	private TextView txtAbsentTypeItemHkpylo;
	private TextView txtAbsentTypeItemHkpyhi;
	private TextView txtAbsentTypeItemHkvllo;
	private TextView txtAbsentTypeItemHkvlhi;
	private TextView txtAbsentTypeItemHkmein;
	private TextView txtAbsentTypeItemAgroup;
	
	public ViewAbsentTypeItem(TextView txtAbsentTypeItemCompanyCode,
			TextView txtAbsentTypeItemAbsentType,
			TextView txtAbsentTypeItemDescription,
			TextView txtAbsentTypeItemHkrllo, TextView txtAbsentTypeItemHkrlhi,
			TextView txtAbsentTypeItemHkpylo, TextView txtAbsentTypeItemHkpyhi,
			TextView txtAbsentTypeItemHkvllo, TextView txtAbsentTypeItemHkvlhi,
			TextView txtAbsentTypeItemHkmein, TextView txtAbsentTypeItemAgroup) {
		super();
		this.txtAbsentTypeItemCompanyCode = txtAbsentTypeItemCompanyCode;
		this.txtAbsentTypeItemAbsentType = txtAbsentTypeItemAbsentType;
		this.txtAbsentTypeItemDescription = txtAbsentTypeItemDescription;
		this.txtAbsentTypeItemHkrllo = txtAbsentTypeItemHkrllo;
		this.txtAbsentTypeItemHkrlhi = txtAbsentTypeItemHkrlhi;
		this.txtAbsentTypeItemHkpylo = txtAbsentTypeItemHkpylo;
		this.txtAbsentTypeItemHkpyhi = txtAbsentTypeItemHkpyhi;
		this.txtAbsentTypeItemHkvllo = txtAbsentTypeItemHkvllo;
		this.txtAbsentTypeItemHkvlhi = txtAbsentTypeItemHkvlhi;
		this.txtAbsentTypeItemHkmein = txtAbsentTypeItemHkmein;
		this.txtAbsentTypeItemAgroup = txtAbsentTypeItemAgroup;
	}

	public TextView getTxtAbsentTypeItemCompanyCode() {
		return txtAbsentTypeItemCompanyCode;
	}

	public void setTxtAbsentTypeItemCompanyCode(
			TextView txtAbsentTypeItemCompanyCode) {
		this.txtAbsentTypeItemCompanyCode = txtAbsentTypeItemCompanyCode;
	}

	public TextView getTxtAbsentTypeItemAbsentType() {
		return txtAbsentTypeItemAbsentType;
	}

	public void setTxtAbsentTypeItemAbsentType(TextView txtAbsentTypeItemAbsentType) {
		this.txtAbsentTypeItemAbsentType = txtAbsentTypeItemAbsentType;
	}

	public TextView getTxtAbsentTypeItemDescription() {
		return txtAbsentTypeItemDescription;
	}

	public void setTxtAbsentTypeItemDescription(
			TextView txtAbsentTypeItemDescription) {
		this.txtAbsentTypeItemDescription = txtAbsentTypeItemDescription;
	}

	public TextView getTxtAbsentTypeItemHkrllo() {
		return txtAbsentTypeItemHkrllo;
	}

	public void setTxtAbsentTypeItemHkrllo(TextView txtAbsentTypeItemHkrllo) {
		this.txtAbsentTypeItemHkrllo = txtAbsentTypeItemHkrllo;
	}

	public TextView getTxtAbsentTypeItemHkrlhi() {
		return txtAbsentTypeItemHkrlhi;
	}

	public void setTxtAbsentTypeItemHkrlhi(TextView txtAbsentTypeItemHkrlhi) {
		this.txtAbsentTypeItemHkrlhi = txtAbsentTypeItemHkrlhi;
	}

	public TextView getTxtAbsentTypeItemHkpylo() {
		return txtAbsentTypeItemHkpylo;
	}

	public void setTxtAbsentTypeItemHkpylo(TextView txtAbsentTypeItemHkpylo) {
		this.txtAbsentTypeItemHkpylo = txtAbsentTypeItemHkpylo;
	}

	public TextView getTxtAbsentTypeItemHkpyhi() {
		return txtAbsentTypeItemHkpyhi;
	}

	public void setTxtAbsentTypeItemHkpyhi(TextView txtAbsentTypeItemHkpyhi) {
		this.txtAbsentTypeItemHkpyhi = txtAbsentTypeItemHkpyhi;
	}

	public TextView getTxtAbsentTypeItemHkvllo() {
		return txtAbsentTypeItemHkvllo;
	}

	public void setTxtAbsentTypeItemHkvllo(TextView txtAbsentTypeItemHkvllo) {
		this.txtAbsentTypeItemHkvllo = txtAbsentTypeItemHkvllo;
	}

	public TextView getTxtAbsentTypeItemHkvlhi() {
		return txtAbsentTypeItemHkvlhi;
	}

	public void setTxtAbsentTypeItemHkvlhi(TextView txtAbsentTypeItemHkvlhi) {
		this.txtAbsentTypeItemHkvlhi = txtAbsentTypeItemHkvlhi;
	}

	public TextView getTxtAbsentTypeItemHkmein() {
		return txtAbsentTypeItemHkmein;
	}

	public void setTxtAbsentTypeItemHkmein(TextView txtAbsentTypeItemHkmein) {
		this.txtAbsentTypeItemHkmein = txtAbsentTypeItemHkmein;
	}

	public TextView getTxtAbsentTypeItemAgroup() {
		return txtAbsentTypeItemAgroup;
	}

	public void setTxtAbsentTypeItemAgroup(TextView txtAbsentTypeItemAgroup) {
		this.txtAbsentTypeItemAgroup = txtAbsentTypeItemAgroup;
	}
}
