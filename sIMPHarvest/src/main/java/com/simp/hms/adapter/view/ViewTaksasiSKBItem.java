package com.simp.hms.adapter.view;

import com.simp.hms.widget.EditTextCustom;

import android.widget.EditText;
import android.widget.TextView;

public class ViewTaksasiSKBItem {
	private TextView txtTaksasiSkbItemLineSkb;
	private TextView btnTaksasiSkbItemQtyJanjangMinus;
	private EditTextCustom edtTaksasiSkbItemQtyJanjang;
	private TextView btnTaksasiSkbItemQtyJanjangPlus;

	public ViewTaksasiSKBItem(TextView txtTaksasiSkbItemLineSkb,
			TextView btnTaksasiSkbItemQtyJanjangMinus,
			EditTextCustom edtTaksasiSkbItemQtyJanjang,
			TextView btnTaksasiSkbItemQtyJanjangPlus) {
		super();
		this.txtTaksasiSkbItemLineSkb = txtTaksasiSkbItemLineSkb;
		this.btnTaksasiSkbItemQtyJanjangMinus = btnTaksasiSkbItemQtyJanjangMinus;
		this.edtTaksasiSkbItemQtyJanjang = edtTaksasiSkbItemQtyJanjang;
		this.btnTaksasiSkbItemQtyJanjangPlus = btnTaksasiSkbItemQtyJanjangPlus;
	}

	public TextView getTxtTaksasiSkbItemLineSkb() {
		return txtTaksasiSkbItemLineSkb;
	}

	public void setTxtTaksasiSkbItemLineSkb(TextView txtTaksasiSkbItemLineSkb) {
		this.txtTaksasiSkbItemLineSkb = txtTaksasiSkbItemLineSkb;
	}

	public TextView getBtnTaksasiSkbItemQtyJanjangMinus() {
		return btnTaksasiSkbItemQtyJanjangMinus;
	}

	public void setBtnTaksasiSkbItemQtyJanjangMinus(
			TextView btnTaksasiSkbItemQtyJanjangMinus) {
		this.btnTaksasiSkbItemQtyJanjangMinus = btnTaksasiSkbItemQtyJanjangMinus;
	}

	public EditTextCustom getEdtTaksasiSkbItemQtyJanjang() {
		return edtTaksasiSkbItemQtyJanjang;
	}

	public void setEdtTaksasiSkbItemQtyJanjang(
			EditTextCustom edtTaksasiSkbItemQtyJanjang) {
		this.edtTaksasiSkbItemQtyJanjang = edtTaksasiSkbItemQtyJanjang;
	}

	public TextView getBtnTaksasiSkbItemQtyJanjangPlus() {
		return btnTaksasiSkbItemQtyJanjangPlus;
	}

	public void setBtnTaksasiSkbItemQtyJanjangPlus(
			TextView btnTaksasiSkbItemQtyJanjangPlus) {
		this.btnTaksasiSkbItemQtyJanjangPlus = btnTaksasiSkbItemQtyJanjangPlus;
	}

}
