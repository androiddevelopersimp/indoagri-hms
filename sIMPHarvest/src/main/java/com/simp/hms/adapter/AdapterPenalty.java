package com.simp.hms.adapter;

import java.util.List;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewPenaltyItem;
import com.simp.hms.model.Penalty;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

public class AdapterPenalty extends BaseAdapter{
	private Context context;
	private List<Penalty> listPenalty, listTemp;
	private int layout;

	public AdapterPenalty(Context context, List<Penalty> listPenalty, int layout){
		this.context = context;
		this.listPenalty = listPenalty;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listPenalty.size();
	}

	@Override
	public Object getItem(int pos) {
		return listPenalty.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		Penalty penalty = listPenalty.get(pos);
		
		ViewPenaltyItem view;
		
		TextView txtPenaltyItemName;
		EditText edtPenaltyItemCount;
		TextView txtPenaltyItemUom;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtPenaltyItemName = (TextView) convertView.findViewById(R.id.txtPenaltyItemName);
            edtPenaltyItemCount = (EditText) convertView.findViewById(R.id.edtPenaltyItemCount);
            txtPenaltyItemUom = (TextView) convertView.findViewById(R.id.txtPenaltyItemUom);
            
            view = new ViewPenaltyItem(txtPenaltyItemName, edtPenaltyItemCount, txtPenaltyItemUom);
            
            convertView.setTag(view);
        }else{
        	view = (ViewPenaltyItem) convertView.getTag();
        	
        	txtPenaltyItemName = view.getTxtPenaltyItemName();
        	edtPenaltyItemCount = view.getEdtPenaltyItemCount();
        	txtPenaltyItemUom = view.getTxtPenaltyItemUom();
        }

        txtPenaltyItemName.setText(penalty.getPenaltyDesc());
//        edtPenaltyItemCount.setText(penalty.get);
        txtPenaltyItemUom.setText(penalty.getUom());

        return convertView;
	}
}
