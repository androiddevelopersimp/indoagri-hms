package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNReportHarvesterItem;
import com.simp.hms.model.BPNReportHarvester;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBPNReportHarvester extends BaseAdapter{
	private Context context;
	private List<BPNReportHarvester> lstSummary, lstTemp;
	private int layout;
	
	public AdapterBPNReportHarvester(Context context, List<BPNReportHarvester> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BPNReportHarvester item = lstSummary.get(pos);
		
		ViewBPNReportHarvesterItem view;
		TextView txtBpnReportHarvesterItemHarvesterName;
		TextView txtBpnReportHarvesterItemQtyJanjang;
		TextView txtBpnReportHarvesterItemQtyLooseFruit;
		TextView txtBpnReportHarvesterItemQtyMentah;
		TextView txtBpnReportHarvesterItemQtyBusuk;
		TextView txtBpnReportHarvesterItemQtyTangkaiPanjang;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtBpnReportHarvesterItemHarvesterName = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemHarvesterName);
            txtBpnReportHarvesterItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyJanjang);
            txtBpnReportHarvesterItemQtyLooseFruit = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyLooseFruit);
            txtBpnReportHarvesterItemQtyMentah = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyMentah);
            txtBpnReportHarvesterItemQtyBusuk = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyBusuk);
            txtBpnReportHarvesterItemQtyTangkaiPanjang = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyTangkaiPanjang);
            
            view = new ViewBPNReportHarvesterItem(txtBpnReportHarvesterItemHarvesterName, txtBpnReportHarvesterItemQtyJanjang, txtBpnReportHarvesterItemQtyLooseFruit,
            		txtBpnReportHarvesterItemQtyMentah, txtBpnReportHarvesterItemQtyBusuk, txtBpnReportHarvesterItemQtyTangkaiPanjang);
             
            convertView.setTag(view);
        }else{
        	view = (ViewBPNReportHarvesterItem) convertView.getTag();
        	
        	txtBpnReportHarvesterItemHarvesterName = view.getTxtBpnReportHarvesterItemHarvesterName();
        	txtBpnReportHarvesterItemQtyJanjang = view.getTxtBpnReportHarvesterItemQtyJanjang();
        	txtBpnReportHarvesterItemQtyLooseFruit = view.getTxtBpnReportHarvesterItemQtyLooseFruit();
        	txtBpnReportHarvesterItemQtyMentah = view.getTxtBpnReportHarvesterItemQtyMentah();
        	txtBpnReportHarvesterItemQtyBusuk = view.getTxtBpnReportHarvesterItemQtyBusuk();
        	txtBpnReportHarvesterItemQtyTangkaiPanjang = view.getTxtBpnReportHarvesterItemQtyTangkaiPanjang();
        }

        txtBpnReportHarvesterItemHarvesterName.setText(item.getName());
        txtBpnReportHarvesterItemQtyJanjang.setText(String.valueOf((int) item.getQtyJanjang()));
        txtBpnReportHarvesterItemQtyLooseFruit.setText(String.valueOf(Utils.round(item.getQtyLooseFruit(), 2)));
        txtBpnReportHarvesterItemQtyMentah.setText(String.valueOf((int) item.getQtyMentah()));
        txtBpnReportHarvesterItemQtyBusuk.setText(String.valueOf((int) item.getQtyBusuk()));
        txtBpnReportHarvesterItemQtyTangkaiPanjang.setText(String.valueOf((int)  item.getQtyTangkaiPanjang()));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<BPNReportHarvester>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BPNReportHarvester> lstResult = new ArrayList<BPNReportHarvester>();
				BPNReportHarvester item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<BPNReportHarvester>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
