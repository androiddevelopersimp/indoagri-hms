package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBlockHdrcItem;
import com.simp.hms.model.BlockHdrc;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBlockHdrc extends BaseAdapter{
	private Context context;
	private List<BlockHdrc> lst_block_hdrc, lst_temp;
	private int layout;    
	
	private ViewBlockHdrcItem vie_block_hdrc;
	private TextView txt_block_hdrc_item_id;
	private TextView txt_block_hdrc_item_company_code;
	private TextView txt_block_hdrc_item_estate;
	private TextView txt_block_hdrc_item_block;
	private TextView txt_block_hdrc_item_valid_from;
	private TextView txt_block_hdrc_item_valid_to;
	private TextView txt_block_hdrc_item_divisi;
	private TextView txt_block_hdrc_item_type;
	private TextView txt_block_hdrc_item_status;
	private TextView txt_block_hdrc_item_project_definition;
	private TextView txt_block_hdrc_item_owner;
	private TextView txt_block_hdrc_item_tgl_tanam;
	private TextView txt_block_hdrc_item_mandt;
	
	public AdapterBlockHdrc(Context context, List<BlockHdrc> lst_block_hdrc, int layout){
		this.context = context;
		this.lst_block_hdrc = lst_block_hdrc;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_block_hdrc.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_block_hdrc.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BlockHdrc block_hdrc = lst_block_hdrc.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
        	txt_block_hdrc_item_id = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_id);
        	txt_block_hdrc_item_company_code = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_company_code);
        	txt_block_hdrc_item_estate = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_estate);
        	txt_block_hdrc_item_block = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_block);
        	txt_block_hdrc_item_valid_from = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_valid_from);
        	txt_block_hdrc_item_valid_to = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_valid_to);
        	txt_block_hdrc_item_divisi = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_divisi);
        	txt_block_hdrc_item_type = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_type);
        	txt_block_hdrc_item_status = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_status);
        	txt_block_hdrc_item_project_definition = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_project_definition);
        	txt_block_hdrc_item_owner = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_owner);
        	txt_block_hdrc_item_tgl_tanam = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_tgl_tanam);
        	txt_block_hdrc_item_mandt = (TextView) convertView.findViewById(R.id.txt_block_hdrc_item_mandt);
            
            vie_block_hdrc = new ViewBlockHdrcItem(txt_block_hdrc_item_id, txt_block_hdrc_item_company_code, txt_block_hdrc_item_estate, txt_block_hdrc_item_block, 
					   txt_block_hdrc_item_valid_from, txt_block_hdrc_item_valid_to, txt_block_hdrc_item_divisi,
					   txt_block_hdrc_item_type, txt_block_hdrc_item_status, txt_block_hdrc_item_project_definition, txt_block_hdrc_item_owner,
					   txt_block_hdrc_item_tgl_tanam, txt_block_hdrc_item_mandt);     
            convertView.setTag(vie_block_hdrc);
        }else{
        	vie_block_hdrc = (ViewBlockHdrcItem) convertView.getTag();
        	
        	txt_block_hdrc_item_id = vie_block_hdrc.getTxtblockHdrcItemId();
        	txt_block_hdrc_item_company_code = vie_block_hdrc.getTxtBlockHdrcItemCompanyCode();
        	txt_block_hdrc_item_estate = vie_block_hdrc.getTxtblockHdrcItemEstate();
        	txt_block_hdrc_item_block = vie_block_hdrc.getTxtblockHdrcItemBlock();
        	txt_block_hdrc_item_valid_from = vie_block_hdrc.getTxtblockHdrcItemValidFrom();
        	txt_block_hdrc_item_valid_to = vie_block_hdrc.getTxtblockHdrcItemValidTo();
        	txt_block_hdrc_item_divisi = vie_block_hdrc.getTxtblockHdrcItemDivisi();
        	txt_block_hdrc_item_type = vie_block_hdrc.getTxtblockHdrcItemType();
        	txt_block_hdrc_item_status = vie_block_hdrc.getTxtblockHdrcItemStatus();
        	txt_block_hdrc_item_project_definition = vie_block_hdrc.getTxtblockHdrcItemProjectDefinition();
        	txt_block_hdrc_item_owner = vie_block_hdrc.getTxtblockHdrcItemOwner();
        	txt_block_hdrc_item_tgl_tanam = vie_block_hdrc.getTxtBlockHdrcItemTglTanam();
        	txt_block_hdrc_item_mandt = vie_block_hdrc.getTxtblockHdrcItemMandt();
        }
        
    	txt_block_hdrc_item_id.setText(String.valueOf(block_hdrc.getId()));
    	txt_block_hdrc_item_company_code.setText(block_hdrc.getCompanyCode());
    	txt_block_hdrc_item_estate.setText(block_hdrc.getEstate());
    	txt_block_hdrc_item_block.setText(block_hdrc.getBlock());
    	txt_block_hdrc_item_valid_from.setText(block_hdrc.getValidFrom());
    	txt_block_hdrc_item_valid_to.setText(block_hdrc.getValidTo());
    	txt_block_hdrc_item_divisi.setText(block_hdrc.getDivisi());
    	txt_block_hdrc_item_type.setText(block_hdrc.getType());
    	txt_block_hdrc_item_status.setText(block_hdrc.getStatus());
    	txt_block_hdrc_item_project_definition.setText(block_hdrc.getProjectDefinition());
    	txt_block_hdrc_item_owner.setText(block_hdrc.getOwner());
    	txt_block_hdrc_item_tgl_tanam.setText(block_hdrc.getTglTanam());
    	txt_block_hdrc_item_mandt.setText(block_hdrc.getMandt());

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lst_block_hdrc = null;
				lst_block_hdrc = (List<BlockHdrc>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BlockHdrc> list_result = new ArrayList<BlockHdrc>();
				BlockHdrc block_hdrc;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<BlockHdrc>(lst_block_hdrc);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						block_hdrc = lst_temp.get(i);
						
						if(block_hdrc.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(block_hdrc);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
	
	public void addData(BlockHdrc blockHdrc){
		boolean found = false;
		
		for(int i = 0; i < lst_block_hdrc.size(); i++){
			if(blockHdrc.getBlock().equalsIgnoreCase(lst_block_hdrc.get(i).getBlock())){
				found = true;
				break;
			}
		}
		
		if(!found){
			lst_block_hdrc.add(blockHdrc);
			notifyDataSetChanged();
		}
	}
}
