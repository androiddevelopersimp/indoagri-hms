package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSNumber;
import com.simp.hms.model.SPBSWB;

import java.util.List;

public class AdapterSPBSNumber extends BaseAdapter {

    private Context context;
    private List<SPBSWB> listData, listTemp;
    private int layout;

    public AdapterSPBSNumber(Context context, List<SPBSWB> listData, int layout) {
        this.context = context;
        this.listData = listData;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int pos) {
        return listData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        SPBSWB item = listData.get(pos);

        ViewSPBSNumber view;

        TextView txtStringOne;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtStringOne = (TextView) convertView.findViewById(R.id.txtSPBSNo);

            view = new ViewSPBSNumber(txtStringOne);

            convertView.setTag(view);
        } else {
            view = (ViewSPBSNumber) convertView.getTag();

            txtStringOne = view.getStringOne();

        }

        txtStringOne.setText(item.getSpbsNumber());

        return convertView;
    }


}
