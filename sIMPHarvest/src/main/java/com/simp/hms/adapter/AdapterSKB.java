package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSKBItem;
import com.simp.hms.model.SKB;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSKB extends BaseAdapter{
	private Context context;
	private List<SKB> lst_skb, lst_temp;
	private int layout;
	
	private ViewSKBItem vie_skb;
	private TextView txt_skb_item_id;
	private TextView txt_skb_item_company_code;
	private TextView txt_skb_item_estate;
	private TextView txt_skb_item_block;
	private TextView txt_skb_item_baris_skb;
	private TextView txt_skb_item_valid_from;
	private TextView txt_skb_item_valid_to;
	private TextView txt_skb_item_baris_blok;
	private TextView txt_skb_item_jumlah_pokok;
	private TextView txt_skb_item_pokok_mati;
	private TextView txt_skb_item_tanggal_tanam;
	private TextView txt_skb_item_line_skb;
	private TextView txt_skb_item_mandt;
	
	public AdapterSKB(Context context, List<SKB> lst_skb, int layout){
		this.context = context;
		this.lst_skb = lst_skb;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_skb.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_skb.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		SKB skb = lst_skb.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
        	txt_skb_item_id = (TextView) convertView.findViewById(R.id.txt_skb_item_id);
        	txt_skb_item_company_code = (TextView) convertView.findViewById(R.id.txt_skb_item_company_code);
        	txt_skb_item_estate = (TextView) convertView.findViewById(R.id.txt_skb_item_estate);
        	txt_skb_item_block = (TextView) convertView.findViewById(R.id.txt_skb_item_block);
        	txt_skb_item_baris_skb = (TextView) convertView.findViewById(R.id.txt_skb_item_baris_skb);
        	txt_skb_item_valid_from = (TextView) convertView.findViewById(R.id.txt_skb_item_valid_from);
        	txt_skb_item_valid_to = (TextView) convertView.findViewById(R.id.txt_skb_item_valid_to);
        	txt_skb_item_baris_blok = (TextView) convertView.findViewById(R.id.txt_skb_item_baris_blok);
        	txt_skb_item_jumlah_pokok = (TextView) convertView.findViewById(R.id.txt_skb_item_jumlah_pokok);
        	txt_skb_item_pokok_mati = (TextView) convertView.findViewById(R.id.txt_skb_item_pokok_mati);
        	txt_skb_item_tanggal_tanam = (TextView) convertView.findViewById(R.id.txt_skb_item_tanggal_tanam);
        	txt_skb_item_line_skb = (TextView) convertView.findViewById(R.id.txt_skb_item_line_skb);
        	txt_skb_item_mandt = (TextView) convertView.findViewById(R.id.txt_skb_item_mandt);
            
            vie_skb = new ViewSKBItem(txt_skb_item_id, txt_skb_item_company_code, txt_skb_item_estate, txt_skb_item_block, 
					   txt_skb_item_baris_skb, txt_skb_item_valid_from, txt_skb_item_valid_to, txt_skb_item_baris_blok,
					   txt_skb_item_jumlah_pokok, txt_skb_item_pokok_mati, txt_skb_item_tanggal_tanam, txt_skb_item_line_skb,
					   txt_skb_item_mandt);     
            convertView.setTag(vie_skb);
        }else{
        	vie_skb = (ViewSKBItem) convertView.getTag();
        	
        	txt_skb_item_id = vie_skb.getTxtSkbItemId();
        	txt_skb_item_company_code = vie_skb.getTxtSkbItemCompanyCode();
        	txt_skb_item_estate = vie_skb.getTxtSkbItemEstate();
        	txt_skb_item_block = vie_skb.getTxtSkbItemBlock();
        	txt_skb_item_baris_skb = vie_skb.getTxtSkbItemBarisSkb();
        	txt_skb_item_valid_from = vie_skb.getTxtSkbItemValidFrom();
        	txt_skb_item_valid_to = vie_skb.getTxtSkbItemValidTo();
        	txt_skb_item_baris_blok = vie_skb.getTxtSkbItemBarisBlok();
        	txt_skb_item_jumlah_pokok = vie_skb.getTxtSkbItemJumlahPokok();
        	txt_skb_item_pokok_mati = vie_skb.getTxtSkbItemPokokMati();
        	txt_skb_item_tanggal_tanam = vie_skb.getTxtSkbItemTanggalTanam();
        	txt_skb_item_line_skb = vie_skb.getTxtSkbItemLineSkb();
        	txt_skb_item_mandt = vie_skb.getTxtSkbItemMandt();
        }

    	txt_skb_item_id.setText(String.valueOf(skb.getId()));
    	txt_skb_item_company_code.setText(skb.getCompanyCode());
    	txt_skb_item_estate.setText(skb.getEstate());
    	txt_skb_item_block.setText(skb.getBlock());
    	txt_skb_item_baris_skb.setText(skb.getBarisSkb());
    	txt_skb_item_valid_from.setText(skb.getValidFrom());
    	txt_skb_item_valid_to.setText(skb.getValidTo());
    	txt_skb_item_baris_blok.setText(skb.getBarisBlok());
    	txt_skb_item_jumlah_pokok.setText(String.valueOf(skb.getJumlahPokok()));
    	txt_skb_item_pokok_mati.setText(String.valueOf(skb.getPokokMati()));
    	txt_skb_item_tanggal_tanam.setText(skb.getTanggalTanam());
    	txt_skb_item_line_skb.setText(skb.getLineSkb());

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lst_skb = null;
				lst_skb = (List<SKB>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SKB> list_result = new ArrayList<SKB>();
				SKB skb;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<SKB>(lst_skb);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						skb = lst_temp.get(i);
						
						if(skb.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(skb);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
