package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPBSDestinationItem {
	private TextView txtSpbsDestinationItemType;
	private TextView txtSpbsDestinationItemDesc;
	
	public ViewSPBSDestinationItem() {
		super();
	}

	public ViewSPBSDestinationItem(TextView txtSpbsDestinationItemType,
			TextView txtSpbsDestinationItemDesc) {
		super();
		this.txtSpbsDestinationItemType = txtSpbsDestinationItemType;
		this.txtSpbsDestinationItemDesc = txtSpbsDestinationItemDesc;
	}

	public TextView getTxtSpbsDestinationItemType() {
		return txtSpbsDestinationItemType;
	}

	public void setTxtSpbsDestinationItemType(TextView txtSpbsDestinationItemType) {
		this.txtSpbsDestinationItemType = txtSpbsDestinationItemType;
	}

	public TextView getTxtSpbsDestinationItemDesc() {
		return txtSpbsDestinationItemDesc;
	}

	public void setTxtSpbsDestinationItemDesc(TextView txtSpbsDestinationItemDesc) {
		this.txtSpbsDestinationItemDesc = txtSpbsDestinationItemDesc;
	}
}
