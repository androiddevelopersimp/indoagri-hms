package com.simp.hms.adapter.view;

import android.widget.TextView;

/**
 * Created by Anka.Wirawan on 8/4/2016.
 */
public class ViewBlksugcItem {

    private TextView txt_blcksugc_block;
    private TextView txt_blksugc_phase;
    private TextView txt_blksugc_sub_division;
    private TextView txt_blksugc_hektar;

    public ViewBlksugcItem() {
    }

    public ViewBlksugcItem(TextView txt_blcksugc_block, TextView txt_blksugc_phase, TextView txt_blksugc_sub_division,
                           TextView txt_blksugc_hektar) {
        this.txt_blcksugc_block = txt_blcksugc_block;
        this.txt_blksugc_phase = txt_blksugc_phase;
        this.txt_blksugc_sub_division = txt_blksugc_sub_division;
        this.txt_blksugc_hektar = txt_blksugc_hektar;
    }

    public TextView getTxt_blcksugc_block() {
        return txt_blcksugc_block;
    }

    public void setTxt_blcksugc_block(TextView txt_blcksugc_block) {
        this.txt_blcksugc_block = txt_blcksugc_block;
    }

    public TextView getTxt_blksugc_phase() {
        return txt_blksugc_phase;
    }

    public void setTxt_blksugc_phase(TextView txt_blksugc_phase) {
        this.txt_blksugc_phase = txt_blksugc_phase;
    }

    public TextView getTxt_blksugc_sub_division() {
        return txt_blksugc_sub_division;
    }

    public void setTxt_blksugc_sub_division(TextView txt_blksugc_sub_division) {
        this.txt_blksugc_sub_division = txt_blksugc_sub_division;
    }

    public TextView getTxt_blksugc_hektar() {
        return txt_blksugc_hektar;
    }

    public void setTxt_blksugc_hektar(TextView txt_blksugc_hektar) {
        this.txt_blksugc_hektar = txt_blksugc_hektar;
    }
}
