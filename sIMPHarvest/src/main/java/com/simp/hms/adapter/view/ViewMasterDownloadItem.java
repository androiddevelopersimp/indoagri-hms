package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewMasterDownloadItem {
	private TextView txtMasterDowloadItemName;
	private TextView txtMasterDowloadItemFileName;
	private TextView txtMasterDowloadItemSyncDate;
	private TextView txtMasterDowloadItemStatus;

	public ViewMasterDownloadItem(TextView txtMasterDowloadItemName,
			TextView txtMasterDowloadItemFileName,
			TextView txtMasterDowloadItemSyncDate,
			TextView txtMasterDowloadItemStatus) {
		super();
		this.txtMasterDowloadItemName = txtMasterDowloadItemName;
		this.txtMasterDowloadItemFileName = txtMasterDowloadItemFileName;
		this.txtMasterDowloadItemSyncDate = txtMasterDowloadItemSyncDate;
		this.txtMasterDowloadItemStatus = txtMasterDowloadItemStatus;
	}

	public TextView getTxtMasterDowloadItemName() {
		return txtMasterDowloadItemName;
	}

	public void setTxtMasterDowloadItemName(TextView txtMasterDowloadItemName) {
		this.txtMasterDowloadItemName = txtMasterDowloadItemName;
	}

	public TextView getTxtMasterDowloadItemFileName() {
		return txtMasterDowloadItemFileName;
	}

	public void setTxtMasterDowloadItemFileName(
			TextView txtMasterDowloadItemFileName) {
		this.txtMasterDowloadItemFileName = txtMasterDowloadItemFileName;
	}

	public TextView getTxtMasterDowloadItemSyncDate() {
		return txtMasterDowloadItemSyncDate;
	}

	public void setTxtMasterDowloadItemSyncDate(
			TextView txtMasterDowloadItemSyncDate) {
		this.txtMasterDowloadItemSyncDate = txtMasterDowloadItemSyncDate;
	}

	public TextView getTxtMasterDowloadItemStatus() {
		return txtMasterDowloadItemStatus;
	}

	public void setTxtMasterDowloadItemStatus(
			TextView txtMasterDowloadItemStatus) {
		this.txtMasterDowloadItemStatus = txtMasterDowloadItemStatus;
	}

}
