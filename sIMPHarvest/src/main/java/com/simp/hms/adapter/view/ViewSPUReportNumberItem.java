package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPUReportNumberItem {
    private TextView txtSpuReportNumberItemSpuNumber;
    private TextView txtSpuReportNumberItemQtyGoodQty;
    private TextView txtSpuReportNumberItemQtyGoodWeight;
    private TextView txtSpuReportNumberItemQtyBadQty;
    private TextView txtSpuReportNumberItemQtyBadWeight;
    private TextView txtSpuReportNumberItemQtyPoorQty;
    private TextView txtSpuReportNumberItemQtyPoorWeight;

    public ViewSPUReportNumberItem(TextView txtSpuReportNumberItemSpuNumber,
                                    TextView txtSpuReportNumberItemQtyGoodQty,
                                    TextView txtSpuReportNumberItemQtyGoodWeight,
                                    TextView txtSpuReportNumberItemQtyBadQty,
                                   TextView txtSpuReportNumberItemQtyBadWeight,
                                   TextView txtSpuReportNumberItemQtyPoorQty,
                                   TextView txtSpuReportNumberItemQtyPoorWeight) {
        super();
        this.txtSpuReportNumberItemSpuNumber = txtSpuReportNumberItemSpuNumber;
        this.txtSpuReportNumberItemQtyGoodQty = txtSpuReportNumberItemQtyGoodQty;
        this.txtSpuReportNumberItemQtyGoodWeight = txtSpuReportNumberItemQtyGoodWeight;
        this.txtSpuReportNumberItemQtyBadQty = txtSpuReportNumberItemQtyBadQty;
        this.txtSpuReportNumberItemQtyBadWeight = txtSpuReportNumberItemQtyBadWeight;
        this.txtSpuReportNumberItemQtyPoorQty = txtSpuReportNumberItemQtyPoorQty;
        this.txtSpuReportNumberItemQtyPoorWeight = txtSpuReportNumberItemQtyPoorWeight;
    }

    public TextView getTxtSpuReportNumberItemSpuNumber() {
        return txtSpuReportNumberItemSpuNumber;
    }

    public void setTxtSpuReportNumberItemSpuNumber(
            TextView txtSpuReportNumberItemSpuNumber) {
        this.txtSpuReportNumberItemSpuNumber = txtSpuReportNumberItemSpuNumber;
    }


    public TextView getTxtSpuReportNumberItemQtyGoodQty() {
        return txtSpuReportNumberItemQtyGoodQty;
    }

    public void setTxtSpuReportNumberItemQtyGoodQty(
            TextView txtSpuReportNumberItemQtyGoodQty) {
        this.txtSpuReportNumberItemQtyGoodQty = txtSpuReportNumberItemQtyGoodQty;
    }

    public TextView getTxtSpuReportNumberItemQtyGoodWeight() {
        return txtSpuReportNumberItemQtyGoodWeight;
    }

    public void setTxtSpuReportNumberItemQtyGoodWeight(
            TextView txtSpuReportNumberItemQtyGoodWeight) {
        this.txtSpuReportNumberItemQtyGoodWeight = txtSpuReportNumberItemQtyGoodWeight;
    }

    public TextView getTxtSpuReportNumberItemQtyBadQty() {
        return txtSpuReportNumberItemQtyBadQty;
    }

    public void setTxtSpuReportNumberItemQtyBadQty(
            TextView txtSpuReportNumberItemQtyBadQty) {
        this.txtSpuReportNumberItemQtyBadQty = txtSpuReportNumberItemQtyBadQty;
    }

    public TextView getTxtSpuReportNumberItemQtyBadWeight() {
        return txtSpuReportNumberItemQtyBadWeight;
    }

    public void setTxtSpuReportNumberItemQtyBadWeight(
            TextView txtSpuReportNumberItemQtyBadWeight) {
        this.txtSpuReportNumberItemQtyBadWeight = txtSpuReportNumberItemQtyBadWeight;
    }

    public TextView getTxtSpuReportNumberItemQtyPoorQty() {
        return txtSpuReportNumberItemQtyPoorQty;
    }

    public void setTxtSpuReportNumberItemQtyPoorQty(
            TextView txtSpuReportNumberItemQtyPoorQty) {
        this.txtSpuReportNumberItemQtyPoorQty = txtSpuReportNumberItemQtyPoorQty;
    }

    public TextView getTxtSpuReportNumberItemQtyPoorWeight() {
        return txtSpuReportNumberItemQtyPoorWeight;
    }

    public void setTxtSpuReportNumberItemQtyPoorWeight(
            TextView txtSpuReportNumberItemQtyPoorWeight) {
        this.txtSpuReportNumberItemQtyPoorWeight = txtSpuReportNumberItemQtyPoorWeight;
    }
}
