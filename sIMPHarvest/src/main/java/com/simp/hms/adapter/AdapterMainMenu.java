package com.simp.hms.adapter;

import java.util.List;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewMainMenuItem;
import com.simp.hms.model.MainMenuItem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterMainMenu extends BaseAdapter{
	private Context context;
	private List<MainMenuItem> lst_main_menu;
	private int layout;
	
	private ViewMainMenuItem vie_main_menu;
	private ImageView img_main_menu_item;
	private TextView txt_main_menu_item;
	
	public AdapterMainMenu(Context context, List<MainMenuItem> lst_main_menu, int layout){
		this.context = context;
		this.lst_main_menu = lst_main_menu;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_main_menu.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_main_menu.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		MainMenuItem item = lst_main_menu.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            img_main_menu_item = (ImageView) convertView.findViewById(R.id.img_main_menu_item);
            txt_main_menu_item = (TextView) convertView.findViewById(R.id.txt_main_menu_item);
            
            vie_main_menu = new ViewMainMenuItem(img_main_menu_item, txt_main_menu_item);     
            convertView.setTag(vie_main_menu);
        }else{
        	vie_main_menu = (ViewMainMenuItem) convertView.getTag();
        	
        	img_main_menu_item = vie_main_menu.getImgMainMenuItem();
        	txt_main_menu_item = vie_main_menu.getTxtMainMenuItem();
        }
        
        img_main_menu_item.setBackgroundResource(item.getIcon());
        txt_main_menu_item.setText(item.getLabel());   

        return convertView;
	}
}
