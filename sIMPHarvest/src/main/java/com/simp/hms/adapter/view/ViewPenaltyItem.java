package com.simp.hms.adapter.view;

import android.widget.EditText;
import android.widget.TextView;

public class ViewPenaltyItem {
	private TextView txtPenaltyItemName;
	private EditText edtPenaltyItemCount;
	private TextView txtPenaltyItemUom;
	
	public ViewPenaltyItem(TextView txtPenaltyItemName,
			EditText edtPenaltyItemCount, TextView txtPenaltyItemUom) {
		super();
		this.txtPenaltyItemName = txtPenaltyItemName;
		this.edtPenaltyItemCount = edtPenaltyItemCount;
		this.txtPenaltyItemUom = txtPenaltyItemUom;
	}

	public TextView getTxtPenaltyItemName() {
		return txtPenaltyItemName;
	}

	public void setTxtPenaltyItemName(TextView txtPenaltyItemName) {
		this.txtPenaltyItemName = txtPenaltyItemName;
	}

	public EditText getEdtPenaltyItemCount() {
		return edtPenaltyItemCount;
	}

	public void setEdtPenaltyItemCount(EditText edtPenaltyItemCount) {
		this.edtPenaltyItemCount = edtPenaltyItemCount;
	}

	public TextView getTxtPenaltyItemUom() {
		return txtPenaltyItemUom;
	}

	public void setTxtPenaltyItemUom(TextView txtPenaltyItemUom) {
		this.txtPenaltyItemUom = txtPenaltyItemUom;
	}
	
}
