package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.TextView;

public class ViewBlockPlanningItem {
	private Button btnBlockPlanningItemDelete;
	private TextView txtBlockPlanningItemName;
	
	public ViewBlockPlanningItem(Button btnBlockPlanningItemDelete,
			TextView txtBlockPlanningItemName) {
		super();
		this.btnBlockPlanningItemDelete = btnBlockPlanningItemDelete;
		this.txtBlockPlanningItemName = txtBlockPlanningItemName;
	}

	public Button getBtnBlockPlanningItemDelete() {
		return btnBlockPlanningItemDelete;
	}

	public void setBtnBlockPlanningItemDelete(Button btnBlockPlanningItemDelete) {
		this.btnBlockPlanningItemDelete = btnBlockPlanningItemDelete;
	}

	public TextView getTxtBlockPlanningItemName() {
		return txtBlockPlanningItemName;
	}

	public void setTxtBlockPlanningItemName(TextView txtBlockPlanningItemName) {
		this.txtBlockPlanningItemName = txtBlockPlanningItemName;
	}
}
