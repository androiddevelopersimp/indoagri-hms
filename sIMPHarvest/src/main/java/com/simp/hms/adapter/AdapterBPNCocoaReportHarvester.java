package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNCocoaReportHarvesterItem;
import com.simp.hms.model.BPNCocoaReportHarvester;
import com.simp.hms.model.BPNKaretReportHarvester;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterBPNCocoaReportHarvester extends BaseAdapter {
    private Context context;
    private List<BPNCocoaReportHarvester> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNCocoaReportHarvester(Context context, List<BPNCocoaReportHarvester> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }


    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNCocoaReportHarvester item = lstSummary.get(pos);

        ViewBPNCocoaReportHarvesterItem view;
        TextView txtBpnReportHarvesterItemHarvesterName;

        TextView txtBpnReportHarvesterItemQtyGoodQty;
        TextView txtBpnReportHarvesterItemQtyGoodWeight;
        TextView txtBpnReportHarvesterItemQtyBadQty;
        TextView txtBpnReportHarvesterItemQtyBadWeight;
        TextView txtBpnReportHarvesterItemQtyPoorQty;
        TextView txtBpnReportHarvesterItemQtyPoorWeight;

        if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
            txtBpnReportHarvesterItemHarvesterName = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemHarvesterName);

            txtBpnReportHarvesterItemQtyGoodQty = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyGoodQty);
            txtBpnReportHarvesterItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyGoodWeight);
            txtBpnReportHarvesterItemQtyBadQty = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyBadQty);
            txtBpnReportHarvesterItemQtyBadWeight = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyBadWeight);
            txtBpnReportHarvesterItemQtyPoorQty = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyPoorQty);
            txtBpnReportHarvesterItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyPoorWeight);

            view = new ViewBPNCocoaReportHarvesterItem(txtBpnReportHarvesterItemHarvesterName,
                    txtBpnReportHarvesterItemQtyGoodQty, txtBpnReportHarvesterItemQtyGoodWeight,
                    txtBpnReportHarvesterItemQtyBadQty, txtBpnReportHarvesterItemQtyBadWeight,
                    txtBpnReportHarvesterItemQtyPoorQty, txtBpnReportHarvesterItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewBPNCocoaReportHarvesterItem) convertView.getTag();

            txtBpnReportHarvesterItemHarvesterName = view.getTxtBpnReportHarvesterItemHarvesterName();
            txtBpnReportHarvesterItemQtyGoodQty = view.getTxtBpnReportHarvesterItemQtyGoodQty();
            txtBpnReportHarvesterItemQtyGoodWeight = view.getTxtBpnReportHarvesterItemQtyGoodWeight();
            txtBpnReportHarvesterItemQtyBadQty = view.getTxtBpnReportHarvesterItemQtyBadQty();
            txtBpnReportHarvesterItemQtyBadWeight = view.getTxtBpnReportHarvesterItemQtyBadWeight();
            txtBpnReportHarvesterItemQtyPoorQty = view.getTxtBpnReportHarvesterItemQtyPoorQty();
            txtBpnReportHarvesterItemQtyPoorWeight = view.getTxtBpnReportHarvesterItemQtyPoorWeight();
        }

        txtBpnReportHarvesterItemHarvesterName.setText(item.getName());
        txtBpnReportHarvesterItemQtyGoodQty.setText(String.valueOf(Utils.round(item.getQtyGoodQty(), 0)));
        txtBpnReportHarvesterItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 0)));
        txtBpnReportHarvesterItemQtyBadQty.setText(String.valueOf(Utils.round(item.getQtyBadQty(), 0)));
        txtBpnReportHarvesterItemQtyBadWeight.setText(String.valueOf(Utils.round(item.getQtyBadWeight(), 0)));
        txtBpnReportHarvesterItemQtyPoorQty.setText(String.valueOf(Utils.round(item.getQtyPoorQty(), 0)));
        txtBpnReportHarvesterItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 0)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNCocoaReportHarvester>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNCocoaReportHarvester> lstResult = new ArrayList<BPNCocoaReportHarvester>();
                BPNCocoaReportHarvester item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNCocoaReportHarvester>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
