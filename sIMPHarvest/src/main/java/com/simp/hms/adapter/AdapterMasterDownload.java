package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewMasterDownloadItem;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.MasterDownload;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterMasterDownload extends BaseAdapter{
	private Context context;
	private List<MasterDownload> lstMaster, lstTemp;
	private int layout;

	public AdapterMasterDownload(Context context, List<MasterDownload> listMaster, int layout){
		this.context = context;
		this.lstMaster = listMaster;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstMaster.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstMaster.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		MasterDownload item = lstMaster.get(pos);
		
		ViewMasterDownloadItem view;
		
		TextView txtMasterDowloadItemName;
		TextView txtMasterDowloadItemFileName;
		TextView txtMasterDowloadItemSyncDate;
		TextView txtMasterDowloadItemStatus;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtMasterDowloadItemName = (TextView) convertView.findViewById(R.id.txtMasterDowloadItemName);
            txtMasterDowloadItemFileName = (TextView) convertView.findViewById(R.id.txtMasterDowloadItemFileName);
            txtMasterDowloadItemSyncDate = (TextView) convertView.findViewById(R.id.txtMasterDowloadItemSyncDate);
            txtMasterDowloadItemStatus = (TextView) convertView.findViewById(R.id.txtMasterDowloadItemStatus);
            
            view = new ViewMasterDownloadItem(txtMasterDowloadItemName, txtMasterDowloadItemFileName, txtMasterDowloadItemSyncDate, txtMasterDowloadItemStatus);
            convertView.setTag(view);
        }else{
        	view = (ViewMasterDownloadItem) convertView.getTag();
        	
        	txtMasterDowloadItemName = view.getTxtMasterDowloadItemName();
        	txtMasterDowloadItemFileName = view.getTxtMasterDowloadItemFileName();
        	txtMasterDowloadItemSyncDate = view.getTxtMasterDowloadItemSyncDate();
        	txtMasterDowloadItemStatus = view.getTxtMasterDowloadItemStatus();
        }

    	txtMasterDowloadItemName.setText(item.getName());
    	txtMasterDowloadItemFileName.setText(item.getFileName());
    	txtMasterDowloadItemSyncDate.setText(new DateLocal(new Date(item.getSyncDate())).getDateString(DateLocal.FORMAT_MASTER_XML));
    	
    	switch (item.getStatus()) {
		case 1:
			txtMasterDowloadItemStatus.setText("Success");
			break;
		case -1:
			txtMasterDowloadItemStatus.setText("Failed");
			break;
		default:
	    	txtMasterDowloadItemStatus.setText("-");
			break;
		}

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstMaster = null;
				lstMaster = (List<MasterDownload>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<MasterDownload> listResult = new ArrayList<MasterDownload>();
				MasterDownload md;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<MasterDownload>(lstMaster);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						md = lstTemp.get(i);
						
						if(md.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(md);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
