package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPBSNumber {

    private TextView stringOne;

    public ViewSPBSNumber() {
        super();
    }

    public ViewSPBSNumber(TextView stringOne) {
        super();
        this.stringOne = stringOne;
    }

    public TextView getStringOne() {
        return stringOne;
    }

    public void setStringOne(TextView stringOne) {
        this.stringOne = stringOne;
    }

}
