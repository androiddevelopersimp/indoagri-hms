package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPBSReportTphItem {
	private TextView txtSpbsReportTphItemSpbsNumber;
	private TextView txtSpbsReportTphItemSpbsDate;
	private TextView txtSpbsReportTphItemBlock;
	private TextView txtSpbsReportTphItemTPh;
	private TextView txtSpbsReportTphItemBpnDate;
	private TextView txtSpbsReportTphItemQtyJanjang;
	private TextView txtSpbsReportTphItemQtyLooseFruit;

	public ViewSPBSReportTphItem(TextView txtSpbsReportTphItemSpbsNumber,
			TextView txtSpbsReportTphItemSpbsDate,
			TextView txtSpbsReportTphItemBlock,
			TextView txtSpbsReportTphItemTPh,
			TextView txtSpbsReportTphItemBpnDate,
			TextView txtSpbsReportTphItemQtyJanjang,
			TextView txtSpbsReportTphItemQtyLooseFruit) {
		super();
		this.txtSpbsReportTphItemSpbsNumber = txtSpbsReportTphItemSpbsNumber;
		this.txtSpbsReportTphItemSpbsDate = txtSpbsReportTphItemSpbsDate;
		this.txtSpbsReportTphItemBlock = txtSpbsReportTphItemBlock;
		this.txtSpbsReportTphItemTPh = txtSpbsReportTphItemTPh;
		this.txtSpbsReportTphItemBpnDate = txtSpbsReportTphItemBpnDate;
		this.txtSpbsReportTphItemQtyJanjang = txtSpbsReportTphItemQtyJanjang;
		this.txtSpbsReportTphItemQtyLooseFruit = txtSpbsReportTphItemQtyLooseFruit;
	}

	public TextView getTxtSpbsReportTphItemSpbsNumber() {
		return txtSpbsReportTphItemSpbsNumber;
	}

	public void setTxtSpbsReportTphItemSpbsNumber(
			TextView txtSpbsReportTphItemSpbsNumber) {
		this.txtSpbsReportTphItemSpbsNumber = txtSpbsReportTphItemSpbsNumber;
	}

	public TextView getTxtSpbsReportTphItemSpbsDate() {
		return txtSpbsReportTphItemSpbsDate;
	}

	public void setTxtSpbsReportTphItemSpbsDate(
			TextView txtSpbsReportTphItemSpbsDate) {
		this.txtSpbsReportTphItemSpbsDate = txtSpbsReportTphItemSpbsDate;
	}

	public TextView getTxtSpbsReportTphItemBlock() {
		return txtSpbsReportTphItemBlock;
	}

	public void setTxtSpbsReportTphItemBlock(TextView txtSpbsReportTphItemBlock) {
		this.txtSpbsReportTphItemBlock = txtSpbsReportTphItemBlock;
	}

	public TextView getTxtSpbsReportTphItemTPh() {
		return txtSpbsReportTphItemTPh;
	}

	public void setTxtSpbsReportTphItemTPh(TextView txtSpbsReportTphItemTPh) {
		this.txtSpbsReportTphItemTPh = txtSpbsReportTphItemTPh;
	}

	public TextView getTxtSpbsReportTphItemBpnDate() {
		return txtSpbsReportTphItemBpnDate;
	}

	public void setTxtSpbsReportTphItemBpnDate(
			TextView txtSpbsReportTphItemBpnDate) {
		this.txtSpbsReportTphItemBpnDate = txtSpbsReportTphItemBpnDate;
	}

	public TextView getTxtSpbsReportTphItemQtyJanjang() {
		return txtSpbsReportTphItemQtyJanjang;
	}

	public void setTxtSpbsReportTphItemQtyJanjang(
			TextView txtSpbsReportTphItemQtyJanjang) {
		this.txtSpbsReportTphItemQtyJanjang = txtSpbsReportTphItemQtyJanjang;
	}

	public TextView getTxtSpbsReportTphItemQtyLooseFruit() {
		return txtSpbsReportTphItemQtyLooseFruit;
	}

	public void setTxtSpbsReportTphItemQtyLooseFruit(
			TextView txtSpbsReportTphItemQtyLooseFruit) {
		this.txtSpbsReportTphItemQtyLooseFruit = txtSpbsReportTphItemQtyLooseFruit;
	}

}
