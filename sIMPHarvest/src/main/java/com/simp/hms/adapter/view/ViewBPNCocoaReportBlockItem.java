package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBPNCocoaReportBlockItem {
    private TextView txtBpnReportBlockItemBlock;

    private TextView txtBpnReportBlockItemQtyGoodQty;
    private TextView txtBpnReportBlockItemQtyGoodWeight;
    private TextView txtBpnReportBlockItemQtyBadQty;
    private TextView txtBpnReportBlockItemQtyBadWeight;
    private TextView txtBpnReportBlockItemQtyPoorQty;
    private TextView txtBpnReportBlockItemQtyPoorWeight;

    public ViewBPNCocoaReportBlockItem(
            TextView txtBpnReportBlockItemBlock,
            TextView txtBpnReportBlockItemQtyGoodQty,
            TextView txtBpnReportBlockItemQtyGoodWeight,
            TextView txtBpnReportBlockItemQtyBadQty,
            TextView txtBpnReportBlockItemQtyBadWeight,
            TextView txtBpnReportBlockItemQtyPoorQty,
            TextView txtBpnReportBlockItemQtyPoorWeight) {
        super();
        this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
        this.txtBpnReportBlockItemQtyGoodQty = txtBpnReportBlockItemQtyGoodQty;
        this.txtBpnReportBlockItemQtyGoodWeight = txtBpnReportBlockItemQtyGoodWeight;
        this.txtBpnReportBlockItemQtyBadQty = txtBpnReportBlockItemQtyBadQty;
        this.txtBpnReportBlockItemQtyBadWeight = txtBpnReportBlockItemQtyBadWeight;
        this.txtBpnReportBlockItemQtyPoorQty = txtBpnReportBlockItemQtyPoorQty;
        this.txtBpnReportBlockItemQtyPoorWeight = txtBpnReportBlockItemQtyPoorWeight;
    }

    public TextView getTxtBpnReportBlockItemBlock() {
        return txtBpnReportBlockItemBlock;
    }

    public void setTxtBpnReportBlockItemBlock(
            TextView txtBpnReportBlockItemBlock) {
        this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
    }

    public TextView getTxtBpnReportBlockItemQtyGoodQty() {
        return txtBpnReportBlockItemQtyGoodQty;
    }

    public void setTxtBpnReportBlockItemQtyGoodQty(
            TextView txtBpnReportBlockItemQtyGoodQty) {
        this.txtBpnReportBlockItemQtyGoodQty = txtBpnReportBlockItemQtyGoodQty;
    }

    public TextView getTxtBpnReportBlockItemQtyGoodWeight() {
        return txtBpnReportBlockItemQtyGoodWeight;
    }

    public void setTxtBpnReportBlockItemQtyGoodWeight(
            TextView txtBpnReportBlockItemQtyGoodWeight) {
        this.txtBpnReportBlockItemQtyGoodWeight = txtBpnReportBlockItemQtyGoodWeight;
    }

    public TextView getTxtBpnReportBlockItemQtyBadQty() {
        return txtBpnReportBlockItemQtyBadQty;
    }

    public void setTxtBpnReportBlockItemQtyBadQty(
            TextView txtBpnReportBlockItemQtyBadQty) {
        this.txtBpnReportBlockItemQtyBadQty = txtBpnReportBlockItemQtyBadQty;
    }

    public TextView getTxtBpnReportBlockItemQtyBadWeight() {
        return txtBpnReportBlockItemQtyBadWeight;
    }

    public void setTxtBpnReportBlockItemQtyBadWeight(
            TextView txtBpnReportBlockItemQtyBadWeight) {
        this.txtBpnReportBlockItemQtyBadWeight = txtBpnReportBlockItemQtyBadWeight;
    }

    public TextView getTxtBpnReportBlockItemQtyPoorQty() {
        return txtBpnReportBlockItemQtyPoorQty;
    }

    public void setTxtBpnReportBlockItemQtyPoorQty(
            TextView txtBpnReportBlockItemQtyPoorQty) {
        this.txtBpnReportBlockItemQtyPoorQty = txtBpnReportBlockItemQtyPoorQty;
    }

    public TextView getTxtBpnReportBlockItemQtyPoorWeight() {
        return txtBpnReportBlockItemQtyPoorWeight;
    }

    public void setTxtBpnReportBlockItemQtyPoorWeight(
            TextView txtBpnReportBlockItemQtyPoorWeight) {
        this.txtBpnReportBlockItemQtyPoorWeight = txtBpnReportBlockItemQtyPoorWeight;
    }

}
