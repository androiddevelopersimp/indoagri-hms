package com.simp.hms.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewTaksasiSummaryItem;
import com.simp.hms.model.TaksasiSummary;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterTaksasiSummary extends BaseAdapter{
	private Context context;
	private List<TaksasiSummary> listTaksasiSummary, listTemp;
	private int layout;

	public AdapterTaksasiSummary(Context context, List<TaksasiSummary> listTaksasiSummary, int layout){
		this.context = context;
		this.listTaksasiSummary = listTaksasiSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return listTaksasiSummary.size();
	}

	@Override
	public TaksasiSummary getItem(int pos) {
		return listTaksasiSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		TaksasiSummary taksasiSummary = listTaksasiSummary.get(pos);
		
		ViewTaksasiSummaryItem view;
		
		TextView txtTaksasiSummaryItemBarisSkb;
		TextView txtTaksasiSummaryItemQtyJanjang;
		TextView txtTaksasiSummaryItemQtyPokok;
		TextView txtTaksasiSummaryItemTaksasiKg;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtTaksasiSummaryItemBarisSkb = (TextView) convertView.findViewById(R.id.txtTaksasiSummaryItemBarisSkb);
            txtTaksasiSummaryItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtTaksasiSummaryItemQtyJanjang);
            txtTaksasiSummaryItemQtyPokok = (TextView) convertView.findViewById(R.id.txtTaksasiSummaryItemQtyPokok);
            txtTaksasiSummaryItemTaksasiKg = (TextView) convertView.findViewById(R.id.txtTaksasiSummaryItemTaksasiKg);
            
            view = new ViewTaksasiSummaryItem(txtTaksasiSummaryItemBarisSkb, txtTaksasiSummaryItemQtyJanjang, 
            		txtTaksasiSummaryItemQtyPokok, txtTaksasiSummaryItemTaksasiKg);
            
            convertView.setTag(view);
        }else{
        	view = (ViewTaksasiSummaryItem) convertView.getTag();
        	
        	txtTaksasiSummaryItemBarisSkb = view.getTxtTaksasiSummaryItemBarisSkb();
        	txtTaksasiSummaryItemQtyJanjang = view.getTxtTaksasiSummaryItemQtyJanjang();
        	txtTaksasiSummaryItemQtyPokok = view.getTxtTaksasiSummaryItemQtyPokok();
        	txtTaksasiSummaryItemTaksasiKg = view.getTxtTaksasiSummaryItemTaksasiKg();
        }

    	txtTaksasiSummaryItemBarisSkb.setText(String.valueOf(taksasiSummary.getBarisSkb()));
    	txtTaksasiSummaryItemQtyJanjang.setText(String.valueOf(taksasiSummary.getQtyJanjang()));
    	txtTaksasiSummaryItemQtyPokok.setText(String.valueOf(taksasiSummary.getQtyPokok()));
    	txtTaksasiSummaryItemTaksasiKg.setText(new DecimalFormat("###,###,###,###.##").format(Utils.round(taksasiSummary.getTaksasiKg(), 2)));

        return convertView;  
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				listTaksasiSummary = null;
				listTaksasiSummary = (List<TaksasiSummary>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<TaksasiSummary> listResult = new ArrayList<TaksasiSummary>();
				TaksasiSummary spbsSummary;
				
				if(listTemp == null){
					listTemp = new ArrayList<TaksasiSummary>(listTaksasiSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = listTemp.size();
					result.values = listTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < listTemp.size(); i++){
						spbsSummary = listTemp.get(i);
						
						if(String.valueOf(spbsSummary.getBarisSkb()).toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(spbsSummary);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
	
	public void addData(TaksasiSummary taksasiSummary){
		boolean found = false;
		int index = 0;
		
		for(int i = 0; i < listTaksasiSummary.size(); i++){
			if(taksasiSummary.getBarisSkb() == listTaksasiSummary.get(i).getBarisSkb()){
				found = true;
				index = i;
				break;
			}
		}
		
		if(!found){
			listTaksasiSummary.add(taksasiSummary);
			notifyDataSetChanged();
		}else{
			updateData(index, taksasiSummary);
		}
	}
	
	public void updateData(int pos, TaksasiSummary taksasiSummary){
		listTaksasiSummary.set(pos, taksasiSummary);
		
		notifyDataSetChanged();
	}
}
