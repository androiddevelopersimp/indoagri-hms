package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewSPBSReportNumberItem {
	private TextView txtSpbsReportNumberItemSpbsNumber;
	private TextView txtSpbsReportNumberItemSpbsDate;
	private TextView txtSpbsReportNumberItemDriver;
	private TextView txtSpbsReportNumberItemLicensePlate;
	private TextView txtSpbsReportNumberItemQtyJanjang;
	private TextView txtSpbsReportNumberItemQtyLooseFruit;
	private TextView txtSpbsReportNumberDestType;

	public ViewSPBSReportNumberItem(TextView txtSpbsReportNumberItemSpbsNumber,
			TextView txtSpbsReportNumberItemSpbsDate,
			TextView txtSpbsReportNumberItemDriver,
			TextView txtSpbsReportNumberItemLicensePlate,
			TextView txtSpbsReportNumberItemQtyJanjang,
			TextView txtSpbsReportNumberItemQtyLooseFruit,
			TextView txtSpbsReportNumberDestType) {
		super();
		this.txtSpbsReportNumberItemSpbsNumber = txtSpbsReportNumberItemSpbsNumber;
		this.txtSpbsReportNumberItemSpbsDate = txtSpbsReportNumberItemSpbsDate;
		this.txtSpbsReportNumberItemDriver = txtSpbsReportNumberItemDriver;
		this.txtSpbsReportNumberItemLicensePlate = txtSpbsReportNumberItemLicensePlate;
		this.txtSpbsReportNumberItemQtyJanjang = txtSpbsReportNumberItemQtyJanjang;
		this.txtSpbsReportNumberItemQtyLooseFruit = txtSpbsReportNumberItemQtyLooseFruit;
		this.txtSpbsReportNumberDestType = txtSpbsReportNumberDestType;
	}

	public TextView getTxtSpbsReportNumberItemSpbsNumber() {
		return txtSpbsReportNumberItemSpbsNumber;
	}

	public void setTxtSpbsReportNumberItemSpbsNumber(
			TextView txtSpbsReportNumberItemSpbsNumber) {
		this.txtSpbsReportNumberItemSpbsNumber = txtSpbsReportNumberItemSpbsNumber;
	}

	public TextView getTxtSpbsReportNumberItemSpbsDate() {
		return txtSpbsReportNumberItemSpbsDate;
	}

	public void setTxtSpbsReportNumberItemSpbsDate(
			TextView txtSpbsReportNumberItemSpbsDate) {
		this.txtSpbsReportNumberItemSpbsDate = txtSpbsReportNumberItemSpbsDate;
	}

	public TextView getTxtSpbsReportNumberItemDriver() {
		return txtSpbsReportNumberItemDriver;
	}

	public void setTxtSpbsReportNumberItemDriver(
			TextView txtSpbsReportNumberItemDriver) {
		this.txtSpbsReportNumberItemDriver = txtSpbsReportNumberItemDriver;
	}

	public TextView getTxtSpbsReportNumberItemLicensePlate() {
		return txtSpbsReportNumberItemLicensePlate;
	}

	public void setTxtSpbsReportNumberItemLicensePlate(
			TextView txtSpbsReportNumberItemLicensePlate) {
		this.txtSpbsReportNumberItemLicensePlate = txtSpbsReportNumberItemLicensePlate;
	}

	public TextView getTxtSpbsReportNumberItemQtyJanjang() {
		return txtSpbsReportNumberItemQtyJanjang;
	}

	public void setTxtSpbsReportNumberItemQtyJanjang(
			TextView txtSpbsReportNumberItemQtyJanjang) {
		this.txtSpbsReportNumberItemQtyJanjang = txtSpbsReportNumberItemQtyJanjang;
	}

	public TextView getTxtSpbsReportNumberItemQtyLooseFruit() {
		return txtSpbsReportNumberItemQtyLooseFruit;
	}

	public void setTxtSpbsReportNumberItemQtyLooseFruit(
			TextView txtSpbsReportNumberItemQtyLooseFruit) {
		this.txtSpbsReportNumberItemQtyLooseFruit = txtSpbsReportNumberItemQtyLooseFruit;
	}


	public TextView getTxtSpbsReportNumberDestType() {
		return txtSpbsReportNumberDestType;
	}

	public void setTxtSpbsReportNumberDestType(
			TextView txtSpbsReportNumberDestType) {
		this.txtSpbsReportNumberDestType = txtSpbsReportNumberDestType;
	}
}
