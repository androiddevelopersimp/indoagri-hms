package com.simp.hms.adapter;

import android.app.Activity;
import android.database.sqlite.SQLiteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSptaHistoryItem;
import com.simp.hms.database.DatabaseHandler;
import com.simp.hms.dialog.DialogNotification;
import com.simp.hms.model.DateLocal;
import com.simp.hms.model.SPTA;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPTAHistory extends BaseAdapter{
	private Activity activity;
	private List<SPTA> lstSpta, lstTemp;
	private int layout;


	public AdapterSPTAHistory(Activity activity, List<SPTA> listItems, int layout){
		this.activity = activity;
		this.lstSpta = listItems;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSpta.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSpta.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		SPTA item = lstSpta.get(pos);
		
		ViewSptaHistoryItem view;
		TextView txtSptaHistoryItemSptaNum;
		TextView txtSptaHistoryItemSptaDate;
		TextView txtSptaHistoryItemPetak;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtSptaHistoryItemSptaNum = (TextView) convertView.findViewById(R.id.txtSptaHistoryItemSptaNum);
            txtSptaHistoryItemSptaDate = (TextView) convertView.findViewById(R.id.txtSptaHistoryItemSptaDate);
			txtSptaHistoryItemPetak = (TextView) convertView.findViewById(R.id.txtSptaHistoryItemPetak);
            
            view = new ViewSptaHistoryItem(txtSptaHistoryItemSptaNum, txtSptaHistoryItemSptaDate, txtSptaHistoryItemPetak);
            
            convertView.setTag(view);
        }else{
        	view = (ViewSptaHistoryItem) convertView.getTag();
        	
        	txtSptaHistoryItemSptaNum = view.getTxtSptaHistoryItemSptaNum();
        	txtSptaHistoryItemSptaDate = view.getTxtSptaHistoryItemSptaDate();
			txtSptaHistoryItemPetak = view.getTxtSptaHistoryItemPetak();
        }

        txtSptaHistoryItemSptaNum.setText(item.getSptaNum());
        txtSptaHistoryItemSptaDate.setText(new DateLocal(item.getSptaDate(), DateLocal.FORMAT_INPUT).getDateString(DateLocal.FORMAT_VIEW));
		txtSptaHistoryItemPetak.setText(item.getPetakId());

        return convertView;
	}
	
	public void deleteData(int pos){
		SPTA item = lstSpta.get(pos);

		String year = item.getZyear();
		String companyCode = item.getCompanyCode();
		String estate = item.getEstate();
		String sptaNum = item.getSptaNum();
		int status = item.getStatus();
		
		if(status == 0){
			DatabaseHandler database = new DatabaseHandler(activity);
			
			try{
				database.openTransaction();
				database.deleteData(SPTA.TABLE_NAME,
						SPTA.XML_ZYEAR + " = ? " + " and " +
						SPTA.XML_COMPANY_CODE + " = ? " + " and " +
						SPTA.XML_ESTATE + " = ? " + " and " +
						SPTA.XML_SPTA_NUM + " = ? ",
						new String [] {year, companyCode, estate, sptaNum});
				
				database.commitTransaction();
				
				lstSpta.remove(pos);
				notifyDataSetChanged();
			}catch(SQLiteException e){
				e.printStackTrace();
			}finally{
				database.closeTransaction();
			}
			
		}else{
			new DialogNotification(activity, activity.getResources().getString(R.string.informasi), 
					activity.getResources().getString(R.string.data_already_export), false).show();
		}
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSpta = null;
				lstSpta = (List<SPTA>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SPTA> listResult = new ArrayList<SPTA>();
				SPTA spta;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<SPTA>(lstSpta);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						spta = lstTemp.get(i);
						
						if(spta.getSptaNum().toLowerCase(Locale.getDefault()).contains(constraint)){
							listResult.add(spta);
						}
					}
					result.count = listResult.size();
					result.values = listResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
