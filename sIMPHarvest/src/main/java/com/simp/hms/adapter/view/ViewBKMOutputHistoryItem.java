package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBKMOutputHistoryItem {
	private TextView txtBkmAbsentHistoryItemDate;
	private TextView txtBkmAbsentHistoryItemGang;
	private TextView txtBkmAbsentHistoryItemBlock;
	
	public ViewBKMOutputHistoryItem(TextView txtBkmAbsentHistoryItemDate,
			TextView txtBkmAbsentHistoryItemGang, TextView txtBkmAbsentHistoryItemBlock) {
		this.txtBkmAbsentHistoryItemDate = txtBkmAbsentHistoryItemDate;
		this.txtBkmAbsentHistoryItemGang = txtBkmAbsentHistoryItemGang;
		this.txtBkmAbsentHistoryItemBlock = txtBkmAbsentHistoryItemBlock;
	}

	public TextView getTxtBkmAbsentHistoryItemDate() {
		return txtBkmAbsentHistoryItemDate;
	}

	public void setTxtBkmAbsentHistoryItemDate(TextView txtBkmAbsentHistoryItemDate) {
		this.txtBkmAbsentHistoryItemDate = txtBkmAbsentHistoryItemDate;
	}

	public TextView getTxtBkmAbsentHistoryItemGang() {
		return txtBkmAbsentHistoryItemGang;
	}

	public void setTxtBkmAbsentHistoryItemGang(TextView txtBkmAbsentHistoryItemGang) {
		this.txtBkmAbsentHistoryItemGang = txtBkmAbsentHistoryItemGang;
	}

	public TextView getTxtBkmAbsentHistoryItemBlock() {
		return txtBkmAbsentHistoryItemBlock;
	}

	public void setTxtBkmAbsentHistoryItemBlock(
			TextView txtBkmAbsentHistoryItemBlock) {
		this.txtBkmAbsentHistoryItemBlock = txtBkmAbsentHistoryItemBlock;
	}
}
