package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPUReportTphItemPOD;
import com.simp.hms.model.SPUReportTphPOD;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPUReportTphPOD extends BaseAdapter {
    private Context context;
    private List<SPUReportTphPOD> lstSummary, lstTemp;
    private int layout;

    public AdapterSPUReportTphPOD(Context context, List<SPUReportTphPOD> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        SPUReportTphPOD item = lstSummary.get(pos);

        ViewSPUReportTphItemPOD view;
        TextView txtSpuReportTphItemTPh;
        TextView txtSpuReportTphItemQtyPODQty;
        TextView txtSpuReportTphItemQtyGoodWeight;
        TextView txtSpuReportTphItemQtyEstimasiQty;
        TextView txtSpuReportTphItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtSpuReportTphItemTPh = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemTPh);
            txtSpuReportTphItemQtyPODQty = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyPODQty);
            txtSpuReportTphItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyGoodWeight);
            txtSpuReportTphItemQtyEstimasiQty = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyEstimasiQty);
            txtSpuReportTphItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtSpuReportTphItemQtyPoorWeight);

            view = new ViewSPUReportTphItemPOD(txtSpuReportTphItemTPh,
                                            txtSpuReportTphItemQtyPODQty, txtSpuReportTphItemQtyGoodWeight,
                                            txtSpuReportTphItemQtyEstimasiQty, txtSpuReportTphItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewSPUReportTphItemPOD) convertView.getTag();

            txtSpuReportTphItemTPh = view.getTxtSpuReportTphItemTph();
            txtSpuReportTphItemQtyPODQty = view.getTxtSpuReportTphItemQtyPODQty ();
            txtSpuReportTphItemQtyGoodWeight = view.getTxtSpuReportTphItemQtyGoodWeight();
            txtSpuReportTphItemQtyEstimasiQty = view.getTxtSpuReportTphItemQtyEstimasiQty();
            txtSpuReportTphItemQtyPoorWeight = view.getTxtSpuReportTphItemQtyPoorWeight();
        }

        txtSpuReportTphItemTPh.setText(item.getTph());
        txtSpuReportTphItemQtyPODQty.setText(String.valueOf(Utils.round(item.getQtyPODQty(), 2)));
        txtSpuReportTphItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 2)));
        txtSpuReportTphItemQtyEstimasiQty.setText(String.valueOf(Utils.round(item.getQtyEstimasiQty(), 2)));
        txtSpuReportTphItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<SPUReportTphPOD>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPUReportTphPOD> lstResult = new ArrayList<SPUReportTphPOD>();
                SPUReportTphPOD item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<SPUReportTphPOD>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getTph().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
