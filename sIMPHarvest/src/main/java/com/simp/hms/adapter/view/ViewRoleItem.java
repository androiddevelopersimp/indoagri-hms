package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewRoleItem {
	private TextView txtItemRole;

	public ViewRoleItem(TextView txtItemRole) {
		super();
		this.txtItemRole = txtItemRole;
	}

	public TextView getTxtItemRole() {
		return txtItemRole;
	}

	public void setTxtItemRole(TextView txtItemRole) {
		this.txtItemRole = txtItemRole;
	}
}
