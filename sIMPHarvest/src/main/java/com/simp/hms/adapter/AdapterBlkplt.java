package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBlkpltItem;
import com.simp.hms.model.BLKPLT;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBlkplt extends BaseAdapter{
	private Context context;
	private List<BLKPLT> lst_blkplt, lst_temp;
	private int layout;
	
	private ViewBlkpltItem vie_skb;
	private TextView txt_blkplt_item_id;
	private TextView txt_blkplt_item_company_code;
	private TextView txt_blkplt_item_estate;
	private TextView txt_blkplt_item_block;
	private TextView txt_blkplt_item_valid_from;
	private TextView txt_blkplt_item_valid_to;
	private TextView txt_blkplt_item_crop_type;
	private TextView txt_blkplt_item_previous_crop;
	private TextView txt_blkplt_item_finish_date;
	private TextView txt_blkplt_item_reference;
	private TextView txt_blkplt_item_jarak_tanam;
	private TextView txt_blkplt_item_harvesting_date;
	private TextView txt_blkplt_item_harvested;
	private TextView txt_blkplt_item_plan_date;
	private TextView txt_blkplt_item_topography;
	private TextView txt_blkplt_item_soil_type;
	private TextView txt_blkplt_item_soil_category;
	private TextView txt_blkplt_item_previous_block;
	private TextView txt_blkplt_item_prod_trees;
	
	public AdapterBlkplt(Context context, List<BLKPLT> lst_skb, int layout){
		this.context = context;
		this.lst_blkplt = lst_skb;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_blkplt.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_blkplt.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BLKPLT blkplt = lst_blkplt.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txt_blkplt_item_id = (TextView) convertView.findViewById(R.id.txt_blkplt_item_id);
            txt_blkplt_item_company_code = (TextView) convertView.findViewById(R.id.txt_blkplt_item_company_code);
            txt_blkplt_item_estate = (TextView) convertView.findViewById(R.id.txt_blkplt_item_estate);
            txt_blkplt_item_block = (TextView) convertView.findViewById(R.id.txt_blkplt_item_block2);
            txt_blkplt_item_valid_from = (TextView) convertView.findViewById(R.id.txt_blkplt_item_valid_from);
            txt_blkplt_item_valid_to = (TextView) convertView.findViewById(R.id.txt_blkplt_item_valid_to);
            txt_blkplt_item_crop_type = (TextView) convertView.findViewById(R.id.txt_blkplt_item_crop_type);
            txt_blkplt_item_previous_crop = (TextView) convertView.findViewById(R.id.txt_blkplt_item_previous_crop);
            txt_blkplt_item_finish_date = (TextView) convertView.findViewById(R.id.txt_blkplt_item_finish_date);
            txt_blkplt_item_reference = (TextView) convertView.findViewById(R.id.txt_blkplt_item_reference);
            txt_blkplt_item_jarak_tanam = (TextView) convertView.findViewById(R.id.txt_blkplt_item_jarak_tanam);
            txt_blkplt_item_harvesting_date = (TextView) convertView.findViewById(R.id.txt_blkplt_item_harvesting_date);
            txt_blkplt_item_harvested = (TextView) convertView.findViewById(R.id.txt_blkplt_item_harvested2);
            txt_blkplt_item_plan_date = (TextView) convertView.findViewById(R.id.txt_blkplt_item_plan_date);
            txt_blkplt_item_topography = (TextView) convertView.findViewById(R.id.txt_blkplt_item_topography);
            txt_blkplt_item_soil_type = (TextView) convertView.findViewById(R.id.txt_blkplt_item_soil_type);
            txt_blkplt_item_soil_category = (TextView) convertView.findViewById(R.id.txt_blkplt_item_soil_category);
            txt_blkplt_item_previous_block = (TextView) convertView.findViewById(R.id.txt_blkplt_item_previous_block);
            txt_blkplt_item_prod_trees = (TextView) convertView.findViewById(R.id.txt_blkplt_item_prod_trees);
            
            vie_skb = new ViewBlkpltItem(txt_blkplt_item_id, txt_blkplt_item_company_code,
        			txt_blkplt_item_estate, txt_blkplt_item_block, txt_blkplt_item_valid_from,
        			txt_blkplt_item_valid_to, txt_blkplt_item_crop_type,
        			txt_blkplt_item_previous_crop, txt_blkplt_item_finish_date,
        			txt_blkplt_item_reference, txt_blkplt_item_jarak_tanam,
        			txt_blkplt_item_harvesting_date, txt_blkplt_item_harvested,
        			txt_blkplt_item_plan_date, txt_blkplt_item_topography,
        			txt_blkplt_item_soil_type, txt_blkplt_item_soil_category,
        			txt_blkplt_item_previous_block, txt_blkplt_item_prod_trees);  
            convertView.setTag(vie_skb);
        }else{
        	vie_skb = (ViewBlkpltItem) convertView.getTag();
        	
        	txt_blkplt_item_id = vie_skb.getTxtBlkpltItemId();
        	txt_blkplt_item_company_code = vie_skb.getTxtBlkpltItemCompanyCode();
        	txt_blkplt_item_estate = vie_skb.getTxtBlkpltItemEstate();
        	txt_blkplt_item_block = vie_skb.getTxtBlkpltItemBlock();
        	txt_blkplt_item_valid_from = vie_skb.getTxtBlkpltItemValidFrom();
        	txt_blkplt_item_valid_to = vie_skb.getTxtBlkpltItemValidTo();
        	txt_blkplt_item_crop_type = vie_skb.getTxtBlkpltItemCropType();
        	txt_blkplt_item_previous_crop = vie_skb.getTxtBlkpltItemPreviousCrop();
        	txt_blkplt_item_finish_date = vie_skb.getTxtBlkpltItemFinishDate();
        	txt_blkplt_item_reference = vie_skb.getTxtBlkpltItemReference();
        	txt_blkplt_item_jarak_tanam = vie_skb.getTxtBlkpltItemJarakTanam();
        	txt_blkplt_item_harvesting_date = vie_skb.getTxtBlkpltItemHarvestingDate();
        	txt_blkplt_item_harvested = vie_skb.getTxtBlkpltItemHarvested();
        	txt_blkplt_item_plan_date = vie_skb.getTxtBlkpltItemPlanDate();
        	txt_blkplt_item_topography = vie_skb.getTxtBlkpltItemTopography();
        	txt_blkplt_item_soil_type = vie_skb.getTxtBlkpltItemSoilType();
        	txt_blkplt_item_soil_category = vie_skb.getTxtBlkpltItemSoilCategory();
        	txt_blkplt_item_previous_block = vie_skb.getTxtBlkpltItemPreviousBlock();
        	txt_blkplt_item_prod_trees = vie_skb.getTxtBlkpltItemProdTrees();
        }

        txt_blkplt_item_id.setText(String.valueOf(blkplt.getId()));
        txt_blkplt_item_company_code.setText(blkplt.getCompanyCode());
        txt_blkplt_item_estate.setText(blkplt.getEstate());
        txt_blkplt_item_block.setText(blkplt.getBlock());
        txt_blkplt_item_valid_from.setText(blkplt.getValidFrom());
        txt_blkplt_item_valid_to.setText(blkplt.getValidTo());
        txt_blkplt_item_crop_type.setText(blkplt.getCropType());
        txt_blkplt_item_previous_crop.setText(blkplt.getPreviousCrop());
        txt_blkplt_item_finish_date.setText(blkplt.getFinishDate());
        txt_blkplt_item_reference.setText(blkplt.getReference());
        txt_blkplt_item_jarak_tanam.setText(blkplt.getJarakTanam());
        txt_blkplt_item_harvesting_date.setText(blkplt.getHarvestingDate());
        txt_blkplt_item_harvested.setText(String.valueOf(blkplt.getHarvested()));
        txt_blkplt_item_plan_date.setText(blkplt.getPlanDate());
        txt_blkplt_item_topography.setText(blkplt.getTopography());
        txt_blkplt_item_soil_type.setText(blkplt.getSoilType());
        txt_blkplt_item_soil_category.setText(blkplt.getSoilCategory());
        txt_blkplt_item_prod_trees.setText(String.valueOf(blkplt.getProdTrees()));

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lst_blkplt = null;
				lst_blkplt = (List<BLKPLT>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BLKPLT> list_result = new ArrayList<BLKPLT>();
				BLKPLT blkplt;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<BLKPLT>(lst_blkplt);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						blkplt = lst_temp.get(i);
						
						if(blkplt.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(blkplt);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
