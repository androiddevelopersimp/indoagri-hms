package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNCocoaReportTphItem;
import com.simp.hms.adapter.view.ViewBPNKaretReportTphItem;
import com.simp.hms.model.BPNCocoaReportTph;
import com.simp.hms.model.BPNKaretReportTph;
import com.simp.hms.model.DateLocal;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AdapterBPNCocoaReportTph extends BaseAdapter{
    private Context context;
    private List<BPNCocoaReportTph> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNCocoaReportTph(Context context, List<BPNCocoaReportTph> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNCocoaReportTph item = lstSummary.get(pos);

        ViewBPNCocoaReportTphItem view;
        TextView txtBpnReportTphItemTph;
        TextView txtBpnReportTphItemBpnDate;

        TextView txtBpnReportTphItemQtyGoodQty;
        TextView txtBpnReportTphItemQtyGoodWeight;
        TextView txtBpnReportTphItemQtyBadQty;
        TextView txtBpnReportTphItemQtyBadWeight;
        TextView txtBpnReportTphItemQtyPoorQty;
        TextView txtBpnReportTphItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtBpnReportTphItemTph = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemTph);
            txtBpnReportTphItemBpnDate = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemBpnDate);

            txtBpnReportTphItemQtyGoodQty = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyGoodQty);
            txtBpnReportTphItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyGoodWeight);
            txtBpnReportTphItemQtyBadQty = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyBadQty);
            txtBpnReportTphItemQtyBadWeight = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyBadWeight);
            txtBpnReportTphItemQtyPoorQty = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyPoorQty);
            txtBpnReportTphItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtBpnReportTphItemQtyPoorWeight);

            view = new ViewBPNCocoaReportTphItem(txtBpnReportTphItemTph, txtBpnReportTphItemBpnDate,
                    txtBpnReportTphItemQtyGoodQty, txtBpnReportTphItemQtyGoodWeight,
                    txtBpnReportTphItemQtyBadQty, txtBpnReportTphItemQtyBadWeight,
                    txtBpnReportTphItemQtyPoorQty, txtBpnReportTphItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewBPNCocoaReportTphItem) convertView.getTag();

            txtBpnReportTphItemTph = view.getTxtBpnReportTphItemTph();
            txtBpnReportTphItemBpnDate = view.getTxtBpnReportTphItemBpnDate();

            txtBpnReportTphItemQtyGoodQty = view.getTxtBpnReportTphItemQtyGoodQty();
            txtBpnReportTphItemQtyGoodWeight = view.getTxtBpnReportTphItemQtyGoodWeight();
            txtBpnReportTphItemQtyBadQty = view.getTxtBpnReportTphItemQtyBadQty();
            txtBpnReportTphItemQtyBadWeight = view.getTxtBpnReportTphItemQtyBadWeight();
            txtBpnReportTphItemQtyPoorQty = view.getTxtBpnReportTphItemQtyPoorQty();
            txtBpnReportTphItemQtyPoorWeight = view.getTxtBpnReportTphItemQtyPoorWeight();
        }

        txtBpnReportTphItemTph.setText(item.getTph());
        txtBpnReportTphItemBpnDate.setText(new DateLocal(new Date(item.getCreatedDate())).getDateString(DateLocal.FORMAT_REPORT_VIEW));

        txtBpnReportTphItemQtyGoodQty.setText(String.valueOf(Utils.round(item.getQtyGoodQty(), 0)));
        txtBpnReportTphItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 0)));
        txtBpnReportTphItemQtyBadQty.setText(String.valueOf(Utils.round(item.getQtyBadQty(), 0)));
        txtBpnReportTphItemQtyBadWeight.setText(String.valueOf(Utils.round(item.getQtyBadWeight(), 0)));
        txtBpnReportTphItemQtyPoorQty.setText(String.valueOf(Utils.round(item.getQtyPoorQty(), 0)));
        txtBpnReportTphItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 0)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNCocoaReportTph>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNCocoaReportTph> lstResult = new ArrayList<BPNCocoaReportTph>();
                BPNCocoaReportTph item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNCocoaReportTph>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getTph().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
