package com.simp.hms.adapter.view;

import android.widget.TextView;

/**
 * Created by Fernando.Siagian on 30/10/2017.
 */

public class ViewBPNKaretReportBlockItem {
    private TextView txtBpnReportBlockItemBlock;

    private TextView txtBpnReportBlockItemQtyLatexWet;
    private TextView txtBpnReportBlockItemQtyLatexDRC;
    private TextView txtBpnReportBlockItemQtyLatexDry;
    private TextView txtBpnReportBlockItemQtyLumpWet;
    private TextView txtBpnReportBlockItemQtyLumpDRC;
    private TextView txtBpnReportBlockItemQtyLumpDry;
    private TextView txtBpnReportBlockItemQtySlabWet;
    private TextView txtBpnReportBlockItemQtySlabDRC;
    private TextView txtBpnReportBlockItemQtySlabDry;
    private TextView txtBpnReportBlockItemQtyTreelaceWet;
    private TextView txtBpnReportBlockItemQtyTreelaceDRC;
    private TextView txtBpnReportBlockItemQtyTreelaceDry;

    public ViewBPNKaretReportBlockItem(
            TextView txtBpnReportBlockItemBlock,
            TextView txtBpnReportBlockItemQtyLatexWet,
            TextView txtBpnReportBlockItemQtyLatexDRC,
            TextView txtBpnReportBlockItemQtyLatexDry,
            TextView txtBpnReportBlockItemQtyLumpWet,
            TextView txtBpnReportBlockItemQtyLumpDRC,
            TextView txtBpnReportBlockItemQtyLumpDry,
            TextView txtBpnReportBlockItemQtySlabWet,
            TextView txtBpnReportBlockItemQtySlabDRC,
            TextView txtBpnReportBlockItemQtySlabDry,
            TextView txtBpnReportBlockItemQtyTreelaceWet,
            TextView txtBpnReportBlockItemQtyTreelaceDRC,
            TextView txtBpnReportBlockItemQtyTreelaceDry) {
        super();
        this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
        this.txtBpnReportBlockItemQtyLatexWet = txtBpnReportBlockItemQtyLatexWet;
        this.txtBpnReportBlockItemQtyLatexDRC = txtBpnReportBlockItemQtyLatexDRC;
        this.txtBpnReportBlockItemQtyLatexDry = txtBpnReportBlockItemQtyLatexDry;
        this.txtBpnReportBlockItemQtyLumpWet = txtBpnReportBlockItemQtyLumpWet;
        this.txtBpnReportBlockItemQtyLumpDRC = txtBpnReportBlockItemQtyLumpDRC;
        this.txtBpnReportBlockItemQtyLumpDry = txtBpnReportBlockItemQtyLumpDry;
        this.txtBpnReportBlockItemQtySlabWet = txtBpnReportBlockItemQtySlabWet;
        this.txtBpnReportBlockItemQtySlabDRC = txtBpnReportBlockItemQtySlabDRC;
        this.txtBpnReportBlockItemQtySlabDry = txtBpnReportBlockItemQtySlabDry;
        this.txtBpnReportBlockItemQtyTreelaceWet = txtBpnReportBlockItemQtyTreelaceWet;
        this.txtBpnReportBlockItemQtyTreelaceDRC = txtBpnReportBlockItemQtyTreelaceDRC;
        this.txtBpnReportBlockItemQtyTreelaceDry = txtBpnReportBlockItemQtyTreelaceDry;
    }

    public TextView getTxtBpnReportBlockItemBlock() {
        return txtBpnReportBlockItemBlock;
    }

    public void setTxtBpnReportBlockItemBlock(
            TextView txtBpnReportBlockItemBlock) {
        this.txtBpnReportBlockItemBlock = txtBpnReportBlockItemBlock;
    }

    public TextView getTxtBpnReportBlockItemQtyLatexWet() {
        return txtBpnReportBlockItemQtyLatexWet;
    }

    public void setTxtBpnReportBlockItemQtyLatexWet(
            TextView txtBpnReportBlockItemQtyLatexWet) {
        this.txtBpnReportBlockItemQtyLatexWet = txtBpnReportBlockItemQtyLatexWet;
    }

    public TextView getTxtBpnReportBlockItemQtyLatexDRC() {
        return txtBpnReportBlockItemQtyLatexDRC;
    }

    public void setTxtBpnReportBlockItemQtyLatexDRC(
            TextView txtBpnReportBlockItemQtyLatexDRC) {
        this.txtBpnReportBlockItemQtyLatexDRC = txtBpnReportBlockItemQtyLatexDRC;
    }

    public TextView getTxtBpnReportBlockItemQtyLatexDry() {
        return txtBpnReportBlockItemQtyLatexDry;
    }

    public void setTxtBpnReportBlockItemQtyLatexDry(
            TextView txtBpnReportBlockItemQtyLatexDry) {
        this.txtBpnReportBlockItemQtyLatexDry = txtBpnReportBlockItemQtyLatexDry;
    }

    public TextView getTxtBpnReportBlockItemQtyLumpWet() {
        return txtBpnReportBlockItemQtyLumpWet;
    }

    public void setTxtBpnReportBlockItemQtyLumpWet(
            TextView txtBpnReportBlockItemQtyLumpWet) {
        this.txtBpnReportBlockItemQtyLumpWet = txtBpnReportBlockItemQtyLumpWet;
    }

    public TextView getTxtBpnReportBlockItemQtyLumpDRC() {
        return txtBpnReportBlockItemQtyLumpDRC;
    }

    public void setTxtBpnReportBlockItemQtyLumpDRC(
            TextView txtBpnReportBlockItemQtyLumpDRC) {
        this.txtBpnReportBlockItemQtyLumpDRC = txtBpnReportBlockItemQtyLumpDRC;
    }

    public TextView getTxtBpnReportBlockItemQtyLumpDry() {
        return txtBpnReportBlockItemQtyLumpDry;
    }

    public void setTxtBpnReportBlockItemQtyLumpDry(
            TextView txtBpnReportBlockItemQtyLumpDry) {
        this.txtBpnReportBlockItemQtyLumpDry = txtBpnReportBlockItemQtyLumpDry;
    }

    public TextView getTxtBpnReportBlockItemQtySlabWet() {
        return txtBpnReportBlockItemQtySlabWet;
    }

    public void setTxtBpnReportBlockItemQtySlabWet(
            TextView txtBpnReportBlockItemQtySlabWet) {
        this.txtBpnReportBlockItemQtySlabWet = txtBpnReportBlockItemQtySlabWet;
    }

    public TextView getTxtBpnReportBlockItemQtySlabDRC() {
        return txtBpnReportBlockItemQtySlabDRC;
    }

    public void setTxtBpnReportBlockItemQtySlabDRC(
            TextView txtBpnReportBlockItemQtySlabDRC) {
        this.txtBpnReportBlockItemQtySlabDRC = txtBpnReportBlockItemQtySlabDRC;
    }

    public TextView getTxtBpnReportBlockItemQtySlabDry() {
        return txtBpnReportBlockItemQtySlabDry;
    }

    public void setTxtBpnReportBlockItemQtySlabDry(
            TextView txtBpnReportBlockItemQtySlabDry) {
        this.txtBpnReportBlockItemQtySlabDry = txtBpnReportBlockItemQtySlabDry;
    }

    public TextView getTxtBpnReportBlockItemQtyTreelaceWet() {
        return txtBpnReportBlockItemQtyTreelaceWet;
    }

    public void setTxtBpnReportBlockItemQtyTreelaceWet(
            TextView txtBpnReportBlockItemQtyTreelaceWet) {
        this.txtBpnReportBlockItemQtyTreelaceWet = txtBpnReportBlockItemQtyTreelaceWet;
    }

    public TextView getTxtBpnReportBlockItemQtyTreelaceDRC() {
        return txtBpnReportBlockItemQtyTreelaceDRC;
    }

    public void setTxtBpnReportBlockItemQtyTreelaceDRC(
            TextView txtBpnReportBlockItemQtyTreelaceDRC) {
        this.txtBpnReportBlockItemQtyTreelaceDRC = txtBpnReportBlockItemQtyTreelaceDRC;
    }

    public TextView getTxtBpnReportBlockItemQtyTreelaceDry() {
        return txtBpnReportBlockItemQtyTreelaceDry;
    }

    public void setTxtBpnReportBlockItemQtyTreelaceDry(
            TextView txtBpnReportBlockItemQtyTreelaceDry) {
        this.txtBpnReportBlockItemQtyTreelaceDry = txtBpnReportBlockItemQtyTreelaceDry;
    }

}
