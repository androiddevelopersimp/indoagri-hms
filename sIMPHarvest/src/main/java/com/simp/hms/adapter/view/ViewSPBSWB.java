package com.simp.hms.adapter.view;

import android.widget.LinearLayout;
import android.widget.TextView;

public class ViewSPBSWB {

    private TextView wbNumber;
    private TextView spbsNumber;
    private TextView nettWeight;
    private LinearLayout llWB;

    public ViewSPBSWB() {
        super();
    }

    public ViewSPBSWB(TextView wbNumber, TextView spbsNumber,TextView nettWeight, LinearLayout llWB) {
        super();
        this.wbNumber = wbNumber;
        this.spbsNumber = spbsNumber;
        this.nettWeight = nettWeight;
        this.llWB = llWB;
    }

    public TextView getWbNumber() {
        return wbNumber;
    }

    public void setWbNumber(TextView wbNumber) {
        this.wbNumber = wbNumber;
    }

    public TextView getSpbsNumber() {
        return spbsNumber;
    }

    public void setSpbsNumber(TextView spbsNumber) {
        this.spbsNumber = spbsNumber;
    }

    public LinearLayout getLlWB() {
        return llWB;
    }

    public void setLlWB(LinearLayout llWB) {
        this.llWB = llWB;
    }

    public TextView getNettWeight() {
        return nettWeight;
    }

    public void setNettWeight(TextView nettWeight) {
        this.nettWeight = nettWeight;
    }
}
