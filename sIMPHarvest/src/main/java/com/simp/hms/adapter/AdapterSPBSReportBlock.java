package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSReportBlockItem;
import com.simp.hms.model.SPBSReportBlock;
import com.simp.hms.routines.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSPBSReportBlock extends BaseAdapter{
	private Context context;
	private List<SPBSReportBlock> lstSummary, lstTemp;
	private int layout;
	
	public AdapterSPBSReportBlock(Context context, List<SPBSReportBlock> lstSummary, int layout){
		this.context = context;
		this.lstSummary = lstSummary;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstSummary.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstSummary.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		SPBSReportBlock item = lstSummary.get(pos);
		
		ViewSPBSReportBlockItem view;
		TextView txtSPBSReportBlockItemSpbsNumber;
		TextView txtSPBSReportBlockItemSpbsDate;
		TextView txtSPBSReportBlockItemBlock;
		TextView txtSPBSReportBlockItemQtyJanjang;
		TextView txtSPBSReportBlockItemQtyLooseFruit;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtSPBSReportBlockItemSpbsNumber = (TextView) convertView.findViewById(R.id.txtSpbsReportBlockItemSpbsNumber);
            txtSPBSReportBlockItemSpbsDate = (TextView) convertView.findViewById(R.id.txtSpbsReportBlockItemSpbsDate);
            txtSPBSReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtSpbsReportBlockItemBlock);
            txtSPBSReportBlockItemQtyJanjang = (TextView) convertView.findViewById(R.id.txtSpbsReportBlockItemQtyJanjang);
            txtSPBSReportBlockItemQtyLooseFruit = (TextView) convertView.findViewById(R.id.txtSpbsReportBlockItemQtyLooseFruit);
            
            view = new ViewSPBSReportBlockItem(txtSPBSReportBlockItemSpbsNumber, txtSPBSReportBlockItemSpbsDate, txtSPBSReportBlockItemBlock,
            		txtSPBSReportBlockItemQtyJanjang, txtSPBSReportBlockItemQtyLooseFruit);
             
            convertView.setTag(view);
        }else{
        	view = (ViewSPBSReportBlockItem) convertView.getTag();
        	
        	txtSPBSReportBlockItemSpbsNumber = view.getTxtSpbsReportBlockItemSpbsNumber();
        	txtSPBSReportBlockItemSpbsDate = view.getTxtSpbsReportBlockItemSpbsDate();
        	txtSPBSReportBlockItemBlock = view.getTxtSpbsReportBlockItemBlock();
        	txtSPBSReportBlockItemQtyJanjang = view.getTxtSpbsReportBlockItemQtyJanjang();
        	txtSPBSReportBlockItemQtyLooseFruit = view.getTxtSpbsReportBlockItemQtyLooseFruit();
        }

        txtSPBSReportBlockItemSpbsNumber.setText(item.getSpbsNumber());
        txtSPBSReportBlockItemSpbsDate.setText(item.getSpbsDate());
        txtSPBSReportBlockItemBlock.setText(item.getBlock());
        txtSPBSReportBlockItemQtyJanjang.setText(String.valueOf((int) item.getQtyJanjangAngkut()));
        txtSPBSReportBlockItemQtyLooseFruit.setText(String.valueOf(Utils.round(item.getQtyLooseFruitAngkut(), 2)));
        
        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstSummary = null;
				lstSummary = (List<SPBSReportBlock>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<SPBSReportBlock> lstResult = new ArrayList<SPBSReportBlock>();
				SPBSReportBlock item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<SPBSReportBlock>(lstSummary);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
