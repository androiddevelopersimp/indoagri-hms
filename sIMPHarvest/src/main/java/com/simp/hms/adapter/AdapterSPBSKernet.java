package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPBSKernetItem;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.model.Employee;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterSPBSKernet extends BaseAdapter {
	private Activity activity;
	private List<Employee> lstEmployee, lstTemp;
	private int layout;
	
	public AdapterSPBSKernet(Activity activity, List<Employee> lstEmployee, int layout){
		this.activity = activity;
		this.lstEmployee = lstEmployee;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstEmployee.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstEmployee.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		final Employee item = lstEmployee.get(pos);
		
		ViewSPBSKernetItem view;
		Button btnSpbsKernetItemDelete;
		TextView txtSpbsKernetItemName;
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            btnSpbsKernetItemDelete = (Button) convertView.findViewById(R.id.btnSpbsKernetItemDelete);
            txtSpbsKernetItemName = (TextView) convertView.findViewById(R.id.txtSpbsKernetItemName);
            
            view = new ViewSPBSKernetItem(btnSpbsKernetItemDelete, txtSpbsKernetItemName);
             
            convertView.setTag(view);
        }else{
        	view = (ViewSPBSKernetItem) convertView.getTag();
        	
        	btnSpbsKernetItemDelete = view.getBtnSpbsKernetItemDelete();
        	txtSpbsKernetItemName = view.getTxtSpbsKernetItemName();
        }

        txtSpbsKernetItemName.setText(item.getName());
        
        btnSpbsKernetItemDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new DialogConfirm(activity, activity.getResources().getString(R.string.informasi),
						activity.getResources().getString(R.string.data_delete), item, pos).show();
			}
		});
        
        return convertView;
	}
	
	public void addData(Employee employee){
		lstEmployee.add(employee);
		
		notifyDataSetChanged();
	}
	
	public void deleteData(int pos){
		lstEmployee.remove(pos);
		notifyDataSetChanged();
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lstEmployee = null;
				lstEmployee = (List<Employee>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<Employee> lstResult = new ArrayList<Employee>();
				Employee item;
				
				if(lstTemp == null){
					lstTemp = new ArrayList<Employee>(lstEmployee);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lstTemp.size();
					result.values = lstTemp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lstTemp.size(); i++){
						item = lstTemp.get(i);
						
						if(item.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
							lstResult.add(item);
						}
					}
					result.count = lstResult.size();
					result.values = lstResult;
				}
				
				return result;
			}
		};
		return filter;
	}
}
