package com.simp.hms.adapter.view;

import android.widget.TextView;

/**
 * Created by Anka.Wirawan on 8/5/2016.
 */
public class ViewSptaHistoryItem {
    private TextView txtSptaHistoryItemSptaNum;
    private TextView txtSptaHistoryItemSptaDate;
    private TextView txtSptaHistoryItemPetak;

    public ViewSptaHistoryItem() {
    }

    public ViewSptaHistoryItem(TextView txtSptaHistoryItemSptaNum, TextView txtSptaHistoryItemSptaDate,
                               TextView txtSptaHistoryItemPetak) {
        this.txtSptaHistoryItemSptaNum = txtSptaHistoryItemSptaNum;
        this.txtSptaHistoryItemSptaDate = txtSptaHistoryItemSptaDate;
        this.txtSptaHistoryItemPetak = txtSptaHistoryItemPetak;
    }

    public TextView getTxtSptaHistoryItemSptaNum() {
        return txtSptaHistoryItemSptaNum;
    }

    public void setTxtSptaHistoryItemSptaNum(TextView txtSptaHistoryItemSptaNum) {
        this.txtSptaHistoryItemSptaNum = txtSptaHistoryItemSptaNum;
    }

    public TextView getTxtSptaHistoryItemSptaDate() {
        return txtSptaHistoryItemSptaDate;
    }

    public void setTxtSptaHistoryItemSptaDate(TextView txtSptaHistoryItemSptaDate) {
        this.txtSptaHistoryItemSptaDate = txtSptaHistoryItemSptaDate;
    }

    public TextView getTxtSptaHistoryItemPetak() {
        return txtSptaHistoryItemPetak;
    }

    public void setTxtSptaHistoryItemPetak(TextView txtSptaHistoryItemPetak) {
        this.txtSptaHistoryItemPetak = txtSptaHistoryItemPetak;
    }
}
