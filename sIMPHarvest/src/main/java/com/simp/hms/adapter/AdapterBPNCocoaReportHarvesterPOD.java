package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNCocoaReportHarvesterItemPOD;
import com.simp.hms.model.BPNCocoaReportHarvesterPOD;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterBPNCocoaReportHarvesterPOD extends BaseAdapter {
    private Context context;
    private List<BPNCocoaReportHarvesterPOD> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNCocoaReportHarvesterPOD(Context context, List<BPNCocoaReportHarvesterPOD> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }


    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNCocoaReportHarvesterPOD item = lstSummary.get(pos);

        ViewBPNCocoaReportHarvesterItemPOD view;
        TextView txtBpnReportHarvesterItemHarvesterName;

        TextView txtBpnReportHarvesterItemQtyPODQty;
        TextView txtBpnReportHarvesterItemQtyGoodWeight;
        TextView txtBpnReportHarvesterItemQtyEstimasiQty;
        TextView txtBpnReportHarvesterItemQtyPoorWeight;

        if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
            txtBpnReportHarvesterItemHarvesterName = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemHarvesterName);

            txtBpnReportHarvesterItemQtyPODQty = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyPODQty);
            txtBpnReportHarvesterItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyGoodWeight);
            txtBpnReportHarvesterItemQtyEstimasiQty = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyEstimasiQty);
            txtBpnReportHarvesterItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtBpnReportHarvesterItemQtyPoorWeight);

            view = new ViewBPNCocoaReportHarvesterItemPOD(txtBpnReportHarvesterItemHarvesterName,
                    txtBpnReportHarvesterItemQtyPODQty, txtBpnReportHarvesterItemQtyGoodWeight,
                    txtBpnReportHarvesterItemQtyEstimasiQty, txtBpnReportHarvesterItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewBPNCocoaReportHarvesterItemPOD) convertView.getTag();

            txtBpnReportHarvesterItemHarvesterName = view.getTxtBpnReportHarvesterItemHarvesterName();
            txtBpnReportHarvesterItemQtyPODQty = view.getTxtBpnReportHarvesterItemQtyPODQty();
            txtBpnReportHarvesterItemQtyGoodWeight = view.getTxtBpnReportHarvesterItemQtyGoodWeight();
            txtBpnReportHarvesterItemQtyEstimasiQty = view.getTxtBpnReportHarvesterItemQtyEstimasiQty();
            txtBpnReportHarvesterItemQtyPoorWeight = view.getTxtBpnReportHarvesterItemQtyPoorWeight();
        }

        txtBpnReportHarvesterItemHarvesterName.setText(item.getName());
        txtBpnReportHarvesterItemQtyPODQty.setText(String.valueOf(Utils.round(item.getQtyPODQty(), 0)));
        txtBpnReportHarvesterItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 0)));
        txtBpnReportHarvesterItemQtyEstimasiQty.setText(String.valueOf(Utils.round(item.getQtyEstimasiQty(), 0)));
        txtBpnReportHarvesterItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 0)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNCocoaReportHarvesterPOD>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNCocoaReportHarvesterPOD> lstResult = new ArrayList<BPNCocoaReportHarvesterPOD>();
                BPNCocoaReportHarvesterPOD item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNCocoaReportHarvesterPOD>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getName().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
