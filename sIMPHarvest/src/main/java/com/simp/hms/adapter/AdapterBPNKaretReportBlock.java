package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNKaretReportBlockItem;
import com.simp.hms.adapter.view.ViewBPNReportBlockItem;
import com.simp.hms.model.BPNKaretReportBlock;
import com.simp.hms.model.BPNReportBlock;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Fernando.Siagian on 30/10/2017.
 */

public class AdapterBPNKaretReportBlock extends BaseAdapter {
    private Context context;
    private List<BPNKaretReportBlock> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNKaretReportBlock(Context context, List<BPNKaretReportBlock> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNKaretReportBlock item = lstSummary.get(pos);

        ViewBPNKaretReportBlockItem view;
        TextView txtBpnReportBlockItemBlock;

        TextView txtBpnReportBlockItemQtyLatexWet;
        TextView txtBpnReportBlockItemQtyLatexDRC;
        TextView txtBpnReportBlockItemQtyLatexDry;
        TextView txtBpnReportBlockItemQtyLumpWet;
        TextView txtBpnReportBlockItemQtyLumpDRC;
        TextView txtBpnReportBlockItemQtyLumpDry;
        TextView txtBpnReportBlockItemQtySlabWet;
        TextView txtBpnReportBlockItemQtySlabDRC;
        TextView txtBpnReportBlockItemQtySlabDry;
        TextView txtBpnReportBlockItemQtyTreelaceWet;
        TextView txtBpnReportBlockItemQtyTreelaceDRC;
        TextView txtBpnReportBlockItemQtyTreelaceDry;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtBpnReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemBlock);

            txtBpnReportBlockItemQtyLatexWet = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyLatexWet);
            txtBpnReportBlockItemQtyLatexDRC = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyLatexDRC);
            txtBpnReportBlockItemQtyLatexDry = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyLatexDry);
            txtBpnReportBlockItemQtyLumpWet = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyLumpWet);
            txtBpnReportBlockItemQtyLumpDRC = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyLumpDRC);
            txtBpnReportBlockItemQtyLumpDry = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyLumpDry);
            txtBpnReportBlockItemQtySlabWet = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtySlabWet);
            txtBpnReportBlockItemQtySlabDRC = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtySlabDRC);
            txtBpnReportBlockItemQtySlabDry = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtySlabDry);
            txtBpnReportBlockItemQtyTreelaceWet = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyTreelaceWet);
            txtBpnReportBlockItemQtyTreelaceDRC = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyTreelaceDRC);
            txtBpnReportBlockItemQtyTreelaceDry = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyTreelaceDry);

            view = new ViewBPNKaretReportBlockItem(txtBpnReportBlockItemBlock,
                    txtBpnReportBlockItemQtyLatexWet, txtBpnReportBlockItemQtyLatexDRC, txtBpnReportBlockItemQtyLatexDry,
                    txtBpnReportBlockItemQtyLumpWet, txtBpnReportBlockItemQtyLumpDRC, txtBpnReportBlockItemQtyLumpDry,
                    txtBpnReportBlockItemQtySlabWet, txtBpnReportBlockItemQtySlabDRC, txtBpnReportBlockItemQtySlabDry,
                    txtBpnReportBlockItemQtyTreelaceWet, txtBpnReportBlockItemQtyTreelaceDRC, txtBpnReportBlockItemQtyTreelaceDry);


            convertView.setTag(view);
        }else{
            view = (ViewBPNKaretReportBlockItem) convertView.getTag();

            txtBpnReportBlockItemBlock = view.getTxtBpnReportBlockItemBlock();
            txtBpnReportBlockItemQtyLatexWet = view.getTxtBpnReportBlockItemQtyLatexWet();
            txtBpnReportBlockItemQtyLatexDRC = view.getTxtBpnReportBlockItemQtyLatexDRC();
            txtBpnReportBlockItemQtyLatexDry = view.getTxtBpnReportBlockItemQtyLatexDry();
            txtBpnReportBlockItemQtyLumpWet = view.getTxtBpnReportBlockItemQtyLumpWet();
            txtBpnReportBlockItemQtyLumpDRC = view.getTxtBpnReportBlockItemQtyLumpDRC();
            txtBpnReportBlockItemQtyLumpDry = view.getTxtBpnReportBlockItemQtyLumpDry();
            txtBpnReportBlockItemQtySlabWet = view.getTxtBpnReportBlockItemQtySlabWet();
            txtBpnReportBlockItemQtySlabDRC = view.getTxtBpnReportBlockItemQtySlabDRC();
            txtBpnReportBlockItemQtySlabDry = view.getTxtBpnReportBlockItemQtySlabDry();
            txtBpnReportBlockItemQtyTreelaceWet = view.getTxtBpnReportBlockItemQtyTreelaceWet();
            txtBpnReportBlockItemQtyTreelaceDRC = view.getTxtBpnReportBlockItemQtyTreelaceDRC();
            txtBpnReportBlockItemQtyTreelaceDry = view.getTxtBpnReportBlockItemQtyTreelaceDry();
        }

        txtBpnReportBlockItemBlock.setText(item.getBlock());
        txtBpnReportBlockItemQtyLatexWet.setText(String.valueOf(Utils.round(item.getQtyLatexWet(), 2)));
        txtBpnReportBlockItemQtyLatexDRC.setText(String.valueOf(Utils.round(item.getQtyLatexDRC(), 2)));
        txtBpnReportBlockItemQtyLatexDry.setText(String.valueOf(Utils.round(item.getQtyLatexDry(), 2)));
        txtBpnReportBlockItemQtyLumpWet.setText(String.valueOf(Utils.round(item.getQtyLumpWet(), 2)));
        txtBpnReportBlockItemQtyLumpDRC.setText(String.valueOf(Utils.round(item.getQtyLumpDRC(), 2)));
        txtBpnReportBlockItemQtyLumpDry.setText(String.valueOf(Utils.round(item.getQtyLumpDry(), 2)));
        txtBpnReportBlockItemQtySlabWet.setText(String.valueOf(Utils.round(item.getQtySlabWet(), 2)));
        txtBpnReportBlockItemQtySlabDRC.setText(String.valueOf(Utils.round(item.getQtySlabDRC(), 2)));
        txtBpnReportBlockItemQtySlabDry.setText(String.valueOf(Utils.round(item.getQtySlabDry(), 2)));
        txtBpnReportBlockItemQtyTreelaceWet.setText(String.valueOf(Utils.round(item.getQtyTreelaceWet(), 2)));
        txtBpnReportBlockItemQtyTreelaceDRC.setText(String.valueOf(Utils.round(item.getQtyTreelaceDRC(), 2)));
        txtBpnReportBlockItemQtyTreelaceDry.setText(String.valueOf(Utils.round(item.getQtyTreelaceDry(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNKaretReportBlock>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNKaretReportBlock> lstResult = new ArrayList<BPNKaretReportBlock>();
                BPNKaretReportBlock item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNKaretReportBlock>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
