package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewSPUReportBlockItemPOD;
import com.simp.hms.model.SPUReportBlockPOD;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterSPUReportBlockPOD extends BaseAdapter {
    private Context context;
    private List<SPUReportBlockPOD> lstSummary, lstTemp;
    private int layout;

    public AdapterSPUReportBlockPOD(Context context, List<SPUReportBlockPOD> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        SPUReportBlockPOD item = lstSummary.get(pos);

        ViewSPUReportBlockItemPOD view;TextView txtSPBSReportBlockItemSpbsNumber;
        TextView txtSpuReportBlockItemBlock;
        TextView txtSpuReportBlockItemQtyPODQty;
        TextView txtSpuReportBlockItemQtyGoodWeight;
        TextView txtSpuReportBlockItemQtyEstimasiQty;
        TextView txtSpuReportBlockItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtSpuReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemBlock);
            txtSpuReportBlockItemQtyPODQty = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyPODQty);
            txtSpuReportBlockItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyGoodWeight);
            txtSpuReportBlockItemQtyEstimasiQty = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyEstimasiQty);
            txtSpuReportBlockItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtSpuReportBlockItemQtyPoorWeight);

            view = new ViewSPUReportBlockItemPOD(txtSpuReportBlockItemBlock, txtSpuReportBlockItemQtyPODQty, txtSpuReportBlockItemQtyGoodWeight,
                    txtSpuReportBlockItemQtyEstimasiQty, txtSpuReportBlockItemQtyPoorWeight);

            convertView.setTag(view);
        }else{
            view = (ViewSPUReportBlockItemPOD) convertView.getTag();

            txtSpuReportBlockItemBlock = view.getTxtSpuReportBlockItemBlock();
            txtSpuReportBlockItemQtyPODQty = view.getTxtSpuReportBlockItemQtyPODQty ();
            txtSpuReportBlockItemQtyGoodWeight = view.getTxtSpuReportBlockItemQtyGoodWeight();
            txtSpuReportBlockItemQtyEstimasiQty = view.getTxtSpuReportBlockItemQtyEstimasiQty();
            txtSpuReportBlockItemQtyPoorWeight = view.getTxtSpuReportBlockItemQtyPoorWeight();
        }
        txtSpuReportBlockItemBlock.setText(item.getBlock());
        txtSpuReportBlockItemQtyPODQty.setText(String.valueOf(Utils.round(item.getQtyPODQty(), 2)));
        txtSpuReportBlockItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 2)));
        txtSpuReportBlockItemQtyEstimasiQty.setText(String.valueOf(Utils.round(item.getQtyEstimasiQty(), 2)));
        txtSpuReportBlockItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 2)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<SPUReportBlockPOD>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<SPUReportBlockPOD> lstResult = new ArrayList<SPUReportBlockPOD>();
                SPUReportBlockPOD item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<SPUReportBlockPOD>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
