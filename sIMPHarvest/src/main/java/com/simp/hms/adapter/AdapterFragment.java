package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.simp.hms.model.FragmentItems;

public class AdapterFragment extends FragmentStatePagerAdapter {
//	protected static final String[] CONTENT = new String[] { "This", "Is", "A", "Test", "asdas"};
//	private List<Fragment> list_fragment;
	private List<FragmentItems> listFragment = new ArrayList<FragmentItems>();
	
	public AdapterFragment(FragmentManager fm) {
		super(fm);
	}
	
//	public void setListFragment(List<Fragment> list_fragment){
//		this.list_fragment = list_fragment;
//	}
//	
//	public List<Fragment> getListFragment(){
//		return list_fragment;
//	}
	
	public void setListFragment(List<FragmentItems> list_frag){
		this.listFragment = list_frag;
	}

	@Override
	public Fragment getItem(int pos) {
//		Fragment frag;
//		
//		frag = list_fragment.get(pos);	
//		return frag; 
		Fragment frag;
		
		frag = listFragment.get(pos).getFragment();
		
		return frag;
	}
	
	@Override
    public int getItemPosition(Object object){
	        return POSITION_NONE;
    }

	@Override
	public int getCount() {
//		return list_fragment.size();
		return listFragment.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
//		return "anka";
		return  listFragment.get(position).getName();
	}

	@Override
	public Object instantiateItem(ViewGroup view, int pos) {
		return super.instantiateItem(view, pos);
	}

	
}
