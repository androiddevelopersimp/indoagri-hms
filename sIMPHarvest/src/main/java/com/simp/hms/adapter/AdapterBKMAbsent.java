package com.simp.hms.adapter;

import java.util.List;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBKMAbsentItem;
import com.simp.hms.dialog.DialogConfirm;
import com.simp.hms.model.BKMLine;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AdapterBKMAbsent extends BaseAdapter{
	private Activity activity;
	private List<BKMLine> lstBkmAbsent;
	private int layout;
	
	private Handler repeatUpdateHandler = new Handler();
	
	private boolean mAutoIncrement = false;
	private boolean mAutoDecrement = false;
	
	public double mandays = 1;
	private BKMLine activeBkmAbsent;
	private int activePos;
	
	public AdapterBKMAbsent(Activity activity, List<BKMLine> lstBkmAbsent, int layout){
		this.activity = activity;
		this.lstBkmAbsent = lstBkmAbsent;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lstBkmAbsent.size();
	}

	@Override
	public Object getItem(int pos) {
		return lstBkmAbsent.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(final int pos, View convertView, ViewGroup parent) {
		ViewBKMAbsentItem vieBkmAbsent;
		TextView txtBkmAbsentItemName;
		TextView txtBkmAbsentItemAbsentType;
		Button btnBKMAbsentItemMandaysMin;
		final EditText edtBkmAbsentItemMandays;
		Button btnBKMAbsentItemMandaysPlus;
		Button btnBkmAbsentItemDelete;
		
		final BKMLine bkmAbsent = lstBkmAbsent.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
            txtBkmAbsentItemName = (TextView) convertView.findViewById(R.id.txtBkmAbsentItemName);
            txtBkmAbsentItemAbsentType = (TextView) convertView.findViewById(R.id.txtBkmAbsentItemAbsentType);
            btnBKMAbsentItemMandaysMin = (Button) convertView.findViewById(R.id.btnBKMAbsentItemMandaysMinus);
            edtBkmAbsentItemMandays = (EditText) convertView.findViewById(R.id.edtBkmAbsentItemMandays);
            btnBKMAbsentItemMandaysPlus = (Button) convertView.findViewById(R.id.btnBKMAbsentItemMandaysPlus);
            btnBkmAbsentItemDelete = (Button) convertView.findViewById(R.id.btnBkmAbsentItemDelete);
            
            vieBkmAbsent = new ViewBKMAbsentItem(txtBkmAbsentItemName, txtBkmAbsentItemAbsentType, btnBKMAbsentItemMandaysMin, edtBkmAbsentItemMandays, 
            		btnBKMAbsentItemMandaysPlus, btnBkmAbsentItemDelete, true);     
            convertView.setTag(vieBkmAbsent);
        }else{
        	vieBkmAbsent = (ViewBKMAbsentItem) convertView.getTag();
        	
        	txtBkmAbsentItemName = vieBkmAbsent.getTxtBkmAbsentItemName();
        	txtBkmAbsentItemAbsentType = vieBkmAbsent.getTxtBkmAbsentItemAbsentType();
        	btnBKMAbsentItemMandaysMin = vieBkmAbsent.getBtnBKMAbsentItemMandaysMin();
        	edtBkmAbsentItemMandays = vieBkmAbsent.getEdtBkmAbsentItemMandays();
        	btnBKMAbsentItemMandaysPlus = vieBkmAbsent.getBtnBKMAbsentItemMandaysPlus();
        	btnBkmAbsentItemDelete = vieBkmAbsent.getBtnBkmAbsentItemDelete();
        }
        
        txtBkmAbsentItemName.setText(bkmAbsent.getName());
        txtBkmAbsentItemAbsentType.setText(bkmAbsent.getAbsentType());
        edtBkmAbsentItemMandays.setText(String.valueOf(bkmAbsent.getMandays()));
        
        btnBKMAbsentItemMandaysMin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				decrement(edtBkmAbsentItemMandays, pos, bkmAbsent);
			}
		});
        
        btnBKMAbsentItemMandaysMin.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				mAutoDecrement = true;
	            repeatUpdateHandler.post( new RptUpdaterMin(edtBkmAbsentItemMandays, pos, bkmAbsent) );
	            
				return false;
			}
		});

        
        btnBKMAbsentItemMandaysMin.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if( (event.getAction() == MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL) && mAutoDecrement ){
					mAutoDecrement = false;
	            }
				
				return false;
			}
		});
        
        btnBKMAbsentItemMandaysPlus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				increment(edtBkmAbsentItemMandays, pos, bkmAbsent);
			}
		});
        
        btnBKMAbsentItemMandaysPlus.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				mAutoIncrement = true;
	            repeatUpdateHandler.post( new RptUpdaterPlus(edtBkmAbsentItemMandays, pos, bkmAbsent) );
				return false;
			}
		});
        
        btnBKMAbsentItemMandaysPlus.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if( (event.getAction()==MotionEvent.ACTION_UP || event.getAction()==MotionEvent.ACTION_CANCEL) && mAutoIncrement ){
					mAutoIncrement = false;
	            }
				return false;
			}
		});
        
        btnBkmAbsentItemDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				new DialogConfirm(activity, activity.getResources().getString(R.string.informasi), 
						String.format(activity.getResources().getString(R.string.data_delete_2), bkmAbsent.getName()), 
						bkmAbsent, R.id.btnBkmAbsentItemDelete).show();
				
				/*
				lstBkmAbsent.get(pos).setVisible(false);
				lstBkmAbsent.remove(pos);
				
				notifyDataSetChanged();
				*/
			}
		});

        return convertView;
	}
	
	public void updateData(int pos, BKMLine bkmAbsent){
		lstBkmAbsent.set(pos, bkmAbsent);
		
		notifyDataSetChanged();
	}
	
	public void addData(BKMLine bkmAbsent){
		lstBkmAbsent.add(bkmAbsent);
		
		notifyDataSetChanged();
	}
	
	public void increment(TextView v, int pos, BKMLine bkmAbsent){
		double temp = 0;
		
		if(bkmAbsent.getMandays() < 1){
			temp = bkmAbsent.getMandays() + 0.01;
			bkmAbsent.setMandays(temp);
			
			Log.d("tag", ":" + temp);
			
			activePos = pos;
			activeBkmAbsent = bkmAbsent;
			
			v.setText(String.valueOf(temp));
		}
	}
	
	public void decrement(TextView v, int pos, BKMLine bkmAbsent){
		double temp = 0;
		
		if(bkmAbsent.getMandays() > 0){
			temp = bkmAbsent.getMandays() - 0.01;
			bkmAbsent.setMandays(temp);
			
			Log.d("tag", ":" + temp);
			
			activePos = pos;   
			activeBkmAbsent = bkmAbsent;
			
			v.setText(String.valueOf(temp));
		}
	}
	
	private class RptUpdaterMin implements Runnable {
		private TextView v;
		private int pos;
		private BKMLine bkmAbsent;
		
		public RptUpdaterMin(TextView v, int pos, BKMLine bkmAbsent){
			this.v = v;
			this.pos = pos;
			this.bkmAbsent = bkmAbsent;
		}
		
	    public void run() {
	        if(mAutoDecrement){
	        	decrement(v, pos, bkmAbsent);
	            repeatUpdateHandler.postDelayed( new RptUpdaterMin(v, pos, bkmAbsent), 100);
	        }else{
	        	v.setText(String.valueOf(bkmAbsent.getMandays()));
	        	
	        	updateData(pos, bkmAbsent);
	        }
	    }
	}
	
	private class RptUpdaterPlus implements Runnable {
		private TextView v;
		private int pos;
		private BKMLine bkmAbsent;
		
		public RptUpdaterPlus(TextView v, int pos, BKMLine bkmAbsent){
			this.v = v;
			this.pos = pos;
			this.bkmAbsent = bkmAbsent;
		}
		
	    public void run() {
	        if(mAutoIncrement){
	            increment(v, pos, bkmAbsent);
	            repeatUpdateHandler.postDelayed( new RptUpdaterPlus(v, pos, bkmAbsent), 100);
	        }else{
	        	v.setText(String.valueOf(bkmAbsent.getMandays()));
	        	
	        	updateData(pos, bkmAbsent);
	        }
	    }
	}
	
	
}
