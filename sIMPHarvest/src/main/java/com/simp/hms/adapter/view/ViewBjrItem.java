package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBjrItem {
	private TextView txt_bjr_item_id;
	private TextView txt_bjr_item_company_code;
	private TextView txt_bjr_item_estate;
	private TextView txt_bjr_item_block;
	private TextView txt_bjr_item_eff_date;
	private TextView txt_bjr_item_bjr;
	private TextView txt_bjr_item_uom;
	private TextView txt_bjr_item_mandt;
	
	public ViewBjrItem(TextView txt_bjr_item_id, TextView txt_bjr_item_company_code, TextView txt_bjr_item_estate,
			TextView txt_bjr_item_block, TextView txt_bjr_item_eff_date, TextView txt_bjr_item_bjr, TextView txt_bjr_item_uom, TextView txt_bjr_item_mandt) {
		this.txt_bjr_item_id = txt_bjr_item_id;
		this.txt_bjr_item_company_code = txt_bjr_item_company_code;
		this.txt_bjr_item_estate = txt_bjr_item_estate;
		this.txt_bjr_item_block = txt_bjr_item_block;
		this.txt_bjr_item_eff_date = txt_bjr_item_eff_date;
		this.txt_bjr_item_bjr = txt_bjr_item_bjr;
		this.txt_bjr_item_uom = txt_bjr_item_uom;
		this.txt_bjr_item_mandt = txt_bjr_item_mandt;
	}
	
	public TextView getTxtBjrItemId() {
		return txt_bjr_item_id;
	}
	public void setTxtBjrItemId(TextView txt_bjr_item_id) {
		this.txt_bjr_item_id = txt_bjr_item_id;
	}
	public TextView getTxtBjrItemCompanyCode() {
		return txt_bjr_item_company_code;
	}
	public void setTxtBjrItemCompanyCode(TextView txt_bjr_item_company_code) {
		this.txt_bjr_item_company_code = txt_bjr_item_company_code;
	}
	public TextView getTxtBjrItemEstate() {
		return txt_bjr_item_estate;
	}
	public void setTxtBjrItemEstate(TextView txt_bjr_item_estate) {
		this.txt_bjr_item_estate = txt_bjr_item_estate;
	}
	public TextView getTxtBjrItemBlock() {
		return txt_bjr_item_block;
	}
	public void setTxtBjrItemBlock(TextView txt_bjr_item_block) {
		this.txt_bjr_item_block = txt_bjr_item_block;
	}
	public TextView getTxtBjrItemEffDate() {
		return txt_bjr_item_eff_date;
	}
	public void setTxtBjrItemEffDate(TextView txt_bjr_item_eff_date) {
		this.txt_bjr_item_eff_date = txt_bjr_item_eff_date;
	}
	public TextView getTxtBjrItemBjr() {
		return txt_bjr_item_bjr;
	}
	public void setTxtBjrItemBjr(TextView txt_bjr_item_bjr) {
		this.txt_bjr_item_bjr = txt_bjr_item_bjr;
	}
	public TextView getTxtBjrItemUom() {
		return txt_bjr_item_uom;
	}
	public void setTxtBjrItemUom(TextView txt_bjr_item_uom) {
		this.txt_bjr_item_uom = txt_bjr_item_uom;
	}
	public TextView getTxtBjrItemMandt() {
		return txt_bjr_item_mandt;
	}
	public void setTxtBjrItemMandt(TextView txt_bjr_item_mandt) {
		this.txt_bjr_item_mandt = txt_bjr_item_mandt;
	}
}
