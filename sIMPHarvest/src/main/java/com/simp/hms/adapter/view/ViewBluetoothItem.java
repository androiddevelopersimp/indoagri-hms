package com.simp.hms.adapter.view;

import android.widget.CheckBox;
import android.widget.TextView;

public class ViewBluetoothItem {
	private TextView txtBluetoothItemDeviceName;
	private TextView txtBluetoothItemDeviceAddress;
	private CheckBox cbxBluetoothItemDeviceSelected;

	public ViewBluetoothItem() {}

	public ViewBluetoothItem(TextView txtBluetoothItemDeviceName,
			TextView txtBluetoothItemDeviceAddress,
			CheckBox cbxBluetoothItemDeviceSelected) {
		super();
		this.txtBluetoothItemDeviceName = txtBluetoothItemDeviceName;
		this.txtBluetoothItemDeviceAddress = txtBluetoothItemDeviceAddress;
		this.cbxBluetoothItemDeviceSelected = cbxBluetoothItemDeviceSelected;
	}

	public TextView getTxtBluetoothItemDeviceName() {
		return txtBluetoothItemDeviceName;
	}

	public void setTxtBluetoothItemDeviceName(
			TextView txtBluetoothItemDeviceName) {
		this.txtBluetoothItemDeviceName = txtBluetoothItemDeviceName;
	}

	public TextView getTxtBluetoothItemDeviceAddress() {
		return txtBluetoothItemDeviceAddress;
	}

	public void setTxtBluetoothItemDeviceAddress(
			TextView txtBluetoothItemDeviceAddress) {
		this.txtBluetoothItemDeviceAddress = txtBluetoothItemDeviceAddress;
	}

	public CheckBox getCbxBluetoothItemDeviceSelected() {
		return cbxBluetoothItemDeviceSelected;
	}

	public void setCbxBluetoothItemDeviceSelected(
			CheckBox cbxBluetoothItemDeviceSelected) {
		this.cbxBluetoothItemDeviceSelected = cbxBluetoothItemDeviceSelected;
	}

}
