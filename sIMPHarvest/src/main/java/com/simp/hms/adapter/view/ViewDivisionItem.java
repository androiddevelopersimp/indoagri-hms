package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewDivisionItem {
	private TextView txtDivisionItemDivision;

	public ViewDivisionItem(TextView txtDivisionItemDivision) {
		super();
		this.txtDivisionItemDivision = txtDivisionItemDivision;
	}

	public TextView getTxtDivisionItemDivision() {
		return txtDivisionItemDivision;
	}

	public void setTxtDivisionItemDivision(TextView txtDivisionItemDivision) {
		this.txtDivisionItemDivision = txtDivisionItemDivision;
	}
}
