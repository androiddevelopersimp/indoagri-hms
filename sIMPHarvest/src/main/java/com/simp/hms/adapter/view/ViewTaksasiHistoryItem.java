package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewTaksasiHistoryItem {
	private TextView txtTaksasiHistoryItemTaksasiDate;
	private TextView txtTaksasiHistoryItemBlock;
	private TextView txtTaksasiHistoryItemForeman;

	public ViewTaksasiHistoryItem(TextView txtTaksasiHistoryItemTaksasiDate,
			TextView txtTaksasiHistoryItemBlock,
			TextView txtTaksasiHistoryItemForeman) {
		super();
		this.txtTaksasiHistoryItemTaksasiDate = txtTaksasiHistoryItemTaksasiDate;
		this.txtTaksasiHistoryItemBlock = txtTaksasiHistoryItemBlock;
		this.txtTaksasiHistoryItemForeman = txtTaksasiHistoryItemForeman;
	}

	public TextView getTxtTaksasiHistoryItemTaksasiDate() {
		return txtTaksasiHistoryItemTaksasiDate;
	}

	public void setTxtTaksasiHistoryItemTaksasiDate(
			TextView txtTaksasiHistoryItemTaksasiDate) {
		this.txtTaksasiHistoryItemTaksasiDate = txtTaksasiHistoryItemTaksasiDate;
	}

	public TextView getTxtTaksasiHistoryItemBlock() {
		return txtTaksasiHistoryItemBlock;
	}

	public void setTxtTaksasiHistoryItemBlock(
			TextView txtTaksasiHistoryItemBlock) {
		this.txtTaksasiHistoryItemBlock = txtTaksasiHistoryItemBlock;
	}

	public TextView getTxtTaksasiHistoryItemForeman() {
		return txtTaksasiHistoryItemForeman;
	}

	public void setTxtTaksasiHistoryItemForeman(
			TextView txtTaksasiHistoryItemForeman) {
		this.txtTaksasiHistoryItemForeman = txtTaksasiHistoryItemForeman;
	}

}
