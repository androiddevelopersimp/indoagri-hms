package com.simp.hms.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBlksbcItem;
import com.simp.hms.model.BLKSBC;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class AdapterBlksbc extends BaseAdapter{
	private Context context;
	private List<BLKSBC> lst_blksbc, lst_temp;
	private int layout;
	
	private ViewBlksbcItem vie_blksbc;
	private TextView txt_blksbc_item_id;
	private TextView txt_blksbc_item_company_code;
	private TextView txt_blksbc_item_estate;
	private TextView txt_blksbc_item_block;
	private TextView txt_blksbc_item_valid_from;
	private TextView txt_blksbc_item_basis_normal;
	private TextView txt_blksbc_item_basis_friday;
	private TextView txt_blksbc_item_basis_holiday;
	private TextView txt_blksbc_item_premi_normal;
	private TextView txt_blksbc_item_premi_friday;
	private TextView txt_blksbc_item_premi_holiday;
	
	public AdapterBlksbc(Context context, List<BLKSBC> lst_blksbc, int layout){
		this.context = context;
		this.lst_blksbc = lst_blksbc;
		this.layout = layout;
	}

	@Override
	public int getCount() {
		return lst_blksbc.size();
	}

	@Override
	public Object getItem(int pos) {
		return lst_blksbc.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		BLKSBC blksbc = lst_blksbc.get(pos);
		
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);
        
        	txt_blksbc_item_id = (TextView) convertView.findViewById(R.id.txt_blksbc_item_id);
        	txt_blksbc_item_company_code = (TextView) convertView.findViewById(R.id.txt_blksbc_item_company_code);
        	txt_blksbc_item_estate = (TextView) convertView.findViewById(R.id.txt_blksbc_item_estate);
        	txt_blksbc_item_block = (TextView) convertView.findViewById(R.id.txt_blksbc_item_block);
        	txt_blksbc_item_valid_from = (TextView) convertView.findViewById(R.id.txt_blksbc_item_valid_from);
        	txt_blksbc_item_basis_normal = (TextView) convertView.findViewById(R.id.txt_blksbc_item_basis_normal);
        	txt_blksbc_item_basis_friday = (TextView) convertView.findViewById(R.id.txt_blksbc_item_basis_friday);
        	txt_blksbc_item_basis_holiday = (TextView) convertView.findViewById(R.id.txt_blksbc_item_basis_holiday);
        	txt_blksbc_item_premi_normal = (TextView) convertView.findViewById(R.id.txt_blksbc_item_premi_normal);
        	txt_blksbc_item_premi_friday = (TextView) convertView.findViewById(R.id.txt_blksbc_item_premi_friday);
        	txt_blksbc_item_premi_holiday = (TextView) convertView.findViewById(R.id.txt_blksbc_item_premi_holiday);
            
            vie_blksbc = new ViewBlksbcItem(txt_blksbc_item_id, txt_blksbc_item_company_code, txt_blksbc_item_estate, txt_blksbc_item_block, 
            		txt_blksbc_item_valid_from, txt_blksbc_item_basis_normal, txt_blksbc_item_basis_friday, txt_blksbc_item_basis_holiday, 
            		txt_blksbc_item_premi_normal, txt_blksbc_item_premi_friday, txt_blksbc_item_premi_holiday);
            
            convertView.setTag(vie_blksbc);
        }else{
        	vie_blksbc = (ViewBlksbcItem) convertView.getTag();
        	
        	txt_blksbc_item_id = vie_blksbc.getTxtBlksbcItemId();
        	txt_blksbc_item_company_code = vie_blksbc.getTxtBlksbcItemCompanyCode();
        	txt_blksbc_item_estate = vie_blksbc.getTxtBlksbcItemEstate();
        	txt_blksbc_item_block = vie_blksbc.getTxtBlksbcItemBlock();
        	txt_blksbc_item_valid_from = vie_blksbc.getTxtBlksbcItemValidFrom();
        	txt_blksbc_item_basis_normal = vie_blksbc.getTxtBlksbcItemBasisNormal();
        	txt_blksbc_item_basis_friday = vie_blksbc.getTxtBlksbcItemBasisFriday();
        	txt_blksbc_item_basis_holiday = vie_blksbc.getTxtBlksbcItemBasisHoliday();
        	txt_blksbc_item_premi_normal = vie_blksbc.getTxtBlksbcItemPremiNormal();
        	txt_blksbc_item_premi_friday = vie_blksbc.getTxtBlksbcItemPremiFriday();
        	txt_blksbc_item_premi_holiday = vie_blksbc.getTxtBlksbcItemPremiHoliday();
        }

    	txt_blksbc_item_id.setText(String.valueOf(blksbc.getId()));
    	txt_blksbc_item_company_code.setText(blksbc.getCompanyCode());
    	txt_blksbc_item_estate.setText(blksbc.getEstate());
    	txt_blksbc_item_block.setText(blksbc.getBlock());
    	txt_blksbc_item_valid_from.setText(blksbc.getValidFrom());
    	txt_blksbc_item_basis_normal.setText(String.valueOf(blksbc.getBasisNormal()));
    	txt_blksbc_item_basis_friday.setText(String.valueOf(blksbc.getBasisFriday()));
    	txt_blksbc_item_basis_holiday.setText(String.valueOf(blksbc.getBasisHoliday()));
    	txt_blksbc_item_premi_normal.setText(String.valueOf(blksbc.getPremiNormal()));
    	txt_blksbc_item_premi_friday.setText(String.valueOf(blksbc.getPremiFriday()));
    	txt_blksbc_item_premi_holiday.setText(String.valueOf(blksbc.getPremiHoliday()));

        return convertView;
	}
	
	public Filter getFilter() {
		Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				lst_blksbc = null;
				lst_blksbc = (List<BLKSBC>) results.values;
				notifyDataSetChanged();
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults result = new FilterResults();
				List<BLKSBC> list_result = new ArrayList<BLKSBC>();
				BLKSBC blksbc;
				
				if(lst_temp == null){
					lst_temp = new ArrayList<BLKSBC>(lst_blksbc);
				}
				
				if(constraint == null || constraint.length() == 0){
					result.count = lst_temp.size();
					result.values = lst_temp;
				}else{
					constraint = constraint.toString().toLowerCase(Locale.getDefault());
					
					for(int i = 0; i < lst_temp.size(); i++){
						blksbc = lst_temp.get(i);
						
						if(blksbc.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
							list_result.add(blksbc);
						}
					}
					result.count = list_result.size();
					result.values = list_result;
				}
				
				return result;
			}
		};
		return filter;
	}
}
