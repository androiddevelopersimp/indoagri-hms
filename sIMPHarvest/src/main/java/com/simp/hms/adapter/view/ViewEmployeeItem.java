package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewEmployeeItem {
	private TextView txt_employee_item_id;
	private TextView txt_employee_item_company_code;
	private TextView txt_employee_item_estate;
	private TextView txt_employee_item_fiscal_year;
	private TextView txt_employee_item_fiscal_period;
	private TextView txt_employee_item_nik;
	private TextView txt_employee_item_name;
	private TextView txt_employee_item_term_date;
	private TextView txt_employee_item_divisi;
	private TextView txt_employee_item_role_id;
	private TextView txt_employee_item_job_pos;
	private TextView txt_employee_item_gang_code;
	private TextView txt_employee_item_cost_center;
	private TextView txt_employee_item_emp_type;
	private TextView txt_employee_item_valid_from;
	private TextView txt_employee_item_harvester_code;
	
	public ViewEmployeeItem(TextView txt_employee_item_id, TextView txt_employee_item_company_code,
			TextView txt_employee_item_estate, TextView txt_employee_item_fiscal_year,
			TextView txt_employee_item_fiscal_period, TextView txt_employee_item_nik, TextView txt_employee_item_name,
			TextView txt_employee_item_term_date, TextView txt_employee_item_divisi,
			TextView txt_employee_item_role_id, TextView txt_employee_item_job_pos,
			TextView txt_employee_item_gang_code, TextView txt_employee_item_cost_center, TextView txt_employee_item_emp_type,
			TextView txt_employee_item_valid_from, TextView txt_employee_item_harvester_code) {
		this.txt_employee_item_id = txt_employee_item_id;
		this.txt_employee_item_company_code = txt_employee_item_company_code;
		this.txt_employee_item_estate = txt_employee_item_estate;
		this.txt_employee_item_fiscal_year = txt_employee_item_fiscal_year;
		this.txt_employee_item_fiscal_period = txt_employee_item_fiscal_period;
		this.txt_employee_item_nik = txt_employee_item_nik;
		this.txt_employee_item_name = txt_employee_item_name;
		this.txt_employee_item_term_date = txt_employee_item_term_date;
		this.txt_employee_item_divisi = txt_employee_item_divisi;
		this.txt_employee_item_role_id = txt_employee_item_role_id;
		this.txt_employee_item_job_pos = txt_employee_item_job_pos;
		this.txt_employee_item_gang_code = txt_employee_item_gang_code;
		this.txt_employee_item_cost_center = txt_employee_item_cost_center;
		this.txt_employee_item_emp_type = txt_employee_item_emp_type;
		this.txt_employee_item_valid_from = txt_employee_item_valid_from;
		this.txt_employee_item_harvester_code = txt_employee_item_harvester_code;
	}
	
	public TextView getTxtEmployeeItemId() {
		return txt_employee_item_id;
	}
	public void setTxtEmployeeItemId(TextView txt_employee_item_id) {
		this.txt_employee_item_id = txt_employee_item_id;
	}
	public TextView getTxtEmployeeItemCompanyCode() {
		return txt_employee_item_company_code;
	}
	public void setTxtEmployeeItemCompanyCode(
			TextView txt_employee_item_company_code) {
		this.txt_employee_item_company_code = txt_employee_item_company_code;
	}
	public TextView getTxtEmployeeItemEstate() {
		return txt_employee_item_estate;
	}
	public void setTxtEmployeeItemEstate(TextView txt_employee_item_estate) {
		this.txt_employee_item_estate = txt_employee_item_estate;
	}
	public TextView getTxtEmployeeItemFiscalYear() {
		return txt_employee_item_fiscal_year;
	}
	public void setTxtEmployeeItemFiscalYear(
			TextView txt_employee_item_fiscal_year) {
		this.txt_employee_item_fiscal_year = txt_employee_item_fiscal_year;
	}
	public TextView getTxtEmployeeItemFiscalPeriod() {
		return txt_employee_item_fiscal_period;
	}
	public void setTxtEmployeeItemFiscalPeriod(
			TextView txt_employee_item_fiscal_period) {
		this.txt_employee_item_fiscal_period = txt_employee_item_fiscal_period;
	}
	public TextView getTxtEmployeeItemNik() {
		return txt_employee_item_nik;
	}
	public void setTxtEmployeeItemNik(TextView txt_employee_item_nik) {
		this.txt_employee_item_nik = txt_employee_item_nik;
	}
	public TextView getTxtEmployeeItemName() {
		return txt_employee_item_name;
	}
	public void setTxtEmployeeItemName(TextView txt_employee_item_name) {
		this.txt_employee_item_name = txt_employee_item_name;
	}
	public TextView getTxtEmployeeItemTermDate() {
		return txt_employee_item_term_date;
	}
	public void setTxtEmployeeItemTermDate(TextView txt_employee_item_term_date) {
		this.txt_employee_item_term_date = txt_employee_item_term_date;
	}
	public TextView getTxtEmployeeItemDivisi() {
		return txt_employee_item_divisi;
	}
	public void setTxtEmployeeItemDivisi(TextView txt_employee_item_divisi) {
		this.txt_employee_item_divisi = txt_employee_item_divisi;
	}
	public TextView getTxtEmployeeItemRoleId() {
		return txt_employee_item_role_id;
	}
	public void setTxtEmployeeItemRoleId(TextView txt_employee_item_role_id) {
		this.txt_employee_item_role_id = txt_employee_item_role_id;
	}
	public TextView getTxtEmployeeItemJobPos() {
		return txt_employee_item_job_pos;
	}
	public void setTxtEmployeeItemJobPos(TextView txt_employee_item_job_pos) {
		this.txt_employee_item_job_pos = txt_employee_item_job_pos;
	}
	public TextView getTxtEmployeeItemGangCode() {
		return txt_employee_item_gang_code;
	}
	public void setTxtEmployeeItemGangCode(TextView txt_employee_item_gang_code) {
		this.txt_employee_item_gang_code = txt_employee_item_gang_code;
	}
	public TextView getTxtEmployeeItemCostCenter() {
		return txt_employee_item_cost_center;
	}
	public void setTxtEmployeeItemCostCenter(
			TextView txt_employee_item_cost_center) {
		this.txt_employee_item_cost_center = txt_employee_item_cost_center;
	}
	public TextView getTxtEmployeeItemEmType() {
		return txt_employee_item_emp_type;
	}
	public void setTxtEmployeeItem_empType(TextView txt_employee_item_emp_type) {
		this.txt_employee_item_emp_type = txt_employee_item_emp_type;
	}
	public TextView getTxtEmployeeItemValidFrom() {
		return txt_employee_item_valid_from;
	}
	public void setTxtEmployeeItemValidFrom(
			TextView txt_employee_item_valid_from) {
		this.txt_employee_item_valid_from = txt_employee_item_valid_from;
	}

	public TextView getTxtEmployeeItemHarvesterCode() {
		return txt_employee_item_harvester_code;
	}

	public void setTxtEmployeeItemHarvesterCode(
			TextView txt_employee_item_harvester_code) {
		this.txt_employee_item_harvester_code = txt_employee_item_harvester_code;
	}
	
	
}


