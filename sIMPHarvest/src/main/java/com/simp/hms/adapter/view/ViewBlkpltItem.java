package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBlkpltItem {
	private TextView txt_blkplt_item_id;
	private TextView txt_blkplt_item_company_code;
	private TextView txt_blkplt_item_estate;
	private TextView txt_blkplt_item_block;
	private TextView txt_blkplt_item_valid_from;
	private TextView txt_blkplt_item_valid_to;
	private TextView txt_blkplt_item_crop_type;
	private TextView txt_blkplt_item_previous_crop;
	private TextView txt_blkplt_item_finish_date;
	private TextView txt_blkplt_item_reference;
	private TextView txt_blkplt_item_jarak_tanam;
	private TextView txt_blkplt_item_harvesting_date;
	private TextView txt_blkplt_item_harvested;
	private TextView txt_blkplt_item_plan_date;
	private TextView txt_blkplt_item_topography;
	private TextView txt_blkplt_item_soil_type;
	private TextView txt_blkplt_item_soil_category;
	private TextView txt_blkplt_item_previous_block;
	private TextView txt_blkplt_item_prod_trees;
	
	public ViewBlkpltItem(TextView txt_blkplt_item_id, TextView txt_blkplt_item_company_code,
			TextView txt_blkplt_item_estate, TextView txt_blkplt_item_block, TextView txt_blkplt_item_valid_from,
			TextView txt_blkplt_item_valid_to, TextView txt_blkplt_item_crop_type,
			TextView txt_blkplt_item_previous_crop, TextView txt_blkplt_item_finish_date,
			TextView txt_blkplt_item_reference, TextView txt_blkplt_item_jarak_tanam,
			TextView txt_blkplt_item_harvesting_date, TextView txt_blkplt_item_harvested,
			TextView txt_blkplt_item_plan_date, TextView txt_blkplt_item_topography,
			TextView txt_blkplt_item_soil_type, TextView txt_blkplt_item_soil_category,
			TextView txt_blkplt_item_previous_block, TextView txt_blkplt_item_prod_trees) {
		this.txt_blkplt_item_id = txt_blkplt_item_id;
		this.txt_blkplt_item_company_code = txt_blkplt_item_company_code;
		this.txt_blkplt_item_estate = txt_blkplt_item_estate;
		this.txt_blkplt_item_block = txt_blkplt_item_block;
		this.txt_blkplt_item_valid_from = txt_blkplt_item_valid_from;
		this.txt_blkplt_item_valid_to = txt_blkplt_item_valid_to;
		this.txt_blkplt_item_crop_type = txt_blkplt_item_crop_type;
		this.txt_blkplt_item_previous_crop = txt_blkplt_item_previous_crop;
		this.txt_blkplt_item_finish_date = txt_blkplt_item_finish_date;
		this.txt_blkplt_item_reference = txt_blkplt_item_reference;
		this.txt_blkplt_item_jarak_tanam = txt_blkplt_item_jarak_tanam;
		this.txt_blkplt_item_harvesting_date = txt_blkplt_item_harvesting_date;
		this.txt_blkplt_item_harvested = txt_blkplt_item_harvested;
		this.txt_blkplt_item_plan_date = txt_blkplt_item_plan_date;
		this.txt_blkplt_item_topography = txt_blkplt_item_topography;
		this.txt_blkplt_item_soil_type = txt_blkplt_item_soil_type;
		this.txt_blkplt_item_soil_category = txt_blkplt_item_soil_category;
		this.txt_blkplt_item_previous_block = txt_blkplt_item_previous_block;
		this.txt_blkplt_item_prod_trees = txt_blkplt_item_prod_trees;
	}
	
	public TextView getTxtBlkpltItemId() {
		return txt_blkplt_item_id;
	}
	public void setTxtBlkpltItemId(TextView txt_blkplt_item_id) {
		this.txt_blkplt_item_id = txt_blkplt_item_id;
	}
	public TextView getTxtBlkpltItemCompanyCode() {
		return txt_blkplt_item_company_code;
	}
	public void setTxtBlkpltItemCompanyCode(
			TextView txt_blkplt_item_company_code) {
		this.txt_blkplt_item_company_code = txt_blkplt_item_company_code;
	}
	public TextView getTxtBlkpltItemEstate() {
		return txt_blkplt_item_estate;
	}
	public void setTxtBlkpltItemEstate(TextView txt_blkplt_item_estate) {
		this.txt_blkplt_item_estate = txt_blkplt_item_estate;
	}
	public TextView getTxtBlkpltItemBlock() {
		return txt_blkplt_item_block;
	}
	public void setTxtBlkpltItemBlock(TextView txt_blkplt_item_block) {
		this.txt_blkplt_item_block = txt_blkplt_item_block;
	}
	public TextView getTxtBlkpltItemValidFrom() {
		return txt_blkplt_item_valid_from;
	}
	public void setTxtBlkpltItemValidFrom(TextView txt_blkplt_item_valid_from) {
		this.txt_blkplt_item_valid_from = txt_blkplt_item_valid_from;
	}
	public TextView getTxtBlkpltItemValidTo() {
		return txt_blkplt_item_valid_to;
	}
	public void setTxtBlkpltItemValidTo(TextView txt_blkplt_item_valid_to) {
		this.txt_blkplt_item_valid_to = txt_blkplt_item_valid_to;
	}
	public TextView getTxtBlkpltItemCropType() {
		return txt_blkplt_item_crop_type;
	}
	public void setTxtBlkpltItemCropType(TextView txt_blkplt_item_crop_type) {
		this.txt_blkplt_item_crop_type = txt_blkplt_item_crop_type;
	}
	public TextView getTxtBlkpltItemPreviousCrop() {
		return txt_blkplt_item_previous_crop;
	}
	public void setTxtBlkpltItemPreviousCrop(
			TextView txt_blkplt_item_previous_crop) {
		this.txt_blkplt_item_previous_crop = txt_blkplt_item_previous_crop;
	}
	public TextView getTxtBlkpltItemFinishDate() {
		return txt_blkplt_item_finish_date;
	}
	public void setTxtBlkpltItemFinishDate(TextView txt_blkplt_item_finish_date) {
		this.txt_blkplt_item_finish_date = txt_blkplt_item_finish_date;
	}
	public TextView getTxtBlkpltItemReference() {
		return txt_blkplt_item_reference;
	}
	public void setTxtBlkpltItemReference(TextView txt_blkplt_item_reference) {
		this.txt_blkplt_item_reference = txt_blkplt_item_reference;
	}
	public TextView getTxtBlkpltItemJarakTanam() {
		return txt_blkplt_item_jarak_tanam;
	}
	public void setTxtBlkpltItemJarakTanam(TextView txt_blkplt_item_jarak_tanam) {
		this.txt_blkplt_item_jarak_tanam = txt_blkplt_item_jarak_tanam;
	}
	public TextView getTxtBlkpltItemHarvestingDate() {
		return txt_blkplt_item_harvesting_date;
	}
	public void setTxtBlkpltItemHarvestingDate(
			TextView txt_blkplt_item_harvesting_date) {
		this.txt_blkplt_item_harvesting_date = txt_blkplt_item_harvesting_date;
	}
	public TextView getTxtBlkpltItemHarvested() {
		return txt_blkplt_item_harvested;
	}
	public void setTxtBlkpltItemHarvested(TextView txt_blkplt_item_harvested) {
		this.txt_blkplt_item_harvested = txt_blkplt_item_harvested;
	}
	public TextView getTxtBlkpltItemPlanDate() {
		return txt_blkplt_item_plan_date;
	}
	public void setTxtBlkpltItemPlanDate(TextView txt_blkplt_item_plan_date) {
		this.txt_blkplt_item_plan_date = txt_blkplt_item_plan_date;
	}
	public TextView getTxtBlkpltItemTopography() {
		return txt_blkplt_item_topography;
	}
	public void setTxtBlkpltItemTopography(TextView txt_blkplt_item_topography) {
		this.txt_blkplt_item_topography = txt_blkplt_item_topography;
	}
	public TextView getTxtBlkpltItemSoilType() {
		return txt_blkplt_item_soil_type;
	}
	public void setTxtBlkpltItemSoilType(TextView txt_blkplt_item_soil_type) {
		this.txt_blkplt_item_soil_type = txt_blkplt_item_soil_type;
	}
	public TextView getTxtBlkpltItemSoilCategory() {
		return txt_blkplt_item_soil_category;
	}
	public void setTxtBlkpltItemSoilCategory(
			TextView txt_blkplt_item_soil_category) {
		this.txt_blkplt_item_soil_category = txt_blkplt_item_soil_category;
	}
	public TextView getTxtBlkpltItemPreviousBlock() {
		return txt_blkplt_item_previous_block;
	}
	public void setTxtBlkpltItemPreviousBlock(
			TextView txt_blkplt_item_previous_block) {
		this.txt_blkplt_item_previous_block = txt_blkplt_item_previous_block;
	}
	public TextView getTxtBlkpltItemProdTrees() {
		return txt_blkplt_item_prod_trees;
	}
	public void setTxtBlkpltItemProdTrees(TextView txt_blkplt_item_prod_trees) {
		this.txt_blkplt_item_prod_trees = txt_blkplt_item_prod_trees;
	}
	
	
	
	
}
