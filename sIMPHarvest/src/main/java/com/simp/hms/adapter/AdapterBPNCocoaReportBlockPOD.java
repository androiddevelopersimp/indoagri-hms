package com.simp.hms.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.simp.hms.R;
import com.simp.hms.adapter.view.ViewBPNCocoaReportBlockItem;
import com.simp.hms.adapter.view.ViewBPNCocoaReportBlockItemPOD;
import com.simp.hms.model.BPNCocoaReportBlock;
import com.simp.hms.model.BPNCocoaReportBlockPOD;
import com.simp.hms.routines.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterBPNCocoaReportBlockPOD extends BaseAdapter {
    private Context context;
    private List<BPNCocoaReportBlockPOD> lstSummary, lstTemp;
    private int layout;

    public AdapterBPNCocoaReportBlockPOD(Context context, List<BPNCocoaReportBlockPOD> lstSummary, int layout){
        this.context = context;
        this.lstSummary = lstSummary;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return lstSummary.size();
    }

    @Override
    public Object getItem(int pos) {
        return lstSummary.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        BPNCocoaReportBlockPOD item = lstSummary.get(pos);

        ViewBPNCocoaReportBlockItemPOD view;
        TextView txtBpnReportBlockItemBlock;

        TextView txtBpnReportBlockItemQtyPODQty;
        TextView txtBpnReportBlockItemQtyGoodWeight;
        TextView txtBpnReportBlockItemQtyEstimasiQty;
        TextView txtBpnReportBlockItemQtyPoorWeight;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(layout, null);

            txtBpnReportBlockItemBlock = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemBlock);

            txtBpnReportBlockItemQtyPODQty = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyPODQty);
            txtBpnReportBlockItemQtyGoodWeight = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyGoodWeight);
            txtBpnReportBlockItemQtyEstimasiQty = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyEstimasiQty);
            txtBpnReportBlockItemQtyPoorWeight = (TextView) convertView.findViewById(R.id.txtBpnReportBlockItemQtyPoorWeight);

            view = new ViewBPNCocoaReportBlockItemPOD(txtBpnReportBlockItemBlock,
                    txtBpnReportBlockItemQtyPODQty, txtBpnReportBlockItemQtyGoodWeight,
                    txtBpnReportBlockItemQtyEstimasiQty, txtBpnReportBlockItemQtyPoorWeight);


            convertView.setTag(view);
        }else{
            view = (ViewBPNCocoaReportBlockItemPOD) convertView.getTag();

            txtBpnReportBlockItemBlock = view.getTxtBpnReportBlockItemBlock();
            txtBpnReportBlockItemQtyPODQty = view.getTxtBpnReportBlockItemQtyPODQty();
            txtBpnReportBlockItemQtyGoodWeight = view.getTxtBpnReportBlockItemQtyGoodWeight();
            txtBpnReportBlockItemQtyEstimasiQty = view.getTxtBpnReportBlockItemQtyEstimasiQty();
            txtBpnReportBlockItemQtyPoorWeight = view.getTxtBpnReportBlockItemQtyPoorWeight();
        }

        txtBpnReportBlockItemBlock.setText(item.getBlock());
        txtBpnReportBlockItemQtyPODQty.setText(String.valueOf(Utils.round(item.getQtyPODQty(), 0)));
        txtBpnReportBlockItemQtyGoodWeight.setText(String.valueOf(Utils.round(item.getQtyGoodWeight(), 0)));
        txtBpnReportBlockItemQtyEstimasiQty.setText(String.valueOf(Utils.round(item.getQtyEstimasiQty(), 0)));
        txtBpnReportBlockItemQtyPoorWeight.setText(String.valueOf(Utils.round(item.getQtyPoorWeight(), 0)));

        return convertView;
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                lstSummary = null;
                lstSummary = (List<BPNCocoaReportBlockPOD>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                List<BPNCocoaReportBlockPOD> lstResult = new ArrayList<BPNCocoaReportBlockPOD>();
                BPNCocoaReportBlockPOD item;

                if(lstTemp == null){
                    lstTemp = new ArrayList<BPNCocoaReportBlockPOD>(lstSummary);
                }

                if(constraint == null || constraint.length() == 0){
                    result.count = lstTemp.size();
                    result.values = lstTemp;
                }else{
                    constraint = constraint.toString().toLowerCase(Locale.getDefault());

                    for(int i = 0; i < lstTemp.size(); i++){
                        item = lstTemp.get(i);

                        if(item.getBlock().toLowerCase(Locale.getDefault()).contains(constraint)){
                            lstResult.add(item);
                        }
                    }
                    result.count = lstResult.size();
                    result.values = lstResult;
                }

                return result;
            }
        };
        return filter;
    }
}
