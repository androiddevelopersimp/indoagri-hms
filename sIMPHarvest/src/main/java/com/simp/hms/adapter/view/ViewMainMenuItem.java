package com.simp.hms.adapter.view;

import android.widget.ImageView;
import android.widget.TextView;

public class ViewMainMenuItem {
	private ImageView img_main_menu_item;
	private TextView txt_main_menu_item;
	
	public ViewMainMenuItem(ImageView img_main_menu_item, TextView txt_main_menu_item) {
		super();
		this.img_main_menu_item = img_main_menu_item;
		this.txt_main_menu_item = txt_main_menu_item;
	}

	public ImageView getImgMainMenuItem() {
		return img_main_menu_item;
	}

	public void setImgMainMenuItem(ImageView img_main_menu_item) {
		this.img_main_menu_item = img_main_menu_item;
	}

	public TextView getTxtMainMenuItem() {
		return txt_main_menu_item;
	}

	public void setTxtMainMenuItem(TextView txt_main_menu_item) {
		this.txt_main_menu_item = txt_main_menu_item;
	}
}
