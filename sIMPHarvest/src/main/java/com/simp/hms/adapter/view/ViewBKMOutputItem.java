package com.simp.hms.adapter.view;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ViewBKMOutputItem {
	private TextView txtBkmOutputItemName;
	private TextView txtBkmOutputItemAbsentType;
	private TextView txtBkmOutputItemMandays;
	private Button btnBKMOutputItemOutputMin;
	private EditText edtBkmOutputItemOutput;
	private Button btnBKMOutputItemOutputPlus;
	private boolean isVisible;
	
	public ViewBKMOutputItem(TextView txtBkmOutputItemName,
			TextView txtBkmOutputItemAbsentType,
			TextView txtBkmOutputItemMandays, Button btnBKMOutputItemOutputMin, EditText edtBkmOutputItemOutput, 
			Button btnBKMOutputItemOutputPlus, boolean isVisible) {
		this.txtBkmOutputItemName = txtBkmOutputItemName;
		this.txtBkmOutputItemAbsentType = txtBkmOutputItemAbsentType;
		this.txtBkmOutputItemMandays = txtBkmOutputItemMandays;
		this.btnBKMOutputItemOutputMin = btnBKMOutputItemOutputMin;
		this.edtBkmOutputItemOutput = edtBkmOutputItemOutput;
		this.btnBKMOutputItemOutputPlus = btnBKMOutputItemOutputPlus;
		this.isVisible = isVisible;
	}

	public TextView getTxtBkmOutputItemName() {
		return txtBkmOutputItemName;
	}

	public void setTxtBkmOutputItemName(TextView txtBkmOutputItemName) {
		this.txtBkmOutputItemName = txtBkmOutputItemName;
	}

	public TextView getTxtBkmOutputItemAbsentType() {
		return txtBkmOutputItemAbsentType;
	}

	public void setTxtBkmOutputItemAbsentType(TextView txtBkmOutputItemAbsentType) {
		this.txtBkmOutputItemAbsentType = txtBkmOutputItemAbsentType;
	}

	public TextView getTxtBkmOutputItemMandays() {
		return txtBkmOutputItemMandays;
	}

	public void setTxtBkmOutputItemMandays(TextView txtBkmOutputItemMandays) {
		this.txtBkmOutputItemMandays = txtBkmOutputItemMandays;
	}
	

	public Button getBtnBKMOutputItemOutputMin() {
		return btnBKMOutputItemOutputMin;
	}

	public void setBtnBKMOutputItemOutputMin(Button btnBKMOutputItemOutputMin) {
		this.btnBKMOutputItemOutputMin = btnBKMOutputItemOutputMin;
	}

	public EditText getEdtBKMOutputItemOutput() {
		return edtBkmOutputItemOutput;
	}

	public void setTxtBkmOutputItemOutput(EditText edtBkmOutputItemOutput) {
		this.edtBkmOutputItemOutput = edtBkmOutputItemOutput;
	}
	
	public Button getBtnBKMOutputItemOutputPlus() {
		return btnBKMOutputItemOutputPlus;
	}

	public void setBtnBKMOutputItemOutputPlus(Button btnBKMOutputItemOutputPlus) {
		this.btnBKMOutputItemOutputPlus = btnBKMOutputItemOutputPlus;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
}
