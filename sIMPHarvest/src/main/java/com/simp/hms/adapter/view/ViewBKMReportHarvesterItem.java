package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBKMReportHarvesterItem {
	private TextView txtBkmReportHarvesterItemHarvesterName;
	private TextView txtBkmReportHarvesterItemAbsentType;
	private TextView txtBkmReportHarvesterItemMandays;
	private TextView txtBkmReportHarvesterItemOutput;

	public ViewBKMReportHarvesterItem(
			TextView txtBkmReportHarvesterItemHarvesterName,
			TextView txtBkmReportHarvesterItemAbsentType,
			TextView txtBkmReportHarvesterItemMandays,
			TextView txtBkmReportHarvesterItemOutput) {
		super();
		this.txtBkmReportHarvesterItemHarvesterName = txtBkmReportHarvesterItemHarvesterName;
		this.txtBkmReportHarvesterItemAbsentType = txtBkmReportHarvesterItemAbsentType;
		this.txtBkmReportHarvesterItemMandays = txtBkmReportHarvesterItemMandays;
		this.txtBkmReportHarvesterItemOutput = txtBkmReportHarvesterItemOutput;
	}

	public TextView getTxtBkmReportHarvesterItemHarvesterName() {
		return txtBkmReportHarvesterItemHarvesterName;
	}

	public void setTxtBkmReportHarvesterItemHarvesterName(
			TextView txtBkmReportHarvesterItemHarvesterName) {
		this.txtBkmReportHarvesterItemHarvesterName = txtBkmReportHarvesterItemHarvesterName;
	}

	public TextView getTxtBkmReportHarvesterItemAbsentType() {
		return txtBkmReportHarvesterItemAbsentType;
	}

	public void setTxtBkmReportHarvesterItemAbsentType(
			TextView txtBkmReportHarvesterItemAbsentType) {
		this.txtBkmReportHarvesterItemAbsentType = txtBkmReportHarvesterItemAbsentType;
	}

	public TextView getTxtBkmReportHarvesterItemMandays() {
		return txtBkmReportHarvesterItemMandays;
	}

	public void setTxtBkmReportHarvesterItemMandays(
			TextView txtBkmReportHarvesterItemMandays) {
		this.txtBkmReportHarvesterItemMandays = txtBkmReportHarvesterItemMandays;
	}

	public TextView getTxtBkmReportHarvesterItemOutput() {
		return txtBkmReportHarvesterItemOutput;
	}

	public void setTxtBkmReportHarvesterItemOutput(
			TextView txtBkmReportHarvesterItemOutput) {
		this.txtBkmReportHarvesterItemOutput = txtBkmReportHarvesterItemOutput;
	}

}
