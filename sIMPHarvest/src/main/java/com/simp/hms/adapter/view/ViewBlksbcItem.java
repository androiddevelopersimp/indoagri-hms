package com.simp.hms.adapter.view;

import android.widget.TextView;

public class ViewBlksbcItem {
	private TextView txt_blksbc_item_id;
	private TextView txt_blksbc_item_company_code;
	private TextView txt_blksbc_item_estate;
	private TextView txt_blksbc_item_block;
	private TextView txt_blksbc_item_valid_from;
	private TextView txt_blksbc_item_basis_normal;
	private TextView txt_blksbc_item_basis_friday;
	private TextView txt_blksbc_item_basis_holiday;
	private TextView txt_blksbc_item_premi_normal;
	private TextView txt_blksbc_item_premi_friday;
	private TextView txt_blksbc_item_premi_holiday;
	
	public ViewBlksbcItem(TextView txt_blksbc_item_id, TextView txt_blksbc_item_company_code, TextView txt_blksbc_item_estate, TextView txt_blksbc_item_block,
			TextView txt_blksbc_item_valid_from, TextView txt_blksbc_item_basis_normal, TextView txt_blksbc_item_basis_friday, TextView txt_blksbc_item_basis_holiday,
			TextView txt_blksbc_item_premi_normal, TextView txt_blksbc_item_premi_friday, TextView txt_blksbc_item_premi_holiday) {
		this.txt_blksbc_item_id = txt_blksbc_item_id;
		this.txt_blksbc_item_company_code = txt_blksbc_item_company_code;
		this.txt_blksbc_item_estate = txt_blksbc_item_estate;
		this.txt_blksbc_item_block = txt_blksbc_item_block;
		this.txt_blksbc_item_valid_from = txt_blksbc_item_valid_from;
		this.txt_blksbc_item_basis_normal = txt_blksbc_item_basis_normal;
		this.txt_blksbc_item_basis_friday = txt_blksbc_item_basis_friday;
		this.txt_blksbc_item_basis_holiday = txt_blksbc_item_basis_holiday;
		this.txt_blksbc_item_premi_normal = txt_blksbc_item_premi_normal;
		this.txt_blksbc_item_premi_friday = txt_blksbc_item_premi_friday;
		this.txt_blksbc_item_premi_holiday = txt_blksbc_item_premi_holiday;
	}

	public TextView getTxtBlksbcItemId() {
		return txt_blksbc_item_id;
	}

	public void setTxtBlksbcItemId(TextView txt_blksbc_item_id) {
		this.txt_blksbc_item_id = txt_blksbc_item_id;
	}

	public TextView getTxtBlksbcItemCompanyCode() {
		return txt_blksbc_item_company_code;
	}

	public void setTxtBlksbcItemCompanyCode(
			TextView txt_blksbc_item_company_code) {
		this.txt_blksbc_item_company_code = txt_blksbc_item_company_code;
	}

	public TextView getTxtBlksbcItemEstate() {
		return txt_blksbc_item_estate;
	}

	public void setTxtBlksbcItemEstate(TextView txt_blksbc_item_estate) {
		this.txt_blksbc_item_estate = txt_blksbc_item_estate;
	}

	public TextView getTxtBlksbcItemBlock() {
		return txt_blksbc_item_block;
	}

	public void setTxtBlksbcItemBlock(TextView txt_blksbc_item_block) {
		this.txt_blksbc_item_block = txt_blksbc_item_block;
	}

	public TextView getTxtBlksbcItemValidFrom() {
		return txt_blksbc_item_valid_from;
	}

	public void setTxtBlksbcItemValidFrom(TextView txt_blksbc_item_valid_from) {
		this.txt_blksbc_item_valid_from = txt_blksbc_item_valid_from;
	}

	public TextView getTxtBlksbcItemBasisNormal() {
		return txt_blksbc_item_basis_normal;
	}

	public void setTxtBlksbcItemBasisNormal(
			TextView txt_blksbc_item_basis_normal) {
		this.txt_blksbc_item_basis_normal = txt_blksbc_item_basis_normal;
	}

	public TextView getTxtBlksbcItemBasisFriday() {
		return txt_blksbc_item_basis_friday;
	}

	public void setTxtBlksbcItemBasisFriday(
			TextView txt_blksbc_item_basis_friday) {
		this.txt_blksbc_item_basis_friday = txt_blksbc_item_basis_friday;
	}

	public TextView getTxtBlksbcItemBasisHoliday() {
		return txt_blksbc_item_basis_holiday;
	}

	public void setTxtBlksbcItemBasisHoliday(
			TextView txt_blksbc_item_basis_holiday) {
		this.txt_blksbc_item_basis_holiday = txt_blksbc_item_basis_holiday;
	}

	public TextView getTxtBlksbcItemPremiNormal() {
		return txt_blksbc_item_premi_normal;
	}

	public void setTxtBlksbcItemPremiNormal(
			TextView txt_blksbc_item_premi_normal) {
		this.txt_blksbc_item_premi_normal = txt_blksbc_item_premi_normal;
	}

	public TextView getTxtBlksbcItemPremiFriday() {
		return txt_blksbc_item_premi_friday;
	}

	public void setTxtBlksbcItemPremiFriday(
			TextView txt_blksbc_item_premi_friday) {
		this.txt_blksbc_item_premi_friday = txt_blksbc_item_premi_friday;
	}

	public TextView getTxtBlksbcItemPremiHoliday() {
		return txt_blksbc_item_premi_holiday;
	}

	public void setTxtBlksbcItemPremiHoliday(
			TextView txt_blksbc_item_premi_holiday) {
		this.txt_blksbc_item_premi_holiday = txt_blksbc_item_premi_holiday;
	}
}
