/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.simp.hms;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.simp.hms";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 81;
  public static final String VERSION_NAME = "1.9.3";
}
